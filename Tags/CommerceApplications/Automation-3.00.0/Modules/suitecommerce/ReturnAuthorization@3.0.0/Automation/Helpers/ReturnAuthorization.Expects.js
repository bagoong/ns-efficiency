defineHelper('ReturnAuthorization.Expects', 
{
	verifyReturnData: function(params, cb)
	{
		this
			.waitForExist(Selectors.ReturnAuthorization.returnList, 5000)	

			.getText(Selectors.ReturnAuthorization.returnTitle.replace('%s', params.returnId), function(err, text){
				expect(text).toMatch(params.returnNo);
			})

			.getText(Selectors.ReturnAuthorization.returnDate.replace('%s', params.returnId), function(err, text){
				expect(text).toMatch(params.returnDate);
			})

			.getText(Selectors.ReturnAuthorization.returnAmount.replace('%s', params.returnId), function(err, text){
				expect(text).toMatch(params.returnAmount);
			})

			.getText(Selectors.ReturnAuthorization.returnItemsQuantity.replace('%s', params.returnId), function(err, text){
				expect(text).toMatch(params.returnItemsQuantity);
			})

			.getText(Selectors.ReturnAuthorization.returnStatus.replace('%s', params.returnId), function(err, text){
				expect(text).toMatch(params.returnStatus);
			})

			.call(cb)
		;
	}

,	checkReturnAuthorizationPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.ReturnAuthorization.returnAuthorizationPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Return Authorization page is not present.');
        	})
        	.call(cb)
        ;
	}
});