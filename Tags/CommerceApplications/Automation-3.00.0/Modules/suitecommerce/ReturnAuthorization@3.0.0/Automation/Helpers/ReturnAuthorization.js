defineHelper('ReturnAuthorization', 
{

	enterReturn: function(data_id, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ReturnAuthorization.enterReturn.replace('%s', data_id), 5000)	
			.click(Selectors.ReturnAuthorization.enterReturn.replace('%s', data_id))
			.waitForAjax()

			.call(cb)
		;
	}

});