defineHelper('ReturnAuthorization.Request.Expects', 
{
	verifyThankYouPage: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ReturnAuthorization.confirmationThankYou, 10000)
			.getText(Selectors.ReturnAuthorization.confirmationThankYou).then( function (text)
			{
				expect(text).toBeSameText('THANK YOU!');
			})

			.getText(Selectors.ReturnAuthorization.confirmationText).then( function (text)
			{
				expect(text[1]).toBeSameText('Your request was successfully submitted and a representative will contact you briefly. An email was sent to you with a copy of this request.');
			})

			.isExisting(Selectors.ReturnAuthorization.buttonGoToListRequest).then( function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Go to list of returns button is not displayed in confirmation retutn page.');
			})

			.call(cb)
		;
	}
});