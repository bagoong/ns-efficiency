defineHelper('TransactionHistory.Details.Expects', 
{
	
	checkHeader: function (params, cb)
	{
		this		

			.getText(Selectors.TransactionHistory.receiptNo).then(function (text)
			{
				expect(text).toMatch(params.receiptNo);
			})

			.getText(Selectors.TransactionHistory.receiptDate).then(function (text)
			{
				expect(text).toMatch(params.date);
			})

			.getText(Selectors.TransactionHistory.receiptAmount).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.getText(Selectors.TransactionHistory.receiptStatus).then(function (text)
			{
				expect(text).toMatch(params.status);
			})

			.call(cb)
		;
	}

,   checkPaymentInformation: function (params, cb)
	{
		var client = this;

		client
			.TransactionHistory.Details.Expects.checkPaymentMethod(
	            params.paymentMethod
	        )
    	;
    	if(params.billingAddress)
    	{
    		client
		        .TransactionHistory.Details.Expects.checkBillingAddress(
		            params.billingAddress
		        )
		    ;
    	}    
    	client    
			.call(cb)
		;
	}

,   checkPaymentInformationInvoice: function (params, cb)
	{
		var client = this;

		client
			.TransactionHistory.Details.Expects.checkPaymentMethod(
	            params.paymentMethod
	        )
    	;
    	if(params.billingAddress)
    	{
    		client
		        .TransactionHistory.Details.Expects.checkBillingAddressInvoice(
		            params.billingAddress
		        )
		    ;
    	}    
    	client    
			.call(cb)
		;
	}	

,   checkPaymentMethod: function (paymentMethod, cb)
	{
		var client = this;
		paymentMethod.forEach(function (payment)
		{
			
			if (payment.paymentType === 'Credit Card') {
	            client
	                .TransactionHistory.Details.Expects.checkCreditCard(payment)
	            ;
	        }
	        else if (payment.paymentType === 'Deposit' || payment.paymentType === 'Credit Memo')
	        {
	        	client
        			.TransactionHistory.Details.Expects.checkOtherPayment(payment)
        		;
	        }
	        else if (payment.paymentType === 'Cash' || payment.paymentType === 'Check' || payment.paymentType === 'Paypal')
	        {
	        	if(payment.internalId)
	        	{
	        		client
	        			.TransactionHistory.Details.Expects.checkPayment(payment);
	        	}
	        	else
	        	{
		            client
		            	.isExisting(Selectors.TransactionHistory.paymentMethod).then(function (isExisting)
						{
							expect(isExisting).toBeTrueOrFailWith('Payment Method is not present');
						})
						.getText(Selectors.TransactionHistory.paymentMethod).then(function (text)
						{
							expect(text).toMatch(payment.paymentInfo);
						})
					;
	        	}

	        }
		})
		this
			.call(cb)
		;
	}	

,   checkCreditCard: function (creditCard, cb)
	{
		this
			.getAttribute(Selectors.TransactionHistory.creditCardIcon, 'alt').then(function (text)
			{
				expect(text).toMatch(creditCard.ccName);
			})

			.isExisting(Selectors.TransactionHistory.creditCardNumber).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card Number is not present');
			})
			.getText(Selectors.TransactionHistory.creditCardNumber).then(function (text)
			{
				var regexCC = creditCard.ccNumber.match(/\d{4}$/i); 
				expect(text).toMatch(regexCC[0]);
			})

			.isExisting(Selectors.TransactionHistory.creditCardExpDate).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card ExpDate is not present');
			})
			.getText(Selectors.TransactionHistory.creditCardExpDate).then(function (text)
			{
				expect(text).toMatch(creditCard.ccExpireMonth + '/' + creditCard.ccExpireYear);
			})

			.isExisting(Selectors.TransactionHistory.creditCardName).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card Name is not present');
			})
			.getText(Selectors.TransactionHistory.creditCardName).then(function (text)
			{
				expect(text).toMatch(creditCard.nameInCard);
			})			

			.call(cb)
		;
	}
    
,   checkBillingAddress: function (params, cb)
	{
		var client = this;
		client 
			.isExisting(Selectors.TransactionHistory.billingAddress).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Billing Address is not displayed');
				if(isExisting){
					client
						.Address.getAddressDataType(
							{
								parentElementXPath: Selectors.TransactionHistory.billingAddress
							}
							, function (err, found_address) 
							{	
								if(found_address)
								{
									expect(params.fullname.toLowerCase()).toBe(found_address.fullname.toLowerCase());
									if(params.company || (found_address.company != ''))
									{
										expect(params.company.toLowerCase()).toBe(found_address.company.toLowerCase());
									}
									expect(params.addr1).toBe(found_address.addr1);
									expect(params.addr2).toBe(found_address.addr2);
									expect(params.city.toLowerCase()).toBe(found_address.city.toLowerCase());
									expect(params.country.toLowerCase()).toBe(found_address.country.toLowerCase());
									expect(params.state.toLowerCase()).toBe(found_address.state.toLowerCase());
									expect(params.zip).toBe(found_address.zip);
									if(params.phone || (found_address.phone != undefined))
									{
										expect(params.phone).toBe(found_address.phone);
									}						
								}
							}
						)
					;	
				}
			})

			.call(cb)
		;
	}

,   checkBillingAddressInvoice: function (params, cb)
	{
		var client = this;
		client 
			.isExisting(Selectors.TransactionHistory.billingAddress).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Billing Address is not displayed');
				if(isExisting){
					client
						.Address.getAddressDataType(
							{
								parentElementXPath: Selectors.TransactionHistory.billingAddress
							}
							, function (err, found_address) 
							{	
								if(found_address)
								{
									expect(params.fullname.toLowerCase()).toBe(found_address.fullname.toLowerCase());
									if(params.company || (found_address.company != ''))
									{
										expect(params.company.toLowerCase()).toBe(found_address.company.toLowerCase());
									}
									expect(params.addr1).toBe(found_address.addr1);
									expect(params.addr2).toBe(found_address.addr2);
									expect(params.city.toLowerCase()).toBe(found_address.city.toLowerCase());
									expect(params.country.toLowerCase()).toBe(found_address.country.toLowerCase());
									expect(params.state.toLowerCase()).toBe(found_address.state.toLowerCase());
									expect(params.zip).toBe(found_address.zip);
									if(params.phone || (found_address.phone != undefined))
									{
										expect(params.phone).toBe(found_address.phone);
									}						
								}
							}
						)
					;	
				}
			})

			.call(cb)
		;
	}

,	checkOtherPayment: function (payment, cb)
	{
		var client = this;
		client
			.isExisting(Selectors.TransactionHistory.otherPaymentNumber.replace('%s', payment.internalId)).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Other Payment Method ' + payment.number + ' is not present');
			})

			.getText(Selectors.TransactionHistory.otherPaymentNumber.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.number);
			})

			.getText(Selectors.TransactionHistory.otherPaymentDate.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.date);
			})

			.getText(Selectors.TransactionHistory.otherPaymentAmount.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.amount);
			})
			
			.call(cb)
		;
	}

,	checkPayment: function (payment, cb)
	{
		var client = this;
		client
			.isExisting(Selectors.TransactionHistory.paymentNumber.replace('%s', payment.internalId)).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Payment Method ' + payment.number + ' is not present');
			})

			.getText(Selectors.TransactionHistory.paymentNumber.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.number);
			})

			.getText(Selectors.TransactionHistory.paymentDate.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.date);
			})

			.getText(Selectors.TransactionHistory.paymentAmount.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.amount);
			})

			.getText(Selectors.TransactionHistory.paymentInfoCard.replace('%s', payment.internalId) + ' ' + Selectors.TransactionHistory.paymentMethod).then(function (text)
			{
				expect(text).toMatch(payment.paymentType);
			})
			
			.call(cb)
		;
	}


,   checkSummary: function (params, cb)
	{
		var client = this;

		if (params.discountTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryDiscount).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Discount is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryDiscount).then(function (text)
				{
					expect(text).toMatch(params.discountTotal);
				})
			;
		}

		if (params.shippingTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryShipping).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Shipping total is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryShipping).then(function (text)
				{
					expect(text).toMatch(params.shippingTotal);
				})
			;
		}

		if (params.handlingTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryHandling).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Handling total is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryHandling).then(function (text)
				{
					expect(text).toMatch(params.handlingTotal);
				})
			;
		}

		if (params.giftCertTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryGiftCert).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Gift Certificate is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryGiftCert).then(function (text)
				{
					expect(text).toMatch(params.giftCertTotal);
				})
			;
		}

		if (params.promoCode)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryPromoCode).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Promo code is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryPromoCode).then(function (text)
				{
					expect(text).toMatch(params.promoCode);
				})
			;
		}

		this
			.isExisting(Selectors.TransactionHistory.summarySubtotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summarySubtotal).then(function (text)
			{
				expect(text).toMatch(params.subTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTax).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Tax total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTax).then(function (text)
			{
				expect(text).toMatch(params.taxTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotal).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotalItems).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Amount of Items is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotalItems).then(function (text)
			{
				expect(text).toMatch(params.totalItems);
			})

			.call(cb)
		;
	}

,   checkSummaryInvoice: function (params, cb)
	{
		var client = this;

		if (params.discountTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryDiscountInvoice).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Discount is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryDiscountInvoice).then(function (text)
				{
					expect(text).toMatch(params.discountTotal);
				})
			;
		}

		if (params.shippingTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryShippingInvoice).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Shipping total is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryShippingInvoice).then(function (text)
				{
					expect(text).toMatch(params.shippingTotal);
				})
			;
		}

		if (params.handlingTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryHandlingInvoice).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Handling total is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryHandlingInvoice).then(function (text)
				{
					expect(text).toMatch(params.handlingTotal);
				})
			;
		}

		if (params.giftCertTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryGiftCertInvoice).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Gift Certificate is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryGiftCertInvoice).then(function (text)
				{
					expect(text).toMatch(params.giftCertTotal);
				})
			;
		}

		if (params.promoCode)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryPromoCodeInvoice).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Promo code is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryPromoCodeInvoice).then(function (text)
				{
					expect(text).toMatch(params.promoCode);
				})
			;
		}

		this
			.isExisting(Selectors.TransactionHistory.summarySubtotalInvoice).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summarySubtotalInvoice).then(function (text)
			{
				expect(text).toMatch(params.subTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTaxInvoice).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Tax total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTaxInvoice).then(function (text)
			{
				expect(text).toMatch(params.taxTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotalInvoice).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotalInvoice).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotalItemsInvoice).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Amount of Items is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotalItemsInvoice).then(function (text)
			{
				expect(text).toMatch(params.totalItems);
			})

			.call(cb)
		;
	}


,   checkSummaryCreditMemo: function (params, cb)
	{
		var client = this;

		if (params.discountTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryDiscountCM).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Discount is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryDiscountCM).then(function (text)
				{
					expect(text).toMatch(params.discountTotal);
				})
			;
		}

		if (params.shippingTotal)
		{
			client
				.isExisting(Selectors.TransactionHistory.summaryShippingCM).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Shipping total is not present in summary');
				})
				.getText(Selectors.TransactionHistory.summaryShippingCM).then(function (text)
				{
					expect(text).toMatch(params.shippingTotal);
				})
			;
		}


		this
			.isExisting(Selectors.TransactionHistory.summarySubtotalCM).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summarySubtotalCM).then(function (text)
			{
				expect(text).toMatch(params.subTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTaxCM).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Tax total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTaxCM).then(function (text)
			{
				expect(text).toMatch(params.taxTotal);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotalCM).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Total is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotalCM).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.isExisting(Selectors.TransactionHistory.summaryTotalItemsCM).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Amount of Items is not present in summary');
			})
			.getText(Selectors.TransactionHistory.summaryTotalItemsCM).then(function (text)
			{
				expect(text).toMatch(params.totalItems);
			})

			.call(cb)
		;
	}

,   checkItems: function (params, cb)
	{
		var client = this;
		if(params.itemList)
		{
			params.itemList.forEach(function (item)
			{
				client
					.getAttribute(Selectors.TransactionHistory.itemId, 'data-item-id').then(function (text){
						expect(text).toMatch(item.internalId);
					})

					.isExisting(Selectors.TransactionHistory.itemName.replace('%s', item.internalId)).then(function (isExisting)
					{
						expect(isExisting).toBeTrueOrFailWith('Item ' + item.name + ' is not present');
						if(isExisting)
						{
							client
								.getText(Selectors.TransactionHistory.itemName.replace('%s', item.internalId)).then(function (text){
									expect(text.toLowerCase()).toMatch(item.name.toLowerCase());
								})

								.getText(Selectors.TransactionHistory.itemSku.replace('%s', item.internalId)).then(function (text){
									expect(text.toLowerCase()).toMatch(item.sku.toLowerCase());
								})

								.getText(Selectors.TransactionHistory.itemPrice.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.price);
								})

								.getText(Selectors.TransactionHistory.itemQuantity.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.quantity);
								})

								.getText(Selectors.TransactionHistory.itemAmount.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.amount);
								})

								if(item.options)
								{
									item.options.forEach(function (option)
									{
										client
											.getText(Selectors.TransactionHistory.itemOptionLabel.replace('%s', item.internalId)).then(function (text){
												expect(text).toMatch(option.title);
											})

											.getText(Selectors.TransactionHistory.itemOptionValue.replace('%s', item.internalId)).then(function (text){
												expect(text).toMatch(option.value);
											})
										;
									})
								}
							;
						}
					})
				;
			})
		}

		client
			.call(cb)
		;
	}

});