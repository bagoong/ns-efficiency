defineHelper('TransactionHistory', 
{

	openTransaction: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.TransactionHistory.openTransaction.replace('%s', params.transactionId), 5000)

	    	.click(Selectors.TransactionHistory.openTransaction.replace('%s', params.transactionId))
	    	.waitForAjax()  

			.call(cb)
		;
	}
	
	, 

	getTransactionDetailsById: function(params, cb)
	{
		var client = this;
		var parent_element = Selectors.TransactionHistory.transactionItem.replace('%s', params) + ' ';
		var transactionItem = {
								  'transactionId' : params
								, 'transactionNo' : ''
								, 'date'          : ''
								, 'amount'        : ''
								, 'status'        : ''
								};

		this
		.waitForAjax()
		.waitForExist(parent_element, 5000)

		.getText(parent_element + Selectors.TransactionHistory.transactionNo).then(function (text){
			transactionItem.transactionNo = text;
		})

		.getText(parent_element + Selectors.TransactionHistory.transactionDate).then(function (text){
			transactionItem.date = text;
		})

		.getText(parent_element + Selectors.TransactionHistory.transactionAmount).then(function (text){
			transactionItem.amount = text;
		})

		.getText(parent_element + Selectors.TransactionHistory.transactionStatus).then(function (text){
			transactionItem.status = text;
		})

		.waitForAjax()  

		.call(function()
		{
			cb(null, transactionItem);
		})
	; 
	}

});