defineHelper('GlobalViews.PaymentMethod', 
{
	getPaymentMethodDataType: function(cb)
	{
		var client = this;

		this.isExisting(Selectors.GlobalViews.paymentMethodInvoiceContainer, function(err, is_invoice)
		{
			if (is_invoice)
			{
				client.GlobalViews.PaymentMethod.getInvoiceDataType(cb);
			}
			else
			{
				client.GlobalViews.PaymentMethod.getCreditCardDataType(cb);
			}
		});
	}


,	getCreditCardDataType: function(cb)
	{	
		var datatype = {
				'cardType': ''
			,	'cardEnding': ''
			,	'expMonth': ''
			,	'expYear': ''
			,	'name': ''
		};

		this
			.getAttribute(Selectors.GlobalViews.paymentMethodCreditCardIcon, 'alt', function(err, text)
			{
				datatype.cardType = text;
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardNumber, function(err, text)
			{
				var match_ending = text.match(/\d{4}$/i); 
				datatype.cardEnding = match_ending[0];
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardExpDate, function(err, text)
			{
				var match_date = text.match(/(\d+)\/(\d+)/);
				datatype.expMonth = match_date[1];
				datatype.expYear = match_date[2];
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardName, function(err, text)
			{
				datatype.name = text;
			})

			.call(function()
			{
				cb(null, datatype);
			})
		;
	}

,	getInvoiceDataType: function(cb)
	{
		var datatype = {
			'terms': ''
		,	'purchaseNumber': ''
		};

		this
			.getText(Selectors.GlobalViews.paymentMethodInvoiceTerms, function(err, text)
			{
				text = text.replace(/.*?terms/i, '').trim();
				datatype.terms = text;
			})

			.getText(Selectors.GlobalViews.paymentMethodInvoicePurchaseNumber, function(err, text)
			{
				if (typeof text !== 'undefined')
				{
					text = text.replace(/.*?:/i, '').trim();
					datatype.purchaseNumber = text;
				}
				
			})

			.call(function()
			{
				cb(null, datatype);
			})
		;
	}

});