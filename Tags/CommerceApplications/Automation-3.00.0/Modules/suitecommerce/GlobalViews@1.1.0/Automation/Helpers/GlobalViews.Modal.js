defineHelper('GlobalViews.Modal', 
{

	isModalOpen: function(cb)
	{
		this
			.waitForAjax()
			.isExisting(Selectors.GlobalViews.modalContainer, cb)
		;
	}

,	waitForModal: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.GlobalViews.modalContainer, 5000)
			.call(cb)
		;
	}

,	waitForModalClosure: function(cb)
	{
		this
			.waitForAjax()
			// reverse wait for exist
			.waitForExist(Selectors.GlobalViews.modalContainer, 5000, true)
			.call(cb)
		;
	}

,	clickYesOnModal: function(cb)
	{	
		this
			.waitForExist(Selectors.GlobalViews.confirm, 5000)
			.click(Selectors.GlobalViews.confirm)
			.waitForAjax()
			.call(cb)
		;
	}
});