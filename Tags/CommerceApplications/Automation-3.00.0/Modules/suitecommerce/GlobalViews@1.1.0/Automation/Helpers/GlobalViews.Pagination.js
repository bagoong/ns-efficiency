var async = require('async');

defineHelper('GlobalViews.Pagination',
{
	verifyPaginationLinkExistence: function (cb)
	{
		var client = this;

		this
			.waitForExist(Selectors.GlobalViews.actionable, 5000)
			.getText(Selectors.GlobalViews.actionable, function(err, text) {
			    if (text.length === 20)
			    {
			    	client
			    		.isExisting(Selectors.GlobalViews.paginationText, function(err, text) {
					        expect(text).toBe(true);
					    })
			    	;
			    } else {
			    	client
			    		.isExisting(Selectors.GlobalViews.paginationText, function(err, text) {
					        expect(text).toBe(false);
					    })
			    	;
			    }
			})
			.call(cb)
		;
	}

	/*
	@method searchBySelector
	@param {String} selector to search
	@param {Boolean} reset whether to return to the initial page after the search
	@returns {success: boolean, elements: [seleniumIds]}
	@usage:

	client
		//
		.GlobalViews.Pagination.searchBySelector(selector, true , function(err, result)
		{
			expect(result.success).toBe(exists);
		})
		.call(cb);

	*/
,	searchBySelector: function(selector, reset, cb)
	{
		var client = this
		,	search_exhausted = false
		,	start_url = ''
		,	found = {
				success: false
			,	elements: []
		};

		var test = function()
		{
			return search_exhausted || found.success;
		}

		var pageSearch = function(cb_until)
		{
			client.isExisting(Selectors.GlobalViews.buttonPaginatorNext, function(err, result)
			{
				search_exhausted = !result;
			})

			.elements(selector, function(err, result)
			{
				if (result.value.length > 0 )
				{
					found.success = true;
					found.elements = result.value;
				}
				else if (!search_exhausted)
				{
					client
						.click(Selectors.GlobalViews.buttonPaginatorNext)
						.pause(500)
						.waitForAjax();
				}
			})

			.call(cb_until);
		}

		var doneSearch = function()
		{
			if (!reset)
			{
				return cb(null, found);
			}

			client.url(start_url)
			.pause(500)
			.waitForAjax()
			.call(function()
			{
				cb(null, found);
			})
		}

		client
			.url(function(err, result)
			{
				start_url = result.value;
			})
			.call(function()
			{
				async.until(test, pageSearch, doneSearch);
			});
	}

,	clickGoToNextPage: function (cb)
	{
		var client = this;

		this
			.isVisible(Selectors.GlobalViews.buttonPaginatorNext, function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.click(Selectors.GlobalViews.buttonPaginatorNext)
						.waitForAjax()
					;
				}
			})
			.call(cb)
		;
	}

,	nextPageExistence: function (cb)
	{
		var client = this;
		this.isExisting(Selectors.GlobalViews.buttonPaginatorNext).then(function (isExisting)
		{
			client.call(function ()
			{
				cb(null, isExisting);
			});
		});
	}
});