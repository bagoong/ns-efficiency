defineHelper('GlobalViews.Message.Expects', 
{

	errorMessage: function(errorLabel, cb)
	{
		this
			.getText(Selectors.GlobalViews.errorLabel, function(err, text)
			{
				expect(text).toMatch(errorLabel);
			})
			.call(cb)
		;
	}

});