
defineHelper('OrderWizard.Module.MultiShipTo.EnableLink', 
{
	clickShipToMultipleAddresses: function(cb)
	{
		this
			.click(Selectors.OrderWizardModuleMultiShipToEnableLink.ChangeShippingModeLink)
			.waitForAjax()
			.call(cb)
		;
	}

});

