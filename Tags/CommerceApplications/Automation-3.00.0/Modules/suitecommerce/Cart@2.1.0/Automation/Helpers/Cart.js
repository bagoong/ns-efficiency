var _ = require('underscore');

defineHelper('Cart',
{

	clickProceedToCheckout: function(cb)
	{
		this
			.waitForExist(Selectors.Cart.buttonProceedToCheckout, 10000)
			.click(Selectors.Cart.buttonProceedToCheckout)
			.waitForAjax()
			// NECESARY PAUSE UNTIL WAIT FOR REDIRECT WORKS
			.pause(2000)
			.call(cb)
		;
	}

,	increaseItemQuantity: function(obj, cb)
	{
		this
			.pause(3000)
			.waitForExist(".item-views-cell-actionable-sku", 2000)
			.click("//*[@class='item-views-cell-actionable-sku']//span[normalize-space(text())='" + obj.sku + "']/../../../..//*[@class='cart-item-actions-item-list-actionable-edit-button']/a")
			.waitForExist("#in-modal-quantity", 2000)
			.execute("jQuery('#in-modal-quantity').val('" + obj.qty + "')")
			.click(".quick-view-confirmation-modal-view-cart-button")
			.waitForAjax()
			.call(cb)
		;
	}

,	getProductQty: function(cb)
	{
		var self = this;
		this
			.pause(3000)
			.getValue(".cart-item-summary-quantity-value", cb)
		;
	}

,	getOptionsSelected: function(item, cb)
	{
		var prefix = '.cart-detailed-left [data-item-id="' + item.id + '"] .item-views-selected-option'
		,	client = this;

		item.options = {};

		client
			.waitForExist(prefix, 10000)
			.getAttribute(prefix, 'name', function(err, names)
				{
				names = !_.isArray(names) ? [names] : names
				names.forEach(function(name)
					{
					client.getText(prefix + '[name="' + name + '"] .item-views-selected-option-label', function(err, label)
				{
						client.getText(prefix + '[name="' + name + '"] .item-views-selected-option-value', function(err, value)
					{
							item.options[name] = { 'label': label,  'value': value };
						});
					});
				});
					})
			.call(cb)
		;
	}

,	removeItem: function(sku, cb)
	{
		var prefix = "//*[@class='item-views-cell-actionable-sku']//span[normalize-space(text())='" + sku + "']/../../../.."
		this
			.waitForExist(prefix + "//button[@class='cart-item-actions-item-list-actionable-edit-button-drop']", 10000)
			.click(prefix + "//button[@class='cart-item-actions-item-list-actionable-edit-button-drop']")

			.waitForExist(prefix + "//a[@class='cart-item-actions-item-list-actionable-edit-content-remove']", 10000)
			.click(prefix + "//a[@class='cart-item-actions-item-list-actionable-edit-content-remove']")

			.waitForAjax()

			.call(cb)
		;
	}

,	removeItemByDataId: function(data_id, cb)
	{
		this
			.waitForExist('tr[data-item-id="' + data_id + '"] button[class="cart-item-actions-item-list-actionable-edit-button-drop"]', 5000)
			.click('tr[data-item-id="' + data_id + '"] button[class="cart-item-actions-item-list-actionable-edit-button-drop"]')

			.waitForExist('tr[data-item-id="' + data_id + '"] a[class="cart-item-actions-item-list-actionable-edit-content-remove"]', 5000)
			.click('tr[data-item-id="' + data_id + '"] a[class="cart-item-actions-item-list-actionable-edit-content-remove"]')

			.waitForAjax()

			.call(cb)
		;
	}

,	verifyCorrectItemQuantityAmount: function(params, cb)
	{
		this
			.waitForExist('tr[data-item-id="' + params.data_id + '"] a[class="item-views-cell-actionable-name-link"]', 5000)

			// CHECKS NAME
			.getText('tr[data-item-id="' + params.data_id + '"] a[class="item-views-cell-actionable-name-link"]', function(err, text)
			{
				expect(text).toBeSameText(params.name);
			})

			// CHECKS SKU
			.getText('tr[data-item-id="' + params.data_id + '"] span[class="item-views-cell-actionable-sku-value"]', function(err, text)
			{
				expect(text).toMatch(params.sku);
			})

			// CHECKS QUANTITY
			.getValue('tr[data-item-id="' + params.data_id + '"] input[name="quantity"]', function(err, text)
			{
				expect(text).toMatch(params.quantity);
			})

			// CHECKS AMOUNT
			.getText('tr[data-item-id="' + params.data_id + '"] span[class="cart-item-summary-amount-value"]', function(err, text)
			{
				expect(text).toMatch(params.amount);
			})

			.call(cb)
		;
	}

,	addItemToSaveForLater: function(sku, cb)
	{
		var prefix = "//*[@class='item-views-cell-actionable-sku']//span[normalize-space(text())='" + sku + "']/../../../.."
		this
			.waitForExist(prefix + "//button[@class='cart-item-actions-item-list-actionable-edit-button-drop']", 10000)
			.click(prefix + "//button[@class='cart-item-actions-item-list-actionable-edit-button-drop']")

			.waitForExist(prefix + "//a[@class='cart-item-actions-item-list-actionable-edit-content-remove']", 10000)
			.click(prefix + "//a[@class='cart-item-actions-item-list-actionable-edit-content-saveforlater']")

			.waitForAjax()

			.call(cb)
		;
	}

,	isElementInCart: function(sku, cb)
	{
		this
			.pause(3000)
			.isExisting("//*[@class='item-views-cell-actionable-sku']//span[normalize-space(text())='" + sku + "']", cb)
		;
	}

,	isInPage: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.cart-detailed', 10000)
			.waitForAjax()
			.isExisting('.cart-detailed', cb)
		;
	}

	/*	UNEXPECTED BEHAVIOUR AND UNTRUSTHFUL CODE
		USE THE SAFE VERSIONS: getItemDataTypeById or getItemDataTypeBySku

                      .
                     \'/
                   -=>*<=-
                  .-"/.\
                 /    '
                _|
             _.|_|._
           .'       '.
          /           \
         |         #   |
         |             |
          \           /
           '.       .'
		     `'---'`
	*/
,	getItemDataType: function(item, cb)
	{

		this
			.Cart.getItemDataTypeById(item.id, function(err, itemData)
			{
				Object.keys(itemData).forEach(function(key)
				{
					item[key] = itemData[key];
				});
			})
			.call(cb)
		;
	}


,	getItemDataTypeById: function(dataItemId, cb)
	{
		var prefix = '.cart-detailed-left [data-item-id="' + dataItemId + '"] ';

		var item = {
			id: dataItemId
		}

		this
			.waitForAjax()
			.waitForExist(prefix + '.item-views-cell-actionable-name-link', 10000)
			.getText(prefix + '.item-views-cell-actionable-name-link', function(err, text){
				item.name = text;
			})
			.getText(prefix + '.item-views-price-lead', function(err, text){
				item.itemPrice = text;
			})
			.getText(prefix + '.item-views-cell-actionable-sku-value', function(err, text){
				item.sku = text;
			})
			.getText(prefix + '.cart-item-summary-amount-value', function(err, text){
				item.itemTotal = text;
			})
			.getAttribute(prefix + 'a img', 'src', function(err, text){
				item.imageUrl = text;
			})

			// PLEASE FIX THIS IN SOME MOMENT, NEEDS A CALLBACK OR PROMISE
			.Cart.getOptionsSelected(item)

			.call(function()
			{
				cb(null, item);
			})
		;
	}


,	getItemDataTypeBySku: function(sku, cb)
	{
		var prefix = "//*[@class='item-views-cell-actionable-sku']//span[normalize-space(text())='" + sku + "']/../../../.."

		var dataType = {
			'Name': ''
		,	'Sku': ''
		,	'ItemPrice': ''
		,	'ItemTotal': ''
		,	'Quantity': ''
		,	'Id': ''
		}

		this
			.waitForExist(prefix + "//*[@class='item-views-cell-actionable-name-link']", 10000)
			.getText(prefix + "//*[@class='item-views-cell-actionable-name-link']", function(err, text){
				dataType.Name = text;
			})
			.getText(prefix + "//*[@class='item-views-price-lead']", function(err, text){
				dataType.ItemPrice = text;
			})
			.getText(prefix + "//*[@class='item-views-cell-actionable-sku-value']", function(err, text){
				dataType.Sku = text;
			})
			.getValue(prefix + "//*[@name='quantity']", function(err, text){
				dataType.Quantity = text;
			})
			.getText(prefix + "//*[@class='cart-item-summary-amount-value']", function(err, text){
				dataType.ItemTotal = text;
			})
			.getAttribute(prefix, "data-item-id", function(err, text){
				dataType.Id = text;
			})
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}
});