defineHelper('Cart.Expects', 
{
	isItemCorrectlyAdded: function(item, cb)
	{
		this
			.Cart.getItemDataTypeBySku(item.Sku, function(err, data){
	            var name_matcher = new RegExp('^' + data.Name + '$', 'i')
	            expect(item.Name).toMatch(name_matcher);
	            expect(item.Sku).toEqual(data.Sku);
	            expect(item.ItemPrice).toEqual(data.ItemPrice);
	            expect(item.ItemTotal).toEqual(data.ItemTotal);
	            expect(item.Quantity).toEqual(data.Quantity);
        	}) 
        	.call(cb)
        ;
	}
});