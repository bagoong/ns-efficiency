var _ = require('underscore');

defineHelper('Cart.Confirmation',
{
	getProductDataType: function(cb)
	{
		var prefix = "//*[@class='cart-confirmation-modal']"
		var dataType = {
			'name': ''
		,	'sku': ''
		,	'itemPrice': ''
		,	'quantity': ''
		}

		this
			.waitForAjax()
			.waitForExist(prefix, 10000)

			.getText(prefix + "//*[@class='cart-confirmation-modal-item-name']", function(err, text){
				dataType.name = text;
			})
			.getText(prefix + "//*[@class='item-views-price-lead']", function(err, text){
				dataType.itemPrice = text;
			})
			.getText(prefix + "//*[@class='cart-confirmation-modal-sku']", function(err, text){
				dataType.sku = text;
				//console.log("SKU: "+text)
			})
			.getText(prefix + "//*[@class='cart-confirmation-modal-quantity-number']", function(err, text){
				dataType.quantity = text;
			})
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}
,	getGiftCertificateData: function(cb)
	{

		var prefix = "//*[@class='cart-confirmation-modal']"
		var dataType = [
			{
				'dataCartOptionId': 'GIFTCERTFROM'
			,	'label': ''
			,	'value': ''
			}
		,	{
				'dataCartOptionId': 'GIFTCERTRECIPIENTNAME'
			,	'label': ''
			,	'value': ''
			}
		,	{
				'dataCartOptionId': 'GIFTCERTRECIPIENTEMAIL'
			,	'label': ''
			,	'value': ''
			}
		,	{
				'dataCartOptionId': 'GIFTCERTMESSAGE'
			,	'label': ''
			,	'value': ''
			}
		];

		this
			.waitForAjax()
			.waitForExist(prefix + "//*[@data-view='Item.SelectedOptions']", 10000)

			.getText(prefix + "//*[@name='From']//*[@class='item-views-selected-option-label']", function(err, text){
				dataType[0].label = text;
			})
			.getText(prefix + "//*[@name='From']//*[@class='item-views-selected-option-value']", function(err, text){
				dataType[0].value = text
			})
			.getText(prefix + "//*[@name='Recipient Name']//*[@class='item-views-selected-option-label']", function(err, text){
				dataType[1].label = text;
			})
			.getText(prefix + "//*[@name='Recipient Name']//*[@class='item-views-selected-option-value']", function(err, text){
				dataType[1].value = text
			})
			.getText(prefix + "//*[@name='Recipient Email']//*[@class='item-views-selected-option-label']", function(err, text){
				dataType[2].label = text;
			})
			.getText(prefix + "//*[@name='Recipient Email']//*[@class='item-views-selected-option-value']", function(err, text){
				dataType[2].value = text
			})
			.getText(prefix + "//*[@name='Gift Message']//*[@class='item-views-selected-option-label']", function(err, text){
				dataType[3].label = text;
			})
			.getText(prefix + "//*[@name='Gift Message']//*[@class='item-views-selected-option-value']", function(err, text){
				dataType[3].value = text
			})
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}
,	clickViewCartAndCheckout: function(cb)
	{
		this
			.waitForExist(".cart-confirmation-modal-view-cart-button", 10000)
			.click(".cart-confirmation-modal-view-cart-button")
			.waitForAjax()
			// NECESARY PAUSE UNTIL WAIT FOR REDIRECT WORKS
			.pause(2000)
			.call(cb)
		;
	}
});