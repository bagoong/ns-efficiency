defineHelper('PrintStatement.Expects', 
{

	checkPrintStatementPageDisplayed: function (cb)
	{
		this
			.waitForAjax()
			.isExisting(Selectors.OrderHistory.printStatementPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Print A Statement page is not present.');
        	})
        	.call(cb)
        ;
	}

});