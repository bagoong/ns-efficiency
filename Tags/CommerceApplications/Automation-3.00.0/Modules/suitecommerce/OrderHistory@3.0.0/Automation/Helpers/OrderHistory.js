defineHelper('OrderHistory',
{
	enterOrder: function(params, cb)
	{
		var client = this;
		this
			.waitForAjax()

			.OrderHistory.paginateUntilPurchaseIsPresent(params.purchaseId, function (err, isPresent)
			{
				if(isPresent)
				{
					client
				    	.click(Selectors.OrderHistory.orderButton.replace('%s', params.purchaseId))
				    	.waitForAjax()
				}
				else
				{
					pending('Purchase is not present');
				}
			})

			.call(cb)
		;
	}

,	paginateUntilPurchaseIsPresent: function(purchaseId, cb)
	{
		var client = this;

		var itemSelector = Selectors.OrderHistory.orderButton.replace('%s', purchaseId)

		client.GlobalViews.Pagination.searchBySelector(itemSelector, false, function(err, result)
		{
			cb(null, result.success);
		});
	}

,	getOrderDetailsById: function(params, cb)
	{
		var client = this;
		var parent_element = Selectors.OrderHistory.order.replace('%s', params) + ' ';
		var purchase = {
			'purchaseId'     : params
		,	'purchaseNo'     : ''
		,	'date'           : ''
		,	'amount'         : ''
		,	'origin'         : ''
		,	'trackItems'     : ''
		};

		this
			.waitForAjax()
			.waitForExist(parent_element, 5000)

			.getText(parent_element + Selectors.OrderHistory.purchaseNo).then(function (text){
				purchase.purchaseNo = text;
			})

			.getText(parent_element + Selectors.OrderHistory.purchaseDate).then(function (text){
				purchase.date = text;
			})

			.getText(parent_element + Selectors.OrderHistory.purchaseAmount).then(function (text){
				purchase.amount = text;
			})

	    	.isExisting(parent_element + Selectors.OrderHistory.purchaseOrigin).then(function (exists){
				if(exists)
				{
					client
						.getText(parent_element + Selectors.OrderHistory.purchaseOrigin).then(function (text){
							purchase.origin = text;
						})
					;
				}
			})

			.OrderHistory.getTrakingValue(params, function (err, values){
				purchase.trackItems = values;
			})

	    	.waitForAjax()

			.call(function()
			{
				cb(null, purchase);
			})
		;
	}

,	getOrderDetailsWithoutSCISBundleById: function(params, cb)
	{
		var client = this;
		var parent_element = Selectors.OrderHistory.order.replace('%s', params) + ' ';
		var purchase = {
			'purchaseId'     : params
		,	'purchaseNo'     : ''
		,	'date'           : ''
		,	'amount'         : ''
		,	'status'         : ''
		,	'trackItems'     : ''
		};

		this
			.waitForAjax()
			.waitForExist(parent_element, 5000)

			.getText(parent_element + Selectors.OrderHistory.purchaseNo).then(function (text){
				purchase.purchaseNo = text;
			})

			.getText(parent_element + Selectors.OrderHistory.purchaseDate).then(function (text){
				purchase.date = text;
			})

			.getText(parent_element + Selectors.OrderHistory.purchaseAmount).then(function (text){
				purchase.amount = text;
			})

	    	.getText(parent_element + Selectors.OrderHistory.purchaseStatus).then(function (text){
				purchase.status = text;
			})

			.OrderHistory.getTrakingValue(params, function (err, values){
				purchase.trackItems = values;
			})

	    	.waitForAjax()

			.call(function()
			{
				cb(null, purchase);
			})
		;
	}

,	getTrakingValue: function(orderId, cb)
	{
		var parent_element = Selectors.OrderHistory.order.replace('%s', orderId) + ' ';
		var trackingNumber = '';
		var client = this;
		this
			.isExisting(parent_element + Selectors.OrderHistory.trackingNumberEmpty).then(function (exists){
				if(exists)
				{
					client
						.getText(parent_element + Selectors.OrderHistory.trackingNumberEmpty).then(function (text){
							trackingNumber = text;
						})
						.call(function()
						{
							cb(null, trackingNumber);
						})
					;
				}
				else
				{
					client
						.isExisting(parent_element + Selectors.OrderHistory.trackingNumberListButton).then(function (exists){
							if(exists)
							{
								client
									.click(parent_element + Selectors.OrderHistory.trackingNumberListButton)
								;
							}
							client
								.getText(parent_element + Selectors.OrderHistory.trackingNumber).then(function (text){
									trackingNumber = text;
								})
								.call(function()
								{
									cb(null, trackingNumber);
								})
							;
						})
					;
				}
			})
		;
	}

,	clickOpenOrders: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.filterByOpen, 5000)

			.click(Selectors.OrderHistory.filterByOpen)
			.waitForAjax()

			.call(cb)
		;
	}

,	clickInStoreOrders: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.filterByInStore, 5000)

			.click(Selectors.OrderHistory.filterByInStore)
			.waitForAjax()

			.call(cb)
		;
	}

,	clickAllOrders: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.filterByAll, 5000)

			.click(Selectors.OrderHistory.filterByAll)
			.waitForAjax()

			.call(cb)
	}

,	cancelOrder: function(cb)
	{
		this
			.waitForExist(Selectors.OrderHistory.cancelOrderButton, 5000)
			.click(Selectors.OrderHistory.cancelOrderButton)

			.waitForExist(Selectors.OrderHistory.cancelOrderButtonModal, 5000)
			.click(Selectors.OrderHistory.cancelOrderButtonModal)

			.call(cb)
		;
	}


/*
,	verifyExpectedStatus: function(param, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.recordStatus, 5000)
			.getText(Selectors.OrderHistory.recordStatus, function(err, text){
				expect(param.expectedMessage).toMatch(text);
			})
			.call(cb)
		;
	}
*/
,	enterFirstOrder: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.recordTitle)
	    	.click(Selectors.OrderHistory.firstElement)
	    	.waitForAjax()
			.call(cb)
		;
	}

});