defineHelper('OrderHistory.TrackingNumber', 
{

	verifyTrackingStatus: function(param, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.trackingNumber, 5000)
			.getText(Selectors.OrderHistory.trackingNumber).then(function (text){
				expect(param.expectedMessage).toMatch(text);				
			})
			.call(cb)
		;	
	}	

});