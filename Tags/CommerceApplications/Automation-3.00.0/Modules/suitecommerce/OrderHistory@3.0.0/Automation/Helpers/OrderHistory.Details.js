defineHelper('OrderHistory.Details',
{
	openAccordeons: function (cb)
	{
		var client = this;
		this
			.waitForAjax()
			.isExisting(Selectors.OrderHistory.detailHeadAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.OrderHistory.detailHeadAccordion)
						.waitForAjax()
					;
				}
			})
			.isExisting(Selectors.OrderHistory.detailSecondaryAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.OrderHistory.detailSecondaryAccordion)
						.waitForAjax()
						.isExisting(Selectors.OrderHistory.detailSecondaryAccordionCollapsed).then(function (isCollapsed)
						{
					        if(isCollapsed)
					        {
					        	client
									.click(Selectors.OrderHistory.detailSecondaryAccordionCollapsed)
									.waitForAjax()
								;
					        }
					    })
					;
				}
			})
			.isExisting(Selectors.OrderHistory.detailSecondaryShippingAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.OrderHistory.detailSecondaryShippingAccordion)
						.waitForAjax()
						.isExisting(Selectors.OrderHistory.detailSecondaryShippingAccordionCollapsed).then(function (isCollapsed)
						{
					        if(isCollapsed)
					        {
					        	client
									.click(Selectors.OrderHistory.detailSecondaryShippingAccordionCollapsed)
									.waitForAjax()
								;
					        }
					    })
					;
				}
			})

			.call(cb)
		;
	}

,	getTrakingValue: function(fulfillmentId, cb)
	{
		var parent_element = Selectors.OrderHistory.fulfillmentId.replace('%s', fulfillmentId) + ' ';
		var trackingNumber = '';
		var client = this;
		client
			.pause(5000)
			.isExisting(parent_element + Selectors.OrderHistory.fulfillmentTrackingNumberListButton).then(function (exists){
				if(exists)
				{
					client
						.click(parent_element + Selectors.OrderHistory.fulfillmentTrackingNumberListButton)
						.getText(parent_element + Selectors.OrderHistory.fulfillmentTrackingNumbers).then(function (text){
							trackingNumber = text;
						})
						.call(function()
						{
							cb(null, trackingNumber);
						})
					;
				}
				else
				{
					client
						.isExisting(parent_element + Selectors.OrderHistory.fulfillmentTrackingNumbers).then(function (exists){
							if(exists)
							{
								client
									.getText(parent_element + Selectors.OrderHistory.fulfillmentTrackingNumbers).then(function (text){
										trackingNumber = text;
									})
									.call(function()
									{
										cb(null, trackingNumber);
									})
								;
							}
							else
							{
								client
									.call(function()
									{
										cb(null, '');
									})
								;
							}
						})
					;
				}
			})
		;
	}

,  clickDownloadPDF: function (cb)
	{
		this
			.click(Selectors.OrderHistory.buttonDownloadPDF)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickRequestReturn: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.requestReturnButton, 5000)

			.click(Selectors.OrderHistory.requestReturnButton)

			.call(cb)
		;
	}

,	reorderAllItems: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.reorderButton, 5000)

			.click(Selectors.OrderHistory.reorderButton)
			.waitForAjax()

			.call(cb)
		;
	}

, 	goToFinishCheckout: function(cb){
		var client = this;
		this
			.waitForExist(Selectors.OrderHistory.goToCheckoutMsg, 5000)
			.click(Selectors.OrderHistory.linkToCheckout)
			.call(cb)
		;
}

});