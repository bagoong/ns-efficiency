defineHelper('OrderWizard.Module.Confirmation.Expects',
{

	checkExpectedSkipLoginMessage: function(cb)
	{
		this
			.getText('.order-wizard-step-guest-message', function(err, text)
			{
				expect(text).toMatch(/Checking out as a guest.*?please login/i)
			})
			.call(cb)
		;

	}

,	checkExpectedOrderWizardPage: function(cb)
	{
		this
			.getText('.wizard-header-title', function(err, text)
			{
				expect(text).toMatch(/Checkout/i, 'Current page is not Checkout Order Wizard');
			})
			.call(cb)
		;
	}

,	checkExpectedThankYouPage: function (cb)
	{
		this
			.getText(Selectors.OrderWizardModuleConfirmation.confirmationTitle, function(err, text)
	        {
	            expect(text).toMatch(/THANK YOU FOR SHOPPING/i, 'Current page is not Thank You Page');
	        })
	        .call(cb)
		;
	}

, 	checkExpectedContinueButton: function (cb)
	{

		this
			.waitForAjax()
			.waitForExist(Selectors.OrderWizardModuleConfirmation.continueButton, 5000)
			.call(cb)
		;
	}

, 	checkExpectedDownloadPDFButton: function (cb)
	{

		this
			.waitForAjax()
			.waitForExist(Selectors.OrderWizardModuleConfirmation.downloadPDF, 5000)
			.call(cb)
		;
	}

, 	checkExpectedSummaryPresence: function (cb)
	{

		this
			.waitForAjax()
			.waitForExist(Selectors.OrderWizardModuleConfirmation.summaryBox, 5000)
			.call(cb)
		;
	}

});