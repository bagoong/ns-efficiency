defineHelper('ProductList', 
{
	isItemListed: function(itemName, cb)
	{
		this
			.waitForExist(Selectors.ProductList.buttonEdit, 10000)
			.isExisting(Selectors.ProductList.textItemName.replace('%s', itemName), cb)
	}

,	getItems: function(cb)
	{
		var client = this;
		var items = [];

		this
			.elements(Selectors.ProductList.productsrowInWishlistTable, function(err, elements) {
				elements.value.forEach(function(element) 
				{
					var element_id = element.ELEMENT;

					client
						.elementIdAttribute(element_id, 'data-id', function(err, res)
						{
							if (err) return false;

							var data_id = res.value;

							var item_found = {
								dataId: data_id
							,	dataItemId: ""
							};

							client
								.elementIdAttribute(element_id, 'data-item-id', function(err, item_id)
								{
									item_found.dataItemId = item_id.value;
								})
							;

							items.push(item_found);
						})
					;
				});
			})

			.call(function() {
				cb(null, items)
			})
		;
	}

,	getErrorList: function(cb)
	{
		var error_label;
		this
			.waitForAjax()
			.getText(Selectors.ProductList.dataValidationError, function(err, text)
			{
				error_label = text;
			})
			.call(function() {
				if (typeof error_label === 'string' || typeof error_label === 'undefined')
				{
					error_label = [error_label];
				}
				cb(null, error_label)
			})
		;
	}

,	clickItemOptions: function (data, cb)
	{
		this
			.waitForAjax()
			.waitForExist('.item-views-option-tile[data-cart-option-id=\'' + data.id + '\'] ul.item-views-option-tile-picker a[data-value= \'' + data.value + '\']')
			.click('.item-views-option-tile[data-cart-option-id=\'' + data.id + '\'] ul.item-views-option-tile-picker a[data-value= \'' + data.value + '\']')
			.call(cb)
		;
	}

});