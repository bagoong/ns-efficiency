var _ = require('underscore');

defineHelper('ProductList.SaveForLater.Expects', 
{
	checkItemIsAddedToSaveForLater: function(cb) {
		
		client = this;
		this
			.waitForAjax()
	        .ProductList.SaveForLater.isElementInSaveForLater(function(err, isInSFL){
	        	client.isInSFL = isInSFL
	        	expect(isInSFL).toBeTruthy();
	        })
        	.call(cb)
        ;
	}
,   checkItemSaveForLaterData: function(dataset, cb) {

	this
      	.ProductList.SaveForLater.getItemDataTypeById(function(err, data)
        {
        	expect(dataset.Item.Name).toEqual(data.Name);
        	expect(dataset.Item.ItemPrice).toEqual(data.ItemPrice);
        	expect(dataset.Item.Quantity).toEqual(data.Quantity);
        	expect(data.isMoveToCart).toBeTruthy();
        	expect(data.isRemove).toBeTruthy();
        })
      	.call(cb)
	}
});