defineHelper('ProductList.Expects', 
{
	isItemListed: function(itemName, cb)
	{
		this
			.ProductList.isItemListed(itemName, function(err, isListed){
        		expect(isListed).toBeTruthy();
        	})
        	.call(cb)
        ;
	}
});