var _ = require('underscore');

defineHelper('ProductList.WishList', 
{
	createNewWishList: function(params, cb)
	{
		this
			.waitForAjax()
			.click(Selectors.ProductList.buttonCreateNewList)
			.waitForAjax()
			.setValue(Selectors.ProductList.inputNewListName, params.listname)
			.setValue(Selectors.ProductList.inputNewListNotes, params.notes)
			.click(Selectors.ProductList.buttonCreateListSubmit)
			.waitForAjax()
			.call(cb)
		;
	}

,	fillWishList: function(params, cb)
	{
		this
			.waitForAjax()
			.setValue(Selectors.ProductList.inputNewListName, params.listname)
			.setValue(Selectors.ProductList.inputNewListNotes, params.notes)
			.call(cb)
		;
	}

,	clickCreateList: function (cb)
	{
		this
			.waitForAjax()
			.click(Selectors.ProductList.buttonCreateListSubmit)
			.waitForAjax()
			.call(cb)
		;
	}

,	getWishLists: function (cb)
	{
		var client = this;
		var items = [];

		client
			.elements(Selectors.ProductList.productListTableofLists, function (err, elements) {
				elements.value.forEach(function (element) 
				{
					var element_id = element.ELEMENT;
 
					client
						.elementIdAttribute(element_id, 'data-id', function (err, res)
						{
							if (err) 
								{
									return cb(err);
								}

							var data_id = res.value;

							var item_found = {
								dataId: data_id
							,	dataItemId: ""
							};

							client
								.elementIdAttribute(element_id, 'data-item-id', function (err, item_id)
								{
									item_found.dataItemId = item_id.value;
								})
							;

							items.push(item_found);
						})
					;
				});
			})
			.call(function()
			{
				cb(null, items)
			})
		;
	}

,	getWishListDataType: function (params, cb)
	{
		var client = this;

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element_xpath = params.parentElementXPath || "";

		var datatype = {
			'listname'       : ''
		,	'products'       : '0'
		,	'notes'          : ''
		};

		this
			.waitForAjax()
			
			.waitForExist(parent_element_xpath + Selectors.ProductList.listColumnDetails, 5000)

			.getText(parent_element_xpath + Selectors.ProductList.listColumnDetails + Selectors.ProductList.titleListName , function(err, text){
				datatype.listname = text;
			})


			.isExisting(parent_element_xpath + Selectors.ProductList.listColumnInfo + Selectors.ProductList.listColumnInfoWishlistDescription, function(err, exists){
				if(exists)
				{
					client
						.getText( parent_element_xpath + Selectors.ProductList.listColumnInfo + Selectors.ProductList.listColumnInfoWishlistDescriptionValue, function(err, text){
							datatype.notes = text.trim();
						})
					;
				}
			})	
			.isExisting(parent_element_xpath + Selectors.ProductList.listColumnDetails + Selectors.ProductList.listColumnDetailsItemsNumber, function(err, exists){
				if(exists)
				{
					client
						.getText( parent_element_xpath + Selectors.ProductList.listColumnDetails + Selectors.ProductList.listColumnDetailsItemsNumber, function(err, text){
							datatype.products = text;
						})
					;
				}
			})

			.call(function()
			{
				cb(null, datatype);
			})
		;
	}

, 	getWishListsDataType: function(params, cb)
	{
		var client = this;
		var wishLists = [];

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		this
			.ProductList.WishList.getWishLists(function(err, found_wishLists)
			{
				found_wishLists.forEach(function(found_wishList)
				{
					var id = found_wishList.dataId

					client
						.ProductList.WishList.getWishListDataType({parentElementXPath: Selectors.ProductList.productId.replace('%s', id)}, function(err, wishListDT)
						{
							wishLists.push(wishListDT)
						})
					;	
				})
			})
			.call(function() {
				cb(null, wishLists)
			})
		;
	}

,	editWishList: function(wishlist_to_search, cb)
	{
		var client = this;

		this
			.waitForAjax()
			.ProductList.WishList.getWishLists(function(err, found_WishLists)
			{
				found_WishLists.forEach(function(found_wishList)
				{
					var id = found_wishList.dataId
					client
						.ProductList.WishList.getWishListDataType({parentElementXPath: Selectors.ProductList.productId.replace('%s', id)}, function(err, wishListDT)
						{
							var comparable_wishList_dt = client.util.cloneData(wishListDT);

							if(_.isEqual(wishlist_to_search, comparable_wishList_dt))
							{				
								client
									.waitForVisible(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonEditList, 10000)
									.click(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonEditList)
									.waitForAjax()
								;
								return false;
							}
						})
					;	
					
				})
			})
			.call(cb)
		;
	}

,	addWishListToCart: function (wishlist_name, cb)
	{
		var client = this;
		this
			.waitForAjax()
			.ProductList.WishList.getWishLists(function (err, found_WishLists)
			{
				found_WishLists.forEach(function (found_wishList)
				{
					var id = found_wishList.dataId
					client
						.ProductList.WishList.getWishListDataType({parentElementXPath: Selectors.ProductList.productId.replace('%s', id)}, function(err, wishListDT)
						{
							var comparable_wishList_dt = client.util.cloneData(wishListDT);

							if(wishlist_name.toUpperCase() === comparable_wishList_dt.listname)
							{				
								client
									.waitForVisible(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonAddListtoCart)
									.click(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonAddListtoCart)
									.waitForAjax()
								;
								return false;
							}
						})
					;	
					
				})
			})
			.call(cb)
		;
	}

,	viewCartAndCheckout: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.buttonViewCartModalListAdded, 10000)
			.click(Selectors.ProductList.buttonViewCartModalListAdded)
			.waitForAjax()
			.call(cb)
		;
	}

,	removeAllWishLists: function(cb)
	{
		var client = this;

		this
			.waitForAjax()
			.ProductList.WishList.getWishLists(function(err, found_WishLists)
			{
				found_WishLists.forEach(function(found_wishList)
				{
					var id = found_wishList.dataId
								
					client
						.isExisting(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonEditListMoreActions, function(err, exists)
						{
							if(exists)
							{
								client
									.waitForVisible(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonEditListMoreActions, 10000)
									.click(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.buttonEditListMoreActions)
									.waitForAjax()
									.waitForVisible(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.LinkDeleteList, 10000)
									.click(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.LinkDeleteList)
									.waitForAjax()
									.waitForVisible(Selectors.ProductList.buttonRemoveListModalYes, 10000)
									.click(Selectors.ProductList.buttonRemoveListModalYes)
									.waitForAjax()
								;
							}
						})
					;
				})					
			})
			.call(cb)
		;
	}

,	isItemListed: function(itemName, cb)
	{
		this
			.waitForExist(Selectors.ProductList.buttonEdit, 10000)
			.isExisting(Selectors.ProductList.textItemName.replace('%s', itemName), cb)
	}

,	goToPDP: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productListFulName)
			.click(Selectors.ProductList.productListFulName)
			.call(cb)
		;
	}

,	selectItemFromWishList: function(cb) {

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productListMarkItemSelected)
			.click(Selectors.ProductList.productListMarkItemSelected)
			.call(cb)
		;
	}

,	removeSelectedItemFromWishList: function(cb) {
		this
			.click(Selectors.ProductList.buttonExpander)
			.waitForExist(Selectors.ProductList.dropDownOptionExpander)
			.click(Selectors.ProductList.dropDownOptionExpander)
			.waitForAjax()
			.call(cb)
		;
	}

,	moveItemToWishlist: function(wishlist,cb) {
		this
			.waitForExist(Selectors.ProductList.buttonMovetoWishlist)
			.click(Selectors.ProductList.buttonMovetoWishlist)
			.waitForExist(Selectors.ProductList.linkSelectedWishlist.replace('%s', wishlist))
			.click(Selectors.ProductList.linkSelectedWishlist.replace('%s', wishlist))
			.waitForAjax()
			.call(cb)
		;
	}

});