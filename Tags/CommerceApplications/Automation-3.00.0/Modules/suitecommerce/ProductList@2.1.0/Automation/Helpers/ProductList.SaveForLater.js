defineHelper('ProductList.SaveForLater', 
{
	isElementInSaveForLater: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId), 10000)
			.isExisting(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId), cb)
	}

,	getItemDataTypeById: function(cb)
	{
		var prefix = Selectors.ProductList.productSaveforLater.replace('%s', this.itemId)
		var dataType = {
			'Name': ''
		,	'ItemPrice': ''
		,	'Quantity': ''
		,	'isMoveToCart': false
		,	'isRemove': false
		}

		this
			.waitForExist(prefix, 10000)
			.getText(prefix + Selectors.ProductList.productNameLink, function(err, text){
				dataType.Name = text;
			})
			.getText(prefix + Selectors.ProductList.productFinalPrice, function(err, text){
				dataType.ItemPrice = text;
			})
			.getValue(prefix + Selectors.ProductList.productQuantity, function(err, text){
				dataType.Quantity = text;
			})
			.isExisting(prefix + Selectors.ProductList.buttonMovetoCart, function(err, isVisible){
				dataType.isMoveToCart = isVisible;
			})
			.isExisting(prefix + Selectors.ProductList.buttonRemoveProduct, function(err, isVisible){
				dataType.isRemove = isVisible;
			})
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}

,	removeItem: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId) + Selectors.ProductList.buttonRemoveProduct, 5000)
			.click(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId) + Selectors.ProductList.buttonRemoveProduct)
			.waitForAjax()
			.waitForExist(Selectors.ProductList.buttonModalRemoveYes, 5000)
			.click(Selectors.ProductList.buttonModalRemoveYes)
			
			.call(cb)
	}

,	moveSFLItemToCart: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId) + Selectors.ProductList.buttonMovetoCart, 5000)
			.click(Selectors.ProductList.productSaveforLater.replace('%s', this.itemId) + Selectors.ProductList.buttonMovetoCart)
		
			.call(cb)
	}

});