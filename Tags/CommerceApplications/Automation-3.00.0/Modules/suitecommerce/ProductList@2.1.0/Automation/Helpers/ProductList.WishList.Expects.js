var _ = require('underscore');

defineHelper('ProductList.WishList.Expects', 
{
	checkWishListHeader: function(cb)
	{
		var client = this;

		this
			.isExisting(Selectors.ProductList.titleProductList, function(err, is_Existing)
			{
				expect(is_Existing).toBeTruthy();
			})
			.isExisting(Selectors.ProductList.noItemsContainer, function(err, is_Existing)
			{
				if(is_Existing)
				{
					client
						.waitForAjax()
						.getText(Selectors.ProductList.noItemsMessage, function(err, text)
						{
							expect(text.trim()).toEqual("You don't have items in this list yet. Explore the store or search for an item you would like to add.");
							return false;
						})
					;
				}
			})
			.ProductList.getItems(function(err, item_list)
			{
				if(item_list.length === 1)
				{
					client
						.isExisting(Selectors.ProductList.buttonBulkAddtoCart, function(err, is_Existing)
						{
							expect(is_Existing).toBeTruthy();
						})
					;
				}
				else if(item_list.length >= 2)
				{
					client
						.isExisting(Selectors.ProductList.buttonBulkAddtoCart, function(err, is_Existing)
						{
							expect(is_Existing).toBeTruthy();
						})
						.isExisting(Selectors.ProductList.labelSortBy, function(err, is_Existing)
						{
							expect(is_Existing).toBeTruthy();
						})
					;
				}
			})
			.call(cb)
		;
	}

,	checkWishListListed: function(expected_wishList, cb)
	{
		var status = false;
		this
			.waitForAjax()
			.ProductList.WishList.getWishListsDataType(function(err, found_WishLists) {
				found_WishLists.forEach(function(found_WishList){
					if(_.isEqual(found_WishList, expected_wishList))
					{
						status = true;
						return false;
					}	
				})
				expect(status).toBeTruthy();
			})

			.call(cb)
		;
	}

,	checkErrorListContains: function(expected_error, cb)
	{
		var status = false;
		this
			.ProductList.getErrorList(function(err, errorList)
			{
				errorList.forEach(function(found_error)
				{
					if(_.isEqual(found_error, expected_error))
					{
						status = true;
						return false
					}
				})
				expect(status).toBeTruthy();
			})
			.call(cb)
		;
	}

,	checkSingleListPresent: function(cb)
	{
		this
			.ProductList.WishList.getWishListsDataType(function(err, found_WishLists) {
				expect(found_WishLists.length).toEqual(1);
			})
			.call(cb)
		;
	}

,	checkMinimunQuantityMessageInMyWishList: function(wishlist_to_search, cb)
	{
		var client = this;

		this
			.waitForAjax()
			.ProductList.WishList.getWishLists(function(err, found_WishLists)
			{
				found_WishLists.forEach(function(found_wishList)
				{
					var id = found_wishList.dataId
					client
						.ProductList.WishList.getWishListDataType({parentElementXPath: Selectors.ProductList.productId.replace('%s', id)}, function(err, wishListDT)
						{
							var comparable_wishList_dt = client.util.cloneData(wishListDT);

							if(_.isEqual(wishlist_to_search, comparable_wishList_dt))
							{			
								client
									.isExisting(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.productMinQuantity, function(err, exists)
									{
										expect(exists).toBeTruthy();
										if(exists)
										{
											client
												.getText(Selectors.ProductList.productId.replace('%s', id) + Selectors.ProductList.productMinQuantity, function(err, text)
												{
													console.log(text.trim())
													expect(text.trim()).toEqual('The quantity of some of the items needs to be updated to match the minimum required to purchase. Go to ' + comparable_wishList_dt.listname)
												})
											;
										}
									})
								;
								return false;
							}
						})
					;	
					
				})
			})
			.call(cb)
		;
	}

,	checkMinimunQuantityMessageInItem: function(item_to_search, cb)
	{
		var client = this
		this
			.waitForAjax()
			.isExisting(Selectors.ProductList.searchItemId.replace('%s', item_to_search.id), function(err, exists)
			{
				expect(exists).toBeTruthy();
				if(exists)
				{
					client
						.getText(Selectors.ProductList.searchItemId.replace('%s', item_to_search.id) + Selectors.ProductList.labelMinQuantity, function(err, text)
						{
							expect(text.trim()).toEqual('The minimum quantity to purchase this item is 6. Do you want to change it from 1 to 6? Yes, update it')
						})
					;
				}
			})
			.call(cb)
		;
	}

,	checkItemIsAddedToWishList: function(itemName, cb) {
		
		this
			.waitForAjax()
			.ProductList.WishList.isItemListed(itemName, function(err, isListed){
        		expect(isListed).toBeTruthy();
        	})
        	.call(cb)
        ;
	}

,	checkAddButtonDisable: function(cb) {

		this
			
			.waitForAjax()

			.waitForExist(Selectors.ProductList.buttonAddToWishList, 5000)

			.getAttribute(Selectors.ProductList.buttonAddToWishList, 'disabled').then(function(prop) {
				expect(prop).toEqual('true');
			})

			.call(cb)
		;
		
	}

,	checkEmptyWishList: function(cb) {
		this
			.waitForExist(Selectors.ProductList.EmptyWishList, 5000)
			
			.getText(Selectors.ProductList.EmptyWishList, function(err, text){
				expect(text).toEqual("You don't have items in this list yet. Explore the store or search for an item you would like to add.");
			})
			
			.call(cb)
		;
	}
	//param order === 1 -> ASC
,	checkOrderByPrice: function(order, cb) {
		var client = this
		,	previous = undefined
		,	elementValue;
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productPrice, 5000)
			
			.getAttribute(Selectors.ProductList.productPrice, 'data-rate').then(function(attr) {
		      	attr.forEach(function (element) {
		      		
		      		if (previous !== undefined)
		      		{
		      			elementValue = parseInt(element);
		      			order === 1 ? expect(previous < elementValue).toBeTruthy() : expect(previous > elementValue).toBeTruthy();
		      		}
		      		
		      		previous = parseInt(element);
				})
		    })

		   .call(cb)
		;
	}

,	checkOrderByName: function(order, cb) {
		var client = this
		,	previous = undefined;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productName, 5000)
			
			.getText(Selectors.ProductList.productName).then(function(attr) {

		      	attr.forEach(function (element) {

		      		if (previous !== undefined)
		      		{
	
		      			if (order === 1)
		      			{
		      				expect((previous.localeCompare(element) < 0) || (previous.localeCompare(element) === 0)).toBeTruthy();
		      			}else 
		      			{
							expect((previous.localeCompare(element) > 0) || (previous.localeCompare(element) === 0)).toBeTruthy(); 
		      			}

		      		}

		      		previous = element;
				})
		    })

		   .call(cb)
		;
	}

,	checkOrderByDate: function(order, cb) {
		var client = this
		,	previous = undefined
		,	aux
		,	elementValue
		, 	previous
		,	previous_ts
		,	elementValue_ts;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductList.productDateAdded, 5000)
			
			.getText(Selectors.ProductList.productDateAdded).then(function(attr) {
			 	attr.forEach(function (element) {
			 		
			 		aux = element.split(":");

			 		if (previous !== undefined)
		      		{
						elementValue = new Date(aux[1]);
						elementValue_ts = elementValue.getTime();

						if (elementValue_ts !== previous_ts)
						{
							order === 1 ? expect(previous).toBeBefore(elementValue) : expect(previous).toBeAfter(elementValue);
						}

					}

					previous = new Date(aux[1]);
					previous_ts = previous.getTime();
				})


			})
		   .call(cb)
		;

	}

,	checkOutOfStock: function(message,cb) {
		this
			.getText(Selectors.ProductList.OutOfStock).then(function(text) {
				expect(text.trim()).toEqual(message);
			})
			.call(cb)
		;
	}

});