
defineHelper('OrderWizard.Module.PaymentMethod', 
{
	choosePaymentMethod: function(params, cb)
	{
		var client = this;
		var paymethod_selector = 'select[data-action=select-payment-method]';

		this
			.call(function()
			{
				if (!params) return false;

				if (params.index)
				{
					client.selectByIndex(paymethod_selector, params.index)
				}
				else if (params.text)
				{
					client.selectByVisibleText(paymethod_selector, params.text)
				}
				else if (params.value)
				{
					client.selectByValue(paymethod_selector, params.value)
				}
			})
		
			.waitForAjax()

			.call(cb)
		;
	}

,	fillPurchaseOrderNumber: function(order_number, cb)
	{
		this
			.setValue('.order-wizard-paymentmethod-invoice-module-purchase-order-value', order_number)
			.call(cb)
		;
	}	
	
,	clickEditSelectedCreditCard: function(cb)
	{
		this.click(Selectors.OrderWizardModulePaymentMethod.editCreditCardButton, cb);
	}

});

