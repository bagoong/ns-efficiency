defineHelper('Merchandising',
{
	isMerchandisingPresent: function(cb)
	{
		this
			.waitForExist(Selectors.Merchandising.merchandisingZone, 5000)
			.getText(Selectors.Merchandising.merchandisingZone, function(err, text)
			{
				expect(text).toMatch('No merchandising zone were found');
			})
			.call(cb)
		;
	}
}
