defineHelper('Balance', 
{

	clickPrintAStatement: function (cb)
	{
		this
			.waitForExist(Selectors.Balance.printAStatement, 5000)
			.click(Selectors.Balance.printAStatement)
			.waitForAjax()
			.call(cb)
		;
	}			

});	