defineHelper('Balance.Expects', 
{

	expectsNoPaymentDue: function (cb)
	{
		this

			.waitForExist(Selectors.Balance.continueButton, 5000)

			.getText(Selectors.Balance.continueButton, function(err, text){
				expect(text).toMatch('No Payment Due');
			})

			.getAttribute(Selectors.Balance.continueButton, 'disabled', function(err, text){
				expect(text).toBeTruthy();
			})

			.call(cb)
		;
	}	

,	expectsMakeAPayment: function (cb)
	{
		this

			.waitForExist(Selectors.Balance.continueButton, 5000)

			.getText(Selectors.Balance.continueButton, function(err, text){
				expect(text).toMatch('Continue to Payment');
			})

			.getAttribute(Selectors.Balance.continueButton, 'disabled', function(err, text){
				expect(text).toBeFalsy();
			})

			.call(cb)
		;
	}		

,	expectsPrintAStatementTitle: function (cb)
	{
		this
			.waitForExist(Selectors.Balance.printTitle, 5000)
			.getText(Selectors.Balance.printTitle, function(err, text){
				expect(text).toMatch('PRINT A STATEMENT');
			})
			.call(cb)
		;
	}	

,	checkAccountBalancePageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Balance.accountBalancePageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Account Balance page is not present.');
        	})
        	.call(cb)
        ;
	}	

});	