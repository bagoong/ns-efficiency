var _ = require('underscore');
var async = require('async');

defineHelper('Address', 
{

	getAddressDataType: function (params, cb)
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element_xpath = params.parentElementXPath || "";

		var datatype = {
			'fullname'       : ''
		,	'company'        : ''
		,	'addr1'          : ''
		,	'addr2'          : ''
		,	'city'           : ''
		,	'country'        : ''
		,	'state'          : ''
		,	'zip'            : ''
		,	'phone'          : ''
		,	'isResidential'  : false
		,	'defaultShipping': false
		,	'defaultBilling' : false
		};

		this
			.waitForAjax()
			
			.waitForExist(parent_element_xpath + Selectors.Address.addressDetail, 5000)

			.getText(parent_element_xpath + Selectors.Address.addressCompany, function(err, text){
				datatype.company = text;
			})

			.getText(parent_element_xpath + Selectors.Address.addressFullName, function(err, text){
				if (err && datatype.company) {
					datatype.fullname = datatype.company;
					datatype.company = '';
				} else {
					datatype.fullname = text;
				}
			})

			.getText( parent_element_xpath + Selectors.Address.addressAddr1, function(err, text){
				datatype.addr1 = text;
			})

			.getText( parent_element_xpath + Selectors.Address.addressAddr2, function(err, text){
				datatype.addr2 = text || '';
			})			

			.getText( parent_element_xpath + Selectors.Address.addressCity, function(err, text){
				datatype.city = text;
			})

			.getText(parent_element_xpath + Selectors.Address.addressState, function(err, text){
				datatype.state = text;
			})

			.getText(parent_element_xpath + Selectors.Address.addressCountry, function(err, text){
				datatype.country = text;
			})

			.getText(parent_element_xpath + Selectors.Address.addressZip, function(err, text){
				datatype.zip = text;
			})

			.getText(parent_element_xpath + Selectors.Address.addressPhone, function(err, text){
				datatype.phone = text;
			})

			.isExisting(parent_element_xpath + Selectors.Address.defaultShipping, function(err, exists){
				datatype.defaultShipping = exists;
			})

			.isExisting(parent_element_xpath + Selectors.Address.defaultBilling, function(err, exists){
				datatype.defaultBilling = exists;
			})

			.call(function()
			{
				cb(null, datatype);
			})
		;

	}



,	setSameShippingAddress: function (new_status, cb) 
	{
		var client = this;

		this
			.waitFor(Selectors.Address.sameAs, 10000)

			.isSelected(Selectors.Address.sameAs, function(err, selected) 
			{
				if (typeof new_status === 'boolean' && new_status !== selected) 
				{
					client
						.scroll(Selectors.Address.sameAs)
						.click(Selectors.Address.sameAs)
					;
				}
			})

			.call(cb)
		;
	}

,	removeSameShippingAddress: function(cb) 
	{
		this.Address.setSameShippingAddress(false).call(cb);
	}


,	markSameShippingAddress: function(cb)
	{
		this.Address.setSameShippingAddress(true).call(cb);
	}


,	fillAddress: function (params, cb) 
	{	
		var client = this;

		var parent_element = params.parentElementXPath || "";

		parent_element += Selectors.Address.editForm;

		this
			.waitForExist( parent_element + Selectors.Address.editFullName, 10000)	
			// Necesary pause because waitForExist is not working here
			.pause(2000)
			.setValue( parent_element + Selectors.Address.editFullName, params.fullname)
			.setValue( parent_element + Selectors.Address.editCompany, params.company || '')			
			.setValue( parent_element + Selectors.Address.editAddr1, params.addr1)
			.setValue( parent_element + Selectors.Address.editAddr2, params.addr2 || '')

			.call(function() {
				if (params.country != '')
				{
					client
						.selectByVisibleText( parent_element + Selectors.Address.editCoutry, params.country);
				}
			})

			.getTagName( parent_element + Selectors.Address.editStateSelect, function(err, tagname)
			{
				if (tagname === 'select' && params.state) {
					client.selectByVisibleText( parent_element + Selectors.Address.editStateSelect, params.state);
				} else {
					client.setValue( parent_element + Selectors.Address.editState, params.state || '');
				}
			})
			
			.setValue( parent_element + Selectors.Address.editCity, params.city)
			
			.setValue( parent_element + Selectors.Address.editZip, params.zip)

			.setValue( parent_element + Selectors.Address.editPhone, params.phone)

			.call(function()
			{
				if(params.defaultShipping)
				{
					client
						.Address.clickMakeThisMyDefaultShippingAddress()
					;
				}

				if(params.defaultBilling)
				{
					client
						.Address.clickMakeThisMyDefaultBillingAddress()
					;
				}
			})

			.call(cb)
		;
	}


,	clickChangeAddress: function(params, cb)
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element = params.parentElementXPath || "";
		var change_address_xpath = parent_element + Selectors.Address.changeAddress;

		this
			.waitForExist(change_address_xpath, 5000)
			.scroll(change_address_xpath)
			.click(change_address_xpath)
			.call(cb)
		;
	}


,	clickAddNewAddressAddressBook: function(cb)
	{
		this
			.waitForExist(Selectors.Address.addNewAddress, 5000)
			.click(Selectors.Address.addNewAddress)
			.call(cb)
		;

	}	

,	clickMakeThisMyDefaultShippingAddress: function(cb)
	{
		this
			.click(Selectors.Address.makeDefaultShipping)
			.waitForAjax()
			.call(cb)
		;

	}

,	clickMakeThisMyDefaultBillingAddress: function(cb)
	{
		this
			.click(Selectors.Address.makeDefaultBilling)
			.waitForAjax()
			.call(cb)
		;
	}	


,	clickSaveAddress: function(cb) 
	{
		this
			.click(Selectors.Address.saveAddress)
			.waitForAjax()
			.call(cb)
		;
	}


,	getBillingAddresses: function(cb)
	{
		var params = {parentElementXPath: Selectors.Address.billingParent};
		this.Address.getAddresses(params, cb);
	}


,	getShippingAddresses: function(cb)
	{
		var params = {parentElementXPath: Selectors.Address.shippingParent};
		this.Address.getAddresses(params, cb);
	}


, 	getAddressesDataType: function(params, cb)
	{
		var client = this;
		var addresses = [];

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		this
			.Address.getAddresses(function(err, found_addresses)
			{
				found_addresses.forEach(function(found_address)
				{
					var id = found_address.dataId

					client
						.Address.getAddressDataType({parentElementXPath: Selectors.Address.dataParent.replace('%s', id)}, function(err, addressDT)
						{
							addresses.push(addressDT)
						})
					;	
				})
			})
			.call(function() {
				cb(null, addresses)
			})
		;
	}

,	getAddresses: function(params, cb)
	{
		var client = this;
		var addresses = [];

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element = params.parentElementXPath || "";
		
		this
			.elements(parent_element + Selectors.Address.detailContainer, function(err, elements) {
				elements.value.forEach(function(element) 
				{
					var element_id = element.ELEMENT;

					client
						.elementIdAttribute(element_id, 'data-id', function(err, res)
						{
							if (err) return false;

							var data_id = res.value;

							var address_found = {
								dataId: data_id
							,	company: ""
							};

							client
								// IMPORTANT: we use getHtml INSTEAD OF getText because getText only works
								// for elements visible on screen and scrolling seems to work properly only with form items.

								.getHTML(parent_element + Selectors.Address.getAddressCompany.replace('%s', data_id), false, function(err, html)
								{
									if (Array.isArray(html)) {
										html = html[0];
									}

									var company = client.util.stripTags(html);
									address_found.company = company;
								})
							;

							addresses.push(address_found);
						})
					;
				});
			})

			.call(function() {
				cb(null, addresses)
			})
		;

	}

,	removePreviousAddresses: function (params, cb) 
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var client = this;

		client
			.Address.getAddresses(params, function(err, addresses)
			{
				addresses.forEach(function(address_data) 
				{
					var no_filter = !params.fullname && !params.company;
					var title_filter = params && (params.fullname === address_data.company || params.company === address_data.company);

					if (no_filter || title_filter)
					{
						client
							.Address.clickRemoveAddressUsingDataId(
								address_data.dataId
							)
							.pause(500)
						;
					}
				});

				client.call(cb);
			})
		;

	}

,	clickRemoveAddressUsingDataId: function(address_data_id, cb) 
	{
		var client = this;
		
		
		var remove_selector = Selectors.Address.remove.replace('%s', address_data_id);
			

		client.isExisting(remove_selector, function(err, is_existing)
		{
			if (!is_existing) return client.call(cb);

			client
				.scroll(remove_selector)

				// USING GET ATTRIBUTE BECAUSE IS ENABLED IS NOT WORKING PROPERLY IN WEBDRIVERIO
				.getAttribute(remove_selector, 'disabled', function(err, is_disabled)
				{
					if (is_disabled) return client.call(cb);

					client
						.click(remove_selector)
						.GlobalViews.Modal.clickYesOnModal()
						.call(cb)
					;
				})
			;
		});
	}


,	clickSelectThisAddress: function(params, cb)
	{
		var client = this;

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element_xpath = params.parentElementXPath || "";

		client
			.Address.getAddresses(params, function(err, addresses)
			{				
				var address_found = false;

				addresses.forEach(function(address)
				{
					if (address_found) return false;

					address_found = params.fullname === address.company || params.company === address.company;

					if (address_found)
					{
						var select_selector = parent_element_xpath + Selectors.Address.select.replace('%s', address.dataId);

						client
							.waitForExist(select_selector, 4000)
							.scroll(select_selector)
							.click(select_selector)
							.call(cb)
						;
					}
				});

				if (!address_found) {
					expectAndEnd("Address").toBe("found.", "\n" + JSON.stringify(params, null, 4) + "\n");
				}
			})
		;
	}


,	clickShipToThisAddress: function(params, cb) {
		params = this.util.cloneData(params);
		params.parentElementXPath = Selectors.Address.shipTo;
		this.Address.clickSelectThisAddress(params, cb);
	}


,	verifyShippingAddressNotDefault: function(cb)
	{
		this
			.isExisting(Selectors.Address.defaultshippingText, function(err, text){
				//console.log(text);
				expect(text).toBe(false);
			})
			.call(cb)
	}

,	verifyShippingAddressDefault: function(cb)
	{
		this
			.isExisting(Selectors.Address.defaultshippingText, function(err, text){
				//console.log(text);
				expect(text).toBe(true);
			})
			.call(cb)
	}

,	clickEditAddress: function(address_to_search, cb)
	{
		var client = this;

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var extra_fields = ['isResidential', 'defaultBilling', 'defaultShipping'];
		
		var has_extra_fields = _.every(extra_fields, function(field) {
			return field in address_to_search;
		});

		this
			.waitForAjax()
			.Address.getAddresses(function(err, found_addresses)
			{
				found_addresses.forEach(function(found_address)
				{
					var id = found_address.dataId
					client
						.Address.getAddressDataType({parentElementXPath: Selectors.Address.editAddress.replace('%s', id)}, function(err, addressDT)
						{
							var comparable_address_dt = client.util.cloneData(addressDT);

							if (!has_extra_fields)
							{
								extra_fields.forEach(function(field)
								{
									delete comparable_address_dt[field]
								});
							}
							if(_.isEqual(address_to_search, comparable_address_dt))
							{				
								client
									.waitForExist(Selectors.Address.editDetail.replace('%s', id), 10000)
									.click(Selectors.Address.editDetail.replace('%s', id))
									.waitForAjax()
								;
								return false;
							}
						})
					;	
					
				})
			})
			.call(cb)
		;
	}	

,	getErrorList: function(cb)
	{
		var client = this;
		var error_label = '';
		this
			.waitForAjax()
			.isExisting(Selectors.Address.errorList, function(err, is_existing)
			{
				client	
					.getText(Selectors.Address.errorList, function(err, text)
					{
						error_label = text;
					})
				;
			})
			.call(function() {
				if (typeof error_label === 'string' || typeof error_label === 'undefined')
				{
					error_label = [error_label];
				}

				cb(null, error_label)
			})
		;
	}

,	isCompanyRequired: function(cb)
	{
		this
			.isExisting(Selectors.Address.companyRequired, function(err, is_existing)
			{
				cb(null, !is_existing)
			})
	}

});