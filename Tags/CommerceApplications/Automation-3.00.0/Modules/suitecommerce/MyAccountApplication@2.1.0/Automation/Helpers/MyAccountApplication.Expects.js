defineHelper('MyAccountApplication.Expects', 
{

	loadingIndicatorPresence: function (cb)
	{
		this
			.isExisting(Selectors.MyAccountApplication.imgLoadidingIndicator).then(function (isExisting)
			{
				expect(isExisting).toBeTrue();
			})
			.call(cb)
		;
	}

});