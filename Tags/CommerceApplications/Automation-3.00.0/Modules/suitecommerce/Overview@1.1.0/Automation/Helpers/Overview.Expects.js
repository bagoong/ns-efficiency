defineHelper('Overview.Expects', 
{
	isOverviewPage: function(cb)
	{
		this
			.waitForAjax()

			.isVisible(Selectors.Overview.homeMySettings).then(function(err, is_visible)
			{
				expect(is_visible).toBeTruthy()
			})
			
			.call(cb)
		;
	}

,	verifyPurchaseIsDisplayedInRecentPurchases: function (purchases,cb)
	{
		var client = this;
		purchases.forEach(function (purchase)
		{
			client
				.Overview.RecentPurchases.getPurchaseDetailsById(purchase.purchaseId).then(function (found_order)
				{
	            	expect(found_order).toHaveEqualFields(purchase);
	        	})
	        ;
		});
		
		this
			.call(cb)
		;
	}

,	verifyPurchaseIsDisplayedInRecentPurchasesWithoutSCISBundle: function (purchases,cb)
	{
		var client = this;
		purchases.forEach(function (purchase)
		{
			client
				.Overview.RecentPurchases.getPurchaseDetailsWithoutSCISBundleById(purchase.purchaseId).then(function (found_order)
				{
	            	expect(found_order).toHaveEqualFields(purchase);
	        	})
	        ;
		});
		
		this
			.call(cb)
		;
	}

,	verifyAllRecentPurchasesIsEmpty: function(cb)
	{
		this
			.waitForExist(Selectors.Overview.emptyRecentOrders, 5000)
			.getText(Selectors.Overview.emptyRecentOrders).then(function (text)
			{
				expect(text).toBe('You don\'t have any purchases in your account right now.');
			})
			.call(cb)
		;
	}
});