defineHelper('Overview.RecentPurchases', 
{
	getPurchaseDetailsById: function(params, cb)
	{
		var client = this;
		var parent_element = Selectors.Overview.order.replace('%s', params) + ' ';
		var purchase = {
			'purchaseId'     : params
		,	'purchaseNo'     : ''
		,	'date'           : ''
		,	'amount'         : ''
		,	'origin'         : ''
		,	'trackItems'     : ''
		};

		this
			.waitForAjax()
			.waitForExist(parent_element, 5000)

			.getText(parent_element + Selectors.Overview.purchaseNo).then(function (text){
				purchase.purchaseNo = text;
			})

			.getText(parent_element + Selectors.Overview.purchaseDate).then(function (text){
				purchase.date = text;
			})

			.getText(parent_element + Selectors.Overview.purchaseAmount).then(function (text){
				purchase.amount = text;
			})

	    	.isExisting(parent_element + Selectors.Overview.purchaseOrigin).then(function (exists){
				if(exists)
				{
					client
						.getText(parent_element + Selectors.Overview.purchaseOrigin).then(function (text){
							purchase.origin = text;
						})
					;
				}
			})

			.Overview.RecentPurchases.getTrakingValue(params, function (err, values){
				purchase.trackItems = values;
			})

	    	.waitForAjax()  

			.call(function()
			{
				cb(null, purchase);
			})
		;	
	}
,
	getPurchaseDetailsWithoutSCISBundleById: function(params, cb)
	{
		var client = this;
		var parent_element = Selectors.Overview.order.replace('%s', params) + ' ';
		var purchase = {
			'purchaseId'     : params
		,	'purchaseNo'     : ''
		,	'date'           : ''
		,	'amount'         : ''
		,	'status'         : ''
		,	'trackItems'     : ''
		};

		this
			.waitForAjax()
			.waitForExist(parent_element, 5000)

			.getText(parent_element + Selectors.Overview.purchaseNo).then(function (text){
				purchase.purchaseNo = text;
			})

			.getText(parent_element + Selectors.Overview.purchaseDate).then(function (text){
				purchase.date = text;
			})

			.getText(parent_element + Selectors.Overview.purchaseAmount).then(function (text){
				purchase.amount = text;
			})

			.getText(parent_element + Selectors.Overview.purchaseStatus).then(function (text){
				purchase.status = text;
			})

			.Overview.RecentPurchases.getTrakingValue(params, function (err, values){
				purchase.trackItems = values;
			})

	    	.waitForAjax()  

			.call(function()
			{
				cb(null, purchase);
			})
		;	
	}


,	getTrakingValue: function(orderId, cb)
	{
		var parent_element = Selectors.Overview.order.replace('%s', orderId) + ' ';
		var trackingNumber = '';
		var client = this;
		this
			.isExisting(parent_element + Selectors.Overview.trackingNumberEmpty).then(function (exists){
				if(exists)
				{
					client
						.getText(parent_element + Selectors.Overview.trackingNumberEmpty).then(function (text){
							trackingNumber = text;
						})
						.call(function()
						{
							cb(null, trackingNumber);
						})
					;
				}
				else
				{
					client
						.isExisting(parent_element + Selectors.Overview.trackingNumberListButton).then(function (exists){
							if(exists)
							{
								client
									.click(parent_element + Selectors.Overview.trackingNumberListButton)
								;
							}
							client
								.getText(parent_element + Selectors.Overview.trackingNumber).then(function (text){
									trackingNumber = text;
								})
								.call(function()
								{
									cb(null, trackingNumber);
								})
							;	
						})	
					;
				}
			})
		;
	}
});