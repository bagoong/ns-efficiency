defineHelper('Overview.Shipping', 
{

	verifyDefaultShippingOverview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Overview.shippingCard, 5000)

			.getText(Selectors.Overview.shippingCard, function(err, text){
				expect(text).not.toMatch('We have no default address on file for this account.');
			})

			.call(cb)
		;
	}
		
,	verifyNotDefaultShippingOverview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Overview.shippingCard, 5000)

			.getText(Selectors.Overview.shippingCard, function(err, text){
				expect(text).toMatch('We have no default address on file for this account.');
			})

			.call(cb)
		;
	}	


});