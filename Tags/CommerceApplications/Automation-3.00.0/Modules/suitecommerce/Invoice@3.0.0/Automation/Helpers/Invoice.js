defineHelper('Invoice', 
{

	opensPaidInFull: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.openPaidInFull, 5000)

	    	.click(Selectors.Invoice.openPaidInFull)
	    	.waitForAjax()  

			.call(cb)
		;
	}

	//	PAID
,	enterOrder: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.enterOrder.replace('%s', params.invoiceId), 5000)

	    	.click(Selectors.Invoice.enterOrder.replace('%s', params.invoiceId))
	    	.waitForAjax()  

			.call(cb)
		;	
	}	

	//	OPEN
,	openInvoiceClick: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.openInvoiceClick.replace('%s', params.invoiceId), 5000)

	    	.click(Selectors.Invoice.openInvoiceClick.replace('%s', params.invoiceId))
	    	.waitForAjax()  

			.call(cb)
		;
	}	

,	enterOpenInvoiceByName: function(params, cb)
	{
		this
			.waitForExist(Selectors.Invoice.invoiceByName.replace('%s', params.invoiceName), 5000)
			.click(Selectors.Invoice.invoiceByName.replace('%s', params.invoiceName))
			.waitForAjax()
			.call(cb)
		;
	}	

,	clickRequestReturn: function(cb)
	{
		this
			.waitForAjax()	
			.waitForExist(Selectors.Invoice.requestReturn, 5000)

			.click(Selectors.Invoice.requestReturn)

			.call(cb)
		;
	}		

,	clickMakeAPayment: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.makeAPaymentButton, 5000)
			.click(Selectors.Invoice.makeAPaymentButton)
			.call(cb)
		;
	}	

,	openBillingAccordeon: function(cb)
	{
		var client = this;
		this
			.waitForAjax() 
			.isExisting(Selectors.Invoice.collapsedBilling, function(err, isExisting) {
		        if(isExisting)
		        {	        	
		        	client
						.click(Selectors.Invoice.collapsedBilling)
						.waitForAjax() 
					;
		        }
		    })

			.call(cb)
		;
	}			

,	getInvoiceDataTypeFromOverview: function(params, cb)
	{
		var invoiceDT = 
		{
			'invoiceNo'      : ''
		,	'invoiceDate'    : ''
		,	'invoiceDueDate' : ''
		,	'invoiceAmount'  : ''
		,	'invoiceStatus'  : ''
		}

		this
			.waitForExist(Selectors.Invoice.paidInvoiceNo.replace('%s', params.invoiceId), 5000)
			.getText(Selectors.Invoice.openInvoiceNo.replace('%s', params.invoiceId), function(err, text)
			{
				invoiceDT.invoiceNo = '#' + text;
			})

			.getText(Selectors.Invoice.openInvoiceDate.replace('%s', params.invoiceId), function(err, text)
			{
				invoiceDT.invoiceDate = text;
			})

			.getText(Selectors.Invoice.openInvoiceDueDate.replace('%s', params.invoiceId), function(err, text)
			{
				invoiceDT.invoiceDueDate = text;
			})

			.getText(Selectors.Invoice.openInvoiceAmount.replace('%s', params.invoiceId), function(err, text)
			{
				invoiceDT.invoiceAmount = text;
			})
			.getText(Selectors.Invoice.invoiceOverviewStatus, function(err, text)
			{
				invoiceDT.invoiceStatus = text;
			})
			.call(function()
			{
				cb(null, invoiceDT);
			})
		;
	}

,	getInvoiceDataTypeFromDetails: function(cb)
	{
		var invoiceDT = 
		{
			'invoiceNo'            : ''
		,	'invoiceDate'          : ''
		,	'invoiceOrder'         : ''
		,	'invoiceDueDate'       : ''
		,	'invoiceAmount'        : ''
		,	'invoiceStatus'        : ''
		,	'invoiceTerm'          : ''
		,	'invoiceMemo'          : ''
		,	'invoiceItemsPrice'    : ''
		,	'invoiceTax'           : ''
		,	'invoiceShippingPrice' : ''
		,	'invoiceHandlingPrice' : ''
		}

		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.invoiceNo, 5000)
			.getText(Selectors.Invoice.invoiceNo, function(err, text)
			{
				invoiceDT.invoiceNo = text;
			})
			.getText(Selectors.Invoice.invoiceDate, function(err, text){
				invoiceDT.invoiceDate = text;
			})
			.getText(Selectors.Invoice.invoiceOrder, function(err, text){
				invoiceDT.invoiceOrder = text;
			})
			.getText(Selectors.Invoice.invoiceDueDate, function(err, text){
				invoiceDT.invoiceDueDate = text;
			})
			.getText(Selectors.Invoice.invoiceTerm, function(err, text){
				invoiceDT.invoiceTerm = text;
			})
			.getText(Selectors.Invoice.invoiceMemo, function(err, text){
				invoiceDT.invoiceMemo = text;
			})
			.getText(Selectors.Invoice.invoiceItems, function(err, text){
				invoiceDT.invoiceItemsPrice = text.trim();
			})
			.getText(Selectors.Invoice.invoiceTax, function(err, text){
				invoiceDT.invoiceTax = text.trim();
			})
			.getText(Selectors.Invoice.invoiceShipping, function(err, text){
				invoiceDT.invoiceShippingPrice = text.trim();
			})
			.getText(Selectors.Invoice.invoiceHandling, function(err, text){
				invoiceDT.invoiceHandlingPrice = text.trim();
			})
			.getText(Selectors.Invoice.invoiceAmountDue, function(err, text){
				invoiceDT.invoiceAmount = text;
			})
			.getText(Selectors.Invoice.invoiceStatus, function(err, text){
				invoiceDT.invoiceStatus = text;
			})
			.call(function()
			{
				cb(null, invoiceDT);
			})		
		;
	}

,	changePaidInFullDate: function(date, cb) {
		this
		.setValue(Selectors.Invoice.invoicePaidInFullDateFrom, date.from)
		.setValue(Selectors.Invoice.invoicePaidInFullDateTo, date.to)
	
		.click('.invoice-paid-list-head-invoicenumber')
		
        .waitForExist(Selectors.ListHeader.startCalendar, 5000)
        .waitForExist(Selectors.ListHeader.endCalendar, 5000)
		.call(cb);
	}
});