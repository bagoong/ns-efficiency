defineHelper('ItemDetails.SB', 
{
	addProduct: function(params, cb)
	{
		var client = this;

		var product_url = "http://no-product-url.local";

		// If it cames from SuiteTalk
		if (params.item)
		{
			params.internalId = params.item.internalId;
			params.urlComponent = params.item.urlComponent;
		}
		
		if (params.urlComponent)
		{
			var baseUrl = Preconditions.Configuration.website.baseUrl.replace(/\/?$/, '/');
			product_url = baseUrl + params.urlComponent;
		}
		else if (params.internalId)
		{
			var baseUrl = Preconditions.Configuration.website.baseUrl.replace(/\/?$/, '/');
			product_url = baseUrl  + 's.nl/it.A/id.' + params.internalId + '/.f';
		}
		// LEGACY CODE
		else if (params.siteNumber && params.accountNumber && params.itemId)
		{
			product_url = 'http://shopping.netsuite.com/s.nl/c.' + params.accountNumber + '/n.' + params.siteNumber + '/it.A/id.' + params.itemId + '/.f';
		}
		else if (params.baseUrl && params.name)
		{
			product_url = params.name;

			if (!product_url.match(/^http/i) && params.baseUrl) {
				product_url = params.baseUrl + product_url;
			}
		}
		// END OF LEGACY CODE

		this
			.url(product_url)

			.ItemDetails.SB.addToCart(
				params.quantity || 1,
				cb
			)

			.call(cb)
		;
	}

,	addProducts: function(productsList, cb)
	{
		var client = this;

		var baseUrl = Preconditions.Configuration.website.baseUrl;

		if (!Array.isArray(productsList))
		{
			productsList = [productsList];
		}

		client
			.call(function()
			{
				productsList.forEach(function(item)
				{
					client.ItemDetails.SB.addProduct({
						baseUrl: baseUrl
					,	item: item
					,	quantity: item.quantity
					});
				});	
			})
			.call(cb)
		;
	}	


,	addToCart: function(quantity, cb)
	{
		var client = this;

		if (typeof quantity === 'function') {
			cb = quantity;
			quantity = 1;
		}

		this
			.call(function()
			{
				if (quantity !== 1)
				{
					client.setValue('[name=qty]', quantity + '');
				}
			})
			
			.click('#addtocart')
			.waitForAjax(10000)
			
			.isExisting('//a[text()=\'View Cart\']', function(err, not_in_viewcart)
			{
				if (not_in_viewcart)
				{
					client
						.click('//a[text()=\'View Cart\']')
						.waitForAjax(10000)
					;
				}
			})
			
			.call(cb)
		;
	}


});