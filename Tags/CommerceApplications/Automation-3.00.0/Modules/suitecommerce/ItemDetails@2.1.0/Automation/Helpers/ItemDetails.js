var _ = require('underscore');

defineHelper('ItemDetails',
{

	addToCart: function(params, cb)
	{
		var datatype = null;

		if (typeof params === 'function')
		{
			cb = params;
			params = {};
		}
		else if (!_(params).isObject())
		{
			params = {
				quantity: params
			}
		}

		var client = this;

		this
			.waitForAjax()

			.ItemDetails.getProductDataType(function(err, result)
			{
				if (err)
				{
					throw new Error('Couldn\'t generate datatype for product ' + params.name);
				}

				datatype = result;
			})

			.call(function()
			{
				if (!params.itemOptions) return false;

				client
					.ItemDetails.selectOptions(params.itemOptions)
				;
			})

			.call(function() {
				if (!params.quantity) return false;

				client
					.click(Selectors.ItemDetails.quantity)
					.keys('\uE017') //delete
					.keys(String(params.quantity))

					.getValue(Selectors.ItemDetails.quantity, function(err, quantity_val)
					{
						if (!err)
						{
							datatype.Quantity = quantity_val;
						}
					})
				;
			})

			.waitForAjax()
			// NEEDS TO CLICK AWAY TO LOSE THE FOCUS OF THE SPINNER
			.click(".item-details-content-header-title")
			.waitForExist('button.item-details-add-to-cart-button', 5000)
			.waitForEnabled('button.item-details-add-to-cart-button', 10000)
			.click("button.item-details-add-to-cart-button")

			.waitForAjax()

			.waitForExist('.cart-confirmation-modal-view-cart-button', 5000)
			.click(".cart-confirmation-modal-view-cart-button")

			.waitForAjax()

			.call(function()
			{
				cb(null, datatype);
			})
		;

	}

,	goToProductDetailsPage: function(params, cb)
	{
		var client = this;

		var urlData = {
			baseUrl: params.baseUrl
		,	urlComponent: params.urlComponent
		,	internalId: params.internalId
		}

		// If it comes from SuiteTalk
		if (params.item)
		{
			urlData.internalId = params.item.internalId;
			urlData.urlComponent = params.item.urlComponent;
		}

		if (urlData.urlComponent)
		{
			urlData.baseUrl = Preconditions.Configuration.website.baseUrl;
		}
		else if (urlData.internalId)
		{
			urlData.baseUrl = Preconditions.Configuration.website.baseUrl;
			urlData.urlComponent = 'product/' + urlData.internalId;
		}
		// Legacy for compatibility
		// TODO: remove this when everything works wit preconditions
		else if (params.name)
		{
			urlData.urlComponent = params.name;
		}
		// end of legacy code

		var productUrl = urlData.urlComponent;

		if (_(productUrl).isString() && !productUrl.match(/^http/i) && urlData.baseUrl) {
			productUrl = urlData.baseUrl.replace(/\/+$/, '') + '/' + productUrl;
		} else {
			var failMessage = 'Should add product to cart with the following';
			failMessage += ' url data: ' + JSON.stringify(urlData);
			expect().toFailWith(failMessage);
		}

		client
			.url(productUrl)
			.call(cb)
		;
	}

,	addProduct: function (params, cb)
	{
		var client = this;

		var addToCartParams = {
			quantity: params.quantity
		,	itemOptions: params.options || params.itemOptions || params.matrixSelectedOptions
		}

		if (params.item)
		{
			if (params.item.isMatrix && !_(params.item.matrixChilds).isEmpty())
			{
				addToCartParams.itemOptions = params.item.matrixChilds[0].matrixSelectedOptions;
			}
		}

		this
			.ItemDetails.goToProductDetailsPage(params)

			.ItemDetails.addToCart(
				addToCartParams
			,	cb
			)
		;
	}


,	addProducts: function(productsList, cb)
	{
		var client = this;

		if (!Array.isArray(productsList))
		{
			productsList = [productsList];
		}

		client
			.call(function()
			{
				productsList.forEach(function(item)
				{
					if ( !_(item).isObject() || (_(item).isObject() && _(item).isArray()))
					{
						var foundType = (Array.isArray(item))? 'Array' : typeof item;
						expect().toBeTrueOrFailWith('Expected items in "addProducts" to be valid. \'' + foundType + '\' is not a valid item.');
						return false;
					}

					client.ItemDetails.addProduct({
						item: item
					,	quantity: item.quantity
					});
				});
			})
			.call(cb)
		;
	}


,	selectOption: function(option, cb)
	{
		if (!option)
		{
			return this.call(cb);
		}

		// IT COMES FROM SUITETALK
		if (option.optionScriptId)
		{
			this
				.waitForAjax()
				.waitForExist("[data-id='" + option.optionScriptId + "'] a[data-value='" + option.valueInternalId + "']")
				.click("[data-id='" + option.optionScriptId + "'] a[data-value='" + option.valueInternalId + "']")
			;
		}
		// TODO: LEGACY CODE - DELETE WHEN FINISH REFACTORING ALL TESTS TO PRECONDITIONS SYSTEM
		else if (option.dataValue)
		{
			this
				.waitForAjax()
				.waitForExist(".item-views-option-tile[data-cart-option-id='"
					+ option.dataCartOptionId + "'] [data-value='"
					+ option.dataValue + "']", 5000)

				.click(".item-views-option-tile[data-cart-option-id='"
					+ option.dataCartOptionId + "'] [data-value='"
					+ option.dataValue + "']")
			;
		}
		else
		{
			this
				.waitForAjax()
				.waitForExist('#option-' + option.dataCartOptionId, 10000)
				.click('#option-' + option.dataCartOptionId)
				.waitForAjax()
				.setValue('#option-' + option.dataCartOptionId, option.value)
				.waitForAjax()
				// NEEDS TO CLICK AWAY TO LOSE THE FOCUS
				.click('.item-details-content-header-title')
				.waitForAjax()
			;
		}

		this.call(cb);
	}

,	selectOptions: function(options, cb)
	{
		var client = this;

		if (options)
		{
			// if it's a parent item, we select the first possible child
			if (!_(options.matrixChilds).isEmpty())
			{
				options = options.matrixChilds[0];
			}

			// if it's a child item, we pass the matrixSelectedOptions field
			if (!_(options.matrixSelectedOptions).isEmpty())
			{
				options = options.matrixSelectedOptions;
			}

			options.forEach(function(option)
			{
				client.ItemDetails.selectOption(option);
			});
		}

		client.call(cb);
	}

,	addToWishlist: function(wishlist, cb)
	{
		this
			.waitForAjax()
			.waitForExist(".item-details", 10000)

			.waitForExist('button.product-list-control-button-wishlist')
			.click("button.product-list-control-button-wishlist")

			.waitForExist('.product-list-control-item-checkbox')
			.click(".product-list-control-item-checkbox")

			.waitForAjax()

			.call(cb)
		;
	}

,	addToWishlistSLM: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(".item-details", 10000)

			.waitForExist('button.product-list-control-single-button-wishlist')
			.click("button.product-list-control-single-button-wishlist")

			.waitForAjax()

			.call(cb)
		;
	}

,	correctlyAddToWishlist: function(cb)
	{
		var message = null;
		this
			.waitForAjax()
			.waitForExist(".global-views-message-success", 10000)
			.waitForAjax()
			.getText(".global-views-message-success div", function(err, text)
			{
				message = text;
			})
			.call(function()
			{
				cb(null, message);
			})
		;
	}

,	addToSelectedWishlist: function(wishlistId, cb)
	{
		this
			.waitForAjax()
			.waitForExist(".item-details", 10000)

			.waitForExist(Selectors.ProductList.buttonAddToWishlist)
			.click(Selectors.ProductList.buttonAddToWishlist)

			.waitForExist(Selectors.ProductList.wishlistId.replace('%s', wishlistId))
			.click(Selectors.ProductList.wishlistId.replace('%s', wishlistId))

			.waitForAjax()

			.call(cb)
		;
	}

,	isInPage: function(cb)
	{
		installHelpers(this,'generic')

		this
			.waitForAjax()
			.waitForExist('.product-detail', 10000)
			.waitForAjax()
			.isExisting('.product-detail', cb)
		;
	}

,	getProductDataType: function(cb){
		var prefix = "//*[@class='item-details-main-content']"
		var dataType = {
			'Name': ''
		,	'Sku': ''
		,	'ItemPrice': ''
		,	'Quantity': ''
		}

		this
			.waitForAjax()
			.waitForExist(prefix + "//*[@class='item-details-content-header-title']", 10000)
			.getText(prefix + "//*[@class='item-details-content-header-title']", function(err, text){
				dataType.Name = text;
			})
			.getText(prefix + "//*[@class='item-details-price']//*[@class='item-views-price-lead']", function(err, text){
				dataType.ItemPrice = text;
			})
			.getText(prefix + "//*[@class='item-details-sku-value']", function(err, text){
				dataType.Sku = text;
			})
			.getValue(prefix + "//*[@class='item-details-quantity-value']", function(err, text){
				dataType.Quantity = text;
			})

			.call(function()
			{
				cb(null, dataType)
			})
		;
	}

,	getStockMessage: function(cb)
	{
		this
			.waitForAjax()
			.getText(".item-views-stock-msg-out-text", function(err, text)
			{
				if(!err)
				{
					cb(null,text[0]);
				}
				else
				{
					cb(err, null);
				}
			})
		;
	}
});
