var _ = require('underscore');

defineHelper('ItemDetails.QuickView',
{
	getProductDataType: function(cb)
	{
		var prefix = "//*[@class='quick-view-confirmation-modal']"
		var dataType = {
			'Name': ''
		,	'Sku': ''
		,	'ItemPrice': ''
		,	'Quantity': ''
		,	'isImagePresent': false
		,	'isAddToCart': false
		,	'isAddToWishlist': false
		}

		this
			.waitForAjax()
			.waitForExist(prefix + "//*[@class='quick-view-confirmation-modal-item-name']", 10000)
			.getText(prefix + "//*[@class='quick-view-confirmation-modal-item-name']", function(err, text){
				dataType.Name = text;
			})
			.getText(prefix + "//*[@class='item-views-price-lead']", function(err, text){
				dataType.ItemPrice = text;
			})
			.getText(prefix + "//*[@class='quick-view-confirmation-modal-sku-value']", function(err, text){
				dataType.Sku = text;
			})
			.getValue(prefix + "//*[@class='quick-view-quantity-value']", function(err, text){
				dataType.Quantity = text;
			})
			.isExisting(prefix + "//*[@class='item-details-image-gallery']", function(err, isVisible){
				dataType.isImagePresent = isVisible;
			})
			.isExisting(prefix + "//*[@class='quick-view-confirmation-modal-view-cart-button']", function(err, isVisible){
				dataType.isAddToCart = isVisible;
			})
			.isExisting(prefix + "//*[@class='product-list-control-button-wishlist']", function(err, isVisible){
				dataType.isAddToWishlist = isVisible;
			})

			.call(function()
			{
				cb(null, dataType)
			})
		;
	}

,	addToWishlist: function(wishlist, cb)
	{
		this
			.waitForAjax()
			.waitForExist(".global-views-modal-content", 10000)

		    .waitForExist('button.product-list-control-button-wishlist')
		    .click("button.product-list-control-button-wishlist")

		    .waitForExist('.product-list-control-item-checkbox')
		    .click(".product-list-control-item-checkbox")

		    .waitForAjax()

			.call(cb)
		;
	}

,	correctlyAddToWishlist: function(cb)
	{
		var message = null;
		this
			.waitForAjax()
			.waitForExist(".global-views-message-success", 10000)
			.waitForAjax()
			.getText(".global-views-message-success div", function(err, text)
			{
				message = text;
			})
			.call(function()
			{
				cb(null, message);
			})
		;
	}

,	addToCart: function(sku, cb)
	{
		this
			.waitForAjax()
			.waitForExist('//*[@class = \'quick-view-confirmation-modal-sku-value\'][contains(text(),\'' + sku + '\')]/../..//button[@class = \'quick-view-confirmation-modal-view-cart-button\']',10000)
			.click('//*[@class = \'quick-view-confirmation-modal-sku-value\'][contains(text(),\'' + sku + '\')]/../..//button[@class = \'quick-view-confirmation-modal-view-cart-button\']')
			.waitForAjax()
			.call(cb)
		;
	}

,	continueShopping: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.cart-confirmation-modal-continue-shopping-button',10000)
			.click('.cart-confirmation-modal-continue-shopping-button')
			.waitForAjax()
			.call(cb)
	}

,	goToViewCartAndCheckout: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.cart-confirmation-modal-view-cart-button',10000)
			.click('.cart-confirmation-modal-view-cart-button')
			.waitForAjax()
			.call(cb)
		;
	}

,	selectOption: function(option, cb)
	{
		if (option)
		{
			if (option.dataValue)
			{
				this
					.waitForAjax()
				    .waitForExist(".item-views-option-tile[data-cart-option-id='"
				    	+ option.dataCartOptionId + "'] [data-value='"
				    	+ option.dataValue + "']", 5000)

				    .click(".item-views-option-tile[data-cart-option-id='"
				    	+ option.dataCartOptionId + "'] [data-value='"
				    	+ option.dataValue + "']")

				;
			}
			else
			{
				this
					.waitForAjax()
					.waitForExist('#in-modal-option-' + option.dataCartOptionId, 10000)
					.click('#in-modal-option-' + option.dataCartOptionId)
					.waitForAjax()
					.setValue('#in-modal-option-' + option.dataCartOptionId, option.value)
					.waitForAjax()
					// NEEDS TO CLICK AWAY TO LOSE THE FOCUS
					.click('.quick-view-confirmation-modal-item-name')
					.waitForAjax()
				;
			}
		}

		this.call(cb);
	}

,	selectOptions: function(options, cb)
	{
		var client = this;

		if (options)
		{
			options.forEach(function(option, index)
			{
				client.ItemDetails.QuickView.selectOption(option);
			});
		}

		client.call(cb);
	}

,	closeQuickView: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.global-views-modal-content-header-close', 10000)
			.click('.global-views-modal-content-header-close')
			.waitForAjax()
			.call(cb)
	}

,	addToWishlistSLM: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(".quick-view-confirmation-modal", 10000)

		    .waitForExist('button.product-list-control-single-button-wishlist')
		    .click("button.product-list-control-single-button-wishlist")

		    .waitForAjax()

			.call(cb)
		;
	}

,	isAddToCartButtonDisable: function(disable, cb)
	{
		this
			.getAttribute('.global-views-modal [data-type="add-to-cart"]', 'disabled', function(err, disabled)
			{
				var isDisable = disabled ? true : false;
				expect(isDisable).toBe(disable);
			})
			.call(cb)
		;
	}

,	selectQuantity: function (quantity, cb)
	{
		this
			.click('.quick-view-quantity-value')
			.keys('\uE017') //delete
			.keys(String(quantity))

			// NEEDS TO CLICK AWAY TO LOSE THE FOCUS OF THE SPINNER
			.click('.quick-view-confirmation-modal-item-name')
			.waitForAjax()
			.call(cb)
		;
	}

,	getOptionsSelected: function(item, cb)
	{
		var prefix = '.global-views-modal .item-views-option-tile'
		,	client = this;

		item.options = {};

		client
			.waitForExist(prefix, 10000)
			.getAttribute(prefix, 'data-cart-option-id', function(err, names)
			{
				names = !_.isArray(names) ? [names] : names
				names.forEach(function(name)
				{
					client
						.isExisting(prefix + '[data-cart-option-id="' + name + '"] [data-active="true"] span', function(err, isExisting)
						{
							if (isExisting)
							{
								item.options[name] = {};

								client
									.getText(prefix + '[data-cart-option-id="' + name + '"] [data-active="true"] span', function(err, text)
									{
										item.options[name].value = text;
									})
									.getAttribute(prefix + '[data-cart-option-id="' + name + '"] [data-active="true"]', 'data-value', function(err, value)
									{
										item.options[name].dataValue = value;
									})
									.getText(prefix + '[data-cart-option-id="' + name + '"] .item-views-option-tile-title strong', function(err, label)
									{
										item.options[name].label = label;
									})
									.getText(prefix + '[data-cart-option-id="' + name + '"] .item-views-option-tile-title', function(err, text)
									{
										item.options[name].text = text;
									})
								;
							}
						})
					;
				});
			})
			.call(cb)
		;
	}

,	getItemDataType: function(item, cb)
	{
		var prefix = '.global-views-modal ';

		this
			.waitForAjax()
			.waitForExist(prefix + '.quick-view-confirmation-modal-item-name', 10000)
			.getText(prefix + '.quick-view-confirmation-modal-item-name', function(err, text){
				item.name = text;
			})
			.getText(prefix + '.item-views-price-lead', function(err, text){
				item.itemPrice = text;
			})
			.getText(prefix + '.quick-view-confirmation-modal-sku-value', function(err, text){
			 	item.sku = text;
			})
			.getValue(prefix + '.quick-view-quantity-value', function(err, text){
				item.quantity = text;
			})
			.getAttribute(prefix + '.quick-view-confirmation-modal-img img', 'src', function(err, text){
				item.imageUrl = text;
			})
			.ItemDetails.QuickView.getOptionsSelected(item)
			.ItemDetails.QuickView.getAllOptions(item)
			.call(cb)
		;
	}

,	getAllOptions: function(item, cb)
	{
		var prefix = '.global-views-modal .item-views-option-tile'
		,	client = this;

		item.allOptions = {};

		client
			.waitForExist(prefix, 10000)
			.getAttribute(prefix, 'data-cart-option-id', function(err, names)
			{
				names = !_.isArray(names) ? [names] : names
				names.forEach(function(name)
				{
					item.allOptions[name] = { values: {} };

					client
						.getText(prefix + '[data-cart-option-id="' + name + '"] .item-views-option-tile-title strong', function(err, label)
						{
							item.allOptions[name].label = label;
						})
						.getAttribute(prefix + '[data-cart-option-id="' + name + '"] .item-views-option-tile-anchor', 'data-value', function(err, values)
						{
							values = !_.isArray(values) ? [values] : values
							values.forEach(function(value)
							{
								client
									.getText(prefix + '[data-cart-option-id="' + name + '"] .item-views-option-tile-anchor[data-value="' + value + '"]', function(err, text)
									{
										item.allOptions[name].values[text] = value;
									});
							});
						})
					;
				});
			})
			.call(cb)
		;
	}
});
