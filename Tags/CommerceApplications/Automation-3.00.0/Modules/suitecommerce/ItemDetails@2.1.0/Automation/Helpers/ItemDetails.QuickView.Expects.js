defineHelper('ItemDetails.QuickView.Expects',
{
	correctlyAddedToWishList : function(cb)
	{
		this
			.ItemDetails.QuickView.correctlyAddToWishlist(function (err, confirmationText){
    			expect(confirmationText).toEqual("Good! You added this item to your product list");
    		})
    		.call(cb)
    	;
	}

,	isCartHelpVisible: function(visible, cb)
	{
		this
			.isVisible('.quick-view-add-to-cart-help', function(err, isVisible)
			{
				expect(isVisible).toEqual(visible);
			})
			.call(cb)
		;
	}

,	checkItemData: function (data, item, cb)
	{
		data.name && expect(data.name).toBeSameText(item.name);
		data.sku && expect(data.sku).toEqual(item.sku);
		data.imageUr && expect(unescape(item.imageUrl)).toContain(data.imageUrl);
		data.itemPrice && expect(data.itemPrice).toEqual(item.itemPrice);

		if (data.options)
		{
			expect(data.options.length).toEqual(Object.keys(item.options).length);

			data.options.forEach(function(value, index)
			{
				var option = item.options[value.dataCartOptionId];

				expect(option.label).toEqual(value.label.toUpperCase());
				expect(option.value).toEqual(value.value);
				expect(option.dataValue).toEqual(value.dataValue);
				expect(option.text).toEqual((value.label + ' ' + value.value).toUpperCase());
			});
		}

		if (data.allOptions)
		{
			data.allOptions.forEach(function(dataOption, index)
			{
				var option = item.allOptions[dataOption.dataCartOptionId];

				expect(option.label).toEqual(dataOption.label.toUpperCase());

				dataOption.values.forEach(function(value, index)
				{
					expect(option.values[value.value]).toEqual(value.dataValue);
				});
			});
		}

		this
			.call(cb)
		;
	}
})
