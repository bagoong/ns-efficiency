
defineHelper('OrderWizard.Module.Package', 
{
	
	getShippableItems: function(cb)
	{
		var client = this;
		var shippable_items = [];

		//item-views-cell-selectable-item-displayname-viewonly

		this
			.getAttribute('[data-view=\'ShippableItems.Collection\'] tr', 'data-item-id', function(err, data_item_ids)
			{
				data_item_ids.forEach(function(data_item_id)
				{
					var item = {
						'dataItemId': data_item_id
					,	'name': ''
					,	'sku': ''
					,	'quantity': 0
					};

					client
						//.waitForAjax()
						//.waitForExist('tr[data-item-id=\'' + data_item_id + '\'] .item-views-cell-selectable-item-displayname', 5000)

						.getText('tr[data-item-id=\'' + data_item_id + '\'] .item-views-cell-selectable-item-displayname', function(err, text)
						{
							item.name = text;
						})

						.getText('tr[data-item-id=\'' + data_item_id + '\'] .item-views-cell-selectable-item-sku', function(err, text)
						{
							item.sku = text.replace(/^.*?:/, '').trim();
						})

						.getTextAsFloat('tr[data-item-id=\'' + data_item_id + '\'] .order-wizard-msr-package-creation-edit-quantity-value', function(err, quantity)
						{
							item.quantity = quantity;
						})						
					;

					shippable_items.push(item);
				})
			})
			.call(function()
			{
				//console.log(shippable_items);
				cb(null, shippable_items);
			})
		;
	}


,	addItemToShipment: function(params, cb)
	{
		this
			.call(cb)
		;
	}


,	clickCreateShipment: function(cb)
	{
		this
			.waitForExist('.order-wizard-msr-package-creation-button-create')
			.click('.order-wizard-msr-package-creation-button-create')
			.call(cb)
		;
	}

});

