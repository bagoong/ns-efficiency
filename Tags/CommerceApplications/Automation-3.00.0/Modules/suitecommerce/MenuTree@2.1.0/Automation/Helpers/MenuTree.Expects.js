defineHelper('MenuTree.Expects',
{

	verifyLoadedLayout: function (layout, cb)
	{
		this
			.waitForAjax()
			.waitForExist('.' + layout, 5000)
			.isExisting('.' + layout).then(function (text)
			{
				expect(text).toBeTrue();
			})
			.call(cb)
		;
	}

,	verifyQuotesLink: function (linkPresence, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.buttonQuotes)
			.isExisting(Selectors.MenuTree.buttonQuotes).then(function (isExisting)
			{
				expect(isExisting).toBe(linkPresence);
			})
			.call(cb)
		;
	}

});