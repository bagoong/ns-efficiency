defineHelper('Profile', 
{

	modifyProfileInformation: function(params, cb)
	{
		var client = this;

		if( Array.isArray(params) )
		{
			params.forEach(function(param)
			{
				client
					.setValue(Selectors.Profile[param.name], param.data)
				;
			})
			
			client
				.Profile.clickUpdate()
			;

			params.forEach(function(param)
			{
				client
					.Profile.verifyUpdate(param)
				;
			})

		} else if (!Array.isArray(params)) {
			client
				.setValue(Selectors.Profile[params.name], params.data)
				.Profile.clickUpdate()
				.Profile.verifyUpdate(params)
			;
		}

		this
			.call(cb)
		;
	}	

,	verifyUpdate: function(params, cb)
	{
		var client = this;

		if (params.errorStatus)
		{	
			client
				.waitForExist(Selectors.Profile.errorMessage.replace('%s', params.name), 5000)
				.getText(Selectors.Profile.errorMessage.replace('%s', params.name), function(err, text)
				{
					expect(text).toMatch(params.errorLabel);
				})				
			;

		} else if (!params.errorStatus) {
			client
				.waitForExist(Selectors.Profile.successMessage, 5000)
				.getText(Selectors.Profile.successMessage, function(err, text)
				{
					expect(text).toMatch('Profile successfully updated!');
				})				
			;
		} else {
			client
				.waitForExist(Selectors.Profile.successMessage, 5000)
				.getText(Selectors.Profile.successMessage, function(err, text)
				{
					expect(text).not.toMatch('Profile successfully updated!');
				})				
			;			
		}

		this
			.call(cb)
		;
	}	

,	clickUpdate: function(cb)
	{
		this
			.waitForExist(Selectors.Profile.updateButton, 5000)
			.click(Selectors.Profile.updateButton)
			.call(cb)
		;
	}	

,	editProfile: function (params, cb)
	{
		this
			// .waitForExist('a[data-id=settings]', 10000)
			// .click('a[data-id=settings]')
			// .waitForExist('a[data-id=profileinformation]')
			// .click('a[data-id=profileinformation]')
			
			.waitFor(Selectors.Profile.editButton, 100000)
			.click(Selectors.Profile.editButton)

			.waitForExist(Selectors.Profile.inputFirstname)
			
			//.clearElement('input[name="firstname"]')
	        .setValue(Selectors.Profile.inputFirstname, params.firstname || '')

			//.clearElement('input[name="lastname"]')
	        .setValue(Selectors.Profile.inputLastname, params.lastname || '')

			//.clearElement('input[name="companyname"]')
	        .setValue(Selectors.Profile.inputCompanyname, params.companyname || '')

			//.clearElement('input[name="phone"]')
	        .setValue(Selectors.Profile.inputPhone, params.phone || '')

	        .click(Selectors.Profile.profileHeader)

	        .click(Selectors.Profile.updateButton)

	        .waitForAjax()

	        .call(cb);
	}

,	updateEmailPreferences: function(params, cb)
	{
		this
			.setCheckboxValue({selector: Selectors.Profile.subscriptionTwo, check: params.Billing})

			.setCheckboxValue({selector: Selectors.Profile.subscriptionOne, check: params.Marketing})

			.setCheckboxValue({selector: Selectors.Profile.subscriptionFour, check: params.Newsletter})

			.setCheckboxValue({selector: Selectors.Profile.subscriptionFive, check: params.ProductUpdates})

			.setCheckboxValue({selector: Selectors.Profile.subscriptionThree, check: params.Surveys})

			.click(Selectors.Profile.emailPreferences)

			.waitForAjax()

			.call(cb)
		;
	}

,	updatePassword: function(params, cb)
	{
		this
			.waitFor(Selectors.Profile.passCurrent, 100000)

			.setValue(Selectors.Profile.passCurrent, params.currentPassword)

			.setValue(Selectors.Profile.passNew, params.newPassword)
			
			.setValue(Selectors.Profile.passRepeat, params.confirmPassword)

			.click(Selectors.Profile.updatePasswordButton)

			.waitForAjax()

			.call(cb)
		;
	}
});