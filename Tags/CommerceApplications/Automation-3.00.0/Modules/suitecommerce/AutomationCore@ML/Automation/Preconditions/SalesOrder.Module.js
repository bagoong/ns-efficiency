/* globals Preconditions */
var suitetalk = require('suitetalk'); 

// NS TRANSACTIONS DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/salesorder.html

// @class Preconditions.Transaction
Preconditions.SalesOrder = {
	getByCustomer: function(customer, credentials, cb)
	{
		'use strict';

		suitetalk.setCredentials(credentials);

		suitetalk.search({
				recordType: 'transaction'
			,	filters: {
					type: {
						type: 'SearchEnumMultiSelectField'
					,	operator: 'anyOf'
					,	searchValue: [
							'SalesOrd'
						]
					}
				}

			,	joins: [{
					recordType: 'customer'
				,	filters: {
						internalId: {
							type: 'SearchMultiSelectField'
						,	operator: 'anyOf'
						,	searchValue: [{
									type: 'RecordRef'
								,	internalId: customer.internalId
							}]
						}
					}
				}]
			}
		)

		.then(function(response)
		{
			var records = response.searchResponse[0].searchResult[0].recordList[0].record;

			if (Array.isArray(records)) {
				records.forEach(function(val)
				{
					val.internalId = val.$.internalId;
				});
			}

			cb(null, records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}
};