
var	suitetalk = require('suitetalk')
,	_ = require('underscore');


Preconditions.add('get_one_preferred_invoice_terms', ['configuration'], function(configuration, cb)
{
	Preconditions.PaymentMethod.getAvailableInvoiceTerms(
		Preconditions.Configuration.credentials
	,	function(error, terms)
		{
			if (!error && _(terms).isEmpty())
			{
				pending("Site has not preferred invoice terms");
			}

			cb(error, _(terms).sample());
		}
	);
});

Preconditions.add('get_external_payment_methods', ['configuration'], function(configuration, cb)
{
	var availableExternalPaymentMethods = [];

	var siteSettings = Preconditions.Configuration.website.siteSettings;

	if (Array.isArray(siteSettings.paymentmethods))
	{
		siteSettings.paymentmethods.forEach(function(paymentMethod)
		{
			var name = null;

			if (paymentMethod.isexternal === 'T')
			{
				availableExternalPaymentMethods.push(paymentMethod)
			}
		});
		if(availableExternalPaymentMethods.length)
		{
			cb(null, availableExternalPaymentMethods)
		}
		else
		{
			pending("Site has not external payments", cb);
		}
	}
});

Preconditions.add('get_one_external_payment_method', ['configuration'], function(configuration, cb)
{
	var availableExternalPaymentMethods = [];

	var siteSettings = Preconditions.Configuration.website.siteSettings;

	if (Array.isArray(siteSettings.paymentmethods))
	{
		siteSettings.paymentmethods.forEach(function(paymentMethod)
		{
			var name = null;

			if (paymentMethod.isexternal === 'T')
			{
				availableExternalPaymentMethods.push(paymentMethod)
			}
		});
		if(availableExternalPaymentMethods.length)
		{
			cb(null, availableExternalPaymentMethods[0])
		}
		else
		{
			pending("Site has not external payments", cb);
		}
	}
});
