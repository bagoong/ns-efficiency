
var	suitetalk = require('suitetalk')
,	async = require('async')
,	_ = require('underscore')
;

Preconditions.CustomRecord = {

	getCustomizations: function(params, cb)
	{
		var options = {
			getCustomizationType: params.customizationType || params.getCustomizationType
		,	includeInactives: params.includeInactives || false
		}

		suitetalk.setCredentials(
			Preconditions.Configuration.credentials
		);

		suitetalk
			.getCustomizationId(
				options
			)

			.then(function(response)
			{
				response = Preconditions.Helpers.formatResponseRecord(response);

				//console.log(JSON.stringify(response, null, 4));

				var records = response.getCustomizationIdResponse[0].getCustomizationIdResult[0].customizationRefList[0].customizationRef;
				var status = response.getCustomizationIdResponse[0].getCustomizationIdResult[0].status[0].$;

				if (Array.isArray(records))
				{
					records = records.map(function(record)
					{
						return {
							name: record.name
						,	internalId: record.$.internalId
						,	type: record.$.type
						,	scriptId: record.$.scriptId
						}
					});
				}

				var result = {
					records: records
				,	status: status
				}

				cb(null, result);
			})
			.catch(function(error)
			{
				//console.log(error.stack);
				cb(error, null);
			});
		;
	}


,	getCustomRecordType: function(scriptId, cb)
	{
		Preconditions.CustomRecord.getCustomizations(
			{
				customizationType: 'customRecordType'
			}
		,	function(err, res)
			{
				if (err)
				{
					cb(err);
				}
				else
				{
					var customRecordType = _.find(res.records, function(rec)
					{
						return rec.scriptId === scriptId;
					});

					cb(null, customRecordType);
				}
			}
		);
	}

,	_searchCustomRecords: function(params, cb)
	{
		var customRecordTypeId = params.customRecordTypeId || params.recType;

		suitetalk.setCredentials(
			Preconditions.Configuration.credentials
		);

		suitetalk
			.searchBasic({
				'filters': {
					'recType': {
						type: 'RecordRef'
					,	internalId: customRecordTypeId + ''
					}
				}
			,	'recordType': 'customRecord'
			})
			.then(function(res)
			{
				var response = Preconditions.Helpers.formatSearchResponse(res);
				cb(null, response);
			})
			.catch(function(error)
			{
				cb(error);
			})
		;
	}


,	searchCustomRecords: function(params, done)
	{
		var self = this;

		params = typeof params === 'string' ? { scriptId: params } : params;

		var customRecordScriptId = params.scriptId || params.customizationId;
		var customRecordTypeId = params.recordTypeId;

		async.waterfall([
			function(cb)
			{
				if (!customRecordTypeId)
				{
					self.getCustomRecordType(customRecordScriptId, function(err, res)
					{
						if (err) return cb(err);
						cb(null, res.internalId);
					});
				}
				else
				{
					cb(null, customRecordTypeId);
				}
			}

		,	function(custRecTypeId, cb)
			{
				params.customRecordTypeId = custRecTypeId;
				self._searchCustomRecords(params, cb);
			}
		],	done);
	}
}