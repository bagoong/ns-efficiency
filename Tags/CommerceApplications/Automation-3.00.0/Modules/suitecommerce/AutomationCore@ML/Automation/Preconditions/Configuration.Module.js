var Preconditions = require('preconditions')
,	suitetalk = require('suitetalk')
,	path = require('path')
,	fs = require('fs')
,	args = require('yargs').argv
,	_ = require('underscore')
,	request = require('request')
,	async = require('async')
,	vm = require('vm')
,	inquirer = require('inquirer')
;

// var DISTRO_ROOT_PATH = path.resolve(path.join(__dirname, '../../../../../'))
var DISTRO_ROOT_PATH = path.resolve(process.cwd())
	AUTOMATION_TEMP_PATH = path.join(DISTRO_ROOT_PATH, "automation");
;

Preconditions.Configuration = {

	configurationComplete: false
,	configurationChanged : false

,	configurationFilePath: null

,	credentials: {
		'email': 'webservices@example.com'
	,	'password': 'ExamplePass'
	,	'account': 'Account number, ex: 123456'
	,	'roleId': 'Role Id, ex: 3'
	,	'molecule': null
	}

,	website: {
		'baseUrl': 'http://examplesite.com'
	,	'siteId': 'Site number, ex: 2'
	,	'subsidiary': null
	,	'displayOutOfStockItems': false
	,	'displayOutOfStockMessage': true
	,	'allowOutOfStockBackOrder': false
	}

,	suites: {
		'standard-checkout': {
			'tests': [ 'CheckoutApplication/Standard/*' ]
		}
	,	'one-page-checkout': {
			'tests': [ 'CheckoutApplication/OnePage/*' ]
		}
	,	'billing-first-checkout': {
			'tests': [ 'CheckoutApplication/BillingFirst/*' ]
		}
	}

,	initConfigurationTemplate: function()
	{
		return {
			'_comment': 'Remove this entry when you changed the example data for the real one.'
		,	'credentials': this.credentials
		,	'website': this.website
		,	'suites': this.suites
		}
	}


,	getConfiguration: function(options)
	{
		var self = this;

		var options = options || {};
		var excludeSections = options.excludeSections || [];

		var exportableSections = _(self.sections).difference(excludeSections);

		var dataset = _.pick(self, exportableSections);

		return dataset;
	}


,	sections: [
		'credentials'
	,	'website'
	]


,	addSection: function(sectionName, sectionData)
	{
		var self = this;

		if (self.sections.indexOf(sectionName) === -1)
		{
			self.sections.push(sectionName);
		}

		if (!_.isObject(self[sectionName]))
		{
			self[sectionName] = _.clone(sectionData);
		}

		if (_.isObject(self[sectionName]) && !_.isArray(self[sectionName]))
		{
			self[sectionName] = _.defaults(self[sectionName], sectionData);
		}

		self.upgradeConfigurationFile();

		return self[sectionName];
	}


,	loadConfiguration: function(done)
	{
		var self = this;

		self.initConfigurationPaths();

		self.loadConfigurationFromFile(
			self.configurationFilePath
		);

		self.validations = {
			website: false
		,	credentials: false
		}

		async.waterfall(
		[
			function(cb)
			{
				self.checkWebsite(cb);
			}

		,	function(cb)
			{
				self.checkCredentials(cb);
			}

		,	function(cb)
			{
				self.configurationComplete = self.validations.website && self.validations.credentials;

				if (self.configurationComplete)
				{
					if (!self.configurationChanged)
					{
						return cb();
					}

					self.promptPersistConfiguration(cb)
				}
				else
				{
					self.raiseIncompleteConfigurationFileError();
				}
			}
		,	function(cb)
			{
				done(null, self.getConfiguration());
			}
		]
		,	function(error)
			{
				throw error;
			}
		);
	}


,	getAutomationTempPath: function(relPath)
	{
		var automationTempPath = AUTOMATION_TEMP_PATH;

		if (relPath)
		{
			automationTempPath = path.join(automationTempPath, relPath);
		}

		return automationTempPath;
	}


,	getConfigurationFilesDir: function(relPath)
	{
		var configurationPath = path.join(AUTOMATION_TEMP_PATH, "websites");

		if (!fs.existsSync(configurationPath))
		{
			fs.mkdirSync(configurationPath);
		}

		if (relPath)
		{
			configurationPath = path.join(configurationPath, relPath);
		}

		return configurationPath;
	}


,	loadConfigurationFromFile: function(configFilePath)
	{
		var self = this;
		var configFileName = path.basename(configFilePath);

		if (fs.existsSync(configFilePath))
		{
			self.configurationFilePath = configFilePath;

			try
			{
				var parsedConfig = JSON.parse(fs.readFileSync(configFilePath, {encoding: 'utf8'}));
			}
			catch (error)
			{
				console.log("");
				console.log("  Invalid JSON file:");
				console.log("  " + configFilePath);
				console.log("");
				process.exit(1);
			}

			if (parsedConfig && '_comment' in parsedConfig)
			{
				self.configurationComplete = false;
			}

			self.sections = _.union(self.sections, Object.keys(parsedConfig));
			_(self).extend(parsedConfig);
		}
		else
		{
			self.raiseNonExistentPathError(configFilePath);
		}
	}


,	upgradeConfigurationFile: function()
	{
		var self = this;

		var dataset = JSON.parse(
			fs.readFileSync(self.configurationFilePath)
		);

		dataset = _.extend(
			dataset
		,	self.getConfiguration({
				excludeSections: ['website', 'credentials']
			})
		);

		fs.writeFileSync(
			self.configurationFilePath
		,	JSON.stringify(dataset, null, 4)
		);
	}


,	initConfigurationPaths: function()
	{
		var self = this;

		var configurationDir = self.getConfigurationFilesDir();

		if (!fs.existsSync(configurationDir))
		{
			fs.mkdirSync(configurationDir);
		}

		var websiteExampleConfigPath = path.join(configurationDir, "example.json");

		if (!fs.existsSync(websiteExampleConfigPath))
		{
			fs.writeFileSync(websiteExampleConfigPath, JSON.stringify(self.initConfigurationTemplate(), null, 4));
		}

		var websiteConfigName = args.website || args.site || "default.json";
		websiteConfigName = (! /\.json$/.test(websiteConfigName))? websiteConfigName + ".json" : websiteConfigName;

		var websiteConfigPath = path.join(configurationDir, websiteConfigName);

		if (websiteConfigName === "default.json" && !fs.existsSync(websiteConfigPath))
		{
			fs.writeFileSync(websiteConfigPath, JSON.stringify(self.initConfigurationTemplate(), null, 4));
		}

		self.configurationFilePath = websiteConfigPath;
	}


,	checkWebsite: function(done)
	{
		var self = this;

		var baseUrl = args.url || self.website.baseUrl;

		var isValidSite = false;

		async.waterfall([
			function(cb)
			{
				if (!baseUrl || /examplesite.com/i.test(baseUrl))
				{
					self.configurationChanged = true;
					return self.promptWebsite(cb);
				}
				else
				{
					baseUrl = Preconditions.Website.fixUrl(baseUrl);

					self.getConfigurationFromWebsite(baseUrl, function(error, onlineConfiguration)
					{
						cb(error, onlineConfiguration);
					});
				}
			}

		,	function(onlineConfiguration)
			{
				self.website.baseUrl = onlineConfiguration.baseUrl;
				self.applyConfigurationFromWebsite(onlineConfiguration);
				self.validations.website = true;

				done();
			}

		]
		,	function(error)
			{
				if (error === 'invalid_site' || error === 'undefined_baseurl')
				{
					return self.raiseInvalidSiteError(baseUrl);
				}
				else if (error)
				{
					throw error;
				}
			}
		);
	}


,	promptWebsite: function(done)
	{
		var self = this;
		var defaultMessage = 'Site url (ex: http://website.com):';

		var onlineConfiguration = null;

		inquirer.prompt(
		{
				type: 'input'
			,	name: 'url'
			,	message: defaultMessage
			,	filter: function(url)
				{
					return Preconditions.Website.fixUrl(url);
				}
			,	validate: function(url)
				{
					var validateDone = this.async();

					url = url.trim();

					if (url.length === 0)
					{
						return validateDone(defaultMessage);
					}

					url = Preconditions.Website.fixUrl(url);

					self.getConfigurationFromWebsite(url, function(error, resultConfiguration)
					{
						if (error === 'invalid_site' || error === 'undefined_baseurl')
						{
							return validateDone(url + ' is not a valid SuiteCommerce site.');
						}
						else
						{
							onlineConfiguration = resultConfiguration;
							return validateDone(true);
						}
					});

				}
			}
		,	function(answers)
			{
				self.validations.website = true;
				done(null, onlineConfiguration);
			}
		);
	}


,	getConfigurationFromWebsite: function(baseUrl, done)
	{
		Preconditions.Website.getConfigurationFromWebsite(
			baseUrl, done
		);
	}


, 	applyConfigurationFromWebsite: function(onlineConfiguration)
	{
		var self = this;

		self.website.subsidiary = onlineConfiguration.subsidiary || self.website.subsidiary || null;
		self.website.siteId = onlineConfiguration.siteId || self.website.siteId;
		self.website.siteSettings = onlineConfiguration.siteSettings || {};
		self.website.checkoutSkipLogin = null || onlineConfiguration.checkoutSkipLogin;
		self.website.siteType = null || onlineConfiguration.siteType;
		self.website.priceLevel = null || onlineConfiguration.siteSettings.defaultpricelevel;
		self.website.passwordProtectedSite = null || onlineConfiguration.passwordProtectedSite;

		if (onlineConfiguration.order)
		{
			self.website.displayOutOfStockItems = onlineConfiguration.order.outofstockitems !== 'REMOVE';
			self.website.displayOutOfStockMessage = onlineConfiguration.order.outofstockbehavior === 'ENABLENMSG';
			self.website.allowOutOfStockBackOrder = true;
		}

		self.credentials.account = onlineConfiguration.companyId || self.credentials.account;
	}


,	checkCredentials: function(done)
	{
		var self = this;

		var currentEmail = args.user || args.email || self.credentials.email;

		if (currentEmail && /\@example\.com/.test(currentEmail))
		{
			currentEmail = undefined;
		}

		var validEmail = currentEmail && self.validateEmail(currentEmail);

		var currentPassword = self.credentials.password;
		var validPassword = currentPassword && !/ExamplePass/i.test(currentPassword);

		var validCredentials = validEmail && validPassword;

		async.waterfall([
			function(cb)
			{
				if (!validCredentials)
				{
					return cb();
				}

				self.doLogin(
					{
						'email': encodeURIComponent(currentEmail)
					,	'password': currentPassword
					,	'account': self.credentials.account
					,	'vm': self.credentials.vm
					}
				,	function(err, res)
					{
						if (err)
						{
							var errMsg = '';

							errMsg += '  ERROR! LOGIN FAILED for ' + currentEmail;
							errMsg += '\n\n'
							errMsg += '  Try again or change it at:'
							errMsg += '\n'
							errMsg += '  ' + self.configurationFilePath;
							errMsg += '\n\n'
							errMsg += '  If you left the password entry as a blank field you will be asked\n'
							errMsg += '  everytime when you run the automation.'

							if (err.message && !/invalid email/i.test(err.message))
							{
								errMsg = err.message;
							}

							console.log("\n" + errMsg + "\n");
							process.exit(1);
						}
						else
						{
							validCredentials = true;
						}

						return cb();
					}
				);
			}

		,	function(cb)
			{
				if (validCredentials)
				{
					return cb();
				}

				self.promptCredentials(
					{ email: currentEmail }
				,	cb
				);
			}

		,	function(cb)
			{
				self.validations.credentials = true;
				done();
			}
		]);
	}


,	promptCredentials: function(params, done)
	{
		var self = this;

		if (typeof params === 'function')
		{
			done = params;
			params = {};
		}

		var currentEmail = params.email;
		var currentPassword = null;

		var validCredentials = false;

		var failedAttempts = 0;

		async.whilst(
			function() { return !validCredentials; }
		,	function(loopCb)
			{
				async.waterfall([
					function(waterfallCb)
					{
						if (currentEmail)
						{
							return waterfallCb();
						}

						inquirer.prompt(
							{
								type: 'input'
							,	name: 'email'
							,	message: 'Account email:'

							,	default: currentEmail

							,	validate: function(input)
								{
									return self.validateEmail(input) || 'Account email:';
								}
							}

						,	function(answers)
							{
								currentEmail = answers.email;
								self.configurationChanged = true;

								waterfallCb();
							}
						);
					}
				,	function(waterfallCb)
					{
						inquirer.prompt(
							{
								type: 'password'
							,	name: 'password'
							,	message: currentEmail + ' password:'

							,	validate: function(password)
								{
									var validateDone = this.async();
									password = password.trim();

									if (!password.length) { return validateDone(currentEmail + ' password:'); }

									self.doLogin(
										{
											'email': currentEmail
										,	'password': password
										,	'account': self.credentials.account
										}
									,	function(err, res)
										{
											if (!err)
											{
												return validateDone(true);
											}

											failedAttempts++;

											if (failedAttempts > 2)
											{
												console.log("\n\nFailed to login " + failedAttempts + " times.");
												console.log("Please re-check your Netsuite credentials.");
												console.log("");
												process.exit(1);
											}

											if (err.message && !/invalid email/i.test(err.message))
											{
												console.log("\n\n" + err.message);
												process.exit(1);
											}

											return validateDone('Login failed! Try again.');
										}
									);
								}
							}
						,	function(answers)
							{
								currentPassword = answers.password;
								validCredentials = true;
								loopCb()
							}
						);
					}
				]);
			}

		,	function(err)
			{
				self.credentials.email = currentEmail;
				self.credentials.password = currentPassword;

				done();
			}
		);
	}


,	validateEmail: function(email)
	{
		return typeof email === "string" && /[^\s]+\@[^\s]+/.test(email);
	}


,	doLogin: function(params, done)
	{
		var self = this;

		self.getUserAccountData(
			{
				'email'   : params.email
			,	'password': params.password
			,	'account' : params.account
			,	'vm': params.vm
			}
		,	function(err, userAccountData)
			{
				if (err)
				{
					done(err, null);
				}
				else
				{
					//console.log(userAccountData);
					self.credentials.roleId = userAccountData.role.internalId;
					self.account = userAccountData;
					self.account = _.extend(self.account, userAccountData.account);
					done(null, self.account);
				}
			}
		);
	}


,	getUserAccountData: function(params, done)
	{
		var self = this;
		var accountId = params.account;

		self.getUserAccounts(
			{
				'email': params.email
			,	'password': params.password
			,	'vm': params.vm
			}
		,	function(err, accounts)
			{
				if (Array.isArray(accounts))
				{
					var accountData = _(accounts).find(function(data)
					{
						return data.account.internalId === accountId;
					});

					if (!accountData)
					{
					 	return done('no_access_to_account', null);
					}

					return done(null, accountData);
				}

				return done(err, null);
			}
		);
	}


,	getUserAccounts: function(config, done)
	{
		config = config || this.credentials;

		var self = this;
		var requestUrl;

		if (config.molecule)
		{
			requestUrl = 'https://rest.'+ config.molecule + '.netsuite.com/rest/roles';
		}
		else if(config.vm)
		{
			requestUrl = config.vm + '/rest/roles';
		}
		else
		{
			requestUrl = 'https://rest.netsuite.com/rest/roles';
		}

		request.get(
			requestUrl
		,	{
				headers: {
					'Accept': '*/*'
				,	'Accept-Language': 'en-us'
				,	'Authorization': 'NLAuth nlauth_email=' + config.email + ', nlauth_signature=' + config.password
				}
			,	rejectUnauthorized: false
			}
		,	function(err, request, body)
			{
				if (err)
				{
					return done(err, null);
				}
				else
				{
					var response = JSON.parse(body);

					if (response.error)
					{
						return done(response.error, null);
					}
					else
					{
						return done(null, response);
					}
				}
			}
		)
	}


,	promptPersistConfiguration: function(done)
	{
		var self = this;

		console.log("  ");
		console.log("  Verified configuration:");
		console.log("  ");
		console.log("  Url     : " + self.website.baseUrl);
		console.log("  E-mail  : " + self.credentials.email);
		console.log("  Account : " + self.account.name + ' (id ' + self.account.internalId + ')');
		console.log("  Role    : " + self.account.role.name + ' (id ' + self.account.role.internalId + ')' );
		console.log("  ");
		console.log("  Configuration File:");
		console.log("  " + self.configurationFilePath);
		console.log("  ");


		inquirer.prompt(
			{
				type: 'confirm'
			,	name: 'saveConfiguration'
			,	message: 'Save configuration?'
			,	default: true
			}
		,	function(answers)
			{
				if (answers.saveConfiguration)
				{
					var dataset = self.getConfiguration();

					dataset = _(dataset).omit([
						'_comment'
					]);

					dataset.website =  _(dataset.website).omit([
						'siteSettings'
					]);

					dataset.credentials = _.clone(dataset.credentials);
					dataset.credentials.password = '';

					fs.writeFileSync(
						self.configurationFilePath
					,	JSON.stringify(dataset, null, 4)
					);
					// persist to a file
				}

				done();
			}
		);


	}


,	raiseIncompleteConfigurationFileError: function(configFilePath)
	{
		if (!configFilePath)
		{
			configFilePath = this.configurationFilePath;
		}

		var configFileName = path.basename(configFilePath);
		errorMessage  = "ERROR! File with example/incomplete data '" + configFileName + "'";
		errorMessage += "\n\nPlease set real values, remove '_comment' entry and try again."
		errorMessage += "\n\nFile path: " + configFilePath;
		console.log("\n" + errorMessage + "\n");
		process.exit(1);
	}


,	raiseNonExistentPathError: function(configFilePath)
	{
		var configFileName = path.basename(configFilePath);
		errorMessage  = "ERROR! Non existent configuration file '" + configFileName + "'";
		errorMessage += "\n\nExpected path: " + configFilePath;
		console.log("\n" + errorMessage + "\n");
		process.exit(1)
	}

,	raiseInvalidSiteError: function(websiteUrl)
	{
		error  = "\n  ERROR! The specified website is not supported by the automation framework.";
		error += "\n  Please check if the site is a supported SuiteCommerce site and try again.";
		error += "\n";
		error += "\n  Url: " + websiteUrl;
		//error += "\n  SiteId : " + self.website.siteId;
		//error += "\n  Account: " + self.credentials.account;

		console.error(error + "\n");
		process.exit(1);
	}

}

module.exports = Preconditions.Configuration;