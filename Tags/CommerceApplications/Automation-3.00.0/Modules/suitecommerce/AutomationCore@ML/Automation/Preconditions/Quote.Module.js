var suitetalk = require('suitetalk')
,	_ = require('underscore');

// NS ESTIMATE DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/schema/record/estimate.html
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/estimate.html

// @class Preconditions.Quote
Preconditions.Quote = {

	formatQuoteItemList: function (itemList)
	{
		itemList = itemList[0].item;

		itemList = _.map(itemList, function(wrappedItem)
		{
			wrappedItem = _.clone(wrappedItem);

			var itemEntry = wrappedItem.item[0];

			wrappedItem.internalId = itemEntry.internalId;
			wrappedItem.name = itemEntry.name;

			var taxCodeEntry = wrappedItem.taxCode[0];
			wrappedItem.taxCode = taxCodeEntry.internalId;
			wrappedItem.taxCodeName = taxCodeEntry.name;

			var priceEntry = wrappedItem.price[0];
			wrappedItem.priceLevel = priceEntry.internalId;
			wrappedItem.priceLevelName = priceEntry.name;
			delete wrappedItem.price;

			if (_.isArray(wrappedItem.options))
			{
				//console.log(JSON.stringify(wrappedItem.options, null, 4));

				var opts = wrappedItem.options[0].customField;

				wrappedItem.options = _.map(opts, function(optEntry)
				{
					return {
						'optionInternalId': optEntry.$.internalId
					,	'optionScriptId': optEntry.$.scriptId
					,	'valueInternalId': optEntry.value[0].internalId
					,	'valueName': optEntry.value[0].name
					}
				});
			}

			return wrappedItem;
		});

		return itemList;
	}

,	formatQuoteFields: function (record)
	{
		var self = this;

		record.entityStatusInternalId = record.entityStatus[0].internalId;
		record.entityStatusName = record.entityStatus[0].name.toString();
		record.total = '$' + record.total.toFixed(2);
		record.subTotal = '$' + record.subTotal.toFixed(2);

		record.shippingCost = record.shippingCost ? '$' + record.shippingCost : '$0.00';

		record.taxTotal === 0 ?
			record.taxTotal = '$0.00' :
			record.taxTotal = '$' + record.taxTotal.toString();

		record.tranId = record.tranId.toString();

		record.message = record.message ? record.message.replace(/<br>/g, '\n') : '';

		var rawShippingAddress = record.shippingAddress[0];
		record.shippingAddress = Preconditions.Address.wrapAddress(rawShippingAddress);
		delete record.shippingAddress.$;

		var rawBillingAddress = record.billingAddress[0];
		record.billingAddress = Preconditions.Address.wrapAddress(rawBillingAddress);
		delete record.billingAddress.$;

		var dateFields = [
			'dueDate', 'tranDate', 'createdDate', 'lastModifiedDate', 'expectedCloseDate'
		]

		dateFields.forEach(function (fieldName)
		{
			record[fieldName] = new Date(record[fieldName]).toISOString().substring(0,10);
		})

		record.itemList = self.formatQuoteItemList(record.itemList);

		return record;
	}

,	getQuotesByCustomer: function (customer, credentials, cb)
	{
		var wrappedParams = {
			customer: {
				internalId: customer.internalId
			}
		}

		this.searchQuotes(wrappedParams, credentials, cb);
	}

,	searchQuotes: function (params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		var searchParams = {
			recordType: 'transaction'
		}

		params.type = '_estimate';

		searchParams.filters = Preconditions.Helpers.generateSearchFilters(
			{
				'entityStatus'   : 'recordRef'
			,	'internalId'     : 'recordRef'
			}
		,	params
		);

		if (params.customer)
		{
			var customerFilters = Preconditions.Helpers.generateSearchFilters(
			{
				'internalId' : 'recordRef'
			,	'email'      : 'string'
			}
			,	params.customer
			);

			searchParams.joins = [{
				recordType: 'customer'
			,	filters: customerFilters
			}];
		}

		suitetalk.search(
			searchParams
		)

		.then(function (response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			var records = response.records;

			records = _(records).map(self.formatQuoteFields, self);

			//console.log(records);
			cb(null, records);
		})
		.catch(function (error)
		{
			console.log(error);
			cb(error);
		});
	}

,	changeQuoteStatus: function (params, credentials, cb)
	{
		var quote = params.quote || params.estimate;
		var entityStatus = params.status || params.entityStatus;

		suitetalk.setCredentials(credentials);

		suitetalk.update({

			recordType: 'estimate'
		,	recordNamespace: 'nstransactions'
		,	internalId: quote.internalId
		,	fields: [
				{
					name: 'entityStatus'
				,	nstype: 'RecordRef'
				,	type: 'customerStatus'
				,	internalId: entityStatus
				}
			]
		})
		.then(function (response)
		{
			//var records = response.searchResponse[0].searchResult[0].recordList[0].record;
			//console.log(JSON.stringify(response, null, 4))
			cb(null, response);
		})
		.catch(function (error)
		{
			cb(error, null);
		});
	}

	// @method updateQuoteAddress Updates the quote address
	// @param {Automation.Preconditions.Quote.Address} quoteInfo
	// @param {Automation.Preconditions.Credentials} credentials
	// @param {Function} cb
	// @return {internalId:Number}
,	updateQuoteAddress: function (quoteInfo, credentials, cb)
	{
		var self = this;

		quoteInfo.address = Preconditions.Address.unwrapAddress(quoteInfo.address);
		quoteInfo.address.addrPhone = quoteInfo.address.addrPhone || quoteInfo.address.phone;

		var addressFields = [];
		var valueFieldNames = [
			'attention', 'addressee', 'addr1', 'addr2', 'addr3'
		,	'city', 'state', 'zip', 'addrPhone', 'override'
		];

		valueFieldNames.forEach(function (fieldName)
		{
			if (typeof quoteInfo.address[fieldName] !== "undefined" && quoteInfo.address[fieldName] !== null)
			{
				addressFields.push({
					name: fieldName
				,	value: quoteInfo.address[fieldName] + ''
				,	namespace: 'nscommon'
				});
			}
		});

		addressFields.push({
			name: 'country'
		,	value: Preconditions.Address.getCountryConstant(quoteInfo.address.country)
		,	nstype: 'Country'
		,	namespace: 'nscommon'
		});

		var quote = quoteInfo.quote || quoteInfo.estimate;

		suitetalk.setCredentials(credentials);
		suitetalk.update({
			recordType: 'estimate'
		,	recordNamespace: 'nstransactions'
		,	internalId: quote.internalId
		,	fields: [
				{
					name: quoteInfo.addressType
				,	namespace: 'nstransactions'
				,	nstype: 'Address'
				,	nstypeNamespace: 'nscommon'
				,	fields: addressFields
				}
			]
		})
		.then(function (response)
		{
			var record = response.updateResponse[0].writeResponse[0].baseRef[0].$;
			//console.log(JSON.stringify(response, null, 4))
			//console.log(record);
			cb(null, record);
		})
		.catch(function (error)
		{
			cb(error, null);
		});
	}

,	createQuote: function (params, credentials, cb)
	{
		this.createQuotes(
			[params]
		,	credentials
		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, res[0]);
				}
			}
		);
	}

,	createQuotes: function (params, credentials, cb)
	{
		var self = this;
		var records, recordDefaults;

		if (Array.isArray(params))
		{
			records = params;
			recordDefaults = {};
		}
		else
		{
			records = params.quotes || params.records;
			recordDefaults = params;
		}

		var formattedRecords = _(records).map(function(record)
		{
			record = _(record).defaults(recordDefaults);

			var formattedRecord = {
				recordType: 'estimate'
			,	recordNamespace: 'nstransactions'
			,	fields: self.formatAddRequestFields(record)
			};

			return formattedRecord;
		});

		suitetalk.setCredentials(credentials);

		suitetalk.addList({
			recordType: 'estimate'
		,	recordNamespace: 'nstransactions'
		,	records: formattedRecords

		}).then(function (response)
		{
			var responseData = response.addListResponse[0].writeResponseList[0];
			//TODO: check errors
			//console.log(JSON.stringify(responseData, null, 4));

			if (responseData.status[0].$.isSuccess === 'true')
			{
				var results = [];

				responseData.writeResponse.forEach(function (responseDataEntry)
				{
					//console.log(results);
					//console.log(JSON.stringify(responseDataEntry,null,4));
					results.push({
						internalId: responseDataEntry.baseRef[0].$.internalId
					});
				})

				cb(null, results);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function (err)
		{
			console.log(err);
			cb(err, null);
		})
	}

,	formatAddRequestFields: function (params)
	{
		var fields = [];

		params.entity = params.entity || params.customer;
		params.taxItem = params.taxItem || params.tax || params.salesTaxItem || Preconditions.Tax.getNullTax();
		params.entityStatus = params.entityStatus || '10'

		params.salesRep = _.isObject(params.salesRep) && params.salesRep.internalId ? params.salesRep.internalId : params.salesRep;

		var recordRefFieldNames = [
			'entity', 'taxItem', 'entityStatus', 'salesRep', 'promoCode',
			'discountItem', 'messageSel', 'shipMethod'
		]

		recordRefFieldNames.forEach(function (fieldName)
		{
			if (typeof(params[fieldName]) !== 'undefined') {
				var recordRefVal = params[fieldName];

				if (recordRefVal && recordRefVal.internalId)
				{
					recordRefVal = recordRefVal.internalId;
				}

				fields.push({
					name: fieldName
				,	internalId: recordRefVal + ''
				,	nstype: 'RecordRef'
				});
			}
		});

		var valueFieldNames = [
			'tranId', 'tranDate', 'dueDate', 'probability'
		];

		valueFieldNames.forEach(function (fieldName)
		{
			if (typeof params[fieldName] !== 'undefined' && params[fieldName] !== null)
			{
				fields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				//,	namespace: 'nscommon'
				});
			}
		});

		var wrapped_item_list = [];

		params.items = (Array.isArray(params.items)) ? params.items : [params.items];

		params.items.forEach(function (item)
		{
			if (item.isMatrix && !_(item.matrixChilds).isEmpty())
			{
				item = _.sample(item.matrixChilds);
			}

			wrapped_item_list.push({
				name: 'item'
			,	nstype: 'EstimateItem'
			,	fields: [
					{
						name: 'item'
					,	internalId: item.internalId
					,	nstype: 'RecordRef'
					}
				]
			});
		});

		fields.push({
			name: 'itemList'
		,	nstype: 'EstimateItemList'
		,	fields: wrapped_item_list
		});

		return fields;
	}

,	getStatusOptions: function ()
	{
		return this.statusOptions;
	}

,	statusOptions: {
		ClosedLost: '14'
	,	Qualified:  '7'
	,	InDiscussion:  '8'
	,	IdentifiedDecisionMakers: '9'
	,	Proposal: '10'
	,	InNegotiation: '11'
	,	Purchasing: '12'
	}
}