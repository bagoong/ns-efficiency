// @module AutomationCore
var _ = require('underscore');

// @automationTask get_three_transaction_inventory_items Returns three inventory items in an array, if they are matrix, it returns a child
// @return {Array<Automation.Item.Model>} The returned valued will be an array containing only 3 elements.
Preconditions.add('get_three_transaction_inventory_items', ['configuration', 'get_inventory_items'], function (configuration, items, cb)
{
	var	items = _.sample(items, 3)
	,	returnedItems = [];
	_.each(items, function (item, index)
	{
		if (item.isMatrix === true)
		{
			returnedItems.push(_.sample(item.matrixChilds));
		}
		else
		{
			returnedItems.push(item);
		}
	});

	cb(null, returnedItems)
});