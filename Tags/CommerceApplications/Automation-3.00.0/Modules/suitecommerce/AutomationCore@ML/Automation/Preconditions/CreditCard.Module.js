
var	suitetalk = require('suitetalk')
,	_ = require('underscore');


Preconditions.CreditCard = {

	getAvailableCreditCards: function (credentials, cb)
	{

		/*
		var availableCreditCards = {};
		var siteSettings = Preconditions.Configuration.website.siteSettings;

		if (Array.isArray(siteSettings.paymentmethods))
		{
			siteSettings.paymentmethods.forEach(function(paymentMethod)
			{
				var name = null;

				if (/visa/i.test(paymentMethod.name))
				{
					name = "visa";
				}
				else if (/master/i)
				{
					name = "mastercard";
				}

				if (name)
				{
					availableCreditCards[name] = paymentMethod.internalid;
				}
			});
		}
		*/
		Preconditions.PaymentMethod.getAvailableCreditCards(credentials, function(err, result)
		{
			cb(err, result);
		})
	}


,	createCustomerCreditCard: function (params, credentials, cb)
	{
		this.createCustomerCreditCards(params, credentials, cb);
	}

,	createCustomerCreditCards: function (params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		var customer = params.customer;
		var creditCards = params.creditcard || params.creditCard || params.creditCards;

		creditCards = Array.isArray(creditCards) ? creditCards : [creditCards];

		var creditCardRecords = creditCards.map(function (cCard) 
		{
			return self.getCustomerCreditCardFieldset(cCard)
		})

		var fields = [
			{
				name: 'creditCardsList'
			,	nstype: 'CustomerCreditCardsList'
			,	replaceAll: params.replaceAll || false
			,	fields: creditCardRecords
			}
		];

		suitetalk.update({
			recordType: 'customer'
		,	internalId: customer.internalId
		//,	recordNamespace: 'nscommon'
		,	fields: fields

		}).then(function(response)
		{
			var responseData = response.updateResponse[0].writeResponse[0];
			//TODO: check errors
			//console.log(JSON.stringify(responseData, null, 4));

			if (responseData.status[0].$.isSuccess === "true")
			{
				//console.log(responseData.baseRef[0].$["internalId"]);
				var customer = {
					internalId: responseData.baseRef[0].$.internalId
				}

				cb(null, customer);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function(err)
		{
			console.log(err);
			cb(err, null);
		})
	}


,	getCustomerCreditCardFieldset: function (params)
	{
		var self = this;

		params = self.unwrapCreditCard(params);

		var creditCardFields = [];

		/*
		<ns6:ccNumber xsi:type="xsd:string">****************</ns6:ccNumber>
		<ns6:ccExpireDate xsi:type="xsd:dateTime">2018-08-01T03:00:00.000Z</ns6:ccExpireDate>
		<ns6:ccName xsi:type="xsd:string">Don Gregorio</ns6:ccName>
		<ns6:paymentMethod xsi:type="ns7:RecordRef" internalId="8" xmlns:ns7="urn:core_2014_2.platform.webservices.netsuite.com"/>
		<ns6:ccDefault xsi:type="xsd:boolean">true</ns6:ccDefault>
		*/

		var valueFieldNames = [
			'ccNumber', 'ccExpireDate', 'ccName', 'ccDefault'
		];

		valueFieldNames.forEach(function(fieldName)
		{
			if (typeof params[fieldName] !== "undefined" && params[fieldName] !== null)
			{
				creditCardFields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				//,	namespace: 'nscommon'
				});
			}
		});

		creditCardFields.push({
			name: "paymentMethod"
		,	nstype: "RecordRef"
		,	internalId: params.paymentMethod + ''
		});


		var creditCardFieldset = {
			name: 'creditCards'
		,	nstype: 'CustomerCreditCards'
		,	fields: creditCardFields
		};

		return creditCardFieldset;
	}

,	unwrapCreditCard: function (creditCard)
	{
		//console.log(creditCard);
		/*
			'id': '123'
		,	'cardTypeId': '8'
		,	'cardType': 'visa'
		,	'cardNumber': '4111111111111111'
		,	'cardEnding': '1111'
		,	'expMonth': '7'
		,	'expYear': '2018'
		,	'securityNumber': '123'
		,	'name': 'Flash Gordon'

		,	'defaultShipping': false
		,	'defaultBilling' : false
		*/
		creditCard = _.clone(creditCard);

		var fieldAliases = {
			'cardNumber': 'ccNumber'
		,	'name': 'ccName'
		,	'cardTypeId': 'paymentMethod'
		,	'defaultCreditCard': 'ccDefault'
		}

		//console.log(fieldAliases);

		Object.keys(fieldAliases).forEach(function(fieldName)
		{
			if (creditCard[fieldName])
			{
				var newFieldName = fieldAliases[fieldName];
				creditCard[newFieldName] = creditCard[fieldName];
				delete creditCard[fieldName];
			}

		});

		if (creditCard.expMonth && creditCard.expYear)
		{
			// We add a day different than 1 because if the date is the first day of the month at 0:00:00, the backend makes it a day earlier, making it a month before
			var expDateString = creditCard.expYear + '-' + creditCard.expMonth + '-10';
			creditCard.ccExpireDate = new Date(expDateString).toISOString();
		}

		//console.log(creditCard);

		return creditCard;
	}

}
