var	_ = require('underscore');


Preconditions.add('generate_one_customer_registration_data', function(cb)
{
	var customer = {
		'email': new Date().getTime() + '@automationbacon.com'
	,	'firstName': 'Roberto'
	,	'lastName': new Date().getTime() + 'Bolanos'
	,	'company': 'Queen'
	,	'password': 'a1s2d3q4'
	,	'passwordConfirmation': 'a1s2d3q4'
	}

	cb(null, customer);
});


Preconditions.add('create_one_customer', ['configuration', 'generate_one_customer_registration_data'], function(configuration, customer, cb)
{
	Preconditions.Customer.createCustomer(
		customer
	,	Preconditions.Configuration.credentials
	,	function(err, res)
		{
			if (err)
			{
				cb(err, null)
			}
			else
			{
				customer.internalId = res.internalId
				cb(null, customer);
			}

		}
	);
});


Preconditions.add(
	'add_default_shipping_address_to_customer'
,	[
		'create_one_customer'
	,	'generate_one_shipping_address_data'
	]
,	function(customer, shippingAddress, cb)
	{
		// defaultShipping workaround for expects
		var address = _.clone(shippingAddress);
		address.defaultShipping = true;

		Preconditions.Address.createCustomerAddress(
			{
				'customer': customer
			,	'replaceAll': false
			,	'address' : address
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, shippingAddress)
				}
			}
		);
	}
);


Preconditions.add(
	'add_default_billing_address_to_customer'
,	[
		'create_one_customer'
	,	'generate_one_billing_address_data'
	]
,	function(customer, billingAddress, cb)
	{
		// defaultBilling workaround for expects
		var address = _.clone(billingAddress);
		address.defaultBilling = true;

		Preconditions.Address.createCustomerAddress(
			{
				'customer': customer
			,	'replaceAll': false
			,	'address' : address
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, billingAddress)
				}
			}
		);
	}
);

Preconditions.add(
	'scis_add_default_shipping_address_to_customer'
,	[
		'create_one_customer'
	,	'scis_generate_one_shipping_address_data'
	]
,	function(customer, shippingAddress, cb)
	{
		// defaultShipping workaround for expects
		var address = _.clone(shippingAddress);
		address.defaultShipping = true;

		Preconditions.Address.createCustomerAddress(
			{
				'customer': customer
			,	'replaceAll': false
			,	'address' : address
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, shippingAddress)
				}
			}
		);
	}
);


Preconditions.add(
	'scis_add_default_billing_address_to_customer'
,	[
		'create_one_customer'
	,	'scis_generate_one_billing_address_data'
	]
,	function(customer, billingAddress, cb)
	{
		// defaultBilling workaround for expects
		var address = _.clone(billingAddress);
		address.defaultBilling = true;

		Preconditions.Address.createCustomerAddress(
			{
				'customer': customer
			,	'replaceAll': false
			,	'address' : address
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, billingAddress)
				}
			}
		);
	}
);

Preconditions.add(
	'add_default_credit_card_to_customer'
,	[
		'create_one_customer'
	,	'generate_one_visa_credit_card_data'
	]
,	function(customer, creditCard, cb)
	{
		creditCard.ccDefault = true;

		Preconditions.CreditCard.createCustomerCreditCard(
			{
				'customer': customer
			,	'replaceAll': false
			,	'creditCard' : creditCard
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, creditCard)
				}
			}
		);
	}
);


Preconditions.add(
	'create_one_full_customer'
,	[
		'create_one_customer'
	,	'add_default_shipping_address_to_customer'
	,	'add_default_billing_address_to_customer'
	,	'add_default_credit_card_to_customer'
	]
,	function (
		customerRegisterData
	,	shippingAddress
	,	billingAddress
	,	creditCard
	,	cb
	)
	{
		//customer.defaultBillingAddress = billingAddress;
		//customer.defaultShippingAddress = shippingAddress;
		//customer.defaultCreditCard = creditCard;

		Preconditions.Customer.getCustomerByEmail(
				customerRegisterData.email
			,	Preconditions.Configuration.credentials
			,	function (err, customer)
				{
					customer.defaultCreditCard = creditCard;
					customer.password = customerRegisterData.password;

					cb(null, customer);
				}
		);
	}
);

Preconditions.add(
	'scis_create_one_full_customer'
,	[
		'create_one_customer'
	,	'scis_add_default_shipping_address_to_customer'
	,	'scis_add_default_billing_address_to_customer'
	,	'add_default_credit_card_to_customer'
	]
,	function (
		customerRegisterData
	,	shippingAddress
	,	billingAddress
	,	creditCard
	,	cb
	)
	{
		//customer.defaultBillingAddress = billingAddress;
		//customer.defaultShippingAddress = shippingAddress;
		//customer.defaultCreditCard = creditCard;

		Preconditions.Customer.getCustomerByEmail(
				customerRegisterData.email
			,	Preconditions.Configuration.credentials
			,	function (err, customer)
				{
					customer.defaultCreditCard = creditCard;
					customer.password = customerRegisterData.password;

					cb(null, customer);
				}
		);
	}
);

Preconditions.add(
	'create_one_full_customer_with_multiple_credit_card'
,	[
		'create_one_customer'
	,	'add_default_shipping_address_to_customer'
	,	'add_default_billing_address_to_customer'
	,	'add_multiple_credit_card_to_customer'
	]
,	function (
		customerRegisterData
	,	shippingAddress
	,	billingAddress
	,	creditCards
	,	cb
	)
	{

		Preconditions.Customer.getCustomerByEmail(
				customerRegisterData.email
			,	Preconditions.Configuration.credentials
			,	function (err, customer)
				{
					customer.password = customerRegisterData.password;

					cb(null, customer);
				}
		);
	}
);

Preconditions.add(
	'add_multiple_credit_card_to_customer'
,	[
		'create_one_customer'
	,	'generate_one_visa_credit_card_data'
	,	'generate_one_discover_credit_card_data'
	,	'generate_one_master_credit_card_data'
	]
,	function(customer, visaCreditCard, discoverCreditCard, masterCreditCard, cb)
	{
		visaCreditCard.ccDefault = true;

		var creditCards = [
					visaCreditCard
				,	discoverCreditCard
				,	masterCreditCard
				]

		Preconditions.CreditCard.createCustomerCreditCards(
			{
				'customer': customer
			,	'replaceAll': false
			,	'creditCards' : creditCards
			}

		,	Preconditions.Configuration.credentials

		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, creditCards)
				}
			}
		);
	}
);