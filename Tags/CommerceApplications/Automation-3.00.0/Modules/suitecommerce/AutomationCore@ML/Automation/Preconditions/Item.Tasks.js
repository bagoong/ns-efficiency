var _ = require('underscore');

// ANY INVENTORY ITEM (SIMPLE, MATRIX, ETC)

Preconditions.add('get_inventory_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "inventoryItem"
	,	pendingOnEmptyResult: 'Site has not inventory items'
	}, cb);
});

Preconditions.add('get_one_inventory_item', ['get_inventory_items'], function (simpleItems, cb)
{
	cb(null, _.sample(simpleItems));
});


// ONLY NON-MATRIX INVENTORY

Preconditions.add('get_inventory_non_matrix_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "inventoryItem"
	,	matrix: false
	,	pendingOnEmptyResult: 'Site has not inventory non matrix items'
	}, cb);
});



// ONLY MATRIX INVENTORY

Preconditions.add('get_inventory_matrix_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "inventoryItem"
	,	matrix: true
	,	pendingOnEmptyResult: 'Site has not inventory non matrix items'
	});
});


Preconditions.add('get_one_inventory_non_matrix_item', ['get_inventory_non_matrix_items'], function (simpleItems, cb)
{
	cb(null, _.sample(simpleItems));
});

Preconditions.add('get_one_item_multiple_upc_codes', ['get_inventory_non_matrix_items'], function (simpleItems, cb)
{
	var items = _.filter(simpleItems, function (item)
	{
		return item.customFieldList 
			&& item.customFieldList[0].customField 
			&& item.customFieldList[0].customField[0].$.scriptId === "custitem_ns_pos_additional_upcs"; 
	});
	var item = _.sample(items);
	item.multipleUpcCodes = item.customFieldList[0].customField[0].value;

	cb(null, item);
});


Preconditions.add('get_one_inventory_non_matrix_item_non_min_quantity', ['get_inventory_non_matrix_items'], function (simpleItems, cb)
{
	var items = _.filter(simpleItems, function (item)
	{
		return !(item.minimumQuantity);
	});
	cb(null, _.sample(items));
});

// ANY MATRIX ITEM

Preconditions.add('get_matrix_options_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		matrix: true
	,	pendingOnEmptyResult: 'Site has not matrix items'
	}, cb);
});

Preconditions.add('get_one_matrix_child_item', ['get_one_matrix_options_item'], function (item, cb)
{
	if (item.matrixChilds)
	{
		cb(null,_.sample(item.matrixChilds));
	}
	else
	{
		console.log("ERROR matrix child returned", item);
		cb(null,item);
	}

});




Preconditions.add('get_one_matrix_options_item', ['get_matrix_options_items'], function (itemsWithMatrixOptions, cb)
{
	cb(null, _.sample(itemsWithMatrixOptions));
});


Preconditions.add('get_two_dim_matrix_options_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		matrix: true
	,	matrixDimension: 2
	,	pendingOnEmptyResult: 'Site has not two dimension matrix items'
	}, cb);
});

Preconditions.add('get_two_dim_matrix_options_item', ['get_two_dim_matrix_options_items'], function (itemsWithMatrixOptions, cb)
{
	cb(null, _.sample(itemsWithMatrixOptions));
});

Preconditions.add('get_one_dim_matrix_options_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		matrix: true
	,	matrixDimension: 1
	,	pendingOnEmptyResult: 'Site has not two dimension matrix items'
	}, cb);
});

Preconditions.add('get_one_dim_matrix_options_item', ['get_one_dim_matrix_options_items'], function (itemsWithMatrixOptions, cb)
{
	cb(null, _.sample(itemsWithMatrixOptions));
});

Preconditions.add('get_non_inventory_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "nonInventoryItem"
	,	pendingOnEmptyResult: 'Site has not inventory items'
	}, cb);
});

Preconditions.add('get_one_non_inventory_item', ['get_non_inventory_items'], function (items, cb)
{
	cb(null, _.sample(items));
});



// ANY KIT ITEM

Preconditions.add('get_kit_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "kit"
	}, cb);
});


Preconditions.add('get_one_kit_item', ['get_kit_items'], function (items, cb)
{
	cb(null, _.sample(items));
});


// ANY SERVICE ITEM

Preconditions.add('get_service_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "service"
	}, cb);
});



Preconditions.add('get_one_service_item', ['get_service_items'], function (items, cb)
{
	cb(null, _.sample(items));
});



// ANY DOWNLOAD ITEM

Preconditions.add('get_download_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "downloadItem"
	,	pendingOnEmptyResult: 'Site has not download items'
	}, cb);
});



Preconditions.add('get_one_download_item', ['get_download_items'], function (items, cb)
{
	cb(null, _.sample(items));
});


// ANY GIFT CERTIFICATE ITEM

Preconditions.add('get_gift_certificate_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "giftCertificateItem"
	,	pendingOnEmptyResult: 'Site has not gift certificate items'
	}, cb);
});



Preconditions.add('get_one_gift_certificate_item', ['get_gift_certificate_items'], function (items, cb)
{
	cb(null, _.sample(items));
});


// ANY ASSEMBLY CERTIFICATE ITEM

Preconditions.add('get_assembly_items', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		type: "assembly"
	,	pendingOnEmptyResult: 'Site has not assembly items'
	}, cb);
});



Preconditions.add('get_one_assembly_item', ['get_assembly_items'], function (items, cb)
{
	cb(null, _.sample(items));
});



Preconditions.add('get_items_with_url_component', ['configuration'], function (configuration, cb)
{
	Preconditions.Item.searchItems({
		urlComponent: {
			value: ''
		,	operator: 'notEmpty'
		}
	,	type: "inventoryItem"
	,	matrix: false
	,	pendingOnEmptyResult: 'Site has not items with urlComponent defined'
	}, cb);
});


Preconditions.add('get_inventory_items_out_of_stock', ['get_inventory_items'], function (items, cb)
{
	var item = _.filter(items, function(item) { return item.stockOnlineTotal === 0; });

	if (!item) return pending('Site has not inventory items out of stock');

	cb(null, item);
});


Preconditions.add('get_one_inventory_item_out_of_stock', ['get_inventory_items_out_of_stock'], function (items, cb)
{
	cb(null, _.sample(items));
});


Preconditions.add('get_inventory_items_minimum_quantity', ['get_inventory_items'], function (items, cb)
{
	Preconditions.Item.searchItems({
		type: "inventoryItem"
	,	minimumQuantity: { 'value': 1, 'operator': 'greaterThan'}
	,	pendingOnEmptyResult: 'Site has not inventory items with minimum quantity'
	}, cb);
});


Preconditions.add('get_one_inventory_item_minimum_quantity', ['get_inventory_items_minimum_quantity'], function (items, cb)
{
	cb(null, _.sample(items));
});


Preconditions.add('get_inventory_items_not_matrix', ['configuration'], function (items, cb)
{
	Preconditions.Item.searchItems({
		type: "inventoryItem"
	,	matrix: false
	,	pendingOnEmptyResult: 'Site has not inventory non matrix items'
	}, cb);
});


Preconditions.add('get_one_inventory_item_not_matrix', ['get_inventory_items_not_matrix'], function (items, cb)
{
	cb(null, _.sample(items));
});


Preconditions.add('get_matrix_child_items', ['configuration'], function (items, cb)
{
	Preconditions.Item.searchItems({
		matrixChild: true
	,	pendingOnEmptyResult: 'Site has not matrix child items'
	}, cb);
});


Preconditions.add('get_one_matrix_child_item', ['get_matrix_child_items'], function (items, cb)
{
	cb(null, _.sample(items));
});


Preconditions.add('get_matrix_parent_items', ['configuration'], function (items, cb)
{
	Preconditions.Item.searchItems({
		matrix: true
	,	matrixChild: false
	,	pendingOnEmptyResult: 'Site has not matrix items'
	}, cb);
});


Preconditions.add('get_one_matrix_parent_item', ['get_matrix_parent_items'], function (items, cb)
{
	cb(null, _.sample(items));
});