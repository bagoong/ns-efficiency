var Preconditions = require('preconditions')
,	suitetalk = require('suitetalk')
,	path = require('path')
,	_ = require('underscore')
,	fs = require('fs')
,	args = require('yargs').argv
;

Preconditions.SCIS = {

	initializeAdminUserConfiguration: function()
	{
		return {
			'company': '4410096'
		,	'siteN': '2'
		,	'role': 3
		,	'user': 'kiwi@auto.com'
		,	'password': 'amargo'
		,	'devid': ''
		,	'devname': ''
		};
	}

,	initializeEmployeeUserConfiguration: function()
	{
		return {
			'company': '4410096'
		,	'siteN': '2'
		,	'role': 3
		,	'user': 'admin@auto.com'
		,	'password': 'amargo'
		,	'devid': ''
		,	'devname': ''
		};
	}	

,	initializeClerkUserConfiguration: function()
	{
		return {
			'company': '4410096'
		,	'siteN': '2'
		,	'role': 1000
		,	'user': 'clerk@auto.com'
		,	'password': 'amargo'
		,	'devid': '1'
		,	'devname': '1'
		};	
	}

,	initializeCustomerConfiguration: function()
	{
		return {
			'id': '35'
		,	'name': 'kiwi sample'
		,	'internalId': '35'
		,	'email': 'kiwi@testing.com'
		,   'addressId': '28'
		,   'shipMethodId': '152'
		};	
	}	

,	getAccountsTemplate: function()
	{
		var self = this;

		return {
			'clerk': self.initializeClerkUserConfiguration()
		,	'admin': self.initializeAdminUserConfiguration()
		,	'employee': self.initializeEmployeeUserConfiguration()
		,	'customer': self.initializeCustomerConfiguration()
		};
	}		

};


Preconditions.add('scis_configuration', ['configuration'], function (configuration, cb)
{	
	Preconditions.Configuration.addSection(
		'scisAccounts'
	,	Preconditions.SCIS.getAccountsTemplate()
	);

	Preconditions.Configuration.addSection(
		'globals'
	,	{
			SINGLE_INSTANCE_BROWSER: true
		}
	);

	cb(null, Preconditions.Configuration.getConfiguration());
});



Preconditions.add('scis_location_configuration', ['configuration', 'get_subsidiary'], function (configuration, subsidiary, cb)
{	
	Preconditions.Configuration.subsidiary = subsidiary;
	Preconditions.Configuration.subsidiaryAsCustomerLocation = true;
	cb(null, Preconditions.Configuration.getConfiguration());
});

