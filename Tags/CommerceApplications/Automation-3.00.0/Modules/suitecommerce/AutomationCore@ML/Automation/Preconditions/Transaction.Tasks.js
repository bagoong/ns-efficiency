var _ = require("underscore")

Preconditions.add(
		'create_one_sales_order_pending_approval'
	,	['configuration', 'create_one_full_customer', 'get_one_service_item']
	,	function (configuration, customer, serviceItem, cb)
	{

	var subsidiary = configuration.website.subsidiary || null;

	Preconditions.Transaction.createTransaction
	(
		{ 
			customer: customer
		,	recordType: 'salesOrder'
		,	shipMethod: null
		,	items: [ serviceItem ]
		,	orderStatus: '_pendingApproval' 
		,	location: Preconditions.Configuration.website.subsidiary 
		}
		,	cb
	);
});