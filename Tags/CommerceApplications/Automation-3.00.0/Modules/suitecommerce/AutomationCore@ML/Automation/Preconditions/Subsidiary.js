var Preconditions = require('preconditions')
,	suitetalk = require('suitetalk')
,	path = require('path')
,	_ = require("underscore")
;

/*
	SuiteTalk Schema Browser docs for Item:
	https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/subsidiary.html
*/

Preconditions.Subsidiary = {

	searchSubsidiaries: function(params, cb)
	{
		var self = this;

		var credentials = params.credentials || Preconditions.Configuration.credentials;

		expect(credentials).toBeDefined("Credentials not defined");

		suitetalk.setCredentials(credentials);

		var filters = Preconditions.Helpers.generateSearchFilters(
			{
				'internalId' : 'recordRef'
			}
		,	params
		);

		//console.log(JSON.stringify(filters, null, 4));
		//locationTotalValue //locationQuantityAvailable  //quantityAvailable

		return suitetalk.searchBasic({
			recordType: 'subsidiary'
		,	filters: filters
		})
		.then(function(response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			cb(null, response.records);
		})
		.catch(function(error)
		{
			console.log(error.stack);
			cb(error);
		});
	}

}


Preconditions.add('get_subsidiary', ['configuration'], function(configuration, cb)
{
	var subsidiaryId = Preconditions.Configuration.website.subsidiary;
	if (subsidiaryId)
	{
		Preconditions.Subsidiary.searchSubsidiaries({
			internalId: subsidiaryId
		}, function(err, res)
		{
			var s = res[0];

			s.mainAddress = Preconditions.Address.wrapAddress(s.mainAddress[0]);
			delete s.mainAddress.$;

			cb(err, s);
		});
	}
	else
	{
		cb();
	}
});