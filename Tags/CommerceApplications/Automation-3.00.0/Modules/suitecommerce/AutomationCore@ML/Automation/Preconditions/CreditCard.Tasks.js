var	_ = require('underscore');


Preconditions.add('generate_one_visa_credit_card_data', ['configuration'], function(configuration, cb)
{
	Preconditions.CreditCard.getAvailableCreditCards(
		configuration.credentials
	,	function(err, availableCreditCards)
		{
			var creditCard = _.find(availableCreditCards, function(creditCard)
			{
				return /visa/i.test(creditCard.name);
			});

			var creditCardVisaId = creditCard.internalId;
			var error = null;
		
			if (creditCardVisaId)
			{
				var creditCard = {
					'id': '123'
				,	'cardTypeId': creditCardVisaId
				,	'cardType': 'visa'
				,	'cardNumber': '4111111111111111'
				,	'cardEnding': '1111'
				,	'expMonth': '10'
				,	'expYear': '2018'
				,	'securityNumber': '123'
				,	'name': 'Flash Gordon'
				};
			}
			else
			{
				error = 'Visa credit card not found on website';
			}

			cb(error, creditCard);
		}
	);	
});


Preconditions.add(
	'generate_one_default_visa_credit_card_data'
,	['configuration', 'generate_one_visa_credit_card_data']
,	function(configuration, creditCard, cb)
	{
		creditCard.ccDefault = true;
		cb(null, creditCard);
	}
);


Preconditions.add(
	'generate_one_non_default_visa_credit_card_data'
,	['configuration', 'generate_one_visa_credit_card_data']
,	function(configuration, creditCard, cb)
	{
		creditCard.ccDefault = false;
		cb(null, creditCard);
	}
);

Preconditions.add('generate_one_discover_credit_card_data', ['configuration'], function(configuration, cb)
{
	Preconditions.CreditCard.getAvailableCreditCards(
		configuration.credentials
	,	function(err, availableCreditCards)
		{
			var creditCard = _.find(availableCreditCards, function(creditCard)
			{
				return /discover/i.test(creditCard.name);
			});

			var creditCardDiscoverId = creditCard.internalId;
			var error = null;
		
			if (creditCardDiscoverId)
			{
				var creditCard = {
					'id': '123'
				,	'cardTypeId': creditCardDiscoverId
				,	'cardType': 'discover'
				,	'cardNumber': '6011111111111117'
				,	'cardEnding': '1117'
				,	'expMonth': '10'
				,	'expYear': '2022'
				,	'securityNumber': '123'
				,	'name': 'Flash Gordon'
				};
			}
			else
			{
				error = 'Discover credit card not found on website';
			}

			cb(error, creditCard);
		}
	);	
});

Preconditions.add(
	'generate_one_default_discover_credit_card_data'
,	['configuration', 'generate_one_discover_credit_card_data']
,	function(configuration, creditCard, cb)
	{
		creditCard.ccDefault = true;
		cb(null, creditCard);
	}
);

Preconditions.add(
	'generate_one_non_default_discover_credit_card_data'
,	['configuration', 'generate_one_discover_credit_card_data']
,	function(configuration, creditCard, cb)
	{
		creditCard.ccDefault = false;
		cb(null, creditCard);
	}
);

Preconditions.add('generate_one_master_credit_card_data', ['configuration'], function(configuration, cb)
{
	Preconditions.CreditCard.getAvailableCreditCards(
		configuration.credentials
	,	function(err, availableCreditCards)
		{
			var creditCard = _.find(availableCreditCards, function(creditCard)
			{
				return /Master\s*Card/i.test(creditCard.name);
			});

			var creditCardMasterCardId = creditCard.internalId;
			var error = null;
		
			if (creditCardMasterCardId)
			{
				var creditCard = {
					'id': '123'
				,	'cardTypeId': creditCardMasterCardId
				,	'cardType': 'Master Card'
				,	'cardNumber': '5555555555554444'
				,	'cardEnding': '4444'
				,	'expMonth': '11'
				,	'expYear': '2022'
				,	'securityNumber': '123'
				,	'name': 'Flash Gordon'
				};
			}
			else
			{
				error = 'MasterCard credit card not found on website';
			}

			cb(error, creditCard);
		}
	);	
});