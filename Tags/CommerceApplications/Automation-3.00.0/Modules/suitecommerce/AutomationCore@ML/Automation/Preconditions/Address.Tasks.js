
var _ = require('underscore');


Preconditions.add('generate_one_shipping_address_data', function(cb)
{	
	var address = {
		'fullname': 'My Shipping Name'
	,	'company': 'My Shipping Company'
	,	'addr1': 'Mings Street 123'
	,	'addr2': ''
	,	'city': 'Mongo City'
	,	'country': 'United States'
	,	'state': 'New York'
	,	'zip': 'J1934'
	,	'phone': '12345678901234'
	};

	cb(null, address);
});


Preconditions.add('generate_one_new_shipping_address_data', function(cb)
{
	var address = {
		"fullname": "My New Shipping"
	,	"company": "My New Shipping Company"
	,	"addr1": "New Street 123"
	,	"addr2": ""
	,	"city": "New Mocha City"
	,	"country": "United States"
	,	"state": "New York"
	,	"zip": "J1934"
	,	"phone": "1234901234"
	};

	cb(null, address);
});


Preconditions.add('generate_one_billing_address_data', function(cb)
{
	var address = {
		'fullname': 'My Billing Name'
	,	'company': 'My Billing Company'
	,	'addr1': 'Mindafggs Street 123'
	,	'addr2': ''
	,	'city': 'Billing City'
	,	'country': 'United States'
	,	'state': 'New York'
	,	'zip': 'J1934'
	,	'phone': '1234901234'
	};

	cb(null, address);
});


Preconditions.add('generate_one_new_billing_address_data', function(cb)
{
	var address = {
		"fullname": "My New Billing"
	,	"company": "My New Billing Company"
	,	"addr1": "Minasdafggs Street 123"
	,	"addr2": ""
	,	"city": "Mogdfgerwngo City"
	,	"country": "United States"
	,	"state": "New York"
	,	"zip": "J1934"
	,	"phone": "1234901234"
	};

	cb(null, address);
});

//SCIS Addresses generation

Preconditions.add('scis_generate_one_shipping_address_data', ['scis_location_configuration'], function (configuration, cb)
{	
	var address = {};
	if (Preconditions.Configuration.subsidiaryAsCustomerLocation)
	{
		address = Preconditions.Configuration.subsidiary.mainAddress
		delete address.internalId;
		delete address.company;
	}
	else
	{
		address = {
			'fullname': 'My Shipping Name'
		,	'company': 'My Shipping Company'
		,	'addr1': 'Mings Street 123'
		,	'addr2': ''
		,	'city': 'Mongo City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '12345678901234'
		};
	}

	cb(null, address);
});


Preconditions.add('scis_generate_one_new_shipping_address_data', ['scis_location_configuration'], function (configuration,cb)
{
	var address = {};
	
	if (Preconditions.Configuration.subsidiaryAsCustomerLocation)
	{
		address = Preconditions.Configuration.subsidiary.mainAddress
		delete address.internalId;
		delete address.company;
	}
	else
	{
		address = {
			"fullname": "My New Shipping"
		,	"company": "My New Shipping Company"
		,	"addr1": "New Street 123"
		,	"addr2": ""
		,	"city": "New Mocha City"
		,	"country": "United States"
		,	"state": "New York"
		,	"zip": "J1934"
		,	"phone": "1234901234"
		};
	}

	cb(null, address);
});


Preconditions.add('scis_generate_one_billing_address_data', ['scis_location_configuration'], function (configuration, cb)
{
	var address = {};

	if (Preconditions.Configuration.subsidiaryAsCustomerLocation)
	{
		address = Preconditions.Configuration.subsidiary.mainAddress
		delete address.internalId;
		delete address.company;
	}
	else
	{
		address = {
			'fullname': 'My Billing Name'
		,	'company': 'My Billing Company'
		,	'addr1': 'Mindafggs Street 123'
		,	'addr2': ''
		,	'city': 'Billing City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '1234901234'
		};
	}

	cb(null, address);
});


Preconditions.add('scis_generate_one_new_billing_address_data', ['scis_location_configuration'], function (configuration, cb)
{
	var address = {};

	if (Preconditions.Configuration.subsidiaryAsCustomerLocation)
	{
		address = Preconditions.Configuration.subsidiary.mainAddress
		delete address.internalId;
		delete address.company;
	}
	else
	{
		address = {
			"fullname": "My New Billing"
		,	"company": "My New Billing Company"
		,	"addr1": "Minasdafggs Street 123"
		,	"addr2": ""
		,	"city": "Mogdfgerwngo City"
		,	"country": "United States"
		,	"state": "New York"
		,	"zip": "J1934"
		,	"phone": "1234901234"
		};
	}	

	cb(null, address);
});
