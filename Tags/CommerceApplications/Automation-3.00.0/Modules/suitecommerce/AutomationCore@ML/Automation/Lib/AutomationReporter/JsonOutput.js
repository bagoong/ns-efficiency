
var	path = require('path')
,	fs = require('fs')
,	_ = require('underscore')
,	Handlebars = require('handlebars')
;


var JsonOutput = function (outputFilePath)
{
	var self = this;

	outputFilePath = /\.json?$/i.test(outputFilePath) ? outputFilePath : outputFilePath + '.json';
	self.outputFilePath = outputFilePath;


	self.saveResults = function(automationReporterResults)
	{
		var results = automationReporterResults;

		var overview = _.map(results.specs, function(specData)
		{
			return _.omit(specData, [
				"passedExpects", "failedExpects"
			]);
		});

		var details = _.map(results.specs, function(specData)
		{
			return _.pick(specData, [
				"description", "location", "failedExpects", "passedExpects"
			]);
		});

		var organizedResults = {
			suite: results.suite
		,	summary: results.summary
		,	overview: overview
		,	details: details
		}

		fs.writeFileSync(self.outputFilePath, JSON.stringify(organizedResults, null, 4));
	}
}


module.exports = JsonOutput;