registerHelper('utilities',
{
	// @module automation @class Client @method generateRandomString
	// @param {Number} length @return {String}
	generateRandomString: function(length, cb)
	{
		var client = this;

	    this.call(function()
	    {
	    	var text = client.util.generateRandomString(length);
	    	cb(null, text)
	    })
	}

	// @method getTextAsFloat
,	getTextAsFloat: function(selector, cb)
	{
		var client = this;

		this
			.getText(selector, function(err, text)
			{
				result = client.util.textToFloat(text);

				client.call(function()
				{
					cb(err, result);
				});
			})
		;
	}

	//@method clog
,	clog: function()
	{
		var params = Array.prototype.slice.call(arguments,0, arguments.length - 1)
		,	cb = arguments[arguments.length - 1];

		this.call(function ()
		{
			console.log(params);

		})
		.call(cb);
	}

});
