var _ = require('underscore');

registerHelper('generic',
{
	// @module automation @class Client @method waitForRedirect
	waitForRedirect: function (cb)
	{
		var client = this
		,	url = null;
		var interval = setInterval(function()
		{
			client.url(function(err, new_url)
			{
				if (client.isEnded || (url !== null && url !== new_url.value))
				{
					clearInterval(interval);
					return client.call(cb);
				}

				url = new_url.value;
			});

		}, 50)
	}

	// @method waitForAjax 
	//@param {Number?} timeout
	//@param {Function} cb
,	waitForAjax: function(timeout, cb)
	{
		var client = this;

		if (_.isFunction(timeout))
		{
			cb = timeout;
			timeout = 20000;
		}

		if (this.JS_DISABLED_MODE === true)
		{
			return client.call(cb);
		}

		var execution_interval = 250;
		// var timeout_limit = 20000;
		var timeout_count = 0;

		var interval = setInterval(function()
		{
			client.execute(
				function() {
					if (typeof jQuery === 'function')
					{
						return jQuery.active;
					}
					// FALLBACK IF JQUERY IS NOT LOADED IN THE PAGE
					return 0;
				}
			,	function(err, result)
				{
					timeout_count += execution_interval;

					on_timeout_limit = timeout_count > timeout;

					if (on_timeout_limit || client.isEnded || (result !== null && result.value === 0))
					{
						clearInterval(interval);
						return client.pause(500).call(cb);
					}
				}
			);

		}, execution_interval);
	}


,	forEachWebdriverId: function(selector, cb) {
		var client = this;

		this
			.elements(selector, function(err, found_elements)
			{
				found_elements.value.forEach(function(element)
				{
					var element_id = element.ELEMENT;

					client.call(function()
					{
						cb(element_id);
					});
				})
			})
		;
	}


,	setCheckboxValue: function(params, cb)
	{
		var client = this;
		this
			.waitFor(params.selector, 10000)
			.isSelected(params.selector, function(err, check_value)
			{
				if((params.check && !check_value)||(!params.check && check_value))
				{
					client
						.click(params.selector)
					;
				}
			})
			.call(cb)
		;
	}

,	radioButtonClick: function(selector, cb)
	{
		// THIS METHOD IS A WORKARROUND FOR SELENIUM FOCUS ISSUE WITH RADIOS
		this
			.scroll(selector)
			// IF DOUBLE CLICK IS NOT WORKING, TRY THIS:
			//.execute( function(selector) { jQuery(selector).click(); }, selector )
			.click(selector)
			.click(selector)
			.call(cb)
		;
	}

,	safeClick: function(options, cb)
	{
		var client = this;

		client.isExisting(options.selector, function(err, exists)
		{
			if(exists)
			{
				client.click(options.selector, cb);
			}
			else
			{
				expect(options.selector).toBe('present in the page');
				options.callback();
			}
		});
	}
});
