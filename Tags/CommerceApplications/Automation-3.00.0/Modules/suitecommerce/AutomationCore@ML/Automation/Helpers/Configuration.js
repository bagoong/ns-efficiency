defineHelper('Configuration', 
{
	isSiteBuilderBasic: function (cb) 
	{
		var isBasic = false;
		
		this
			.execute(function() {
				return SCM['SC.MyAccount.Configuration'].isBasic;
			}, function (err, result) 
			{
				isBasic = result.value === true;
				cb(err, isBasic);
			})
		;
	}
});