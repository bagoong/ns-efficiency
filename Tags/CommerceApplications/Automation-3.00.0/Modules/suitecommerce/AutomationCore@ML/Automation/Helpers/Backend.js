defineHelper('Backend', 
{
	fullBackendLogin: function(params, cb)
	{
		this
			.Backend.goToLogin()
			.Backend.loginToBackend(params.admin)
			.Backend.secretQuestion(params.questions)
			.call(cb)
		;
	}
	
,	goToLogin: function(cb)
	{
		var client = this;
		this
			.waitForAjax()
			.isExisting(Selectors.AutomationCore.backendGoToLogin, function(err, existing){
				client
					.click(Selectors.AutomationCore.backendGoToLogin)
					.waitForAjax()
				;
			})

			.call(cb)
		;
	}

,	loginToBackend: function(params, cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendEmail, 5000)
			.setValue(Selectors.AutomationCore.backendEmail, params.user)
			.setValue(Selectors.AutomationCore.backendPassword, params.password)
			.click(Selectors.AutomationCore.backendSubmit)
			.waitForAjax()
			.call(cb)
		;
	}

,	secretQuestion: function(params, cb)
	{
		var client = this;
		this
			.getText(Selectors.AutomationCore.backendText, function(err, text){
				for (i = 0; i < text.length; i++) {
					if(text[i] == 'Question' && text[i+1])
					{
						params.forEach(function(param){
							if(text[i+1].match(param.keyword)){
								client
									.setValue(Selectors.AutomationCore.backendAnswer, param.answer)
									.click(Selectors.AutomationCore.backendSubmiter)
									.waitForAjax()
									.call(cb)
								;
							};
						});
					}
				}			
			})					
		;
	}

,	openOrder: function(params, cb)
	{
		this
			.url('https://system.netsuite.com/app/accounting/transactions/salesord.nl?id=' + params.orderId + '&whence=')
			.waitForAjax()
			.call(cb)
		;
	}	

,	approveOrder: function(cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendApprove, 5000)
			.click(Selectors.AutomationCore.backendApprove)
			.waitForAjax()
			.call(cb)
		;
	}	

,	fullfillOrder: function(cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendProcess, 5000)	
			.click(Selectors.AutomationCore.backendProcess)
			.waitForAjax()
			// PAUSE IS NECESARY FOR SOME CASES WHEN THE BACK END IS SLOW
			.pause(2000)
			.waitForExist(Selectors.AutomationCore.backendLocation, 5000)	
			.click(Selectors.AutomationCore.backendLocation)
			.waitForAjax()
			.waitForExist(Selectors.AutomationCore.backendLocationOpt, 5000)
			.click(Selectors.AutomationCore.backendLocationOpt)
			.waitForAjax()
			.waitForExist(Selectors.AutomationCore.backendSave, 5000)
			.click(Selectors.AutomationCore.backendSave)
			// PAUSE IS NECESARY FOR SOME CASES WHEN THE BACK END IS SLOW
			.pause(2000)
			.alertAccept()
			.waitForAjax()
			.call(cb)
		;
	}

,	markPacked: function(cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendPacked, 5000)	
			.click(Selectors.AutomationCore.backendPacked)
			.waitForAjax()
			.call(cb)
		;
	}

,	markShipped: function(cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendShipped, 5000)	
			.click(Selectors.AutomationCore.backendShipped)
			.waitForAjax()
			.call(cb)
		;
	}

,	bill: function(cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendBill, 5000)	
			.click(Selectors.AutomationCore.backendBill)
			.waitForAjax()
			.waitForExist(Selectors.AutomationCore.backendSave, 5000)
			// PAUSE IS NECESARY FOR SOME CASES WHEN THE BACK END IS SLOW
			.pause(2000)
			.click(Selectors.AutomationCore.backendSave)
			// PAUSE IS NECESARY FOR SOME CASES WHEN THE BACK END IS SLOW
			.pause(2000)
			.alertAccept()
			.waitForAjax()
			.call(cb)
		;
	}

,	takeInvoiceName: function(params, cb)
	{
		this
			.waitForExist(Selectors.AutomationCore.backendTitleId, 5000)
			.getText(Selectors.AutomationCore.backendTitleId, function(err, text){
				params.invoiceName = text;
			})
			.call(cb)
		;
	}			

,	clickInCheckBox: function(options, cb)
	{
		var client = this;

		this
			.waitForExist(options.selectorToCheck, 5000)
			.isExisting(options.selectorToCheck, function(err, exists)
			{
				if (exists)
				{
					client
						.isSelected(options.selectorToCheck, function(err, isSelected) {
								//If the elemenent is unchecked
								if (isSelected === false) {
									//If you want to CHECK this
									if (options.check)
									{
										client.click(options.selectorToClicking);
										console.log(options.selectorToCheck + ' was checked');

									}
									else
									{
										console.log(options.selectorToCheck + ' had been previously unchecked');
									}											
								} else {
									//If you want to UNCHECK this
									if (!options.check)
									{
										client.click(options.selectorToClicking);
										console.log(options.selectorToCheck + ' was unchecked');
									}
									else
									{
										console.log(options.selectorToCheck + ' had been previously checked');
									}
								}
						})
						.call(cb)
					;
				}
				else 
				{
					console.log(options.selectorToCheck + ' element is not present in this page');
				}
			});
	}

,	confirmPopup: function(options, cb)
	{
		var client = this
		,	main
		,	popup;

		this
			.waitForAjax()
			.pause(2000)
			.windowHandles(function(err, res) {
				var res = res || {}
				,	countWindows = res.value && res.value.length ? res.value.length: 0;

				if (countWindows > 1)
				{
					main = res.value[0];
					popup = res.value[countWindows - 1];
					console.log('main: ' + main);
					console.log('pop: ' + popup);
					client
						.pause(2000)
						.window(popup)
						.switchTab(popup)
						.waitForExist(Selectors.PosApplication.agreeButton, 5000)
						.click(Selectors.PosApplication.agreeButton)
						.pause(2000)
						.window(main)
						.switchTab(main)
						.pause(10000)
						.call(cb)
					;
				} else 
				{
					client.call(cb)
				}
			})
		;
	}
});