/*
	IMPORTANT, PLEASE INSTALL THE FOLLOWING PACKAGES THAT AREN'T IN 
	THE GULP DISTRO SETUP:
	
	npm install request
	npm install tough-cookie

*/

'use strict';

var request = require('request');
var async = require('async');


var Checkout = function(params)
{
	['siteNumber', 'siteLocation', 'accountNumber'].forEach(function(val)
	{
		if (typeof params[val] === 'undefined') {
			throw val + " param is required to initialize account requests";
		}
	});

	this.siteNumber = params.siteNumber;
	this.siteLocation = params.siteLocation;
	this.accountNumber = params.accountNumber;

	if (params.rawUrl) {
		this.baseUrl = params.rawUrl;
	} else {
		this.baseUrl = 'https://checkout.netsuite.com/c.' + this.siteNumber + '/' + this.siteLocation;
	}

	this.request = request.defaults(
	{
		jar: true
	});
}

Checkout.prototype.url = function(rel_path)
{
	var url = this.baseUrl;

	url += rel_path.replace(/#[^\/]+$/, '');
	url += (/\?/.test(rel_path))? '&' : '?';

	url += 'c=' + this.accountNumber + '&n=' + this.siteNumber;

	//console.log(url);

	return url;
}

Checkout.prototype.addToCart = function(params, cb) {
	var self = this;
	var done = cb;

	var options = {
		uri: self.url('/services/LiveOrder.Line.Service.ss')
	,	method: 'POST'
	,	json: {"item":{"internalid":params.productId},"quantity":params.productAmount,"options":null,"splitquantity":null}

	} 

	self.request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Item Added");
			//console.log(response);

			if (typeof done === 'function')
			{
				done(error, response, body);
			}
		}
	});	

}


Checkout.prototype.checkoutOrder = function(params, cb) {
	var self = this;
	var done = cb;

	var options = {
		uri: self.url('/services/LiveOrder.Service.ss')
		, method: 'POST'
		, json: {
		    "paymentmethods": [
		        params.paymentMethod // THIS IS AN ARRAY
		    ],
		    "internalid": null,
		    "latest_addition": null,
		    "promocode": null,
		    "ismultishipto": false,
		    "shipmethod": params.shipMethod, // THIS IS THE INTERNAL ID, EXAMPLE: 40
		    "billaddress": params.billAddress, // THIS IS THE INTERNAL ID, EXAMPLE: 3796
		    "shipaddress": params.shipAddress, // THIS IS THE INTERNAL ID, EXAMPLE: 3796
		    "isPaypalComplete": false,
		    "agreetermcondition": true,
		    "options": {},
		    "sameAs": true
		}

	};
	
	self.request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Order created");
			if (typeof done === 'function')
			{
				params.orderId = response.body.confirmation.internalid;
				done(error, response, body);
			}
		}
	});
}

var exports = module.exports = function(params)
{
	return new Checkout(params);
}

