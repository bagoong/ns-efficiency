var _ = require('underscore');


GLOBAL.onJasmineSpecContext = function()
{
	try
	{
		return _.isFunction(expect) && _.isObject(expect());
	}
	catch (noJasmineSpecError)
	{
		return false;
	}
}

var onJasmineContext = function()
{
	return _.isFunction(expect);
}


var isSeleniumServerDown = function(e)
{
	var serverDown = false;

	if (_.isObject(e) && e.message)
	{
		serverDown = /ECONNREFUSED|connect to selenium server/i.test(e.message);
	}

	return serverDown;
}


var getErrorString = function(e)
{
	var errorString = "";

	if (_.isObject(e) && e.message)
	{
		errorString += '\n\n---------------------------------------------';
		errorString += '\n Error message:';
		errorString += '\n---------------------------------------------';
		errorString += '\n' + e.message

		if (e.stack)
		{
			errorString += '\n\n---------------------------------------------';
			errorString += '\n Stack trace:';
			errorString += '\n---------------------------------------------';
			errorString += '\n\n' + e.stack;
			errorString += '\n';
		}
	}
	else
	{
		errorString = e + '';
	}

	return errorString;
}


GLOBAL.dumpError = function(e)
{
	console.error(getErrorString(e));
}


process.on('uncaughtException', function(e) {
	if (isSeleniumServerDown(e))
	{
		console.log("\n============================================");
		console.log("\n    ERROR! Selenium server is not running.");
		console.log("\n    Please open another CMD and run:");
		console.log("\n      webdriver-manager start");
		console.log("\n    Please keep it open while running tests.");
		console.log("\n============================================");
		console.log("");
		process.exit(1);
	}

	if (onJasmineSpecContext())
	{
		expect(e).addExpectationResult(false, {
			matcherName: '',
			passed: false,
			expected: '',
			actual: '',
			message: 'Exception thrown: ' + e,
			error: e && e.message ? e : null
		});

		jasmineSpecDone();
	}
	else
	{
		dumpError(e);
		process.exit(1);
	}
});
