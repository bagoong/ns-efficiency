

jasmine.generateFormInputValues = function (dataset)
{
	var generatedTestDatasets = []
	,	validCombination = ''
	,	dummie = ''
	,	data = dataset.item.options;

	for (var i = 0; i < data.length; i++)
	{
		validCombination += data[i].required ? '1': '0';
		dummie += '0';
	}

	var count = Math.pow(2, validCombination.length);

	for (var i = 0; i < count; i++)
	{
		var bin = i.toString(2)
		,	fullBin = dummie.slice(String(bin).length) + bin
		,	valid = true
		,	testData = JSON.parse(JSON.stringify(dataset));

		testData.item.inputs = [];
		testData.item.options = [];

		for (var j = 0; j < fullBin.length; j++)
		{
			if (~~fullBin[j])
			{
				var option = {};

				option.label = data[j].label;
				option.dataCartOptionId = data[j].dataCartOptionId;
				option.value = data[j].value;
				testData.item.inputs.push(data[j].label);

				testData.item.options.push(option);
			}
		}

		for (var j = 0; j < validCombination.length && valid; j++)
		{
			if (~~validCombination[j])
			{
				valid = valid && (~~fullBin[j] && ~~validCombination[j])
			}
		}

		testData.item.valid = !!valid;
		generatedTestDatasets.push(testData);
	}

	return generatedTestDatasets;
}
