/* jshint node: true */
'use strict';

var	webdriverio = require('webdriverio')
,	FirefoxProfile = require('firefox-profile')
,	args = require('yargs').argv
,	_ = require('underscore')
;

var testSuite = _.chain(GLOBAL)
	.result('AutomationEnvironment')
	.result('suite')
	.value() || {};


GLOBAL.WEBDRIVER_BROWSER = args.browser || testSuite.browser || 'chrome';

// @module automation @class Browser


var initBrowserOptions = function()
{
	var options = {};

	if (args.host || testSuite.host)
	{
		options.host = args.host || testSuite.host;
	}

	if (args.port || testSuite.port)
	{
		options.port = args.port || testSuite.port;
		options.port = parseInt(options.port);
	}

	return options;
}


var setBrowserResolution = function(client)
{
	var resolution = GLOBAL.WEBDRIVER_RESOLUTION;

	if (resolution)
	{
		client.setViewportSize(
			GLOBAL.WEBDRIVER_RESOLUTION
		);

		GLOBAL.WEBDRIVER_RESOLUTION = resolution;

		console.log("  Resolution: " + resolution.width + "x" + resolution.height);
	}
	else
	{
		client.windowHandleMaximize();
	}
}


GLOBAL.getBrowserInstall = function(browser_name)
{
	switch (browser_name)
	{
		case 'ie':
		case 'ie8':
		case 'ie9':
		case 'ie10':
		case 'ie11':
		case 'internetexplorer':
		case 'internet explorer':
			return function(jasmine_context, done)
			{
				// 'internet explorer' is the only valid ie string
				browser_name = 'internet explorer';
				installBrowser({browserName : browser_name}, jasmine_context, done);
			}

		case 'safari':
			return installSafariBrowser;

		case 'phantom':
		case 'phantomjs':
			return installPhantomjsBrowser;

		case 'firefox':
			return installFirefoxBrowser;

		case 'android':
			return installAndroidProfile;

		case 'pagegenerator':
		case 'disablejs':
			return installDisabledJsBrowser;

		case 'ios':
			return installIOSProfile;

		case 'chrome':
		default:
			return installChromeBrowser;
	}

}


GLOBAL.installBrowser = function(params, jasmine_context, done)
{
	// INITIALIZE WITH HOST AND PORT ARGS IF DEFINED
	var options = initBrowserOptions();

	options.desiredCapabilities = {
			browserName: params.browserName
		,	javascriptEnabled: true
		//,	nativeEvents: false
		,	"ie.ensureCleanSession": true
		,	acceptSslCerts: true
	};

	var client = jasmine_context.client = webdriverio.remote(options);

	client
		.init()
		.call(function()
		{
	setBrowserResolution(client);
		})
		.call(done)
	;
}


GLOBAL.installSafariBrowser = function(jasmine_context, done)
{
	installBrowser({browserName : 'safari'}, jasmine_context, done);
}


GLOBAL.installChromeBrowser = function(jasmine_context, done)
{
	var options = initBrowserOptions();

	options.desiredCapabilities = {
		browserName: 'chrome'
	,	javascriptEnabled: true
	,	chromeOptions: {
			args: [
				// FIX CROSS DOMAIN REQUEST AJAX ERROR
				'disable-web-security'
			,	'allow-running-insecure-content'
			//,	'user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3'
			//,	'enable-strict-powerful-feature-restrictions'
			]
		,	prefs: {
				//Set option 1 to enable geolocation and option 2 to disable
        		"profile.default_content_setting_values.geolocation": AutomationEnvironment.browserAllowGeoLocation ? 1 : 2
      		}
		}
	};

	var client = jasmine_context.client = webdriverio.remote(options);

	client
		.init()
		.call(function()
		{
	setBrowserResolution(client);
		})
		.call(done)
	;
}


GLOBAL.installPhantomjsBrowser = function(jasmine_context, done)
{
	var options = initBrowserOptions();

	options.desiredCapabilities = {
		browserName: 'phantomjs'
	,	javascriptEnabled: true
	,	acceptSslCerts: true
	,	'phantomjs.cli.args': [
			'--ignore-ssl-errors=true'
		,	'--web-security=false'
		]
	}

	var client = jasmine_context.client = webdriverio.remote(options);

	client
		.init()
	// REQUIRED FOR PHANTOMJS, IF IT IS NOT SET, RESOLUTION IS TOO SMALL
		.windowHandleSize(BROWSER_RESOLUTIONS.desktop)
		.call(function()
		{
	setBrowserResolution(client);
		})
		.call(done)
	;
}


GLOBAL.installFirefoxBrowser = function(jasmine_context, done)
{
	var fp = new FirefoxProfile();

	fp.setPreference('webdriver_enable_native_events', false);
	fp.setPreference('security.fileuri.strict_origin_policy', false);
	fp.setPreference('accept_untrusted_certs', true);
	fp.setPreference("geo.prompt.testing", true);
    fp.setPreference("geo.prompt.testing.allow", AutomationEnvironment.browserAllowGeoLocation); //Set false to disable geolocation

	fp.encoded(function(prof)
	{
		var options = initBrowserOptions();

		options.desiredCapabilities = {
			browserName: 'firefox'
		//,	nativeEvents: false
		,	acceptSslCerts: true
		,	firefox_profile: prof
		};

		var client = jasmine_context.client = webdriverio.remote(options);

	client
		.init()
		.call(function()
		{
		setBrowserResolution(client);
		})
		.call(done)
	;
	});
}


// @method installDisabledJsBrowser Close the current webdriverio client and opens one that will have
// JavaScript disabled and install it to passed jasmine context so the rest of the text code uses that client.
// The default implementation uses firefox (via firefox-profile node module)
// @param jasmine_context @param {Function} done
GLOBAL.installDisabledJsBrowser = function(jasmine_context, done)
{
	var ffclient;

	var fp = new FirefoxProfile();

	fp.setPreference('security.fileuri.strict_origin_policy', false);
	fp.setPreference('accept_untrusted_certs', true);
	fp.setPreference('javascript.enabled', false);

	fp.updatePreferences();

	//console.log(fp);

	fp.encoded(function(prof)
	{
		var options = initBrowserOptions();

		options.desiredCapabilities = {
			browserName: 'firefox'
		,	javascriptEnabled: false
		//,	nativeEvents: false
		,	acceptSslCerts: true
		,	firefox_profile: prof
		};

		var ffclient = jasmine_context.client = webdriverio.remote(options);

		ffclient.JS_DISABLED_MODE = true;

		ffclient
			.init()
			.call(function()
			{
				setBrowserResolution(ffclient);
			})
			.url("about:config")
			.keys("\uE007")
			.keys("javascript.enabled")
			.pause(1000)
			.keys("\uE004")
			.keys("\uE007")
			.pause(2000)
			.call(done)
		;
	});
};


// @method installAndroidProfile Close the current webdriverio client and opens one into Android device.
// @param jasmine_context @param {Function} done
GLOBAL.installAndroidProfile = function(jasmine_context, done)
{
	GLOBAL.IGNORE_DEFAULT_BROWSER = false;

	if(jasmine_context.client)
	{
		jasmine_context.client.end();
	}

	var androidClient;

	androidClient = webdriverio.remote(GLOBAL.Capabilities.GalaxyS6);

	androidClient.JS_DISABLED_MODE = false;

	androidClient.init().then(function()
	{
	jasmine_context.client = androidClient;

	// INSTALL FIRST LEVEL, FOR GENERAL USE CORE COMMANDS
	installComponents(androidClient, "util");
	installCoreCommands(androidClient, "generic", "utilities");

	done();
	});
};


// @method installIOSProfile Close the current webdriverio client and opens one into IOS device.
// @param jasmine_context @param {Function} done
GLOBAL.installIOSProfile = function(jasmine_context, done)
{
	GLOBAL.IGNORE_DEFAULT_BROWSER = false;

	if(jasmine_context.client)
	{
		jasmine_context.client.end();
	}

	var IOSClient;

	IOSClient = webdriverio.remote(GLOBAL.Capabilities.IOSSimulator);

	IOSClient.JS_DISABLED_MODE = false;

	IOSClient.init().then(function()
	{
		jasmine_context.client = IOSClient;

		// INSTALL FIRST LEVEL, FOR GENERAL USE CORE COMMANDS
		installComponents(IOSClient, "util");
		installCoreCommands(IOSClient, "generic", "utilities");

		done();
	});
};