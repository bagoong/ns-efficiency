var	path = require('path')
,	fs = require('fs')
,	_ = require('underscore')
,	args = require('yargs').argv
,	fsUtil = require('../Lib/FsUtil')
;

var statusId = args['test-run'] || args["testrun"] || args['execution-pid'] || process.pid;
var statusFilePath = './automation/execution/status.' + statusId + '.json';

var executionStatus = fsUtil.readJsonFileSync(statusFilePath);

if (!executionStatus)
{
	executionStatus = {
		specs: []
	}
}


GLOBAL.AutomationEnvironment = _.extend(GLOBAL.AutomationEnvironment, {
	executionStatusFilePath: statusFilePath
,	executionStatus: executionStatus
,	currentSpec: null
,	currentDescribe: null
,	browserAllowGeoLocation: true
});


GLOBAL.isCurrentSpecExecutable = function()
{
	var previousExecution = GLOBAL.getCurrentSpecPreviousExecutionData();
	var finishedStates = ["passed", "pending"];
	var hasFinishedPreviousExecution = previousExecution && finishedStates.indexOf(previousExecution.status) !== -1;
	return !previousExecution || !hasFinishedPreviousExecution;
}


GLOBAL.getCurrentSpecPreviousExecutionData = function()
{
	var currentSpec = GLOBAL.AutomationEnvironment.currentSpec;

	if (!currentSpec || !executionStatus) { return null; }

	return _.find(executionStatus.specs, function(specEntry)
	{
		return !specEntry.specDone && (specEntry.description === currentSpec.fullName);
	});
}


jasmine.getEnv().addReporter({

	jasmineStarted: function()
	{
		executionStatus.specs.forEach(function(specData)
		{
			specData.specDone = false;
		});
	}

,	suiteStarted: function(describeData)
	{
		GLOBAL.AutomationEnvironment.currentDescribe = describeData;
	}

,	suiteDone: function(describeData)
	{
		GLOBAL.AutomationEnvironment.currentDescribe = describeData;
	}

,	specStarted: function(specData)
	{
		GLOBAL.AutomationEnvironment.currentSpec = specData;
	}

,	specDone: function(specData)
	{
		var currentSpecStatus = getCurrentSpecPreviousExecutionData();

		if (currentSpecStatus)
		{
			currentSpecStatus.specDone = true;
		}
		else
		{
			specData.specDone = true;
			executionStatus.specs.push(specData);
		}

		var currentSpecLocation = _.find(GLOBAL.AutomationEnvironment.specLocations, function(specLocationEntry)
		{
			return !specLocationEntry.specDone && (specLocationEntry.fullName === specData.fullName);
		});

		if (currentSpecLocation)
		{
			currentSpecLocation.specDone = true;
			specData.location = currentSpecLocation.location;
		}

		GLOBAL.AutomationEnvironment.currentSpec = specData;
	}
});
