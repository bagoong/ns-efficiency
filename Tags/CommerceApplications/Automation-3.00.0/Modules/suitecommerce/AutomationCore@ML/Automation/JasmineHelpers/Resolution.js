var	args = require('yargs').argv
,	_ = require('underscore')
;

var testSuite = _.chain(GLOBAL)
	.result('AutomationEnvironment')
	.result('suite')
	.value() || {};


GLOBAL.BROWSER_RESOLUTIONS = {
	desktop: { width: 1024, height: 768 }
,	tablet:  { width: 768,  height: 1024 }
,	mobile:  { width: 370,  height: 480 } // 370px FIXES CHROME SMALL WINDOW SIZE ISSUE WITH 320/360px
}


var resolution_argv = args.resolution || testSuite.resolution;

if (resolution_argv)
{
	var resolution = BROWSER_RESOLUTIONS.desktop

	var custom_resolution_regex = /(\d{3,4})\s*x\s*(\d{3,4})/gi;
	var custom_resolution_match = custom_resolution_regex.exec(resolution_argv);

	if (custom_resolution_match)
	{
		resolution = {
			width: parseInt(custom_resolution_match[1])
		,	height: parseInt(custom_resolution_match[2])
		}
	}
	else if (resolution_argv in BROWSER_RESOLUTIONS)
	{
		GLOBAL.WEBDRIVER_RESOLUTION_KEYNAME = resolution_argv;
		resolution = BROWSER_RESOLUTIONS[resolution_argv];
	}

	GLOBAL.WEBDRIVER_RESOLUTION = resolution;
}