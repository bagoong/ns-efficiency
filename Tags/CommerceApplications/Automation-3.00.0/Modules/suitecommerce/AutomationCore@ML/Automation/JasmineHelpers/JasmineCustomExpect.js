// INSTALL JASMINE EXTRA EXPECTS

require('jasmine-expect');


// OTHER JASMINE CUSTOM EXPECTS DEFINED BY AUTOMATION TEAM

beforeEach(function ()
{
	jasmine.addMatchers(
	{
		// @module automation @class JasmineMatchers
		// @method toHaveEqualFields @return {Boolean}
		toHaveEqualFields: function(util, customEqualityTesters)
		{
			return {
				compare: function(actual, expected)
				{
					var result = {};

					var differences = []
					var pass = false;

					if (Object.prototype.toString.call(actual) === '[object Object]')
					{
						Object.keys(expected).forEach(function(field) {

							// WE CONVERT ALL NUMBERS TO STRING TO DO A SOFT EQUALITY TEST
							var actualFieldValue = actual[field];
							actualFieldValue = typeof actualFieldValue === 'number' ? actualFieldValue + '' : actualFieldValue;

							var expectedFieldValue = expected[field];
							expectedFieldValue = typeof expectedFieldValue === 'number' ? expectedFieldValue + '' : actualFieldValue;

							// THEN WE COMPARE THE JSON STRING
							if (JSON.stringify(actualFieldValue) !== JSON.stringify(expectedFieldValue))
							{
								differences.push([field, actual[field], expected[field]]);
							}
						});

						pass = differences.length === 0;
					}

					//result.pass = util.equals(actual, expected, customEqualityTesters);
					if (pass)
					{
						result.message = "\nExpected datatype to have different field values";
					} else
					{
						result.message = "\nExpected datatype to have same field values";
					}

					result.pass = pass;

					result.message += "\n\nFound: " + JSON.stringify(actual/*, null, 4*/);
					result.message += "\n\nExpected: " + JSON.stringify(expected/*, null, 4*/);

					if (differences.length)
					{
						result.message += "\n\nDifferent fields:";

						differences.forEach(function(element)
						{
							result.message += "\n" + element[0] + " -> expected " + JSON.stringify(element[1]) + " to equal " + JSON.stringify(element[2]);
						});
					}

					return result;
				}
			,
			}
		}

		// @method toBeSameArray
		// @return {result:Boolean}
	,	toBeSameArray: function (util, customEqualityTesters)
		{
			return {
				compare: function (actual, expected)
				{
					var result = {}
					,	differences = []
					,	pass = false;


					actual.forEach(function (field, index) {
						if (JSON.stringify(actual[index]) !== JSON.stringify(expected[index]))
						{
							differences.push([field, actual[index], expected[index]]);
						}
					});

					pass = differences.length === 0;

					if (!pass)
					{
						result.message = "\nExpected array to have same field values";
					}

					result.pass = pass;

					result.message += "\n\nFound: " + JSON.stringify(actual);
					result.message += "\n\nExpected: " + JSON.stringify(expected);

					if (differences.length)
					{
						result.message += "\n\nDifferent fields:";

						differences.forEach(function (element)
						{
							result.message += "\n" + element[0] + " -> expected " + JSON.stringify(element[1]) + " to equal " + JSON.stringify(element[2]);
						});
					}

					return result;
				}
			,
			}
		}

	,	toBeSameText: function(util, customEqualityTesters)
		{
			return {
				compare: function(actual, expected)
				{
					var result = {};

					var pass = String(actual).toLowerCase() === String(expected).toLowerCase();

					if (pass)
					{
						result.message = "\nExpected text not to be the same";
					} else
					{
						result.message = "\nExpected be the same.";
					}

					result.pass = pass;

					result.message += "\nFound: " + actual;
					result.message += "\nExpected: " + expected;

					return result;
				}
			}
		}

	,	toBeSameMoney: function(util, customEqualityTesters)
		{
			return {
				compare: function(actual, expected)
				{
					var result = {};
					var siteSettings = Preconditions.Configuration.website.siteSettings || {};
					var decimalSeparator = siteSettings.decimalseparator || '.';
					var replaceRegex = new RegExp("[^\\d\\" + decimalSeparator + "]", 'g');

					actual = parseFloat(String(actual).replace(replaceRegex, ''));
					//console.log(actual);
					expected = parseFloat(String(expected).replace(replaceRegex, ''));
					//console.log(expected);

					var pass = actual === expected;

					if (pass)
					{
						result.message = "\nExpected number is different.";
					} else
					{
						result.message = "\nExpected number is the same.";
					}

					result.pass = pass;

					result.message += "\nFound: " + actual;
					result.message += "\nExpected: " + expected;

					return result;
				}
			}
		}

	,	toBeLessMoney: function(util, customEqualityTesters)
		{
			return {
				compare: function(actual, expected)
				{
					var result = {};
					var siteSettings = Preconditions.Configuration.website.siteSettings || {};
					var decimalSeparator = siteSettings.decimalseparator || '.';
					var replaceRegex = new RegExp("[^\\d\\" + decimalSeparator + "]", 'g');

					actual = parseFloat(String(actual).replace(replaceRegex, ''));
					//console.log(actual);
					expected = parseFloat(String(expected).replace(replaceRegex, ''));
					//console.log(expected);

					var pass = actual < expected;

					if (pass)
					{
						result.message = "\nExpected number is less.";
					} else
					{
						result.message = "\nExpected number is the same.";
					}

					result.pass = pass;

					result.message += "\nFound: " + actual;
					result.message += "\nExpected: " + expected;

					return result;
				}
			}
		}

	,	toBeTrueOrFailWith: function(util, customEqualityTesters)
		{
			return {
				compare: function(isTrue, message)
				{
					var result = {};

					result.pass = isTrue === true;
					result.message = message;

					return result;
				}
			}
		}

	,	toBeEqualDates: function (util, customEqualityTesters)
		{
			return {
				compare: function (found, expected)
				{
					var result = {}
					,	dateFound = new Date(found)
					,	dateExpected = new Date(expected);

					dateFound = dateFound.toISOString().substring(0,10);
					dateExpected = dateExpected.toISOString().substring(0,10);

					result.pass = dateFound === dateExpected ? true : false;

					return result;
				}
			}
		}

	,	toFailWith: function(util, customEqualityTesters)
		{
			return {
				compare: function(found, message)
				{
					return {
						pass: false
					,	message: message
					};
				}
			}
		}

	});
});
