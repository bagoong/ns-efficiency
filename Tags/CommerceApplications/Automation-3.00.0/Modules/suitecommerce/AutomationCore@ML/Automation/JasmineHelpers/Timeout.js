var	_ = require('underscore')
,	args = require('yargs').argv
;

// default timeout is 10 minutes, --timeout
var timeoutSeconds = 10 * 60;

if (_.isNumber(args.timeout) || _.isString(args.timeout))
{
	var timeoutTimeMatch = /(\d+)([ms]?)/i.exec(args.timeout + '');

	if (timeoutTimeMatch)
	{
		timeoutSeconds = parseInt(timeoutTimeMatch[1]);

		if (timeoutTimeMatch[2] == 'm')
		{
			timeoutSeconds = timeoutSeconds * 60;
		}
	}
}

// jasmine timeout is in milliseconds
jasmine.DEFAULT_TIMEOUT_INTERVAL = timeoutSeconds * 1000;