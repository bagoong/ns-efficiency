var 
	_ = require('underscore')
	jasmineSpecParser = require('../Lib/JasmineSpecParser')
;


var testSuite = _.chain(GLOBAL)
	.result('AutomationEnvironment')
	.result('suite')
	.value() || {};



var automationEnvironment = GLOBAL.AutomationEnvironment;

if (typeof automationEnvironment.specLocations === 'undefined')
{
	automationEnvironment.specLocations = [];
}


if (Array.isArray(automationEnvironment.spec_files) && Array.isArray(testSuite.testCases))
{
	automationEnvironment.spec_files.forEach(function(filePath)
	{
		var specsFound = jasmineSpecParser.extractJasmineSpecs(filePath);

		specsFound.forEach(function(specData)
		{
			var specFullName = specData.describe + " " + specData.it;

			var specTestCase = _.find(testSuite.testCases, function(testCase)
			{
				return testCase.filePath === filePath;
			});

			if (specTestCase)
			{
				automationEnvironment.specLocations.push({
					fullName: specFullName
				,	location: specTestCase.location
				});
			}
		})
	});
}

