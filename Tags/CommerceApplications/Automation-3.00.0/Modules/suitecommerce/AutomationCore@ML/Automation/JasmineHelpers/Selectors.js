// LOADS SELECTORS.JSON

var fs = require('fs');
	_ = require('underscore')
;

GLOBAL.Selectors = {};

var selectorsJson = fs.readFileSync('automation/selectors.json');
var rawSelectors = JSON.parse(selectorsJson);


beforeAll(function(done)
{
	var orderedSelectors = {};

	var responsiveEntryKeys = ['tablet', 'mobile'];
	var	activeResponsiveEntryKeys = [];

	if (_.isObject(GLOBAL.WEBDRIVER_RESOLUTION) && GLOBAL.WEBDRIVER_RESOLUTION.width)
	{
		var resolutionWidth = parseInt(GLOBAL.WEBDRIVER_RESOLUTION.width);

		if (resolutionWidth < 1024)
		{
			activeResponsiveEntryKeys.push('tablet');
		}

		if (resolutionWidth < 768)
		{
			activeResponsiveEntryKeys.push('mobile');
		}
	}

	Object.keys(rawSelectors).forEach(function (moduleName)
	{
		var baseModuleSelectors = _.omit(
			rawSelectors[moduleName], responsiveEntryKeys
		);

		var responsiveModuleSelectors = _.pick(
			rawSelectors[moduleName], activeResponsiveEntryKeys
		);

		var moduleSelectors = _.extend(
			baseModuleSelectors, responsiveModuleSelectors
		);

		Object.keys(moduleSelectors).forEach(function(section)
		{
			_.extend(moduleSelectors, moduleSelectors[section]);
			delete moduleSelectors[section]; 
		});

		var flatModuleName = moduleName.replace(/\./g, '');

		orderedSelectors[flatModuleName] = moduleSelectors;
	});


	GLOBAL.Selectors = orderedSelectors;

	done();
});

