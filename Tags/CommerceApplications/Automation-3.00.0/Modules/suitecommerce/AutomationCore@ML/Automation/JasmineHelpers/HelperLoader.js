/* jshint node: true */
'use strict';

var _ = require('underscore');

var HelpersRegistry = GLOBAL.HelpersRegistry = {};
var DefineRegistry = {};

var HelperDependencies = GLOBAL.HelperDependencies = {};

// @module automation @class JasmineContext 
// @method registerHelper @param {String} name @param {Function} helper
GLOBAL.registerHelper = function(name, helper)
{
	HelpersRegistry[name] = helper;
};

GLOBAL.defineHelper = function(name, helper)
{
	HelpersRegistry[name] = helper;
	DefineRegistry[name] = true;
};

GLOBAL.defineHelperDependencies = function(helper_name)
{
	var helpers = _.toArray(arguments).slice(1);
	HelperDependencies[helper_name] = helpers;
}


/* @method installHelpers @param {Client} client. Usage example: 
	
	
*/
GLOBAL.installHelpers = function(client)
{
	var helpers = _.toArray(arguments).slice(1);

	//console.log("%j", HelperDependencies);

	// ADD HELPER DEPENDENCIES
	Object.keys(HelperDependencies).forEach(function(helper_name)
	{
		var helper_dependencies = HelperDependencies[helper_name];

		helper_dependencies.forEach(function(helper_dependency)
		{
			helpers.push(helper_dependency);
		});
	});

	// INSTALL THE USER REQUIRED HELPERS
	helpers.forEach(function (helper_name)
	{
		var helper = HelpersRegistry[helper_name]
		,	keys = Object.keys(helper)
		,	object_reference;

		if (DefineRegistry[helper_name])
		{
			if (~helper_name.indexOf('.'))
			{
				object_reference = client;
				helper_name.split('.').forEach(function(token)
				{
					if (!object_reference[token])
					{
						object_reference[token] = {};
					}
					object_reference = object_reference[token];
				});
			}
			else
			{
				object_reference = client[helper_name] = client[helper_name] || {};
			}
		}

		keys.forEach(function(key)
		{
			var command_name = DefineRegistry[helper_name] ? helper_name + '.' + key : helper_name + '_' + key;
			client.addCommand(command_name, helper[key]);
			if (DefineRegistry[helper_name])
			{
				object_reference[key] = function()
				{
					return client[command_name].apply(client, arguments);
				}
			}
		});

	});
};


/* @method installCoreCommands @param {Client} client. Usage example: 
	
	installCoreCommands(client, 
		'generic', 'myCustomHelper'
	);

	Installs the helper methods as first-level WebdriverIO commands.
*/
GLOBAL.installCoreCommands = function(client)
{
	var helpers = _.toArray(arguments).slice(1);

	helpers.forEach(function (helper_name)
	{
		var helper = HelpersRegistry[helper_name]
		,	keys = Object.keys(helper);

		keys.forEach(function(command_name)
		{
			client.addCommand(command_name, helper[command_name]);
		});
		
	});
}

