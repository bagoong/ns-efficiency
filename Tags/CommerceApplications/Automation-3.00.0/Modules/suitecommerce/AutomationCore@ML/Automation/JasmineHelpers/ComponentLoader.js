var _ = require('underscore');

ComponentsRegistry = {};

// @module automation @class JasmineContext
// @method registerComponent @param {String} name @param {TODO} component
GLOBAL.registerComponent = function(name, component)
{
	ComponentsRegistry[name] = component;
};


// @method installComponents @param {Client} client
GLOBAL.installComponents = function(client)
{
	var components = _.toArray(arguments).slice(1);

	components.forEach(function (component_name)
	{
		client[component_name] = ComponentsRegistry[component_name];
	});
}