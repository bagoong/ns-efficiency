var _ = require('underscore');

defineHelper('Facets.Filter', 
{
	pushState: function(states, cb)
	{
		this.Facets.Filter.getState(function(err, result)
		{
			states.push(result);
			cb(null, result);
		});
	}

,	popState: function(states, cb)
	{
		cb(null, states.pop());
	}

	//@method getState facet state for the current page	
	//@param cb
,	getState: function(cb)
	{
		var client = this
		,	page_state = {
				
				title: ''
			,	urlPath: ''

				//facets extracted from the page url
			,	urlFacets: {}

				//facets extracted from the page
			,	pageFacets: []

			,	hasGlobalClear: false
			,	productCount: 0
		};
		
		client

			.getTitle(function(err, result)
			{
				page_state.title = result.toLowerCase();
			})

			.execute(function(){ return location.pathname; }, function(err, result)
			{
				//gets window.location.pathname and all applied facets from url
				page_state.urlPath = result.value;
				var url_matcher = /\/([^\/]+)\/([^\/]+)/g;
				var url_match;

				while (url_match = url_matcher.exec(result.value))
				{
					page_state.urlFacets[url_match[1]] = url_match[2].toLowerCase();
				}
			})	

			.isExisting(Selectors.Facets.clearAll, function(err, result)
			{
				page_state.hasGlobalClear = result;
			})

			.getText(Selectors.Facets.clearAny, function(err, result)
			{
				result = _.isUndefined(result) ? [] : result;
				result = _.isArray(result) ? result : [result];

				page_state.pageFacets = _.map(result, function(item)
				{
					return item.toLowerCase();
				});
			})

			.getAttribute(Selectors.Facets.title, 'data-quantity', function (err, result)
			{
				page_state.productCount = parseInt(result, 10);
			})

			.call(function()
			{
				cb(null, page_state);
			});
	}

	//@set
	//@param {groupSelector: String, selector: String} selector
	//@param cb
,	set: function(group_selector, selector, cb)
	{
		var client = this
		,	facet = { id: '', value: '' };

		client
			.getAttribute(group_selector, 'data-facet-id', function(err, result)
			{
				if (_.isArray(result))
				{
					facet.id = result[0];
				}
				else
				{
					facet.id = result;
				}
			})

			.getAttribute(selector, 'title', function(err, result)
			{
				if (_.isArray(result))
				{
					facet.value = result[0];
				}
				else
				{
					facet.value = result;	
				}
			})

			.click(selector)
			.pause(500)
			.waitForAjax()

			.call(function()
			{
				cb(null, facet); 
			});
	}

	//@setRange
	//@param cb
,	setRange: function(options, cb)
	{
		var client = this
		,	facet = { id: '', value: '' };
		
		client

			.getAttribute(options.groupSelector, 'data-facet-id', function(err, result)
			{
				if (_.isArray(result))
				{
					facet.id = result[0];
				}
				else
				{
					facet.id = result;
				}
			})

			.moveToObject(options.selector)

			.buttonDown(options.selector)

			.moveToObject(options.selector, options.offesetX, 0)

			.buttonUp()

			.pause(500)
			.waitForAjax()

			.getText(options.titleSelector, function(err, result)
			{
				facet.value = result.replace('$','').toLowerCase();
			})

			.call(function()
			{
				cb(null, facet);				
			});
	}

	//@remove
	//@param {String} facet_title
	//@param cb
,	remove: function(facet_title, cb)
	{
		var client = this
		,	selector = Selectors.Facets.clearId.replace('%s', facet_title);

		client
			.click(selector)
			.pause(500)
			.waitForAjax()
			.call(cb);
	}

,	clearAll: function(cb)
	{
		var client = this;

		client
			.click(Selectors.Facets.clearAll)
			.pause(500)
			.waitForAjax()
			.call(cb);
	}

});



//compares two string with an alphabet. 
//	string must be of the same length 
//	ignores all characters not included in the alphabet
function equalWithAlphabet(s1, s2, significant_alphabet)
{
	significant_alphabet = significant_alphabet || /[a-zA-Z0-9\-_]/;
	var s1_tokens = s1.split('');
	var s2_tokens = s2.split('');
	
	if (s1_tokens.length !== s2_tokens.length)
	{
		return false;
	}

	for(var i = 0; i<s1_tokens.length; i++)
	{
		if (significant_alphabet.test(s1_tokens[i]) && s1_tokens[i] !== s2_tokens[i])
		{
			return false;
		}
	}

	return true;
}

var facet_parser = /\/([^\/]+)\/([^\/]+)/g;
