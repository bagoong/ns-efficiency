
var _ = require("underscore");


defineHelper('Facets.ItemList',
{

	paginateUntilProductIsPresent: function(productId, cb)
	{
		var client = this;

		var itemSelector = '.facets-facet-browse-results [data-item-id="' + productId + '"]';

		client.GlobalViews.Pagination.searchBySelector(itemSelector, false, function(err, result)
		{
			cb(null, result.success);
		});
	}

,	searchAndClickItemByDataItemId: function(productId, cb)
	{
		var dataItemVisible = false;
		var client = this;

		this
			// wait until shopping is loaded
			.waitForAjax()
			.Facets.ItemList.paginateUntilProductIsPresent(productId, function(err, productPresent)
			{
				if (productPresent)
				{
					var detailsSelector = '.facets-facet-browse-results [data-item-id="' + productId + '"] .facets-item-cell-grid-details';

					client
						// IMPORTANT: WE HAVE TO WAIT FOR THE AJAX LOADING THE PICTURES
						// IF NOT, THE QUICK VIEW BUTTON OVERLAYS WITH TITLE ANCHOR, CAUSING
						// THE QUICKVIEW BUTTON TO BE CLICKED BY ERROR.
						.scroll(detailsSelector)
						.waitForAjax()
						.click(detailsSelector + ' .facets-item-cell-grid-title')
						.waitForAjax()
						.call(cb)
					;
				}
				else
				{
					throw new Error('Product with id ' + productId + ' not found in product list.')
				}
			})
		;
	}

,	isInPage: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.facets-facet-browse-content', 10000)
			.waitForAjax()
			.isExisting('.facets-facet-browse-content', cb)
		;
	}


,	clickItemQuickViewByDataItemId: function(productId, cb)
	{
		var dataItemVisible = false;
		var client = this;

		this
			// wait until shopping is loaded
			.waitForAjax()
			.Facets.ItemList.paginateUntilProductIsPresent(productId, function(err, productPresent)
			{
				if (productPresent)
				{
					var cell_item = ".facets-facet-browse-results [data-item-id='" + productId + "']";

					client
						.waitForExist(cell_item, 10000)
						.moveToObject(cell_item)
						.execute(function(productId)
						{
							jQuery(".facets-facet-browse-results *[data-item-id=" + productId + "] .facets-item-cell-grid-quick-view-wrapper").css('display', 'block')
						}, productId)
						.click("//*[@data-item-id=" + productId + "]/descendant::*[@class='facets-item-cell-grid-quick-view-link']")
						.call(cb)
					;
				}
				else
				{
					throw new Error('Product with id ' + productId + ' not found in product list.')
				}
			})
		;
	}


,	clickGoToNextPage: function(cb)
	{
		var client = this;

		this
			.isVisible(Selectors.GlobalViews.buttonPaginatorNext, function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.click(Selectors.GlobalViews.buttonPaginatorNext)
						.waitForAjax()
					;
				}
			})

			.call(cb)
		;
	}

,	goToLastPage: function(cb)
	{
		var client = this;
		this.pageNumber = 0
		this
			.isVisible(Selectors.GlobalViews.buttonPaginatorNext, function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.click(Selectors.GlobalViews.buttonPaginatorNext)
						.waitForAjax()
						.Facets.ItemList.goToLastPage()
						.call(cb)
					;
				}
				else
				{
					client
						.waitForAjax()
						.getHTML('.global-views-pagination-active', false, function(cb, page)
						{
							client.pageNumber = parseInt(page);
						})
						.call(cb)
					;
				}
			})
		;
	}

,	goToFirstPage: function(cb)
	{
		var client = this;
		this.pageNumber = 0
		this
			.isVisible(Selectors.GlobalViews.buttonPaginatorPrev, function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.click(Selectors.GlobalViews.buttonPaginatorPrev)
						.waitForAjax()
						.Facets.ItemList.goToFirstPage()
						.call(cb)
					;
				}
				else
				{
					client
						.waitForAjax()
						.getHTML('.global-views-pagination-active', false, function(cb, page)
						{
							client.pageNumber = parseInt(page);
						})
						.call(cb)
					;
				}
			})
		;
	}


,	getDataItemIds: function(cb)
	{
		var client = this;
		var items = [];

		this
			.elements('[data-type=item]', function(err, elements)
			{
				elements.value.forEach(function(element)
				{
					client
						.elementIdAttribute(element.ELEMENT, 'data-item-id', function(err, res)
						{
							if (!err)
							{
								var data_item_id = res.value;
								items.push(data_item_id);
							}
						})
					;
				});
			})

			.call(function() {
				cb(null, items)
			})
		;
	}


,	clickItemByDataItemId: function(product_id, cb)
	{
		var link_selector = Selectors.Facets.productByIdTitleLink.replace("%s", product_id);

		this
			.waitForExist(link_selector, 5000)
			.click(link_selector)
			.call(cb)
		;
	}


,	clickRandomItem: function(cb)
	{
		var client = this;

		this
			.Facets.ItemList.getDataItemIds(function(err, data_item_ids)
			{
				if (data_item_ids.length)
				{
					var random_item_id = _.sample(data_item_ids);
					//console.log(random_item_id)

					client
						.Facets.ItemList.clickItemByDataItemId(random_item_id)
						.call(cb)
					;
				}
				else
				{
					client
						.call(cb);
				}
			})
		;
	}

,	validateItemsPerPageAndPaginator: function(cb)
	{
		var client = this;
		var data =
		{
			'totalItems': 0
		,	'itemsPerPage': 0
		,	'calculatedPages': 0
		,	'itemsInLastPage': 0
		}
		this
			.waitForExist('.facets-facet-browse-title',10000)
			.getAttribute('.facets-facet-browse-title', 'data-quantity', function(err, items)
			{
				data.totalItems = items;
			})
			.getAttribute("[data-view='Facets.ItemListShowSelector'] [selected]", 'class', function(err, per_Page)
			{
				data.itemsPerPage = per_Page;
			})
			.isExisting('.global-views-pagination-links', function(err, is_Existing)
			{
				if(is_Existing)
				{
					data.calculatedPages = Math.ceil(data.totalItems / data.itemsPerPage);
					data.itemsInLastPage = data.totalItems % data.itemsPerPage;
					if(data.itemsInLastPage == 0)
					{
						data.itemsInLastPage = data.itemsPerPage;
					}
				}
			})
			.call(function()
			{
				cb(null, data)
			})
		;
	}

,	setProductsPerPage: function(productsPerPage, cb)
	{
		var client = this;
		this
			.waitForAjax()
			.getAttribute("[data-view='Facets.ItemListShowSelector'] [selected]", 'class', function(err, per_Page)
			{
				if(productsPerPage != per_Page){
					client
						.waitForAjax()
						.click('.facets-item-list-show-selector')
						.waitForExist("//*[@class='" + productsPerPage + "']", 5000)
						.click("//*[@class='" + productsPerPage + "']")
						.waitForAjax()
				}
			})
			.call(cb)
		;
	}

,	getItemsInPage: function(cb)
	{
		var itemsInPage;
		this
			.Facets.ItemList.getDataItemIds(function(err, data_item_ids)
			{
				itemsInPage = data_item_ids.length
			})
			.call(function()
			{
				cb(null, itemsInPage)
			})
		;
	}

,	getTitle: function(cb)
	{
		var title;
		this
			.getText('.facets-facet-browse-title', function(err, productTitle)
			{
				title = productTitle;
			})
			.call(function()
			{
				cb(null, title)
			})
		;
	}

,	setSortBy: function(sort, cb)
	{
		var client = this;
		var sortBy;
		if(sort == 'LowToHigh')
		{
			sortBy = 'onlinecustomerprice-asc';
		}
		else
		{
 			sortBy = 'onlinecustomerprice-desc';
		}
		this
			.waitForAjax()
			.getAttribute("[data-view='Facets.ItemListSortSelector'] [selected]", 'class', function(err, sortClass)
			{
				if(sortBy != sortClass){
					client
						.waitForAjax()
						.click("[data-view='Facets.ItemListSortSelector'] .facets-item-list-sort-selector")
						.waitForExist("//*[@class='" + sortBy + "']", 5000)
						.click("//*[@class='" + sortBy + "']")
						.waitForAjax()
				}
			})
			.call(cb)
		;
	}

,	getItemsDataType: function(sort,cb)
	{
		var id;
		var dataTypes = [];
		var listToCheck;
		var client = this;
		this
			.waitForAjax()
			.Facets.ItemList.getDataItemIds(function(err, list)
			{
				listToCheck = list;
				while(typeof listToCheck !== 'undefined' && listToCheck.length > 0)
				{
					id = listToCheck[listToCheck.length - 1]
					listToCheck.pop()
					client
						.Facets.ItemList.getItemDataTypeById({Id: id, Sort: sort} , function(err, dataType)
						{
							dataTypes.push(dataType);
						})
					;
				}
			})
			.call(function()
			{
				cb(null, dataTypes);
			})

	}

,	getItemDataTypeById: function(params, cb)
	{
		var client = this;
		var prefix = "//*[@class='facets-facet-browse-results']//*[@data-item-id='" + params.Id + "']"
		var dataType = {
			'Name': ''
		,	'ItemPrice': ''
		,	'Id': params.Id
		}

		this
			.waitForAjax()
			.waitForExist(prefix + "//*[@class='facets-item-cell-grid-title']/span", 10000)
			.getText(prefix + "//*[@class='facets-item-cell-grid-title']/span", function(err, text){
				dataType.Name = text;
				console.log("Name: " + dataType.Name)
			})
			.getText(prefix + "//*[@class='item-views-price-lead']", function(err, text){
								dataType.ItemPrice = text;
				console.log("ItemPrice: " + dataType.ItemPrice)
							})
			// .isExisting(prefix + "//*[@class='item-views-price-lead']", function(err, is_present)
			// {
			// 	if(is_present)
			// 	{
			// 		if(params.Sort === 'HighToLow')
			// 		{
			// 			client
			// 				.getAttribute(prefix + "//*[@class='item-view-lead-price']//*[@itemprop='highPrice']", 'data-rate', function(err, text)
			// 				{
			// 					dataType.ItemPrice = text;
			// 				})
			// 			;
			// 		}
			// 		else
			// 		{
			// 			client
			// 				.getAttribute(prefix + "//*[@class='item-view-lead-price']//*[@itemprop='lowPrice']", 'data-rate', function(err, text)
			// 				{
			// 					dataType.ItemPrice = text;
			// 				})
			// 			;
			// 		}
			// 	}
			// 	else
			// 	{
			// 		client
			// 			.getAttribute(prefix + "//*[@class='item-view-lead-price']", 'data-rate', function(err, text){
			// 				dataType.ItemPrice = text;
			// 			})
			// 		;
			// 	}
			// })
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}

,	searchAndAddByQuickview: function(item, cb)
	{
		this
			.click(Selectors.Header.shop)
			.Facets.ItemList.clickItemQuickViewByDataItemId(item.id)
			.ItemDetails.QuickView.selectOptions(item.options)
	        .ItemDetails.QuickView.addToCart(item.sku)
	        .ItemDetails.QuickView.continueShopping()
			.call(cb)
		;
	}

});
