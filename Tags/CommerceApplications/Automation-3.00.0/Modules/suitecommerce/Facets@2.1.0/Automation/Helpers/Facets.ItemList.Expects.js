defineHelper('Facets.ItemList.Expects', 
{

	checkItemsPerPage: function (ItemsPerPage, cb) 
	{
		var client = this;
		this
			.waitForAjax()
			.isVisible('.global-views-pagination-next', function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.Facets.ItemList.getDataItemIds(function(err, items)
						{
							expect(parseInt(ItemsPerPage)).toEqual(items.length)
							client
								.Facets.ItemList.Expects.checkItemIsNotDuplicated(items)
							;
						})
						.click('.global-views-pagination-next')
						.waitForAjax()
						.Facets.ItemList.Expects.checkItemsPerPage(ItemsPerPage)
					;
				}
				else
				{
					client
						.Facets.ItemList.getDataItemIds(function(err, items)
						{
							if(items.length == ItemsPerPage)
							{
								expect(parseInt(ItemsPerPage)).toEqual(items.length)
								client
									.Facets.ItemList.Expects.checkItemIsNotDuplicated(items)
								;
							}
							else
							{
								expect(parseInt(ItemsPerPage)).toBeGreaterThan(items.length)
								client
									.Facets.ItemList.Expects.checkItemIsNotDuplicated(items)
								;
							}
							
						})
						.call(cb)
					;
				}
			})
			.call(cb)
		;
	}

, 	checkItemIsNotDuplicated: function(listOfItems, cb)
	{
		var listToCheck = listOfItems;
		var name;
		while(typeof listToCheck !== 'undefined' && listToCheck.length > 0)
		{	
			expect(listToCheck).toBeNonEmptyArray()
			name = listToCheck[listToCheck.length-1]
			listToCheck.pop()
			expect(listToCheck).not.toContain(name)
		}
		this
			.call(cb)
		;
	}

,	checkItemsSort: function(sort, cb)
	{
		var client = this;
		var listToCheck;
		this
			.waitForAjax()
			.isVisible('.global-views-pagination-next', function (err, is_paginator_visible)
			{
				if(is_paginator_visible)
				{
					client
						.Facets.ItemList.Expects.checkItemsSortInOnePage(sort)
						.waitForAjax()
						.click('.global-views-pagination-next')
						.waitForAjax()
						.pause(2000)
						.Facets.ItemList.Expects.checkItemsSort(sort)
					;
				}
				else
				{
					client
						.Facets.ItemList.Expects.checkItemsSortInOnePage(sort)
						.call(cb)
					;
				}
			})
			.call(cb)
		;
	}

,	checkItemsSortInOnePage: function(sort, cb)
	{
		var sortingBy = sort;
		var price;
		var client = this;
		this
			.Facets.ItemList.getItemsDataType(sort, function(err, list)
			{
				listToCheck = list
				while(typeof listToCheck !== 'undefined' && listToCheck.length > 1)
				{	
					if(sortingBy == 'HighToLow')
					{
						expect(listToCheck).toBeNonEmptyArray()
						price = client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice);
						listToCheck.pop()
						if(price === client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))
						{
							expect(price).toEqual(client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))	
						}
						else
						{
							expect(price).toBeGreaterThan(client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))			
						}
					}
					else
					{
						expect(listToCheck).toBeNonEmptyArray()
						price = client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice);
						listToCheck.pop()
						if(price === client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))
						{
							expect(price).toEqual(client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))	
						}
						else
						{
							expect(price).toBeLessThan(client.util.textToFloat(listToCheck[listToCheck.length-1].ItemPrice))			
						}
					}
				}
			})
			.call(cb)
		;
	}

});