defineHelper('ReorderItems.Expects', 
{

	checkReorderItemsPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.ReorderItems.reorderItemsPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Reorder Items page is not present.');
        	})
        	.call(cb)
        ;
	}	
});