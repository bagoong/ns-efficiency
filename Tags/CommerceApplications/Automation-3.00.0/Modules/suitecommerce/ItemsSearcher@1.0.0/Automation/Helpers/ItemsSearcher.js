// @module ItemsSearcher
defineHelper('ItemsSearcher',
{
	// @method writeValue Write value on the items searcher
	// @params {String} value
	// @return {Void}
	writeValue: function (value, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ItemsSearcher.inputSearch, 5000)
			.setValue(Selectors.ItemsSearcher.inputSearch, value)
			.call(cb)
		;
	}

});