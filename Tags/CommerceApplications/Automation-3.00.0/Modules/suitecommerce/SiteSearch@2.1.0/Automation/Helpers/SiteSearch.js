defineHelper('SiteSearch', 
{
	searchByName: function(itemName, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.SiteSearch.linkSearch, 10000)
			.click(Selectors.SiteSearch.linkSearch)
			.waitForAjax()
			.waitForExist(Selectors.SiteSearch.inputSearch, 10000)
			.addValue(Selectors.SiteSearch.inputSearch, itemName)
			.waitForAjax()
			.click(Selectors.SiteSearch.buttonSubmitSearch)
			.waitForAjax()
			.call(cb)
		;
	}
});
