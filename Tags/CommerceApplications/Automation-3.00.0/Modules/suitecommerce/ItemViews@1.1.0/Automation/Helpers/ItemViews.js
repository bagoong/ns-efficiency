defineHelper('ItemViews', {
	clickLogin: function(itemid, cb)
	{
		this
			.waitForAjax()
			.click('[data-item-id="'+ itemid +'"] .item-views-price-login-to-see-prices a')
			.call(cb)
		;
	}
,	clickLoginOnPDP: function (cb)
	{
		this
			.click('.item-details-info .item-views-price-login-to-see-prices a')
			.call(cb)
		;
	}
,	clickLoginOnQuickView: function (cb)
	{
		this
			.click('.quick-view-confirmation-modal .item-views-price-login-to-see-prices a')
			.call(cb)
		;
	}
});