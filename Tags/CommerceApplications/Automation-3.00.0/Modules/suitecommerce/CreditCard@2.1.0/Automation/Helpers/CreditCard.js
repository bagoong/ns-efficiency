var _ = require("underscore");

defineHelper('CreditCard', 
{

	checkExpectedCreditCard: function(expected_creditCard, cb)
	{
		this		
			.waitForExist(Selectors.CreditCard.creditCardIcon, 5000)
			.getAttribute(Selectors.CreditCard.creditCardIcon, 'alt',function(err, text){
				expect(text.toLowerCase()).toMatch(expected_creditCard.cardType.toLowerCase());
			})
			.getText(Selectors.CreditCard.creditCardNumber, function(err, text){
				var regexCC = expected_creditCard.cardNumber.match(/\d{4}$/i); 
				expect(text).toMatch(regexCC[0]);
			})
			.getText(Selectors.CreditCard.creditCardExpDate, function(err, text){
				expect(text).toMatch(expected_creditCard.expMonth + '/' + expected_creditCard.expYear);
			})
			.getText( Selectors.CreditCard.creditCardName, function(err, text){
				expect(text).toMatch(expected_creditCard.name);
			})
			.call(cb)
		;
	}	

,	verifyCreditCardDefault: function(cb)
	{
		this
			.isExisting(Selectors.CreditCard.defaultIcon, function(err, text){
				expect(text).toBe(true);
			})
			.call(cb)
		;
	}	

,	verifyCreditCardNotDefault: function(cb)
	{
		this
			.isExisting(Selectors.CreditCard.defaultIcon, function(err, text){
				expect(text).toBe(false);
			})
			.call(cb)
		;
	}

,	fillCreditCard: function (params, cb) 
	{	
		var client = this;

		this
			.waitForExist(Selectors.CreditCard.editNumber, 10000)	
			.setValue(Selectors.CreditCard.editNumber, params.cardNumber)
			.selectByVisibleText(Selectors.CreditCard.editMonth, params.expMonth.replace(/^0/, ""))
			.selectByVisibleText(Selectors.CreditCard.editYear, params.expYear)
			
			.CreditCard.fillSecurityNumber(params.securityNumber)
			
			.setValue(Selectors.CreditCard.editName, params.name)

			.call(function()
			{
				if(params.defaultCreditCard || params.ccDefault)
				{
					client
						.CreditCard.clickMakeThisMyDefaultCreditCard()
					;
				}
			})

			.call(cb)
		;
	}

,	fillSecurityNumber: function(security_number, cb)
	{
		var client = this;
		this
			.isExisting(Selectors.CreditCard.editSecurityNumber, function(err, existing)
			{
				if (existing) 
				{
					client.setValue(Selectors.CreditCard.editSecurityNumber, security_number);
				}
			})
			.call(cb)
		;
	}	

,	fillEditCreditCard: function (params, cb) 
	{	
		var client = this;
		
		this
			.waitForExist(Selectors.CreditCard.editNumber, 10000)	
			.selectByVisibleText(Selectors.CreditCard.editMonth, (params.expMonth + '').replace(/^0/, ""))
			.selectByVisibleText(Selectors.CreditCard.editYear, params.expYear  + '')
			.setValue(Selectors.CreditCard.editName, params.name)

			.isExisting(Selectors.CreditCard.editDefault, function(err, is_existing)
			{
				if (is_existing)
				{
					client.setCheckboxValue({
						selector: Selectors.CreditCard.editDefault, check: params.defaultCreditCard
					});	
				}
			})

			.call(cb)
		;
	}

,	clickMakeThisMyDefaultCreditCard: function(cb)
	{
		this
			.click(Selectors.CreditCard.defaultCheckBox)
			.waitForAjax()
			.call(cb)
		;

	}	

,	clickAddCard: function(cb)
	{
		this
			.waitForExist(Selectors.CreditCard.addCardButton)
			//.click(Selectors.CreditCard.editCreditCardTitle)
			.waitForAjax()
			.click(Selectors.CreditCard.addCardButton)
			.waitForAjax()
			.call(cb)
		;

	}

,	clickAddNewCard: function(cb)
	{
		this
			.waitForExist(Selectors.CreditCard.addNewCreditCardButton)
			.click(Selectors.CreditCard.addNewCreditCardButton)
			.waitForAjax()
			.call(cb)
		;

	}

,	clickChangeCard: function(cb)
	{
		this
			.waitForExist(Selectors.CreditCard.changeCardButton)
			.click(Selectors.CreditCard.changeCardButton)
			.waitForAjax()
			.call(cb)
		;

	}

,	clickAddCreditCard: function(cb)
	{
		this
			.waitForExist(Selectors.CreditCard.addCreditCardButton)
			.click(Selectors.CreditCard.addCreditCardButton)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickUseCreditCard: function(params, cb)
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var client = this;

		this
			.waitForExist(Selectors.CreditCard.useThisCreditCardButton)
			.CreditCard.getCreditCardsDataType({ includeDataId: true }, function(err, cards) 
			{
				console.log(cards);

				var dataId = params.dataId;

				if (params.index) {
					dataId = cards[params.index].dataId;
				}

				if (!dataId) {
					dataId = cards[0].dataId;
				}

				client
					.click(Selectors.CreditCard.useThisCreditCardButton.replace('%s', dataId))
					.waitForAjax()
				;
			})

			.call(cb)
		;
	}

,	removePreviousCreditCards: function (params, cb) 
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var client = this;

		client
			.CreditCard.getCreditCards(params, function(err, creditCards)
			{
				creditCards.forEach(function(creditCard_data) 
				{

					var no_filter = !params.cardNumber;
					if(params.cardNumber){
						var title_filter = params && params.cardNumber.match(/\d{4}$/i);
					} else {
						var title_filter = params && params.cardNumber;
					}

					if (no_filter || title_filter)
					{
						client
							.CreditCard.clickRemoveCreditCardUsingDataId(
								creditCard_data.dataId
							)
							.pause(500)
						;
					}
				});

				client.call(cb);
			})
		;

	}	

,	getCreditCards: function(params, cb)
	{
		var client = this;
		var creditCards = [];

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element = params.parentElementXPath || "";

		this
			.elements(parent_element + Selectors.CreditCard.creditCardClass, function(err, elements) {

				elements.value.forEach(function(element) 
				{
					var element_id = element.ELEMENT;

					client
						.elementIdAttribute(element_id, 'data-id', function(err, res)
						{
							if (err) return false;

							var data_id = res.value;

							var creditCard_found = {
								dataId: data_id
							,	number: ""
							};

							client
								// IMPORTANT: we use getHtml INSTEAD OF getText because getText only works
								// for elements visible on screen and scrolling seems to work properly only with form items.

								.getHTML(parent_element + Selectors.CreditCard.creditCardDataId.replace('%s', data_id), false, function(err, html)
								{

									if (Array.isArray(html)) {
										html = html[0];
									}

									numbers = client.util.stripTags(html).match(/\d+/)

									creditCard_found.number = numbers[0];
								})
							;

							creditCards.push(creditCard_found);
						})
					;
				});
			})

			.call(function() {
				cb(null, creditCards)
			})
		;

	}



,	clickRemoveCreditCardUsingDataId: function(creditCard_data_id, cb) 
	{
		var client = this;
		

		var remove_selector = Selectors.CreditCard.removeCreditCard.replace('%s', creditCard_data_id);
			
		client.isExisting(remove_selector, function(err, is_existing)
		{
			if (!is_existing) return client.call(cb);

			client
				.scroll(remove_selector)

				// USING GET ATTRIBUTE BECAUSE IS ENABLED IS NOT WORKING PROPERLY IN WEBDRIVERIO
				.getAttribute(remove_selector, 'disabled', function(err, is_disabled)
				{
					if (is_disabled) return client.call(cb);

					client
						.click(remove_selector)
						.GlobalViews.Modal.clickYesOnModal()
						.call(cb)
					;
				})
			;
		});
	}

,	getCreditCardDataType: function(params, cb)
	{
		var client = this;
		
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element_xpath = params.parentElementXPath || "";

		var datatype = {
			'cardType'         : ''
		,	'cardNumber'       : ''
		,	'cardEnding'       : ''
		,	'expMonth'         : ''
		,	'expYear'          : ''
		,	'securityNumber'   : ''
		,	'name'             : ''
		,	'defaultCreditCard': false
		};

		this
			.call(function(){
				if (params.includeDataId)
				{
					client.getAttribute(parent_element_xpath + "//*[@class='creditcard']", 'data-id', function(err, att)
					{
						datatype.dataId = att;
					})
				}
			})

			.getAttribute(parent_element_xpath + Selectors.CreditCard.getIcon, 'alt', function(err, text){
				datatype.cardType = text;
			})

			.getText( parent_element_xpath + Selectors.CreditCard.getEnding, function(err, text){
				var cType = text.split('in ')[1]
				datatype.cardEnding = cType;
			})

			.getText( parent_element_xpath + Selectors.CreditCard.getExpDate, function(err, text){
				var date = text.split('Expires ')[1].split('/')
				datatype.expMonth = date[0];
				datatype.expYear = date[1];
			})	

			.getText(parent_element_xpath + Selectors.CreditCard.getName, function(err, text){
				datatype.name = text;
			})

			.isExisting(parent_element_xpath + Selectors.CreditCard.getDefault, function(err, text){
				datatype.defaultCreditCard = text;
			})

			.call(function()
			{
				cb(null, datatype);
			})
		;
	}

,	getCreditCardsDataType: function(params, cb)
	{
		var client = this;
		var creditCards = [];

		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		this
			.CreditCard.getCreditCards(function(err, found_creditCards)
			{
				found_creditCards.forEach(function(found_creditCard)
				{
					var id = found_creditCard.dataId

					client
						.CreditCard.getCreditCardDataType({parentElementXPath: Selectors.CreditCard.getId.replace('%s', id)}, function(err, creditCardDT)
						{
							if (params.includeDataId)
							{
								creditCardDT.dataId = id;
							}
							
							creditCards.push(creditCardDT)
						})
					;	
				})
			})
			.call(function() {
				cb(null, creditCards)
			})
		;
	}

,	getCreditCardIdFromNumber: function(ending, cb)
	{
		var client = this;
		var id
		this
			.CreditCard.getCreditCards(function(err, creditCards)
			{
				creditCards.forEach(function(credit_card)
				{
					if(credit_card.number == ending)
					{
						id = credit_card.dataId
					}
				})
			})
			.call(function() {
				cb(null, id)
			})
		;
	}

,	editCreditCard: function(params, cb)
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element_xpath = params.parentElementXPath || "";

		this
			.click(parent_element_xpath + Selectors.CreditCard.editButton)
			.call(cb)
		;
	}

,	getErrorList: function(cb)
	{
		var error_label;
		this
			.waitForAjax()
			.getText(Selectors.CreditCard.errorMessage, function(err, text)
			{
				error_label = text;
			})
			.call(function() {
				if (typeof error_label === 'string' || typeof error_label === 'undefined')
				{
					error_label = [error_label];
				}

				cb(null, error_label)
			})
		;
	}

,	selectNonDefaultCreditCard: function(creditCardList, cb)
	{
		var client = this;
		var creditCards = [];

		_.each(creditCardList, function (creditCard)
		{
			if(!creditCard.ccDefault)
			{
				creditCards.push(creditCard)
			}
		})
		var creditCard = creditCards.pop()
		client
			.click(Selectors.CreditCard.useThisCreditCardButton.replace('%s', creditCard.internalId))

			.waitForAjax()

			.call(function ()
			{
				cb(null, creditCard)
			})
		;
	}

});