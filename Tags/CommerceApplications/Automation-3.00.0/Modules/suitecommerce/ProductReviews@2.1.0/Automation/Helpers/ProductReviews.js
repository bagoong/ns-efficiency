defineHelper('ProductReviews', 
{
	getReviewSummary: function(cb)
	{
		var dataType = {
			'reviewAmount': ''
		,   'fiveStars': ''
		,   'fourStars': ''
		,   'threeStars': ''
		,   'twoStars': ''
		,   'oneStars': ''
		}

		this
			.waitForExist('.product-reviews-center-container-header-number', 10000)
			.getText('.product-reviews-center-container-header-number', function(err, text){
				dataType.reviewAmount = text;
			})
			.waitForExist("[data-id='stars5'] .global-views-rating-by-star-label-count", 10000)
			.getText("[data-id='stars5'] .global-views-rating-by-star-label-count", function(err, text){
				dataType.fiveStars = text;
			})
			.waitForExist("[data-id='stars4'] .global-views-rating-by-star-label-count", 10000)
			.getText("[data-id='stars4'] .global-views-rating-by-star-label-count", function(err, text){
				dataType.fourStars = text;
			})
			.waitForExist("[data-id='stars3'] .global-views-rating-by-star-label-count", 10000)
			.getText("[data-id='stars3'] .global-views-rating-by-star-label-count", function(err, text){
				dataType.threeStars = text;
			})
			.waitForExist("[data-id='stars2'] .global-views-rating-by-star-label-count", 10000)
			.getText("[data-id='stars2'] .global-views-rating-by-star-label-count", function(err, text){
				dataType.twoStars = text;
			})
			.waitForExist("[data-id='stars1'] .global-views-rating-by-star-label-count", 10000)
			.getText("[data-id='stars1'] .global-views-rating-by-star-label-count", function(err, text){
				dataType.oneStars = text;
			})
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}

,	getReviewById: function(id, cb)
	{
		var prefix = ".product-reviews-review[data-id='" + id + "']";
		var dataType = {
			'Title': '' 
		,	'Body': ''
		,	'Reviewer': ''
		,   'CreationDate': ''
		,   'Stars': ''
		}
		
		this
			.waitForExist(prefix + ' .product-reviews-review-comment-item-cell-date', 10000)
			.getText(prefix + ' .product-reviews-review-comment-item-cell-date', function(err, text){
				dataType.CreationDate = text;
			})
			.waitForExist(prefix + ' .product-reviews-review-comment-username span', 10000)
			.getText(prefix + ' .product-reviews-review-comment-username span', function(err, text){
				dataType.Reviewer = text;
			})
			.waitForExist(prefix + ' h4', 10000)
			.getText(prefix + ' h4', function(err, text){
				dataType.Title = text;
			})
			.waitForExist(prefix + ' .product-reviews-review-review-description', 10000)
			.getText(prefix +  ' .product-reviews-review-review-description', function(err, text){
				dataType.Body = text;
			})
			.waitForExist(prefix + ' .global-views-star-rating-area', 10000)
			.getAttribute(prefix + ' .global-views-star-rating-area', "data-value", function(err, text){
				dataType.Stars = text;
			})
			
			.call(function()
			{
				cb(null, dataType)
			})
		;
	}	

,	clickWriteAReview: function (cb)
	{
		var client = this;
		this
			.waitForAjax()
			.isExisting(Selectors.ProductReviews.buttonWriteAReview).then(function (isExisting)
			{
				if (isExisting)
				{
					client.click(Selectors.ProductReviews.buttonWriteAReview)
				}else{
					client.click(Selectors.ProductReviews.buttonWriteAReviewFooter)
				}
			})
			.call(cb)
		;
	}	

,	clicksSubmit: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonSubmit, 5000)	
			.scroll(Selectors.ProductReviews.buttonSubmit)
			.click(Selectors.ProductReviews.buttonSubmit)
			.call(cb)
		;
	}	

,	clickBack: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.linkBack, 5000)
			.click(Selectors.ProductReviews.linkBack)
			.call(cb)
		;
	}	

,	clickPreview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonPreview, 5000)
			.click(Selectors.ProductReviews.buttonPreview)
			.call(cb)
		;
	}

,	clickSubmitPreview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonSubmitPreview, 5000)
			.click(Selectors.ProductReviews.buttonSubmitPreview)
			.call(cb)
		;		
	}

,	clickEditReview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonEditReview, 5000)
			.click(Selectors.ProductReviews.buttonEditReview)
			.call(cb)
		;		
	}

,	clickYesHelpful: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.click(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber))
			.call(cb)
		;
	}

,	clickNoHelpful: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.click(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber))
			.call(cb)
		;
	}	

,	fillReviewFields: function (params, cb)
	{
		this
			.ProductReviews.fillRating(params.reviewRating)
			.ProductReviews.fillAuthor(params.reviewAuthor)
			.ProductReviews.fillText(params.reviewText)
			.ProductReviews.fillTitle(params.reviewHeadline)
			.call(cb)
		;
	}

	// CAN'T CLEAR THE RATING STARS
,	clearFields: function (cb)
	{
		this
			.ProductReviews.fillAuthor('')
			.ProductReviews.fillText('')
			.ProductReviews.fillTitle('')
			.call(cb)
		;
	}	

,	fillRating: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonRating.replace('%s', params), 5000)
			.click(Selectors.ProductReviews.buttonRating.replace('%s', params))
			.call(cb)
		;
	}

,	fillAuthor: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.inputAuthor, 5000)
			.setValue(Selectors.ProductReviews.inputAuthor, params)
			.call(cb)
		;
	}

,	fillText: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.inputText, 5000)
			.setValue(Selectors.ProductReviews.inputText, params)
			.call(cb)
		;
	}

,	fillTitle: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.inputHeadline, 5000)
			.setValue(Selectors.ProductReviews.inputHeadline, params)
			.call(cb)
		;
	}

,	getHelpfulCount: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				params.reviewYesCount = parseInt(text.replace(/\D*/g, ''));
			})

			.waitForExist(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				params.reviewNoCount = parseInt(text.replace(/\D*/g, ''));
			})
			.call(cb)
		;
	}	

	// WORKS FOR STARS (5star, 4star, 3star, 2star, 1star, all) AND OTHER ORDERS
,	changeSortingFilter: function (value, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.selectStarsFilter, 5000)
			.click(Selectors.ProductReviews.selectStarsFilter.replace('%s', value))
			.call(cb)
		;
	}

,	changeSortOrder: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonSortOrder, 5000)
			.click(Selectors.ProductReviews.buttonSortOrder)
			.call(cb)
		;
	}

,	fillAllAttributeRatings: function (rating, cb)
	{
		var client = this;
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerAttributeTitles, 5000)
			.getAttribute(Selectors.ProductReviews.containerAttributeTitles, "for", function (err, text)
			{
				for (i = 0, count = text.length; i < count; ++i)
				{	
					client
						.ProductReviews.fillAttributeRating(rating, text[i])
					;		
				}
			})
			.call(cb)
		;
	}

,	fillAttributeRating: function (rating, attribute, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonAttributeRating.replace('%s', attribute).replace('%n', rating), 5000)
			.click(Selectors.ProductReviews.buttonAttributeRating.replace('%s', attribute).replace('%n', rating))
			.call(cb)
	}		

});