defineHelper('ProductReviews.Expects', 
{

	thankYouPageDisplayed: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerThankYou, 5000)
			.getText(Selectors.ProductReviews.containerThankYou, function (err, text) 
			{
				expect(text).toMatch("THANK YOU");
			})
			.call(cb)
		;
	}

,	previewInThankYouPage: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.metaRatingValue, 5000)
			.getAttribute(Selectors.ProductReviews.metaRatingValue, 'content', function (err, text) 
			{
				expect(text).toMatch(params.reviewRating);
			})

			.waitForExist(Selectors.ProductReviews.textAuthor, 5000)
			.getText(Selectors.ProductReviews.textAuthor, function (err, text) 
			{
				expect(text).toMatch(params.reviewAuthor);
			})

			.waitForExist(Selectors.ProductReviews.textDescription, 5000)
			.getText(Selectors.ProductReviews.textDescription, function (err, text) 
			{
				expect(text).toMatch(params.reviewText);
			})

			.waitForExist(Selectors.ProductReviews.textName, 5000)
			.getText(Selectors.ProductReviews.textName, function (err, text) 
			{
				expect(text).toMatch(params.reviewHeadline);
			})
			.call(cb)
		;
	}

,	previewPage: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.metaRatingValue, 5000)
			.getAttribute(Selectors.ProductReviews.metaRatingValue, 'content', function (err, text)
			{
				expect(text).toMatch(params.reviewRating);
			})

			.waitForExist(Selectors.ProductReviews.textPreviewAuthor, 5000)
			.getText(Selectors.ProductReviews.textPreviewAuthor, function (err, text)
			{
				expect(text).toMatch(params.reviewAuthor);
			})

			.waitForExist(Selectors.ProductReviews.textPreviewDescription, 5000)
			.getText(Selectors.ProductReviews.textPreviewDescription, function (err, text)
			{
				expect(text).toMatch(params.reviewText);
			})

			.waitForExist(Selectors.ProductReviews.textPreviewName, 5000)
			.getText(Selectors.ProductReviews.textPreviewName, function (err, text)
			{
				expect(text).toMatch(params.reviewHeadline);
			})
			.call(cb)
		;
	}		

,	verifyPDP: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textTitle, 5000)
			.getText(Selectors.ProductReviews.textTitle, function (err, text)
			{
				expect(text).toBeSameText(params.itemName);
			})
			.call(cb)
		;
	}

,	editPage: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerRating, 5000)
			.getAttribute(Selectors.ProductReviews.containerRating, 'data-value', function (err, text) 
			{
				expect(text).toMatch(params.reviewRating);
			})

			.waitForExist(Selectors.ProductReviews.inputAuthor, 5000)
			.getValue(Selectors.ProductReviews.inputAuthor, function (err, text) 
			{
				expect(text).toMatch(params.reviewAuthor);
			})

			.waitForExist(Selectors.ProductReviews.inputText, 5000)
			.getValue(Selectors.ProductReviews.inputText, function (err, text) 
			{
				expect(text).toMatch(params.reviewText);
			})

			.waitForExist(Selectors.ProductReviews.inputHeadline, 5000)
			.getValue(Selectors.ProductReviews.inputHeadline, function (err, text) 
			{
				expect(text).toMatch(params.reviewHeadline);
			})
			.call(cb)
		;
	}

,	checkErrors: function (cb)
	{
		this
			.ProductReviews.Expects.attributeError()
			.ProductReviews.Expects.ratingError()
			.ProductReviews.Expects.authorError()
			.ProductReviews.Expects.textError()
			.ProductReviews.Expects.titleError()			
			.call(cb)
		;
	}

,	attributeError: function (cb)
	{
		var client = this;
		var i = 0;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerAttributeTitles, 5000)
			.getAttribute(Selectors.ProductReviews.containerAttributeTitles, "for", function (err, text)
			{
				text = (Array.isArray(text))? text: [text];
				text.forEach(function()
				{
					client.call(function ()
					{
						client.getAttribute(Selectors.ProductReviews.containerSpecificAttributeFill.replace('%s', text[i]), 'style', function (err, width)
						{
							if (width == 'width: 0%;')
							{
								client
									.waitForExist(Selectors.ProductReviews.textAttributeError.replace('%s', text[i]), 5000)
									.getText(Selectors.ProductReviews.textAttributeError.replace('%s', text[i]), function (err, message)
									{
										expect(message).toMatch('Attribute is required');
									})
								;
							}
							i++;
						})	
					})	
					;
				})
				
			})
			.call(cb)
		;	
	}	

,	ratingError: function (cb)
	{
		client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerAttributeFill)
			.getAttribute(Selectors.ProductReviews.containerAttributeFill, 'style', function (err, text)
			{
				if (text == 'width: 0%;')
				{
					client
						.waitForExist(Selectors.ProductReviews.textRatingError, 5000)
						.getText(Selectors.ProductReviews.textRatingError, function (err, text)
						{
							expect(text).toMatch('Rating is required');
						})
					;
				}
			})
			
			.call(cb)
		;
	}

,	authorError: function (cb)
	{
		client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textPreviewAuthor)
			.getText(Selectors.ProductReviews.textPreviewAuthor, function (err, text)
			{
				if (undefined != text)
				{
					client
						.waitForExist(Selectors.ProductReviews.textAuthorError, 5000)
						.getText(Selectors.ProductReviews.textAuthorError, function (err, text)
						{
							expect(text).toMatch('Writer is required');
						})
					;
				}
			})
			
			.call(cb)
		;
	}

,	textError: function (cb)
	{
		client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textPreviewDescription)
			.getText(Selectors.ProductReviews.textPreviewDescription, function (err, text)
			{
				if (undefined != text)
				{
					client
						.waitForExist(Selectors.ProductReviews.textTextError, 5000)
						.getText(Selectors.ProductReviews.textTextError, function (err, text)
						{
							expect(text).toMatch('Text is required');
						})
					;
				}
			})
			
			.call(cb)
		;
	}

,	titleError: function (cb)
	{
		client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textPreviewName)
			.getText(Selectors.ProductReviews.textPreviewName, function (err, text)
			{
				if (undefined != text)
				{
					client
						.waitForExist(Selectors.ProductReviews.textTitleError, 5000)
						.getText(Selectors.ProductReviews.textTitleError, function (err, text)
						{
							expect(text).toMatch('Title is required');
						})
					;
				}
			})
			
			.call(cb)
		;
	}	

,	yesHelpfulToIncrease: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				expect(parseInt(text.replace(/\D*/g, ''))).toBe(params.reviewYesCount + 1);
			})
			.call(cb)
		;
	}	

,	noHelpfulToIncrease: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				expect(parseInt(text.replace(/\D*/g, ''))).toBe(params.reviewNoCount + 1);
			})
			.call(cb)
		;
	}	

,	yesHelpfulToNotIncrease: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewYesHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				expect(parseInt(text.replace(/\D*/g, ''))).toBe(params.reviewYesCount);
			})
			.call(cb)
		;
	}	

,	noHelpfulToNotIncrease: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), 5000)
			.getText(Selectors.ProductReviews.buttonReviewNoHelpfulCounter.replace('%s', params.reviewNumber), function (err, text)
			{
				expect(parseInt(text.replace(/\D*/g, ''))).toBe(params.reviewNoCount);
			})
			.call(cb)
		;
	}	

	// Amount of stars is indicated by the % filled
	// width: 100%; = 5 stars
	// width: 80%; = 4 stars
	// width: 60%; = 3 stars
	// width: 40%; = 2 stars
	// width: 20%; = 1 stars
,	amountOfStarsDisplayed: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerReviewsStars, 5000)
			.getAttribute(Selectors.ProductReviews.containerReviewsStars, 'style', function (err, text) 
			{
				if(typeof text === 'string')
				{
					text = text.split();
				}
				
				for (i = 0, count = text.length; i < count; ++i) 
				{
					expect(params).toMatch(text[i]);
				}
			})
			.call(cb)
		;
	}	

,	orderedByDate: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textDateReviews, 5000)
			.getText(Selectors.ProductReviews.textDateReviews, function (err, text)
			{
				if(typeof text === 'string')
				{
					text = text.split();
				}

				for (i = 0, count = text.length; i < count-1; ++i)
				{
					expect(new Date(text[i])).toBeBefore(new Date(text[i+1]));
				}
			})
			.call(cb)
		;
	}

,	orderedByDateInversed: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.textDateReviews, 5000)
			.getText(Selectors.ProductReviews.textDateReviews, function (err, text)
			{
				if(typeof text === 'string')
				{
					text = text.split();
				}

				for (i = 0, count = text.length; i < count-1; ++i)
				{
					expect(new Date(text[i])).toBeAfter(new Date(text[i+1]));
				}
			})
			.call(cb)
		;
	}	

,	orderedByRating: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerReviewsStars, 5000)
			.getAttribute(Selectors.ProductReviews.containerReviewsStars, 'style', function (err, text)
			{
				if(typeof text === 'string')
				{
					text = text.split();
				}

				for (i = 0, count = text.length; i < count-1; ++i)
				{
					initValue = parseInt(text[i].replace(/\D*/g, ''));
					nextValue = parseInt(text[i+1].replace(/\D*/g, ''));
					if(initValue != nextValue)
					{
						expect(initValue).toBeGreaterThan(nextValue);					
					}					
				}
			})
			.call(cb)
		;
	}

,	orderedByRatingInversed: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerReviewsStars, 5000)
			.getAttribute(Selectors.ProductReviews.containerReviewsStars, 'style', function (err, text)
			{
				if(typeof text === 'string')
				{
					text = text.split();
				}

				for (i = 0, count = text.length; i < count-1; ++i)
				{
					initValue = parseInt(text[i].replace(/\D*/g, ''));
					nextValue = parseInt(text[i+1].replace(/\D*/g, ''));
					if(initValue != nextValue)
					{
						expect(initValue).toBeLessThan(nextValue);					
					}					
				}
			})
			.call(cb)
		;
	}

	// Amount of stars is indicated by the % filled
	// width: 100%; = 5 stars
	// width: 80%; = 4 stars
	// width: 60%; = 3 stars
	// width: 40%; = 2 stars
	// width: 20%; = 1 stars
,	amountOfStarsDisplayedAttribute: function (params, cb)
	{		
		this
			.waitForAjax()
			.waitForExist(Selectors.ProductReviews.containerReviewsStarsAttribute, 5000)
			.getAttribute(Selectors.ProductReviews.containerReviewsStarsAttribute, 'style', function (err, text) 
			{
				expect(params).toMatch(text);
			})
			.call(cb)
		;
	}	

});