// @module RequestQuoteWizard
defineHelper('RequestQuoteWizard',
{
	// @method clickRequestAQuote Clicks the Request a Quote link
	// @return {Void}
	clickRequestAQuote: function (cb)
	{
		this
			.click(Selectors.RequestQuoteAccessPoints.linkRequestAQuote)
			.call(cb)
		;
	}

	// @method removeItem Remove an item by internalId
	// @params {Number} internalId
	// @return {Void}
,	removeItem: function (internalId, cb)
	{
		this
			// Clicks outside to lose focus and validate
			.click(Selectors.RequestQuoteWizard.textItemCounter)
			.click(Selectors.RequestQuoteWizard.btnRemoveInternalId.replace('%s', internalId))
			.call(cb)
		;
	}

	// @method removeAllItems Removes all items
	// @return {Void}
,	removeAllItems: function (cb)
	{
		client = this;
		this
			.isExisting(Selectors.RequestQuoteWizard.btnRemove)
			.then(function (existing)
			{
				if (existing)
				{
					client
						.click(Selectors.RequestQuoteWizard.btnRemove)
						.RequestQuoteWizard.removeAllItems()
						.call(cb)
					;
				}
			})
			.call(cb)
		;
	}

	// @method clickSubmit
	// @return {Void}
,	clickSubmit: function (cb)
	{
		this
			.click(Selectors.RequestQuoteWizard.btnSubmit)
			.call(cb)
		;
	}

});