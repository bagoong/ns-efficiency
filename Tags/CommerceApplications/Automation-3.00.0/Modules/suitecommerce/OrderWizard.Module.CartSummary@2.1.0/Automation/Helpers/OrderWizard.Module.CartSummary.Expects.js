'use strict';

defineHelper('OrderWizard.Module.CartSummary.Expects',
{
	checkExpectedCartSummary: function (cb)
	{
		var client = this;

		this
			.timeoutsAsyncScript(15000)
			.executeAsync(function (done)
			{
				var suiteScriptService = 'services/LiveOrder.Service.ss?internalid=cart&c=' + SC.ENVIRONMENT.companyId + '&n=' + SC.ENVIRONMENT.siteSettings.siteid;
				var routeString = SC.ENVIRONMENT.baseUrl.replace('{{file}}', suiteScriptService); 

				var url = 'https://' + SC.ENVIRONMENT.currentHostString + routeString;

				jQuery.get(url, function (data)
				{
					done(data);
				});
			}, function (err, data)
			{
				//console.log(JSON.stringify(data,null,4));
				if(!err){
					var expected_data = {
						shipping: data.value.summary.shippingcost
					,	tax: data.value.summary.taxtotal
					,	handling: data.value.summary.handlingcost
					,	discount: data.value.summary.discounttotal
					,	subtotal: data.value.summary.subtotal
					, 	total: data.value.summary.total
					};

					client
						.OrderWizard.Module.CartSummary.getSummaryDataType(function (err, datatype)
						{
							expect(datatype).toHaveEqualFields(expected_data);
						})
					;
				}
			})
			.call(cb)
		;
	}
});

