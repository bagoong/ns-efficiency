// @polarion PSGE-1490

// CHECKOUT AS GUEST TESTS

describe('Site Builder - Checkout as Guest', function() 
{
	// former checkoutAsGuestSB1
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}
				
				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	// TEST FLOW
    client
    	.ItemDetails.SB.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)
		
		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
