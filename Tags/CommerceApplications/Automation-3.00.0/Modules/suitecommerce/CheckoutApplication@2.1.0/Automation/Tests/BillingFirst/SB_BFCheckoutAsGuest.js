// @polarion PSGE-1491

// BILLING FIRST CHECKOUT AS GUEST
describe('Site Builder - Billing First Checkout as Guest', function() 
{
	// former checkoutAsGuestSB1
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_billing_first_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'website': configuration	
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


function doTest(done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.SB.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		// BILLING ADDRESS
		
		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// DELIVERY

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})
		
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.billingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
