// ONE PAGE CHECKOUT AS GUEST TESTS


describe('One Page Checkout with Skip Login', function() 
{
	// former checkoutAsGuestSCA2
	it('Should checkout using promocode and checking same shipping-billing address checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'		
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				,	'promoCode': promoCode
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()


		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)

		.Address.markSameShippingAddress()


		.OrderWizard.Module.Register.fillGuestEmail(dataset.guest.email)


		// WAITING FOR DELIVERY METHOD ENABLED IN OPC IS REALLY SLOW!
		.OrderWizard.Module.Shipmethod.waitForDeliveryMethodEnabled()


		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})	

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)		

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.shippingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
