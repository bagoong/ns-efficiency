// CHECKOUT AS RETURNING CUSTOMER AND PAY WITH INVOICE
//@polarion PSGE-886


describe('Checkout Invoice Payment', function ()
{
	var failed_preconditions = false;
	var common_dataset = {};

	beforeAll(function(done)
	{
		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_customer_registration_data'
		,	'get_one_preferred_invoice_terms'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	guestCustomer
			,	invoiceTerms
			)
			{
				common_dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'configuration': configuration		
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				
				,	'invoice': {
						'terms': invoiceTerms.name
					,	'purchaseNumber': '4u70m4t1on'
					}
				}

				done();
			}
		);
	});


	// former standardPayWithInvoice
	it('Should checkout as a guest and pay with an invoice', function (done)
	{
		common_dataset.guest.email = common_dataset.guest.email.replace(/.*?\@/, this.client.util.timestamp() + '@');
		doTest(done, this.client, common_dataset);
	});

	// former standardPayWithInvoice1
	it('Should checkout as a guest and pay with a purchase-numbered invoice', function (done)
	{
		var custom_dataset = this.client.util.cloneData(common_dataset);

		custom_dataset.invoice.purchaseNumber = '';
		custom_dataset.guest.email = custom_dataset.guest.email.replace(/.*?\@/, this.client.util.timestamp() + '@');

		doTest(done, this.client, custom_dataset);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()

		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT METHOD
		.Address.markSameShippingAddress()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod({
			value: 'invoice'
		})

		.OrderWizard.Module.PaymentMethod.fillPurchaseOrderNumber(
			dataset.invoice.purchaseNumber
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.Expects.checkExpectedInvoice(
			dataset.invoice
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}

