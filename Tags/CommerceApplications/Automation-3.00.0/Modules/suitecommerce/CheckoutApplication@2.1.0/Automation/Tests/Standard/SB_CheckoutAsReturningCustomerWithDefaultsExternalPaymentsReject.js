// CHECKOUT AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES


describe('Checkout as Returning Customer', function()
{
	// former checkoutAsReturningCustomerSCA1
	it('With External Payment Rejected', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'get_one_external_payment_method'


		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	externalPayment

			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'externalPayment': externalPayment

				}

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.SB.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()

		.LoginRegister.SB.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		.Address.Expects.checkExpectedAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.Address.Expects.checkExpectedAddress(
			dataset.defaultBillingAddress
		)

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod({
			value: "external_checkout_" + dataset.externalPayment.key
		})

		.OrderWizard.clickContinue()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethodExternal.chooseStatus('REJECT')

		.OrderWizard.Module.PaymentMethodExternal.clickConfirmOrder()

		.OrderWizard.Module.PaymentMethod.Expects.checkErrorMessageIsDisplayed()

		.call(done)
	;
}
