// CHECKOUT AS GUEST TESTS


describe('Checkout as Guest', function() 
{

	// former checkoutAsGuest_viewCartEstimatedTotal_SCA
	it('Checks if estimated total in View Cart remains hidden', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_customer_registration_data'

		,	function (
				err
			,	configuration
			,	inventoryItem
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'sca_configuration_with_standard_checkout': configuration		
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				}

				//Preconditions.Item.showItemData(dataset.items);
				doTest(done, client, dataset);
			}
		);
		
	});

});


var doTest = function(done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.addProducts(
			dataset.items
		)

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.OrderSummary.Expects.checkThatEstimatedTotalIsNotVisible()

		.Cart.clickProceedToCheckout()

		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		.OrderWizard.Module.CartSummary.clickEditCart()

		.Header.goToShopping()

		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.OrderSummary.Expects.checkThatEstimatedTotalIsNotVisible()

		.Header.signOut()

		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.OrderSummary.Expects.checkThatEstimatedTotalIsNotVisible()

		.call(done)
	;
}

