// ONE PAGE CHECKOUT AS RETURNING CUSTOMER TESTS

describe('One Page Checkout as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA1
	it('WITH defaults', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				}

				//console.log(dataset);
				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
})


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE

		.LoginRegister.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		// INFORMATION PAGE

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.OrderWizard.Module.Shipmethod.chooseMethod()

		//.CreditCard.fillCreditCard(
		//	dataset.creditCard
		//)
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}