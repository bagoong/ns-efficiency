// CHECKOUT AS GUEST TESTS


describe('Checkout Multi Ship To as Guest', function() 
{
	// checkoutMST_StandardFlow_SanityTest
	// checkoutMST_BasicTest1
	// checkoutMST_BasicTest2
	xit('WITH minimum and mandatory steps', function(done)
	{
		doTest(done, this.client, test_dataset);
	});

});


function doTest(done, client, dataset)
{
	'use strict';

	// PREPARE TEST DATA
	dataset.guest.email = dataset.guest.email.replace(/.*?\@/, client.util.timestamp() + '@');

	// TEST FLOW
	client
		.ItemDetails.addProduct({ 
			baseUrl: dataset.baseUrl
		,	name: 'Worlds_Finest'
		,	quantity: 1
		})

		.ItemDetails.addProduct({ 
			baseUrl: dataset.baseUrl
		,	name: 'Hulk'
		,	quantity: 2
		})

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		.OrderWizard.Module.CartSummary.getSummaryDataType(function(err, datatype)
		{	
			//console.log(datatype);
		})

		.OrderWizard.Module.MultiShipTo.EnableLink.Expects.checkIfMultiShipToLinkIsPresent()

		.OrderWizard.Module.MultiShipTo.EnableLink.clickShipToMultipleAddresses()

		.OrderWizard.Module.MultiShipTo.EnableLink.Expects.checkIfSingleShipToLinkIsPresent()
		
		// FILL FIRST MULTI SHIP TO ADDRESS

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// FILL SECOND MULTI SHIP TO ADDRESS
		.OrderWizard.Module.Address.clickAddNewShippingAddress()

		
		.Address.fillAddress(
			dataset.newShippingAddress
		)

		.Address.clickSaveAddress()

		.OrderWizard.clickContinue()

		// SELECT SHIPMENTS

		.OrderWizard.Module.Package.getShippableItems()

		.call(done)
		
		//.OrderWizard.Module.Package.clickCreateShipment()

		//.pause(5000000)

		// CHOOSE SHIPMETHOD
		
		/*
		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
		*/
	;
}


var test_dataset = {
	'baseUrl': 'http://chksf.crovetto.me/'

,	'guest': {
			'name': 'Jane'
		,	'lastName': 'MultiShip'
		,	'email': 'timestamp@automationbacon.com'
	}

,	'shippingAddress': {
			'fullname': 'My Shipping Name'
		,	'company': 'My Shipping Company'
		,	'addr1': 'Mings Street 123'
		,	'addr2': ''
		,	'city': 'Mongo City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '12345678901234'
	}

,	"newShippingAddress": {
			"fullname": "My Other Shipping"
		,	"company": "My Other Shipping Company"
		,	"addr1": "New Street 123"
		,	"addr2": ""
		,	"city": "New Mocha City"
		,	"country": "United States"
		,	"state": "New York"
		,	"zip": "J1934"
		,	"phone": "1234901234"
	}	

,	'billingAddress': {
			'fullname': 'My Billing Name'
		,	'company': 'My Billing Company'
		,	'addr1': 'Mindafggs Street 123'
		,	'addr2': ''
		,	'city': 'Billing City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '1234901234'
	}

,	'creditCard': {
			'id': '123'
		,	'cardTypeId': '8'
		,	'cardType': 'visa'
		,	'cardNumber': '4111111111111111'
		,	'cardEnding': '1111'
		,	'expMonth': '7'
		,	'expYear': '2018'
		,	'securityNumber': '123'
		,	'name': 'Flash Gordon'
	}

,	'shippingMethod': {
			"name" : "Door to Door"
	}		
};
