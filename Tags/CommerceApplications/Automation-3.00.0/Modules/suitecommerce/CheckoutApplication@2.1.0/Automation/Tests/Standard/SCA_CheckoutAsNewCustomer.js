// CHECKOUT AS NEW CUSTOMER TESTS

describe('Checkout as New Customer', function() 
{
	// NEW TEST, not present in former checkoutAsNewCustomerSCA test suite
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	shippingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'newCustomer': newCustomer
				,	'address': shippingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function(done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()

        .LoginRegister.clickCreateAccount()     

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// SHIPPING PAGE

		.Address.fillAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()
		
		// DELIVERY

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.clickSelectThisAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE

		.Address.Expects.checkExpectedBillingAddress(
			dataset.address
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
