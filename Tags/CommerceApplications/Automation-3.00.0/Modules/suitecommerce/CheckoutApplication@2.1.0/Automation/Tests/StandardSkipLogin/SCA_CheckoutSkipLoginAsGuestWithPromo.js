// CHECKOUT AS GUEST SKIP LOGIN TESTS

describe('Checkout Skip Login as Guest', function() 
{
	// former checkoutAsGuestSCA2
	it('WITH promo, WITH same shipping-billing checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'promoCode': promoCode	
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});	
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.clickProceedToCheckout()
		
		// LOGIN MESSAGE
		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		// SHIPPING ADDRESS PAGE
		
		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		.OrderWizard.clickContinue()

		
		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})	

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.markSameShippingAddress()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.Module.Register.fillGuestEmail(dataset.guest.email)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}



