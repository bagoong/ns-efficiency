// BILLING FIRST CHECKOUT SKIP LOGIN AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES

describe('Billing First Checkout Skip Login as Returning Customer', function()
{
	// former checkoutAsReturningCustomerSCA6
	it('Create new billing address and USE it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_billing_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newBillingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newBillingAddress': newBillingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()
		
		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.fillLogin({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		.LoginRegister.clickSubmitLogin()
		
		// BILLING ADDRESS PAGE

		.OrderWizard.Module.Address.hasSelectedBillingAddress(function(err, has_selected)
		{
			if (has_selected) 
			{
				client.OrderWizard.Module.Address.clickChangeBillingAddress();
			}
		})

		.Address.removePreviousAddresses(
			dataset.newBillingAddress
		)
		
		.OrderWizard.Module.Address.clickAddNewBillingAddress()
		
		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// WE -> USE <- THE RECENTLY CREATED BILLING ADDRESS
		.Address.clickSelectThisAddress(
			dataset.newBillingAddress
		)
		
		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// DELIVERY

		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		// PAYMENT AND BILLING ADDRESS
		/*
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		*/
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.newBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}

