// @polarion PSGE-1490

// CHECKOUT AS GUEST TESTS

'use strict';


describe('Checkout as Guest', function ()
{
	// former checkoutAsGuestSCA1
	it('Validate Confirmation Page after standard checkout as guest', function (done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'website': configuration
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	client
		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()

		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		//.OrderWizard.Module.CartSummary.Expects.checkExpectedCartSummary()

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Module.Confirmation.Expects.checkExpectedThankYouPage()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedContinueButton()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedDownloadPDFButton()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedSummaryPresence()

		.call(done)
	;
}


