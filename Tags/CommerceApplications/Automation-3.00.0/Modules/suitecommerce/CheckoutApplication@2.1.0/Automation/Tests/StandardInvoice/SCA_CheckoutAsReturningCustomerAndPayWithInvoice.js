// CHECKOUT AS RETURNING CUSTOMER AND PAY WITH INVOICE
//@polarion PSGE-886


describe('Checkout Invoice Payment', function()
{
	var failed_preconditions = false;	
	var common_dataset = {};

	beforeAll(function(done)
	{
		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'get_one_preferred_invoice_terms'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	invoiceTerms
			)
			{
				common_dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'invoice': {
						'terms': invoiceTerms.name
					,	'purchaseNumber': '4u70m4t1on'
					}
				}

				done();
			}
		);
	});

	// former standardPayWithInvoice
	it('Should checkout as returning customer and pay with an invoice', function(done)
	{
		doTest(done, this.client, common_dataset);
	});

	// former standardPayWithInvoice1
	it('Should checkout as returning customer and pay with a purchase-numbered invoice', function(done)
	{
		var custom_dataset = this.client.util.cloneData(common_dataset);
		custom_dataset.invoice.purchaseNumber = ""

		doTest(done, this.client, custom_dataset);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()

		.LoginRegister.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		.Address.Expects.checkExpectedAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.Address.Expects.checkExpectedAddress(
			dataset.defaultBillingAddress
		)

		// PAYMENT METHOD

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod({
			value: "invoice"
		})

		.OrderWizard.Module.PaymentMethod.fillPurchaseOrderNumber(
			dataset.invoice.purchaseNumber
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.Expects.checkExpectedInvoice(
			dataset.invoice
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}



