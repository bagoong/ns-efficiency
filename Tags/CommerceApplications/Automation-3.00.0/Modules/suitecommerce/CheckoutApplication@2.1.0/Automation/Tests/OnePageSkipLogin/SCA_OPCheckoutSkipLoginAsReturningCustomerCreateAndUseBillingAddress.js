// ONE PAGE CHECKOUT AS RETURNING CUSTOMER TESTS

describe('One Page Checkout Skip Login as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA4
	it('Create new billing address and USE it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_billing_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newBillingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newBillingAddress': newBillingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});
});

var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.fillLogin(
			dataset.customer
		)

		.LoginRegister.clickSubmitLogin()

		// INFORMATION PAGE

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		// WAITING FOR DELIVERY METHOD ENABLED IN OPC IS REALLY SLOW!
		.OrderWizard.Module.Shipmethod.waitForDeliveryMethodEnabled()

		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		//.CreditCard.fillCreditCard(
		//	dataset.creditCard
		//)
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.Address.removePreviousAddresses(
			dataset.newBillingAddress
		)

		.OrderWizard.Module.Address.clickAddNewBillingAddress()

		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// HERE WE -> USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickSelectThisAddress(
			dataset.newBillingAddress
		)

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.newBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}

