// BILLING FIRST CHECKOUT AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES

describe('Billing First Checkout as New Customer', function() 
{
	// NEW TEST, not present in former checkoutAsNewCustomerSCA test suite
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	shippingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'newCustomer': newCustomer
				,	'address': shippingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.clickCreateAccount()     

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// BILLING ADDRESS
		
		.Address.fillAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.Address.clickShipToThisAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		// DELIVERY
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})
		
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.address
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


