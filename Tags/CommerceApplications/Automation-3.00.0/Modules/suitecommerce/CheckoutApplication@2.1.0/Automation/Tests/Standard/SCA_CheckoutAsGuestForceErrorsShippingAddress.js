// CHECKOUT AS GUEST TESTS

var commonDataset = {};


describe('Standard Checkout as Guest FORCING ERRORS', function() 
{
	beforeAll(function(done)
	{
		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	guestCustomer
			)
			{
				commonDataset = {
					'baseUrl': configuration.website.baseUrl
				,	'configuration': configuration		
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				}

				done();
			}
		);
	});

	// checkoutAsGuestSCA_emptyShippingAddress
	it('Shipping address has empty required fields Errors', function(done)
	{
		commonDataset.addressField = [
			{
				'label': 'Full Name'
			,	'name': 'fullname'
			,	'errorLabel': 'Full Name is required'
			}
		,	{
				'label': 'City'
			,	'name': 'city'
			,	'errorLabel': 'City is required'
			}

		,	{
				'label': 'Phone Number'
			,	'name': 'phone'
			,	'errorLabel': 'Phone is required'
			}
		,	{
				'label': 'Zip Code'
			,	'name': 'zip'
			,	'errorLabel': 'Zip Code is required'
			}
		]

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyFullNameShipping
	it('Shipping address has empty Full Name Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Full Name'
		,	'name': 'fullname'
		,	'errorLabel': 'Full Name is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyCityShipping
	it('Shipping address has empty City Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'City'
		,	'name': 'city'
		,	'errorLabel': 'City is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyStateShipping
	it('Shipping address has empty State Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'State'
		,	'name': 'state'
		,	'errorLabel': 'State is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	// checkoutAsGuestSCA_emptyCountryShipping
	it('Shipping address has empty Country Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Country'
		,	'name': 'country'
		,	'errorLabel': 'Country is required'
		}

		doTest(done, this.client, commonDataset);
	});		


	// checkoutAsGuestSCA_emptyAddressLine1Shipping
	it('Shipping address has empty Address Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Address'
		,	'name': 'addr1'
		,	'errorLabel': 'Address is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	// checkoutAsGuestSCA_emptyZipCodeShipping
	it('Shipping address has empty ZipCode Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Zip Code'
		,	'name': 'zip'
		,	'errorLabel': 'Zip Code is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyPhoneShipping
	it('Shipping address has empty Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	//checkoutAsGuestSCA_veryLongPhoneShipping
	it('Shipping address has very long number Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone Number is invalid'
		,	'errorValue': '455444444444444444444444444444233432423444444444444444444444444444444444444'
		}

		doTest(done, this.client, commonDataset);
	});	


	//checkoutAsGuestSCA_lettersInPhoneShipping
	it('Shipping address has letters in Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone Number is invalid'
		,	'errorValue': 'jhsdfjkhsdfjsfdjkjkdsfjkdsf'
		}

		doTest(done, this.client, commonDataset);
	});	

});


var doTest = function (done, client, dataset)
{
	'use strict';

	// PREPARE TEST DATA
	dataset = client.util.cloneData(dataset);
	dataset.guest.email = dataset.guest.email.replace(/.*?\@/, client.util.timestamp() + '@');
	dataset.addressField = (Array.isArray(dataset.addressField))? dataset.addressField : [dataset.addressField];

	dataset.addressField.forEach(function(addressField)
	{
		dataset.shippingAddress[addressField.name] = addressField.errorValue || '';
	});

	// TEST FLOW
    client
		.ItemDetails.addProducts(
			dataset.items
		)
		//.ItemDetails.addProduct('DownloadItem')	
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		.call(function() {
			dataset.addressField.forEach(function(addressField)
			{
				client
					.Address.Expects.checkExpectedShippingAddressFieldError(addressField)
				;
			});
		})

		.call(done)
	;
}


