// BILLING FIRST CHECKOUT SKIP LOGIN AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES

describe('Billing First Checkout Skip Login as New Customer', function()
{
	// newCustomerBillingFirst3
	it('Create new shipping address and and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'generate_one_billing_address_data'
		,	'generate_one_new_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	billingAddress
			,	newShippingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode	
				,	'newCustomer': newCustomer
				,	'billingAddress': billingAddress
				,	'newShippingAddress': newShippingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()
		
		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.clickRegisterLinkInLoginModal()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// BILLING ADDRESS
		
		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.OrderWizard.Module.Address.clickAddNewShippingAddress()
		
		.Address.fillAddress(
			dataset.newShippingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickShipToThisAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// DELIVERY
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})
		
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.billingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
