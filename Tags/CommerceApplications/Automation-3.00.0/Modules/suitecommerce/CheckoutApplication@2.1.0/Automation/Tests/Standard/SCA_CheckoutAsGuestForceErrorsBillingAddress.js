// CHECKOUT AS GUEST TESTS

var commonDataset = {};

describe('Standard Checkout as Guest FORCING ERRORS', function() 
{
	beforeAll(function(done)
	{
		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				commonDataset = {
					'baseUrl': configuration.website.baseUrl
				,	'configuration': configuration		
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				done();
			}
		);
	});

	// checkoutAsGuestSCA_emptyBillingAddress
	it('Billing address has empty required fields Errors', function(done)
	{
		commonDataset.addressField = [
			{
				'label': 'Full Name'
			,	'name': 'fullname'
			,	'errorLabel': 'Full Name is required'
			}
		,	{
				'label': 'City'
			,	'name': 'city'
			,	'errorLabel': 'City is required'
			}

		,	{
				'label': 'Phone Number'
			,	'name': 'phone'
			,	'errorLabel': 'Phone is required'
			}
		,	{
				'label': 'Zip Code'
			,	'name': 'zip'
			,	'errorLabel': 'Zip Code is required'
			}
		]

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyFullNameBilling
	it('Billing address has empty Full Name Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Full Name'
		,	'name': 'fullname'
		,	'errorLabel': 'Full Name is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyCityBilling
	it('Billing address has empty City Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'City'
		,	'name': 'city'
		,	'errorLabel': 'City is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyStateBilling
	it('Billing address has empty State Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'State'
		,	'name': 'state'
		,	'errorLabel': 'State is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	// checkoutAsGuestSCA_emptyCountryBilling
	it('Billing address has empty Country Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Country'
		,	'name': 'country'
		,	'errorLabel': 'Country is required'
		}

		doTest(done, this.client, commonDataset);
	});		


	// checkoutAsGuestSCA_emptyAddressLine1Billing
	it('Billing address has empty Address Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Address'
		,	'name': 'addr1'
		,	'errorLabel': 'Address is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	// checkoutAsGuestSCA_emptyZipCodeBilling
	it('Billing address has empty ZipCode Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Zip Code'
		,	'name': 'zip'
		,	'errorLabel': 'Zip Code is required'
		}

		doTest(done, this.client, commonDataset);
	});


	// checkoutAsGuestSCA_emptyPhoneBilling
	it('Billing address has empty Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone is required'
		}

		doTest(done, this.client, commonDataset);
	});	


	//checkoutAsGuestSCA_veryLongPhoneBilling
	it('Billing address has very long number Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone Number is invalid'
		,	'errorValue': '455444444444444444444444444444233432423444444444444444444444444444444444444'
		}

		doTest(done, this.client, commonDataset);
	});	


	//checkoutAsGuestSCA_lettersInPhoneBilling
	it('Billing address has letters in Phone Error', function(done)
	{
		commonDataset.addressField = {
			'label': 'Phone Number'
		,	'name': 'phone'
		,	'errorLabel': 'Phone Number is invalid'
		,	'errorValue': 'jhsdfjkhsdfjsfdjkjkdsfjkdsf'
		}

		doTest(done, this.client, commonDataset);
	});	

});



var doTest = function(done, client, dataset)
{
	'use strict';

	// PREPARE TEST DATA
	dataset = client.util.cloneData(dataset);

	dataset.addressField = (Array.isArray(dataset.addressField))? dataset.addressField : [dataset.addressField];

	dataset.addressField.forEach(function(addressField)
	{
		dataset.billingAddress[addressField.name] = addressField.errorValue || '';
	});

	// TEST FLOW
    client
		.ItemDetails.addProducts(
			dataset.items
		)

		//.ItemDetails.addProduct('DownloadItem')	
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		.call(function() {
			dataset.addressField.forEach(function(addressField)
			{
				client
					.Address.Expects.checkExpectedFieldError(
						addressField
					)
				;
			});
		})

		.call(done)
	;
}


