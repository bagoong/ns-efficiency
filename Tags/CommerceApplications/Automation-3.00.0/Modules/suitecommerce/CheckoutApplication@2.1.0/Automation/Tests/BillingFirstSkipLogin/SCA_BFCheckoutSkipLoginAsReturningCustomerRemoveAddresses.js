// BILLING FIRST CHECKOUT SKIP LOGIN AS RETURNING CUSTOMER REMOVE ADDRESSES

describe('Billing First Checkout Skip Login as Returning Customer', function()
{
	// removeShippingAddressTest
	it('Should remove all Returning Customer\'s previous addresses', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_shipping_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newBillingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newBillingAddress': newBillingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()
		
		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.fillLogin(
			dataset.customer
		)

		.LoginRegister.clickSubmitLogin()
		
		// BILLING ADDRESS PAGE
		.waitForAjax()
		.waitForAjax()

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.call(function()
		{
			var new_extra_address = null;

			for (var i=2; i < 4; i++)
			{
				new_extra_address = client.util.cloneData(dataset.defaultBillingAddress);

				new_extra_address.fullname = "New #" + i + ' ' + new_extra_address.fullname
				new_extra_address.company = "New #" + i + ' ' + new_extra_address.company

				client
					.OrderWizard.Module.Address.clickAddNewBillingAddress()
					.Address.fillAddress(
						new_extra_address
					)
					.Address.clickSaveAddress()
				;
			}

			client.Address.clickSelectThisAddress(
				new_extra_address
			);
		})

		.OrderWizard.clickContinue()

		.waitForAjax()
		.waitForAjax()

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.Address.removePreviousAddresses()

		.OrderWizard.Module.Address.clickAddNewBillingAddress()

		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()
			
		.Address.clickSelectThisAddress(
			dataset.newBillingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.clickContinue()

		// SHIP METHOD

		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		// PAYMENT METHOD
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.Address.Expects.checkExpectedBillingAddress(
			dataset.newBillingAddress
		)
		
		.call(done)
	;
}

