// CHECKOUT LOGIN TESTS

var _ = require('underscore');

var commonDataset = {};

describe('Checkout Login', function() 
{
	beforeAll(function(done)
	{
		Preconditions.start(
			'sca_configuration'
		,	'get_one_inventory_non_matrix_item'
		,	'create_one_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	customer
			)
			{
				commonDataset = {
					'baseUrl': configuration.website.baseUrl
				,	'configuration': configuration		
				,	'items': [
						inventoryItem
					]
				,	'customer': customer
				}

				done();
			}
		);
	});

	// emptyUserAndPasswordTest
	it('Show error on empty user and password', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : ''  
			,	'password': ''				
			}

		,	'loginField': [
				{
					'name': 'email'
				,	'errorLabel': 'Valid Email is required'
				}
			,	{
					'name': 'password'
				,	'errorLabel': 'Please enter a valid password'
				}				
			]
		});

		doTest(done, this.client, customDataset);
	});


	// nonExistingUserTest
	it('Show error on non-existing user', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : 'nonexistent_' + this.client.util.timestamp() + '@automationbacon.com' 
			,	'password': 'randompass1234'				
			}

		,	'loginAlert': {
				'errorLabel': 'Incorrect email or password. Please correct your email and password, or register if you are a new customer.'			
			}
		});

		doTest(done, this.client, customDataset);
	});

	// wrongEmailFormatTest
	it('Show error on wrong email', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : 'incomplete_email@' 
			,	'password': 'randompass1234'				
			}

		,	'loginField': {
				'name': 'email'
			,	'errorLabel': 'Valid Email is required'
			}
		});

		doTest(done, this.client, customDataset);
	});


	// emptyEmailTest
	it('Show error on empty email', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : ''
			,	'password': commonDataset.customer.password		
			}

		,	'loginField': {
				'name': 'email'
			,	'errorLabel': 'Valid Email is required'
			}
		});

		doTest(done, this.client, customDataset);
	});


	// emptyPasswordTest
	it('Show error on empty password', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : commonDataset.customer.email
			,	'password': ''				
			}

		,	'loginField': {
				'name': 'password'
			,	'errorLabel': 'Please enter a valid password'
			}
		});

		doTest(done, this.client, customDataset);
	});


	// invalidPasswordTest
	it('Show error on invalid password', function(done)
	{
		var customDataset = _.defaults({}, commonDataset);

		_(customDataset).extend({
			'customer': {
				'email'   : commonDataset.customer.email 
			,	'password': 'fail_' + commonDataset.customer.password				
			}

		,	'loginAlert': {
				'errorLabel': 'Incorrect email or password. Please correct your email and password, or register if you are a new customer.'			
			}
		});

		doTest(done, this.client, customDataset);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

	// TEST FLOW
	client
		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()
		
		.LoginRegister.loginAs(
			dataset.customer
		)

		.call(function()
		{
			if (dataset.loginField)
			{
				dataset.loginField = (Array.isArray(dataset.loginField))? dataset.loginField : [dataset.loginField];

				dataset.loginField.forEach(function(loginField)
				{
					client
						.getText( 'input[name=' + loginField.name + '] + p[data-validation-error=block]', function(err, text)
						{
							expect(text).toBe( loginField.errorLabel );
						})
					;
				});
			}

			if (dataset.loginAlert)
			{
				client
					.waitForExist('.login-register-login-form-messages', 5000)
					.getText( '.login-register-login-form-messages', function(err, text)
					{
						expect(text).toBe(dataset.loginAlert.errorLabel);
					})
				;
			}

		})

		.call(done)
	;
}