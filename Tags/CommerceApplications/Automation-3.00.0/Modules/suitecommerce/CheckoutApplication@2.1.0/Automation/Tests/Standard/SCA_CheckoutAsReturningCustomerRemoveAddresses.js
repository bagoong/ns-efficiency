// CHECKOUT AS RETURNING CUSTOMER REMOVING ALL PREVIOUS ADDRESSES

describe('Checkout as Returning Customer', function() 
{
	// removeShippingAddressTest
	it('Remove all shipping addresses', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_shipping_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newShippingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newShippingAddress': newShippingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.loginAs(
			dataset.customer
		)

		.Address.getAddresses(function(err, addresses) {
			if (addresses.length > 0)
			{
				client
					.OrderWizard.Module.Address.clickChangeShippingAddressIfPreviousSelected()
					.Address.removePreviousAddresses()
				;
			} 

			client
				.Address.fillAddress(
					dataset.defaultShippingAddress
				)
				.waitForAjax()
			;
		})
		
		.OrderWizard.clickContinue()

		.OrderWizard.Module.Address.clickChangeShippingAddressIfPreviousSelected()

		.call(function()
		{
			for (var i=2; i < 4; i++)
			{
				var new_extra_address = client.util.cloneData(dataset.defaultShippingAddress);

				new_extra_address.fullname = "New #" + i + ' ' + new_extra_address.fullname

				client
					.OrderWizard.Module.Address.clickAddNewShippingAddress()
					.Address.fillAddress(
						new_extra_address
					)
					.Address.clickSaveAddress()
				;
			}
		})

		// REMOVE ALL ADDRESSES
		.Address.removePreviousAddresses()

		// SET ANOTHER NEW ADDRESS
		.Address.fillAddress(
			dataset.newShippingAddress
		)

		.waitForAjax()

		// DELIVERY METHOD LOAD WHEN ADDRESS IS CHANGED BECOMES REALLY SLOW!
		.OrderWizard.Module.Shipmethod.waitForDeliveryMethodEnabled()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.waitForAjax()

		.OrderWizard.clickContinue()

		.waitForAjax()

		// BILLING 

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.OrderWizard.Module.Address.clickAddNewBillingAddress()

		.Address.fillAddress(
			dataset.defaultBillingAddress
		)

		.Address.clickSaveAddress()

		.Address.clickSelectThisAddress(
			dataset.defaultBillingAddress
		)

		// PAYMENT METHOD
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.newShippingAddress
		)

		.OrderWizard.clickContinue()

		//.pause(10000)
		
		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}


