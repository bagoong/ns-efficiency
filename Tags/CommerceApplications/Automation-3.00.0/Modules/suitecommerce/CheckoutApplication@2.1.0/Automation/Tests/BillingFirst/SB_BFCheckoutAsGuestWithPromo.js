// @polarion PSGE-1491

// BILLING FIRST CHECKOUT AS GUEST
describe('Site Builder - Billing First Checkout as Guest', function() 
{
	// former checkoutAsGuestSB2
	it('WITH promo, WITH same shipping-billing checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_billing_first_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				,	'promoCode': promoCode
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


function doTest(done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.SB.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)

		// BILLING ADDRESS
		
		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// DELIVERY

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})
		
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.Address.markSameShippingAddress()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.shippingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
