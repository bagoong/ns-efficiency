defineHelper('Header.Expects',
{
	checkUsername: function (username, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.username, 5000)
			.getText(Selectors.Header.username, function (err, text)
			{
				expect(text).toBe(username);
			})
			.call(cb)
		;
	}

,	checkLoginLink: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.loginLink, 5000)
			.getText(Selectors.Header.loginLink, function (err, text)
			{
				expect(text).toBe("Login");
			})
			.call(cb)
		;
	}

,	checkQuotesLink: function (linkPresence, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.quotes, 5000)
			.isExisting(Selectors.Header.quotes).then(function (isExisting)
			{
				expect(isExisting).toBe(linkPresence);
			})
			.call(cb)
		;
	}

});
