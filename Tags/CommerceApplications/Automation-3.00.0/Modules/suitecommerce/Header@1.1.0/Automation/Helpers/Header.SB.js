defineHelper('Header.SB', 
{

	signOut: function(cb) 
	{
		this
			.waitForExist(Selectors.Header.SB_signOut, 5000)
			.click(Selectors.Header.SB_signOut)
			.call(cb)
		;	
	}

,	goToMyAccount: function(cb) 
	{
		this
			.waitForExist(Selectors.Header.SB_myAccount, 5000)
			.click(Selectors.Header.SB_myAccount)
			.call(cb)
		;	
	}

,	goToCart: function(cb)
	{
		this
			.waitForExist(Selectors.Header.SB_cartLink, 5000)
			.click(Selectors.Header.SB_cartLink)
			.waitForAjax()
			.call(cb)
		;
	}

});	