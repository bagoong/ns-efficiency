defineHelper('ErrorManagement.Expects',
{
	// Checks the error displayed
	// Errors can be:
	// internal: When internal error is displayed
	// notFound: When page not found error is displayed
	// forbidden: When not allowed error is displayed
	// login: When redirection to login occours
 	errorDisplayed: function (expectedError, cb)
	{
		var client = this;
		client.waitForAjax();
		// Internal Error
		if (expectedError === 'internal')
		{
			client.isExisting(Selectors.ErrorManagement.msgInternalError).then(function (exists)
			{
				expect(exists).toBeTrueOrFailWith('Internal error was not found.');
			});
		}
		// Page not found
		else if (expectedError === 'notFound')
		{
			client.isExisting(Selectors.ErrorManagement.msgNotFoundError).then(function (exists)
			{
				expect(exists).toBeTrueOrFailWith('Not found error was not found.');
			});
		}
		// Forbidden Error
		else if (expectedError === 'forbidden')
		{
			client.isExisting(Selectors.ErrorManagement.msgForbiddenError).then(function (exists)
			{
				expect(exists).toBeTrueOrFailWith('Forbidden error was not found.');
			});
		}
		// Log in
		else if (expectedError === 'login')
		{
			client.LoginRegister.isLoginRegisterPage().then(function (not_loged)
			{
				expect(not_loged).toBeTrueOrFailWith('Login page was not found.');
			});
		}
		else
		{
			console.log('You are not searching a for valid error.');
		}
		client.call(cb);
	}

});