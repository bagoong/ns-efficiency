defineHelper('ShoppingApplication.CMS', 
{
	
	getVisibleTexts: function (text, cb)
	{
		var client = this;

		var result = [];

		client
			.getText(".cms-content-text", function(err, found_contents)
			{
				if (found_contents)
				{
					if (!Array.isArray(found_contents)) 
					{
						found_contents = [found_contents];
					}

					result = found_contents;
				}

			})
			.call(function()
			{
				cb(null, result); 
			})
		;
	}	
});
