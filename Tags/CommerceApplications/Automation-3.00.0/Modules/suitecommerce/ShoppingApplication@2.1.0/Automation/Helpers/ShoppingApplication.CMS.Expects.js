defineHelper('ShoppingApplication.CMS.Expects', 
{
	checkTagContent: function(tag_selector, tag_content, cb) 
	{
		var client = this;

		client
			.isExisting(tag_selector, function(err, tag_exists) 
			{
				if (!tag_exists)
				{
					expect(tag_selector + " tag").toBe(" present.");
					client.call(cb);
				}
				else if (tag_selector.indexOf("meta") === 0)
				{
					client
						.getAttribute(tag_selector, "content", function(err, content)
						{
							expect(content).toBe(tag_content);
						})
					;
				}
				else if (tag_selector === "title" && !GLOBAL.JS_DISABLED_MODE)
				{
					client
						.getTitle(function(err, title)
						{
							expect(title).toBe(tag_content);
						})
					;
				}
				else if (GLOBAL.JS_DISABLED_MODE)
				{
					client
						.getSource(function(err, raw_html) {
							var tag_content_regexp = new RegExp("<" + tag_selector + "[^>]*>(.*?)</" + tag_selector);
							tag_content_match = raw_html.match(tag_content_regexp);
							expect(tag_content_match[1]).toBe(tag_content);
						})
					;
				}
				else
				{
					client
						.getHTML(tag_selector, false, function(err, content)
						{
							if (Array.isArray(content))
							{
								content = content[0];
							}

							expect(content).toBe(tag_content, "Failed to check content of tag " + tag_selector );
						})
					;			
				}
			})
			.call(cb);
		;
	}


,	checkTagsContent: function (tags, cb)
	{
		var client = this;

		Object.keys(tags).forEach(function(tag_selector)
		{
			var tag_content = tags[tag_selector];

			client.ShoppingApplication.CMS.Expects.checkTagContent(
				tag_selector, tag_content
			);	
		});

		client.call(cb);
	}

,	checkIfTextIsVisible: function (text, cb)
	{
		var client = this;

		client
			.ShoppingApplication.CMS.getVisibleTexts(text, function(err, found_contents)
			{
				var lc_found_contents = [];
				
				found_contents.forEach(function(val)
				{
					lc_found_contents.push(val.toLowerCase());
				});

				expect(lc_found_contents).toContain(text.toLowerCase(), "CMS text not found");
			})
			.call(cb)
		;
	}	
});
