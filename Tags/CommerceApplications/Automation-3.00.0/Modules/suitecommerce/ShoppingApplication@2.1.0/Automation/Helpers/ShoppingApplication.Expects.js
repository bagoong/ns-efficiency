defineHelper('ShoppingApplication.Expects', 
{
	// DEPRECATED AND BAD IMPLEMENTED
	// PLEASE REFER TO SiteSearch OR Facets TO DO THIS CHECK
	checkAlertString: function(alertText, cb)
	{
		this
			.waitForAjax()
			.getText(".facets-empty p strong", function(err, text){
				expect(text).toBe('<script>alert(\'Hi, this is XSS\')</script>');
			})
			.call(cb)
		;
	}
});
