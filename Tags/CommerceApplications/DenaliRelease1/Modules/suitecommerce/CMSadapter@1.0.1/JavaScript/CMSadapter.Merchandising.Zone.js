/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CMSadapter
define(
	'CMSadapter.Merchandising.Zone'
,	[	'Merchandising.ItemCollection'
	,	'Merchandising.Zone'
	,	'Merchandising.Context'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function(
		MerchandisingItemCollection
	,	MerchandisingZone
	,	MerchandisingContext

	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	//@class CMSadapter.Merchandising.Zone Adapter class that transform CMS data model into SCA merchandising data model
	//@constructor
	//@param {jQuery} $element Place holder where the merchandising zone will be rendered
	//@param {Object} options
	var CMSMerchandisingZone = function ($element, options)
	{
		var application = options && options.application
		,	layout = application && application.getLayout && application.getLayout()
		,	items_url = decodeURIComponent(options.fields.clob_merch_rule_url)
		,	CMSMerchandisingItemCollection = MerchandisingItemCollection.extend({url: items_url})
		,	exclude = [];

		if (!$element || !layout)
		{
			return;
		}

		this.$element = $element;

		// Convert CMS data into SCA Merchandising models
		this.model = new Backbone.Model(options);
		if (options.fields.boolean_exclude_current)
		{
			exclude.push('$current');
		}

		if (options.fields.boolean_exclude_cart)
		{
			exclude.push('$cart');
		}
		this.model.set('show', options.fields.number_merch_rule_count || 100);
		this.model.set('exclude', exclude);
		this.model.set('template', options.fields.string_merch_rule_template);
		this.model.set('description', options.desc);

		this.options = options;

		//@property {Application} application
		this.application = application;

		//@property {Collection} items
		this.items = new CMSMerchandisingItemCollection();

		//@property {Merchandising.Context} context
		this.context = new MerchandisingContext(layout.modalCurrentView || layout.currentView || layout);

		//@property {jQuery.Deferred} itemsAppendedToDom
		this.itemsAppendedToDom = jQuery.Deferred();

		this.initialize();
	};

	_.extend(CMSMerchandisingZone.prototype, MerchandisingZone.prototype, {

		//@method initialize We override the initialize method to NOT generate the URL from the model, as the URL is already calculate from the CMS code
		//@return {Void}
		initialize: function ()
		{
			this.addLoadingClass();
			// the listeners MUST be added before the fetch occurs
			this.addListeners();

			// fetch the items
			this.items.fetch({
				cache: true
			});
		}

		//@method appendItems We override this function to resolve the itemsAppendedToDom promise and in this way notify that the merchandising zone have finished rendering
		//@return {Void}
	,	appendItems: function ()
		{
			//Hack to solve raise condition problem with the CMS code
			this.$element = jQuery('#'+this.$element.attr('id'));
			this.$element.html('');

			MerchandisingZone.prototype.appendItems.apply(this, arguments);

			this.itemsAppendedToDom.resolve(this.$element.html());
		}
	});

	return CMSMerchandisingZone;
});

