/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module CMSadapter
define('CMSadapter.Render.HTML'
,	[	'jQuery'
	]
,	function (
		jQuery
	)
{
	'use strict';

	//@class CMSadapter.Render.HTML Class responsible to render HTML into a CMS place holder
	return {

		//@method renderContent
		//@param {jQuery} $place_holder Place holder to render the image
		//@param {CMS.Content} content
		//@return {jQuery.Deferred}
		renderContent: function ($place_holder, content)
		{
			$place_holder.html(content.fields.clob_html);
			return jQuery.Deferred().resolve(content.fields.clob_html);
		}
	};
});