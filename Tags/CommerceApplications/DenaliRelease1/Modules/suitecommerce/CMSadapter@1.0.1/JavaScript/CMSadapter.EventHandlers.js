/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CMSadapter
define(
	'CMSadapter.EventHandlers'
,	[	'CMSadapter.Utils'
	,	'CMSadapter.Render'
	,	'SC.Configuration'

	,	'jQuery'
	,	'underscore'
	]
,	function(
		CMSadapterUtils
	,	CMSadapterRender
	,	Configuration

	,	jQuery
	,	_
	)
{
	'use strict';

	//@class CMSadapter.EventHandlers
	return {

		//@method newContent Add a new CMS content into the current page, returning a promise that will completed when the rendering finish
		//@param {CMS.Content} content
		//@return {jQuery.Deferred}
		newContent: function (content, application)
		{
			// A content model is provided, and should be rendered on the current page.
			var context = _.findWhere(content.context, {id: content.current_context})
			,	sequence = context.sequence
			,	area = jQuery('[data-cms-area="'+context.area+'"]')
			,	children = area.children().not('.ns_ContentDropzone')
			,	$render_place_holder
				// NOTE: Currently, all new content should be blank by default
			,	$new_html = jQuery('<div>')
								.addClass(CMSadapterUtils.generateContentHtmlClass(content))
								.attr('id', CMSadapterUtils.generateContentHtmlID(content));

			// insert at proper index, assuming only children of area can be "content",
			// even if they have been wrapped with new HTML (by CMS internal code) it's safe to still inject based on index
			if (sequence === 0 || children.length === 0)
			{
				area.prepend($new_html);
			}
			else if (sequence > (children.length - 1))
			{
				area.append($new_html);
			}
			else
			{
				children.eq(sequence).before($new_html);
			}

			$render_place_holder = jQuery(CMSadapterUtils.generateContentHtmlID(content, true));
			return this.renderContentByType($render_place_holder, content, true, application);
		}

		//@method getSetup Returns the default values CMS needs upon startup
		//@return {CMS.InitialConfiguration}
	,	getSetup: function ()
		{
			//@class CMS.InitialConfiguration
			return {
				//@property {String} site_id
				site_id: Configuration.get('siteSettings.id')
			};
		}
		//@class CMSadapter.EventHandlers

		//@method renderPageContent Render all the CMS content of a passing page, returning a promise that get resolved when all the rendering is finished
		//@param {CMS.Page} page
		//@param {ApplicationSkeleton} application
		//@param {Boolean} optimize_render If the page begin rendered is the same as the current one we can optimize the render stage. We don't re-add the content when the selector is the same
		//@return {jQuery.Deferred}
	,	renderPageContent: function (page, application, optimize_render)
		{
			var self = this
			,	render_promises = [];

			if (page.areas instanceof String)
			{
				//Ugly fix to solve when CMS returns an String instead of an iterable object.
				page.areas = [];
			}

			_.each(page.areas, function (area)
			{
				var selector = '[data-cms-area="'+ area.name +'"]'
				,	content_renders = []
				,	ele_string = '';

				if (!(optimize_render && !jQuery(selector).is(':empty')))
				{
					// Build HTML containers for all content in the area
					_.each(area.contents, function (content)
					{
						ele_string += jQuery('<div>')
									.addClass(CMSadapterUtils.generateContentHtmlClass(content))
									.attr('id', CMSadapterUtils.generateContentHtmlID(content))
									.wrap('<div></div>')
									.parent().html();

						content_renders.push({
							selector: CMSadapterUtils.generateContentHtmlID(content, true)
						,	content: content
						});
					});

					// Inject content
					jQuery(selector).html(ele_string);

					render_promises = render_promises.concat(_.map(content_renders, function (content_to_render)
					{
						return self.renderContentByType(jQuery(content_to_render.selector), content_to_render.content, false, application);
					}));
				}
			});

			if (render_promises.length)
			{
				return jQuery.when.apply(jQuery, render_promises);
			}
			else
			{
				return jQuery.Deferred().resolve('');
			}
		}

		//@method renderContentByType Render the content of a particular content
		//@param {jQuery} $place_holder Place holder where the content is rendered
		//@param {CMS.Content} Content CMS Object containing all the information needed to render a particular CMS content
		//@param {Boolean} is_edit_preview Flag indicating is the page is being edited or not
		//@return {jQuery.Deferred} Promise that gets completed when the render finish
	,	renderContentByType: function ($place_holder, content, is_edit_preview, application)
		{
			//@class CMSAdapter.Render.Options
			var render_options = {
					//@property {Boolean} isEditPreview
					isEditPreview: is_edit_preview
					//@property {Application} application
				,	application: application
					//@property {String} placeHolderId
				,	placeHolderId: $place_holder.attr('id')
				};

			return CMSadapterRender.render(content.type, $place_holder, content, render_options);
		}
		//@class CMSadapter.EventHandlers

		//@method getPageContext Return the current PageContent to indicate the CMS engine where we are do it can determine what to show
		//@return {CMSadapter.EventHandlers.PageContext}
	,	getPageContext: function (application)
		{
			//@class CMSadapter.EventHandlers.PageContext
			return {
				//@property {String} site_id
				site_id: Configuration.get('siteSettings.id')
				//@property {String} path
			,	path: CMSadapterUtils.getPagePath()
				//@property {String} page_type
			,	page_type: application.getLayout().currentView.el.id
			};
		}
	};

});
