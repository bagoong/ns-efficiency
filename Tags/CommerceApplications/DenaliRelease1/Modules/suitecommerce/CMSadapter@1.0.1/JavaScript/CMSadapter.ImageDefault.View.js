/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CMSadapter
define(
	'CMSadapter.ImageDefault.View'
,	[	'cms_adapter_image_default.tpl'

	,	'Backbone'
	]
,	function(
		cms_adapter_image_default_tpl

	,	Backbone
	)
{
	'use strict';

	// @class CMSadapter.ImageDefault.View @extends Backbone.View
	return Backbone.View.extend({

		//@property {Function} template
		template: cms_adapter_image_default_tpl

		//@method getContext @return {CMSadapter.ImageDefault.View.Context}
	,	getContext: function ()
		{
			//@class CMSadapter.ImageDefault.View.Context
			return {
				//@property {Boolean} showStringLink
				showStringLink: !!this.options.fields.string_link
				//@property {String} stringLink
			,	stringLink: this.options.fields.string_link
				//@property {String} imageSource
			,	imageSource: this.options.fields.string_src
				//@property {Boolean} showImageAltText
			,	showImageAltText: !!this.options.fields.string_alt
				//@property {String} altString
			,	altString: this.options.fields.string_alt
			};
		}
	});
});

