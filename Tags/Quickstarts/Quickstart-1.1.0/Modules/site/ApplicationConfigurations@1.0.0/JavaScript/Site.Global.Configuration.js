define('Site.Global.Configuration', [
    'SC.Configuration',

    'item_views_option_color.tpl',
    'item_views_selected_option_color.tpl',

    'underscore'
], function SiteGlobalConfiguration(
    Configuration,
    itemViewsOptionColorTemplate,
    itemViewsSelectedOptionColorTemplate
) {
    'use strict';

    var colors = {
        'black': '#212121',
        'Black': '#212121',
        'gray': '#9c9c9c',
        'Gray': '#9c9c9c',
        'grey': '#9c9c9c',
        'Grey': '#9c9c9c',
        'white': '#ffffff',
        'White': '#ffffff',
        'brown': '#804d3b',
        'Brown': '#804d3b',
        'beige': '#eedcbe',
        'Beige': '#eedcbe',
        'blue': '#0f5da3',
        'Blue': '#0f5da3',
        'light-blue': '#8fdeec',
        'Light-blue': '#8fdeec',
        'purple': '#9b4a97 ',
        'Purple': '#9b4a97 ',
        'lilac': '#ceadd0',
        'Lilac': '#ceadd0',
        'red': '#f63440',
        'Red': '#f63440',
        'pink': '#ffa5c1',
        'Pink': '#ffa5c1',
        'orange': '#ff5200',
        'Orange': '#ff5200',
        'peach': '#ffcc8c',
        'Peach': '#ffcc8c',
        'yellow': '#ffde00',
        'Yellow': '#ffde00',
        'light-yellow': '#ffee7a',
        'Light-yellow': '#ffee7a',
        'green': '#00af43',
        'Green': '#00af43',
        'lime': '#c3d600',
        'Lime': '#c3d600',
        'teal': '#00ab95',
        'Teal': '#00ab95',
        'aqua': '#28e1c5',
        'Aqua': '#28e1c5',
        'burgandy': '#9c0633',
        'Burgandy': '#9c0633',
        'navy': '#002d5d',
        'Navy': '#002d5d',
        'cyan': '#66FFFF',
        'Cyan': '#66FFFF',
        'multi': '#111111',
        'Multi': '#111111'
    };
    /* Add your global config here. It will be merged in the specific app config, not here */
    return {
        colors: colors,
        itemOptions: [{
            // update the name of the custcol here in order to get this working correctly
            cartOptionId: 'custcol1',
            label: 'Color',
            url: 'color',
            colors: colors,
            showSelectorInList: true,
            templates: {
                selector: itemViewsOptionColorTemplate,
                // 'shoppingCartOptionColor'
                selected: itemViewsSelectedOptionColorTemplate
            }
        }]
    };
});