<section id="category-list-header">
    <div id="category-main-image"><img src="{{image}}" /></div>
    <div id="category-main-description">
    	<h1>{{pageTitle}}</h1>
    	<h4>{{description}}</h4>
    </div>
</section>