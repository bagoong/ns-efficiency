define('GoogleMap.Configuration', [
    'underscore',
    'Utils'
], function StoreLocatorConfiguration(
    _
) {
    'use strict';

    var unitsHandlers = {
        miles: {
            precision: 0,
            id: 'miles',
            shortName: 'Mi.',
            name: 'Miles',
            toKilometers: function toKilometers(value) {
                return value * 1.609344;
            }
        },
        kilometers: {
            precision: 0,
            id: 'Kilometers',
            shortName: 'km',
            name: 'Kilometers',
            toMiles: function toMiles(value) {
                return value * 0.621371192;
            }
        }
    };

    return {
        unitsHandlers: unitsHandlers,
        geolocationEnabled: window.navigator && navigator.geolocation,
        SchemaOrg: 'Store',
        mapStyles: [
            {
                featureType: 'poi.business',
                elementType: 'labels',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            }
        ],
        storeMarker: {
            showLabel: true
        }
    };
});