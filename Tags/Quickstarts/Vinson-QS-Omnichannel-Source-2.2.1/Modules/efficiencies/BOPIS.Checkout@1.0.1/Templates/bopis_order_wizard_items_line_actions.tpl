{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
{{#if showBtn}}
<div class="bopis-order-wizard-items-line-actions">
	<a class="bopis-order-wizard-items-line-actions-button-remove" data-action="change-delivery" data-option="{{statusToSet}}">
		{{translate btnText}}
	</a>
</div>
{{/if}}