{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
{{#if hasLinesToShow}}
<div>
    <div>
        <h5 class="order-wizard-msr-package-creation-header-subtitle">
            {{packageSubtitle}}
        </h5>
    </div>
    <div data-type="items-selection-control" class="order-wizard-msr-package-creation-items-remaining-list">
        <div class="order-wizard-bopis-package-row">
            <div data-type="items-remaining-list">
                <table class="order-wizard-bopis-package-products-table md2sm" data-view="Items.Collection"></table>
            </div>
        </div>
    </div>
    <div data-type="module-footer">

    </div>
</div>
{{/if}}