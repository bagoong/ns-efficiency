define('InventoryDisplay.ServiceController', [
    'ServiceController',
    'Application',
    'InventoryDisplay.Model'
],	function InventoryDisplayServiceController(
    ServiceController,
    Application,
    InventoryDisplayModel
) {
    'use strict';

    return ServiceController.extend({
        name:'InventoryDisplay.ServiceController',

        get: function methodGet() {
            var returnContent;
            var	internalid = this.request.getParameter('internalid');
            var	summarize = this.request.getParameter('summarize');
            var locationType = this.request.getParameter('locationType');
            var stores = this.request.getParameter('stores');

            if (summarize && parseInt(locationType, 10) === 2 && internalid) {
                returnContent = InventoryDisplayModel.listSummaryShipping(internalid);
            } else if (!summarize && parseInt(locationType, 10) === 1 && internalid && stores) {
                returnContent = InventoryDisplayModel.listDetailStorePickup(internalid, stores);
            }

            if (returnContent) {
                this.sendContent(
                    returnContent,
                    {'cache': response.CACHE_DURATION_SHORT}
                );
            }
        }
    });
});