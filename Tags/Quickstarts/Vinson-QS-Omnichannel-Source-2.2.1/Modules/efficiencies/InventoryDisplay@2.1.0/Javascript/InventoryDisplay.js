// @module InventoryDisplay
define('InventoryDisplay', [
    'InventoryDisplay.Locations',
    'ItemDetails.View',
    'InventoryDisplay.View',

    'PluginContainer',
    'InventoryDisplay.ItemsKeyMapping'
], function InventoryDisplay(
    Locations,
    ItemDetailsView,
    InventoryDisplayView,

    PluginContainer
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            var SC = window.SC;
            Locations.bootstrap(SC.ENVIRONMENT.published.BOPIS_Locations);

            // append our config to global config
            application.Configuration.InventoryDisplay = SC.ENVIRONMENT.published.InventoryDisplay_config;

            // Initiate plugin to hold div
            ItemDetailsView.prototype.preRenderPlugins =
                ItemDetailsView.prototype.preRenderPlugins || new PluginContainer();

            /* Render child in Item.Stock */
            ItemDetailsView.addExtraChildrenViews({
                'Item.Stock': function wrapperFunction() {
                    return function childViewItemStoreStock() {
                        return new InventoryDisplayView({
                            model: this.model,
                            application: this.application,
                            itemDetailsView: this
                        });
                    };
                }
            });
        }
    };
});
