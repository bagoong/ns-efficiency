/**
 * Created by pzignani on 2/9/16.
 */
define('InventoryDisplay.InventorySummaryHashMap.Model', [
    'Backbone',
    'Backbone.CachedModel',
    'underscore',
    'Utils'
], function InventoryDisplayInventorySummaryHashMapModel(
    Backbone,
    BackboneCachedModel,
    _
) {
    'use strict';

    return BackboneCachedModel.extend({
        urlRoot: _.getAbsoluteUrl('services/InventoryDisplay.Service.ss'),

        getForItem: function getForItem(internalid) {
            return this.get(internalid);
        },

        parse: function parse(response) {
            var hash = {};
            _.each(response, function eachResponseItem(location, itemId) {
                hash[itemId] = new Backbone.Model(location);
            });

            return hash;
        }
    });
});