define('BOPIS.Location.Model', [
    'SC.Model',
    'Models.Init',
    'SuiteletProxy',
    'underscore'
], function BOPISLocationModel(
    SCModel,
    CommerceAPI,
    SuiteletProxy
) {
    'use strict';

    return SCModel.extend({
        name: 'BOPIS.Location',
        record: 'location',
        getWarehousesAndStoresForSite: function getWarehousesAndStoresForSite() {
            var proxyResponse;

            console.time('getWarehousesAndStoresForSite');
            proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_suitelet_locations',
                deployId: 'customdeploy_ef_bopis_suitelet_locations',
                parameters: {
                    website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                    splitByType: true,
                    handler: 'getLocationsByType'
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 2 * 60 * 60
                }
            }).get();

            console.timeEnd('getWarehousesAndStoresForSite');
            console.log('getWarehousesAndStoresForSite-response', proxyResponse);

            return proxyResponse;
        }
    });
});
