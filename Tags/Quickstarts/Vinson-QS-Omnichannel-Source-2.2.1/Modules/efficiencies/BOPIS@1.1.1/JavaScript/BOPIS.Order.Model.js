define('BOPIS.Order.Model', [
    'Order.Model',
    'Store.Model',
    'underscore'
], function BOPISOrderModel(
    OrderModel,
    StoreModel,
    _
) {
    'use strict';
    _.extend(OrderModel.prototype, {
        initialize: _.wrap(OrderModel.prototype.initialize, function wrappedInitialize(fn) {
            fn.apply(this, _.toArray(arguments).slice(1));
            this.on('change:store', function onChangeStore(model, attributes) {
                var store = attributes instanceof StoreModel ? attributes : new StoreModel(attributes, {parse: true});
                model.set('store', store, {silent: true});
            });
        }),
        parse: _.wrap(OrderModel.prototype.parse, function wrappedParse(fn) {
            var originalRet = fn.apply(this, _.toArray(arguments).slice(1));
            originalRet.store = new StoreModel(originalRet.store, {parse: true});
            return originalRet;
        })
    });
});