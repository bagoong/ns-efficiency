/**
 *@NApiVersion 2.x
 */
define([
    '../third_party/underscore-custom',
    '../libs/EFUtil',
    '../BOPIS.Backend.Configuration',
    'N/search',
    'N/runtime',
    'N/file',
    'N/render',
    'N/email',
    'N/record'
], function BopisSalesOrderEmail(
    _,
    EFUtil,
    Configuration,
    searchAPI,
    runtimeAPI,
    fileAPI,
    renderAPI,
    emailAPI,
    recordAPI
) {
    'use strict';

    return {
        EMAIL_TYPE_SO_FULLFILLMENT_REQUEST: 1,
        EMAIL_TYPE_SO_READY_FOR_PICKUP: 2,
        EMAIL_TYPE_SO_PICKED_UP: 3,

        getEmailDetails: function getEmailDetails(emailType) {
            var emailDetails = {};

            emailDetails[this.EMAIL_TYPE_SO_FULLFILLMENT_REQUEST] = {
                'title': 'Order no. $(0) has requested for store pickup fullfillment',
                'emailSentFlagID': 'custbody_ef_is_email_sent_ofr'
            };

            emailDetails[this.EMAIL_TYPE_SO_READY_FOR_PICKUP] = {
                'title': 'Your order no. $(0) is now ready for pickup',
                'emailSentFlagID': 'custbody_ef_is_email_sent_rfp'
            };

            emailDetails[this.EMAIL_TYPE_SO_PICKED_UP] = {
                'title': 'Your order no. $(0) has been picked up',
                'emailSentFlagID': 'custbody_ef_is_email_sent_opu'
            };

            return emailDetails[emailType];
        },

        setEmailFields: function setEmailFields(context) {
            var salesOrderRecord;
            var locations = [];
            var locationInfo = {};
            var lineCount = 0;

            var session = runtimeAPI.getCurrentSession();
            var siteInfoField = session.get({name: 'custbody_ef_website_info'}) || '{}';
            var itemsWebInfoStr = session.get({name: 'custcol_ef_bopis_item_web_info'}) || '{}';
            var itemsWebInfo = {};
            var siteInfo = {};
            var website;

            var line = 0;
            var itemfulfillmentchoice;
            var location;

            if (context.type == context.UserEventType.CREATE) {
                salesOrderRecord = context.newRecord;
                lineCount = salesOrderRecord.getLineCount({
                    sublistId: 'item'
                });

                website = salesOrderRecord.getValue('website');

                try {
                    siteInfo = JSON.parse(siteInfoField);
                    itemsWebInfo = JSON.parse(itemsWebInfoStr);
                }
                catch(e) {
                    log.error('e', e);
                    siteInfo = {};
                    itemsWebInfo = {};
                }

                if ( website && website > 0 ) {
                    for (line = 0; line < lineCount; line++) {
                        itemfulfillmentchoice = salesOrderRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'itemfulfillmentchoice',
                            line: line
                        });

                        location = salesOrderRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'location',
                            line: line
                        });

                        if (itemfulfillmentchoice == 2) {
                            locations.push(location);
                        }else if (itemfulfillmentchoice) {
                            siteInfo.shippingAddress = salesOrderRecord.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'shipaddress',
                                line: line
                            });
                        }

                        if (itemsWebInfo && itemsWebInfo[line]) {
                            salesOrderRecord.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_ef_bopis_item_web_info',
                                line: line,
                                value: JSON.stringify(itemsWebInfo[line])
                            });
                        }
                    }

                    if (locations && locations.length) {
                        var locationsInfo = searchAPI.create({
                            type: 'location',
                            filters: ['internalid', 'anyof', locations],
                            columns: [
                                'name',
                                'custrecord_bopis_location_thumbnail',
                                'custrecord_bopis_location_urlcomponent',
                                'custrecord_bopis_location_short_descript',
                                'address1',
                                'address2',
                                'city',
                                'country',
                                'phone',
                                'state',
                                'zip',
                                'custrecord_bopis_location_opening_hs',
                                'latitude',
                                'longitude',
                                'internalid'
                            ]
                        }).run();

                        locationsInfo.each(function(location) {
                            locationInfo[location.id] = {
                                name: location.getValue('name'),
                                thumbnail: location.getText('custrecord_bopis_location_thumbnail') &&
                                            siteInfo &&
                                            siteInfo.home ?
                                            siteInfo.home + location.getText('custrecord_bopis_location_thumbnail') :
                                            '',
                                urlcomponent: location.getValue('custrecord_bopis_location_urlcomponent'),
                                description: location.getValue('custrecord_bopis_location_short_descript'),
                                address1: location.getValue('address1'),
                                address2: location.getValue('address2'),
                                city: location.getValue('city'),
                                country: location.getValue('country'),
                                state: location.getValue('state'),
                                zip: location.getValue('zip'),
                                phone: location.getValue('phone'),
                                hours: location.getValue('custrecord_bopis_location_opening_hs'),
                                latitude: location.getValue('latitude'),
                                longitude: location.getValue('longitude'),
                                internalid: location.getValue('internalid')
                            };
                            return true;
                        });

                        for (line = 0; line < lineCount; line++) {
                            itemfulfillmentchoice = salesOrderRecord.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'itemfulfillmentchoice',
                                line: line
                            });
                            location = salesOrderRecord.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'location',
                                line: line
                            });

                            if (itemfulfillmentchoice == 2) {
                                salesOrderRecord.setSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_ef_bopis_store_info',
                                    line: line,
                                    value: JSON.stringify(locationInfo[location])
                                });
                            }
                        }
                    }

                    if (siteInfo.shippingAddress) {
                        var shiptoLine = salesOrderRecord.findSublistLineWithValue({
                            sublistId: 'iladdrbook',
                            fieldId: 'iladdrinternalid',
                            value: siteInfo.shippingAddress
                        });

                        if (shiptoLine || shiptoLine === 0) {
                            siteInfo.shippingAddress = salesOrderRecord.getSublistValue({
                                sublistId: 'iladdrbook',
                                fieldId: 'iladdrshipaddr',
                                line: shiptoLine
                            });

                            siteInfo.shippingAddress = siteInfo.shippingAddress.replace(/\n/g, '<br>');
                        }
                    }
                    salesOrderRecord.setValue('custbody_ef_website_info', JSON.stringify(siteInfo));
                }
            }
        },

        getRecipients: function getRecipients(options) {
            var emailRecipients = [];
            var i = 0;
            var count = 0;
            var locationId;
            var itemFulfillmentChoice;

            if ( options.emailType === this.EMAIL_TYPE_SO_FULLFILLMENT_REQUEST ) {
                count = options.salesOrderRecord.getLineCount({sublistId: 'item'});
                for (i = 0; i < count; i++) {
                    itemFulfillmentChoice = parseInt(options.salesOrderRecord.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemfulfillmentchoice',
                        line: i
                    }), 10);

                    if(itemFulfillmentChoice === 2 ){
                        locationId = parseInt(options.salesOrderRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'location',
                            line: i
                        }), 10);
                    }
                }
                searchAPI.create({
                    type: searchAPI.Type.EMPLOYEE,
                    columns: EFUtil.createColumns({
                        name: { name: 'entityid' },
                        email: { name: 'email' }
                    }),
                    filters: EFUtil.createFilters({
                        location: {
                            name: 'location',
                            operator: searchAPI.Operator.IS,
                            values: locationId
                        }
                    })
                }).run().each(function each(result) {
                     emailRecipients.push({
                         name: result.getValue('entityid'),
                         email: result.getValue('email')
                    });
                    return true;
                });
            }else {
                if (! _.isEmpty(options.salesOrderRecord.getValue({fieldId: 'email'})) ) {
                    emailRecipients.push(options.salesOrderRecord.getValue({fieldId: 'email'}));
                }

                if (! _.isEmpty(options.salesOrderRecord.getValue({fieldId: 'custbody_ef_bopis_contact_email'})) ) {
                    emailRecipients.push(options.salesOrderRecord.getValue({fieldId: 'custbody_ef_bopis_contact_email'}));
                }
            }

            return emailRecipients;
        },

        sendEmail: function sendEmail(options) {
            var emailDetail = this.getEmailDetails(options.emailType);
            var salesOrderRecord = recordAPI.load({
                type: recordAPI.Type.SALES_ORDER,
                id: options.salesOrderID,
                isDynamic: true
            });
            var emailRecipients = this.getRecipients({
                salesOrderRecord: salesOrderRecord,
                emailType: options.emailType
            });
            var emailBodyTemplate = fileAPI.load(options.templatePath);
            var templateContents = emailBodyTemplate.getContents();
            var renderer = renderAPI.create();
            var emailContent;
            var title = '';

            renderer.templateContent = templateContents;
            renderer.addRecord('record', salesOrderRecord);
            emailContent = renderer.renderAsString();

            title = emailDetail.title.replace('$(0)', salesOrderRecord.getValue({fieldId: 'memo'}));
            if ( options.emailType === this.EMAIL_TYPE_SO_FULLFILLMENT_REQUEST ) {
                _.each(emailRecipients, function eachEmailRecipient(emailRecipient) {
                    emailContent = emailContent.replace('PLACEHOLDEREMAILRECIPIENTNAME', emailRecipient.name);

                    emailAPI.send({
                        author: Configuration.emailSenderID,
                        recipients: emailRecipient.email,
                        subject: title,
                        body: emailContent
                    });
                });
            }else {
                emailAPI.send({
                    author: Configuration.emailSenderID,
                    recipients: emailRecipients,
                    subject: title,
                    body: emailContent
                });
            }

            salesOrderRecord.setValue({
                fieldId: emailDetail.emailSentFlagID,
                value: true,
                ignoreFieldChange: true
            }).save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
        }
    }
});