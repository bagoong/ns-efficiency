/**
 *@NApiVersion 2.x
 */
define([
    '../third_party/underscore-custom',
    'N/search'
], function BopisLocation(
    _,
    searchAPI
) {
    'use strict';


    return {

        mapResultLine: function mapResultLine(line, columns) {
            var self = this;
            return _.reduce(columns,
                function mapLine(memo, columnObj, columnKey) {
                    memo[columnKey] = self.getCastedColumnValue(line, columnObj);
                    return memo;
                },
                {}
            );
        },

        createColumns: function createColumns(columns) {
            return _.map(columns, function mapColumns(column) {
                return searchAPI.createColumn(column);
            });
        },
        createFilters: function createFilters(filters) {
            return _.map(filters, function mapFilters(filter) {
                return searchAPI.createFilter(filter);
            });
        },
        /**
         * @param {Search} search The search
         * @param {{}} options
         * @param {function} options.callback Transformation to apply to each result
         * @param {[]} [options.mappingColumns] Columns for mapping with map result line
         * @param {Boolean} [options.unique] Expect an unique result
         * @param {Integer} [options.start] Used together with end to search a range
         * @param {Integer} [options.end] Used together with start to search a range
         * @param {Integer} [options.limit] special case of range: start 0, end range
         */
        getResults: function getResults(search, options) {
            var self = this;
            /** @type {ResultSet} */
            var resultSet = search.run();
            var callback = options.callback || (options.mappingColumns && function mappingCallback(line) {
                return self.mapResultLine(line, options.mappingColumns);
            }) || function simpleCallback(line) { return line; };

            /** @type {{}[]} */
            var results = [];

            if (options.limit) {
                results = _.map(resultSet.getRange({start: 0, end: options.limit}), callback);
            } else if (options.start && options.end) {
                results = _.map(resultSet.getRange({start: options.start, end: options.end}), callback);
            } else {
                resultSet.each(function eachResultOfSet(line) {
                    results.push(callback(line));
                    return true;
                });
            }

            if (options.unique) {
                if (results && results[0] && results.length === 1) {
                    return results[0];
                }
                return null;
            }

            return results;
        },

        getCastedColumnValue: function getCastedColumnValue(line, column) {
            var value = line.getValue(column);

            switch (column.type) {
            case 'float':
                if (value === '' && column.hasOwnProperty('default')) {
                    value = column.default;
                } else {
                    value = parseFloat(value);
                }
                break;

            case 'integer':
                if (value === '' && column.hasOwnProperty('default')) {
                    value = column.default;
                } else {
                    value = parseInt(value, 10);
                }

                break;

            case 'object':
                value = {
                    internalid: value,
                    name: line.getText(column)
                };
                break;
            case 'name':
                value = line.getText(column);
                break;
            default:
                break;
            }

            return value;
        }
    };
});