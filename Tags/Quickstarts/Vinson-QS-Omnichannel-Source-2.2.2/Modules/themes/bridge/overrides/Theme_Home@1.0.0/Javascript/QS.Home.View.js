/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Home
define('QS.Home.View', [
    'Home.View',
    'PluginContainer',
    'underscore',
    'SC.Configuration'
],
function QSHomeView(
    HomeView,
    PluginContainer,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            HomeView.prototype.preRenderPlugins =
                HomeView.prototype.preRenderPlugins || new PluginContainer();

            HomeView.prototype.preRenderPlugins.install({
                name: 'themeBridgeHome',
                execute: function execute($el /* , view */) {
                    $el.find('[data-view="FreeText"]')
                        .html(_(Configuration.get('home.freeText', '')).translate());
                }
            });

            // for Carousel
            var carousel = Configuration.get('home.carouselImages', []);
            // for Infoblocks
            var infoblock = Configuration.get('home.infoblock', []);
            var infoblockTile = false;
            if (infoblock.length == '3' || infoblock.length > '5'){
                infoblockTile = true;
            }
            var infoblockFive = false;
            if (infoblock.length == '5'){
                infoblockFive = true;
            }
            // for Free text and images
            var freeTextImages = Configuration.get('home.freeTextImages', []);

            HomeView.prototype.installPlugin('postContext', {
                name: 'themeBridgeContext',
                priority: 10,
                execute: function execute(context, view) {
                    _.extend(context, {
                        // @property {String} url
                        url: _.getAbsoluteUrl(),
                        // @property {Boolean} showCarousel
                        showCarousel: !!carousel.length,
                        // @property {Array<Object>} carousel
                        carousel: carousel,
                        // @property {String} carouselBgrImg
                        carouselBgrImg: _.getAbsoluteUrl(Configuration.get('home.carouselBgrImg')),
                        // @property {Number} infoblockCount
                        infoblockCount: infoblock.length,
                        // @property {Boolean} infoblockTile
                        infoblockTile: infoblockTile,
                        // @property {Boolean} infoblockFive
                        infoblockFive: infoblockFive,
                        // @property {Array<Object>} freeTextImages
                        infoblock: infoblock,
                        // @property {String} freeTextTitle
                        freeTextTitle: _(Configuration.get('home.freeTextTitle')).translate(),
                        // @property {Boolean} showFreeTextImages
                        showFreeTextImages: !!freeTextImages.length,
                        // @property {Array<Object>} freeTextImages - the object contains the properties text:String, href:String
                        freeTextImages: freeTextImages
                    });
                }
            });
        }
    };

});
