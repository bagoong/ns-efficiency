{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="bopis-item-views-summary-package-item-summary">
	{{#unless isDiscountType}}
		<p>{{translate '<span class="bopis-item-views-summary-package-item-summary-label">Quantity</span>: <span class="bopis-item-views-summary-package-item-summary-quantity">$(0)</span>' quantity}}</p>
	{{/unless}}

	{{#if showAmount}}
		<p>
			<span>{{#if showAmountLabel}}{{line.amount_label}}{{else}}{{translate '<span class="bopis-item-views-summary-package-item-summary-label">Amount</span>'}}: {{/if}}</span>
			{{#if hasDiscount}}
				<span class="bopis-item-views-summary-package-item-summary-non-discounted-amount">
					{{#if showAmount}}{{amountFormatted}}{{else}}&nbsp;{{/if}}
				</span>
				<span class="bopis-item-views-summary-package-item-summary-amount">
					{{#if showAmount}}{{totalFormatted}}{{else}}&nbsp;{{/if}}
				</span>
			{{else}}
				<span class="bopis-item-views-summary-package-item-summary-amount">
					{{#if showAmount}}{{amountFormatted}}{{else}}&nbsp;{{/if}}
				</span>
			{{/if}}
		</p>
	{{/if}}
</div>