define('BOPIS.OrderWizard.Module.StockValidator', [
    'Wizard.Module',
    'GlobalViews.Message.View',

    'bopis_order_wizard_stockvalidator.tpl',

    'underscore',
    'jQuery',
    'Utils'
], function BOPISOrderWizardModuleStockValidator(
    WizardModule,
    GlobalViewsMessageView,

    bopisOrderWizardStockValidator,

    _,
    jQuery
) {
    'use strict';
    return WizardModule.extend({
        goToCartLink: '<a href="#" class="order-wizard-cart-summary-edit-cart-link" data-touchpoint="viewcart">' +
                    _('Edit Cart').translate() + '</a>',
        template: bopisOrderWizardStockValidator,
        isValid: function isValid() {
            var errors = [];

            this.wizard.model.get('lines').each(function eachLine(line) {
                var stockInfo = line.get('stockInfo');
                var deliveryStockInfo;
                var item = line.get('item');

                if (stockInfo) {
                    deliveryStockInfo = stockInfo[line.get('deliverymethod')];
                    if (item.get('_isBackorderable') === false) {
                        if (deliveryStockInfo && deliveryStockInfo.quantityAvailable === 0 ||
                            item.get('_isPurchasable') === false) {
                            errors.push({line: line, error: 'NO_STOCK'});
                        }
                    }
                }

                if (_.isUndefined(deliveryStockInfo) && item.get('_isBackorderable') === false) {
                    errors.push({line: line, error: 'NO_STOCK'});
                }
            });

            if (errors.length) {
                this.showError(this.readableErrors(errors));
                return jQuery.Deferred().fail();
            }

            return jQuery.Deferred().resolve();
        },

        readableErrors: function readableErrors(errors) {
            var self = this;
            var errorStrings = [];

            _.each(errors, function readableError(errorLine) {
                var item = errorLine.line.get('item');
                var matrixItemOption = _.pluck(_.filter(item.getPosibleOptions(), {isMatrixDimension: true}), 'cartOptionId');
                var lineItemName = errorLine.line.get('item').get('_name');
                var itemOption = [];

                _.each( item.get('options'), function eachItemOptions(option) {
                    if ( _.contains(matrixItemOption, option.id.toLowerCase()) ) {
                        itemOption.push(option.displayvalue);
                    }
                });

                if ( itemOption.length > 0 ) {
                    lineItemName += ' (';
                    lineItemName += itemOption.join(' / ');
                    lineItemName += ')';
                }

                if (errorLine.line.get('deliverymethod') === 'pickup') {
                    errorStrings.push(_('$(0) is not available for pick up on store $(1) at this time').translate(
                        lineItemName,
                        self.wizard.model.get('store') && self.wizard.model.get('store').get('name')
                    ));
                } else {
                    errorStrings.push(_('$(0) is not available for shipping at this time') .translate(
                        lineItemName)
                    );
                }
            });
            if (errorStrings.length) {
                errorStrings.push(this.goToCartLink);
            }
            return { errorCode: 'ERR_CHK_STOCK', errorMessage: errorStrings.join('<br/>') };
        },

        showError: function showError(readableErrors) {
            var placeholder = jQuery('body [data-type="alert-placeholder-step"]');
            var globalViewMessage = new GlobalViewsMessageView({
                message: _.translate(readableErrors.errorMessage),
                type: 'error',
                closable: true
            });

            placeholder.html(globalViewMessage.render().$el.html());

            if ( placeholder && placeholder.length > 0) {
                jQuery('body').animate({
                    scrollTop: placeholder.offset().top
                }, 600);
            }

            jQuery('[data-action="submit-step"]').prop('disabled', false);
        }
    });
});