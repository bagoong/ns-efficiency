<section id="storelocator" class="storelocator-container">
    <div class="storelocator-header">
        {{#if hideTitle}}
        <h3 class="storelocator-header-title">{{translate 'Store Locator'}}</h3>
        {{/if}}
        <div class="storelocator-form-container" data-view="StoreLocator.Form"></div>
    </div>
    <div class="storelocator-content row">
        <div class="storelocator-list-stores" data-view="StoreLocator.List"></div>
        <div class="storelocator-map-view" data-view="StoreLocator.Map"></div>
    </div>
</section>