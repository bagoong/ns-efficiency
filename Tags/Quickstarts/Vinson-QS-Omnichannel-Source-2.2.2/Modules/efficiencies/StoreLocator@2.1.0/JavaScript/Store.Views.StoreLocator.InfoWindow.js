define('Store.Views.StoreLocator.InfoWindow', [
    'Backbone',
    'SC.Configuration',
    'storelocator_infowindow.tpl',
    'Utilities.ResizeImage'
], function StoreViewsStoreLocatorInfoWindow(
    Backbone,
    Configuration,
    storelocatorInfowindowTpl,
    resizeImage
) {
    'use strict';
    return Backbone.View.extend({
        template: storelocatorInfowindowTpl,
        initialize: function initialize(options) {
            this.model = options.model;
            this.origin = options.origin;
            this.isInStoreStock = options.isInStoreStock;
        },
        getModuleConfig: function moduleConfig() {
            return Configuration.get('storeLocator');
        },
        getContext: function getContext() {
            var oAddress = this.origin && this.origin.lat ?
                ('saddr=' + this.origin.lat() + ',' + this.origin.lng() + '&') : '';

            var gMapsLink = 'http://maps.google.com/maps?' + oAddress +
                'daddr=' + this.model.get('lat') + ',' + this.model.get('lon');

            return {
                hideButtons: this.options.hideButtons,

                model: this.model,

                showStoreLink: (this.getModuleConfig() && this.getModuleConfig().enableStoreDetailPage)
                                && this.model.get('urlcomponent'),

                hasThumbnail: this.model.get('thumbnail') && this.model.get('thumbnail').name,

                storeImageThumbnail:
                    resizeImage(this.model.get('thumbnail') && this.model.get('thumbnail').name, 'thumbnail'),

                baseUrl: window.location.origin,

                gMapsLink: gMapsLink,

                isInStoreStock: this.isInStoreStock
            };
        }
    });
});