define('BOPIS', [
    'BOPIS.Router',
    'InventoryDisplay',
    'BOPIS.LiveOrder.Model',
    'BOPIS.Order.Model',
    'BOPIS.OrderLine.Model',
    'Profile.MyStore'
], function BOPIS(
    Router
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            return new Router(application);
        }
    };
});