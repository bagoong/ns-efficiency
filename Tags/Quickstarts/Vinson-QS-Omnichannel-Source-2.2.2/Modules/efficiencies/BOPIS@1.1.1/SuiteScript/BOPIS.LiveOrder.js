define('BOPIS.LiveOrder', [
    'CustomFieldsParser',
    'LiveOrder.Model',
    'Models.Init',
    'Profile.MyStore.Model',
    'BOPIS.Configuration',
    'Address.Model',
    'Store.Model',
    'underscore',
    'Utils',
    'extendWithEvents',
    'safeParseJSON',
    'SuiteletProxy'
], function LiveOrderInStorePickup(
    customFieldsParser,
    LiveOrder,
    CommerceAPI,
    ProfileMyStoreModel,
    Configuration,
    AddressModel,
    StoreModel,
    _,
    Utils,
    extendWithEvents,
    safeParsingJSON,
    SuiteletProxy
) {
    'use strict';

    var STORE_BODY_FIELD = 'custbody_ef_bopis_store';
    var ALL_BODY_FIELDS = [STORE_BODY_FIELD];
    var SHIPS_FROM_ITEM_FIELD = 'custitem_bopis_ships_from';
    var PICKS_FROM_ITEM_FIELD = 'custitem_bopis_pick_from';

    extendWithEvents(LiveOrder, {
        /**
         * Modifies the cart response to hide away implementation details of BOPIS
         * @param cart
         */
        wrapLiveOrderBOPIS: function wrapLiveOrderBOPIS(cart) {
            if (this.isSecure && Configuration.enabled === true && CommerceAPI.session.isLoggedIn2()) {
                console.log('enhanceOrderBOPIS');
                this.enhanceOrderBOPIS(cart);
                console.log('removeBOPISFromShipMethodList');
                this.removeBOPISFromShipMethodList(cart);
                console.log('enhanceWithStock');
                this.enhanceWithStock(cart);
            }
        },

        /**
         * Write on the response if the order lines indicate shipping, pickup or partial
         * ITEM IS FOR BOPIS IF EITHER:
         *
         * line shipmethod is bopis
         * overall shipmethod is bopis
         * the item is not shippable at all
         * AND it's pickable in at least one place
         *
         * Remove shipping address and method on BOPIS
         * Puts the MST address that is not the store as simple shipping address
         * Puts the MST method that is not BOPIS as simple shipping method
         * @param {{}} cart
         * @param {[]} cart.lines
         * @param {String} cart.shipmethod
         * @param {String} cart.deliverymethod
         */
        enhanceOrderBOPIS: function enhanceOrderBOPIS(cart) {
            var deliveryMethodsSelected;
            var deliveryMethodSelected = 'shipping'; // Default value
            var bopisShippingMethod = this.getBOPISShippingMethod();
            var isMultiShipTo = this.isBackendMST();
            var bopisStoreAddress;
            var shipMethod = null;
            var shipMethods = [];
            var shipAddresses = [];
            var shipAddress = null;
            var dataForShipAddress;
            var fulfillableLines = _.filter(cart.lines, function filterfullfillablelines(l) {
                return l.item.isfulfillable !== false;
            });


            // STEP 0: add cart to response, even if not bopis it's good to have it because of changes of delivery methods
            cart.store = this.getStore();
            bopisStoreAddress = AddressModel.getBOPISStoreAddress();

            // STEP 1: Adjust lines to either shipping or pickup
            _.each(cart.lines, function eachLine(line) {
                var itemShips = line && line.item && safeParsingJSON(line.item[SHIPS_FROM_ITEM_FIELD], []);
                var itemPicks = line && line.item && safeParsingJSON(line.item[PICKS_FROM_ITEM_FIELD], []);

                console.log('enhanceOrderBOPIS-line', JSON.stringify({
                    lineid: line.internalid,
                    itemShips: itemShips,
                    itemPicks: itemPicks,
                    isMST: isMultiShipTo,
                    lineShipMethod: line.shipmethod,
                    cartShipMethod: cart.shipmethod,
                    bopisMethodId: bopisShippingMethod
                }));

                if (
                    (   isMultiShipTo && line.shipmethod === bopisShippingMethod ||
                        !isMultiShipTo && cart.shipmethod === bopisShippingMethod ||
                        itemShips.length === 0
                    ) && itemPicks.length > 0
                ) {
                    line.deliverymethod = 'pickup';
                } else {
                    line.deliverymethod = 'shipping';
                }
            });

            // STEP 2: based on lines, adjust global summary
            deliveryMethodsSelected = _.unique(_.pluck(fulfillableLines, 'deliverymethod'));

            if ( deliveryMethodsSelected && deliveryMethodsSelected.length === 1 ) {
                deliveryMethodSelected = deliveryMethodsSelected[0];
            } else if (deliveryMethodsSelected.length > 1) {
                // The difference between partial and empty is that EMPTY means we still haven't activated MST
                // Behind the scenes
                deliveryMethodSelected = isMultiShipTo ? 'partial' : null;
            }

            cart.deliverymethod = deliveryMethodSelected;

            // finally, adjust shipping method and shipping address
            switch (cart.deliverymethod) {
            case 'pickup':
                /* we have defaulted as pickup because of the items on the cart
                However BOPIS was never set, or shipping address was never set
                But if we don't do this, price won't change until we actually submit this to server for first time
                Which could be even review step
                 */
                if (
                    (!cart.shipmethod || cart.shipmethod !== bopisShippingMethod) ||
                    (!cart.shipaddress && cart.store)
                ) {
                    if (!cart.shipmethod || cart.shipmethod !== bopisShippingMethod) {
                        console.time('enhanceOrderBOPIS-setshippingmethodforbopis');
                        this.setShippingMethod({shipmethod: bopisShippingMethod}, {shipmethod: cart.shipmethod });
                        console.timeEnd('enhanceOrderBOPIS-setshippingmethodforbopis');
                    }
                    if (!cart.shipaddress && cart.store) {
                        console.time('enhanceOrderBOPIS-setshippingaddressforbopis');
                        dataForShipAddress = {store: cart.store};
                        this.setStoreAddress(dataForShipAddress);
                        this.setShippingAddress(dataForShipAddress, { shipaddress: null});
                        console.timeEnd('enhanceOrderBOPIS-setshippingaddressforbopis');
                    }

                    console.timeEnd('enhanceOrderBOPIS-recalculatingcart-bopis');
                    _.extend(cart, this.get());
                    console.timeEnd('enhanceOrderBOPIS-recalculatingcart-bopis');
                    return;
                }

                shipMethod = null;
                shipAddress = null;
                break;
            case 'shipping':
                shipMethod = cart.shipmethod;
                shipAddress = cart.shipaddress;

                if (cart.shipmethod === bopisShippingMethod ||
                    cart.shipaddress === bopisStoreAddress
                ) {
                    if (cart.shipmethod === bopisShippingMethod) {
                        CommerceAPI.order.removeShippingMethod();
                    }
                    if (cart.shipaddress === bopisStoreAddress) {
                        CommerceAPI.order.removeShippingAddress();
                    }

                    console.timeEnd('enhanceOrderBOPIS-recalculatingcart-shipping');
                    _.extend(cart, this.get());
                    console.timeEnd('enhanceOrderBOPIS-recalculatingcart-shipping');
                    return;
                }
                break;
            default:

                /* Find out the ship address that is not BOPIS */
                shipMethods = _.without(
                    _.unique(_.pluck(fulfillableLines, 'shipmethod')),
                    bopisShippingMethod
                );

                /* Find the method that is not BOPIS */
                shipAddresses = _.without(
                    _.unique(_.pluck(fulfillableLines, 'shipaddress')),
                    AddressModel.getBOPISStoreAddress()
                );

                if (shipMethods && shipMethods[0]) {
                    shipMethod = shipMethods[0];
                }

                if (shipAddresses && shipAddresses[0]) {
                    shipAddress = shipAddresses[0];
                }
                break;
            }

            /* delete MST data from output of lines */
            _.each(cart.lines, function deleteShipMethodResponse(line) {
                delete line.shipmethod;
                delete line.shipaddress;
            });


            cart.shipmethod = shipMethod;
            cart.shipaddress = shipAddress;
            cart.sameAs = cart.shipaddress === cart.billaddress;
        },

        /**
         * Adds stock data for stock of the item both for shipping and pickup
         * @param cart
         */
        enhanceWithStock: function enhanceWithStock(cart) {
            var lines = cart.lines;
            var requestLines = [];
            var store = this.getStore();
            var storeId = store && store.internalid;
            var stockResponse;

            _.each(cart.lines, function eachLine(line) {
                if (_.contains(Configuration.ITEM_TYPES_WITH_INVENTORY, line.item.itemtype)) {
                    line.stockInfo = {
                        shipping: line && line.item && safeParsingJSON(line.item[SHIPS_FROM_ITEM_FIELD], []),
                        pickup: line && line.item && safeParsingJSON(line.item[PICKS_FROM_ITEM_FIELD], [])
                    };

                    if (line.stockInfo.shipping) {
                        requestLines.push({
                            internalid: line.item.internalid,
                            locationType: 2
                        });
                    }

                    if (line.stockInfo.pickup && storeId) {
                        requestLines.push({
                            internalid: line.item.internalid,
                            locationType: 1,
                            location: storeId
                        });
                    }
                }
            });

            if (requestLines.length > 0) {
                console.time('enhanceWithStock-suiteletCall');
                stockResponse = new SuiteletProxy({
                    scriptId: 'customscript_ef_bopis_item_inventory',
                    deployId: 'customdeploy_ef_bopis_item_inventory',
                    parameters: {
                        website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                        // This should probably be 'defaultSub' if going in sc env.
                        // But again, then subsidiary should be param in url
                        subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                        handler: 'getCartStockByDeliveryType'
                    },
                    body: JSON.stringify(requestLines),
                    requestType: 'POST',
                    isAvailableWithoutLogin: true,
                    cache: {
                        enabled: true,
                        ttl: 5 * 60
                    }
                }).get();

                console.time('enhanceWithStock-suiteletCall');

                _.each(lines, function eachLineAppendResponse(line) {
                    if (_.contains(Configuration.ITEM_TYPES_WITH_INVENTORY, line.item.itemtype)) {
                        _.extend(line.stockInfo, {
                            shipping: line.stockInfo.shipping &&
                            stockResponse[line.item.internalid + '_' + '2'],
                            pickup: line.stockInfo.pickup &&
                            stockResponse[line.item.internalid + '_' + '1' + '_' + storeId]
                        });
                    }
                });
            }
        },

        removeBOPISFromShipMethodList: function removeBOPISFromShipMethodList(responseData) {
            var i;
            var bopisMethod = this.getBOPISShippingMethod();
            var list = responseData.shipmethods;
            if (!list || list.length === 0) {
                return;
            }

            i = list.length - 1;
            while (i >= 0) {
                if ( list[i] && (
                        list[i].internalid.toString() === bopisMethod.toString()
                    )
                ) {
                    list.splice( i, 1 );
                }
                i--;
            }
        },

        /**
         * Modifies cart request to hide away implementation details of BOPIS
         * @param requestData
         * @param currentOrder
         * @param caller
         */
        unwrapLiveOrderBOPIS: function unwrapLiveOrderBOPIS(requestData, currentOrder, caller) {
            if (this.isSecure && Configuration.enabled === true && CommerceAPI.session.isLoggedIn2())  {
                if (caller === 'setShippingAddressAndMethod') {
                    this.changeDeliveryMethodAndStorePartial(requestData, currentOrder);
                }
                if (caller === 'setShippingAddress') {
                    this.changeDeliveryMethodTotal(requestData, currentOrder);
                }
            }
        },

        /**
         * Gets the store for pickup. Depending on where, if it'll be from the customer or the cart.
         * @return {*} Store
         */
        getStore: function getStore() {
            var storeId;
            var profileStore;
            console.time('BOPISLiveOrder-getStore-customField');
            storeId = customFieldsParser(CommerceAPI.order.getCustomFieldValues()).custbody_ef_bopis_store;
            console.timeEnd('BOPISLiveOrder-getStore-customField');
            if (!storeId) {
                profileStore = ProfileMyStoreModel.get();
                if (profileStore && profileStore.internalid) {
                    AddressModel.setBOPISStoreAsShipAddress(profileStore);
                    this.setStoreAddress({store: profileStore}, {});
                    return profileStore;
                }
                return {};
            }

            return StoreModel.get(storeId);
        },

        /**
         * Method to find out which Shipping Method represents Pick Up In store
         * Matches it based on a configuration parameter
         * It matches by name because ids change between environments
         * @returns {String} shipmethodid
         */
        getBOPISShippingMethod: function getBOPISShippingMethod() {
            var shippingMethods;
            var bopisMethod;

            console.time('getBOPISShippingMethods-find');
            shippingMethods = CommerceAPI.order.getAvailableShippingMethods(['shipmethod', 'name']);
            bopisMethod = _.findWhere(shippingMethods, { name: Configuration.bopisShippingMethodName });
            console.timeEnd('getBOPISShippingMethods-find');

            if (!bopisMethod && shippingMethods !== null) {
                throw _.extend({}, {
                    status: 400,
                    code: 'ERR_BOPIS_SHIP_METHOD',
                    message: 'Buy Online Pick Up In Store is not set up properly. Please contract your administrator'
                });
            }
            console.log('getBOPISShippingMethods-found', bopisMethod);

            return bopisMethod && bopisMethod.shipmethod.toString();
        },

        setStoreAddress: function setStoreAddress(data, currentOrder) {
            var store;
            var newStoreId = data && data.store && data.store.internalid;
            var oldStoreId = currentOrder && currentOrder.store && currentOrder.store.internalid;
            var changeFlag = false;

            console.log('setStoreAddress-settingstore-compare', 'new: ' + newStoreId + '-old:' + oldStoreId);
            if (newStoreId !== oldStoreId) {
                if (newStoreId) {
                    console.time('setStoreAddress-settingAddress');
                    store = StoreModel.get(newStoreId);
                    AddressModel.setBOPISStoreAsShipAddress(store);
                    changeFlag = true;
                    console.timeEnd('setStoreAddress-settingAddress');
                }

                console.time('setStoreAddress-customfield');
                CommerceAPI.order.setCustomFieldValues({custbody_ef_bopis_store: newStoreId || ''});
                console.timeEnd('setStoreAddress-customfield');
            }

            if (data.deliverymethod === 'pickup') {
                data.shipaddress = AddressModel.getBOPISStoreAddress();
                // Updating the address does not trigger a recalc as you would expect (not Always!)
                // So better to force the set again
                if (changeFlag) {
                    currentOrder.shipaddress = null;
                }
            }
        },


        /**
         * Avoids setting custom body fields that are BOPIS related from livorder.options object
         * @param data
         */
        protectCustomBodyFieldsInput: function protectCustomBodyFieldsInput(data) {
            _.each(ALL_BODY_FIELDS, function protectCustomBodyFieldInput(bodyField) {
                if (data.options) {
                    delete data.options[bodyField];
                }
            });
        },

        isBackendMST: function isBackendMST() {
            return CommerceAPI.order.getFieldValues([{ismultishipto: null}], false).ismultishipto.toString() === 'T';
        },

        /**
         *
         * @param data
         * @returns {boolean}
         */
        isBOPISMultiShipTo: function isBOPISMultiShipTo(data) {
            return data.deliverymethod === 'partial';
        },


        /**
         * Change delivery method when it's not a partial ship/pick
         * This means all items are either ship or pick
         * No multi shipto is needed in this case. We just set the BOPIS ship method
         * @param cart is the new data
         * @param currentOrder is the result of this.get() before this request
         */
        changeDeliveryMethodTotal: function changeDeliveryMethodTotal(cart, currentOrder) {
            var orderFields;
            var bopisShipMethod = this.getBOPISShippingMethod();

            // get real values from the order, as currentOrder is 'polluted' with us mingling it
            console.time('changeDeliveryMethodTotal-getshipmethodandaddress');
            orderFields = CommerceAPI.order.getFieldValues({shipmethod: ['shipmethod'], shipaddress: ['internalid']});
            console.timeEnd('changeDeliveryMethodTotal-getshipmethodandaddress');
            // Put back real currentOrder fields for a while.
            // We need native functions check against real values, not our wrapped order.
            currentOrder.shipmethod = orderFields.shipmethod ? orderFields.shipmethod.shipmethod : null;
            currentOrder.shipaddress = orderFields.shipaddress ? orderFields.shipaddress.internalid : null;

            // set store address as address
            this.setStoreAddress(cart, currentOrder);

            /** Set BOPIS ship method or remove bopis ship method */
            if (cart.deliverymethod === 'shipping') {
                cart.shipmethod = cart.shipmethod === bopisShipMethod ? null : cart.shipmethod;
                cart.shipaddress = cart.shipaddress === AddressModel.getBOPISStoreAddress() ? null : cart.shipaddress;
            }
            if (cart.deliverymethod === 'pickup') {
                cart.shipmethod = bopisShipMethod;
                cart.shipaddress = AddressModel.getBOPISStoreAddress();
            }
        },

        /**
         * Manage delivery method update when it's a sales order with both pick up and shipping
         * We internally use MST to achive that
         * @param cart
         * @param currentOrder
         */
        changeDeliveryMethodAndStorePartial: function changeDeliveryMethodAndStorePartial(cart, currentOrder) {
            var storeAddressId;
            var shipAddressId;
            var shipMethodId;
            var bopisShippingMethod;
            var shipAddress;
            var itemids;
            // var oldShipAddress;

            storeAddressId = AddressModel.getBOPISStoreAddress();
            bopisShippingMethod = this.getBOPISShippingMethod();

            shipAddressId = (cart.shipaddress && cart.shipaddress !== storeAddressId) ? cart.shipaddress : null;
            shipMethodId = (cart.shipmethod && cart.shipmethod !== bopisShippingMethod) ? cart.shipmethod : null;
            // oldShipAddress = currentOrder && currentOrder.addresses && currentOrder.addresses[currentOrder.shipaddress];
            shipAddress = shipAddressId && shipAddressId.indexOf('null') === -1 && AddressModel.get(shipAddressId);

            /* Sadly, we cannot allow ship & pick to different countries. (nexuses...)
            * So if we detect a new shipping that is not allowed we throw exception.
            * The order is such because pickup items go first in our wizard
            * */

            if (cart.store && cart.store.country &&
                (currentOrder.store && currentOrder.store.country === cart.store.country) &&
                shipAddress && shipAddress.country !== cart.store.country) {
                console.warn('changeDeliveryMethodAndStorePartial', 'User tried to set different nexus');
                throw _.extend({}, {
                    status: 400,
                    code: 'ERR_BOPIS_SHIP_METHOD',
                    message: 'Pick up and shipping to different countries is not allowed this time.'
                });
            }


            /*
             The trickiest part:
             After reverse-engeneering the SO form, we noticed that you have to first do a no MST with the country
             that you are gonna then do a MST to. The 'reset nexus' thing.
             So if it is a 'new country value' somehow, we have to reset MST to non-MST back and forth
             Just in case, we remove billing address too
             */
            if (cart.store) {
                if ((shipAddress && shipAddress.country !== cart.store.country) || !shipAddress) {
                    shipAddressId = null;
                    shipMethodId = null;
                }

                if (
                    cart.store.country !== (currentOrder.store && currentOrder.store.country)
                ) {
                    console.log('Starting Change of Nexus');
                    shipAddressId = null;
                    shipMethodId = null;
                    cart.billaddress = null;

                    itemids = _.pluck(currentOrder.lines, 'internalid');

                    console.time('changeDeliveryMethodAndStorePartial-');
                    CommerceAPI.order.removeShippingAddress();

                    console.time('changeDeliveryMethodAndStorePartial-clearshippingaddress');
                    CommerceAPI.order.setItemShippingAddress(itemids, null);
                    console.timeEnd('changeDeliveryMethodAndStorePartial-clearshippingaddress');

                    console.time('changeDeliveryMethodAndStorePartial-disablingitemlineshipping');
                    CommerceAPI.order.setEnableItemLineShipping(false);
                    console.timeEnd('changeDeliveryMethodAndStorePartial-disablingitemlineshipping');

                    console.time('changeDeliveryMethodAndStorePartial-settingstoreaddress');
                    this.setStoreAddress(cart, currentOrder);
                    console.timeEnd('changeDeliveryMethodAndStorePartial-settingstoreaddress');

                    console.time('changeDeliveryMethodAndStorePartial-gettingstoreaddress');
                    storeAddressId = AddressModel.getBOPISStoreAddress();
                    console.timeEnd('changeDeliveryMethodAndStorePartial-gettingstoreaddress');

                    console.time('changeDeliveryMethodAndStorePartial-settingshippingaddress');
                    CommerceAPI.order.setShippingAddress(storeAddressId);
                    console.timeEnd('changeDeliveryMethodAndStorePartial-settingshippingaddress');

                    console.time('changeDeliveryMethodAndStorePartial-reenablingMST');
                    CommerceAPI.order.setEnableItemLineShipping(true);
                    console.timeEnd('changeDeliveryMethodAndStorePartial-reenablingMST');

                    console.log('Ending Change of Nexus');
                } else {
                    this.setStoreAddress(cart, currentOrder);
                    storeAddressId = AddressModel.getBOPISStoreAddress();
                }
            } else {
                this.setStoreAddress(cart, currentOrder);
                storeAddressId = AddressModel.getBOPISStoreAddress();
            }

            /* As we use MST, sameAs will cause issues, and actually delete the billing address
            as for reference code order.shipaddress === null when MST is on
            */
            if (cart.sameAs) {
                cart.billaddress = shipAddressId;
                cart.sameAs = false;
            }

            /* SYNC up lines with their ship methods and addresses */
            _.each(cart.lines, function eachCartLine(line) {
                if (line.deliverymethod === 'pickup') {
                    line.shipaddress = storeAddressId;
                    line.shipmethod = bopisShippingMethod;
                } else {
                    line.shipaddress = shipAddressId;
                    line.shipmethod = shipMethodId;
                }
            });

            /* it's MST, no shipaddress or shipmethod */
            cart.shipaddress = null;
            cart.shipmethod = null;
        },
        beforeSubmitBOPIS: function beforeSubmitBOPIS() {
            var bopisShipMethod = this.getBOPISShippingMethod();
            CommerceAPI.context.setSessionObject('BOPISShipMethodID', bopisShipMethod);
        },
        getMultiShipMethodsBOPIS: function getMultiShipMethodsBOPIS(lines) {
            // Get multi ship methods
            var multishipmethods = {};
            var bopisAddress = AddressModel.getBOPISStoreAddress();
            var bopisMethod = this.getBOPISShippingMethod();

            _.each(lines, function eachLine(line) {
                if (line.shipaddress && line.shipaddress !== bopisAddress) {
                    multishipmethods[line.shipaddress] = multishipmethods[line.shipaddress] || [];
                    multishipmethods[line.shipaddress].push(line.internalid);
                }
            });

            if (multishipmethods && _.keys(multishipmethods).length === 1) {
                _.each(_.keys(multishipmethods), function eachKey(address) {
                    var methods = CommerceAPI.order.getAvailableShippingMethods(multishipmethods[address], address);

                    _.each(methods, function eachMethod(method) {
                        method.internalid = method.shipmethod.toString();
                        method.rate_formatted = Utils.formatCurrency(method.rate);
                        delete method.shipmethod;
                    });
                    methods = _.reject(methods, function reject(method) { return method.internalid === bopisMethod; });

                    multishipmethods[address] = methods;
                });

                return multishipmethods[_.keys(multishipmethods)[0]];
            }
            return null;
        },

        /** START: OVERRIDED REFERENCE METHODS */

        /**
         * Needed to override the update method from reference.
         * We don't want to remove promocodes
         * and we want to hack the isMultiShippingEnabled
         * So we use multishipto reference apis, without enabling it in the frontend.
         * @param data
         */

        get: function get() {
            var orderFields = this.getFieldValues();
            var result = {};

            // @class LiveOrder.Model.Data object containing high level shopping order object information.
            // Serializeble to JSON and this is the object that the .ss service will serve
            // and so it will populate front end Model objects
            try {
                // @property {Array<LiveOrder.Model.Line>} lines
                result.lines = this.getLines(orderFields);
            } catch (e) {
                if (e.code === 'ERR_CHK_ITEM_NOT_FOUND') {
                    return this.get();
                }
                throw e;
            }

            orderFields = this.hidePaymentPageWhenNoBalance(orderFields);

            // @property {Array<String>} lines_sort sorted lines ids
            result.lines_sort = this.getLinesSort();

            // @property {String} latest_addition
            result.latest_addition = CommerceAPI.context.getSessionObject('latest_addition');

            // @property {LiveOrder.Model.PromoCode} promocode
            result.promocodes = this.getPromoCodes(orderFields);

            // @property {Boolean} ismultishipto
            result.ismultishipto = this.getIsMultiShipTo(orderFields);

            // Ship Methods
            if (CommerceAPI.order.getFieldValues([{ismultishipto: null}], false).ismultishipto.toString() === 'T') {
                // @property {Array<OrderShipMethod>} multishipmethods
                result.shipmethods = this.getMultiShipMethodsBOPIS(result.lines);

                // These are set so it is compatible with non multiple shipping.
                // result.shipmethods = [];
                // result.shipmethod = null;


                // Correct promocodes
                /*
                if (result.promocode && result.promocode.code) {
                    order.removePromotionCode(result.promocode.code);
                    return this.get(); //Recursive, as it might impact the summary information
                }
                */
            } else {
                // @property {Array<OrderShipMethod>} shipmethods
                result.shipmethods = this.getShipMethods(orderFields);
                // @property {OrderShipMethod} shipmethod
                result.shipmethod = orderFields.shipmethod ? orderFields.shipmethod.shipmethod : null;
            }

            // Addresses
            result.addresses = this.getAddresses(orderFields);
            result.billaddress = orderFields.billaddress ? orderFields.billaddress.internalid : null;
            result.shipaddress = !result.ismultishipto ? orderFields.shipaddress.internalid : null;

            // @property {Array<ShoppingSession.PaymentMethod>} paymentmethods Payments
            result.paymentmethods = this.getPaymentMethods(orderFields);

            // @property {Boolean} isPaypalComplete Paypal complete
            result.isPaypalComplete = CommerceAPI.context.getSessionObject('paypal_complete') === 'T';

            // @property {Array<String>} touchpoints Some actions in
            // the live order may change the URL of the checkout so to be sure we re send all the touchpoints
            result.touchpoints = CommerceAPI.session.getSiteSettings(['touchpoints']).touchpoints;

            // @property {Boolean} agreetermcondition Terms And Conditions
            result.agreetermcondition = orderFields.agreetermcondition === 'T';

            // @property {OrderSummary} Summary
            result.summary = orderFields.summary;

            // @property {Object} options Transaction Body Field
            result.options = this.getTransactionBodyField();

            // @class LiveOrder.Model
            return result;
        },

        update: function update(data) {
            var currentOrder = this.get();
            if (!data.internalid) {
                this.setEmailFields(currentOrder);
            }


            this.isMultiShippingEnabled = true; // temporary enabled
            if (this.isSecure && CommerceAPI.session.isLoggedIn2()) {
                CommerceAPI.order.setEnableItemLineShipping(this.isBOPISMultiShipTo(data));
            }

            // Do the following only if multishipto is active
            // (if the data received determine that MST is enabled and pass the MST Validation)
            if (this.isBOPISMultiShipTo(data)) {
                CommerceAPI.order.removeShippingAddress();
                CommerceAPI.order.removeShippingMethod();
                // this.removePromoCode(currentOrder);
                // this.splitLines(data,currentOrder);
                /* Commented out removePromoCode and split line as it's not needed here */
                this.setShippingAddressAndMethod(data, currentOrder);
            }


            if (!this.isBOPISMultiShipTo(data)) {
                this.setShippingAddress(data, currentOrder);
                this.setShippingMethod(data, currentOrder);
            }
            // moved promo code out from the MST exclusion
            this.setPromoCodes(data, currentOrder);
            this.setBillingAddress(data, currentOrder);
            this.setPaymentMethods(data);
            this.setTermsAndConditions(data);
            this.setTransactionBodyField(data);
        },

        /**
         * We override setShippingMethod just to read from all methods instead of currentOrder.shipmethods
         * because currentOrder.shipmethods won't contain bopis
         * @param data
         * @param currentOrder
         */
        setShippingMethod: function setShippingMethod(data, currentOrder) {
            var shipmethod;
            var shipmethods;

            if ((!this.isMultiShippingEnabled || !data.ismultishipto) &&
                this.isSecure &&
                data.shipmethod !== currentOrder.shipmethod
            ) {
                console.time('setShippingMethod-rereadingmethods');
                shipmethods =  _.map(
                    CommerceAPI.order.getAvailableShippingMethods(['shipmethod', 'name', 'shipcarrier']),
                    function map(method) {
                        return {
                            internalid: method.shipmethod,
                            name: method.name,
                            shipcarrier: method.shipcarrier
                        };
                    }
                );
                console.timeEnd('setShippingMethod-rereadingmethods');

                shipmethod = _.findWhere(shipmethods, {internalid: data.shipmethod});

                if (shipmethod) {
                    CommerceAPI.order.setShippingMethod({
                        shipmethod: shipmethod.internalid,
                        shipcarrier: shipmethod.shipcarrier
                    });
                } else {
                    CommerceAPI.order.removeShippingMethod();
                }
            }
        },
        /**
         * For more flexibility, setShippingAddress and method was reformated to form packages per method-address
         * instead of just address. This is to allow setting first the method and then the address if needed
         * @param cart
         * @param currentOrder
         */

        setShippingAddressAndMethod: function setShippingAddressAndMethod(cart, currentOrder) {
            var packages = {};
            var itemIdsToClean = [];
            var originalLine;

            // Event hookup does not work with wrappings */
            // this.changeDeliveryMethodAndStorePartial(cart, currentOrder);

            _.each(cart.lines, function eachCartLine(line) {
                var key = (line.shipaddress || '') + '-' + (line.shipmethod || '');
                originalLine = _.find(currentOrder.lines, function findOriginalLine(orderLine) {
                    return orderLine.internalid === line.internalid;
                });


                /*  condition removed && originalLine.item.isfulfillable !== false */
                if (originalLine && originalLine.item) {
                    if (line.shipaddress) {
                        packages[key] = packages[key] || {
                            shipMethodId: null,
                            shipAddressId: null,
                            itemIds: []
                        };

                        packages[key].itemIds.push(line.internalid);
                        packages[key].shipMethodId = line.shipmethod || null;
                        packages[key].shipAddressId = line.shipaddress || null;
                    } else {
                        itemIdsToClean.push(line.internalid);
                    }
                } else {
                    console.log('SOMETHING ODD. hay original line?', !!originalLine);
                }
            });

            // CLEAR Shipping address and shipping methods
            if (itemIdsToClean.length) {
                CommerceAPI.order.setItemShippingAddress(itemIdsToClean, null);
                CommerceAPI.order.setItemShippingMethod(itemIdsToClean, null);
            }

            // SET Shipping address and shipping methods
            _.each(packages, function eachPackage(currentPackage) {
                CommerceAPI.order.setItemShippingAddress(currentPackage.itemIds, parseInt(currentPackage.shipAddressId, 10) || null);

                if (!!parseInt(currentPackage.shipAddressId, 10)) {
                    CommerceAPI.order.setItemShippingMethod(currentPackage.itemIds, parseInt(currentPackage.shipMethodId, 10) || null);
                } else {
                    CommerceAPI.order.setItemShippingMethod(currentPackage.itemIds, null);
                }
            });
        },
        /**
         * It
         * @param orderFields
         * @returns {boolean}
         */
        getIsMultiShipTo: function getIsMultiShipTo(/* orderFields*/) {
            return false;
        },
        /** END: OVERRIDED REFERENCE METHODS */

        /** HELPER FUNCTIONS FOR EMAILS */
        setEmailFields: function setEmailFields(params) {
            var touchpoints = CommerceAPI.session.getSiteSettings(['touchpoints']).touchpoints;
            var homeUrl = touchpoints.home;
            var homeParts = homeUrl.split('?');
            var homeDomain = homeParts && homeParts.length ? homeParts[0] : homeUrl;
            var storeInfo = {home: homeDomain, customercenter: touchpoints.customercenter};
            var self = this;
            var itemInfo = {};
            var itemImage;

            CommerceAPI.context.setSessionObject('custbody_ef_website_info', JSON.stringify(storeInfo));
            _.each(params.lines, function eachLine(line, lineIndex) {
                if (line.item) {
                    itemImage = self.changeImageDomain(homeDomain, self.getItemImage(line.item));

                    itemInfo[lineIndex] = {
                        imageUrl: itemImage
                    };
                }
            });

            CommerceAPI.context.setSessionObject('custcol_ef_bopis_item_web_info', JSON.stringify(itemInfo));
        },

        getItemImage: function getItemImage(item) {
            var images = item.itemimages_detail;
            var keys = _.keys(images);
            var firstImage;

            if (images && !_.isEmpty(images)) {
                if (images.thumbnail) {
                    return images.thumbnail.url && images.thumbnail.url.length ?
                                images.thumbnail.url[0].url :
                                images.thumbnail.url;
                } else if (images.main) {
                    return images.main.urls && images.main.urls.length ? images.main.urls[0].url : images.main.url;
                } else if (images.urls && images.urls[0] && images.urls[0].url) {
                    return images.urls[0].url;
                } else if (keys && keys.length) {
                    firstImage = images[_.keys(images)[0]];
                    return firstImage && firstImage.urls ? firstImage.urls[0].url : firstImage.url;
                }
            } else if (item.matrix_parent) {
                return this.getItemImage(item.matrix_parent);
            }

            return '';
        },

        changeImageDomain: function changeImageDomain(homeDomain, imageUrl) {
            var domainIndex = imageUrl.indexOf('/assets');
            var imagePart = imageUrl.substring(domainIndex);
            return homeDomain + imagePart;
        }
    });
});