/**
 *@NApiVersion 2.x
 */
define([
    'N/error',
    'N/runtime'
], function BopisSalesOrder(
    errorAPI,
    runtimeAPI
) {
    'use strict';

    return {
        /**
         * Set pick up in store parameters
         * @param {Record} salesOrderRecord
         */
        transformCustomToNative: function(salesOrderRecord) {
            var pickUpInStoreLocation = salesOrderRecord.getValue('custbody_ef_bopis_store');
            var lineCount = salesOrderRecord.getLineCount({sublistId: 'item'});
            var i;
            var isStorePickupLine;
            var isMultiShipTo = salesOrderRecord.getValue('ismultishipto');
            var session = runtimeAPI.getCurrentSession();
            var bopisShipMethodId = session.get({name: 'BOPISShipMethodID'}) || '';
            var shipMethod = salesOrderRecord.getValue('shipmethod');
            var isFullPickUpInStore = !isMultiShipTo && shipMethod.toString() === bopisShipMethodId;


            log.debug('IS MST', JSON.stringify(isMultiShipTo));
            /* CLEAR SHIPPING TEMPORARY FIELDS */
            if (!isMultiShipTo) {
                if (isFullPickUpInStore) {
                    log.debug('IS FULL STORE PICKUP');
                    salesOrderRecord.setValue('shipmethod', null);
                    salesOrderRecord.setValue('shipcarrier', null);
                    // salesOrderRecord.setValue('shipaddress', null);

                    salesOrderRecord.setValue('location', pickUpInStoreLocation);
                    salesOrderRecord.setValue('fulfillmentchoice', '2');
                } else {
                    log.debug('IS FULL SHIPPING');
                    salesOrderRecord.setValue('fulfillmentchoice', '1');
                    salesOrderRecord.setValue('custbody_ef_bopis_store', '');
                }
            }

            for (i = 0; i < lineCount; i++) {
                isStorePickupLine = isFullPickUpInStore || ((salesOrderRecord.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'shipmethod',
                    line: i
                }) || '').toString() ===  bopisShipMethodId);

                log.debug('IS STORE PICKUP LINE', JSON.stringify(isStorePickupLine));

                if (isStorePickupLine && !pickUpInStoreLocation) {
                    throw errorAPI.create({
                        name: 'ERR_BOPIS_LOCATION',
                        message: 'Orders with items set for Pick up in store require a store'
                    });
                }

                if (isStorePickupLine === true) {
                    if (isMultiShipTo) {
                        /*
                        salesOrderRecord.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'shipaddress',
                            line: i,
                            value: ''
                        });*/

                        salesOrderRecord.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'shipmethod',
                            line: i,
                            value: ''
                        });
                        salesOrderRecord.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'shipcarrier',
                            line: i,
                            value: ''
                        });
                    }

                    salesOrderRecord.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemfulfillmentchoice',
                        line: i,
                        value: '2'
                    });
                    log.debug('ITEM SET VAL', salesOrderRecord.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'item',
                        line: i
                    }));
                    salesOrderRecord.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'location',
                        line: i,
                        value: pickUpInStoreLocation
                    });
                } else {
                    salesOrderRecord.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemfulfillmentchoice',
                        line: i,
                        value: '1'
                    });
                }
            }
        }
    };
});