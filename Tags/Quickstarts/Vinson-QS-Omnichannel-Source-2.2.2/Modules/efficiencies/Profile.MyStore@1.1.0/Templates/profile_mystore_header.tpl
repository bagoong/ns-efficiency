{{#if hasStore}}
<a href="#" class="profile-mystore-header-link" data-toggle="dropdown" title="{{translate 'My Store'}}">
    <i class="profile-mystore-header-icon"></i>
    <span class="profile-mystore-header-text" >{{ model.name }}</span>
    <i class="profile-mystore-header-carret-icon"></i>
</a>
<div class="profile-mystore-header-dropdown" data-action="dropdown-click">
    <div id="store-address">
        <h5 class="profile-mystore-header-storename">{{translate 'My Store:'}} {{ model.name }}</h5>
        <address>
            {{#if model.address1 }} {{ model.address1}} <br> {{/if}}
            {{#if model.address2 }} {{ model.address2 }}<br> {{/if}}

            {{ model.city }}
            {{#if model.state}} , {{/if}}
            {{ model.state }}
            {{#if model.zipcode}} , {{ model.zipcode }} <br> {{/if}}

            {{#if model.country}} {{ model.country }}<br> {{/if}}
        </address>
    </div>
    <div>
        <a name="mystore" data-hashtag="#mystore"
            data-touchpoint="customercenter" href="#mystore" tabindex="-1" class="profile-mystore-button-view-detail">
            {{translate 'See Directions'}}
        </a>
    </div>

    <div class="profile-mystore-button-container">
        <span data-view="Profile.MyStore.StoreLocator"></span>
    </div>
</div>
{{/if}}