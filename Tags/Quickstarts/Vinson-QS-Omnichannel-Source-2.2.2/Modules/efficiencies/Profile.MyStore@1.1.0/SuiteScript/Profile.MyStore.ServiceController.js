define('Profile.MyStore.ServiceController', [
    'ServiceController',
    'Application',
    'Profile.MyStore.Model'
], function ProfileServiceController(
    ServiceController,
    Application,
    ProfileMyStore
) {
    'use strict';

    return ServiceController.extend({
        name:'Profile.MyStore.ServiceController',

        get: function getMethod() {
            this.sendContent(ProfileMyStore.get());
        },
        post: function postMethod() {
            if ( this.data ) {
                this.sendContent(ProfileMyStore.update(this.data));
            }
        },
        put: function putMethod() {
            if ( this.data ) {
                this.sendContent(ProfileMyStore.update(this.data));
            }
        }
    });
});