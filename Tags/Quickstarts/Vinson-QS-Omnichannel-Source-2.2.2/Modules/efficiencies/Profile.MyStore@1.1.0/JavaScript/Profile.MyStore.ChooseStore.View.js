// @module Profile.MyStore
define('Profile.MyStore.ChooseStore.View', [
    'Backbone',
    'Store.Model',
    'Profile.MyStore.Model',
    'profile_mystore_choose_store.tpl',
    'jQuery'
], function ProfileMyStoreChooseStoreView(
    Backbone,
    Store,
    MyStoreModel,
    chooseStoreTpl,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: chooseStoreTpl,

        events: {
            'click [data-action="save-store"]': 'savePreferredStore'
        },

        savePreferredStore: function savePreferredStore() {
            MyStoreModel.getInstance().set(this.options.store).save();

            if ( this.options.isOpenInModal && this.options.itemDetailsView ) {
                this.options.itemDetailsView.hasRenderedInModal = false;
                this.options.itemDetailsView.showInModal();
            } else {
                if ( this.options.application.getLayout().$containerModal ) {
                    this.options.application.getLayout().$containerModal.modal('hide');
                }

                jQuery('.inventory-display-pushpane-map *[data-action="sc-pusher-dismiss"]').trigger('click');
            }
        }
    });
});