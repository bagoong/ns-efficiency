/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       16 Oct 2015     cparayno
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var Handler = (function(){
    function main(request,response){

        var feedId  = request.getParameter('feed');
        var i;
        var test;
        var fileText;
        var recordFeed;
        var feedCount;
        var feedFileIds = [];
        var fileID;

        if( feedId ){
        	recordFeed = nlapiLoadRecord('customrecord_ef_gfg_feed_instance', feedId);
        	feedCount = recordFeed.getLineItemCount('recmachcustrecord_ef_gfg_fif_feed_instance')+1;

        	for(i=1; i<feedCount; i++ ) {
        		fileID = recordFeed.getLineItemValue('recmachcustrecord_ef_gfg_fif_feed_instance', 'custrecord_ef_gfg_fif_file', i);
        		if(fileID){
        			feedFileIds.push( fileID );
        		}
            }

        	for(i=0; i<feedCount; i++ ) {
        		if(feedFileIds[i]){
	        		test = nlapiLoadFile(feedFileIds[i]);
	        		fileText = test.getValue();

	        		if( i != 0){
	        			var firstLineIndex = fileText.indexOf("\n");
	            		fileText = fileText.substring(firstLineIndex,fileText.length);
	        		}
	                response.write(fileText);
        		}
        	}

//        	response.setContentType('PLAINTEXT', 'GoogleFeed.txt', 'inline');

        	//        	response.setContentType('CSV', 'GoogleFeed.csv');

            response.setContentType('PLAINTEXT', 'GoogleFeed.txt');
        }
    }

    return {
        main: main
    };

}());
