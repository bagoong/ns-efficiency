{{!
	� 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showOption}}
    <div class="item-views-selected-option" name="{{label}}">
        <span class="item-views-selected-option-label">{{label}}: </span>
        <span class="item-views-selected-option-value">{{value}}</span>
    </div>
    {{#if giftWrapLine.isValid}}
    <div class="item-views-cell-actionable-selected-options-cell">
        <div class="item-views-selected-option" name="{{label}} Amount">
            <span class="item-views-selected-option-label">{{translate 'Gift Wrap Amount:'}} </span>
            <span class="item-views-selected-option-value">{{giftWrapLine.total_formatted}}</span>
        </div>
    </div>
    {{/if}}
{{/if}}