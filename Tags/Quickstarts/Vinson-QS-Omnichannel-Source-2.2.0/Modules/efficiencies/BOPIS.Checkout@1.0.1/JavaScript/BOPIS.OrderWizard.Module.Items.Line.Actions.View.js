define('BOPIS.OrderWizard.Module.Items.Line.Actions.View', [
    'bopis_order_wizard_items_line_actions.tpl',
    'jQuery',
    'Backbone',
    'Store.Collection',
    'Store.Views.StoreLocator',
    'Profile.MyStore.ChooseStore.View',
    'underscore'
], function BOPISOrderWizardModuleBOPISItemsLineActionsView(
    requestquoteWizardModuleItemsLineActionsTpl,
    jQuery,
    Backbone,
    Stores,
    StoreLocatorView,
    ProfileMyStoreButtonView,
    _
) {
    'use strict';

    return Backbone.View.extend({
        keyMappingCondition: null,
        template: requestquoteWizardModuleItemsLineActionsTpl,

        events: {
            'click [data-action="change-delivery"]': 'changeDelivery'
        },

        changeDelivery: function changePackage(e) {
            var $target = jQuery(e.target);
            var store = (this.options.wizardModel.get('store') &&
                        this.options.wizardModel.get('store').get('internalid'));

            if ($target.data('option') === 'pickup' && _.isUndefined(store)) {
                this._showChooseStore();
            }

            this.options.wizardModel.changeDeliveryForLine(this.model, $target.data('option'));
        },

        getContext: function getContext() {
            return {
                showBtn: this.model.get('item').get(this.keyMappingCondition),
                btnText: this.btnText,
                currentStatus: this.model.get('delivery'),
                statusToSet: this.statusToSet
            };
        },

        _showChooseStore: function _showChooseStore() {
            var chooseStoreView;
            var storeLocatorView = new StoreLocatorView({
                application: this.options.application,
                collection: new Stores(null),
                isInMyStore: true,
                storeSearchText: ''
            });

            storeLocatorView.title = _('Choose Store For Pick Up').translate();
            storeLocatorView.modalClass = 'profile-mystore-modal-storelocator';

            this.listenTo(storeLocatorView.eventBus, 'infoWindowRendered',
                _.bind( function appendChooseStoreButton(store) {
                    chooseStoreView = new ProfileMyStoreButtonView({
                        store: store,
                        application: this.options.application
                    });

                    chooseStoreView.setElement('#main #bot-cont').render();
                }, this));

            this.options.application.getLayout().showInModal(storeLocatorView);
        }
    });
});