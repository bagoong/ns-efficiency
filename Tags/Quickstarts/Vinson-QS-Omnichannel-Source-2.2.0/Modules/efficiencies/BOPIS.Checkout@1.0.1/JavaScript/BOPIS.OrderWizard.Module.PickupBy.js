define('BOPIS.OrderWizard.Module.PickupBy', [
    'Wizard.Module',
    'bopis_order_wizard_pickupby.tpl',
    'underscore',
    'jQuery'
], function BOPISOrderWizardModulePickupBy(
    WizardModule,
    Template,
    _,
    jQuery
) {
    'use strict';

    var PICKUPBY_OPTION_ME = 'me';
    var PICKUPBY_OPTION_OTHERS = 'others';

    return WizardModule.extend({
        template: Template,
        pickupByOption: PICKUPBY_OPTION_ME,
        events: {
            'change [data-action="change-pickupby-options"]': 'changePickupByOptions',
            'blur [data-action="set-pickupby"]': 'setPickupby'
        },
        setPickupby: function savePickup() {
            var pickupByName = this.$el.find('[name="pickupByName"]').val();
            var pickupByEmail = this.$el.find('[name="pickupByEmail"]').val();
            var options = this.wizard.model.get('options') || {};

            if (!this.options.readOnly) {
                if( this.pickupByOption === PICKUPBY_OPTION_OTHERS ) {
                    options.custbody_ef_bopis_contact = pickupByName;
                    options.custbody_ef_bopis_contact_email = pickupByEmail;

                }else {
                    options.custbody_ef_bopis_contact = '';
                    options.custbody_ef_bopis_contact_email = '';
                }

                this.wizard.model.set('options', options, {silent: true});
            }
        },
        initialize: function initialize() {
            var pickupByName;
            var pickupByEmail;
            WizardModule.prototype.initialize.apply(this, arguments);

            pickupByName = this.wizard.model.get('options')
                            && this.wizard.model.get('options').custbody_ef_bopis_contact;
            pickupByEmail = this.wizard.model.get('options')
                            && this.wizard.model.get('options').custbody_ef_bopis_contact_email;


            if (! (_.isEmpty(pickupByName) && _.isEmpty(pickupByEmail))) {
                this.pickupByOption = PICKUPBY_OPTION_OTHERS;
            }

            this.listenTo(this.wizard.model, 'change:deliverymethod', jQuery.proxy(this.render, this));
            this.listenTo(this.wizard.model, 'change:options', jQuery.proxy(this.render, this));
        },

        changePickupByOptions: function changePickupByOptions(e) {
            var $target = jQuery(e.target);
            this.pickupByOption = $target.val();
            this.render();
        },
        present: function present() {
            this.render();
        },
        isActive: function isActive() {
            return this.wizard.shouldOfferBOPIS();
        },
        getContext: function getContext() {
            var pickupByName = this.wizard.model.get('options')
                                && this.wizard.model.get('options').custbody_ef_bopis_contact;
            var pickupByEmail = this.wizard.model.get('options')
                                && this.wizard.model.get('options').custbody_ef_bopis_contact_email;

            return {
                isNotReadOnly: !this.options.readOnly,
                isActive: this.model.getLinesForPicking().length > 0,
                pickupByName: pickupByName,
                pickupByEmail: pickupByEmail,
                isOthersSelected: this.pickupByOption === PICKUPBY_OPTION_OTHERS,
                isMeSelected: this.pickupByOption === PICKUPBY_OPTION_ME,
                isMeSelectedReadonly: _.isEmpty(pickupByName) && _.isEmpty(pickupByEmail)
            };
        },
        isValid: function isValid() {
            var promise = jQuery.Deferred();
            var pickupByName = this.wizard.model.get('options')
                && this.wizard.model.get('options').custbody_ef_bopis_contact;
            var pickupByEmail = this.wizard.model.get('options')
                && this.wizard.model.get('options').custbody_ef_bopis_contact_email;


            if ((!this.options.readOnly) && this.pickupByOption === PICKUPBY_OPTION_OTHERS) {
                if ( _.isEmpty(pickupByName) && _.isEmpty(pickupByEmail) ) {
                    return promise.reject(
                        _('You have selected someone else to pick up your orders. Please provide the name.').translate()
                    );
                }
            }
            return promise.resolve();
        },
        submit: function submit() {
            this.setPickupby();
            return this.isValid();
        }
    });
});