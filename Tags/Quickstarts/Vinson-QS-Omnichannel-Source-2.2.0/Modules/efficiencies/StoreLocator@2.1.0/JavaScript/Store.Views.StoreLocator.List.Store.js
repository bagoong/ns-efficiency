define('Store.Views.StoreLocator.List.Store', [
    'Backbone',
    'SC.Configuration',
    'storelocator_list_store.tpl',
    'Utils'
], function StoreViewsStoreLocatorListStore(
    Backbone,
    Configuration,
    Template
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(data) {
            this.model  = data.model;
            this.application  = data.application;
            this.isInStoreStock = data.isInStoreStock;
            this.isInMyStore = data.isInMyStore;
        },
        getModuleConfig: function getModuleConfig() {
            return Configuration.get('storeLocator');
        },

        getContext: function getContext() {
            var showStoreLink = (this.isInStoreStock || this.isInMyStore) ?
                                    false :
                                    (this.getModuleConfig() && this.getModuleConfig().enableStoreDetailPage) &&
                                    this.model.get('urlcomponent');
            return {
                model: this.model,
                isInStoreStock: this.isInStoreStock,
                showStoreLink: showStoreLink,
                storeMarker: this.getModuleConfig() && this.getModuleConfig().storeMarker,
                configuration: this.getModuleConfig(),
                geolocation: this.getModuleConfig() && this.getModuleConfig().storeMarker.icon ||
                            'http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless.png'
            };
        }
    });
});