define('Store.View.StoreList.store', [
    'Backbone',
    'SC.Configuration',
    'storelist_store.tpl',
    'Utils'
], function storeListStore(
    Backbone,
    Configuration,
    Template
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(data) {
            this.model  = data.model;
        },

        getModuleConfig: function moduleConfig() {
            return Configuration.get('storeLocator');
        },

        getContext: function getContext() {
            return {
                model: this.model,
                showStoreLink: (this.getModuleConfig() && this.getModuleConfig().enableStoreDetailPage)
                                && this.model.get('urlcomponent'),
                configuration: this.getModuleConfig()
            };
        }
    });
});