define('Store.ServiceController', [
    'ServiceController',
    'Application',
    'Store.Model'
], function StoreServiceController(
    ServiceController,
    Application,
    StoreModel
) {

    'use strict';

    return ServiceController.extend({
        name:'Store.ServiceController',

        get: function() {
            var storeContent;
            var urlcomponent = this.request.getParameter('urlcomponent');
            var isForPurchases = this.request.getParameter('for_purchases');
            var lat = this.request.getParameter('lat');
            var lon = this.request.getParameter('lng');
            var limit = this.request.getParameter('limit');
            var radius = this.request.getParameter('radius');

            if (urlcomponent) {
                storeContent = StoreModel.getByUrlcomponent(urlcomponent, isForPurchases);
            } else if (lat && lon && limit && radius) {
                storeContent = StoreModel.nearestStore(lat, lon, limit, isForPurchases, radius);
            } else {
                storeContent = StoreModel.list(isForPurchases);
            }

            this.sendContent(storeContent, {
                'cache': response.CACHE_DURATION_MEDIUM
            });
        }
    });
});