define('Profile.MyStore.Model', [
    'Store.Model',
    'Singleton',
    'underscore',
    'Utils'
], function ProfileMyStoreModel(
    Store,
    Singleton,
    _
) {
    'use strict';

    return Store.extend({
        urlRoot: _.getAbsoluteUrl('services/Profile.MyStore.Service.ss'),
        toJSON: function toJSON() {
            return _.omit(this.attributes, ['marker']);
        }
    }, Singleton);
});

