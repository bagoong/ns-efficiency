{{!
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
}}

<div class="order-history-shipping-group-shipping-information" data-id="{{model.shipaddress.internalid}}">
    <h3 class="order-history-shipping-group-pickup-title"><i class="order-history-pickup-icon"></i> {{translate 'Pickup In Store'}}</h3>
    <div class="order-history-shipping-group-shipping-accordion-divider">
        <div class="order-history-shipping-group-accordion-head">

            <a class="order-history-shipping-group-accordion-head-toggle-secondary collapsed" data-toggle="collapse" data-target="#{{targetAddressId}}" aria-expanded="true" aria-controls="unfulfilled-items">
                    <span class="order-history-shipping-group-shipto-name">
                        {{translate 'Store: $(0)' model.location.name }}
                    </span >
                <i class="order-history-shipping-group-accordion-toggle-icon-secondary"></i>
            </a>
        </div>
        <div class="order-history-shipping-group-accordion-body collapse" id="{{targetAddressId}}" role="tabpanel" data-target="#{{targetAddressId}}">
            <div class="order-history-shipping-group-accordion-container">
                <div><b>{{translate 'Pick up by: '}}</b> {{model.pickup_by}} ({{model.pickup_by_email}})</div>
                <div data-view="StoreDetail"></div>
            </div>
        </div>
    </div>

    {{#if showFulfillment}}
        <div class="order-history-shipping-group-acordion-divider">

            {{#if showFulfillmentAcordion}}
                <div class="order-history-shipping-group-accordion-head">
                    <a class="order-history-shipping-group-accordion-head-toggle {{initiallyCollapsedArrow}}" data-toggle="collapse" data-target="#{{targetId}}" aria-expanded="true" aria-controls="unfulfilled-items">
                        {{translate 'Products pending pickup'}}
                        <i class="order-history-shipping-group-accordion-toggle-icon"></i>
                    </a>
                </div>
                <div class="order-history-shipping-group-accordion-body collapse {{initiallyCollapsed}}" id="{{targetId}}" role="tabpanel" data-target="#{{targetId}}">
                    <div class="order-history-shipping-group-accordion-container" data-content="order-items-body">
                        <div data-view="Fullfillments.Collection"></div>
                    </div>
                </div>
            {{else}}
                <div class="order-history-shipping-group-body-no-header collapse in" id="{{targetId}}" role="tabpanel" data-target="#{{targetId}}">
                    <div class="order-history-shipping-group-accordion-container" data-content="order-items-body">
                        <div data-view="Fullfillments.Collection"></div>
                    </div>
                </div>
            {{/if}}
        </div>
    {{/if}}
</div>
