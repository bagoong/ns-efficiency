define('BOPIS.Store.Model', [
    'Store.Model',
    'Models.Init',
    'SuiteletProxy',
    'underscore'
], function BOPISStoreModel(
    StoreModel,
    CommerceAPI,
    SuiteletProxy,
    _
) {
    'use strict';

    _.extend(StoreModel, {
        /**
         * Returns a store given a urlcomponent
         * @param {String} urlcomponent: urlcomponent for the store
         * @returns {Object}
         */
        getByUrlcomponent: function getByUrlcomponent(urlcomponent) {
            var proxyResponse;

            console.time('Store-getByUrlcomponent');
            proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_suitelet_locations',
                deployId: 'customdeploy_ef_bopis_suitelet_locations',
                parameters: {
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                    website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                    handler: 'getStores',
                    urlcomponent: urlcomponent
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 5 * 60
                }
            }).get();
            console.timeEnd('Store-getByUrlcomponent');
            return proxyResponse;
        },
        /**
         * Given an store internalid returns a Store
         * @param {String} id The id of the store
         * @returns {Object} store
         */
        get: function get(id) {
            var proxyResponse;

            console.time('Store-get');
            proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_suitelet_locations',
                deployId: 'customdeploy_ef_bopis_suitelet_locations',
                parameters: {
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                    website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                    handler: 'getStores',
                    internalid: id
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 1 * 60 * 60
                }
            }).get();
            console.timeEnd('Store-get');
            return proxyResponse;
        },
        /**
         * Returns locations near the latitude and longitude provided
         * @param lat
         * @param lon
         * @param limit
         * @param isForPurchases
         * @param radius
         * @returns {[{}]} Array of Stores
         */
        nearestStore: function list(lat, lon, limit, isForPurchases, radius) {
            var proxyResponse;

            console.time('Store-nearestStore');
            console.log('Store-nearestStore-params', {
                lat: lat,
                lon: lon,
                limit: limit,
                isForPurchases: isForPurchases,
                radius: radius
            });

            proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_suitelet_locations',
                deployId: 'customdeploy_ef_bopis_suitelet_locations',
                parameters: {
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                    website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                    latitude: lat,
                    longitude: lon,
                    limit: limit || 10,
                    searchRadius: radius || 50,
                    handler: 'getNearestStores'
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 5 * 60
                }
            }).get();
            console.time('Store-nearestStore');
            console.log('Store-nearestStore-length', proxyResponse && proxyResponse.length);
            return proxyResponse;
        },

        list: function list() {
            var proxyResponse;
            console.time('Stores-list');
            proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_suitelet_locations',
                deployId: 'customdeploy_ef_bopis_suitelet_locations',
                parameters: {
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: CommerceAPI.session.getShopperSubsidiary(),
                    website: CommerceAPI.session.getSiteSettings(['siteid']).siteid,
                    handler: 'getStores'
                },
                handler: 'getStores',
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 5 * 60
                }
            }).get();
            console.time('Stores-list');
            return proxyResponse;
        }
    });

    return StoreModel;
});
