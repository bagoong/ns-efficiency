define('BOPIS.EntryPoint', [
    'BOPIS.Configuration',
    'BOPIS.Hooks',
    'BOPIS.Location.Model',
    'BOPIS.OrderHistory.Model',
    'BOPIS.Store.Model',

    'BOPIS.LiveOrder.Hooks',
    'BOPIS.Address.Hooks',
    'BOPIS.OrderHistory.Hooks',

    'BOPIS.FAQs.Model',

    'OrderStatusSummary.Model'
], function BOPISEntryPoint() {
    'use strict';
    return {};
});