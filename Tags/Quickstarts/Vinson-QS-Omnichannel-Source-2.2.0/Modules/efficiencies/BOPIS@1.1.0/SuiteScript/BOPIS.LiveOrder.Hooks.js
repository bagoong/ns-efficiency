define('BOPIS.LiveOrder.Hooks', [
    'BOPIS.Configuration',
    'BOPIS.LiveOrder',
    'Application'
], function InStorePickupHooks(
    Configuration,
    LiveOrderInStorePickup,
    Application
) {
    'use strict';
    
    Application.on('after:LiveOrder.get', function bopisAfterLiveOrderGet(Model, cart) {
        Model.wrapLiveOrderBOPIS(cart);
    });

    /** HEADS UP: It should be before: LiveOrder.update
     * However, in a non-multishipto execution path, setShippingAddress is the first method
     * and it receives the currentOrder, that we need for our operations
     * and on a mst environment, setShippingAddressAndMethod is the first function
     */
    Application.on(
        'before:LiveOrder.setShippingAddress',
        function bopisBeforeLiveOrderUpdate(Model, requestData, currentOrder) {
            Model.unwrapLiveOrderBOPIS(requestData, currentOrder, 'setShippingAddress');
        }
    );
    Application.on(
        'before:LiveOrder.setShippingAddressAndMethod',
        function bopisBeforeLiveOrderUpdate(Model, requestData, currentOrder) {
            Model.unwrapLiveOrderBOPIS(requestData, currentOrder, 'setShippingAddressAndMethod');
        }
    );

    Application.on(
        'before:LiveOrder.setTransactionBodyField',
        function beforeLiveOrderSetTransactionBodyField(Model, requestData) {
            Model.protectCustomBodyFieldsInput(requestData);
        }
    );


    Application.on(
        'before:LiveOrder.submit',
        function bopisBeforeLiveOrderSubmit(Model, requestData) {
            Model.beforeSubmitBOPIS(Model, requestData);
        }
    );
});