{{#if shouldRender}}
    {{#if hasStore}}
        {{ translate 'Pick up in store: $(0)' storeName}}
    {{else}}
        {{ translate 'Choose store TODO'}}
    {{/if}}

{{/if}}