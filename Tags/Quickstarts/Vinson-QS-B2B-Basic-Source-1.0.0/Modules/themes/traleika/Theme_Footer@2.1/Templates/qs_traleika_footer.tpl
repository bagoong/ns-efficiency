{{!
	Â© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
<div class="footer-content">
    <div class="footer-newsletter">
        <p>{{translate '<span>:: Subscribe :: </span>Want to be the first to know about new products and special discounts?'}}</p>
        <div data-view="FooterContent"></div>
    </div>
    <div class="footer-content-nav">
        <ul class="footer-content-nav-list about-us">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'Customer Service'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'contact@customername.com'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">(305) 428-2818</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'M-F 9AM-6PM PST'}}</a>
            </li>
        </ul>
        <ul class="footer-content-nav-list my-account">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".my-account .footer-collapsed-list" data-type="collapse">{{translate 'My Account'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Order History'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Returns'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'My Profile'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'My Wishlist'}}</a>
            </li>
        </ul>
        <ul class="footer-content-nav-list customer-service">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".customer-service .footer-collapsed-list" data-type="collapse">{{translate 'Customer Service'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Shipping Info'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Size Guide'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Gift Cards'}}</a>
            </li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Returns'}}</a>
            </li>
        </ul>
        <ul class="footer-content-nav-list company-name">
            <li class="first" data-toggle="collapse" data-target=".company-name .footer-collapsed-list" data-type="collapse">{{translate 'Company Name'}}</li>
            <li class="footer-collapsed-list">1-888-123-4567</li>
            <li class="footer-collapsed-list">1-555-789-3456</li>
            <li class="footer-collapsed-list">{{translate 'Email us'}}</li>
            <li class="footer-collapsed-list">{{translate 'FAQ'}}</li>
        </ul>
    </div>
    <div class="footer-content-bottom">
        <ul class="footer-social">
            <li class="footer-social-item">
                <a class="" href="https://www.twitter.com/" target="_blank">
                    <i class="footer-social-twitter-icon"></i>
                </a>
            </li>
            <li class="footer-social-item">
                <a href="">
                    <i class="footer-social-google-icon"></i>
                </a>
            </li>
            <li class="footer-social-item">
                <a href="">
                    <i class="footer-social-facebook-icon"></i>
                </a>
            </li>
            <li class="footer-social-item">
                <a href="">
                    <i class="footer-social-pinterest-icon"></i>
                </a>
            </li>
        </ul>
        <ul class="footer-copyright">
            <li>
                <a href="">
                    {{#with copyright}}
                        {{#unless hide}}
                            {{#if showRange}}
                                {{translate '&copy; $(0)-$(1) $(2)' initialYear currentYear companyName}}
                            {{else}}
                                {{translate '&copy; $(0) $(1)' currentYear companyName}}
                            {{/if}}
                        {{/unless}}
                    {{/with}}
                </a>
            </li>
            <li><a href="">{{translate 'Privacy Policy'}}</a></li>
            <li><a href="">{{translate 'Terms & Conditions'}}</a></li>
        </ul>
    </div>
</div>
