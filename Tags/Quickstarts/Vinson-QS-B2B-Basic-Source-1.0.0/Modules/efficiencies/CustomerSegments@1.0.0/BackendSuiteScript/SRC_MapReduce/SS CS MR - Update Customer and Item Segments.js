/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */
define([
    'N/search',
    'N/runtime',
    'N/record'
], function CustomerSegmentMapReduceScript(
        search,
        runtime,
        record
) {
    'use strict';

    var audienceControlMapReduce = {
        getInputData: function getInputData() {
            var delimiter = /\u0005/;
            var mySaveSearchResult = [];
            var selectedRecord = runtime.getCurrentScript().getParameter('custscript_ss_cs_mr_selected_rec');
            var saveSearchFieldId = runtime.getCurrentScript().getParameter('custscript_ss_cs_mr_savesearchfieldid');
            var customerSelectedSegments = runtime.getCurrentScript().getParameter('custscript_ss_cs_mr_customersegments');
            if (selectedRecord === 'customer') {
                search.load({
                    id: saveSearchFieldId,
                    columns: [
                        'id',
                        'recordType',
                        'custentity_customer_customersegments',
                        'altname'
                    ]
                }).run().each(function eachSaveSearchResult(result) {
                    mySaveSearchResult.push({
                        id: result.id,
                        name: result.getValue('altname'),
                        recordType: result.recordType,
                        customerSegmentsValue: result.getValue('custentity_customer_customersegments'),
                        customerSegmentsText: result.getText('custentity_customer_customersegments'),
                        customerSelectedSegments: customerSelectedSegments.split(delimiter),
                        selectedRecord: selectedRecord
                    });
                    return true;
                });
            } else {
                search.load({
                    id: saveSearchFieldId,
                    columns: ['id', 'custitem_item_customersegments']
                }).run().each(function eachResult(result) {
                    mySaveSearchResult.push({
                        id: result.id,
                        customerSegmentsValue: result.getValue('custitem_item_customersegments'),
                        customerSegmentsText: result.getText('custitem_item_customersegments'),
                        recordType: result.recordType,
                        customerSelectedSegments: customerSelectedSegments.split(delimiter),
                        selectedRecord: selectedRecord
                    });
                    return true;
                });
            }
            return mySaveSearchResult;
        },
        map: function map(context) {
            var searchResult = JSON.parse(context.value);
            var customer;
            var item;
            if (searchResult.selectedRecord === 'customer') {
                customer = record.load({
                    type: record.Type.CUSTOMER,
                    id: searchResult.id
                });
                customer.setValue('custentity_customer_customersegments', searchResult.customerSelectedSegments);
                customer.save();
            } else {
                item = record.load({
                    type: searchResult.recordType,
                    id: searchResult.id
                });
                item.setValue('custitem_item_customersegments', searchResult.customerSelectedSegments);
                item.save();
            }
            return true;
        }
    };
    return {
        getInputData: audienceControlMapReduce.getInputData,
        map: audienceControlMapReduce.map
    };
});
