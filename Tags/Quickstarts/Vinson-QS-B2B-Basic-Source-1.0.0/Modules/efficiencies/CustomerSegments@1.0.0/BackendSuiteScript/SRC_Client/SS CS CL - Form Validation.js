/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */
define([
    'N/ui/message',
    '../Configuration/SS CS - Backend Configuration'
],
    function CustomerSegmentClientScript(
        message,
        configuration
) {
        var audienceControlClient = {
            fieldChanged: function fieldChanged(context) {
                var currentRecord = context.currentRecord;
                var field = currentRecord.getField({ fieldId: 'custpage_savesearchfield' });
                var fieldId = context.fieldId;
                var selectedRecordValue = currentRecord.getValue({ fieldId: 'custpage_selectrecordfield' });
                if (fieldId === 'custpage_selectrecordfield') {
                    field.removeSelectOption({
                        value: null
                    });
                    field.insertSelectOption({
                        value: '-', text: '-'
                    });
                    if (selectedRecordValue === 'customer') {
                        util.each(configuration.customerSaveSearch, function customerSaveSearchResult(result) {
                            field.insertSelectOption({
                                value: result,
                                text: result
                            });
                        });
                    } else if (selectedRecordValue === 'item') {
                        util.each(configuration.itemSaveSearch, function itemSaveSearchResult(result) {
                            field.insertSelectOption({
                                value: result,
                                text: result
                            });
                        });
                    }
                }
            },
            showErrorMessage: function showErrorMessage(msg) {
                var errMsg = message.create({
                    title: 'Message',
                    message: msg,
                    type: message.Type.ERROR
                });
                errMsg.show({ duration: 5000 });
            },
            saveRecord: function saveRecord(context) {
                var currentRecord = context.currentRecord;
                var recordField = currentRecord.getText({ fieldId: 'custpage_selectrecordfield' });
                var saveSearchField = currentRecord.getText({ fieldId: 'custpage_savesearchfield' });
                var segmentsField = currentRecord.getValue({ fieldId: 'custpage_customersegments' });

                if (recordField === '-') {
                    audienceControlClient.showErrorMessage('Please Select A Record');
                } else if (saveSearchField === '-' || saveSearchField === '') {
                    audienceControlClient.showErrorMessage('Please Select A Save Search');
                } else if (segmentsField[0] === '') {
                    audienceControlClient.showErrorMessage('Please Select A Customer Segments');
                } else {
                    audienceControlClient.showInfoMessage(context);
                }
                return true;
            },
            showInfoMessage: function showInfoMessage(context) {
                var currentRecord = context.currentRecord;
                var myMsg = message.create({
                    title: 'Message',
                    message: currentRecord.getText({ fieldId: 'custpage_selectrecordfield' }) + ' MapReduce script is now processing!',
                    type: message.Type.INFORMATION
                });
                myMsg.show({ duration: 5000 });
            }
        };
        return {
            fieldChanged: audienceControlClient.fieldChanged,
            saveRecord: audienceControlClient.saveRecord
        };
    });
