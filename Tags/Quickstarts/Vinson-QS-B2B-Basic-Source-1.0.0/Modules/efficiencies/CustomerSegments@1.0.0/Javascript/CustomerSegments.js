define('CustomerSegments', [
    'CustomerSegments.Helper',

    'CustomerSegments.Categories',

    'CustomerSegments.Facets.Router',
    'CustomerSegments.ItemDetails.Router',

    'CustomerSegments.Header.Logo.View',
    'CustomerSegments.Home.View',

    'LiveOrder.Model',

    'ItemDetails.Collection',
    'ItemRelations.Related.Collection',
    'ItemRelations.Correlated.Collection',

    'Utils'
], function CustomerSegments(
    Helper,

    Categories,

    FacetsRouter,
    ItemDetailsRouter,

    HeaderLogoView,
    HomeView,

    LiveOrderModel,

    ItemDetailsCollection,
    RelatedCollection,
    CorrelatedCollection
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            // Filter items base on customer group
            Helper.filterCollection(ItemDetailsCollection);
            // Filter related items base on customer groups
            Helper.filterCollection(RelatedCollection, 'relateditems_detail');
            // Filter correlated items base on customer groups
            Helper.filterCollection(CorrelatedCollection, 'correlateditems_detail');
            // Filter items base on customer groups that have been added to cart
            Helper.filterCart(LiveOrderModel);

            application.getLayout().on('beforeAppendView', function applicationGetLayout() {
                // Check if Facets is already been add to searchApiMasterOptions
                if (!Helper.inFacets('custitem_item_customersegments')) {
                    // Update searchApiMasterOptions to add Customer Segment Facets
                    Helper.addToSearchApiMasterOptions();
                }
            });
        }
    };
});
