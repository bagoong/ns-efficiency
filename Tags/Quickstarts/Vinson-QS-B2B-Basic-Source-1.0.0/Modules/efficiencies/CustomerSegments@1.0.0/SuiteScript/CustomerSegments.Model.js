define('CustomerSegments.Model', [
    'SC.Model',
    'SearchHelper',
    'underscore'
], function CustomerSegmentsModel(
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    return SCModel.extend({
        name: 'CustomerSegments',

        record: 'entitygroup',

        filters: [
            { fieldName: 'isinactive', operator: 'is', value1: 'F' }
        ],

        columns: {
            internalid: { fieldName: 'internalid' },
            groupname: { fieldName: 'groupname' },
            logo: { fieldName: 'custentity_customersegment_logo', type: 'object' },
            banner: { fieldName: 'custentity_customersegment_banner', type: 'object' },
            category: { fieldName: 'custentity_entitygroup_commercecategory', type: 'object' },
            bannertext: { fieldName: 'custentity_customersegment_banner_text' }
        },

        get: function get(groupIds) {
            var search;
            var results;
            var banners = [];
            var logos = [];
            var categories = [];

            var category;

            search = new SearchHelper(this.record, this.filters, this.columns);

            search.addFilter({
                fieldName: this.columns.internalid.fieldName,
                operator: 'anyof',
                value1: groupIds.split(',')
            });

            search.search();

            results = search.getResults();

            if (!results) {
                throw notFoundError;
            }

            _(results).each(function eachResult(result) {
                category = _.pick(result.category, 'name').name || '';
                category = category.split(',');

                logos.push(_.pick(result.logo, 'name').name);
                banners.push({
                    label: result.groupname,
                    backgroundimage: _.pick(result.banner, 'name').name,
                    imageLogo: _.pick(result.logo, 'name').name,
                    text: result.bannertext || ''
                });
                categories = _.union(categories, category);
            });

            return {
                logos: logos,
                banners: banners,
                categories: _.compact(categories)
            };
        }
    });
});
