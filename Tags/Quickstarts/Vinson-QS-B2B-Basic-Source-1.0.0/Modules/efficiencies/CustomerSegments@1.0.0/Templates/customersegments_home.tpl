{{!
© 2016 NetSuite Inc.
User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
provided, however, if you are an authorized user with a NetSuite account or log-in, you
may use this code subject to the terms that govern your access and use.
}}

<div class="home">
    <div class="home-banner-top">
        <p class="home-banner-top-message">
            {{translate 'Use promo code <strong>SCADEMO</strong> for <strong>30%</strong> off your purchase'}}
        </p>
    </div>
    <div class="home-slider-container">
        <div class="home-image-slider">
            <ul data-slider class="home-image-slider-list">
                {{#if showThis}}
                {{#if showBanner}}
                {{#each carouselImages}}
                <li>
                    <div class="home-slide-main-container" style="background-image: url('{{backgroundimage}}');">
                        <img src="{{resizeImage imageLogo ../imageHomeSize}}" alt="" />
                        <div class="home-slide-caption">
                            <h2 class="home-slide-caption-title">{{translate label}}</h2>
                            <p>{{translate text}}</p>
                            <div class="home-slide-caption-button-container">
                                <a href="/search" class="home-slide-caption-button">{{translate 'Shop Now'}}</a>
                            </div>
                        </div>
                    </div>
                </li>
                {{/each}}
                {{else}}
                {{#each carouselImages}}
                    <li>
                        <div class="home-slide-main-container">
                            <img src="{{resizeImage this ../imageHomeSize}}" alt="" />
                            <div class="home-slide-caption">
                                <h2 class="home-slide-caption-title">{{translate 'SAMPLE HEADLINE'}}</h2>
                                <p>{{translate 'Example descriptive text displayed on multiple lines.'}}</p>
                                <div class="home-slide-caption-button-container">
                                    <a href="/search" class="home-slide-caption-button">{{translate 'Shop Now'}}</a>
                                </div>
                            </div>
                        </div>
                    </li>
                {{/each}}
                {{/if}}
                {{/if}}
            </ul>
        </div>
    </div>

    <div class="home-banner-main">
        {{#each bottomBannerImages}}
        <div class="home-banner-main-cell-nth{{@index}}">
            <div class="home-banner-main-cell-bg">
                <img src="{{resizeImage this ../imageHomeSizeBottom}}" alt="" >
                <div class="home-banner-main-cell-text">{{translate 'EXAMPLE TEXT'}}</div>
            </div>
        </div>
        {{/each}}
    </div>

    <div class="home-merchandizing-zone">
        <div data-id="your-merchandising-zone" data-type="merchandising-zone"></div>
    </div>

</div>