
define('ThirdPartyShipping', [
    'SC.Configuration',
    'OrderWizard.Module.ThirdPartyShipping',
    'underscore'
],
       function ThirdPartyShipping(
           Configuration,
           OrderWizardModuleThirdPartyShipping,
           _
       ) {
           'use strict';

           return {
               mountToApp: function mountToApp() {
                   var stepsToAddTo = [
                       'Delivery Method',      // Standard
                       'Shipping method',      // Billing First
                       'Checkout Information', // OPC
                       'Review'                // All
                   ];

                   _.each(Configuration.get('checkoutSteps'), function eachCheckoutStep(checkoutStep) {
                       if (_.contains(stepsToAddTo, checkoutStep.name)) {
                           _.each(checkoutStep.steps || {}, function eachStep(step) {
                               step.modules.push([OrderWizardModuleThirdPartyShipping]);
                           });
                       }
                   });
               }
           };
       });
