define('Store.Utils', [
    'underscore',
    'Utils'
], function StoreUtils(
    _,
    Utils
) {
    'use strict';
    var toRadians = function toRadians(degrees) {
        return degrees * Math.PI / 180;
    };

    var extensions = {
        toRadians: toRadians
    };

    _.extend(Utils, extensions);
    _.mixin(extensions);

    return extensions;
});