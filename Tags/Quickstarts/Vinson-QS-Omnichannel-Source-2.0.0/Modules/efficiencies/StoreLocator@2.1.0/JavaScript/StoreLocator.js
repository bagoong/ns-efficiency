/*
    StoreLocator
    Required modules [
        StoreLocator.Configuration - load default configuration,
        StoreLocator.Router - used for routing your applications URL's when using hash tags(#),
        underscore - javascript plugin
   ]
   @return Router new object
*/
define('StoreLocator', [
    'GoogleMap.Configuration',
    'SC.Configuration',
    'StoreLocator.Router',
    'underscore'
], function StoreLocator(
    GoogleMapConfiguration,
    Configuration,
    Router,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            var modulesConfig = Configuration.get('storeLocator');

            _.defaults(modulesConfig, GoogleMapConfiguration);
            modulesConfig.unitsHandler = modulesConfig.unitsHandlers[modulesConfig.units];

            if (!modulesConfig.unitsHandler) {
                throw Error('Units Handler needed');
            }

            application.Configuration.storeLocator = modulesConfig;

            return new Router(application);
        }
    };
});