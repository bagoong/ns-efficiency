define('BOPIS.Router', [
    'Backbone',
    'BOPIS.FAQs.View'
], function BlogRouter(
    Backbone,
    BopisFAQsView
) {
    'use strict';

    return Backbone.Router.extend({

        routes: {
            'pickupinstorefaq': 'bopisFAQs'
        },

        initialize: function initialize(application) {
            this.application = application;
        },

        bopisFAQs: function bopisFAQs() {
            var view = new BopisFAQsView({
                application: this.application
            });

            view.showContent();
        }
    });
});