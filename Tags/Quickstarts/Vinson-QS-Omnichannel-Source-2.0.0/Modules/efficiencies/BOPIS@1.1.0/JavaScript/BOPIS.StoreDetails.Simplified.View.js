define('BOPIS.StoreDetails.Simplified.View', [
    'Backbone',
    'Backbone.CompositeView',
    'GoogleMapsLoader',

    'Store.View.StoreLocator.Directions',
    'Profile.MyStore.StoreLocator.View',

    'bopis_store_details_simplified.tpl',

    'underscore',
    'jQuery'
], function StoreViewStoreDetail(
    Backbone,
    BackboneCompositeView,
    GoogleMapsLoader,

    StoreDirectionsView,
    ProfileMyStoreStoreLocatorView,

    Template,

    _,
    jQuery
) {
    'use strict';


    return Backbone.View.extend({
        template: Template,

        attributes: {
            'id': 'store-detail',
            'class': 'view storeDetail'
        },

        childViews: {
            'Profile.MyStore.StoreLocator': function ProfileMyStoreChildView() {
                if (this.options.showButton) {
                    return new ProfileMyStoreStoreLocatorView({
                        application: this.application,
                        title: _('Choose Store For Pick Up').translate(),
                        btnText: this.hasStore ? 'Change Store' : 'Select Store',
                        model: this.model
                    });
                }
            }
        },

        initialize: function initialize() {
            var self = this;
            BackboneCompositeView.add(this);

            this.model = this.options.model;
            this.application = this.options.application;
            this.configuration = this.application && this.application.getConfig('storeLocator');
            this.mapCoordinates = {
                lat: parseFloat(this.model.get('lat')),
                lng: parseFloat(this.model.get('lon'))
            };
            this.map = null;
            this.marker = null;

            this.options.application.getLayout().on('afterAppendView', function mapResizeOnCollapse() {
                self.mapResize();

                jQuery('.collapse').on('shown.bs.collapse', function onCollapse() {
                    self.mapResize();
                });
            });
        },

        render: function render() {
            this._render();

            if (this.configuration && (this.model && this.model.get('internalid'))) {
                GoogleMapsLoader.loadScript(this.configuration.googleMapsApiKey)
                    .done(_.bind(this.mapCreate, this));
            }

            return this;
        },

        mapCreate: function mapCreate() {
            var google = window.google;
            this.map = new google.maps.Map(this.$('[data-view="StoreLocator.Map"]')[0], {
                center: this.mapCoordinates,
                zoom: 15,
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true,
                disableDefaultUI: true
            });

            this.marker = new google.maps.Marker({
                map: this.map,
                position: this.mapCoordinates
            });
        },

        mapResize: function mapResize() {
            var google;
            var latlngbounds;

            if ( this.map ) {
                google = window.google;
                google.maps.event.trigger(this.map, 'resize');

                latlngbounds = new google.maps.LatLngBounds();
                latlngbounds.extend(new google.maps.LatLng(this.mapCoordinates.lat, this.mapCoordinates.lng));

                this.map.setCenter(latlngbounds.getCenter());
            }
        },

        getContext: function getContext() {
            return {
                model: this.model,
                hasModel: !!(this.model && this.model.get('internalid')),
                configuration: this.configuration,
                showButton: this.options.showButton,
                isMapSizeSmall: this.options.mapSizeSmall
            };
        }
    });
});