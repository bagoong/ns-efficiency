{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="bopis-order-wizard-delivery-method-selector-module">
    <button class="bopis-order-wizard-delivery-method-selector-module-shipping" data-action="submit-delivery" data-value="shipping">
        {{translate 'Shipping'}}
    </button>
    <button class="bopis-order-wizard-delivery-method-selector-module-pickup" data-action="submit-delivery" data-value="pickup">
        {{translate 'Pick Up'}}
    </button>
</div>