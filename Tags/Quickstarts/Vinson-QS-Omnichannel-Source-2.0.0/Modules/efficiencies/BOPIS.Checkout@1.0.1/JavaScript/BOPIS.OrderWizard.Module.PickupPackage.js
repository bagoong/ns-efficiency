define('BOPIS.OrderWizard.Module.PickupPackage', [
    'BOPIS.OrderWizard.Module.PackageBase',
    'BOPIS.OrderWizard.Module.Items.Line.Actions.View',
    'underscore',
    'Utils'
], function BOPISOrderWizardModuleBOPISPackageCreation(
    PackageBase,
    Actions,
    _
) {
    'use strict';

    return PackageBase.extend({
        currentStatus: 'pickup',
        oppositeStatus: 'shipping',

        getActionsView: function getActionsView() {
            return Actions.extend({
                statusToSet: this.oppositeStatus,
                btnText: _('Change to Ship').translate(),
                keyMappingCondition: '_isshippable'
            });
        },

        getCollection: function getCollection() {
            return this.wizard.model.getLinesForPicking();
        },

        getPackageSubtitle: function getPackageSubtitle() {
            return _.translate('Items for Pickup');
        }
    });
});