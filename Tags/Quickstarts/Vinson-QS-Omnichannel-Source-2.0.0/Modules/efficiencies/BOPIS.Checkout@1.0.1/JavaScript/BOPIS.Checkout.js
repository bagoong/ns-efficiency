define('BOPIS.Checkout', [
    'SC.Checkout.Configuration',
    'SC.Checkout.Configuration.Steps.BOPIS',
    'underscore',
    'BOPIS',
    'BOPIS.OrderWizard.Step',
    'BOPIS.OrderWizard.Router'

], function InStorePickup(
    CheckoutConfiguration,
    CheckoutConfigStepsISP,
    _
) {
    'use strict';

    var SiteApplicationConfiguration = {
        checkoutSteps: CheckoutConfigStepsISP
    };

    _.extend(CheckoutConfiguration, SiteApplicationConfiguration);

    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, CheckoutConfiguration);
        }
    };
});