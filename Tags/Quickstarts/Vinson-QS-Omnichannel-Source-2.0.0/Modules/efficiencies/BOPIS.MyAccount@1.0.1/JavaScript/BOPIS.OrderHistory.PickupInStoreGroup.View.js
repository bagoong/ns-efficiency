/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

define('BOPIS.OrderHistory.PickupInStoreGroup.View', [
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',

    'ItemViews.Cell.Navigable.View',
    'ItemViews.Cell.Actionable.View',
    'ItemViews.Item.QuantityAmount.View',
    'OrderHistory.Item.Actions.View',
    'BOPIS.OrderHistory.Fulfillment.StorePickup.View',
    'BOPIS.StoreDetails.Simplified.View',

    'Store.Model',

    'bopis_order_history_pickupinstore_group.tpl',
    'SC.Configuration',

    'underscore',
    'Utils'
], function BOPISOrderHistoryPickupInStoreGroupView(
    Backbone,
    BackboneCompositeView,
    BackboneCollectionView,

    ItemViewsCellNavigableView,
    ItemViewsCellActionableView,
    ItemViewsItemQuantityAmountView,
    OrderHistoryItemActionsView,
    OrderHistoryFulfillmentView,
    StoreDetailView,

    Store,

    orderHistoryPickUpInStoreTpl,
    Configuration,

    _
) {
    'use strict';

    return Backbone.View.extend({
        // @property  {Function} template
        template: orderHistoryPickUpInStoreTpl,

        // @method initialize
        initialize: function initialize() {
            BackboneCompositeView.add(this);
        },

        // @property {Object} childViews
        childViews: {
            'StoreDetail': function childViewStoreDetail() {
                return new StoreDetailView({
                    application: this.options.application,
                    model: this.model.get('location') ? new Store(this.model.get('location')) : new Store(),
                    showButton: false,
                    mapSizeSmall: true
                });
            },

            'Fullfillments.Collection': function childViewFulfillments() {
                return new BackboneCollectionView({
                    collection: this.model.get('fulfillments'),
                    childView: OrderHistoryFulfillmentView,
                    viewsPerRow: 1,
                    childViewOptions: {
                        application: this.options.application
                    }
                });
            }
        },

        getContext: function getContext() {
            var linesLength = 0;
            this.accordionLimit = Configuration.accordionCollapseLimit;
            this.model.get('fulfillments').each(function eachFulfillment(fulfillment) {
                linesLength += fulfillment.get('lines').length;
            });

            return {
                model: this.model,
                showFulfillment: this.model.get('fulfillments').length > 0,
                targetId: 'products-ship-' + this.model.get('internalid').replace(/[^0-9a-zA-Z]/g, '-'),
                targetPendingId: 'products-pending-ship-' + this.model.get('internalid').replace(/[^0-9a-zA-Z]/g, '-'),
                targetAddressId: 'address-ship-' + this.model.get('internalid').replace(/[^0-9a-zA-Z]/g, '-'),
                showFulfillmentAcordion: linesLength > this.accordionLimit,
                initiallyCollapsed: (_.isPhoneDevice()) ? '' : 'in',
                initiallyCollapsedArrow: (_.isPhoneDevice()) ? 'collapsed' : ''
            };
        }
    });
});