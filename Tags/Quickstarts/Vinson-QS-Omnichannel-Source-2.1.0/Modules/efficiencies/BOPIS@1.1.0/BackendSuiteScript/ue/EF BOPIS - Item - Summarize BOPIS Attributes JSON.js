/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

define([
    '../libs/Bopis.Item'
], function EFBOPISItemSummarizeBOPISAttributesJSON(
    bopisItem
) {
    'use strict';

    var Handlers  = {
        beforeSubmit: function beforeSubmit(context) {
            // Initialization
            var itemRecord = context.newRecord;
            bopisItem.setSummarizedLocations(itemRecord);
        }
    };

    return Handlers;
});