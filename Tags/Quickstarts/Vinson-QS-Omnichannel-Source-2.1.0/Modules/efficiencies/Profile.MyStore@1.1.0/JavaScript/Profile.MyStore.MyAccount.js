define('Profile.MyStore.MyAccount', [
    'Profile.MyStore.Router'
], function ProfileMyStore(
    Router
) {
    'use strict';

    return {
        MenuItems: {
            parent: 'settings',
            id: 'mystore',
            name: 'My Store',
            url: 'mystore',
            index: 2
        },
        mountToApp: function mountToApp(application) {
            return new Router(application);
        }
    };
});