define('BOPIS.OrderWizard.Step', [
    'OrderWizard.Step',
    'underscore'
], function BOPISOrderWizardStep(
    OrderWizardStep,
    _
) {
    'use strict';

    /**
     * Fixes a bug on the reference implementation in which it posts 1 time more than needed
     * @type {Function}
     */
    OrderWizardStep.prototype.initialize = _.wrap(OrderWizardStep.prototype.initialize, function bopisInitialize(fn) {
        var self = this;
        fn.apply(this, _.toArray(arguments).slice(1));
        this.listenTo(this.wizard.model, 'sync', function onListenTo(model, attributes, options) {
            self.currentModelState = JSON.stringify(self.wizard.model);

        });
    });
});