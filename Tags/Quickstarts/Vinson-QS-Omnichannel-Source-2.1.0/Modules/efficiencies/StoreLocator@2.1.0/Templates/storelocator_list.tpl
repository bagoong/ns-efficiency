<div id="storelocator-list-container" class="storelocator-navigation-list-wrapper">
    {{#if hadSearched}}
    <h4 class="storelocator-list-header">
        {{#if hasNearest}}
            {{translate 'Your nearest $(0) stores' nearestLength}}
        {{else}}
            {{translate 'No store near you'}}
        {{/if}}
    </h4>
    {{/if}}

    {{#if hasNearest}}
        <ul data-view="StoreLocator.List.Nearest" id="storelocator-list-active" class="stores-store-locator-navigation-list"></ul>
    {{/if}}
</div>