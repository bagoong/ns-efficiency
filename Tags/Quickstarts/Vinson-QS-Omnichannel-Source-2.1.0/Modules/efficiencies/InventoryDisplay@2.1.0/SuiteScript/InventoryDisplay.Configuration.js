define('InventoryDisplay.Configuration', [
    'Configuration',
    'underscore'
], function InventoryDisplayConfiguration(
    GlobalConfiguration,
    _
) {
    'use strict';

    var inventoryDisplay = SC.Configuration.inventoryDisplay;

    var Configuration =  {
        isDisplayAsBadge: inventoryDisplay.badges.enable,

        ITEM_TYPES_WITH_INVENTORY: ['InvtPart'],
        STATUS_IN_STOCK: '1',
        STATUS_OUT_OF_STOCK: '2',
        STATUS_LOW_STOCK: '3',
        STATUS_AVAILABLE: '4',
        STATUS_NOT_AVAILABLE: '5',
        STATUS_NOT_PURCHASABLE: '6',

        LOCATION_TYPE_WAREHOUSE: 2,
        LOCATION_TYPE_STORE: 1
    };

    var stockStatus = {};

    stockStatus[Configuration.STATUS_IN_STOCK] = {
        badgeLabel: inventoryDisplay.inStock.badgeMessage,
        badgeClass: 'inventory-display-bg-in-stock',
        textLabel: inventoryDisplay.inStock.textMessage,
        textLabelForPrefferedStock: inventoryDisplay.moreInStock.textMessage,
        textClass: 'inventory-display-font-in-stock'
    };

    stockStatus[Configuration.STATUS_LOW_STOCK] = {
        badgeLabel:  inventoryDisplay.lowStock.badgeMessage,
        badgeClass: 'inventory-display-bg-limited-stock',
        textLabel:   inventoryDisplay.lowStock.textMessage,
        textClass: 'inventory-display-font-limited-stock'
    };

    stockStatus[Configuration.STATUS_OUT_OF_STOCK] = {
        badgeLabel: inventoryDisplay.outOfStock.badgeMessage,
        badgeClass: 'inventory-display-bg-out-stock',
        textLabel:  inventoryDisplay.outOfStock.textMessage,
        textClass: 'inventory-display-font-out-stock'
    };

    stockStatus[Configuration.STATUS_AVAILABLE] = {
        badgeLabel: inventoryDisplay.available.badgeMessage,
        badgeClass: 'inventory-display-bg-available',
        textLabel:   inventoryDisplay.available.textMessage,
        textClass: 'inventory-display-font-available'
    };

    stockStatus[Configuration.STATUS_NOT_AVAILABLE] = {
        badgeLabel: inventoryDisplay.notAvailable.badgeMessage,
        badgeClass: 'inventory-display-bg-not-available',
        textLabel:    inventoryDisplay.notAvailable.textMessage,
        textClass: 'inventory-display-font-not-available'
    };

    stockStatus[Configuration.STATUS_NOT_PURCHASABLE] = {
        badgeLabel: inventoryDisplay.notPurchaseable.badgeMessage,
        badgeClass: 'inventory-display-bg-not-purchasable',
        textLabel: inventoryDisplay.notPurchaseable.textMessage,
        textClass: 'inventory-display-font-not-purchasable'
    };

    Configuration.STOCK_STATUS =  stockStatus;

    _.extend(Configuration, {
        get: function get() {
            return Configuration;
        }
    });

    _.extend(GlobalConfiguration, {
        inventoryDisplay: Configuration
    });

    if (!GlobalConfiguration.publish) {
        GlobalConfiguration.publish = [];
    }

    GlobalConfiguration.publish.push({
        key: 'InventoryDisplay_config',
        model: 'InventoryDisplay.Configuration',
        call: 'get'
    });

    return Configuration;
});
