// @module InventoryDisplay
define('InventoryDisplay.Status.View', [
    'Backbone',
    'inventorydisplay_status.tpl',
    'jQuery',
    'underscore'
], function InventoryDisplayStatusView(
    Backbone,
    Template,
    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,
        orderType: '',
        hasFetchedStockData: null,
        stockModel: null,
        fetchStockData: jQuery.noop,
        getMyStore: jQuery.noop,

        initialize: function initialize(options) {
            this.stockModel = options.stockModel;

            if (_.isUndefined(this.stockModel) || _.isEmpty(this.stockModel)) {
                if (this.allowFetchStockData()) {
                    this.fetchStockData();
                }else{
                    this.getStockData();
                }
            }else{
                this.hasFetchedStockData = true;
            }
        },

        getModuleConfig: function getModuleConfig() {
            return this.options.application.getConfig('InventoryDisplay');
        },

        isInventoryItem: function isInventoryItem() {
            return _.contains(
                        this.getModuleConfig().ITEM_TYPES_WITH_INVENTORY,
                        this.options.parentItemModel.get('_itemType')
                    );
        },

        allowFetchStockData: function allowFetchStockData() {
            var isSelectionComplete = ( this.model.get('itemoptions_detail')
                                        && this.model.get('itemoptions_detail').matrixtype === 'child' ) ?
                                        true :
                                        this.options.parentItemModel && this.options.parentItemModel.isSelectionComplete();

            return this.isInventoryItem() &&
                   isSelectionComplete &&
                   this.getModelBopisAble() &&
                   this.model.get('_isInStock');
        },

        isForShipping: function isForShipping() {
            return this.orderType === 'shipping';
        },

        isForPickup: function isForPickup() {
            return this.orderType === 'pickup';
        },

        getModelBopisAble: function getModelBopisAble() {
            return this.isForShipping() ? this.model.get('_isshippable') :
                   (this.isForPickup() ? this.model.get('_ispickable') : false );
        },

        getStockData: function getStockData() {
            var newModel = false;
            var statusModel = null;

            if (this.isInventoryItem()) {
                if (this.getModelBopisAble()) {
                    if (!this.model.get('_isInStock')) {
                        newModel = true;
                        statusModel = (this.model.get('_isBackorderable')) ?
                                        this.getModuleConfig().STATUS_AVAILABLE :
                                        this.getModuleConfig().STATUS_OUT_OF_STOCK;
                    }
                }
            } else {
                newModel = true;
                statusModel = (this.model.get('_isBackorderable') || this.model.get('_isInStock')) ?
                                this.getModuleConfig().STATUS_AVAILABLE :
                                this.getModuleConfig().STATUS_OUT_OF_STOCK ;
            }

            if (newModel) {
               this.stockModel = new Backbone.Model({
                                    badge: statusModel,
                                    internalid: this.model.get('internalid'),
                                    stock: 0
                                });

               this.hasFetchedStockData = true;
            }
        },

        checkIfBackOrderable: function checkIfBackOrderable() {
            if ( this.model && this.model.get('_isBackorderable') ) {
                this.stockModel.set('badge', this.getModuleConfig().STATUS_AVAILABLE, {silent: true});
            }
        },

        getContext: function getContext() {
            var store = null;
            var htmlClass = '';
            var statusText = '';
            var stockLocation = '';
            var stockStatus;
            var allowToCheckOtherStore = false;

            if ( this.stockModel &&
                this.stockModel.get('badge') === parseInt(this.getModuleConfig().STATUS_OUT_OF_STOCK, 10)) {
                this.checkIfBackOrderable();
            }

            stockStatus = this.stockModel ?
                              this.getModuleConfig().STOCK_STATUS[ this.stockModel.get('badge') ] : null;

            if ( this.isForPickup() ) {
                store = this.getMyStore();
                stockLocation = store.get('internalid') ? 'at ' + store.get('name') : '';
                if ( (!this.model.get('_ispickable')) &&
                   this.stockModel.get('badge') === this.getModuleConfig().STATUS_NOT_AVAILABLE ) {
                    stockLocation = _(' for pickup').translate();
                }
            }

            if( this.isForShipping() ) {
                stockLocation = _(' for shipping').translate();
            }

            if ( this.getModuleConfig().isDisplayAsBadge ) {
                htmlClass = stockStatus && stockStatus.badgeClass;
                statusText = stockStatus && stockStatus.badgeLabel;
            } else {
                htmlClass = stockStatus && stockStatus.textClass;
                statusText = (this.stockModel && this.stockModel.get('isStockValPreffered')) ?
                                stockStatus && stockStatus.textLabelForPrefferedStock :
                                stockStatus && stockStatus.textLabel;
            }

            return {
                viewID: this.cid,
                iconName: (!_.isEmpty(this.orderType)) ? 'inventory-display-' + this.orderType + '-icon' : false,
                hasFetchedStockData: this.hasFetchedStockData,
                isDisplayAsBadge: this.getModuleConfig().isDisplayAsBadge,
                hasStockModel: !!this.stockModel,
                stock: this.stockModel && this.stockModel.get('stock') || 0,
                htmlClass: htmlClass,
                statusText: _(statusText).translate(),
                stockLocation: _(stockLocation).translate(),
                isForShipping: this.isForShipping(),
                isForPickup: this.isForPickup(),
                isShippable: this.model && this.model.get('_isshippable'),
                isPickable: this.model && this.model.get('_ispickable'),
                hasStore: store && store.get('internalid'),
                allowToCheckOtherStore: this.isForPickup() && (store && store.get('internalid')) &&
                                        ( this.stockModel &&
                                          this.stockModel.get('badge') !== this.getModuleConfig().STATUS_NOT_AVAILABLE )
            };
        }
    });
});