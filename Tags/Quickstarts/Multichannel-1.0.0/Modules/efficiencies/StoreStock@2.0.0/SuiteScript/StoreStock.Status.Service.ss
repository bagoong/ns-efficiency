function service (request) {
    'use strict';
    var method = request.getMethod();
    var	id = request.getParameter('id');
    var store = request.getParameter('stores');
    var StoreStock = require('StoreStock.Model');

    var Application = require('Application');
    try {
        switch (method) {
        case 'GET':
            Application.sendContent(id ?
                StoreStock.listDetail(id, store) : [],
                {'cache': response.CACHE_DURATION_SHORT}
            );
            break;

        default:
            Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}