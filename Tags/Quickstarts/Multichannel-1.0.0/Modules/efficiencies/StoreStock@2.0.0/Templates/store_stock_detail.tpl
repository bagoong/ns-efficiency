{{#if hasModel}}
<div>
    {{#if isDisplayAsBadge }}
        <span class="{{badgeClass}}">{{ translate badgeText }}</span>
    {{else}}
        {{#if isValuePreferredStock }}
            {{ translate "More than $(0) items in stock" value }}
        {{else}}
            {{ translate "$(0) items in stock" value }}
        {{/if}}
    {{/if}}
</div>
{{/if}}