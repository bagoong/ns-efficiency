<div id="storelist" class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xl-12 col">
                <h3 itemprop="name" class="panel-title">{{ translate 'Store List' }}</h3>
            </div>
            <div itemscope itemtype="http://schema.org/{{configuration.SchemaOrg}}" class="col-xl-12 col storelocator-storelist" data-view="StoreList.Store">

            </div>
            <a href="stores" class="store-locator-back-button">{{translate 'Back to store locator'}}</a>
        </div>
    </div>
</div>