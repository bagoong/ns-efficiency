<div data-type="stores-list-store" data-id="{{ model.internalid }}" class="col-xs-12 col-sm-6 col-md-3 storelocator-store">
    <div itemscope itemtype="http://schema.org/{{configuration.SchemaOrg}}" class="storeSection" id="store-{{model.internalid}}">
        <a class="storelocator-group-store-button" href="/stores/{{model.urlcomponent}}">
            <span itemprop="name" class="panel-title">{{ translate model.name }}</span>
        </a>
    </div>
</div>