/**
 * Store.Views.StoreLocator.Search.js
 * Store Locator Search view - Create/setup search region
 */
define('Store.Views.StoreLocator.Search', [
    'Backbone',
    'GoogleMapsLoader',
    'storelocator_search.tpl',
    'jQuery',
    'underscore',
    'Utils'
], function storeLocatorSearch(Backbone, GoogleMapsLoader, Template, jQuery, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        events: {
            'click [name=geolocate]': 'getCurrentLocation',
            'submit form': 'submitSearch'
        },

        initialize: function initialize(options) {
            var self = this;

            this.application = options.application;
            this.configuration = options.configuration;
            this.eventBus = options.eventBus;

            this.storeSearchText = options.storeSearchText && options.storeSearchText || '';
            this.isInStoreStock = options.isInStoreStock
        },

        getContext: function getContext() {
            return {
                showGeolocationButton: this.configuration.geolocationEnabled,
                listStoreLink: '/storelist',
                storeSearchText: this.storeSearchText
            };
        },

        render: function render() {
            this._render();

            GoogleMapsLoader.loadScript(this.options.configuration.googleMapsApiKey)
                .done(_.bind(this.searchInit, this));

            return this;
        },

        searchInit: function searchInit() {
            var self = this;
            var place;
            var autocomplete = new google.maps.places.Autocomplete(
                this.$('*[data-storelist-place="store-list-place"]')[0]
            );

            this.geocoder = new google.maps.Geocoder;

            // Bind event on place changed
            google.maps.event.addListener(autocomplete, 'place_changed', function googleMapsEventAddListener() {
                place = autocomplete.getPlace();

                if (!place.geometry) {
                    return;
                }

                self.options.eventBus.trigger('locationChanged', {
                    location: place.geometry.location,
                    component: 'search'
                });
            });

            this._distanceFilter();

            if (this.options.configuration.geolocationEnabled && this.options.configuration.geolocateOnInit) {
                this.getCurrentLocation();
            }

            if (this.isInStoreStock) {
                if(  this.storeSearchText && this.storeSearchText !== '' ) {
                    this.submitSearch();
                }else{
                    this.getCurrentLocation();
                }
            }

        },

        _distanceFilter: function distanceFilter() {
            var elements = _.map(this.configuration.distanceFilters, function map(l) {
                return jQuery('<option />', {
                    'data-unit': l.unit,
                    value: l.value,
                    text: _.translate(l.text, l.value),
                    selected: l.isDefault ? 'selected' : null
                });
            });

            this.$('*[data-storelist-place="store-list-place"]').append(elements);
        },

        submitSearch: function submitSearch(e) {
            var self = this;
            var address = this.$('*[data-storelist-place="store-list-place"]').val();
            var request;

            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }

            if (!address || address.length < 1) {
                return;
            }

            request = {
                address: address
            };

            if (this.geocoder) {
                this.geocoder.geocode(request, function geocode(result, status) {
                    if (status !== google.maps.GeocoderStatus.OK) {
                        return;
                    }

                    self.options.eventBus.trigger('locationChanged', {
                        location: result[0].geometry.location,
                        component: 'search'
                    });
                });
            }
        },

        getCurrentLocation: function getCurrentLocation() {
            var self = this;
            var location;

            if (window.navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function getCurrentPosition(pos) {
                    var $searchBox;
                    location = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

                    self.options.eventBus.trigger('locationChanged', {
                        location: location,
                        component: 'search'
                    });

                    self.geocoder.geocode({'latLng': location}, function geocode(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $searchBox = self.$('*[data-storelist-place="store-list-place"]');
                                $searchBox.val(results[0].formatted_address);
                            }
                        }
                    });
                },  undefined, /** @type GeolocationPositionOptions */({
                    maximumAge: 60 * 1000,
                    timeout: 10 * 1000
                }));
            }
        }
    });
});