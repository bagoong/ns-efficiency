define('GuestOrderStatus.Model', [
    'GuestOrderStatus.Configuration',
    'SC.Model',
    'SearchHelper',
    'underscore'
], function GuestOrderStatusModel(
    Configuration,
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    return SCModel.extend({
        name: 'GuestOrderStatus',
        record: 'salesorder',
        filters: [
            {fieldName: 'mainline', join: null, operator: 'is', value1: 'T'}
        ],
        columns: {
            internalid: {fieldName: 'internalid'},
            orderid: {fieldName: 'tranid'},
            status: {fieldName: 'status', type: 'text'},
            trackingnumbers: {fieldName: 'trackingnumbers'},
            shipmethod: {fieldName: 'shipmethod', type: 'text'}
        },

        proxy: function proxy(data, request) {
            var serviceUrl = nlapiResolveURL(
                'SUITELET',
                'customscript_ef_gos_sl_guest_order',
                'customdeploy_ef_gos_sl_guest_order',
                false
            );
            var currentDomainMatch = request.getURL().match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
            var currentDomain = currentDomainMatch && currentDomainMatch[0];
            var relativeServiceUrl = serviceUrl.replace(/^.*\/\/[^\/]+/, '');
            var finalServiceUrl;
            var apiResponse;
            var responseData;
            var unknownError = {
                status: 500,
                code: 'ERR_UNKNONW',
                message: 'Internal error'
            };
            if (currentDomain[currentDomain.length - 1] === '/') {
                currentDomain = currentDomain.slice(0, -1);
            }


            data.website = session.getSiteSettings(['siteid']).siteid;
            data.orderid = (data.orderid || '').split('-').pop();

            finalServiceUrl = currentDomain + relativeServiceUrl;
            apiResponse = nlapiRequestURL(finalServiceUrl, JSON.stringify(data), request.getAllHeaders(), 'POST');

            try {
                responseData = JSON.parse(apiResponse.getBody());
            } catch (e) {
                throw unknownError;
            }

            if (parseInt(apiResponse.getHeader('Custom-Header-Status'), 10) !== 200) {
                throw _.extend({}, {
                    status: apiResponse.getHeader('Custom-Header-Status'),
                    code: responseData.errorCode,
                    message: responseData.errorMessage
                });
            }

            return responseData;
        },

        get: function get(data) {
            var orderId = data.orderid;
            var secondField = data.secondField;
            var secondFieldValue = data[secondField];
            var secondFieldConfig;
            var search;
            var result;


            if (!orderId || (!secondFieldValue)) {
                throw notFoundError;
            }

            secondFieldConfig = _.findWhere(Configuration.secondField, {id: secondField});

            if (!secondFieldConfig) {
                throw methodNotAllowedError;
            }

            this.columns[secondField] = { fieldName: secondFieldConfig.id };
            this.filters.push({fieldName: this.columns.orderid.fieldName, operator: 'is', value1: orderId });
            this.filters.push({fieldName: 'website', operator: 'is', value1: data.website });
            this.filters.push({fieldName: secondField, operator: 'is', value1: secondFieldValue});

            search = new SearchHelper(this.record, this.filters, this.columns);
            result = search.search().getResults();
            if (!result || result.length !== 1) {
                throw notFoundError;
            }

            // For form purposes.
            result[0].secondField = secondField;

            return result[0];
        }
    });
});
