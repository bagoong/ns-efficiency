define('BOPIS.OrderHistory.Details.View', [
    'Backbone.CollectionView',
    'OrderHistory.Details.View',
    'BOPIS.OrderHistory.PickupInStoreGroup.View',
    'Transaction.Collection',
    'Profile.Model',
    'order_history_details_shipped_items.tpl',
    'PluginContainer',
    'underscore',
    'bignumber',
    'Utils'
], function BOPISOrderHistoryDetailsView(
    BackboneCollectionView,
    View,
    OrderHistoryPickupInStoreGroupView,
    TransactionCollection,
    Profile,
    orderHistoryDetailsShippedItemsTpl,
    PluginContainer,
    _,
    BigNumber,
    Utils
) {
    'use strict';

    View.prototype.preRenderPlugins =
        View.prototype.preRenderPlugins || new PluginContainer();

    View.prototype.preRenderPlugins.install({
        name: 'BOPIS.OrderHistoryDetail',
        execute: function execute($el /* , view */) {
            $el
                .find('[data-view="ShipGroups"]')
                .before('<div class="order-history-details-pickupinstore-groups" data-view="PickupInStoreGroups"></div>');
        }
    });

    _.extend(View.prototype, {
        initialize: _.wrap(View.prototype.initialize, function wrapInitialize(fn) {
            fn.apply(this, _.toArray(arguments).slice(1));

            _.extend(View.prototype.childViews, {
                'PickupInStoreGroups': function childViewPickupInstore() {
                    return new BackboneCollectionView({
                        collection: this.getPickupInStoreGroups(),
                        childView: OrderHistoryPickupInStoreGroupView,
                        cellTemplate: orderHistoryDetailsShippedItemsTpl,
                        viewsPerRow: 1,
                        childViewOptions: {
                            application: this.application
                        }
                    });
                }
            });
        }),

        getPickupInStoreLines: function getShippableLines() {
            return this.model.get('lines').where({linegroup: 'bopis'});
        },
        getPickupInStoreGroups: function getPickupInStoreGroups() {
            var self = this;
            var pickupInStoreLines = this.getPickupInStoreLines();
            var amount = 0;
            var bopisGroups = {};
            var profile = Profile.getInstance();
            var pendingLine;
            var pickedLine;

            _.each(pickupInStoreLines || [], function eachPickupInStoreLines(line) {
                var storeLocation = line.get('location').internalid;
                var pickupBy = self.model.get('pickup_by') && self.model.get('pickup_by') !== '' ?
                               self.model.get('pickup_by') :
                               profile.get('firstname') + ' ' + profile.get('lastname');

                var pickupByEmail = self.model.get('pickup_by_email') && self.model.get('pickup_by') !== '' ?
                                    self.model.get('pickup_by_email') :
                                    profile.get('email');

                if (storeLocation) {
                    bopisGroups[storeLocation] = bopisGroups[storeLocation] || {
                        internalid: storeLocation,
                        pickup_by: pickupBy,
                        pickup_by_email: pickupByEmail,
                        location: line.get('location'),
                        pending: {
                            lines: [],
                            status: {
                                internalid: 'pending',
                                name: _('Processing Items').translate()
                            }
                        },
                        fulfillments: new TransactionCollection([
                            {
                                internalid: 'picked',
                                lines: [],
                                status: {
                                    internalid: 'picked',
                                    name: _('Ready for Pickup').translate()
                                }
                            }
                        ])
                    };

                    if (line.get('quantitybackordered') || line.get('quantitypicked')) {
                        if (line.get('quantitybackordered')) {
                            pendingLine = line.clone();
                            amount = BigNumber(line.get('rate')).times(pendingLine.get('quantity')).toNumber();
                            pendingLine.set('quantity', line.get('quantitybackordered'));
                            pendingLine.set('amount', amount);
                            pendingLine.set('amount_formatted', Utils.formatCurrency(amount));
                            pendingLine.set('item', line.get('item').attributes);

                            bopisGroups[storeLocation].pending.lines.push(pendingLine);
                        }

                        if (line.get('quantitypicked')) {
                            if (line.get('quantitypicked') === line.get('quantity')) {
                                pickedLine = line;
                            } else {
                                pickedLine = line.clone();
                                amount = BigNumber(line.get('rate')).times(pickedLine.get('quantity')).toNumber();
                                pickedLine.set('quantity', line.get('quantitypicked'));
                                pickedLine.set('amount', amount);
                                pickedLine.set('amount_formatted', Utils.formatCurrency(amount));
                                pickedLine.set('item', line.get('item').attributes);
                            }

                            bopisGroups[storeLocation].fulfillments.get('picked').get('lines').add(pickedLine);
                        }
                    }
                }
            });


            // This trick makes for a better UI than having 3 panels
            _.each(bopisGroups, function eachBopisGroup(bopisGroup) {
                bopisGroup.fulfillments.add(bopisGroup.pending);
                delete bopisGroup.pending;
            });

            this.model.get('storepickupfulfillments').each(function eachStorePickupFulfillment(storeFulfillment) {
                var lines = _.compact(storeFulfillment.get('lines').map(function mapLine(line) {
                    var fulfilledLine;
                    var originalLine = self.model.get('lines').get(line.get('internalid'));

                    if (originalLine && originalLine.get('linegroup') === 'bopis') {
                        if (originalLine.get('quantity') === line.get('fulfilled')) {
                            fulfilledLine = originalLine;
                        } else {
                            fulfilledLine = originalLine.clone();
                            amount = BigNumber(originalLine.get('rate')).times(fulfilledLine.get('quantityfulfilled')).toNumber();
                            fulfilledLine.set('quantity', fulfilledLine.get('quantityfulfilled'));
                            fulfilledLine.set('amount', amount);
                            fulfilledLine.set('amount_formatted', Utils.formatCurrency(amount));
                            fulfilledLine.set('item', originalLine.get('item'), {silent: true});
                        }
                        return fulfilledLine;
                    }
                    return null;
                }));

                if (lines.length) {
                    bopisGroups[storeFulfillment.get('location')] = bopisGroups[storeFulfillment.get('location')] || {
                        internalid: storeFulfillment.get('location'),
                        fulfillments: new TransactionCollection()
                    };
                    bopisGroups[storeFulfillment.get('location')].fulfillments.add(
                        storeFulfillment.clone().set('lines', lines)
                    );
                }
            });

            bopisGroups = _.filter(bopisGroups, function filterPickupInStoreGroups(bopisGroup) {
                bopisGroup.fulfillments.reset(bopisGroup.fulfillments
                    .filter(function filterPicked(fulfillment) {
                        return fulfillment.get('lines').length > 0;
                    }), {silent: true}
                );

                return bopisGroup.fulfillments.length > 0;
            });

            return _.values(bopisGroups);
        }
    });
});