// @module Profile.MyStore
define('Profile.MyStore.Header.View', [
    'Profile.MyStore.Model',
    'Backbone',
    'Backbone.CompositeView',
    'Profile.MyStore.StoreLocator.View',
    'profile_mystore_header.tpl',
    'jQuery'
], function ProfileMyStoreChooseStoreView(
    MyStoreModel,
    Backbone,
    BackboneCompositeView,
    ProfileMyStoreStoreLocatorView,
    myStoreHeaderTpl,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: myStoreHeaderTpl,

        events: {
            'click [data-action="dropdown-click"]': 'dropdownStopPropagation'
        },

        childViews: {
            'Profile.MyStore.StoreLocator': function ProfileMyStoreChildView() {
                return new ProfileMyStoreStoreLocatorView({
                    application: this.options.application,
                    model: this.model
                });
            }
        },

        dropdownStopPropagation: function dropdownStopPropagation(e) {
            // remove dropdown auto close
            e.stopPropagation();
        },

        initialize: function initialize() {
            BackboneCompositeView.add(this);
            this.listenTo(this.model, 'change', jQuery.proxy(this.render, this));
        },

        getContext: function getContext() {
            return {
                hasStore: !this.model.isNew(),
                model: this.model
            };
        }
    });
});