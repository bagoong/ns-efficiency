define('Profile.MyStore.Checkout', [
    'LoginRegister.CheckoutAsGuest.View',
    'LoginRegister.Login.View',
    'LoginRegister.Register.View',
    'Profile.MyStore.Model',
    'underscore'
], function ProfileMyStoreCheckout(
    GuestView,
    LoginView,
    RegisterView,
    ProfileMyStoreModel,
    _
) {
    'use strict';

    var bootstrapMyProfileFn = function bootstrapMyProfileFn(response) {
        response.MyStore && ProfileMyStoreModel.getInstance().set(response.MyStore);
    };

    GuestView.prototype.redirect = _.wrap(GuestView.prototype.redirect,
        function wrapGuestViewRedirect(fn, context, response) {
            bootstrapMyProfileFn(response);
            return fn.apply(this, _.toArray(arguments).slice(1));
        }
    );

    RegisterView.prototype.redirect = _.wrap(RegisterView.prototype.redirect,
        function wrapRegisterViewRedirect(fn, context, response) {
            bootstrapMyProfileFn(response);
            return fn.apply(this, _.toArray(arguments).slice(1));
        }
    );

    LoginView.prototype.refreshApplication = _.wrap(LoginView.prototype.refreshApplication,
        function wrapLoginViewRedirect( fn, response) {
            bootstrapMyProfileFn(response);
            return fn.apply(this, _.toArray(arguments).slice(1));
        }
    );

    return {
        mountToApp: function mountToApp() { }
    };
});