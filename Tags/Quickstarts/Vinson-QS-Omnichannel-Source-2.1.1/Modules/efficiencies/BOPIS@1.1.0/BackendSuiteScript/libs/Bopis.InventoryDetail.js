/**
 *@NApiVersion 2.x
 */
define([

], function BopisInventoryDetail() {
    'use strict';

    var STOCK_BADGES = {'IN_STOCK': 1, 'OUT_OF_STOCK': 2, 'LIMITED_STOCK': 3, 'AVAILABLE': 4, 'NOT_AVAILABLE': 5};

    return {
        /**
         * Returns the value of a stock badge
         * @param {String} status
         */
        _getBadgeValue: function _getBadgeValue(status) {
            return STOCK_BADGES[status];
        },

        /**
         * returns stock detail: stock value and
         * @param options
         * @param {Integer} options.quantityAvailable
         * @param {Integer} options.preferredStockLevel
         * @param {Integer} options.safetyStockLevel
         */
        getDetail: function getDetail(options) {
            var stock = 0;
            var badge = 0;
            var isStockValPreffered = false;
            var hasChecked = false;

            // return preferred stock level to display in PDP
            // e.g. More than [preferered stock level] items in stock
            if (options.preferredStockLevel) {
                if (options.quantityAvailable >= options.preferredStockLevel) {
                    hasChecked = true;
                    isStockValPreffered = true;
                    stock = options.preferredStockLevel;
                    badge = this._getBadgeValue('IN_STOCK');
                }
            }

            // if limited stock, display the actual quantity available in PDP
            // e.g. [quantity available] items in stock
            if (options.safetyStockLevel) {
                if (options.quantityAvailable < options.safetyStockLevel) {
                    hasChecked = true;
                    stock = 0;
                    badge = this._getBadgeValue('OUT_OF_STOCK');
                }
            }

            if (options.preferredStockLevel && options.safetyStockLevel) {
                if (options.quantityAvailable < options.preferredStockLevel &&
                    options.quantityAvailable >= options.safetyStockLevel) {
                    hasChecked = true;
                    stock = options.quantityAvailable;
                    badge = this._getBadgeValue('LIMITED_STOCK');
                }
            }

            if (!hasChecked && stock === 0) {
                isStockValPreffered = false;
                if (options.quantityAvailable > 0) {
                    stock = options.quantityAvailable;
                    badge = this._getBadgeValue('IN_STOCK');
                } else {
                    stock = 0;
                    badge = this._getBadgeValue('OUT_OF_STOCK');
                }
            }

            if (badge === this._getBadgeValue('OUT_OF_STOCK')) {
                if ( options.outOfStockBehavior ) {
                    switch (options.outOfStockBehavior) {
                    case 'Disallow back orders but display out-of-stock message':
                        badge = this._getBadgeValue('OUT_OF_STOCK');
                        break;
                    case 'Allow back orders but display out-of-stock message':
                    case 'Allow back orders with no out-of-stock message':
                        badge = this._getBadgeValue('AVAILABLE');
                        break;
                    case 'Remove out-of-stock items from store':
                        break;

                    default:
                        break;
                    }
                }
            }
            return {stock: stock, badge: badge, isStockValPreffered: isStockValPreffered};
        }
    };
});