function service(request,response) {
    response.setContentType('JSON');

    var custom = order.getCustomFieldValues();
    var other = order.getFieldValues(
        {
            'items': [
                'amount'
                ,	'promotionamount'
                ,	'promotiondiscount'
                ,	'orderitemid'
                ,	'quantity'
                ,	'minimumquantity'
                ,	'onlinecustomerprice_detail'
                ,	'internalid'
                ,	'rate'
                ,	'rate_formatted'
                ,	'options'
                ,	'itemtype'
                ,	'itemid'
                ,   'shipaddress'
                ,   'shipmethod'
                ,   'taxtype1'
                ,   'taxrate1'
                ,   'taxtype2'
                ,   'taxrate2'
                ,   'tax1amt'
            ]
            ,   'customer': null
            ,	'giftcertificates': null
            ,	'shipaddress': null
            ,	'billaddress': null
            ,	'payment': null
            ,	'summary': null
            ,	'promocodes': null
            ,	'shipmethod': null
            ,	'shipmethods': null
            ,	'agreetermcondition': null
            ,	'purchasenumber': null
            ,   'ismultishipto': null
            ,   'status': null
        }, false
    );

    other.custom = custom;
    other.SHIP_METHODS=order.getAvailableShippingMethods(['shipmethod', 'name']);

    response.write(JSON.stringify(other));

}