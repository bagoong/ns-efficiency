define('BOPIS.Hooks', [
    'BOPIS.Configuration',
    'Configuration'
], function BOPISHooks(
    Configuration,
    GlobalConfiguration
) {
    'use strict';


    GlobalConfiguration.publish.push({
        key: 'BOPIS_config',
        model: 'BOPIS.Configuration',
        call: 'get'
    });
    GlobalConfiguration.publish.push({
        key: 'BOPIS_Locations',
        model: 'BOPIS.Location.Model',
        call: 'getWarehousesAndStoresForSite'
    });

    // turn off multishipto on frontend
    GlobalConfiguration.isMultiShippingEnabled = false;
    
    GlobalConfiguration.useCMS = false;
    GlobalConfiguration.orderShoppingFieldKeys.items.push('shipmethod');
    GlobalConfiguration.orderShoppingFieldKeys.items.push('shipaddress');
});