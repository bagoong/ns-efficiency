{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div data-view="Global.BackToTop"></div>
<div class="footer-content">

    <!-- CMS -->
    <div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

    <section class="footer-content-nav-section">
        <div class="footer-content-copyright">
            {{#if showRange}}
                {{translate '&copy; $(0)-$(1) $(2)' firstYear currentYear companyName}}
            {{else}}
                {{#if companyName}}{{translate '&copy; $(0) $(1)' currentYear companyName}}{{/if}}
            {{/if}}
        </div>
    </section>
</div>