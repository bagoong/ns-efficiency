// @module InventoryDisplay
define('InventoryDisplay.View', [
    'Backbone',
    'Backbone.CompositeView',

    'InventoryDisplay.Status.Shipping.View',
    'InventoryDisplay.Status.Pickup.View',
    'InventoryDisplay.Status.View',

    'inventorydisplay.tpl'
], function InventoryDisplayView(
    Backbone,
    BackboneCompositeView,

    InventoryDisplayShippingView,
    InventoryDisplayPickupView,
    InventoryDisplayStatusView,

    Template
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        hasOneChildView: false,

        initialize: function initialize() {
            BackboneCompositeView.add(this);
            this.setInventoryDisplayChildViews();
        },

        setInventoryDisplayChildViews: function setInventoryDisplayChildViews() {
            var statusModel = null;

            if ( this.getCurrentItemModel().get('_isPurchasable') ) {
                if ((!this.getCurrentItemModel().get('_isshippable')) &&
                    (!this.getCurrentItemModel().get('_ispickable'))) {
                    this.hasOneChildView = true;
                    statusModel = this.getModuleConfig().STATUS_NOT_AVAILABLE;
                }else{
                    if (_.isUndefined(this.options.stockModel)) {
                        this.options.stockModel = {};
                    }

                    if (!this.getCurrentItemModel().get('_isshippable')) {
                        this.options.stockModel.shipping =
                            new Backbone.Model({
                                badge: this.getModuleConfig().STATUS_NOT_AVAILABLE,
                                internalid: this.getCurrentItemModel().get('internalid'),
                                stock: 0
                            });
                    }else if (!this.getCurrentItemModel().get('_ispickable')) {
                        this.options.stockModel.pickup =
                            new Backbone.Model({
                                badge: this.getModuleConfig().STATUS_NOT_AVAILABLE,
                                internalid: this.getCurrentItemModel().get('internalid'),
                                stock: 0
                            });
                    }
                }

                if (!this.getCurrentItemModel().get('_isInStock')) {
                    this.hasOneChildView = true;
                    statusModel = (this.model.get('_isBackorderable')) ?
                                    this.getModuleConfig().STATUS_AVAILABLE :
                                    this.getModuleConfig().STATUS_OUT_OF_STOCK;
                }
            } else {
                this.hasOneChildView = true;
                statusModel = this.getModuleConfig().STATUS_NOT_PURCHASABLE;
            }

            if ( this.hasOneChildView ) {
                this.childViews = {
                    'InventoryDisplay.Status': function InventoryDisplayStatusChildView() {
                        return new InventoryDisplayStatusView({
                            application: this.options.application,
                            stockModel: new Backbone.Model({
                                badge: statusModel,
                                internalid: this.getCurrentItemModel().get('internalid'),
                                stock: 0
                            })
                        });
                    }
                };
            } else {
                this.childViews = {
                    'InventoryDisplay.Shipping': function InventoryDisplayForShippingChildView() {
                        return new InventoryDisplayShippingView({
                            model: this.getCurrentItemModel(),
                            parentItemModel: this.model,
                            application: this.options.application,
                            stockModel: this.options.stockModel && this.options.stockModel.shipping
                        });
                    },
                    'InventoryDisplay.Pickup': function InventoryDisplayForPickupChildView() {
                        return new InventoryDisplayPickupView({
                            model: this.getCurrentItemModel(),
                            parentItemModel: this.model,
                            application: this.options.application,
                            stockModel: this.options.stockModel && this.options.stockModel.pickup,
                            itemDetailsView: this.options.itemDetailsView
                        });
                    }
                };
            }
        },

        getModuleConfig: function getModuleConfig() {
            return this.options.application.getConfig('InventoryDisplay');
        },

        getCurrentItemModel: function getCurrentItemModel() {
            var matrixChildren = this.model &&
                                 this.model.getSelectedMatrixChilds();
            return matrixChildren && matrixChildren.length === 1 ? matrixChildren[0] : this.model;
        },

        getContext: function getContext() {
            var isSelectionComplete = ( this.getCurrentItemModel().get('itemoptions_detail')
                                        && this.getCurrentItemModel().get('itemoptions_detail').matrixtype === 'child' ) ?
                                        true :
                                        this.model && this.model.isSelectionComplete();

            return {
                isSelectionComplete: isSelectionComplete,
                hasOneChildView: this.hasOneChildView
            };
        }
    });
});