define('InventoryDisplay.Model', [
    'SC.Model',
    'SuiteletProxy'
], function StoreStockModel(
    SCModel,
    SuiteletProxy
) {
    'use strict';

    return SCModel.extend({
        name: 'InventoryDisplay',

        listSummaryShipping: function listSummaryShipping(id) {
            var proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_item_inventory',
                deployId: 'customdeploy_ef_bopis_item_inventory',
                parameters: {
                    website: session.getSiteSettings(['siteid']).siteid,
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: session.getShopperSubsidiary(),
                    handler: 'getItemQuantityAvaliableDetails',
                    internalid: id,
                    locationType: 2,
                    summarize: 'T'
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 5 * 60
                }
            }).get();
            return proxyResponse;
        },

        listDetailStorePickup: function listDetailStorePickup(id, storeID) {
            var proxyResponse = new SuiteletProxy({
                scriptId: 'customscript_ef_bopis_item_inventory',
                deployId: 'customdeploy_ef_bopis_item_inventory',
                parameters: {
                    website: session.getSiteSettings(['siteid']).siteid,
                    // This should probably be 'defaultSub' if going in sc env.
                    // But again, then subsidiary should be param in url
                    subsidiary: session.getShopperSubsidiary(),
                    handler: 'getItemQuantityAvaliableDetails',
                    internalid: id,
                    locationType: 1,
                    location: storeID
                },
                requestType: 'GET',
                isAvailableWithoutLogin: true,
                cache: {
                    enabled: true,
                    ttl: 5 * 60
                }
            }).get();
            return proxyResponse;
        }
    });
});