<div class="inventory-display-status">
{{#if iconName }}
    <div class="inventory-display-status-icon">
        <span><i class="{{iconName}}"></i></span>
    </div>
{{/if}}

    <div class="inventory-display-status-text">
    {{#if hasFetchedStockData}}
        {{#if hasStockModel}}
            {{#if isDisplayAsBadge}}
                <span class="{{htmlClass}}">{{translate statusText}}</span>
            {{else}}
                <span class="{{htmlClass}}">{{translate statusText stock}}</span>
            {{/if}}

            {{#if stockLocation}}
                {{translate stockLocation}}
                {{#if allowToCheckOtherStore}}
                <a data-action="show-storelocator" class="inventory-display-button-show-storelocator" data-showin="modal">
                    {{translate 'Check other stores'}}
                </a>
                <a data-action="show-storelocator" class="inventory-display-button-show-storelocator-pusher" data-showin="pushpane" data-type="sc-pusher" data-target="pushable-map{{viewID}}">
                    {{translate 'Check other stores'}}
                </a>
                <div class="inventory-display-pushpane-map" data-action="pushable-map{{viewID}}" data-id="pushable-map{{viewID}}">
                    <div data-view="SC.PUSHER.MAP"></div>
                </div>
                {{/if}}
            {{/if}}
        {{/if}}
    {{else}}
        {{#if isForPickup}}
            {{#if hasStore}}
                {{ translate 'Loading...' }}
            {{else}}
                <a data-action="show-storelocator" class="inventory-display-button-show-storelocator" data-showin="modal">
                    {{translate 'Check availability in your local store'}}
                </a>
                <a data-action="show-storelocator" class="inventory-display-button-show-storelocator-pusher"  data-showin="pushpane" data-type="sc-pusher" data-target="pushable-map{{viewID}}">
                    {{translate 'Check availability in your local store'}}
                </a>
                <div class="inventory-display-pushpane-map" data-action="pushable-map{{viewID}}" data-id="pushable-map{{viewID}}">
                    <div data-view="SC.PUSHER.MAP"></div>
                </div>
            {{/if}}
        {{/if}}

        {{#if isForShipping}}
            {{ translate 'Loading...' }}
        {{/if}}
    {{/if}}
    </div>
</div>