define('BOPIS.OrderWizard.Module.PackageBase', [
    'BOPIS.OrderWizard.Module.Items.Line.Cell.View',
    'BOPIS.OrderWizard.Module.Items.Line.Summary.View',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'Wizard.Module',
    'underscore',
    'jQuery',
    'bopis_order_wizard_package_base.tpl',
    'Utils'
], function BOPISOrderWizardModuleBOPISPackageCreation(
    ItemCellActionablePackage,
    SummaryView,
    BackboneCompositeView,
    BackboneCollectionView,
    WizardModule,
    _,
    jQuery,
    templateTpl
) {
    'use strict';

    return WizardModule.extend({
        template: templateTpl,
        getCollection: jQuery.noop,
        getPackageSubtitle: jQuery.noop,
        getActionsView: jQuery.noop,
        currentStatus: null,
        oppositeStatus: null,
        events: {
            'click [data-action="select-unselect-all"]': 'selectUnselectAll'
        },
        initialize: function initialize() {
            WizardModule.prototype.initialize.apply(this, arguments);
            BackboneCompositeView.add(this);
            this.listenTo(this.wizard.model, 'change:deliverymethod', jQuery.proxy(this.render, this));
            this.listenTo(this.wizard.model, 'change:store', jQuery.proxy(this.render, this));
            this.listenTo(this.wizard.model, 'linedeliverychange', jQuery.proxy(this.render, this));
        },
        isActive: function isActive() {
            return this.wizard.shouldOfferBOPIS();
        },

        childViews: {
            'Items.Collection': function ItemsCollection() {
                return new BackboneCollectionView({
                    collection: this.getCollection(),
                    viewsPerRow: 1,
                    childView: ItemCellActionablePackage,
                    childViewOptions: {
                        navigable: true,
                        useLinePrice: true,
                        application: this.wizard.application,
                        wizardModel: this.wizard.model,
                        SummaryView: SummaryView,
                        ActionsView: this.getActionsView(),
                        showAlert: false,
                        generalClass: 'orderwizard-module-items-item',
                        actionsOptions: {
                            wizardModel: this.wizard.model,
                            application: this.wizard.application
                        }
                    }
                });
            }
        },

        getContext: function getContext() {
            var lines = this.getCollection();
            var selectedLinesLength = lines.length;
            var hasMoreThanOneLine = lines.length === 1;

            return {
                hasLinesToShow: selectedLinesLength,
                packageSubtitle: this.getPackageSubtitle(),
                hasMoreThanOneLine: hasMoreThanOneLine
            };
        },

        selectLine: function selectLine(e) {
            var markedLineId;
            var selectedLine;
            if (_.isTargetActionable(e)) {
                return;
            }

            markedLineId = jQuery(e.currentTarget).data('lineId');
            selectedLine = this.wizard.model.get('lines').get(markedLineId);
            selectedLine.set('deliverymethod', this.oppositeStatus);

            this.wizard.model.trigger('deliverymethodlineswap');
        },
        isValid: function isValid() {
            var linesForPicking = this.model.getLinesForPicking();
            if (linesForPicking && linesForPicking.length > 0) {
                if (!this.model.get('store') || this.model.get('store').isNew()) {
                    return jQuery.Deferred().reject(_('Please select a store').translate());
                }
            }

            return jQuery.Deferred().resolve();
        }
    });
});