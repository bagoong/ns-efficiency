define('BOPIS.OrderWizard.Module.CartItemsForPickup', [
    'BOPIS.OrderWizard.Module.CartItemsBase'
], function BOPISOrderWizardModuleCartItemsForPickup(
    CartItemsBase
) {
    'use strict';

    return CartItemsBase.extend({
        cartItemsTitle: 'Pick Up',
        accordionID: 'bopis-accordion-cart-item-pickup',
        getLineCollection: function getCollection() {
            return this.wizard.model.getLinesForPicking();
        }
    });
});