define('Profile.MyStore.EntryPoint', [
    'Profile.MyStore.Model',
    'Configuration',
    'Application'
], function ProfileMyStoreEntryPoint(
    ProfileMyStoreModel,
    Configuration,
    Application
) {
    'use strict';


    var myStoreParam = request.getParameter('myStore') || null;
    var isSSPParam = request.getParameter('sitepath') && request.getParameter('sitepath').indexOf('.ssp') !== -1;
    if (isSSPParam && myStoreParam) {
        context.setSessionObject('registerMyStore', myStoreParam);
    }

    Application.on('after:Account.register', function afterAccountRegister(Model, response) {
        var store;
        var myStore = context.getSessionObject('registerMyStore');
        if (myStore && myStore != '') {
            store = ProfileMyStoreModel.update({internalid: myStore});
            context.setSessionObject('registerMyStore', null);
            response.MyStore = store;
        }
    });

    Application.on('after:Account.registerAsGuest', function(Model, response) {
        var store;
        var myStore = context.getSessionObject('registerMyStore');
        if (myStore && myStore != '') {
            store = ProfileMyStoreModel.update({internalid: myStore});
            context.setSessionObject('registerMyStore', null);
            response.MyStore = store;
        }
    });

    Application.on('after:Account.login', function(Model, response) {
        response.MyStore = ProfileMyStoreModel.get();
    });


    if (typeof Configuration.sessionPublish === 'undefined') {
        Configuration.sessionPublish = [];
    }

    Configuration.sessionPublish.push({
        key: 'MyStore',
        model: 'Profile.MyStore.Model',
        call: 'get'
    });
});