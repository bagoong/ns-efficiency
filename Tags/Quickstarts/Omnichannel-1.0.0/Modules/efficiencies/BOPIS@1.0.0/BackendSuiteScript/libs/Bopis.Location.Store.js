define([
    '../libs/EFUtil',
    '../third_party/underscore-custom',
    'N/search'
], function BOPISLocationStore(
    EFUtil,
    _,
    searchAPI
) {
    'use strict';
    var fieldsets = {
        basic: [
            'internalid',
            'name',
            'location',
            'urlcomponent',
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'lat',
            'lon',
            'phone',
            'zipcode',
            'thumbnail',
            'marker',
            'shortDescription',
            'distance',
            'openingHours'
        ],
        details: [
            'internalid',
            'name',
            'location',
            'urlcomponent',
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'lat',
            'lon',
            'phone',
            'zipcode',
            'mainImage',
            'thumbnail',
            'marker',
            'shortDescription',
            'longDescription',
            'openingHours'
        ]
    };

    return {
        getColumns: function getColumns(options) {
            var columns = {
                internalid: {name: 'internalid'},
                name: {name: 'namenohierarchy'},
                address1: {name: 'address1'},
                address2: {name: 'address2'},
                city: {name: 'city'},
                country: {name: 'country'},
                state: {name: 'state'},
                lat: {name: 'latitude'},
                lon: {name: 'longitude'},
                zip: {name: 'zip'},
                phone: {name: 'phone'},
                urlcomponent: {name: 'custrecord_bopis_location_urlcomponent'},
                mainImage: {name: 'custrecord_bopis_location_main_image', type: 'object'},
                thumbnail: {name: 'custrecord_bopis_location_thumbnail', type: 'object'},
                marker: {name: 'custrecord_bopis_location_marker', type: 'object'},
                shortDescription: {name: 'custrecord_bopis_location_short_descript'},
                longDescription: {name: 'custrecord_bopis_location_long_descript'},
                openingHours: {name: 'custrecord_bopis_location_opening_hs'}
            };

            if (options.latitude && options.longitude) {
                _.extend(columns, {
                    distance: {
                        name: 'formulanumeric',
                        formula: this.getDistanceFormula(options),
                        sort: searchAPI.Sort.ASC
                    }
                });
            } else {
                columns.name.sort = searchAPI.Sort.ASC;
            }

            return columns;
        },
        /*
         * @param {Number} options.latitude
         * @param {Number} options.longitude
         */
        getDistanceFormula: function getDistanceFormula(options) {
            var lat = options.latitude * Math.PI / 180;
            var lon = options.longitude * Math.PI / 180;
            var PI = Math.PI;
            var R = 6371; // earth radius in Kilometers
            var formula = R +
                ' * (2 * ATAN2(SQRT((SIN((' + lat + '- ({latitude} * ' + PI + ' / 180)) / 2) *' +
                'SIN((' + lat + '- ({latitude} * ' + PI + ' / 180)) / 2) + ' +
                'COS(({latitude} * ' + PI + ' / 180)) * COS(' + lat + ') *' +
                'SIN((' + lon + '- ({longitude} * ' + PI + ' / 180)) /2) *' +
                'SIN((' + lon + '- ({longitude} * ' + PI + ' / 180)) /2))),' +
                'SQRT(1 - (SIN((' + lat + '- ({latitude} * ' + PI + ' / 180)) / 2) *' +
                'SIN((' + lat + '- ({latitude} * ' + PI + ' / 180)) / 2) +' +
                'COS(({latitude} * ' + PI + ' / 180)) * COS(' + lat + ') * ' +
                'SIN((' + lon + '- ({longitude} * ' + PI + ' / 180)) /2) * ' +
                'SIN((' + lon + '- ({longitude} * ' + PI + ' / 180)) /2)))))';

            return formula;
        },


        getFilters: function getFilters(options) {
            var filters = [
                {name: 'subsidiary', operator: 'is', values: options.subsidiary },
                {name: 'custrecord_bopis_location_webstores', operator: 'is', values: options.website},
                {name: 'locationtype', operator: 'is', values: 1},
                {name: 'makeinventoryavailable', operator: 'is', values: true},
                {name: 'makeinventoryavailablestore', operator: 'is', values: true},
                {name: 'isinactive', operator: 'is', values: false},
                {name: 'canpickup', operator: 'is', values: true}
            ];

            if (options.searchRadius) {
                filters.push({
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.LESSTHAN,
                    values: options.searchRadius,
                    formula: this.getDistanceFormula(options)
                });
            }

            if (options.urlcomponent) {
                filters.push({
                    name: 'custrecord_bopis_location_urlcomponent',
                    operator: 'is',
                    values: options.urlcomponent
                });
            }

            if (options.internalid) {
                filters.push({
                    name: 'internalid',
                    operator: 'is',
                    values: options.internalid
                });
            }

            return filters;
        },
        /**
         *
         * @param {Object} options
         * @param {Number} options.latitude
         * @param {Number} options.longitude
         * @param {Number} options.website
         * @param {Number} options.subsidiary
         * @param {Number} [options.searchRadius] (Search radius in Kilometers)
         * @param {Number} [options.limit] Maximum of stores to show
         * @param {Number} [options.internalid] Location Id
         * @param {String} [options.urlcomponent] Location Url Component
         */
        getStores: function getStores(options) {
            var filters = this.getFilters(options);
            var columns = this.getColumns(options);
            var results;
            /** @type {Search} */
            var locationSearch;

            locationSearch = searchAPI.create({
                type: searchAPI.Type.LOCATION,
                columns: EFUtil.createColumns(columns),
                filters: EFUtil.createFilters(filters)
            });

            results = EFUtil.getResults(locationSearch, {
                limit: options.limit || null,
                unique: (options.internalid || options.urlcomponent),
                mappingColumns: columns
            });

            return results;
        }
    };
});


