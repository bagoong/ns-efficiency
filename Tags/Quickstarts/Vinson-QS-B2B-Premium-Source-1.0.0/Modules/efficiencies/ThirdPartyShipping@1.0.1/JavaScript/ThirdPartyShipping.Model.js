
define('ThirdPartyShipping.Model', [
    'Backbone',
    'underscore',
    'Utils'
],
       function ThirdPartyShippingModel(
           Backbone,
           _
       ) {
           'use strict';

           return Backbone.Model.extend({
               urlRoot: _.getAbsoluteUrl('services/ThirdPartyShipping.Service.ss'),

               validation:
               {
                   thirdpartyacct:
                   {
                       required: true,
                       msg: _('3rd party shipping account number is required.').translate()
                   },

                   thirdpartycountry:
                   {
                       required: true,
                       msg: _('3rd party shipping country is required.').translate()
                   },

                   thirdpartyzipcode:
                   {
                       required: true,
                       msg: _('3rd party shipping zip code is required.').translate()
                   },

                   thirdpartycarrier:
                   {
                       required: true,
                       msg: _('3rd party shipping carrier is required.').translate()
                   }
               }
           });
       });
