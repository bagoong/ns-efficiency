
define('ThirdPartyShipping.View', [
    'thirdpartyshipping.tpl',
    'SC.Configuration',
    'Handlebars',
    'Backbone',
    'Backbone.FormView',
    'jQuery',
    'underscore'
],
       function ThirdPartyShippingView(
           template,
           Configuration,
           Handlebars,
           Backbone,
           BackboneFormView,
           jQuery,
           _
       ) {
           'use strict';

           Handlebars.registerHelper('thirdpartyshipping_isequal', function thirdPartyShippingIsEqual(a, b, options) {
               return (a && b && a.toString() === b.toString()) ? options.fn(this) : '';
           });

           return Backbone.View.extend({
               template: template,

               bindings:
               {
                   '[name="thirdpartyacct"]': 'thirdpartyacct',
                   '[name="thirdpartycountry"]': 'thirdpartycountry',
                   '[name="thirdpartyzipcode"]': 'thirdpartyzipcode',
                   '[name="thirdpartycarrier"]': 'thirdpartycarrier'
               },

               events:
               {
                   'change [data-thirdpartyshipping="thirdpartyacct"]': 'updateModel',
                   'change [data-thirdpartyshipping="thirdpartycountry"]': 'updateModel',
                   'change [data-thirdpartyshipping="thirdpartyzipcode"]': 'updateModel',
                   'change [data-thirdpartyshipping="thirdpartycarrier"]': 'updateModel'
               },

               updateModel: function updateModel(e) {
                   var target = jQuery(e.target);
                   this.model.set(target.data('thirdpartyshipping'), target.val());
               },

               initialize: function initialize(options) {
                   this.orderModel = options.model;
                   this.model = this.orderModel.get('thirdpartyshipping');
                   BackboneFormView.add(this);
               },

               getContext: function getContext() {
                   return {
                       thirdpartyacct: this.model.get('thirdpartyacct'),
                       thirdpartycountry: this.model.get('thirdpartycountry'),
                       thirdpartyzipcode: this.model.get('thirdpartyzipcode'),
                       thirdpartycarrier: this.model.get('thirdpartycarrier'),
                       thirdpartycarriers: this.model.get('thirdpartycarriers'),
                       thirdpartycountries: _.map(Configuration.get('siteSettings.countries'),
                                                  function mapCountries(country) {
                                                      return {
                                                          code: country.code,
                                                          name: country.name
                                                      };
                                                  })
                   };
               }
           });
       });
