
define('OrderWizard.Module.ThirdPartyShipping', [
    'Wizard.Module',

    'ThirdPartyShipping.Model',
    'ThirdPartyShipping.View',
    'order_wizard_thirdpartyshipping.tpl',

    'SC.Configuration',
    'Profile.Model',
    'Backbone',
    'Backbone.CompositeView',
    'Handlebars',
    'jQuery',
    'underscore'
],
       function OrderWizardModuleThirdPartyShipping(
           WizardModule,

           ThirdPartyShippingModel,
           ThirdPartyShippingView,
           template,

           Configuration,
           ProfileModel,
           Backbone,
           BackboneCompositeView,
           Handlebars,
           jQuery,
           _
       ) {
           'use strict';

           return WizardModule.extend({
               template: template,

               initialize: function initialize() {
                   WizardModule.prototype.initialize.apply(this, arguments);

                   this.profile = ProfileModel.getInstance();

                   if (!this.model.get('thirdpartyshipping')) {
                       this.model.set('thirdpartyshipping', new ThirdPartyShippingModel());
                       if (this.profile.get('isLoggedIn') !== 'T') {
                           this.profile.once('change:isLoggedIn', function changedLoggin() {
                               this.model.get('thirdpartyshipping').fetch();
                           }, this);
                       } else {
                           this.model.get('thirdpartyshipping').fetch();
                       }
                   }

                   this.fieldList = SC.getPublishedObject('ThirdPartyShipping');

                   this.bindEvents();
                   this.model.on('change:ismultishipto', this.bindEvents, this);
                   this.model.get('thirdpartyshipping').once('change:isLoaded', this.render, this);
                   this.model.get('thirdpartyshipping').on('sync', function sync() {
                       var thirdPartyShipping = this.model.get('thirdpartyshipping');
                       if (thirdPartyShipping.get('thirdpartyshipmethod')) {
                           _.each(this.fieldList, function each(field) {
                               this['_' + field] = thirdPartyShipping.get(field) || '';
                           }, this);
                           thirdPartyShipping.set('isLoaded', true);
                       }
                   }, this);

                   BackboneCompositeView.add(this);
               },

               bindEvents: function bindEvents() {
                   var renderCallback = this.render.bind(this);

                   if (this.model.get('ismultishipto')) {
                       this.model.off('change:shipmethod', renderCallback);
                       this.model.on('change:lines', renderCallback);
                   } else {
                       this.model.off('change:lines', renderCallback);
                       this.model.on('change:shipmethod', renderCallback);
                   }
               },

               isActive: function isActive() {
                   var active = false;
                   var thirdPartyShipping = this.model.get('thirdpartyshipping');

                   if (thirdPartyShipping.get('isLoaded')) {
                       // Determine if module should be active
                       active = thirdPartyShipping.get('thirdpartyshipmethod');
                       if (this.model.get('ismultishipto')) {
                           active = this.model.get('lines').some(function some(line) {
                               return active === line.get('shipmethod');
                           });
                       } else {
                           active = active === this.model.get('shipmethod');
                       }
                   }

                   // Clear sales order options if not active
                   if (!active) this.clearOrder();

                   return active;
               },

               getOrderName: function getOrderName(name) {
                   return 'custbody_ss_' + name;
               },

               clearOrder: function clearOrder() {
                   var options = this.model.get('options');
                   if (options) {
                       _.each(this.fieldList, function clearField(field) {
                           var option = this.getOrderName(field);
                           if (options[option]) options[option] = '';
                       }, this);
                       this.model.set('options', options);
                   }
               },

               saveToOrder: function saveToOrder() {
                   var thirdPartyShipping = this.model.get('thirdpartyshipping');
                   var options = this.model.get('options');
                   _.each(this.fieldList, function eachField(field) {
                       var result;
                       var val = thirdPartyShipping.get(field);
                       if (field === 'thirdpartycountry') {
                           val = Configuration.get('siteSettings.countries')[val].name;
                       }
                       if (field === 'thirdpartycarrier') {
                           result = _.find(thirdPartyShipping.get('thirdpartycarriers'),
                                           function findCarrier(carrier) {
                                               return carrier.id === thirdPartyShipping.get('thirdpartycarrier');
                                           });
                           if (result) val = result.text;
                       }
                       options[this.getOrderName(field)] = val;
                   }, this);
                   this.model.set('options', options);
               },

               submit: function submit() {
                   var validated = jQuery.Deferred();
                   var savePromise = jQuery.Deferred().done(this.saveToOrder.bind(this)).done(validated.resolve);

                   var result;
                   var data = jQuery('fieldset[data-fieldset-type="thirdpartyshipping"]').serializeObject();

                   var thirdPartyShipping = this.model.get('thirdpartyshipping');
                   var changed = _.some(this.fieldList, function someChanged(field) {
                       return thirdPartyShipping.get(field) !== this['_' + field];
                   }, this);

                   if (changed) {
                       Backbone.Validation.bind(this.childViewInstances.ThirdPartyShippingView);

                       result = thirdPartyShipping.save(data, {
                           wait: true,
                           forceUpdate: false,
                           error: validated.reject,
                           success: savePromise.resolve
                       });

                       if (!result) {
                           validated.reject({
                               errorCode: 'ERR_THIRD_PARTY_SHIPPING_FIELDS',
                               errorMessage: _('Please review third party shipping fields.').translate()
                           });
                       }
                   } else {
                       savePromise.resolve();
                   }

                   return validated;
               },

               render: function render() {
                   this._render();
                   this.renderChilds();
               },

               childViews:
               {
                   ThirdPartyShippingView: function thirdPartyShippingViewChildView() {
                       return new ThirdPartyShippingView({
                           model: this.model
                       });
                   }
               }
           });
       });
