
<div class="order-wizard-thirdpartyshipping">
    <h3 class="order-wizard-thirdpartyshipping-title">
	{{translate '3rd Party Shipping'}}
    </h3>

    <div data-view="ThirdPartyShippingView"></div>
</div>
