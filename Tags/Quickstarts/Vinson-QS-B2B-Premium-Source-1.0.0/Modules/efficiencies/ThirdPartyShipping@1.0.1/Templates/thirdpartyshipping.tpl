
<fieldset data-fieldset-type="thirdpartyshipping">
    <div data-type="alert-placeholder"></div>

    <small class="thirdpartyshipping-required">
	{{translate 'Required '}}<span class="thirdpartyshipping-required-marker">*</span>
    </small>

    <div class="thirdpartyshipping-line" data-validation="control-group">
        <label class="thirdpartyshipping-line-label" for="thirdpartyacct">{{translate 'Account Number <small class="thirdpartyshipping-required-marker">*</small>'}}</label>
        <div class="thirdpartyshipping-line-input-container" data-validation="control">
            <input class="thirdpartyshipping-line-input" type="text" id="thirdpartyacct" name="thirdpartyacct" data-thirdpartyshipping="thirdpartyacct" maxlength="300" value="{{thirdpartyacct}}"/>
        </div>
    </div>

    <div class="thirdpartyshipping-line" data-validation="control-group">
        <label class="thirdpartyshipping-line-label" for="thirdpartycountry">{{translate 'Country <small class="thirdpartyshipping-required-marker">*</small>'}}</label>
        <div class="thirdpartyshipping-line-select-container" data-validation="control">
            <select class="thirdpartyshipping-line-select" name="thirdpartycountry" data-thirdpartyshipping="thirdpartycountry">
                <option value="">{{translate '-- Select --'}}</option>
                {{#each thirdpartycountries}}
                <option value="{{code}}" {{#thirdpartyshipping_isequal code ../thirdpartycountry}}selected="selected"{{/thirdpartyshipping_isequal}}>{{translate name}}</option>
                {{/each}}
            </select>
        </div>
    </div>

    <div class="thirdpartyshipping-line" data-validation="control-group">
        <label class="thirdpartyshipping-line-label" for="thirdpartyzipcode">{{translate 'Zip Code <small class="thirdpartyshipping-required-marker">*</small>'}}</label>
        <div class="thirdpartyshipping-line-input-container" data-validation="control">
            <input class="thirdpartyshipping-line-input" type="text" name="thirdpartyzipcode" data-thirdpartyshipping="thirdpartyzipcode" maxlength="300" value="{{thirdpartyzipcode}}"/>
        </div>
    </div>

    <div class="thirdpartyshipping-line" data-validation="control-group">
        <label class="thirdpartyshipping-line-label" for="thirdpartycarrier">{{translate 'Carrier <small class="thirdpartyshipping-required-marker">*</small>'}}</label>
        <div class="thirdpartyshipping-line-select-container" data-validation="control">
            <select class="thirdpartyshipping-line-select" name="thirdpartycarrier" data-thirdpartyshipping="thirdpartycarrier">
                <option value="">{{translate '-- Select --'}}</option>
                {{#each thirdpartycarriers}}
                <option value="{{id}}" {{#thirdpartyshipping_isequal id ../thirdpartycarrier}}selected="selected"{{/thirdpartyshipping_isequal}}>{{translate text}}</option>
                {{/each}}
            </select>
        </div>
    </div>
</fieldset>
