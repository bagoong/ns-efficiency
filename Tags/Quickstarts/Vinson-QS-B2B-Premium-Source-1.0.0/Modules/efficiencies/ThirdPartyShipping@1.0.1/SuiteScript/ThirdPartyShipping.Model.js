
define('ThirdPartyShipping.Model', [
    'SC.Model',
    'Configuration',
    'RecordHelper',
    'underscore'
],
       function ThirdPartyShippingModel(
           SCModel,
           Configuration,
           RecordHelper,
           _
       ) {
           'use strict';

           if (!Configuration.publish) Configuration.publish = [];
           Configuration.publish.push({
               key: 'ThirdPartyShipping',
               model: 'ThirdPartyShipping.Model',
               call: 'getFieldList'
           });

           return SCModel.extend({
               name: 'ThirdPartyShipping',

               getFieldList: function getFieldList() {
                   return [
                       'thirdpartyacct',
                       'thirdpartycountry',
                       'thirdpartyzipcode',
                       'thirdpartycarrier'
                   ];
               },

               getFieldOptions: function getFieldOptions(recordType, fieldName) {
                   try {
                       return _.map(nlapiCreateRecord(recordType).getField(fieldName).getSelectOptions(),
                                    function map(option) {
                                        return {
                                            id: option.getId(),
                                            text: option.getText()
                                        };
                                    });
                   } catch (e) {
                       nlapiLogExecution('ERROR', 'ThirdPartyShipping.Model getFieldOptions', e);
                       return [];
                   }
               },

               get: function get() {
                   var retVal = {};
                   var result;

                   try {
                       result = new RecordHelper('customer', _.object(
                           this.getFieldList(), _.map(this.getFieldList(), function mapFieldList(field) {
                               return { fieldName: field };
                           })
                       )).search([nlapiGetUser()]).getResult();

                       _.each(this.getFieldList(), function setFieldValues(field) {
                           var val = result[field];
                           if (val) retVal[field] = val;
                       });

                       retVal.thirdpartycarriers = this.getFieldOptions('customer', 'thirdpartycarrier');
                       retVal.thirdpartyshipmethod = Configuration.get('checkoutApp.thirdPartyShipping') || '';
                   } catch (e) {
                       nlapiLogExecution('ERROR', 'ThirdPartyShipping.Model get', e);
                   }

                   return retVal;
               },

               update: function update(data) {
                   // nlapiSubmitField does not work for these fields
                   var customerRecord = nlapiLoadRecord('customer', nlapiGetUser());
                   _.each(this.getFieldList(), function updateFieldValues(field) {
                       customerRecord.setFieldValue(field, data[field]);
                   });
                   nlapiSubmitRecord(customerRecord, false, true);
               }
           });
       });
