/*
 © 2016 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// ThirdPartyShipping.ServiceController.js
// ----------------
// Service to manage third party shipping requests
define('ThirdPartyShipping.ServiceController', [
    'ServiceController',
    'ThirdPartyShipping.Model'
],
       function ThirdPartyShippingServiceController(
           ServiceController,
           ThirdPartyShippingModel
       ) {
           'use strict';

           // @class ThirdPartyShipping.ServiceController Manage third party shipping requests
           // @extend ServiceController
           return ServiceController.extend({

               // @property {String} name Mandatory for all ssp-libraries model
               name: 'ThirdPartyShipping.ServiceController',

               // @property {Service.ValidationOptions} options. All the required validation, permissions, etc.
               // The values in this object are the validation needed for the current service.
               // Can have values for all the request methods ('common' values) and specific for each one.
               options: {
                   common: {
                       requireLoggedInPPS: true
                   },
                   put: {
                       requireLogin: false
                   }
               },

               // @method get The call to ThirdPartyShipping.Service.ss with http method 'get' is managed by this function
               // @return {ThirdPartyShipping.Model.Item}
               get: function get() {
                   return ThirdPartyShippingModel.get();
               },

               // @method put The call to ThirdPartyShipping.Service.ss with http method 'put' is managed by this function
               // @return {ThirdPartyShipping.Model.Item}
               post: function post() {
                   // Pass the data to the ThirdPartyShipping's update method and send it response
                   ThirdPartyShippingModel.update(this.data);
                   return ThirdPartyShippingModel.get();
               }
           });
       });
