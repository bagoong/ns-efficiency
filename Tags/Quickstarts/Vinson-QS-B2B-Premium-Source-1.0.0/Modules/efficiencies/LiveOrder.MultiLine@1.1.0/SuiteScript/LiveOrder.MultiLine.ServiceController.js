define('LiveOrder.MultiLine.ServiceController', [
    'ServiceController',
    'Application',
    'LiveOrder.Model'
], function LiveOrderMultiLineServiceController(
    ServiceController,
    Application,
    LiveOrderModel
) {
    'use strict';
    return ServiceController.extend({
        name:'LiveOrder.MultiLine.ServiceController',
        options: {
            post: {
                checkLoggedInCheckout: true
            }
        },
        post: function post() {
            var data = JSON.parse(this.request.getBody() || '{}');
            LiveOrderModel.addMultipleItems(_.isArray(data) ? data : [data]);

            this.get();
        },
        get: function get() {
            Application.sendContent(
                LiveOrderModel.get() || {}
            );
        }
    });
});