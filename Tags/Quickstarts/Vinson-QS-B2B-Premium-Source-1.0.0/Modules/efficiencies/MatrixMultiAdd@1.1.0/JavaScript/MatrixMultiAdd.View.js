define('MatrixMultiAdd.View', [
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'MatrixMultiAdd.Item.HashMap',
    'GlobalViews.Message.View',

    'MatrixMultiAdd.Row.View',

    'MatrixMultiAdd.SubTotal.View',

    'MatrixMultiAdd.RowHead.View',

    'matrix_multi_add_view.tpl',

    'SC.Configuration',
    'LiveOrder.Model',

    'underscore',
    'jQuery',

    'Utils',
    'LiveOrder.Model.MultiLine'

], function MatrixMultiAddView(
    Backbone,
    BackboneCompositeView,
    BackboneCollectionView,
    MatrixMultiAddItemHashMap,
    GlobalViewsMessageView,
    MatrixMultiAddRowView,
    MatrixMultiAddSubTotalView,

    MatrixMultiAddRowHeadView,

    Template,

    Configuration,
    LiveOrderModel,

    _,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,
        events: {
            'click [data-action="add-to-cart"]': 'addToCart',
            'input  [data-action="add-quantity"]': 'addQuantity',
            'paste  [data-action="add-quantity"]': 'addQuantity',
            'change [data-action="add-quantity"]': 'addQuantity'
        },
        initialize: function initialize(options) {
            BackboneCompositeView.add(this);
            
            this.application = options.application;
            this.itemsForCart = new MatrixMultiAddItemHashMap(); // all items to be added to cart.
            this.validationResponse = '';

            this.colsFieldID = (this.model.get('_getColsOption')) ? this.model.get('_getColsOption').cartOptionId : '';
            this.rowsFieldID = (this.model.get('_getRowsOption')) ? this.model.get('_getRowsOption').cartOptionId : '';

            this.on('afterCompositeViewRender', jQuery.proxy(this.tableHeight, this));
            this.listenTo(this.itemsForCart, 'put', jQuery.proxy(this.render, this));
            this.listenTo(this.itemsForCart, 'remove', jQuery.proxy(this.render, this));
            this.listenTo(this.options.application.getLayout(), 'afterAppendView', jQuery.proxy(this.tableHeight, this));
        },

        tableHeight: function tableHeight() {
            // control the height of the table if its greater than the viewport height
            var header = this.$('[data-section="matrix-multi-add-header"]');
            var grid = this.$('[data-section="matrix-multi-add-table"]');
            var footer = this.$('[data-section="matrix-multi-add-footer"]');
            var moduleHeight = header.height() + grid.height() + footer.height();
            var viewportHeight = jQuery(window).height();
            if (moduleHeight > viewportHeight) {
                // value 30 is to allow some extra space
                jQuery(grid).css('height', viewportHeight - header.height() - footer.height() - 30)
                    .addClass('matrix-multi-add-table-overflow');
            }
        },

        addToCart: function addToCart() {
            var cart = LiveOrderModel.getInstance();
            var layout = this.application.getLayout();
            var self = this;

            cart.addMultipleItems(this.itemsForCart.getAll()).done(function done() {
                layout.goToCart();
            });
        },

        addQuantity: function addQuantity(e) {
            var $target = jQuery(e.target);

            var model = this.model.clone();
            var colValue = ($target.closest('tr').data('rowid')) ? $target.closest('tr').data('rowid') : 0;
            var rowValue = ($target.data('rowid')) ? $target.data('rowid') : 0;
            var quantity = parseInt($target.val(), 10);

            // validate if value of Quantity i NaN
            (isNaN(quantity)) ? quantity = 0 : quantity = quantity;

            colValue ? model.setOption(this.colsFieldID, colValue, true) : '';
            rowValue ? model.setOption(this.rowsFieldID, rowValue, true) : '';

            model.set('quantity', quantity);

            // validate stocks
            this.validate(model);


            if (quantity < 1) {
                this.itemsForCart.remove({
                    col: colValue,
                    row: rowValue
                });
            } else {
                this.itemsForCart.put({
                    col: colValue,
                    row: rowValue
                }, model);
            }
        },

        validate: function validate(model) {
            var validationResult = model.validateStocks();
            (validationResult.itemStatus)
                ? this.validationResponse = validationResult.itemStatus
                : this.validationResponse = '';
        },
        
        getOptionValues: function getOptionValues() {
            var thumbnails = this.model.get('itemimages_detail') && this.model.get('itemimages_detail').media;
            var cols = this.model.get('_getColorOptions');

            var optionValues = this.model.get('_getColsOption');
            var self = this;

            if ( optionValues ) {
                optionValues = _.map(optionValues.values, function mapcolsOptions(colsOptionValue) {
                    if ( colsOptionValue.internalid ) {
                        return {
                            internalid: colsOptionValue.internalid,
                            isAvailable: colsOptionValue.isAvailable,
                            label: colsOptionValue.label,
                            thumbnail: (thumbnails) ? thumbnails[colsOptionValue.label] : [],
                            cols: (_.findWhere(cols, {colorsname: colsOptionValue.label})) ?
                                   _.findWhere(cols, {colorsname: colsOptionValue.label}).hex : '',
                            rows: self.getrowsOptionValues(colsOptionValue.internalid)
                        };
                    }
                    return null;
                });
                // if column is not available only row
            } else {
                optionValues = {
                    rows: self.getrowsOptionValues()
                };
            }

            return optionValues;
        },

        getcolsOptionValues: function getcolsOptionValues() {
            var thumbnails = this.model.get('itemimages_detail') && this.model.get('itemimages_detail').media;
            var cols = this.model.get('_getColorOptions');

            var colsOptions = this.model.get('_getColsOption');

            if ( colsOptions ) {
                colsOptions = _.map(colsOptions.values, function mapcolsOptions(colsOptionValue) {
                    if ( colsOptionValue.internalid ) {
                        return {
                            internalid: colsOptionValue.internalid,
                            isAvailable: colsOptionValue.isAvailable,
                            label: colsOptionValue.label,
                            thumbnail: (thumbnails) ? thumbnails[colsOptionValue.label] : [],
                            cols: cols[colsOptionValue.label]
                        };
                    }
                    return null;
                });
            }

            return colsOptions;
        },

        getrowsOption: function getrowsOption() {
            var rowsOptions;
            if (this.model.get('_getRowsOption')) {
                rowsOptions = _.findWhere(this.model.get('itemoptions_detail').fields, {
                    internalid: this.model.get('_getRowsOption').cartOptionId
                });
            }

            return rowsOptions;
        },

        getcolsOption: function getcolsOption() {
            var colsOptions;
            if (this.model.get('_getColsOption')) {
                colsOptions = _.findWhere(this.model.get('itemoptions_detail').fields, {
                    internalid: this.model.get('_getColsOption').cartOptionId
                });
            }

            return colsOptions;
        },

        getrowsOptionValues: function getrowsOptionValues(colsID) {
            var rowsItems;
            var available;
            var self;
            var newModel;
            var childs;
            var childModel;
            var colsInternalId;
            var rowsOptions;

            rowsOptions = this.getrowsOption();
            newModel = [];
            rowsItems = [];
            self = this;

            if ( rowsOptions )  {
                rowsItems = _.map(rowsOptions.values, function mapRowItems(rowsData) {
                    (colsID) ? self.model.setOption(self.model.get('_getColsOption').cartOptionId, colsID, true) : '';
                    (rowsData.internalid) ?
                        self.model.setOption(self.model.get('_getRowsOption').cartOptionId, rowsData.internalid, true) : '';
                    newModel.push(self.model);

                    _.each(newModel, function eachNewModel(data) {
                        childs = data.getSelectedMatrixChilds();
                        childModel;
                        childModel = null;
                        if (childs && childs.length === 1) {
                            childModel = childs[0];
                        }
                    });

                    // use to validate if item is available to website or not
                    if (childModel) {
                        colsInternalId = colsID;
                        available = true;
                    } else {
                        available = false;
                    }


                    return {
                        colsID: colsInternalId,
                        internalid: (rowsData.internalid) ? rowsData.internalid : '',
                        label: rowsData.label,
                        isAvailable: available
                    };
                });
            }

            return rowsItems;
        },

        childViews: {

            'MatrixMultiAdd.RowHead': function childViewRowHead() {
                return new BackboneCollectionView({
                    collection: (this.getrowsOption()) ? this.getrowsOption() : [],
                    childView: MatrixMultiAddRowHeadView,
                    viewsPerRow: 1
                });
            },

            'MatrixMultiAdd.Row': function childViewRow() {
                return new BackboneCollectionView({
                    collection: new Backbone.Collection(this.getOptionValues()),
                    childView: MatrixMultiAddRowView,
                    viewsPerRow: 10,
                    childViewOptions: {
                        rowsCollection: new Backbone.Collection(this.getrowsOptionValues()),
                        colsCollection: new Backbone.Collection(this.getcolsOptionValues()),
                        parentModel: this.model,
                        itemsForCart: this.itemsForCart
                    }
                });
            },
            'MatrixMultiAdd.SubTotal': function subTotal() {
                return new MatrixMultiAddSubTotalView({
                    collection: this.model,
                    viewsPerRow: 1,
                    itemsForCart: this.itemsForCart
                });
            }
        },

        getContext: function getContext() {
            var isMatrixDimension;
            var colsOnly = false;

            // check if item is matrix
            if (this.getrowsOption()) {
                isMatrixDimension = this.getrowsOption().ismatrixdimension;
            } else {
                if (this.getcolsOption()) {
                    isMatrixDimension = this.getcolsOption().ismatrixdimension;
                    colsOnly = true;
                } else {
                    isMatrixDimension = [];
                }
            }

            return {
                errMsg: this.validationResponse,
                showGrid: this.model.get('_showGrid'),
                ismatrixdimension: isMatrixDimension,
                colsOnly: colsOnly
            };
        }
    });
});