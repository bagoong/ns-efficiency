define('MatrixMultiAdd.ItemsKeyMapping', [
    'Backbone',
    'SC.Configuration',
    'underscore'
], function MatrixMultiAddItemsKeyMapping(
    Backbone,
    Configuration,
    _
) {
    'use strict';

    Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};
    _.extend(Configuration.itemKeyMapping, {
        _showGrid: function showGridItem(item) {
            var matrixChildren = item.get('_matrixChilds') && item.get('_matrixChilds').length;
            var siteWide = Configuration.get('matrixMultiAddtoCart.siteWide');
            var perItem = item.get('custitem_ef_mma_show');

            // Only enable the feature when we have 1 or 2 matrix options
            if (item.get('_matrixOptions') && item.get('_matrixOptions').length <= 2 ) {
                if (_.isPhoneDevice() !== true) {
                    if (matrixChildren !== 0) {
                        if (siteWide || perItem) {
                            return true;
                        }
                    }
                }
                // }
                // disable this module on small mobile devices or if there are no matrix options
                return false;
            }
        },
        _matrixOptions: function _matrixOptions(item) {
            return _.filter(item.getPosibleOptions(), {isMatrixDimension: true});
        },
        _getColsOption: function _getColsOption(item) {
            var optionIds = _.pluck(
                item.get('_matrixOptions'),
                'cartOptionId'
            );

            var cols = _.pluck(Configuration.get('matrixMultiAddtoCart.fields'), 'columnFieldsID');

            var colsPossibleOptions = _.intersection(
                cols,
                optionIds
            );

            return item.getPosibleOptionByCartOptionId(colsPossibleOptions[0]);
        },
        _getRowsOption: function _getRowsOption(item) {
            var optionIds = _.pluck(
                item.get('_matrixOptions'),
                'cartOptionId'
            );

            var row = _.pluck(Configuration.get('matrixMultiAddtoCart.fields'), 'rowsFieldIds');

            var rowsPossibleOptions = _.intersection(
                row,
                optionIds
            );

            return item.getPosibleOptionByCartOptionId(rowsPossibleOptions[0]);
        },

        _getColorOptions: function _getColorOptions() {
            return Configuration.get('matrixMultiAddtoCart.color');
        }
    });
});