{{#if pricingSchedule}}
<div class="quantity-pricing">
    <h4 class="quantity-pricing-title">{{translate 'Quantity Pricing'}}</h4>
    <div class="quantity-pricing-cms-intro" data-cms-area="quantity_pricing_intro" data-cms-area-filters="page_type"></div>
    <div class="quantity-pricing-table-wrapper">
        <table class="quantity-pricing-table">
            <thead class="quantity-pricing-quantities">
                <tr>
                {{#each pricingSchedule}}
                    {{#if @last}}
                        <th class="quantity-pricing-table-cell">{{minimum}} +</th>
                    {{else}}
                        <th class="quantity-pricing-table-cell">
                            {{#if singleFigure}}
                                {{singleFigure}}
                            {{else}}
                                {{minimum}} - {{maximum}}
                            {{/if}}
                        </th>
                    {{/if}}

                {{/each}}
                </tr>
            </thead>
            <tbody class="quantity-pricing-prices">
                <tr>
                    {{#each pricingSchedule}}
                        <td class="quantity-pricing-table-cell">{{price}}</td>
                    {{/each}}
                </tr>
            </tbody>
        </table>
    </div>
</div>
{{/if}}