define([
],
    function CustomerSegmentBackendConfiguration(

    ) {
        'use strict';

        return {
            clientScriptFileId: 175697, // Client Script Internal id from file cabinet
            customerSaveSearch: [
                'customsearch_customers_save_search'
            ], // Save search for customer that will be iterated from the client script
            itemSaveSearch: [
                'customsearch_item_save_search'
            ] // Save search for Item that will be iterated from the client script
        };
    });
