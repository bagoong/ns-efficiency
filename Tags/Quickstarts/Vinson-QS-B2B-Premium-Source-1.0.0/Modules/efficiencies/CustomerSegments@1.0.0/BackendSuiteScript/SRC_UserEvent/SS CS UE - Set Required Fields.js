/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

define([], function CustomerSegment() {
    'use strict';

    var Handlers = {
        beforeSubmit: function beforeSubmit(context) {
            // Initialization
            var formData = context.newRecord;
            var customerSegments = [];
            var customerSegmentText = formData.getText('custentity_customer_customersegments');
            var customerSegmentIds = formData.getValue('custentity_customer_customersegments');

            util.each(customerSegmentIds, function utilEach(value, key) {
                customerSegments.push({ 'id': value, 'value': customerSegmentText[key].replace(/ /g, '-') });
            });

            formData.setValue('custentity_hidden_customersegment', JSON.stringify(customerSegments));
        }
    };

    return Handlers;
});
