define('CustomerSegments.ItemDetails.Router', [
    'Backbone',
    'ItemDetails.Router',
    'Profile.Model',
    'CustomerSegments.Helper',
    'AjaxRequestsKiller',
    'underscore',
    'Utils'
], function CustomerSegmentsItemDetailsRouter(
    Backbone,
    ItemDetailsRouter,
    ProfileModel,
    Helper,
    AjaxRequestsKiller,
    _
) {
    'use strict';

    _.extend(ItemDetailsRouter.prototype, {
        /* eslint-disable */
        productDetails: function (api_query, base_url, options)
        {
            // Decodes URL options
            _.each(options, function (value, name)
            {
                options[name] = decodeURIComponent(value);
            });

            var application = this.application
                ,	model = new this.Model()
                // we create a new instance of the ProductDetailed View
                ,	view = new this.View({
                    model: model
                    ,	baseUrl: base_url
                    ,	application: this.application
                });

            model.application = this.application;
            model.fetch({
                data: api_query
                ,	killerId: AjaxRequestsKiller.getKillerId()
                ,	pageGeneratorPreload: true
            }).then(
                // Success function
                function (data, result, jqXhr)
                {
                    if (!model.isNew() && Helper.isItemGroup(_.first(data.items)))
                    {
                        // once the item is fully loaded we set its options
                        model.parseQueryStringOptions(options);

                        if (!(options && options.quantity))
                        {
                            model.set('quantity', model.get('_minimumQuantity'));
                        }

                        if (api_query.id && model.get('urlcomponent') && SC.ENVIRONMENT.jsEnvironment === 'server')
                        {
                            nsglobal.statusCode = 301;
                            nsglobal.location = model.get('_url') + model.getQueryString();
                        }

                        if (data.corrections && data.corrections.length > 0)
                        {
                            if (model.get('urlcomponent') && SC.ENVIRONMENT.jsEnvironment === 'server')
                            {
                                nsglobal.statusCode = 301;
                                nsglobal.location = '/' + data.corrections[0].url + model.getQueryString();
                            }
                            else
                            {
                                return Backbone.history.navigate('#' + data.corrections[0].url + model.getQueryString(), {trigger: true});
                            }
                        }

                        // we first prepare the view
                        view.prepareViewToBeShown();

                        // then we show the content
                        view.showContent(options);
                    }
                    else if (jqXhr.status >= 500)
                    {
                        application.getLayout().internalError();
                    }
                    else if (jqXhr.responseJSON.errorCode !== 'ERR_USER_SESSION_TIMED_OUT')
                    {
                        // We just show the 404 page
                        application.getLayout().notFound();
                    }
                    else
                    {
                        application.getLayout().notFound();
                    }
                }
                // Error function
                ,	function (jqXhr)
                {
                    // this will stop the ErrorManagment module to process this error
                    // as we are taking care of it
                    try
                    {
                        jqXhr.preventDefault = true;
                    }
                    catch (e)
                    {
                        // preventDefault could be read-only!
                        console.log(e.message);
                    }

                    if (jqXhr.status >= 500)
                    {
                        application.getLayout().internalError();
                    }
                    else if (jqXhr.responseJSON.errorCode !== 'ERR_USER_SESSION_TIMED_OUT')
                    {
                        // We just show the 404 page
                        application.getLayout().notFound();
                    }
                }
            );
        }
        /* eslint-enable */
    });
});
