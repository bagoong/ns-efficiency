define('CustomerSegments.Home.View', [
    'Backbone',

    'SC.Configuration',

    'CustomerSegments.Helper',

    'Home.View',
    'customersegments_home.tpl',

    'jQuery',
    'underscore',
    'Utils'
], function CustomerSegmentsBanner(
    Backbone,

    Configuration,

    Helper,

    HomeView,
    customersegmentsHomeTpl,

    jQuery,
    _,
    Utils

) {
    'use strict';

    var carouselImages = _.map(Configuration.get('home.carouselImages', []), function mapCarouselImages(url) {
        return Utils.getAbsoluteUrl(url);
    });


    HomeView.prototype.getContext =
        _.wrap(HomeView.prototype.getContext, function getContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                carouselImages: this.model.get('carouselImages'),
                showThis: this.model.get('showThis'),
                showBanner: this.model.get('showBanner')
            });

            return context;
        });

    _.extend(HomeView.prototype, {
        template: customersegmentsHomeTpl,

        initialize: _.wrap(HomeView.prototype.initialize, function initialize(fn) {
            var self;

            fn.apply(this, _.toArray(arguments).slice(1));

            this.model = new Backbone.Model({
                showThis: false,
                showBanner: false
            });

            self = this;

            jQuery.when(Helper.setGroupsInfo()).done(function whenSetGroupsInfo(response) {
                if (response.groupBanners) {
                    self.model.set('carouselImages', response.groupBanners);
                    self.model.set('showThis', true);
                    self.model.set('showBanner', true);
                } else {
                    self.model.set('carouselImages', carouselImages);
                    self.model.set('showThis', true);
                    self.model.set('showBanner', false);
                }

                self.render();
            });
        })
    });
});
