define('CustomerSegments.Helper', [
    'SC.Configuration',
    'Profile.Model',
    'CustomerSegments.Model',
    'jQuery',
    'underscore',
    'Utils'
], function CustomerSegmentsFacetsRouter(
    Configuration,
    ProfileModel,
    CustomerSegmentsModel,
    jQuery,
    _
) {
    'use strict';

    var CustomerSegments = Configuration && Configuration.get('customerSegments');

    /*
    * Provide support for customer segment module
    **/
    var Helper = {
        loadPromiseCustomerGroup: jQuery.Deferred(),

        loadPromiseGroupInfo: jQuery.Deferred(),

        /*
        * Set Customer groups and add to Global configuration*/
        setCustomerGroups: function setCustomerGroups() {
            var profile;
            var customerGroups;
            var customerGroupIds;

            // Check if Customer Groups is already set, then return promise
            if (CustomerSegments.customerGroups && !_.isEmpty(CustomerSegments.customerGroups)) {
                return Helper.loadPromiseCustomerGroup.resolve(CustomerSegments);
            }

            // Get Customer Profile and set Customer Groups details to Global Configuration
            ProfileModel.getPromise().done(function ProfileModelGetPromise() {
                profile = ProfileModel.getInstance();
                customerGroups = profile.get('customerGroup') || '';
                customerGroupIds = profile.get('customerGroupids') || CustomerSegments.defaultGroupId;

                // Check Configuration for Guest user setting
                if (!CustomerSegments.guestUser && _.isEmpty(customerGroups)) {
                    customerGroups = CustomerSegments.defaultGroup.replace(new RegExp(' ', 'g'), '-');
                }

                // Add new configuration for customer segments
                _.extend(CustomerSegments, {
                    customerGroups: customerGroups,
                    customerGroupIds: customerGroupIds
                });

                return Helper.loadPromiseCustomerGroup.resolve(CustomerSegments);
            });

            return Helper.loadPromiseCustomerGroup;
        },

        /*
        * Set Group information and add to Global configuration
        * @return: Promise
        * */
        setGroupsInfo: function setGroupsInfo() {
            var model = new CustomerSegmentsModel();
            var groupsInfo;

            // Check if Group info is already set, then return promise
            if (CustomerSegments.groupLogo && !_.isEmpty(CustomerSegments.groupLogo)) {
                return Helper.loadPromiseGroupInfo.resolve(CustomerSegments);
            }

            // Get Customer group and fetch group data
            jQuery.when(Helper.setCustomerGroups()).done(function whenSetCustomerGroups(response) {
                if (response.customerGroups && !_.isEmpty(response.customerGroups)) {
                    model.fetch({
                        data: {
                            groupIds: response.customerGroupIds
                        },
                        success: function success(groups) {
                            groupsInfo = {
                                groupLogo: groups.get('logos'),
                                groupBanners: groups.get('banners'),
                                groupCategory: groups.get('categories')
                            };

                            _.extend(CustomerSegments, groupsInfo);

                            return Helper.loadPromiseGroupInfo.resolve(groupsInfo);
                        }
                    });
                }

                return Helper.loadPromiseGroupInfo.resolve(CustomerSegments);
            });

            return Helper.loadPromiseGroupInfo;
        },

        /*
        * Add Facets Filter to searchApiMasterOptions
        * @params: view - current view being access, not required
        * */
        addToSearchApiMasterOptions: function addToSearchApiMasterOptions(view) {
            jQuery.when(Helper.setCustomerGroups()).done(function whenSetCustomerGroups(response) {
                if (!_.isEmpty(response.customerGroups)) {
                    _.each(Configuration.searchApiMasterOptions, function eachApiOptions(value) {
                        _.extend(value, {
                            'custitem_item_customersegments': response.customerGroups,
                            'facet.exclude': !_.isEmpty(value['facet.exclude']) ?
                            value['facet.exclude'] + ', custitem_item_customersegments' :
                                'custitem_item_customersegments',
                            'include': 'facets'
                        });
                    });
                }

                if (view) {
                    view.showPage();
                }
            });
        },

        /*
        * Check if Facet is already added to configuration
        * @return boolean
        * */
        inFacets: function inFacets(id) {
            return _.has(Configuration.searchApiMasterOptions.Facets, id);
        },

        /*
        * Filter collection base on current groups
        * @param: View - current view being access
        * @param: type - relateditems_detail || correlateditems_detail, not required
        * @return: (Array) Filtered items
        * */
        filterCollection: function extendCollection(View, type) {
            _.extend(View.prototype, {
                parse: function parse(response) {
                    var originalItems = _.compact(response.items) || [];
                    var self = this;
                    var items = {};

                    if (type) {
                        _.each(_.pluck(originalItems, type), function eachOriginalItemsType(dataitems) {
                            _.each(dataitems, function eachDataitems(item) {
                                if (Helper.isItemGroup(item)) {
                                    if (!_.contains(self.itemsIds, item.internalid) && !items[item.internalid]) {
                                        items[item.internalid] = item;
                                    }
                                }
                            });
                        });
                    } else {
                        _.each(originalItems, function eachOriginalItems(item) {
                            if (Helper.isItemGroup(item)) {
                                if (!_.contains(self.itemsIds, item.internalid) && !items[item.internalid]) {
                                    items[item.internalid] = item;
                                }
                            }
                        });
                    }

                    return _.toArray(items);
                }
            });
        },

        /*
        * Filter cart items base on customer group
        * @param: View - current view being access
        * */
        filterCart: function cartFilter(View) {
            View.loadCart().done(function doneLoadCart() {
                var cart = View.getInstance();

                _.each(cart.get('lines').models, function eachCartLines(line) {
                    if (line) {
                        if (!Helper.isItemGroup(line.get('item').toJSON())) {
                            cart.removeLine(line, cart.get('options'));
                        }
                    }
                });
            });
        },

        /*
        * Check if items is available to the current group
        * @param: item - current item being checked
        * @return Boolean
        * */
        isItemGroup: function isItemGroup(item) {
            var itemGroupStr = _.pick(item, 'custitem_item_customersegments');
            var itemGroups = !_.isEmpty(itemGroupStr) ? itemGroupStr.custitem_item_customersegments.split(', ') : '';
            var profileGroups;

            if (!_.isEmpty(CustomerSegments.customerGroups)) {
                profileGroups = CustomerSegments.customerGroups.replace(new RegExp('-', 'g'), ' ').split(',');

                return !_.isEmpty(_.intersection(itemGroups, profileGroups));
            }

            return true;
        }
    };

    return Helper;
});
