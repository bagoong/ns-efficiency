define('CustomerSegments.Model', [
    'Backbone',
    'Utils'
], function CustomerSegmentModel(
    Backbone,
    Utils
) {
    'use strict';

    return Backbone.Model.extend({
        urlRoot: Utils.getAbsoluteUrl('services/CustomerSegments.Service.ss')
    });
});
