define('CustomerSegments.ServiceController', [
    'ServiceController',
    'Application',
    'CustomerSegments.Model'
], function CustomerSegmentsServiceController(
    ServiceController,
    Application,
    CustomerSegmentsModel
) {
    'use strict';

    return ServiceController.extend({
        name: 'CustomerSegments.ServiceController',

        get: function get() {
            var groupIds = this.request.getParameter('groupIds');

            this.sendContent(CustomerSegmentsModel.get(groupIds), {
                'cache': response.CACHE_DURATION_MEDIUM
            });
        }
    });
});
