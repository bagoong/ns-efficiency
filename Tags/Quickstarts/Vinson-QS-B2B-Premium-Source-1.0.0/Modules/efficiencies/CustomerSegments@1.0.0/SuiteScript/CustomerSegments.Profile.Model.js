define('CustomerSegments.Profile.Model', [
    'Application',
    'Models.Init',
    'Profile.Model',
    'CustomFieldsParser',
    'underscore'
], function CustomerSegmentsProfileModel(
    Application,
    CommerceAPI,
    Profile,
    customFieldsParser,
    _
) {
    'use strict';

    _.extend(Profile, {
        getCustomerGroup: function getCustomerGroup() {
            var customFields;
            var customerGroups = [];

            if (CommerceAPI.session.isLoggedIn2()) {
                customFields = customFieldsParser(CommerceAPI.customer.getCustomFieldValues());
                customerGroups = JSON.parse(customFields.custentity_hidden_customersegment);
            }

            return customerGroups;
        }
    });
    Application.on('after:Profile.get', function afterProfileGet(Model, responseData) {
        var customerGroups = Model.getCustomerGroup();

        responseData.customerGroup = _.pluck(customerGroups, 'value').toString();
        responseData.customerGroupids = _.pluck(customerGroups, 'id').toString();
    });
});
