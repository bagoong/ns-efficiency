{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="site-logo" class="content-banner"></div>

<a class="header-logo" href="{{headerLinkHref}}" data-touchpoint="{{headerLinkTouchPoint}}" data-hashtag="{{headerLinkHashtag}}" title="{{headerLinkTitle}}">

    {{#if logoUrl}}
        {{#if showThis}}
        <img class="header-logo-image" src="{{{logoUrl}}}" alt="{{siteName}}" style="max-height:{{minHeight}};">
        {{/if}}
    {{else}}
        <span class="header-logo-sitename">
            {{translate siteName}}
        </span>
    {{/if}}
</a>
