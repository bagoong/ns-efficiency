# Quick Order
This module allows the customer to lookup multiple products at once using the SKU and then add these items to the basket at the same time. This is mostly commonly used in combination with a physical catalogue. 

## Documentation
https://confluence.corp.netsuite.com/x/41H3Ag
