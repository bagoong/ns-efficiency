<section class="quick-order">

    <header class="quick-order-header">
        <h1 class="quick-order-title">{{translate 'Quick Order'}}</h1>
    </header>

    <div data-type="alert-placeholder"></div>

        <div class="quick-order-column-labels">
            <div class="quick-order-item-label">
                {{translate 'Item'}} <span class="quick-order-required-label">{{translate '(Required)'}}</span>
            </div>
            <div class="quick-order-qty-label">
                {{translate 'Quantity'}}
            </div>
            <div class="quick-order-remove-label">
                {{translate 'Remove'}}
            </div>
            <div class="quick-order-item-details-label">
                {{translate 'Item Details'}}
            </div>
        </div>

        <hr>

        <div data-view="QuickView.CollectionView"></div>

    <div class="quick-order-buttons row">
        <div class="col-md-6"><button class="quick-order-addline-button" data-action="addEmptyLine">{{translate 'Add Line'}} <i class="quick-order-add-line"></i></button></div>
        <div class="col-md-6"><button class="quick-order-add-to-cart-button" data-action="multiadd">{{translate 'Add to Cart'}}</button></div>
    </div>

<i class="quick-order-add-line"></i>

</section>

