define('QuickOrder.ServiceController', [
    'ServiceController',
    'Application',
    'QuickOrder.Model'
], function ItemBadgesServiceController(
    ServiceController,
    Application,
    QuickOrderModel
) {

    'use strict';

    return ServiceController.extend({
        name:'QuickOrder.ServiceController',

        get: function() {
            var keyword = this.request.getParameter('keyword');
            this.sendContent(QuickOrderModel.get(keyword, this.request), {
                'cache': response.CACHE_DURATION_MEDIUM
            });
        }
    });
});