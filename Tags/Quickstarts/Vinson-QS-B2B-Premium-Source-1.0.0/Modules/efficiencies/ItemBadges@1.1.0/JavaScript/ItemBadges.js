/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges', [
    'ItemBadges.Mixin.View',
    'ItemDetails.View',
    'Facets.ItemCell.View',
    'ItemViews.RelatedItem.View',
    'SC.Shopping.Configuration',
    'underscore',
    'SC.Configuration'
], function ItemBadges(
    MixInView,
    ItemDetailsView,
    ItemListView,
    RelatedItemView,
    ShoppingConfig,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            var moduleConfig = Configuration.get('itemBadges');

            if (Configuration.showInFacet) {
                ShoppingConfig.facets.push({
                    id: 'custitem_ef_badges',
                    priority: 20,
                    behavior: 'multi',
                    name: _(moduleConfig.facetName).translate()
                });
            }

            MixInView.extendView(ItemDetailsView, {
                find: '[data-view="ItemDetails.ImageGallery"]',
                htmlClass: 'itembadges-itemdetail'
            });

            MixInView.extendView(ItemListView, {
                find: '.facets-item-cell-grid-link-image,' +
                '.facets-item-cell-list-image, .facets-item-cell-table-link-image',
                htmlClass: 'itembadges-listitem'
            });

            MixInView.extendView(RelatedItemView, {
                find: '.item-views-related-item-thumbnail',
                htmlClass: 'itembadges-carousel'
            });
        }
    };
});