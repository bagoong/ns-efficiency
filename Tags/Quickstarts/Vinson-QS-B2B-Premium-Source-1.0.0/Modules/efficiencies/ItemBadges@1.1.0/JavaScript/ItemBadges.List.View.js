/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges.List.View', [
    'Backbone',
    'itembadges_list.tpl',
    'Utils'
], function ItemBadgesListView(Backbone, Template) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            this.model  = options.model;
        },

        getContext: function getContext() {
            return {
                model: this.model,
                bgcolor: this.model.get('bgColor'),
                name: this.model.get('name'),
                icon: this.model.get('icon'),
                weight: this.model.get('weight'),
                alt: this.model.get('alt'),
                showIcon: this.model.get('icon').internalid || false,
                showText: (this.model.get('icon').internalid) ? false : true
            };
        }
    });
});