define('Footer.Copyright', [
    'underscore'
], function FooterCopyright(
    _
) {
    'use strict';

    var Module = {
        getModuleConfig: function getModuleConfig(application) {
            var Configuration = application.getConfig();
            var defaults = {
                companyName: Configuration.siteSettings.displayname || 'Company Name',
                firstYear: new Date().getFullYear()
            };
            return _.defaults(
                (Configuration.modulesConfig && Configuration.modulesConfig.FooterCopyright) || {},
                defaults
            );
        },
        contextExecute: function contextExecute(application, context) {
            var moduleConfig = Module.getModuleConfig(application);
            var companyName = moduleConfig.companyName;
            var firstYear = parseInt(moduleConfig.firstYear, 10);
            var currentYear = new Date().getFullYear();
            _.extend(context, {
                companyName: companyName,
                firstYear: firstYear,
                currentYear: currentYear,
                showRange: firstYear < currentYear
            });
        }
    };

    return Module;
});