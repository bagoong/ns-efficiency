define('BOPIS.FAQs.Model', [
    'SC.Model',
    'SearchHelper',
    'underscore'
], function LookBookModel(
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    var CONTENT_TYPE_SECTION = '1';
    var CONTENT_TYPE_QUESTION = '2';

    return SCModel.extend({
        name: 'BOPIS.FAQs',
        record: 'customrecord_ef_bopis_faqs',
        columns: {
            internalid: {fieldName: 'internalid'},
            name: {fieldName: 'name'},
            content: {fieldName: 'custrecord_ef_bopis_faqs_content'},
            type: {fieldName: 'custrecord_ef_bopis_faqs_type'},
            parent: {fieldName: 'custrecord_ef_bopis_faqs_parent'},
            order: {fieldName: 'custrecord_ef_bopis_faqs_order'}
        },
        filters: [
            {fieldName: 'isinactive', operator: 'is', value1: 'F'}
        ],
        getHTMLFragment: function getHTMLFragment(str) {
            var startFragmentIndex = str.lastIndexOf('<!--StartFragment-->');
            var endFragmentIndex = str.lastIndexOf('<!--EndFragment-->');

            return (startFragmentIndex !== -1 && endFragmentIndex !== -1) ?
                    str.substring(startFragmentIndex+20, endFragmentIndex-1) : str;
        },
        getContents: function getContents() {
            var self = this;
            var contents = new SearchHelper()
                .setRecord(this.record)
                .setFilters(this.filters)
                .setColumns(this.columns)
                .setSort(this.columns.order.fieldName)
                .setSortOrder('asc')
                .search()
                .getResults();

            var sections = {};

            _.each(contents, function each(content) {
                content.content = self.getHTMLFragment(content.content);

                if (content.type === CONTENT_TYPE_SECTION) {
                    if (! sections[content.internalid] ) {
                        content.questionList = [];
                        content.name = ( content.name === '-' ) ? '' : content.name;
                        sections[content.internalid] = content;
                    }
                } else if (content.type === CONTENT_TYPE_QUESTION) {
                    if (! sections[content.parent] ) {
                        sections[content.parent] = _.findWhere(contents, {internalid: content.parent});
                        sections[content.parent].name = ( sections[content.parent].name === '-' ) ?
                                                        '' :
                                                        sections[content.parent].name;
                        sections[content.parent].content = self.getHTMLFragment(sections[content.parent].content);
                        sections[content.parent].questionList = [];
                    }
                    sections[content.parent].questionList.push(content)
                }
            });

            return _.toArray(sections);
        }
    });
});