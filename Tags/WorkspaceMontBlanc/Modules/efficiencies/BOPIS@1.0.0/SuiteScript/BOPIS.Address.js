define('BOPIS.Address', [
    'Address.Model',
    'BOPIS.Configuration',
    'CustomFieldsParser',
    'underscore'
], function BOPISAddress(
    AddressModel,
    Configuration,
    customEntityFieldsParserFn,
    _
) {
    'use strict';

    /* custom entity field that holds the id of the dummy address that holds store location */
    var ADDRESS_PLACEHOLDER_ENTITY_FIELD = 'custentity_ef_bopis_store_address_id';

    _.extend(AddressModel, {
        /**
         * get the addressId of the address record that is used on this session to keep the store location
         * @returns {String} addressId
         */
        getBOPISStoreAddress: function getBOPISStoreAddress() {
            var customFields;
            var addressPlaceHolderId;

            console.time('getBOPISStoreAddress');
            customFields = customEntityFieldsParserFn(customer.getCustomFieldValues());
            addressPlaceHolderId = customFields[ADDRESS_PLACEHOLDER_ENTITY_FIELD];
            console.timeEnd('getBOPISStoreAddress');

            console.info('getBOPISStoreAddress', 'Address:' + addressPlaceHolderId);
            return addressPlaceHolderId;
        },
        /**
         * Given an array of addresses, remove the ones that have is store flag
         * store flag is done by setting a value of storeAddressMark on addr3 field
         * it modifies the original array
         * @param list
         */
        filterStoreAddress: function filterStoreAddress(list) {
            var bopisStoreAddressId = this.getBOPISStoreAddress();
            var i = list.length - 1;
            while (i >= 0) {
                if ( list[i] && (
                        list[i].addr3 === Configuration.storeAddressMark ||
                        list[i].internalid === bopisStoreAddressId
                    )
                ) {
                    console.log('filterStoreAddress', list[i].internalid);
                    list.splice( i, 1 );
                }
                i--;
            }
        },
        setBOPISStoreAsShipAddress: function setBOPISStoreAsShipAddress(store) {
            var addressPlaceHolderId = this.getBOPISStoreAddress();
            var customFieldsSet = {};

            var storeAddressData = {
                fullname: store.name,
                company: store.internalid,
                addr1: store.address1,
                addr2: store.address2 || ' ',
                addr3: Configuration.storeAddressMark,
                city: store.city,
                country: store.country,
                defaultshipping: 'F',
                defaultbilling: 'F',
                phone: store.phone || '311 555-2368', // dummy phone
                state: store.state,
                zip: store.zip
            };

            if (addressPlaceHolderId) {
                // Update the already created placeholder address
                console.time('setBOPISStoreAsShipAddress-update');
                console.log('setBOPISStoreAsShipAddress-update', 'updating placeholder address' + addressPlaceHolderId);
                this.update(addressPlaceHolderId, storeAddressData);
                console.timeEnd('setBOPISStoreAsShipAddress-update');
            } else {
                // Create a placeholder address
                console.time('setBOPISStoreAsShipAddress-create');
                console.log('setBOPISStoreAsShipAddress-create', ' Creating new address');
                addressPlaceHolderId = this.create(storeAddressData);
                console.log('setBOPISStoreAsShipAddress-create', ' Creating new address result:' + addressPlaceHolderId);
                console.timeEnd('setBOPISStoreAsShipAddress-create');

                customFieldsSet[ADDRESS_PLACEHOLDER_ENTITY_FIELD] = addressPlaceHolderId;
                console.time('setBOPISStoreAsShipAddress-create-updateprofile');
                customer.updateProfile({customfields: customFieldsSet});
                console.timeEnd('setBOPISStoreAsShipAddress-create-updateprofile');
            }
            return addressPlaceHolderId;
        }
    });
});