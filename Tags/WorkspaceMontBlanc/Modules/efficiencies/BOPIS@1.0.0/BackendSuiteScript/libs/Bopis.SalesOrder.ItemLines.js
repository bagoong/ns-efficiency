/**
 *@NApiVersion 2.x
 */
define([
    '../third_party/underscore-custom',
    '../libs/EFUtil',
    'N/search'
], function BOPISSalesOrderItemLines(
    _,
    EFUtil,
    searchAPI
) {
    'use strict';

    return {
        addedFilters: [],
        addedColumns: [],

        addFilters: function(filters) {
            this.addedFilters = filters;
        },

        addColumns: function(columns) {
            this.addedColumns = columns;
        },

        getColumns: function getColumns() {
            return _.extend({
                internalid: {
                    name: 'internalid',
                    summary: searchAPI.Summary.GROUP
                }
            }, this.addedColumns);
        },

        getFilters: function getFilters() {
            return _.extend({
                isMainLineFalse: {
                    name: 'mainline',
                    operator: searchAPI.Operator.IS,
                    values: false
                },
                isTaxLineFalse: {
                    name: 'taxline',
                    operator: searchAPI.Operator.IS,
                    values: false
                },
                isShippingFalse: {
                    name: 'shipping',
                    operator: searchAPI.Operator.IS,
                    values: false
                },
                status: {
                    name: 'status',
                    operator: searchAPI.Operator.ANYOF,
                    values: [
                        'SalesOrd:B',
                        'SalesOrd:D',
                        'SalesOrd:E'
                    ]
                }
            }, this.addedFilters);
        },

        addReadyForPickupColumns: function addReadyForPickupColumns() {
            this.addColumns({
                allquantitypicked: {
                    name: 'formulanumeric',
                    formula: 'SUM(' +
                             'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantity} ELSE 0 END) ' +
                             '- ' +
                             'SUM( ' +
                             'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantitypicked} ELSE 0 END)',
                    summary: searchAPI.Summary.MAX
                }
            });
        },

        addReadyForPickupFilters: function addReadyForPickupFilters() {
            this.addFilters({
                readyForPickupEmailNotSent: {
                    name: 'custbody_ef_is_email_sent_rfp',
                    operator: searchAPI.Operator.IS,
                    values: 'F'
                },
                sumStorePickupLines: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN 1 ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                },
                readyForPickup: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' ' +
                    'THEN (CASE WHEN {quantity} = {quantitypicked} THEN 1 ELSE 0 END) ' +
                    'ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                }
            });
        },

        addOrdersPickedUpColumns: function addReadyForPickupColumns() {
            this.addColumns({
                allquantityfulfilled: {
                    name: 'formulanumeric',
                    formula: 'SUM(' +
                    'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantity} ELSE 0 END) ' +
                    '- ' +
                    'SUM( ' +
                    'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantityshiprecv} ELSE 0 END)',
                    summary: searchAPI.Summary.MAX
                }
            });
        },

        addOrdersPickedUpFilters: function addOrdersPickedUpFilters() {
            this.addFilters({
                ordersPickedUpEmailNotSent: {
                    name: 'custbody_ef_is_email_sent_opu',
                    operator: searchAPI.Operator.IS,
                    values: 'F'
                },
                sumStorePickupLines: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN 1 ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                },
                ordersPickedUp: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' ' +
                    'THEN (CASE WHEN {quantity} = {quantityshiprecv} THEN 1 ELSE 0 END) ' +
                    'ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                }
            });
        },

        addPickupRequestColumns: function addReadyForPickupColumns() {
            this.addColumns({
                allquantityfulfilled: {
                    name: 'formulanumeric',
                    formula: 'SUM(' +
                    'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantity} ELSE 0 END) ' +
                    '- ' +
                    'SUM( ' +
                    'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN {quantityrequestedtofulfill} ELSE 0 END)',
                    summary: searchAPI.Summary.MAX
                }
            });
        },

        addPickupRequestFilter: function addPickupRequestFilter() {
            this.addFilters({
                ordersPickedUpEmailNotSent: {
                    name: 'custbody_ef_is_email_sent_ofr',
                    operator: searchAPI.Operator.IS,
                    values: 'F'
                },
                sumStorePickupLines: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' THEN 1 ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                },
                requestedToFulfill: {
                    formula: 'CASE WHEN {itemfulfillmentchoice} = \'Store Pickup\' ' +
                             'THEN (CASE WHEN {quantity} = {quantityrequestedtofulfill} THEN 1 ELSE 0 END) ' +
                             'ELSE 0 END',
                    name: 'formulanumeric',
                    operator: searchAPI.Operator.NOTEQUALTO,
                    values: 0,
                    summary: searchAPI.Summary.SUM
                }
            });
        },

        getOrderIDs: function getOrderIDs() {
            return searchAPI.create({
                type: searchAPI.Type.SALES_ORDER,
                columns: EFUtil.createColumns(this.getColumns()),
                filters: EFUtil.createFilters(this.getFilters())
            });
        }
    }
});