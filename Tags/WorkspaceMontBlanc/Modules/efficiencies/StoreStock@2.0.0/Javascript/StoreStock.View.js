/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.View', [
    'Backbone',
    'Store.Collection',
    'StoreStock.Collection',

    'StoreStock.Status.View',
    'Store.Views.StoreLocator',

    'store_stock.tpl',

    'jQuery',
    'underscore'
], function StoreStockView(
    Backbone,
    StoreCollection,
    StoreStockCollection,

    StoreStockStatusView,
    StoreLocatorView,

    storeStockTpl,

    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockTpl,

        events: {
            'click [data-action="show-storelocator"]': 'showStoreLocator',
            'keyup [name="store-stock-store"]': 'keyupStoreStockSearch'
        },

        initialize: function initialize(options) {
            var matrixChildren = this.model.getSelectedMatrixChilds();
            this.itemModel = matrixChildren.length === 1 ? matrixChildren[0] : this.model;
            this.collectionStoreStock = new StoreStockCollection();
        },

        getContext: function getContext() {
            var stockStatuses = this.options.application.getConfig('storeStock.stockStatus');
            var quantityAvailable = this.itemModel.get('quantityavailable');
            var stockStatus = stockStatuses[ quantityAvailable > 0 ? 1 : 2 ];

            return {
                isSelectionComplete: this.model.isSelectionComplete(),
                quantityAvailable: quantityAvailable,
                isDisplayAsBadge:
                    quantityAvailable > 0 ?
                        this.options.application.getConfig('storeStock.isDisplayAsBadge')
                        : true,
                badgeClass: stockStatus && stockStatus.htmlClass,
                badgeText: stockStatus && stockStatus.badgeLabel,
                storeStockStoreSearchText: jQuery.cookie('storeStockStoreSearchText')
            };
        },

        keyupStoreStockSearch: function keyupStoreStockSearch(e) {
            if (e.which === 13) {
                this.showStoreLocator();
            }
        },

        showStoreLocator: function showStoreLocator() {
            var storeStockStoreSearchText = this.$('[name=store-stock-store]').val();
            var storeLocatorView = new StoreLocatorView({
                application: this.options.application,
                collection: new StoreCollection(null),
                isInStoreStock: true,
                itemId: this.itemModel.id,
                storeSearchText: storeStockStoreSearchText
            });

            this.listenTo(storeLocatorView.collection, 'sync', _.bind(this.fetchStoreStockDetail, this));

            jQuery.cookie('storeStockStoreSearchText', storeStockStoreSearchText);

            storeLocatorView.title = _('Choose Store').translate();
            storeLocatorView.modalClass = 'store-stock-modal-storelocator';

            this.options.application.getLayout().showInModal(storeLocatorView);
        },

        fetchStoreStockDetail: function fetchStoreStockDetail(storeCollection) {
            var self = this;
            var storeStockStatusViewHolder;
            var appendToElement;
            var storeLocationIDs = storeCollection.map(function mapStoreLocationIds(store) {
                return store.get('location');
            });

            this.collectionStoreStock.fetch({
                data: {
                    id: this.itemModel.id,
                    stores: _.compact(storeLocationIDs).join(',')
                }
            }).success(function success() {
                self.collectionStoreStock.each(function appendToStoreLocator(modelStoreStock) {
                    storeStockStatusViewHolder = new StoreStockStatusView({
                        model: modelStoreStock,
                        application: self.options.application
                    });

                    appendToElement = '*[data-view="StoreStock.Status.View'+modelStoreStock.get('locationId')+'"]';
                    storeStockStatusViewHolder.setElement(appendToElement).render();
                });
            });
        }
    });
});