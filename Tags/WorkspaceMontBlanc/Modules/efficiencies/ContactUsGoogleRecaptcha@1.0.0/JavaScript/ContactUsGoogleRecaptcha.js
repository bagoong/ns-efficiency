define('ContactUsGoogleRecaptcha', [
    'ContactUs.View',
    'Backbone.CompositeView',
    'Backbone.FormView',
    'ContactUsGoogleRecaptcha.View',
    'Backbone',
    'underscore',
    'ContactUsGoogleRecaptcha.validate'

], function ContactUsGoogleRecaptcha(
    ContactUsView,
    BackboneCompositeView,
    BackboneFormView,
    ContactUsGoogleRecaptchaView,
    Backbone,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp( application ) {
            ContactUsView.prototype.initialize =
            _.wrap(ContactUsView.prototype.initialize, function wrapContactUs(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                if (! ContactUsView.prototype.childViews) {
                    ContactUsView.prototype.childViews = {};
                    BackboneCompositeView.add(this);
                }
                _.extend(ContactUsView.prototype.childViews, {
                    'renderCaptcha.GoogleRecaptcha': function childViewGoogleCaptcha() {
                        return new ContactUsGoogleRecaptchaView({
                            application: this.application
                        });
                    }
                });

                this.on('afterViewRender', function afterViewRender() {
                    this.$el
                        .find('.contact-us-extras-cms')
                        .after('<div data-view="renderCaptcha.GoogleRecaptcha"></div>');
                });
            });
        }
    };
});