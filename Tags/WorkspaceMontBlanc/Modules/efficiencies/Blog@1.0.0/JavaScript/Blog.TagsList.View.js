define('Blog.TagsList.View', [
    'Blog.TagsList.Content.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',

    'Backbone',
    'blog_tagslist_view.tpl',


    'underscore',
    'Utils'

], function BlogTagsView(
    BlogTagsContentView,
    BackboneCollectionView,
    BackboneCompositeView,

    Backbone,
    BlogsTagsViewTpl,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: BlogsTagsViewTpl,

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);
            this.collection = options.collection;
        },

        getBreadcrumbPages: function getBreadcrumbPages() {
            return [
                    {text: _.translate('Blog'), href: '/blog'},
                    {text: _.translate('Tag List'), href: '/blog/tag/list'}
            ];
        },

        getSelectedMenu: function getSelectedMenu() {
            return 'tagList';
        },

        childViews: {
            'Blog.Tags.Content': function FnBlogTagContentView() {
                return new BackboneCollectionView({
                    collection: this.collection,
                    childView: BlogTagsContentView,
                    viewsPerRow: 4
                });
            }
        }
    });
});