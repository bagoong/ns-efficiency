define('Blog.Post.DetailPage.View', [
    'Blog.NavigationChildView.View',
    'Blog.Post.DetailPage.Content.View',

    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'SocialSharing.Flyout.View',

    'blog_post_detailpage_view.tpl',

    'SC.Configuration',
    'Backbone',
    'underscore',
    'Utils',
    'jQuery.scPush'

], function BlogPostDetailPageView(
    BlogNavigationChildView,
    BlogPostDetailPageContentView,

    BackboneCollectionView,
    BackboneCompositeView,
    SocialSharingFlyoutView,

    BlogPostDetailPageViewTpl,
    Configuration,
    Backbone,
    _

) {
    'use strict';

    return Backbone.View.extend({

        title: '',

        template: BlogPostDetailPageViewTpl,

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);

            this.application = options.application;
            this.collection = options.collection;
            this.blogNav = options.blogNav;
            this.model = options.model;
        },

        getBreadcrumbPages: function getBreadcrumbPages() {

            this.title = this.model.models[0].get('title');

            return [
                {text: _.translate('Blog'), href: '/blog'},
                {text: this.model.models[0].get('title'), href: '/blog/' +
                       this.model.models[0].get('title')
                 }
            ];
        },


        setMetaTagsByConfiguration: function setMetaTagsByConfiguration(self, meta_tag_configuration) {
            _.each(meta_tag_configuration, function (fn, name)
            {
                var content = fn(self);

                jQuery('<meta />', {
                    name: name,
                    content: content || ''
                }).appendTo(jQuery('head'));
            });
        },

        setMetaTags: function setMetaTags() {
            var self = this;

            var meta_tag_mapping_og = Configuration.metaTagMappingOg;
            var meta_tag_mapping_twitter_product_card = Configuration.metaTagMappingTwitterProductCard;

            this.setMetaTagsByConfiguration(self, meta_tag_mapping_og);
            this.setMetaTagsByConfiguration(self, meta_tag_mapping_twitter_product_card);
        },

        getSelectedMenu: function getSelectedMenu() {
            return 'tagDetails';
        },

        initPlugins: function initPlugins() {
            this.$el.find('[data-action="pushable"]').scPush(
                { target: 'tablet' }
            );
        },

        showContent: function showContent(options) {
            var self = this;

            this.application.getLayout().showContent(this, options && options.dontScroll)
                .done(function fnshowContent() {
                    self.initPlugins();
                });
        },

        childViews: {
            'Blog.ChildViews': function FnBlogNavigationView() {
                return new BlogNavigationChildView({
                    application: this.application,
                    tagsCollection: this.collection,
                    collection: this.collection,
                    blogNav: this.blogNav
                });
            },

            'SocialSharing.Flyout': function SocialSharingFlyout() {
                return new SocialSharingFlyoutView();
            },

            'Blog.Detail': function FnBlogItemsDetailPageContentView() {
                var layout;
                var self;

                self =this;
                layout = this.application.getLayout();

                layout.on('afterAppendView', function () {
                    self.setMetaTags();
                });

                return new BackboneCollectionView({
                    collection: this.model,
                    childView: BlogPostDetailPageContentView,
                    viewsPerRow: 1
                });
            }
        }
    });
});