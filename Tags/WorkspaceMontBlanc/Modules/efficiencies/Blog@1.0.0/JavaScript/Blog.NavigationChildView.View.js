define('Blog.NavigationChildView.View',[
    'Blog.Tags.Navigation.View',
    'Blog.Post.Navigation.View',
    'Blog.Archive.Navigation.View',

    'blog_navigation_childview.tpl',

    'Backbone.CollectionView',
    'Backbone.CompositeView',

    'Backbone',
    'Blog.Utils',
    'underscore',
    'Utils'
], function(
    BlogTagNavigationView,
    BlogPostNavigationView,
    BlogArchiveNavigationView,

    Template,

    BackboneCollectionView,
    BackboneCompositeView,

    Backbone,
    BlogUtils,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);

            this.application = options.application;
            this.tagsCollection = options.tagsCollection;
            this.collection = options.collection;
            this.blogNav = options.blogNav;
        },

        childViews: {
            'Blog.Tags.Navigation': function FnBlogNavigationView() {
                return new BackboneCollectionView({
                    collection: this.tagsCollection.slice(0, 5),
                    childView: BlogTagNavigationView,
                    viewsPerRow: 1
                });
            },
            'Blog.Post.Navigation': function FnBlogNavigationView() {
                return new BackboneCollectionView({
                    collection: this.blogNav.slice(0, 5),
                    childView: BlogPostNavigationView,
                    viewsPerRow: 1
                });
            },
            'Blog.Archive.Navigation': function FnBlogNavigationView() {
                var monthArray;
                var i;
                var tmp;
                var pass;

                i = 0;
                tmp = {};
                monthArray = {};
                pass = {};
                var post_dates = _.pluck(this.blogNav.toJSON(), 'post_date');
                post_dates = _.map(post_dates, function(post_date) {
                    if (post_date) {
                        monthArray[i] = {month: BlogUtils.convertDate(post_date), post_date: post_date};
                        i++;
                    }
                });

                return new BackboneCollectionView({
                    collection: monthArray,
                    childView: BlogArchiveNavigationView,
                    viewsPerRow: 1
                });
            }
        }
    });

});