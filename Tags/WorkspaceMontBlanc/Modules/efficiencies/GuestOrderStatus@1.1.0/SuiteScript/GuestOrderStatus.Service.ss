function service (request) {
    'use strict';
    // Application is defined in ssp library commons.js

    var GuestOrderStatusModel = require('GuestOrderStatus.Model');
    var Application = require('Application');
    var method = request.getMethod();
    var data;
    try {
        data = JSON.parse(request.getBody());
    } catch (e) {
        return Application.sendError(methodNotAllowedError);
    }

    try {
        switch (method) {
        case 'POST':
            Application.sendContent(GuestOrderStatusModel.proxy(data, request));
            break;
        default:
            return Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}