/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

/* exported service*/
// LiveOrder.Line.Service.ss
// ----------------
// Service to manage lines in the live order
function service(request) {
    'use strict';

    var Application = require('Application');
    var method = request.getMethod();
    var data = JSON.parse(request.getBody() || '{}');
    var LiveOrder = require('LiveOrder.Model');
    var _ = require('underscore');

    try {
        // If we are not in the checkout OR we are logged in
        // When on store, login in is not required
        // When on checkout, login is required
        if (!~request.getURL().indexOf('https') || session.isLoggedIn()) {
            switch (method) {
            case 'POST':
                LiveOrder.addMultipleItems(_.isArray(data) ? data : [data]);
                break;

            default:
                // methodNotAllowedError is defined in ssp library commons.js
                return Application.sendError(methodNotAllowedError);
            }

            Application.sendContent(LiveOrder.get() || {});
        } else {
            // unauthorizedError is defined in ssp library commons.js
            Application.sendError(unauthorizedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}