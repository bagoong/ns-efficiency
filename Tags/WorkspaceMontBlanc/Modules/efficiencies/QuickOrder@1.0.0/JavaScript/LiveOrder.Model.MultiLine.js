/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('LiveOrder.Model.MultiLine', [
    'LiveOrder.Model',
    'underscore',
    'Utils'
], function LiveOrderModelMultiLine(
    LiveOrder,
    _
) {
    'use strict';

    var originalModel = LiveOrder.prototype.linesCollection.prototype.model;
    var LiveOrderLineModel = originalModel.extend({
        url: _.getAbsoluteUrl('services/LiveOrder.MultiLine.Service.ss'),
        toJSON: function toJSON() {
            var ret = originalModel.prototype.toJSON.apply(this, arguments);
            ret.referenceLine = this.get('referenceLine');
            return ret;
        }
    });

    var LiveOrderLineCollection = LiveOrder.prototype.linesCollection.extend({
        url: _.getAbsoluteUrl('services/LiveOrder.MultiLine.Service.ss'),
        model: LiveOrderLineModel
    });

    _.extend(LiveOrder.prototype, {
        addMultipleItems: function addMultipleItems(items, options) {
            var linesCollection;
            var promise;

            // Prepares the input for the new collection
            var lines = _.map(items, function map(item) {
                var lineOptions = item.getItemOptionsForCart();
                return {
                    item: {
                        internalid: item.get('internalid')
                    },
                    quantity: item.get('quantity'),
                    options: _.values(lineOptions).length ? lineOptions : null,
                    referenceLine: item.get('referenceLine')
                };
            });

            // Creates the Collection
            linesCollection = new LiveOrderLineCollection(lines);

            // Saves it
            promise = linesCollection.sync('create', linesCollection, this.wrapOptionsSuccess(options));
            if (promise) {
                promise.fail(function fail() {

                });
            }

            return promise;
        }
    });
});