define('Footer.Simplified.View.Copyright', [
    'Footer.Copyright',
    'Footer.Simplified.View'
], function FooterSimplifiedViewCopyright(
    FooterCopyright,
    FooterSimplifiedView
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            FooterSimplifiedView.prototype.installPlugin('postContext', {
                name: 'footerSimplifiedContext',
                priority: 10,
                execute: function execute(context, view) {
                    FooterCopyright.contextExecute(application, context, view);
                }
            });
        }
    };
});