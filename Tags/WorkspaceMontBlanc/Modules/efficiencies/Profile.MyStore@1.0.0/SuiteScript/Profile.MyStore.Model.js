define('Profile.MyStore.Model', [
    'Store.Model',
    'SC.Model',
], function ProfileMyStoreModel(
    StoreModel,
    SCModel
) {
    'use strict';
    return SCModel.extend({
        name: 'Profile.MyStore',

        value: null,
        getServiceUrl: function getServiceUrl() {
            return nlapiResolveURL(
                'SUITELET',
                'customscript_ef_sl_preferredstore',
                'customdeploy_ef_sl_preferredstore',
                true
            );
        },
        getFromCustomerRecord: function getFromCustomerRecord(customerId) {
            var serviceUrl = this.getServiceUrl();
            var apiRequest;
            var response = null;
            var responseJSON;
            var value = null;

            console.time('MyStore-getFromCustomerRecord');
            serviceUrl += '&id=' + encodeURIComponent(customerId);

            try {
                apiRequest = nlapiRequestURL(serviceUrl, null, null, 'GET');
                response = apiRequest ? apiRequest.getBody() : response;
                responseJSON = JSON.parse(response);
                value = responseJSON.value;
            } catch (e) {
                console.error('Cannot write to customer', e);
            }
            console.time('MyStore-getFromCustomerRecord');
            return value;
        },
        /**
         * Suitelet call to write customer. Replace when elevated permissions is released
         * @param customerId
         * @param storeId
         * @returns {*}
         */
        writeToCustomerRecord: function writeToCustomerRecord(customerId, storeId) {
            var serviceUrl;
            var apiRequest;
            var response = null;
            console.time('MyStore-writeToCustomerRecord');
            serviceUrl = this.getServiceUrl();
            serviceUrl += '&id=' + encodeURIComponent(customerId);

            try {
                serviceUrl += '&storeid=' + storeId;
                apiRequest = nlapiRequestURL(serviceUrl, null, null, 'POST');
                response = apiRequest ? apiRequest.getBody() : response;
            } catch (e) {
                console.error('Error while calling write to customer', e);
            }

            console.timeEnd('MyStore-writeToCustomerRecord');

            return response;
        },

        get: function get() {
            var customerPreferredStoreId;
            var finalValue;
            var store;
            var userId = nlapiGetUser() + '';
            var sessionStoreId = context.getSessionObject('ProfileMyStoreId') || null;

            if (this.value !== null) {
                return this.value;
            }

            if (userId && userId !== '0') {
                customerPreferredStoreId = finalValue = this.getFromCustomerRecord(userId);
            }

            if (customerPreferredStoreId) {
                finalValue = customerPreferredStoreId;
            } else {
                finalValue = sessionStoreId;
            }

            if (finalValue) {
                store = StoreModel.get(finalValue);
            }

            this.value = store || {};

            return this.value;
        },

        update: function update(data) {
            var userId = nlapiGetUser();

            if (data.internalid) {
                context.setSessionObject('ProfileMyStoreId', data.internalid);

                if (userId && session.isLoggedIn2() && request.getURL().indexOf('https') === 0) { // if in myaccount or checkout
                    customer.updateProfile({
                        customfields: {
                            custentity_ef_bopis_preferred_store: data.internalid
                        }
                    });
                } else if (userId && session.isLoggedIn2()) {
                    // if in shopping
                    this.writeToCustomerRecord(userId, data.internalid);
                }
                // assign the value, keep it on property
                return (this.value = StoreModel.get(data.internalid));
            }
            return {};
        }
    });
});