<!--
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
-->
<div>
<div class="itembadges-image-row {{#if showIcon}}itembadges-show{{/if}}">
    <img class="itembadges-cell-image"
        {{#with icon}}​
        src="{{resizeImage name 'thumbnail'}}"
        data-id="{{internalid}}"
        data-name="{{name}}"
        {{/with}}
        {{#if alt.length}}​
        alt="{{alt}}"
        {{else}}
        alt="{{name}}"
        {{/if}}
        itemprop="image">
</div>
<div class="itembadges-badge-row {{#if showText}}itembadges-show{{/if}}" style="background:#{{bgcolor}}">
    <div class="itembadges-badge">
        {{name}}
    </div>
</div>
</div>