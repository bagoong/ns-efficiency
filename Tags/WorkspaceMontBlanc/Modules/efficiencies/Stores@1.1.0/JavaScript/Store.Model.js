define('Store.Model', [
    'Backbone.CachedModel',
    'Store.Utils',
    'underscore',
    'Utils'
], function StoreModel(
    CachedModel,
    Utils,
    _
) {
    'use strict';

    return CachedModel.extend({
        urlRoot: _.getAbsoluteUrl('services/Store.Service.ss'),
        getUrl: function getUrl() {
            if (this.get('urlcomponent')) {
                return '/store/' + this.get('urlcomponent');
            }
        },

        getLocation: function getLocation() {
            return {
                lat: this.get('lat'),
                lng: this.get('lon')
            };
        },

        distanceTo: function distanceTo(point) {
            var k = point.lat() + '-' + point.lng();
            var R = 6371; // means radius of earth
            var location;
            var lat1;
            var lon1;
            var lat2;
            var lon2;
            var dLat;
            var dLon;
            var a;
            var c;

            this.distanceCache = this.distanceCache || {};

            if (!this.distanceCache[k]) {
                delete this.distanceCache;
                this.distanceCache = {}; // Limit cache to last address in a quick way

                location = this.getLocation();
                lat1 = Utils.toRadians(location.lat);
                lon1 = Utils.toRadians(location.lng);
                lat2 = Utils.toRadians(point.lat());
                lon2 = Utils.toRadians(point.lng());
                dLat = lat2 - lat1;
                dLon = lon2 - lon1;

                a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
                c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                this.distanceCache[k] = R * c;
            }

            return this.distanceCache[k];
        }
    });
});
