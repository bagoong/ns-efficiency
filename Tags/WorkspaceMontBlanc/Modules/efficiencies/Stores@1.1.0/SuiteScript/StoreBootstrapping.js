define('StoreBootstrapping', [
    'Configuration'
], function StoreBootstrapping(Configuration) {
    'use strict';
    // bootstrap stores on sc.*.environment files but not on sc.user.environment.ssp
    Configuration.publish.push({
        key: 'Stores',
        model: 'Store.Model',
        call: 'listStoresForPurchases'
    });
});