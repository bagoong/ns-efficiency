define('Case.File.EntryPoint', [
    'Case.File.Model',
    'Case.FileUpload.Configuration',
    'Case.FileUpload.Hooks',
    'Case.Model.AppendMessagesToCase',
    'Case.Model.AttachMessageFile'
], function CheckoutFileUploadEntryPoint() {
    'use strict';

    return {};
});