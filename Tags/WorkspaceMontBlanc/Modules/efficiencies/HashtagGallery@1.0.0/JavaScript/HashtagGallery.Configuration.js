define('HashtagGallery.Configuration', function HashtagGalleryConfiguration() {
    'use strict';
    return {
        displayUser: false,
        displayCaption: false,
        minimumToDisplayModule: 0 // 0 shows even if no photos
    };
});