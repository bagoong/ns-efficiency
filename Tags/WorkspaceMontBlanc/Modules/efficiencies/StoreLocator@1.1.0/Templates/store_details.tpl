<div id="stores-store-locator-details" class="container stores-store-locator-details-container">
    <div class="panel panel-default">
        <div class="panel-body" itemscope itemtype="http://schema.org/{{configuration.SchemaOrg}}">
            <div class="col-md-4 col-sm-6">
                <div class="col-xl-12 col print-hide">
                    <h3 itemprop="name" class="panel-title">{{ translate model.name }}</h3>
                </div>

                <div class="col-xl-12 col print-hide">
                    <h3 class="panel-title">{{ translate "STORE ADDRESS" }}</h3>
                    <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">
                            {{ translate model.address1}}
                            {{ translate model.address2 }}
                        </span>
                        <span itemprop="addressLocality">
                            {{ translate model.city}}
                        </span>,
                        <span itemprop="addressRegion">
                            {{ translate model.state }}
                        </span>,
                        <span itemprop="postalCode">{{ translate model.zipcode}}</span>
                        <span itemprop="addressCountry">{{ translate model.country}}</span>
                    </address>
                </div>

                <div class="col-xl-12 print-hide">
                    {{#if model.phone}}
                        <div class="col">
                            <h3 class="panel-title">{{ translate "PHONE" }}</h3>
                            <span itemprop="telephone" content="{{ model.phone }}">{{ model.phone }}</span>
                        </div>
                    {{/if}}
                    {{#if model.openingHours}}
                        <div class="col">
                            <h3 class="panel-title">{{ translate "OPENING TIMES" }}</h3>
                            {{{ translate model.openingHours }}}
                        </div>
                    {{/if}}
                </div>

                <div class="col-xl-12 col" data-view="StoreLocator.DirectionForm"></div>
                    <div class="col-xl-12 col print-hide">
                        <a href="stores" class="store-locator-back-button">{{translate 'Back to store locator'}}</a>
                        <button id="printDirection" class="storelocator-print-direction-button">{{translate 'Print directions'}}</button>
                    </div>
                </div>

                <div id="map" class="col-md-8 col-sm-12 map-container" data-view="StoreLocator.Map"></div>
                <div id="directions" class="col-md-8 col-sm-12 col-md-offset-4 col-xs-offset-0 store-detail-directions"></div>

            </div>
        </div>
    </div>
</div>