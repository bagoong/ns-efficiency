Application.defineModel('Website', {
    get: function(websiteId){

        var CurrencyModel = Application.getModel('Currency');


        var context = nlapiGetContext();
        var record = nlapiLoadRecord('website', websiteId);
        var website = {
            onlinepricelevel: record.getFieldValue('onlinepricelevel'),
            languages: [],
            currencies: [],
            imagedomain: record.getFieldValue('imagedomain'),
            //imageurlbase:'http://' + record.getFieldValue('imagedomain') + this.getFullImagePath(record),
            displayname: record.getFieldValue('displayname'),
            createcustomersascompanies: record.getFieldValue('createcustomersascompanies')
        };

        var companyConfiguration = null;

        //LANGUAGE HANDLING

        var multiLanguageEnabled = context.getSetting('FEATURE', 'MULTILANGUAGE') === 'T';
        if ( multiLanguageEnabled ){
            var languageCount = record.getLineItemCount('sitelanguage');
            for (var i = 1; i <= languageCount; i++)
            {
                var line = {
                    internalid: record.getLineItemValue('sitelanguage','internalid', i),
                    locale: record.getLineItemValue('sitelanguage','locale',i),
                    isonline: record.getLineItemValue('sitelanguage', 'isonline',i) == 'T',
                    isdefault: record.getLineItemValue('sitelanguage', 'isdefault',i) == 'T'


                };
                if(line.isonline)
                {
                    website.languages.push(line);
                }
            }
        }
        else
        {
            companyConfiguration = companyConfiguration || nlapiLoadConfiguration('companyinformation');
            var locale = companyConfiguration.getFieldValue('locale');

            website.languages.push({
                internalid: 1,
                isdefault: true,
                isonline: true,
                locale: locale
            });
        }
        // END OF LANGUAGE HANDLING

        // CURRENCY HANDLING
        var multiCurrencyEnabled = context.getSetting('FEATURE', 'MULTICURRENCY') === 'T';
        if (multiCurrencyEnabled)
        {

            var currencyCount = record.getLineItemCount('sitecurrency');
            for(var j = 1; j <= currencyCount; j++)
            {
                var line = {
                    internalid: record.getLineItemValue('sitecurrency','currency',j),
                    isdefault: record.getLineItemValue('sitecurrency', 'isdefault',j) == 'T',
                    isonline: record.getLineItemValue('sitecurrency', 'isonline',j) == 'T'
                };
                if(line.isonline)
                {
                    website.currencies.push(line);
                }
            }

            var defaultCurrencyId = _.findWhere(website.currencies, {isdefault: true}).internalid;

            website.currencies = CurrencyModel.list(_.pluck(website.currencies,'internalid'));
            _.each(website.currencies, function(val){
                 if(val.internalid == defaultCurrencyId)
                 {
                     val.isdefault = true;
                 } else {
                     val.isdefault = false;
                 }
            });
        }
        else
        {
            companyConfiguration = companyConfiguration || nlapiLoadConfiguration('companyinformation');
            var displaySymbol = companyConfiguration.getFieldValue('displaysymbol');
            website.currencies.push({
                internalid: 1,
                isdefault: true,
                isonline: true,
                symbol: displaySymbol,
                displaysymbol: displaySymbol//,
                //locale_metadata: SC.Configuration.Efficiencies.BackInStockNotification.localecurrency_metadata[0]
            });
        }
        // END OF CURRENCY HANDLING


        website.defaultLanguage = _.findWhere(website.languages, {isdefault: true});
        website.defaultCurrency = _.findWhere(website.currencies, {isdefault: true});
        website.domain = this.getPrimaryDomain(record);

        return website;
    },
    getPrimaryDomain: function(record){
        //For every link on our emails, we need a DOMAIN.
        //If we have a primary domain, use that. If not, use the first of the website record.
        var countDomains = record.getLineItemCount('shoppingdomain');
        var domain;
        for(var i= 1; i<=countDomains; i++){
            if(record.getLineItemValue('shoppingdomain','isprimary',i)=='T')
            {
                domain = {
                    fulldomain: 'http://'+ record.getLineItemValue('shoppingdomain','domain',i) + '/',
                    domain: record.getLineItemValue('shoppingdomain','domain',i),
                    hostingroot: record.getLineItemValue('shoppingdomain','hostingroot',i),
                    isprimary: record.getLineItemValue('shoppingdomain','isprimary',i)==='T'
                }
            }
        }

        //IF we don't have a primary domain, we should be reading the first one of the domains configured
        if(!domain)
        {
            domain = {
                fulldomain: 'http://'+ record.getLineItemValue('shoppingdomain','domain',1) + '/',
                domain: record.getLineItemValue('shoppingdomain','domain',1),
                hostingroot: record.getLineItemValue('shoppingdomain','hostingroot',1),
                isprimary: record.getLineItemValue('shoppingdomain','isprimary',1)==='T'
            }
        }
        return domain;
    },
    getFullImagePath: function(record){
        //This algorithm that looks for the path to the image folder, will only work if the image folder is under "Web Hosting Files"
        //If not, i'm not sure what would happen
        var imageFolderId = record.getFieldValue('imagefolder');
        var pathToImages = '/';
        while( imageFolderId.indexOf('-')===-1 )
        {
            var j = nlapiLookupField('folder', imageFolderId,['internalid','name','parent','externalid','group','class']);

            imageFolderId = j.parent.toString();
            if(j.parent.indexOf('-')===-1 ) { //grandpa shouldn't be "Web Hosting Files"
                pathToImages = '/' + j.name + pathToImages;
            }
        }
        return pathToImages;
    }
});

Application.defineModel('Currency', {
    record: 'currency',
    fields: {
        internalid: {fieldName: 'internalid'},
        name: { fieldName: 'symbol'}
    },
    list: function(ids){
        //Had to be done by LOAD RECORD because of displaysymbol access :( (Not available on searches)
        var search = new SearchHelper(this.record,[{fieldName:'internalid',operator:'anyof',value1: ids }],this.fields);
        var results = search.search(ids).getResults();


        return results;
    }
});