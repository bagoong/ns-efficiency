define('Store.View.StoreList', [
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'Store.View.StoreList.store',
    'storelist.tpl',
    'underscore'
], function storeListView(Backbone, CompositeView, CollectionView, StoreListStoreView, Template, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            this.application = options.application;
            this.collection = options.collection;

            CompositeView.add(this);
        },

        title: _('Store List').translate(),

        getBreadcrumbPages: function getBreadcrumbPages() {
            if (this.application.history.length < 2) {
                this.application.history.push({text: this.title, href: '/storelist'});
            }

            return this.application.history;
        },

        getContext: function getContext() {
            return {
                hasStore: !!this.collection.length
            };
        },

        childViews: {
            'StoreList.Store': function StoreListStore() {
                return new CollectionView({
                    collection: this.collection,
                    childView: StoreListStoreView
                });
            }
        }
    });
});