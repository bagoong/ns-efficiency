/*
     Store.Views.StoreLocator.List - Create/setup List of store region
     Required module [
        Backbone,
        Backbone.CompositeView,
        Backbone.CollectionView,
        Store.Views.StoreLocator.List.Store,
        storelocator_list.tpl,
        jQuery,
        underscore]
*/
define('Store.Views.StoreLocator.List', [
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'Store.Views.StoreLocator.List.Store',
    'storelocator_list.tpl',
    'jQuery',
    'underscore'
], function StoreViewsStoreLocatorList(Backbone, CompositeView, CollectionView, storeListView, Template, jQuery, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        events: {
            'click h4[data-type=stores-list-store]': 'selectStore'
        },

        initialize: function initialize(options) {
            this.application = options.application;
            this.configuration = options.configuration;
            this.collection = options.collection;
            this.eventBus = options.eventBus;


            this.listenTo(this.eventBus, 'zoomToStoreLocation', _.bind(this.storeSelected, this));
            this.listenTo(this.collection, 'change', _.bind(this.originChanged, this));

            CompositeView.add(this);
        },

        getContext: function getContext() {
            return {
                hasNearest: (this.collection.length !== 0) ? true : false,
                nearestLength: this.collection.length
            };
        },

        childViews: {
            'StoreLocator.List.Nearest': function StoreLocatorListNearest() {
                return new CollectionView({
                    collection: this.collection,
                    childView: storeListView
                });
            }
        },

        originChanged: function originChanged() {
            this.render();
        },

        storeSelected: function storeSelected(data) {
            /* cleanup first */
            var $element;
            this.$('li[data-type=stores-list-store]').removeClass('storelocator-highlighted');

            /* then work if store selected */
            if (!data.store) {
                return;
            }

            $element = data.$element || this.$('li[data-id=' + data.store.id + ']');
            $element.addClass('storelocator-highlighted');
        },

        selectStore: function selectStore(e) {
            var $element = jQuery(e.currentTarget);
            var selectedId = $element.data('id');
            var store = this.collection.get(selectedId);

            this.options.eventBus.trigger('zoomToStoreLocation', {
                store: store,
                component: 'list'
            });
        }
    });
});