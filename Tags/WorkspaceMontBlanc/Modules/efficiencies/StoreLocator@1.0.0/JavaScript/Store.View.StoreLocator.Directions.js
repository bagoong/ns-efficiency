define('Store.View.StoreLocator.Directions', [
    'Backbone',
    'GoogleMapsLoader',
    'Store.Views.StoreLocator.InfoWindow',
    'storelocator_directions.tpl',
    'jQuery',
    'underscore'
], function StoreViewStoreLocatorDirections(Backbone, GoogleMapsLoader, InfoWindowView, Template, jQuery, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            this.configuration = options.configuration;
            this.model = options.model;

            this.listenTo(this.options.eventBus,
                'direction',
                _.bind(this.direction, this));
        },

        render: function render() {
            this._render();
            GoogleMapsLoader.loadScript(this.options.configuration.googleMapsApiKey)
                .done(_.bind(this.mapSetup, this));

            return this;
        },

        mapSetup: function mapSetup() {
            this.destination = new google.maps.LatLng(this.model.get('lat'), this.model.get('lon'));
            this._infoWindow = new google.maps.InfoWindow;

            this.createMap();
            this.createMarker();
            this.getInfoWindow();
            this.setEvents();
        },

        setEvents: function setEvents() {
            var self = this;

            this._infoWindow.open(this.map, this.marker);

            google.maps.event.addListenerOnce(this.map, 'idle', function click() {
                google.maps.event.trigger(self.map, 'resize');
                self.map.setCenter(self.destination);
            });
        },

        createMap: function createMap() {
            var config = this.options.configuration;
            this.zoom = config.map.defaultZoom;

            this.map = new google.maps.Map(this.$('#storelocator-map-placeholder')[0], {
                zoom: config.map.defaultZoom,
                animation: google.maps.Animation[config.map.animation],
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: this.destination,
                mapTypeControl: config.map.mapTypeControl,
                panControl: config.map.panControl,
                zoomControl: config.map.zoomControl,
                scaleControl: config.map.scaleControl,
                streetViewControl: config.map.streetViewControl,
                styles: config.mapStyles
            }, config.mapStyles);
        },

        createMarker: function createMarker() {
            var imageData = this.configuration.storeMarker;
            var image;
            var markerOptions;

            if (imageData.icon) {
                image = {
                    url: imageData.icon,
                    origin: new google.maps.Point(imageData.origin.x, imageData.origin.y),
                    size: new google.maps.Size(imageData.size.x, imageData.size.y),
                    anchor: new google.maps.Point(imageData.anchor.x, imageData.anchor.y)
                };
            }

            markerOptions = {
                position: this.destination,
                title: this.configuration.originMarker.text,
                draggable: false,
                animation: google.maps.Animation[this.configuration.storeMarker.animation],
            };

            if (image) {
                markerOptions.icon = image;
            }

            this.marker = new google.maps.Marker(markerOptions);
            this.marker.setMap(this.map);
        },

        getInfoWindow: function getInfoWindow() {
            var infoWindow;

            infoWindow = new InfoWindowView({
                model: this.model,
                origin: this._originMarker && this._originMarker.position
            });

            this._infoWindow.setContent(infoWindow.render().$el.html());

            return this._infoWindow;
        },

        direction: function direction(data) {
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var directionsService = new google.maps.DirectionsService;
            var config = this.options.configuration;
            var self = this;
            var errorMsg = _(self.configuration.errorDirection.text).translate();

            var map = new google.maps.Map(this.$('#storelocator-map-placeholder')[0], {
                zoom: config.map.defaultZoom,
                center: this.destination
            });

            document.getElementById('directions').innerHTML = '';
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('directions'));

            directionsService.route({
                origin: data.location,
                destination: this.destination,
                travelMode: google.maps.TravelMode[data.mode]
            }, function directionsServiceResponse(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    jQuery('#printDirection').show();
                    directionsDisplay.setDirections(response);
                } else {
                    jQuery('#printDirection').hide();
                    self.map = map;
                    self.createMarker();
                    self._infoWindow.open(self.map, self.marker);
                    document.getElementById('directions').innerHTML = errorMsg;
                }
            });
        }
    });
});