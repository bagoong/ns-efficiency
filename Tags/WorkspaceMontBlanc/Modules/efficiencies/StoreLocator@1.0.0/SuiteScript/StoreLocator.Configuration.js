define('StoreLocator.Configuration', [
    'Configuration',
    'underscore'
], function StoreLocatorConfiguration(
    Configuration,
    _
) {
    'use strict';
    var storeLocatorConfiguration = {
        storePage: true,
        googleMapsApiKey: ''
    };

    _.extend(storeLocatorConfiguration, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'StoreLocator_config',
        model: 'StoreLocator.Configuration',
        call: 'get'
    });

    return storeLocatorConfiguration;
});


