define('BlogCategories', [
    'Facets.Browse.View',
    'Blog.Categories.View',

    'Blog.CategoriesPost.Collection',
    'Blog.CategoriesTags.Collection',
    'underscore'
], function BlogCategoriesRelatedView(
    FacetBrowseView,
    BlogCategoriesView,

    BlogPostCollection,
    BlogTagsCollection,
    _
) {
    'use strict';

    var _me;
    var tagscollection;
    var filterTags;

    FacetBrowseView.prototype.initialize =
    _.wrap(FacetBrowseView.prototype.initialize, function wrapProductFormInitialize(fn) {
        _me = this;
        this.items = new BlogPostCollection();
        tagscollection = new BlogTagsCollection();
        fn.apply(this, _.toArray(arguments).slice(1));

        _.extend(FacetBrowseView.prototype.childViews, {
            'Blog.Post': function BlogItems() {
                tagscollection.fetch().done( function results(data) {
                    filterTags = _.findWhere(data, {name: (_me.category) ? _me.category.name : ''});
                    if (filterTags) {
                        _me.items.fetch({
                            data: {
                                tagId: filterTags.recordid
                            }
                        });
                    }
                });

                return new BlogCategoriesView({
                    collection: this.items,
                    application: this.application
                });
            }
        });

        this.on('afterViewRender', function afterViewRender() {
            this.$el
                .find('.facets-facet-browse-results')
                .append('<div data-view="Blog.Post"></div>');
        });
    });
});