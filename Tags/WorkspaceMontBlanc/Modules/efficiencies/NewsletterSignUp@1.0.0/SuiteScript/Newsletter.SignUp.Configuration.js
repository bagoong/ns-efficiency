/**
 * Created by pzignani on 01/10/2014.
 */
define('Newsletter.SignUp.Configuration', [
    'Configuration',
    'underscore'
], function NewsletterSignUpConfiguration(
    Configuration,
    _
) {
    'use strict';

    var newsletterSignUpConfiguration = {
        domain: 'forms.netsuite.com',
        formId: '17',
        hash: '89eeaa726d9f8bc2ae4b'
    };

    _.extend(newsletterSignUpConfiguration, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'NewsletterSignUpConfig',
        model: 'Newsletter.SignUp.Configuration',
        call: 'get'
    });

    return newsletterSignUpConfiguration;
});
