define('MatrixMultiAdd', [
    'MatrixMultiAdd.View',
    'MatrixMultiAdd.Item.Model',
    'ItemDetails.View',
    'PluginContainer',
    'SC.Configuration',
    'jQuery',
    'underscore',
    'MatrixMultiAdd.ItemsKeyMapping'
], function MatrixMultiAdd(
    MatrixMultiAddView,
    MatrixMultiAddItemModel,
    ItemDetailsView,
    PluginContainer,
    Configuration,
    jQuery,
    _
) {
    'use strict';

    _.extend(ItemDetailsView.prototype.childViews, {
        'Item.MatrixMultiAdd': function childViewMatrixMultiAdd() {
            return new MatrixMultiAddView({
                model: this.model,
                application: this.application
            });
        }
    });

    return {
        mountToApp: function mountToApp() {
            ItemDetailsView.prototype.initialize = _.wrap(ItemDetailsView.prototype.initialize, function initialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));
                this.on('afterViewRender', function afterViewRender() {
                    if (this.model.get('_showGrid') === true) {
                        // var section = this.$('[data-id="item-details-options"]').parent('section').parents('section');
                        // place the multi add grid under the main content and hide the standard matrix options
                        this.$el.find('.item-details-main-content').after('<div data-view="Item.MatrixMultiAdd"/>');
                        this.$el.find('.item-details-options, .item-details-actions').remove();
                    }
                });
            });
        }
    };
});