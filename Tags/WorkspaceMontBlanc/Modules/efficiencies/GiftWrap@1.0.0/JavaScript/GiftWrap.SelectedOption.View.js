define('GiftWrap.SelectedOption.View', [
    'ItemViews.SelectedOption.View',
    'GiftWrap.Configuration',
    'underscore'
], function GiftWrapOptionView(
    ItemViewsSelectedOptionView,
    Configuration,
    _
) {
    'use strict';

    ItemViewsSelectedOptionView.prototype.installPlugin('postContext', {
        name: 'giftWrapSelectedContext',
        priority: 10,
        execute: function execute(context, view) {
            var option = view.model;
            var line = view.options.cartLine;
            var giftWrapItem;
            if (option.get('cartOptionId') === Configuration.GiftWrapConfig.cartOptions.giftWrap) {
                giftWrapItem = line.get('giftWrap');
                if (giftWrapItem) {
                    _.extend(context, {
                        giftWrapLine: {
                            total_formatted: giftWrapItem.total_formatted,
                            isValid: (giftWrapItem.amount !== 0) ? true : false
                        }
                    });
                }
            }
        }
    });
});