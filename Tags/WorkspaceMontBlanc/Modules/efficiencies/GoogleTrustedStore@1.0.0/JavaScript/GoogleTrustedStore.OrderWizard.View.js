/*
    © 2015 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('GoogleTrustedStore.OrderWizard.View', [
    'Wizard.Module',
    'Profile.Model',
    'GoogleTrustedStore.LoadScript',
    'GoogleTrustedStore.OrderWizard.tpl',
    'underscore',
    'Utils'
], function GoogleTrustedStoreOrderWizardView(WizardModule, ProfileModel, GTSScript, Template, _) {
    'use strict';

    var Configuration = SC.ENVIRONMENT &&
                                SC.ENVIRONMENT.published &&
                                SC.ENVIRONMENT.published.GoogleTrustedStore_config;

    return WizardModule.extend({
        template: Template,

        className: 'OrderWizard.Module.Confirmation',

        render: function render() {
            this.confirmation = this.model.get('confirmation');
            this.items = this.confirmation.get('itemLines');
            this.profile = this.wizard.profileModel;
            this.currency = SC.ENVIRONMENT.currencyCodeSpecifiedOnUrl;

            this.isDigital = 'N';
            this.isBackorder = 'N';

            this._render();
        },

        formatDate: function formatDate(date) {
            var d = new Date(date);
            var yyyy = d.getFullYear().toString();
            var mm = (d.getMonth() + 1).toString();
            var dd  = d.getDate().toString();

            return yyyy + '-' + (mm[1] ? mm : '0' + mm[0]) + '-' + (dd[1] ? dd : '0' + dd[0]);
        },

        setItemDetails: function setItemDetails() {
            var items = [];
            var self = this;

            _.each(this.items, function eachItems(model) {
                var item = model.item;

                items.push({
                    displayname: item.displayname,
                    onlinecustomerprice: item.onlinecustomerprice,
                    quantity: model.quantity,
                    internalId: item.internalid,
                    shoppingId: (Configuration.gts.google_base_subaccount_id) ?
                        Configuration.gts.google_base_subaccount_id : false
                });

                if (item.custitem_ef_is_digital_goods) {
                    self.isDigital = 'Y';
                }

                if (item.isbackorderable) {
                    self.isBackorder = 'Y';
                }
            });

            this.items = items;
        },

        getContext: function getContext() {
            var self = this;
            var currentDate = new Date();
            var shipDate = new Date().setDate(currentDate.getDate() + Configuration.estShipDate);
            var deliveryDate = new Date().setDate(
                currentDate.getDate() + (Configuration.estShipDate + Configuration.estDeliveryDate));

            this.summary = this.confirmation.get('summary');
            this.setItemDetails();

            return {
                orderId: this.confirmation.get('confirmationnumber'),
                customerEmail: this.profile.get('email'),
                customerCountry: this.profile.get('addresses').models[0].get('country'),
                currency: this.currency,
                orderTotal: this.summary.total,
                orderDiscount: this.summary.discounttotal,
                orderShipping: this.summary.shippingcost,
                orderTax: this.summary.taxtotal,
                estShipDate: self.formatDate(shipDate),
                estDeliveryDate: self.formatDate(deliveryDate),
                isPreorder: this.isBackorder,
                isDigitalGood: this.isDigital,
                items: this.items
            };
        }
    });
});