define('GoogleTrustedStore.LiveOrder.Model', [
    'Application',
    'Utils',
    'LiveOrder.Model',
    'StoreItem.Model',
    'underscore'
], function GoogleTrustedStoreLiveOrder(Application, Utils, LiveOrder, StoreItem, _) {
    'use strict';

    /* EVENTS: */
    Application.on('before:LiveOrder.confirmationCreateResult', function beforeLiveOrderConfirmationCreateResult(Model, placedOrder) {
        var placeOrder = placedOrder;
        Application.on('after:LiveOrder.confirmationCreateResult',
            function afterLiveOrderConfirmationCreateResult(Model, result) {
            result.itemLines = {};

            var items_to_preload = []
            ,   amount
            ,   line_id;

            for (var i = 1; i <= placeOrder.getLineItemCount('item'); i++)
            {
                if (placeOrder.getLineItemValue('item', 'itemtype', i) === 'Discount' && placeOrder.getLineItemValue('item', 'discline', i))
                {
                    var discline = placeOrder.getLineItemValue('item', 'discline', i);

                    line_id = placeOrder.getLineItemValue('item', 'item', i);
                    amount = Math.abs(parseFloat(placeOrder.getLineItemValue('item', 'amount', i)));

                    result.itemLines[line_id].discount = (result.itemLines[line_id].discount) ? result.itemLines[line_id].discount + amount : amount;
                    result.itemLines[line_id].total = result.itemLines[line_id].amount + result.itemLines[line_id].tax_amount - result.itemLines[line_id].discount;
                    result.itemLines[line_id].discount_name = placeOrder.getLineItemValue('item', 'item_display', i);
                }
                else
                {
                    var rate = Utils.toCurrency(placeOrder.getLineItemValue('item', 'rate', i))
                    ,   item_id = placeOrder.getLineItemValue('item', 'item', i)
                    ,   item_type = placeOrder.getLineItemValue('item', 'itemtype', i);

                    amount = Utils.toCurrency(placeOrder.getLineItemValue('item', 'amount', i));

                    var tax_amount = Utils.toCurrency(placeOrder.getLineItemValue('item', 'tax1amt', i)) || 0
                    ,   total = amount + tax_amount;

                    line_id = placeOrder.getLineItemValue('item', 'id', i);

                    result.itemLines[line_id] = {
                        //@property {String} internalid
                        internalid: line_id
                        //@property {Number} quantity
                    ,   quantity: parseInt(placeOrder.getLineItemValue('item', 'quantity', i), 10)
                        //@property {Number} rate
                    ,   rate: rate
                        //@property {Number} amount
                    ,   amount: amount
                        //@property {Number} tax_amount
                    ,   tax_amount: tax_amount
                        //@property {Number} tax_rate
                    ,   tax_rate: placeOrder.getLineItemValue('item', 'taxrate1', i)
                        //@property {String} tax_code
                    ,   tax_code: placeOrder.getLineItemValue('item', 'taxcode_display', i)
                        //@property {Boolean} isfulfillable
                    ,   isfulfillable: placeOrder.getLineItemValue('item', 'fulfillable', i) === 'T'
                        //@property {Number} discount
                    ,   discount: 0
                        //@property {Number} total
                    ,   total: total
                        //@property {Item} item
                    ,   item: item_id
                        //@property {String} type
                    ,   type: item_type
                        //@property {Object} options
                    ,   options: Utils.getItemOptionsObject(placeOrder.getLineItemValue('item', 'options', i))
                    };

                    items_to_preload[item_id] = {
                        //@property {String} id
                        id: item_id
                        //@property {String} type
                    ,   type: item_type
                    };
                }
            }

            var preloaded_items = StoreItem.preloadItems(_.values(items_to_preload));

            _.each(result.itemLines, function (line)
            {
                line.rate_formatted = Utils.formatCurrency(line.rate);
                line.amount_formatted = Utils.formatCurrency(line.amount);
                line.tax_amount_formatted = Utils.formatCurrency(line.tax_amount);
                line.discount_formatted = Utils.formatCurrency(line.discount);
                line.total_formatted = Utils.formatCurrency(line.total);

                line.item = preloaded_items[line.item + "#order"] ? preloaded_items[line.item + "#order"] : { itemid: line.item };
            });

            // remove the temporary address list by id
            delete result.listAddresseByIdTmp;
        });
    });
});