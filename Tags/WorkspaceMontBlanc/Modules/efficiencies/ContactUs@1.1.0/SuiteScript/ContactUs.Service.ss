/*
    © 2015 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

/*exported service*/
// ContactUs.Service.ss
// ----------------
// Service to manage Artist requests

function service (request)
{
    'use strict';

    var Application = require('Application');
    var method;
    var id;
    var ContactUs;
    var data;

    try
    {
        method = request.getMethod();
        id = request.getParameter('internalid');
        ContactUs = require('ContactUs.Model');
        data = JSON.parse(request.getBody() || '{}');

        switch (method)
        {
            case 'POST':
                id = ContactUs.create(data);
            break;

            default:

                Application.sendError(methodNotAllowedError);
        }
    }
    catch (e)
    {
        Application.sendError(e);
    }
}