define('ItemDetails.EntryPoint.LookItems', [
    'ItemDetails.Model.LookItems'
], function ItemDetailsEntryPointLookItems(
    ItemDetailsModelLookItems
) {
    'use strict';
    return {
        ItemDetailsModelLookItems: ItemDetailsModelLookItems
    };
});