define('GoogleRecaptcha', [
    'jQuery'
], function GoogleRecaptcha(
    jQuery
) {
    'use strict';

    var loader = {
        loadedPromise: jQuery.Deferred(),
        initialized: false,
        loadCaptcha: function loadCaptcha() {
            var url = SC.ENVIRONMENT.published.GoogleRecaptcha_config.googleRecaptchaWidget.apiUrl +
                      '?onload=_loadCaptcha&render=explicit';

            if (SC.ENVIRONMENT.jsEnvironment === 'browser') {
                jQuery.ajax({
                    url: url,
                    dataType: 'script',
                    // always call this script without appending timestamp params to the url
                    cache: true,
                    // Don't throw internal errors/404 because of third party integration
                    preventDefault: true
                });
            } else {
                this.loadedPromise.rejectWith('Google ReCaptcha is a Browser Script only');
            }

            this.initialized = true;


            return this.loadedPromise;
        }
    };
    window._loadCaptcha = function _loadCaptcha() {
        grecaptcha.render('Recaptcha', {
            sitekey: SC.ENVIRONMENT.published.GoogleRecaptcha_config.googleRecaptchaWidget.sitekey,
            theme: SC.ENVIRONMENT.published.GoogleRecaptcha_config.googleRecaptchaWidget.theme
        });
    };

    return loader;
});