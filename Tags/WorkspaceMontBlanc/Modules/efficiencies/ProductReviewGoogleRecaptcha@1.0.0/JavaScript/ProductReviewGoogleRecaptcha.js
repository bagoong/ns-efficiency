define('ProductReviewGoogleRecaptcha', [
    'ProductReviews.Form.View',
    'ProductReviewGoogleRecaptcha.View',
    'GlobalViews.Message.View',
    'Backbone.CompositeView',
    'underscore',
    'jQuery',
    'ProductReviewGoogleRecaptcha.validate'

], function ProductReviewGoogleRecaptcha(
    ProductFormReviews,
    ProductReviewGoogleRecaptchaView,
    GlobalViewsMessageView,
    BackboneCompositeView,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            ProductFormReviews.prototype.initialize =
            _.wrap(ProductFormReviews.prototype.initialize, function wrapProductFormInitialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                if (! ProductFormReviews.prototype.childViews) {
                    ProductFormReviews.prototype.childViews = {};
                    BackboneCompositeView.add(this);
                }

                _.extend(ProductFormReviews.prototype.childViews, {
                    'renderCaptcha.GoogleRecaptcha': function childViewGoogleCaptcha() {
                        return new ProductReviewGoogleRecaptchaView({
                            application: this.application
                        });
                    }
                });

                this.on('afterViewRender', function afterViewRender() {
                    this.$el
                        .find('.product-reviews-form-content-groups')
                        .after('<div class="product-reviews-form-content-groups">' +
                            '<div class="product-reviews-form-content-group" ' +
                                    'data-view="renderCaptcha.GoogleRecaptcha">' +
                            '</div></div>');
                });
            });
        }
    };
});