define('Blog.Configuration', [
    'Configuration',
    'underscore'

], function BlogConfiguration(
    Configuration,
    _
) {
    'use strict';

    var BlogConfig = {
        resultsPerPage: 16
    };

    _.extend(BlogConfig, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'Blog_config',
        model: 'Blog.Configuration',
        call: 'get'
    });

    return BlogConfig;
});