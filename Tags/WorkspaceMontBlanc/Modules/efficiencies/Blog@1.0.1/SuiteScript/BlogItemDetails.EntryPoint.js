define('BlogItemDetails.EntryPoint', [
        'Blog.ItemDetailsPost.Model',
        'Blog.ItemDetailsTags.Model'
], function CheckoutFileUploadEntryPoint(
    BlogItemDetailsPostModel,
    BlogItemDetailsTagsModel
) {

    'use strict';

    return {
        BlogItemDetailsPostModel: BlogItemDetailsPostModel,
        BlogItemDetailsTagsModel: BlogItemDetailsTagsModel
    };
});