define('BlogItemDetails', [
    'ItemDetails.View',
    'Blog.ItemDetails.View',

    'Blog.ItemDetailsPost.Collection',
    'Blog.ItemDetailsTags.Collection',

    'blog_item_row.tpl',

    'Backbone',
    'underscore',
    'Utils'

], function BlogItemRelatedView(
    ItemDetailsView,
    BlogItemDetailsView,

    BlogItemDetailsCollection,
    BlogTagsCollection,

    rowTPL,
    Backbone,
    _
) {
    'use strict';
    var _me;
    var tagscollection = '';
    var filterTags;
    var splitTags;
    var splitTags2;

    _.extend(ItemDetailsView.prototype.childViews, {
        'Blog.Post': function BlogItems() {
            tagscollection.fetch().done( function results(data) {
                splitTags = _me.model.get('custitem_ef_blog_tags').split(', ');
                splitTags2 = [];
                _.each(splitTags, function splitTedtags(val) {
                    filterTags = _.findWhere(data, {name: val});
                    (filterTags) ? splitTags2.push(filterTags.recordid) : '';
                });

                if (filterTags) {
                    _me.collection.fetch({
                        data: {
                            tagId: splitTags2.join(',')
                        }
                    });
                }
            });

            return new BlogItemDetailsView({
                collection: this.collection,
                application: this.application
            });
        }

    });

    ItemDetailsView.prototype.initialize =
    _.wrap(ItemDetailsView.prototype.initialize, function wrapItemDetailsInitialize(fn) {
        _me = this;
        this.collection = new BlogItemDetailsCollection();
        tagscollection = new BlogTagsCollection();
        fn.apply(this, _.toArray(arguments).slice(1));

        this.on('afterViewRender', function afterViewRender() {
            this.$el
                .find('.item-details-product-review-content')
                .append('<div data-view="Blog.Post"></div>');
        });
    });
});