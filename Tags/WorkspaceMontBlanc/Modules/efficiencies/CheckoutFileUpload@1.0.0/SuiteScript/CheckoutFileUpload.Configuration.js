define('CheckoutFileUpload.Configuration', [
    'Configuration',
    'underscore'

], function CheckoutFileUploadConfiguration(
    Configuration,
    _
) {
    'use strict';

    var CheckoutFileUpload = {
        suitelet: {
            script: 'customscript_script_ef_cfu_upload_file',
            deploy: 'customdeploy_script_ef_cfu_upload_file'
        },

        tempUploadFolderId: '6206',
        fields: [
            {
                internalid: 'uploadfield',
                name: 'File',
                folderId: '6205',
                required: true,
                requiredMessage: 'File is mandatory',
                bodyFields: {
                    file: 'custbody_ef_cfu_field1',
                    filename: 'custbody_ef_cfu_field1_name',
                    link: 'custbody_ef_cfu_field1_link'
                },
                CheckoutFileUploadSteps: {
                    orderWizardLocation: 'SC.Checkout.Configuration.Steps.Standard',
                    stepGroup: 0,
                    step: 0
                },
                allowTypes: [
                    'PLAINTEXT',
                    'RTF',
                    'WORD',
                    'XMLDOC',
                    'PDF',
                    'HTMLDOC',
                    'MESSAGERFC',
                    'EXCEL',
                    'JPGIMAGE',
                    'GIFIMAGE',
                    'PNGIMAGE',
                    'TIFFIMAGE',
                    'BMPIMAGE'
                ],
                allowTypesExtensions: [
                    'txt',
                    'rtf',
                    'doc',
                    'xml',
                    'pdf',
                    'htm',
                    'eml',
                    'xls',
                    'csv',
                    'jpg',
                    'gif',
                    'png',
                    'tiff',
                    'bmp'
                ]
            },
            {
                internalid: 'uploadfield2',
                name: 'File2',
                folderId: '6205',
                required: true,
                requiredMessage: 'File is mandatory',
                bodyFields: {
                    file: 'custbody_ef_cfu_upload1',
                    filename: 'custbody_ef_cfu_upload2',
                    link: 'custbody_ef_cfu_upload3'
                },
                CheckoutFileUploadSteps: {
                    orderWizardLocation: 'SC.Checkout.Configuration.Steps.Standard',
                    stepGroup: 2,
                    step: 1
                },
                allowTypes: [
                    'PLAINTEXT',
                    'RTF',
                    'WORD',
                    'XMLDOC',
                    'PDF',
                    'HTMLDOC',
                    'MESSAGERFC',
                    'EXCEL',
                    'JPGIMAGE',
                    'GIFIMAGE',
                    'PNGIMAGE',
                    'TIFFIMAGE',
                    'BMPIMAGE'
                ],
                allowTypesExtensions: [
                    'txt',
                    'rtf',
                    'doc',
                    'xml',
                    'pdf',
                    'htm',
                    'eml',
                    'xls',
                    'csv',
                    'jpg',
                    'gif',
                    'png',
                    'tiff',
                    'bmp'
                ]
            },
            {
                internalid: 'uploadfield3',
                name: 'File3',
                folderId: '6205',
                required: true,
                requiredMessage: 'File is mandatory',
                bodyFields: {
                    file: 'custbodycustbody_ef_cfu_upload4',
                    filename: 'custbodycustbody_ef_cfu_upload5',
                    link: 'custbodycustbody_ef_cfu_upload6'
                },
                CheckoutFileUploadSteps: {
                    orderWizardLocation: 'SC.Checkout.Configuration.Steps.Standard',
                    stepGroup: 4,
                    step: 0
                },
                allowTypes: [
                    'PLAINTEXT',
                    'RTF',
                    'WORD',
                    'XMLDOC',
                    'PDF',
                    'HTMLDOC',
                    'MESSAGERFC',
                    'EXCEL',
                    'JPGIMAGE',
                    'GIFIMAGE',
                    'PNGIMAGE',
                    'TIFFIMAGE',
                    'BMPIMAGE'
                ],
                allowTypesExtensions: [
                    'txt',
                    'rtf',
                    'doc',
                    'xml',
                    'pdf',
                    'htm',
                    'eml',
                    'xls',
                    'csv',
                    'jpg',
                    'gif',
                    'png',
                    'tiff',
                    'bmp'
                ]
            }
        ]
    };

    _.extend(CheckoutFileUpload, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'CheckoutFileUpload_config',
        model: 'CheckoutFileUpload.Configuration',
        call: 'get'
    });

    return CheckoutFileUpload;
});