<li class="storelocator-group-store">
    <div itemscope itemtype="http://schema.org/{{configuration.SchemaOrg}}" class="storeSection" id="store-{{model.internalid}}">
        <div class="store-info">
            <h4 data-type="stores-list-store" data-id="{{ model.internalid }}" class="storelocator-group-store-title" itemprop="name">
                {{ model.name }}
                <img src="{{geolocation}}" class="storelocator-list-store-geolocation">
            </h4>
            {{#if showStoreLink}}
                <a class="storelocator-group-store-button" href="/stores/{{model.urlcomponent}}">{{translate 'Details'}}</a>
            {{/if}}

            {{#if isInStoreStock}}
                <div data-view="StoreStock.Status.View{{ model.internalid }}"></div>
            {{/if}}
        </div>
    </div>
</li>