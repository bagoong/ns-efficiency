define('BOPIS.OrderHistory.Fulfillment.StorePickup.View', [
    'OrderHistory.Fulfillment.View',
    'bopis_order_history_fulfillment_storepickup.tpl'
], function BOPISOrderHistoryFulfillmentStorePickupView(
    OrderHistoryFulfillmentView,
    bopisOrderHistoryFulfillmentStorePickupTpl
) {
    'use strict';
    return OrderHistoryFulfillmentView.extend({
        template: bopisOrderHistoryFulfillmentStorePickupTpl
    });
});