/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('QuickOrder.Router', [
    'Backbone',
    'QuickOrder.View',
    'jQuery'
], function QuickOrderRouter(
    Backbone,
    View
) {
    'use strict';
    return Backbone.Router.extend({

        initialize: function initialize(application) {
            this.application = application;
        },

        routes: {
            'quickorder': 'quickorder'
        },

        quickorder: function quickorder() {
            var view = new View({ application: this.application });
            view.showContent();
        }
    });
});
