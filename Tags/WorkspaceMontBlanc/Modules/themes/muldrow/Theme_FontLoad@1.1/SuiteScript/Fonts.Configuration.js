define('Fonts.Configuration', [
    'Configuration',
    'underscore'
], function SiteConfiguration(
    Configuration,
    _
) {
    'use strict';


    Configuration.webFonts = Configuration.webFonts || {};
    Configuration.webFonts.configuration = Configuration.webFonts.configuration || {};
    Configuration.webFonts.configuration.google =  Configuration.webFonts.configuration.google || {};
    Configuration.webFonts.configuration.google.families = Configuration.webFonts.configuration.google.families || [];

    _.extend(Configuration.webFonts, {
        enabled: true,
        async: true
    });

    Configuration.webFonts.configuration.google.families.push('Work+Sans::latin');

});