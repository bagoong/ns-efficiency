define('Site.Configuration', [
    'Configuration',
    'PSCategories.Configuration',
    'BackInStockNotification.Configuration',
    'ContactUs.Configuration',
    'GuestOrderStatus.Configuration',
    'HashtagGallery.Configuration',
    'StoreLocator.Configuration',
    'Newsletter.SignUp.Configuration'

], function SiteConfiguration(
    Configuration,
    CategoriesConfiguration,
    BackInStockNotificationConfiguration,
    ContactUsConfiguration,
    GuestOrderStatusConfiguration,
    HashtagGalleryConfiguration,
    StoreLocatorConfiguration,
    NewsletterConfiguration
) {
    'use strict';

    CategoriesConfiguration;
    BackInStockNotificationConfiguration;
    ContactUsConfiguration;
    GuestOrderStatusConfiguration;
    HashtagGalleryConfiguration;
    StoreLocatorConfiguration;
    NewsletterConfiguration;


    Configuration.webFonts = {
        enabled: true,
        async: true,
        configuration: {
            google: { families: [ 'Roboto:400,700,300:latin' ] }
        }
    };

    // IMPORTANT THINGS TO CUSTOMIZE
    StoreLocatorConfiguration.googleMapsApiKey = 'ABCD';

    ContactUsConfiguration.formId = '1';
    ContactUsConfiguration.hash = '5b04e56061487343809e';

    NewsletterConfiguration.formId = '2';
    NewsletterConfiguration.hash = '7cbe9825db61c5b73ea0';

    CategoriesConfiguration.secureCategories = true;

});