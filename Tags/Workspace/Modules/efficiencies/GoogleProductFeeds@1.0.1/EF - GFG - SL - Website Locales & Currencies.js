var Handler = (function(){


    function main(request,response)
    {
        var siteId = request.getParameter('website');
        var Model = Application.getModel('Website');
        var website = Model.get(siteId);

        Application.sendContent(website);
    }

    return {
        main: main
    };

}());