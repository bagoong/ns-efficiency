var Handler = (function(){
    var context = nlapiGetContext();

    function beforeLoad(pType, form, request){
        var type = pType.toString();

        if (type === 'create' || type === 'copy' || type === 'edit')
        {
            var localeField = form.addField('custpage_fake_locale', 'select', 'Locale');
            var currencyField = form.addField('custpage_fake_currency', 'select', 'Currency');
            form.insertField(localeField,'custrecord_ef_gfg_fi_template');
            form.insertField(currencyField,'custpage_fake_locale');

            if(request && context.getExecutionContext() === 'userinterface')
            {
                var record = nlapiGetNewRecord();

                var url = request.getURL();
                var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
                var domain = matches && matches[1];  // domain will be null if no match is found

                record.setFieldValue('custrecord_ef_gfg_fi_molecule',domain);
            }
        }else if(type === 'view'){

    	   var fileLink = nlapiResolveURL(
                   'SUITELET',
                   'customscript_ef_gfg_sl_combiner',
                   'customdeploy_ef_gfg_sl_combiner',
                   true
               );

    	   fileLink += "&feed="+request.getParameter("id");
           form.addField("custpage_downloadfeed", "url", "", null, null).setLinkText( "Google Feed Link").setDefaultValue( fileLink );
        }

    }

    function beforeSubmit(type){}

    function afterSubmit(type){}

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };

}());