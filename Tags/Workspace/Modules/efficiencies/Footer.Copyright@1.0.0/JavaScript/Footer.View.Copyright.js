define('Footer.View.Copyright', [
    'Footer.Copyright',
    'Footer.View',
    'underscore'
], function FooterViewCopyright(
    FooterCopyright,
    FooterView
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            FooterView.prototype.installPlugin('postContext', {
                name: 'footerContext',
                priority: 10,
                execute: function execute(context, view) {
                    FooterCopyright.contextExecute(application, context, view);
                }
            });
        }
    };
});