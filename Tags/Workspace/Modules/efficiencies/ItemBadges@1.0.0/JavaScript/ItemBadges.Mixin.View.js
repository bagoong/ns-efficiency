/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges.Mixin.View', [
    'ItemBadges.View',
    'ItemBadges.Collection',
    'PluginContainer',
    'underscore'
], function ItemBadgesMixinView(ItemBadgesView, Collection, PluginContainer, _) {
    'use strict';

    return {
        extendView: function extendView(View, param) {
            View.prototype.initialize =
            _.wrap(View.prototype.initialize, function prototypeInitialize(fn) {
                var collection = new Collection();
                fn.apply(this, _.toArray(arguments).slice(1));

                _.extend(View.prototype.childViews, {
                    'Itembadges.View': function ItembadgesView() {
                        return new ItemBadgesView({
                            model: this.model,
                            collection: collection
                        });
                    }
                });

                this.on('afterViewRender', function afterViewRender() {
                    if (this.$('.itemBadge-view-container').length === 0) {
                        this.$(param.find)
                            .after('<div class="itemBadge-view-container ' +
                                param.htmlClass + '" data-view="Itembadges.View"></div>');
                    }
                });
            });
        }
    };
});