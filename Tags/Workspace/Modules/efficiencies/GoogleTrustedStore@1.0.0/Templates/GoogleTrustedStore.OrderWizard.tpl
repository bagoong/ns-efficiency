<!-- START Google Trusted Stores Order -->
<div id="gts-order" style="display:none;" translate="no">

  <!-- start order and merchant information -->
  <span id="gts-o-id">{{orderId}}</span>
  <span id="gts-o-email">{{customerEmail}}</span>
  <span id="gts-o-country">{{customerCountry}}</span>
  <span id="gts-o-currency">{{currency}}</span>
  <span id="gts-o-total">{{orderTotal}}</span>
  <span id="gts-o-discounts">{{orderDiscount}}</span>
  <span id="gts-o-shipping-total">{{orderShipping}}</span>
  <span id="gts-o-tax-total">{{orderTax}}</span>
  <span id="gts-o-est-ship-date">{{estShipDate}}</span>
  <span id="gts-o-est-delivery-date">{{estDeliveryDate}}</span>
  <span id="gts-o-has-preorder">{{isPreorder}}</span>
  <span id="gts-o-has-digital">{{isDigitalGood}}</span>
  <!-- end order and merchant information -->

  <!-- start repeated item specific information -->
  <!-- item example: this area repeated for each item in the order -->
  {{#each items}}
  <span class="gts-item">
    <span class="gts-i-name">{{displayname}}</span>
    <span class="gts-i-price">{{onlinecustomerprice}}</span>
    <span class="gts-i-quantity">{{quantity}}</span>
    {{#if shoppingId}}
      <span class="gts-i-prodsearch-id">{{internalId}}</span>
      <span class="gts-i-prodsearch-store-id">{{shoppingId}}</span>
    {{/if}}
  </span>
  {{/each}}
  <!-- end item 1 example -->
  <!-- end repeated item specific information -->

</div>
<!-- END Google Trusted Stores Order -->