<section class="store-stock-container">
    <div role="tabpanel">
        <ul class="tabpanel-head" role="tablist">
            <li role="presentation" class="active">
                <a class="tabpanel-head-tab" data-target="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">
                    Shipping
                </a>
            </li>
            <li role="presentation">
                <a class="tabpanel-head-tab" data-target="#pickup" aria-controls="pickup" role="tab" data-toggle="tab">
                    Pickup
                </a>
            </li>
        </ul>

        <div class="tabpanel-body">
            <div role="tabpanel" class="tabpanel-tab active" id="shipping">
                <div class="store-stock-total-view">
                    {{#if isDisplayAsBadge}}
                        <span class="{{ badgeClass }}">{{ translate badgeText }}</span>
                    {{else}}
                        {{ translate "$(0) items in stock" quantityAvailable }}
                    {{/if}}
                </div>
            </div>
            <div role="tabpanel" class="tabpanel-tab" id="pickup">
                {{#if isSelectionComplete}}
                    <div>
                        <label class="store-stock-label-find-store">{{translate 'Find your local store'}}</label>
                        <input type="text" name="store-stock-store" class="store-stock-input-store" value="{{storeStockStoreSearchText}}" />
                        <button class="store-stock-button-check-stock" data-action="show-storelocator" >
                            {{translate 'Check Stock'}}
                        </button>
                    </div>
                {{else}}
                    <p class="store-stock-help">
                        <i class="store-stock-help-icon"></i>
                        <span class="store-stock-help-text">
                            {{ translate 'Please select options to check stock on your local store'}}
                        </span>
                    </p>
                {{/if}}
            </div>
        </div>
</section>