define('Store.View.StoreList.store', [
    'Backbone',
    'StoreLocator.Configuration',
    'storelist_store.tpl',
    'Utils'
], function storeListStore(
    Backbone,
    Configuration,
    Template
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(data) {
            this.model  = data.model;
        },

        getContext: function getContext() {
            return {
                model: this.model,
                showStoreLink: Configuration.storePage && this.model.get('urlcomponent'),
                configuration: Configuration
            };
        }
    });
});