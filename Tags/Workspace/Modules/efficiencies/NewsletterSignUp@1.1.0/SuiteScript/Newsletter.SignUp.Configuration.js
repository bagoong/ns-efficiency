/**
 * Created by pzignani on 01/10/2014.
 */
define('Newsletter.SignUp.Configuration', [
    'Configuration'
], function NewsletterSignUpConfiguration() {
    'use strict';

    return {
        domain: 'forms.netsuite.com',
        formId: '1',
        hash: '5b04e56061487343809e'
    };
});
