/* exported service */
// Store.Service.ss
// ----------------
// Service to manage addresses requests

function service(request) {
    'use strict';
    var Application = require('Application');

    var method = request.getMethod();
    var urlcomponent = request.getParameter('urlcomponent');
    var Store = require('Store.Model');


    try {
        switch (method) {
        case 'GET':
            if (urlcomponent) {
                Application.sendContent(Store.getByUrlcomponent(urlcomponent), {
                    'cache': response.CACHE_DURATION_MEDIUM
                });
            } else {
                Application.sendContent(Store.list(), {'cache': response.CACHE_DURATION_MEDIUM});
            }
            break;
        default:
            return Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}