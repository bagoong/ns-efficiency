 <button data-action="show-choose-store" class="profile-mystore-button-choose-store" data-showin="modal">
    {{btnText}}
</button>
 <button data-action="show-choose-store" class="profile-mystore-button-choose-store-pusher" data-showin="pushpane" data-type="sc-pusher" data-target="pushable-map{{viewID}}">
    {{btnText}}
</button>
<div class="profile-mystore-pushpane-map" data-action="pushable-map{{viewID}}" data-id="pushable-map{{viewID}}">
    <div data-view="SC.PUSHER.MAP"></div>
</div>