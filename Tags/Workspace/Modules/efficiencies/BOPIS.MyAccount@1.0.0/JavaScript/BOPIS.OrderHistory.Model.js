define('BOPIS.OrderHistory.Model', [
    'OrderHistory.Model',
    'Transaction.Collection',
    'underscore'
], function BOPISOrderModel(
    OrderHistoryModel,
    TransactionCollection,
    _
) {
    'use strict';

    _.extend(OrderHistoryModel.prototype, {
        initialize: _.wrap(OrderHistoryModel.prototype.initialize, function wrappedInitialize(fn, attributes) {
            fn.apply(this, _.toArray(arguments).slice(1));

            this.on('change:storepickupfulfillments', function onChangePickedupItems(model, attributes) {
                var pickedupitems =
                            attributes instanceof TransactionCollection ?
                            attributes :
                            new TransactionCollection(attributes, {parse: true});
                model.set('storepickupfulfillments', pickedupitems, {silent: true});
            });
            this.trigger('change:storepickupfulfillments', this, attributes && attributes.fulfillments || []);
        })
    });
});