define('LiveOrder.GiftWrap', [
    'Application',
    'Utils',
    'LiveOrder.Model',
    'underscore'
], function LiveOrderGiftWrap(
    Application,
    Utils,
    LiveOrder,
    _
) {
    'use strict';

    var idGenerator = function idGenerator(qty) {
        return (Math.random().toString(36) + '00000000000000000').slice(2, qty + 2);
    };

    /* EVENTS: */
    Application.on('before:LiveOrder.addLine', function beforeLiveOrderAddLineGiftWrap(Model, currentLine) {
        if (currentLine.options && !currentLine.options.custcol_ef_gw_giftwrap) {
            delete currentLine.options.custcol_ef_gw_id;
            delete currentLine.options.custcol_ef_gw_message;
        }
        Model.addGiftWrap(currentLine);
    });
    Application.on('before:LiveOrder.addLines', function beforeLiveOrderAddLinesGiftWrap(Model, lines) {
        _.each(lines, function eachLines(l) {
            if (l.options && !l.options.custcol_ef_gw_giftwrap) {
                delete l.options.custcol_ef_gw_id;
                delete l.options.custcol_ef_gw_message;
            }
        });
        Model.addGiftWraps(lines);
    });
    Application.on('before:LiveOrder.removeLine', function beforeLiveOrderRemoveLine(Model, currentLine) {
        Model.removeGiftWrap(currentLine);
    });
    Application.on('after:LiveOrder.getLines', function afterGetLines(Model, lines) {
        Model.reformatLines(lines);
    });
    Application.on('before:LiveOrder.updateLine', function beforeLiveOrderUpdateLine(Model, lineid, line) {
        if (line.options && !line.options.custcol_ef_gw_giftwrap) {
            delete line.options.custcol_ef_gw_id;
            delete line.options.custcol_ef_gw_message;
        }
    });

    /* END EVENTS */
    _.extend(LiveOrder, {
        tweakLinesGetGiftWrap: function tweakLinesGetGiftWrap(currentLine) {
            var generatedId;
            var generatedIdNL;
            var generatedIdOld;
            var giftItemId;
            var newLine;

            if (currentLine && currentLine.options && currentLine.options.custcol_ef_gw_giftwrap) {
                generatedId = idGenerator(8);
                generatedIdNL = new String('G:' + generatedId).toString(); // HACKS for weird platform bugs with strings
                generatedIdOld = new String('P:' + generatedId).toString(); // HACKS for weird platform bugs
                giftItemId = currentLine.options.custcol_ef_gw_giftwrap;
                newLine = {
                    item: {
                        internalid: parseInt(giftItemId, 10)
                    },
                    quantity: currentLine.quantity,
                    options: {
                        custcol_ef_gw_id: generatedIdNL
                    }
                };

                currentLine.options.custcol_ef_gw_id = generatedIdOld;
                return newLine;
            }
            return null;
        },
        addGiftWraps: function addGiftWraps(lines) {
            var currentLine;
            var giftWrapLine;

            if (_.isArray(lines) && lines.length === 1) { // Only 1 line add to cart support right now

                currentLine = lines[0];
                giftWrapLine = this.tweakLinesGetGiftWrap(currentLine);

                if (giftWrapLine) {
                    lines[1] = giftWrapLine;
                }
            }
        },
        addGiftWrap: function addGiftWrap(currentLine) {
            var giftWrapLine = this.tweakLinesGetGiftWrap(currentLine);
            if (giftWrapLine) {
                Application.once('after:LiveOrder.addLine', function(Model, responseData) {
                    if (responseData) {
                        Model.addLine(giftWrapLine);
                    }
                });
            }
        },
        removeGiftWrap: function removeGiftWrap(currentLine) {
            var orderFieldKeys = [
                'orderitemid',
                'quantity',
                'internalid',
                'options'
            ];

            // Removing current line, we have to find the giftwrap
            var line = order.getItem(currentLine, orderFieldKeys);
            var optionGwId = _.findWhere(line.options, {id: 'CUSTCOL_EF_GW_ID'});
            var optionGw = _.findWhere(line.options, {id: 'CUSTCOL_EF_GW_GIFTWRAP'});

            var key;
            var lines;

            // If it has a giftwrap
            if (optionGwId && optionGw && optionGwId.value && optionGw.value) {
                // we have to search for the other line :(
                key = optionGwId.value.replace('P:', 'G:');
                lines = order.getItems(orderFieldKeys);

                // Why EVERY instead of each? Every will break on the first FALSE returned;
                // We need to iterate only until we get the gift wrap item
                _.every(lines, function every(l) {
                    var gwId = _.findWhere(l.options, { id: 'CUSTCOL_EF_GW_ID'} );
                    // Found it? so after the removeLine, let's hang to it
                    if (gwId && gwId.value === key) {
                        console.log('gwId', JSON.stringify(gwId));
                        Application.once(
                            'after:LiveOrder.removeLine',
                            function afterLiveOrderRemoveLine(Model) {
                                Model.removeLine(l.orderitemid);
                            }
                        );
                        return false;
                    }
                    return true;
                });
            }
        },
        reformatLines: function reformatLines(lines) {
            var giftWrapsHashMap = {};
            var offsets = 0;
            var i;
            var line;
            var option;
            var type;
            var key;

            for (i = 0; i < lines.length; i++) {
                line = lines[i];
                if (line.options) {
                    option = _.findWhere(line.options, {id: 'CUSTCOL_EF_GW_ID'});
                    type = option && option.value && option.value.substr(0, 2);
                    if (type) {
                        key = option.value.replace(type, '');
                        if (type === 'G:') {
                            giftWrapsHashMap[key] = giftWrapsHashMap[key] || {};
                            giftWrapsHashMap[key].giftwrap = i;
                        }
                        if (type === 'P:') {
                            giftWrapsHashMap[key] = giftWrapsHashMap[key] || {};
                            giftWrapsHashMap[key].parent = i;
                        }
                    }
                }
            }

            _.each(giftWrapsHashMap, function _giftWrapsHashMap(value, k) {
                if (value && !_.isUndefined(value.giftwrap) && !_.isUndefined(value.parent)) {
                    lines[value.parent - offsets].giftWrap = lines[value.giftwrap - offsets];
                    lines.splice(value.giftwrap - offsets, 1);

                    offsets++;
                } else {
                    console.error(
                        'Error with giftwrap sync',
                        'Key:' + JSON.stringify(k) + ',Value:' + JSON.stringify(value)
                    );
                }
            });
        }
    });
});