define('ItemDetails.View.GiftWrap', [
    'ItemDetails.View',
    'underscore',
    'Backbone.CollectionView',
    'ItemViews.Option.View',
    'GiftWrap.Configuration',
    'Backbone'
], function ItemDetailsViewGiftWrap(
    View,
    _,
    BackboneCollectionView,
    ItemViewsOptionView,
    GiftWrapConfig,
    Backbone
) {
    'use strict';

    View.prototype.childViews['ItemDetails.Options'] = function ItemDetailsOptions() {
        var optionsToRender = this.model.getPosibleOptions();
        var available;
        var messageOption;
        var messageIndex;
        var giftWrapOption;
        var giftWrapIndex;

        _.each(optionsToRender, function each(option) {
            // If it's a matrix it checks for valid combinations
            if (option.isMatrixDimension) {
                available = this.model.getValidOptionsFor(option.itemOptionId);
                _.each(option.values, function eachOption(value) {
                    value.isAvailable = _.contains(available, value.label);
                });
            }
        }, this);

        // Check gift wrap options order
        messageOption = _.findWhere(optionsToRender, {
            cartOptionId: GiftWrapConfig.GiftWrapConfig.cartOptions.giftWrapMessage
        });

        messageIndex = optionsToRender.indexOf(messageOption);

        giftWrapOption = _.findWhere(optionsToRender, {
            cartOptionId: GiftWrapConfig.GiftWrapConfig.cartOptions.giftWrap
        });

        giftWrapIndex = optionsToRender.indexOf(giftWrapOption);
        if (messageIndex < giftWrapIndex) {
            // gift wrap option should appear before the gift wrap message. We exchange them if this is not the case
            optionsToRender[messageIndex] = giftWrapOption;
            optionsToRender[giftWrapIndex] = messageOption;
        }
        return new BackboneCollectionView({
            collection: new Backbone.Collection(optionsToRender),
            childView: ItemViewsOptionView,
            viewsPerRow: 1,
            childViewOptions: {
                item: this.model
            }
        });
    };
});