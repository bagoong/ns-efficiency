define('Store.Views.StoreLocator.List.Store', [
    'Backbone',
    'StoreLocator.Configuration',
    'storelocator_list_store.tpl',
    'Utils'
], function StoreViewsStoreLocatorListStore(Backbone, Configuration, Template) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(data) {
            this.model  = data.model;
        },

        getContext: function getContext() {
            return {
                model: this.model,
                showStoreLink: Configuration.storePage && this.model.get('urlcomponent'),
                storeMarker: Configuration.storeMarker,
                configuration: Configuration,
                geolocation: Configuration.storeMarker.icon
                    || 'http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless.png'
            };
        }
    });
});