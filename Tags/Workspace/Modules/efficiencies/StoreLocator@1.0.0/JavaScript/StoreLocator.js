/*
    StoreLocator
    Required modules [
        StoreLocator.Configuration - load default configuration,
        StoreLocator.Router - used for routing your applications URL's when using hash tags(#),
        underscore - javascript plugin
   ]
   @return Router new object
*/
define('StoreLocator', [
    'StoreLocator.Configuration',
    'StoreLocator.Router',
    'underscore'
], function StoreLocator(Configuration, Router, _) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            var backendConfig = SC.ENVIRONMENT &&
                                SC.ENVIRONMENT.published &&
                                SC.ENVIRONMENT.published.StoreLocator_config;

            this.application = application;

            _.defaults(Configuration, backendConfig);
            Configuration.unitsHandler = Configuration.unitsHandlers[Configuration.units];

            if (!Configuration.unitsHandler) {
                throw Error('Units Handler needed');
            }

            this.application.Configuration.storeLocator = Configuration;

            return new Router(this.application);
        }
    };
});