<div class="storelocator-form">
    <form role="form">
        <fieldset>
             <div class="control-group">
                <label for="">{{translate 'Zipcode or town name'}}</label>
                <div class="storelocator-input">
                    <input type="text" class="storelocator-search form-control" name="storelist-place" id="storelist-place">
                </div>
             </div>
             <div class="control-group">
                <button type="submit" class="storelocator-submit storelocator-button">
                    {{translate 'Find Store'}}
                </button>
                {{#if showGeolocationButton}}
                    <button type="button" name="geolocate" class="storelocator-geolocate storelocator-button">
                        {{translate 'Use Current Location'}}
                    </button>
                {{/if}}
                <a href="{{listStoreLink}}">{{translate 'Show All Stores'}}</a>
            </div>
        </fieldset>
    </form>
</div>