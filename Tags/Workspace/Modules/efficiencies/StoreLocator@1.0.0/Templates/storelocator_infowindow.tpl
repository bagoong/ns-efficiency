<div id="store-info-window-{{model.internalid}}" class="store-info-window">
    <div id="top-cont">
        <h4>{{ model.name }}</h4>
    </div>
    <div id="mid-cont">

        {{#if hasThumbnail }}
        <div id="store-img" class="infoWinWidth">
            <img src="{{model.thumbnail.name }}" alt="{{model.name}}">
        </div>
		{{/if}}
        <div id="store-address">
            <address>
				{{ model.address1}}
				{{ model.address2 }}<br>
                {{ model.city}}, {{ model.state }}, {{model.zipcode}}<br>
                {{ model.country}}<br>
            </address>
        </div>
    </div>
    <div id="bot-cont" class="infoWinWidth">
        <ul>
            {{#if showStoreLink}}
            	<li>
					<a href="/stores/{{model.urlcomponent}}">{{translate 'Details'}}</a>
				</li>
			{{ else }}
            	<li>
					<a href="{{gMapsLink}}" target="_blank">{{translate 'Directions'}}</a>
				</li>
       		{{/if}}
        </ul>
    </div>
</div>