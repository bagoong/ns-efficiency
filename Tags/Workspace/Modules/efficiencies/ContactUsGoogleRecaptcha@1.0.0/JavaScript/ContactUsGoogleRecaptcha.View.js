define('ContactUsGoogleRecaptcha.View', [
    'GoogleRecaptcha',
    'ContactUs.View',
    'contactUs_google_recaptcha.tpl',
    'Backbone',
    'underscore',
    'Utils'

], function ContactUsGoogleRecaptchaView(
   GoogleRecaptcha,
   ContactUsView,
   ContactUsTpl,
   Backbone,
   _

) {
    'use strict';

    return Backbone.View.extend({
        template: ContactUsTpl,

        render: function render() {
            this._render();
            GoogleRecaptcha.loadCaptcha();
        },

        getContext: function getContext() {
            return {
                rlabel: _.translate('ReCaptcha')
            };
        }
    });
});