define('ContactUsGoogleRecaptcha.validate', [
    'ContactUs.View',
    'GlobalViews.Message.View',
    'Backbone.CompositeView',
    'underscore',
    'jQuery'

], function ProductReviewGoogleRecaptcha(
    ContactUsView,
    GlobalViewsMessageView,
    BackboneCompositeView,
    _,
    jQuery
) {
    'use strict';

    // frontend validation
    ContactUsView.prototype.saveTheForm =
        _.wrap(ContactUsView.prototype.saveTheForm, function wrapProductFormInitialize(fn) {
            var resp;
            var msgContainerParent;
            var globalViewMessage;
            resp = jQuery('.g-recaptcha-response').val();
            if (_.isEmpty(resp)) {
                globalViewMessage = new GlobalViewsMessageView({
                    message: _.translate('ReCaptcha is invalid'),
                    type: 'error',
                    closable: true
                });
                msgContainerParent = jQuery('.msg');
                msgContainerParent.html(globalViewMessage.render().$el.html());
                return false;
            } else {
                jQuery('.global-views-message-button').click();
                this.model.set('g-recaptcha-response', resp);
            }
            fn.apply(this, _.toArray(arguments).slice(1));
        });
});