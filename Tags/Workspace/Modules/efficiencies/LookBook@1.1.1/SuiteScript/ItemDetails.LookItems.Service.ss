function service(request) {
    'use strict';
    var Application = require('Application');
    var ItemDetailsLookItems = require('ItemDetails.Model.LookItems');

    var method = request.getMethod();
    var itemID = request.getParameter('itemID');

    try {
        switch (method) {
        case 'GET':
            if (itemID) {
                Application.sendContent(
                        ItemDetailsLookItems.getSameLookItem(itemID),
                        {'cache': response.CACHE_DURATION_MEDIUM}
                    );
            }
            break;
        default:
            return Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}