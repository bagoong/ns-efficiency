define('Backbone.CompositeView.CollectionViewFix', [
    'underscore',
    'Backbone.CompositeView',
    'Utils'
], function CollectionViewFix(
    _,
    BackboneCompositeView,
    Utils
) {
    'use strict';

    /**
     * FIX: enable data-phone-template and data-tablet-template to work on collection views
     */
    _(BackboneCompositeView).extend({

        _setCustomTemplate: function setCustomTemplate(placeholder, childView, templatePrefix) {
            var template;
            var dataTemplatePrefix = templatePrefix ? templatePrefix : '';
            var templateName = childView.placeholderData[dataTemplatePrefix ? dataTemplatePrefix + 'Template' : 'template'];

            var definitiveTemplateName = Utils.selectByViewportWidth({
                // remember that data-phone-template get's converted in phoneTemplate when we use jQuery.data()
                // PS FIX
                phone: childView.placeholderData[dataTemplatePrefix ? dataTemplatePrefix + 'PhoneTemplate' : 'phoneTemplate'],
                tablet: childView.placeholderData[dataTemplatePrefix ? dataTemplatePrefix + 'TabletTemplate' : 'tabletTemplate'],
                desktop: templateName
            }, templateName);

            if (definitiveTemplateName) {
                // IMPORTANT: we are require()ing the template dynamically! In order to this to work, the template should
                // be ALREADY loaded. This take importance when doing unit tests!
                template = Utils.requireModules(definitiveTemplateName + '.tpl');
                childView[templatePrefix ? templatePrefix + 'Template' : 'template'] = template;
            }
        }

    });
});