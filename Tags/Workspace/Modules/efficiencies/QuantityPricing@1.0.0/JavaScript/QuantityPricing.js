define('QuantityPricing', [
    'QuantityPricing.View',
    'ItemDetails.View',
    'underscore',
    'SC.Configuration'
], function QuantityPricing(
    QuantityPricingView,
    ItemView,
    _,
    Configuration
) {
    'use strict';

    ItemView.prototype.childViews['Item.QuantityPricing'] = function ItemQuantityPricing() {
        return new QuantityPricingView({ model: this.model });
    };

    Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};
    _.extend(Configuration.itemKeyMapping, {

        _priceSchedule: function _priceSchedule(item) {
            // This normalization is for matrix children and pricing schedule bugs
            // price schedule should always start on minimum quantity, not on 0/1
            // this was working right on ITEM API for matrix parents, but not for matrix childs
            var priceSchedule = item.get('onlinecustomerprice_detail').priceschedule;
            var minimumquantity = item.get('minimumquantity');
            var countDeleteLines = 0;

            if (minimumquantity && priceSchedule) {
                _.each(priceSchedule, function eachPriceSchedule(line) {
                    if (line.maximumquantity <= minimumquantity) {
                        countDeleteLines ++;
                    }

                    if (
                        line.minimumquantity < minimumquantity &&
                        (line.maximumquantity > minimumquantity || !line.maximumquantity)
                    ) {
                        line.minimumquantity = minimumquantity;
                    }
                });
            }
            return priceSchedule && priceSchedule.slice(countDeleteLines, priceSchedule.length);
        },
        _priceScheduleForTable: function _priceScheduleForTable(item) {
            var priceSchedule = item.get('_priceSchedule');
            var children = item.getSelectedMatrixChilds();
            var newQuantities;

            if (children && children.length === 1) {
                priceSchedule = children[0].get('_priceSchedule');
            } else if (children && children.length > 1) {
                // We have to check if ALL matrix children have the same price schedule
                // For easy comparison, we're using object's stringify
                priceSchedule = _.reduce(children, function priceScheduleReduce(memo, child) {
                    // check if the memo doesn't come false already
                    return memo &&
                            // check if the child value is the same as the one in the memo
                        (memo === (JSON.stringify(child.get('_priceSchedule'))))  &&
                            // if it is, return the value. Otherwise return false.
                        memo;
                }, JSON.stringify(priceSchedule));
                priceSchedule = priceSchedule && JSON.parse(priceSchedule);
            }

            if (_.isArray(priceSchedule)) {
                newQuantities = _.map(priceSchedule, function eachPriceSchedule(priceLine) {
                    return {
                        minimum: priceLine.minimumquantity || 1,
                        maximum: (priceLine.maximumquantity && (priceLine.maximumquantity - 1) ),
                        price: priceLine.price_formatted
                    };
                });
            }
            return newQuantities;
        }
    });

    return {
        mountToApp: function mountToApp() {
            ItemView.prototype.initialize = _.wrap(ItemView.prototype.initialize, function initialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                this.on('afterViewRender', function afterViewRender() {
                    this.$el.find('.item-details-actions [data-action="plus"], .quick-view-confirmation-modal-price')
                        .after('<div data-view="Item.QuantityPricing"></div>');
                });
            });
        }
    };
});
