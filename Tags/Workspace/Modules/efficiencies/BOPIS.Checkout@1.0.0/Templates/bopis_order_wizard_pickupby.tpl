{{!
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
}}

{{#if isActive}}
    {{#if isNotReadOnly}}
    <div class="bopis-order-wizard-pickupby-module">
        <h5 class="order-wizard-msr-package-creation-header-subtitle">{{translate 'Who will pick up the order?'}}</h5>
        <div class="bopis-form-group">
            <input type="radio"
                   name="pickupby-options"
                   data-action="change-pickupby-options"
                   class="bopis-radio-pickupby-options"
                   value="me"
                   id="pickupby-options-me"
                   {{#if isMeSelected}}checked{{/if}}
            />
            <label class="bopis-form-label" for="pickupby-options-me">{{translate 'Me'}}</label>

            <input type="radio"
                   name="pickupby-options"
                   data-action="change-pickupby-options"
                   class="bopis-radio-pickupby-options"
                   value="others"
                   id="pickupby-options-others"
                   {{#if isOthersSelected}}checked{{/if}}
            />
            <label class="bopis-form-label" for="pickupby-options-others">{{translate 'Someone Else'}}</label>
        </div>

        {{#if isOthersSelected}}
            <div class="bopis-form-group">
                <span>{{translate 'Pick up by:'}}</span>
                <input value="{{ pickupByName }}" name="pickupByName" class="bopis-form-input" data-action="set-pickupby" />
            </div>
            <div class="bopis-form-group">
                <span>{{translate 'Email:'}}</span>
                <input value="{{ pickupByEmail }}" name="pickupByEmail" class="bopis-form-input" data-action="set-pickupby" />
            </div>
        {{/if}}
    </div>
    {{else}}
        <div class="bopis-order-wizard-pickupby-module-wizard-readonly">
            <label class="bopis-label-pickupby">{{translate 'Pick up by:'}}</label>
            {{#if isMeSelectedReadonly}}
                {{translate 'Me'}}
            {{else}}
                {{ pickupByName }} ({{ pickupByEmail }})
            {{/if}}
        </div>
    {{/if}}
{{/if}}