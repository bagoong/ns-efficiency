{{!
© 2015 NetSuite Inc.
User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
provided, however, if you are an authorized user with a NetSuite account or log-in, you
may use this code subject to the terms that govern your access and use.
}}

{{#if isActive}}
    {{#if isReadOnly}}<div class="order-wizard-storepickup-module">{{/if}}

    {{#if isReadOnly}}
        <h3 class="">
            {{translate 'Store for Pick Up'}}
        </h3>
    {{else}}
        <div class="order-wizard-msr-package-creation-header">
            <h5 class="order-wizard-msr-package-creation-header-subtitle">
                {{translate 'Store for pick up: '}}
            </h5>
        </div>
    {{/if}}
    <div data-view="StoreDetail" {{#if isReadOnly}}class="order-wizard-storepickup-module-details"{{/if}}></div>

    {{#if isReadOnly}}</div>{{/if}}
{{/if}}