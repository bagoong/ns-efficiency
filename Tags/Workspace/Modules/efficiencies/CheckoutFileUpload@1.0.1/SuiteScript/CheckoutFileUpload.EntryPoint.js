define('CheckoutFileUpload.EntryPoint', [
    'CheckoutFileUpload.Configuration',
    'CheckoutFileUpload.Model',
    'SuiteletService.Configuration',
    'SuiteletService'
], function CheckoutFileUploadEntryPoint(
    Configuration,
    Model,
    SuiteletServiceConfig,
    SuiteletService
) {
    'use strict';
    return {
        Configuration: Configuration,
        Model: Model,
        SuiteletServiceConfig: SuiteletServiceConfig,
        SuiteletService: SuiteletService
    };
});