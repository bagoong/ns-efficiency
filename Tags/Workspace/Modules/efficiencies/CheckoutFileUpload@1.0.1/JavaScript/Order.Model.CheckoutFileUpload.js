define('Order.Model.CheckoutFileUpload', [
    'Order.Model',
    'CheckoutFileUpload.Model',
    'CheckoutFileUpload.Collection',

    'Backbone',
    'underscore'

], function OrderModelCheckoutFileUpload(
    OrderModel,
    CheckoutFileUploadModel,
    CheckoutFileUploadCollection,

    Backbone,
    _
) {
    'use strict';
    var OrderFile;

    OrderFile = {
        Model: Backbone.Model.extend({}),
        Collection: Backbone.Collection.extend({})
    };

    OrderModel.prototype.filesCollection = OrderFile.Collection;


    _.extend(OrderModel.prototype, {
        filesModel: OrderFile.Model,
        filesCollection: OrderFile.Collection,
        initialize: _.wrap(OrderModel.prototype.initialize, function initialize(fn, attributes) {
            var oldReturn;
            oldReturn = fn.apply(this, _.toArray(arguments).slice(1));

            this.on('change:files', function changeFiles(model, files) {
                model.set('files', new model.filesCollection(files), {silent: true});
            });
            this.trigger('change:files', this, attributes && attributes.files || []);

            return oldReturn;
        })
    });

    return OrderModel;
});