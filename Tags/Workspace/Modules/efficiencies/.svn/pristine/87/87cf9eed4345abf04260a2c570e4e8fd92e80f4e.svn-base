// @module InventoryDisplay
define('InventoryDisplay.Status.Shipping.View', [
    'InventoryDisplay.Status.View',
    'InventoryDisplay.InventorySummaryHashMap.Model'
], function InventoryDisplayStatusShippingView(
    InventoryDisplayStatusView,
    InventorySummaryHashMap
) {
    'use strict';

    return InventoryDisplayStatusView.extend({

        orderType: 'shipping',

        initialize: function initialize() {
            this.stockForShipping = new InventorySummaryHashMap();
            InventoryDisplayStatusView.prototype.initialize.apply(this, arguments);
        },

        fetchStockData: function fetchStockData() {
            var self = this;

            this.listenTo(this.stockForShipping, 'sync', function onSync() {
                self.hasFetchedStockData = true;
                self.stockModel = self.stockForShipping.get(self.model.get('internalid'));
                self.render();
            });

            this.stockForShipping.fetch({
                data: {
                    internalid: this.model.get('internalid'),
                    locationType: this.getModuleConfig().LOCATION_TYPE_WAREHOUSE,
                    summarize: 'T'
                }
            });
        }
    });
});