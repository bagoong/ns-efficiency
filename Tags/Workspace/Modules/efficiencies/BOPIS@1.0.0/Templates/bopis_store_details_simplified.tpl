<div class="bopis-storedetail-placeholder">
    <div class="bopis-storedetail">
        {{#if hasModel}}
            <div class="bopis-storedetail-storename">
                {{ translate model.name }}
            </div>
            <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="bopis-storedetail-address">
                <div itemprop="streetAddress">
                    {{ translate model.address1}}
                </div>
                <div itemprop="streetAddress">
                    {{ translate model.address2 }}
                </div>
                <div>
                    <span itemprop="addressLocality">
                        {{ translate model.city}}
                    </span>
                    {{#if model.state}} , {{/if}}
                    <span itemprop="addressRegion">
                        {{ translate model.state }}
                    </span>
                    {{#if model.zip}} , {{/if}}

                    <span itemprop="postalCode">{{ translate model.zip}}</span>
                </div>
                <div>
                    <span itemprop="addressCountry">{{ translate model.country}}</span>
                </div>
            </address>

            {{#if model.phone}}
                <div itemprop="telephone" content="{{ model.phone }}" class="bopis-storedetail-phone">
                    {{ model.phone }}
                </div>
            {{/if}}
            {{#if model.openingHours}}
                <div class="bopis-storedetail-openinghours">
                    {{{ translate model.openingHours }}}
                </div>
            {{/if}}
        {{/if}}

        {{#if showButton}}
            <div data-view="Profile.MyStore.StoreLocator"></div>
        {{/if}}
    </div>
    {{#if hasModel}}
    <div class="bopis-storedetail-map-view" >
        <div class="bopis-storedetail-map-placeholder {{#if isMapSizeSmall}}bopis-map-placeholder-sm{{else}}bopis-map-placeholder-md{{/if}}"
             data-view="StoreLocator.Map"></div>
    </div>
    {{/if}}
</div>