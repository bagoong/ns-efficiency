<div class="bopis-faqs-container">
    <a data-toggle="toggleAccordion" class="bopis-faqs-link-toggle-accordion">
        [ {{#if isExpandAll}}Hide {{else}}Show {{/if}}All Answers ]
    </a><br/><br/>
    {{#each faqsContent}}
        <div>{{ name }}</div>
        <div class="bopis-faqs-panel-group" data-toggle="accordion">
            {{#each questionList}}
                <div class="panel panel-default">
                    <div class="bopis-faqs-panel-border {{#unless ../../isExpandAll}}collapsed{{/unless}}" aria-expanded="false" data-toggle="collapse" data-target="#collapse{{internalid}}">
                        <h4 class="bopis-faqs-panel-title">
                            {{name}}
                            <i class="bopis-faqs-toggle-icon"></i>
                        </h4>
                    </div>
                    <div id="collapse{{internalid}}" aria-expanded="false" class="bopis-faqs-submenu collapse {{#if ../../isExpandAll}}in{{/if}}">
                        <div class="bopis-faqs-submenu-sub">
                            {{{content}}}
                        </div>
                    </div>
                </div>
            {{/each}}
        </div>
    {{/each}}
</div>