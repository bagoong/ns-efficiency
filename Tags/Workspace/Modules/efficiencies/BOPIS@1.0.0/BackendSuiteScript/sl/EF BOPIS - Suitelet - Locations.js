/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet

 */
define([
    '../libs/Bopis.Location',
    '../libs/Bopis.Location.Store',
    'N/error'
], function EFBOPISSuiteletLocations(
    BopisLocation,
    BopisLocationStore,
    errorAPI
) {
    'use strict';

    var handlers = {
        getStores: function getStores(context) {
            var parsedParams;
            if (context.request.method === 'GET') {
                parsedParams = {
                    subsidiary: parseInt(context.request.parameters.subsidiary, 10),
                    website: parseInt(context.request.parameters.website, 10)
                };
            }

            if (!parsedParams.subsidiary || !util.isNumber(parsedParams.subsidiary)) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Subsidiary missing'
                });
            }

            if (!parsedParams.website || !util.isNumber(parsedParams.website)) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Website missing'
                });
            }

            if (context.request.parameters.internalid) {
                parsedParams.internalid = parseInt(context.request.parameters.internalid, 10);
                if (!util.isNumber(parsedParams.internalid)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Wrong InternalId'
                    });
                }
            }

            if (context.request.parameters.urlcomponent) {
                parsedParams.urlcomponent = context.request.parameters.urlcomponent;
            }

            if (context.request.parameters.limit) {
                parsedParams.limit = parseInt(context.request.parameters.limit, 10);
                if (!util.isNumber(parsedParams.limit)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Limit must be numeric if present'
                    });
                }
            }

            context.response.write({
                output: JSON.stringify(BopisLocationStore.getStores(parsedParams) || {})
            });

        },
        getNearestStores: function getNearestStores(context) {
            var parsedParams;

            if (context.request.method === 'GET') {
                parsedParams = {
                    subsidiary: parseInt(context.request.parameters.subsidiary, 10),
                    website: parseInt(context.request.parameters.website, 10),
                    latitude: parseFloat(context.request.parameters.latitude),
                    longitude: parseFloat(context.request.parameters.longitude),
                    searchRadius: parseInt(context.request.parameters.searchRadius, 10),
                    limit: parseInt(context.request.parameters.limit, 10)

                };

                if (!parsedParams.subsidiary || !util.isNumber(parsedParams.subsidiary)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Subsidiary missing'
                    });
                }

                if (!parsedParams.website || !util.isNumber(parsedParams.website)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Website missing'
                    });
                }

                if (!parsedParams.latitude || !util.isNumber(parsedParams.latitude)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Latitude missing'
                    });
                }

                if (!parsedParams.longitude || !util.isNumber(parsedParams.longitude)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Longitude missing'
                    });
                }

                if (!parsedParams.searchRadius || !util.isNumber(parsedParams.searchRadius)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Radius missing'
                    });
                }

                if (!parsedParams.limit || !util.isNumber(parsedParams.limit)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Limit missing'
                    });
                }

                context.response.write({
                    output: JSON.stringify(BopisLocationStore.getStores(parsedParams))
                });
            }
        },
        getLocationsByType: function getLocationsByType(context) {
            var parsedParams;

            if (context.request.method === 'GET') {
                parsedParams = {
                    subsidiary: parseInt(context.request.parameters.subsidiary, 10),
                    locationType: parseInt(context.request.parameters.locationType, 10),
                    website: parseInt(context.request.parameters.website, 10),
                    splitByType: context.request.parameters.splitByType === 'true'
                };

                if (!parsedParams.subsidiary || !util.isNumber(parsedParams.subsidiary)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Subsidiary missing'
                    });
                }

                if (!parsedParams.website || !util.isNumber(parsedParams.website)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Website missing'
                    });
                }
                if (context.request.parameters.locationType) {
                    parsedParams.locationType = parseInt(context.request.parameters.locationType, 10);
                    if (
                        !util.isNumber(parsedParams.locationType) ||
                        !BopisLocation.LOCATION_TYPES[parsedParams.locationType - 1]
                    ) {
                        throw  errorAPI.create({
                            name: 'SSS_MISSING_REQD_ARGUMENT',
                            message: 'Bad value for Location Type'
                        });
                    }
                }

                // context.response.setHeader('Content-Type', 'application/json; charset=UTF-8');

                context.response.write({
                    output: JSON.stringify(BopisLocation.getLocationsByType(parsedParams))
                });
            }
        }
    };

    return {
        /*
         * This Suitelet needs to be Available without Login for bootstrapping purposes in shopping.environment.ssp
         */

        onRequest: function onRequest(context) {
            if (!context.request.parameters ||
                !context.request.parameters.handler ||
                !handlers[context.request.parameters.handler]) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Handler not specified'
                });
            }
            return handlers[context.request.parameters.handler](context);
        }
    };
});