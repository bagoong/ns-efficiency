<div class="controls checkout-fileupload-view">
    <h3 class="section-header checkout-fileupload-view-title">{{lblFile}}</h3>
    <div class="checkout-fileupload-view-content">
    {{#if file}}
    <div class="checkout-fileupload-view-option">
            <span><a target="_blank" data-action="preView-file" id="preView-file">{{file.filename}}&nbsp;&nbsp;</a></span>
            <input type="button" class="checkout-fileupload-view-remove" data-action="delete-file" data-internalid="{{internalid}}" value="{{lbldelete}}">
    </div>
    {{else}}
    <div class="checkout-fileupload-view-option">
        <form data-action="upload-form" data-type="file-upload" data-attribute-id="{{internalid}}">
            <fieldset>
                <div>
                   {{#if lblRequire}} <small>{{lblRequire}}<span class="checkout-fileupload-view-required-text">*</span></small>{{/if}}
                </div>
                <div>
                <input type="file" name="file" id="file{{internalid}}" >
                <input type="hidden" name="internalid" id="internalid" value="{{internalid}}">
                <input type="submit" class="checkout-fileupload-view-upload" name="submit" id="submit" value="{{lblSubmit}}">
                </div>
                <div>
                    <small class="msg{{internalid}}"></small>
                </div>
            </fieldset>
        </form>
    </div>
    {{/if}}
    </div>
</div>