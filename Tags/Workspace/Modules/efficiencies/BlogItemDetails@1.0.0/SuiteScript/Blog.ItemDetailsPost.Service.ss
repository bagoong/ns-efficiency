/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

function service(request)
{
    'use strict';
    var Application = require('Application');
    var method;
    var BlogItemDetails;
    var tagId;

    method = request.getMethod();
    BlogItemDetails = require('Blog.ItemDetailsPost.Model');
    tagId = request.getParameter('tagId');



    switch (method) {
    case 'GET':
        Application.sendContent(
           (BlogItemDetails.listBlogPost(tagId)),
            { 'cache': response.CACHE_DURATION_MEDIUM }
        );

        break;
    default:
                // methodNotAllowedError is defined in ssp library commons.js
        Application.sendError(methodNotAllowedError);
    }
}