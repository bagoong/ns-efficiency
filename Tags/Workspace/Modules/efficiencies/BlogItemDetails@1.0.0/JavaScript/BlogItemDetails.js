define('BlogItemDetails', [
    'ItemDetails.View',
    'Blog.ItemDetails.View',

    'Backbone.CompositeView',
    'Backbone.CollectionView',

    'Blog.ItemDetailsPost.Collection',
    'Blog.ItemDetailsTags.Collection',

    'blog_item_row.tpl',

    'Backbone',
    'underscore',
    'Utils'

], function BlogItemRelatedView(
    ItemDetailsView,
    BlogPostView,

    BackboneCompositeView,
    BackboneCollectionView,

    BlogItemDetailsCollection,
    BlogTagsCollection,

    rowTPL,
    Backbone,
    _
) {
    'use strict';
    var _me;
    var tagscollection;
    var filterTags;
    var splitTags;
    var splitTags2;

    ItemDetailsView.prototype.initialize =
    _.wrap(ItemDetailsView.prototype.initialize, function wrapProductFormInitialize(fn) {
        BackboneCompositeView.add(this);
        _me = this;
        this.collection = new BlogItemDetailsCollection();
        tagscollection = new BlogTagsCollection();
        fn.apply(this, _.toArray(arguments).slice(1));

        _.extend(ItemDetailsView.prototype.childViews, {
            'Blog.Post': function BlogItems() {
                tagscollection.fetch().done( function results(data) {
                    splitTags = _me.model.get('custitem_ef_blog_tags').split(', ');
                     splitTags2 = [];
                    _.each(splitTags, function(val) {
                        filterTags = _.findWhere(data, {name: val});
                        (filterTags) ? splitTags2.push(filterTags.recordid) : '';
                    });

                    if (filterTags) {
                        _me.collection.fetch({
                            data: {
                                tagId: splitTags2.join(',')
                            }
                        });
                    }
                });

                return new BlogPostView({
                    collection: this.collection,
                    application: this.application
                });
            }

        });

        this.on('afterViewRender', function afterViewRender() {
            this.$el
                .find('.item-details-product-review-content')
                .append('<div data-view="Blog.Post"></div>');
        });
    });
});