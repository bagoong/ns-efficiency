<div class="blog-items-collection-view-cell-span3">
    <div class="blog-item-cell-grid">
        <div class="blog-item-cell-grid-details">
            <a class="blog-item-cell-grid-title" href="{{url}}">
                <span itemprop="name">{{name}}</span>
            </a>
        </div>
    </div>
</div>