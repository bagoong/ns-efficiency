{{!
	� 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
{{#if isGiftWrappable}}
<div class="item-views-option-dropdown-gift-wrap" data-id="{{itemOptionId}}" data-type="option" data-cart-option-id="{{cartOptionId}}">
    {{#if isCheckboxMode}}
    <div class="item-views-option-dropdown-gift-wrap-label-wrapper">
        <label class="item-views-option-dropdown-gift-wrap-checkbox {{#if isActive}}active{{/if}} {{#unless isAvailable}}muted{{/unless}}">
            <input type="checkbox" name="{{cartOptionId}}" value="{{firstOption.internalid}}" {{#if isActive}}checked{{/if}} data-toggle="set-option" data-active="{{isActive}}"
                   data-available="{{isAvailable}}"/>
            <span>{{firstOption.label}}</span>
            {{#if gwModel}}
                {{#if gwModel.isValid}}
                    <div class="item-views-option-dropdown-gift-wrap-label-wrapper">
                        <span class="item-views-option-dropdown-gift-wrap-label">{{translate 'Gift Wrap Amount'}}:</span>
                        {{gwModel.price_formatted}}
                    </div>
                {{/if}}
            {{/if}}
        </label>
    </div>
    {{else}}
        <div class="item-views-option-dropdown-gift-wrap-label-wrapper">
            <span class="item-views-option-dropdown-gift-wrap-label">{{label}}</span>
            {{#if showSelectedOption}}
                <span class="item-views-option-dropdown-gift-wrap-label">:</span> {{selectedOption.label}}
            {{/if}}
        </div>

        <select name="{{cartOptionId}}" id="{{cartOptionId}}" class="item-views-option-dropdown-select" data-toggle="select-option">

            {{#each options}}
                {{#unless label}}
                    <option value="">
                        {{translate '--None--'}}
                    </option>
                {{/unless}}
                {{#if internalId}}
                    <option
                            class="{{#if isActive}}active{{/if}} {{#unless isAvailable}}muted{{/unless}}"
                            value="{{internalId}}"
                            {{#if isActive}}selected{{/if}}
                            data-active="{{isActive}}"
                            data-available="{{isAvailable}}">
                        {{label}}
                        {{#if gwModel}}
                            {{gwModel.price_formatted}}
                        {{/if}}
                    </option>
                {{/if}}
            {{/each}}

        </select>
    {{/if}}
</div>
{{/if}}