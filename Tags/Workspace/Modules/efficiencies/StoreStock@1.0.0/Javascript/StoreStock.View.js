define('StoreStock.View', [
    'Backbone',
    'Backbone.CompositeView',

    'jQuery',
    'underscore',

    'ItemOptionsHelper',
    'ItemDetails.Model',

    'Store.Bootstrapped.Collection',
    'StoreStock.Collection',

    'StoreStock.Detail.View',

    'store_stock_store_selector.tpl'
], function StoreStockView(
    Backbone,
    BackboneCompositeView,
    jQuery,
    _,

    ItemOptionsHelper,
    ItemDetailsModel,

    StoreLocation,
    StoreStockCollection,

    StoreStockDetailView,

    storeStockStoreSelectortpl
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockStoreSelectortpl,

        events: {
            'change select[data-toggle="state-selector"]': 'filterStoreList',
            'change select[data-toggle="store-selector"]': 'showStockDetail'
        },

        initialize: function initialize() {
            var matrixChildren = this.model.getSelectedMatrixChilds();

            BackboneCompositeView.add(this);

            this.stores = StoreLocation.getInstance();
            this.collection = new StoreStockCollection();
            this.selectedLocationId = null;
            this.selectedState = null;
            this.filteredStores = this.stores;

            this.itemModel = matrixChildren.length === 1 ? matrixChildren[0] : this.model;

            this.listenTo(this.collection, 'sync', this.render);
        },

        childViews: {
            'StoreStock.Detail': function childViewStoreStockDetail() {
                var locationInventory = this.collection.findWhere({locationId: this.selectedLocationId});
                return new StoreStockDetailView({
                    hasLocation: this.selectedLocationId !== null,
                    model: locationInventory,
                    application: this.options.application
                });
            }
        },

        getContext: function getContext() {
            var self = this;

            return {
                isSelectionComplete: this.model.isSelectionComplete(),
                hasStoreSelected: !!this.selectedLocationId,
                isDisplayAsBadge: this.options.application.getConfig('storeStock.isDisplayAsBadge'),
                enableStoreFilterByState: this.options.application.getConfig('storeStock.enableStoreFilterByState'),
                state: this.stores.map(function mapState(store) {
                    return {
                        name: store.get('state'),
                        isSelected: self.selectedState === store.get('state')
                    };
                }),
                locations: this.filteredStores.map(function mapLocations(store) {
                    return {
                        locationId: store.get('location'),
                        name: store.get('name'),
                        isSelected: self.selectedLocationId === store.get('location')
                    };
                })
            };
        },

        showStockDetail: function showStockDetail(e) {
            this.selectedLocationId = jQuery(e.target).val();

            this.collection.fetch({
                data: {
                    id: this.itemModel.id,
                    store: this.selectedLocationId
                }
            });
        },

        filterStoreList: function filterStoreList(e) {
            this.selectedState = jQuery(e.target).val();
            if (this.selectedState !== '') {
                this.filteredStores = this.stores.where({state: this.selectedState});
            }else {
                this.filteredStores = this.stores;
            }

            this.selectedLocationId = null;
            this.render();
        }
    });
});