<div class="store-stock-total-container">
    {{#if isDisplayAsBadge}}
        <span class="{{ badgeClass }}">{{ translate badgeText }}</span>
    {{else}}
        {{ translate "$(0) items in stock" quantityAvailable }}
    {{/if}}
</div>
