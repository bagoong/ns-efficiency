<div class="store-stock-container">
    {{#if isSelectionComplete}}
        <div>
            <label class="store-stock-store-label"><strong>{{ translate 'CHECK STORE STOCK' }}</strong></label>
            <div>
                {{#if enableStoreFilterByState}}
                    <select data-toggle="state-selector"  class="store-stock-state-list">
                        <option value="">{{ translate '-- Select State --' }}</option>0
                        {{#each state}}
                        <option value="{{name}}" {{#if isSelected}}selected{{/if}}>
                            {{name}}
                        </option>
                        {{/each}}
                    </select>
                {{/if}}
                <select data-toggle="store-selector"  class="store-stock-store-list">
                    <option value="">{{ translate '-- Select Store --' }}</option>0
                    {{#each locations}}
                        <option value="{{locationId}}" {{#if isSelected}}selected{{/if}}>
                            {{name}}
                        </option>
                    {{/each}}
                </select>
            </div>
        </div>
        {{#if hasStoreSelected }}
            <div data-view="StoreStock.Detail"></div>
        {{/if}}
    {{else}}
        <p class="store-stock-help">
            <i class="store-stock-help-icon"></i>
            <span class="store-stock-help-text">{{ translate 'Please select options to display store stock'}}</span>
        </p>
    {{/if}}
</div>