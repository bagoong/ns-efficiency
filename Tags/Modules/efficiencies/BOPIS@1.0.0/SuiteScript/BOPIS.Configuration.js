define('BOPIS.Configuration', [
    'InventoryDisplay.Configuration',
    'Configuration',
    'Application',
    'underscore'
], function InStorePickupConfiguration(
    InventoryDisplayConfiguration,
    GlobalConfiguration,
    Application,
    _
) {
    'use strict';
    var Configuration = {
        // enable the feature
        enabled: true,
        ITEM_TYPES_WITH_INVENTORY: InventoryDisplayConfiguration.ITEM_TYPES_WITH_INVENTORY,
        // The mark on every address that is not really a customer address but a store pickup location
        storeAddressMark: '{{STORE}}',
        // The name (exact match) of the shipping method that should have value 0 to be used on orders with BOPIS
        bopisShippingMethodName: 'BOPIS'
    };

    _.extend(Configuration, {
        get: function get() {
            return this;
        }
    });

    _.extend(GlobalConfiguration, {
        BOPIS: Configuration
    });

    return Configuration;
});