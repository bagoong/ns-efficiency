define('BOPIS.OrderLine.Model', [
    'OrderLine.Model'
], function OrderLineModelInStorePickup(OrderLineModel) {
    'use strict';

    var originalFn = OrderLineModel.prototype.toJSON;
    OrderLineModel.prototype.toJSON = function toJSON() {
        var originalReturn = originalFn.apply(this, arguments);
        originalReturn.deliverymethod = this.attributes.deliverymethod;
        return originalReturn;
    };
});