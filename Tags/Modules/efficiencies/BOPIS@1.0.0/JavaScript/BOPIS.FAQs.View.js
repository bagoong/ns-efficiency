define('BOPIS.FAQs.View', [
    'Backbone',
    'BOPIS.FAQs.Collection',
    'bopis_faqs.tpl',
    'underscore'
], function BopisFAQsView(
    Backbone,
    BopisFAQsCollection,
    bopisFAQtemplate,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: bopisFAQtemplate,
        title: _('Buy Online Pick Up in Store FAQS').translate(),
        page_header: _('Buy Online Pick Up in Store FAQS').translate(),
        events: {
            'click [data-toggle="toggleAccordion"]': 'toggleAllAccordion'
        },
        expandAll: false,
        initialize: function initialize() {
            this.collection = new BopisFAQsCollection();
            this.collection.fetch();
            this.listenTo(this.collection, 'sync', this.render);
        },

        toggleAllAccordion:function toggleAllAccordion() {
            this.expandAll = this.expandAll ? false : true;
            this.render();
        },

        getContext: function getContext() {
            return {
                isExpandAll: this.expandAll,
                faqsContent: this.collection
            };
        }
    });
});
