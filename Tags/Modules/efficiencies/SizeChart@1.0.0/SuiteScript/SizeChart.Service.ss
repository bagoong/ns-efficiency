function service(request) {
    'use strict';

    var Application = require('Application');
    var method = request.getMethod();
    var id = request.getParameter('internalid');
    var language = request.getParameter('lang');
    //  SizeChart model is defined on ssp library Models.js
    var SizeChart = require('SizeChart.Model');
    var content;

    try {
        // Only can get, modify, update or delete if you are logged in
        switch (method) {
        case 'GET':
            // If the id exist, sends the response of SizeChart.get(id), else sends the response of []
            content = SizeChart.get(id, language);
            Application.sendContent( content, { cache: content.volatility } );
            break;

        default:
            // methodNotAllowedError is defined in ssp library commons.js
            Application.sendError(methodNotAllowedError);
        }
    } catch(e) {
        Application.sendError(e);
    }
}