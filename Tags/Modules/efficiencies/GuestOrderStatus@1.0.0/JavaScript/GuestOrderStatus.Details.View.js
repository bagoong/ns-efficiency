define('GuestOrderStatus.Details.View', [
    'Backbone',
    'guestorderstatus_details.tpl'
], function OrderStatusDetailsView(
    Backbone,
    orderStatusDetailsTpl
) {
    'use strict';
    return Backbone.View.extend({
        template: orderStatusDetailsTpl,

        render: function render() {
            if (!this.model.isNew()) {
                this._render();
            }
            return this;
        },
        getContext: function getContext() {

            var details = [{
                name: 'Order #',
                value: this.model.get('orderid'),
                render: !!this.model.get('orderid')
            }, {
                name: 'Status',
                value: this.model.get('status'),
                render: !!this.model.get('status')
            }, {
                name: 'Tracking Numbers',
                value: this.model.get('trackingnumbers'),
                render: !!this.model.get('trackingnumbers')
            }, {
                name: 'Ship Method',
                value: this.model.get('shipmethod'),
                render: !!this.model.get('shipmethod')
            }, {
                name: 'Email',
                value: this.model.get('email'),
                render: !!this.model.get('email')
            }];

            return {
                details: details
            };
        }
    });
});

