define('CheckoutFileUpload.Model', [
    'Backbone',
    'underscore'

], function CheckoutFileUploadModel(
    Backbone,
    _
) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: _.getAbsoluteUrl('services/CheckoutFileUpload.Service.ss')
    });
});