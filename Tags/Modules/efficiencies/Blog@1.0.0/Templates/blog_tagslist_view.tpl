<div id="content"><div>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="blog-title">
                    <h3 itemprop="name" class="panel-title">{{ translate 'All Tags' }}</h3>
                </div>
                <div class="blog-details-divider-desktop"></div>
                <div class="blog-items-collection-view-row" data-view="Blog.Tags.Content">

                </div>
            </div>
            <div class="blog-details-divider-desktop"></div>
            <div>
                <a href="/blog" class="blog-navigation-item-optionlist-extra-button">{{ translate 'Back to Blog'}}</a>
            </div>
        </div>
    </div>
</div>
