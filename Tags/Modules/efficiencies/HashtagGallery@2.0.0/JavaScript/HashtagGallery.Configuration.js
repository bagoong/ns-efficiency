define('HashtagGallery.Configuration', function HashtagGalleryConfiguration() {
    'use strict';
    return {
        displayUser: true,
        displayCaption: true,
        minimumToDisplayModule: 0 // 0 shows even if no photos
    };
});