function service(request) {
    'use strict';

    var Application = require('Application');
    var HashtagGallery = require('HashtagGallery.Model');
    var _ = require('underscore');
    var method = request.getMethod();
    var result;
    var page;
    var hashtags = [];

    try {
        switch (method) {
        case 'GET':
            page = request.getParameter('page');
            hashtags = (request.getParameter('hashtags') || '').split(',');
            hashtags = _.compact(_.unique(_.map(hashtags,
                function map(h) {
                    return h && h.toString && h.toString() && h.toString().trim();
                }
            )));

            if (!hashtags.length) {
                Application.sendError(methodNotAllowedError);
            }
            result = HashtagGallery.search(hashtags, page);

            // send either the individual review, or the search result
            Application.sendContent(result, {'cache': response.CACHE_DURATION_MEDIUM});

            break;

        default:
            // methodNotAllowedError is defined in ssp library commons.js
            Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}