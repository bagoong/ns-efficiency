/*
     StoreLocator.Router - for routing your applications URL's
     Required modules [Backbone, Store.Model, Store.Collection, Store.Views.StoreLocator, Store.Views.Store]
*/
define('StoreLocator.Router', [
    'Backbone',
    'Store.Model',
    'Store.Collection',
    'Store.Views.StoreLocator',
    'Store.View.StoreDetail',
    'Store.View.StoreList'
], function StoreLocatorRouter(Backbone, Model, Collection, StoreLocatorView, StoreDetailView, StoreListView) {
    'use strict';

    return Backbone.Router.extend({
        routes: {
            'stores': 'storeLocator',
            'stores?*': 'storeLocator',
            'storelist': 'listStore'
        },

        initialize: function initialize(application) {
            this.application = application;
            // Create routes for store page if enable in config
            if (this.application.getConfig('storeLocator.storePage')) {
                this.route('stores/:urlcomponent', 'store');
                this.route('stores/:urlcomponent?*', 'store');
            }

            this.application.history = [{text: 'Store Locator', href: '/stores'}];
        },

        storeLocator: function storeLocator() {
            var collection = {};
            var view = {};

            collection = new Collection(null);

            view = new StoreLocatorView({
                application: this.application,
                collection: collection
            });

            view.showContent();
        },

        store: function store(urlcomponent) {
            var model = new Model();

            var view = new StoreDetailView({
                application: this.application,
                model: model
            });

            model.fetch({
                data: {
                    urlcomponent: urlcomponent
                },
                killerid: this.application.killerid,
                success: function success() {
                    view.showContent();
                }
            });
        },

        listStore: function listStore() {
            var collection = {};
            var self = this;
            var view = {};

            collection = new Collection(null, {
                comparator: this.application.getConfig('storeLocator.list.defaultSort')
            });

            view = new StoreListView({
                application: this.application,
                collection: collection
            });

            collection.fetch({
                killerId: self.application.killerId
            }).success(function success() {
                view.showContent();
            });
        }
    });
});