define('BOPIS.OrderWizard.Router', [
    'OrderWizard.Router',
    'underscore'
], function BOPISOrderWizardRouter(
    OrderWizardRouter,
    _
) {
    'use strict';
    _.extend(OrderWizardRouter.prototype, {
        shouldOfferBOPIS: function shouldOfferBOPIS() {
            return this.model.hasPickUpAbleLines();
        },
        shouldOfferShipping: function shouldOfferShipping() {
            return this.model.hasShippableLines();
        },
        hasItemsToShip: function hasItemsToShip() {
            return this.model.getLinesForShipping().length > 0;
        },
        hasItemsToPick: function hasItemsToPick() {
            return this.model.getLinesForPicking().length > 0;
        }
    });
});