define('BOPIS.OrderWizard.Module.CartItemsBase', [
    'Wizard.Module',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'ItemViews.Cell.Navigable.View',

    'order_wizard_cartitems_module.tpl',

    'jQuery',
    'underscore',
    'Utils'
],
function BOPISOrderWizardModuleCartItemsBase(
    WizardModule,
    BackboneCompositeView,
    BackboneCollectionView,
    ItemViewsCellNavigableView,
    orderWizardCartitemsModuleTpl,
    jQuery,
    _
) {
    'use strict';

    return WizardModule.extend({
        template: orderWizardCartitemsModuleTpl,
        cartItemsTitle: '',
        accordionID: '',
        getLineCollection: jQuery.noop,
        isReady: true,

        render: function render() {
            this.application = this.wizard.application;
            this.profile = this.wizard.options.profile;
            this.options.application = this.wizard.application;


            if (this.state === 'present') {
                this._render();
                this.trigger('ready', true);
            }
        },

        initialize: function initialize() {
            var self = this;
            BackboneCompositeView.add(this);
            WizardModule.prototype.initialize.apply(this, arguments);

            this.wizard.model.on('promocodeUpdated', function onPromoUpdated() {
                self.render();
            });

            // for accordion
            this.wizard.application.getLayout().on('afterRender', function afterRender() {
                self.$el
                    .find('.order-wizard-cartitems-module-accordion-head-toggle')
                    .append(' for ' + self.cartItemsTitle)
                    .attr('data-target', '#' + self.accordionID);

                self.$el
                    .find('.order-wizard-cartitems-module-accordion-body')
                    .attr('id', self.accordionID);
            });
        },

        childViews: {
            'Items.Collection': function ItemsCollection() {
                return new BackboneCollectionView({
                    collection: this.getLineCollection(),
                    childView: ItemViewsCellNavigableView,
                    viewsPerRow: 1,
                    childViewOptions: {
                        navigable: !this.options.hide_item_link,

                        detail1Title: _('Qty:').translate(),
                        detail1: 'quantity',

                        detail2Title: _('Unit price:').translate(),
                        detail2: 'rate_formatted',

                        detail3Title: _('Amount:').translate(),
                        detail3: 'amount_formatted'
                    }
                });
            }
        },

        isActive: function isActive() {
            return this.getLineCollection() && this.getLineCollection().length > 0;
        },

        getContext: function getContext() {
            var lines = this.getLineCollection();
            var itemCount = _.countItems(lines);

            return {
                model: this.model,
                itemCountGreaterThan1: itemCount > 1,
                itemCount: itemCount,
                showOpenedAccordion: _.result(this.options || {}, 'showOpenedAccordion', false),
                showEditCartButton: !this.options.hide_edit_cart_button,
                showHeaders: !this.options.hideHeaders,
                showMobile: this.options.showMobile
            };
        }
    });
});