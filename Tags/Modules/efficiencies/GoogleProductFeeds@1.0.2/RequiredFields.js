define('RequiredFields', function(){
    return {
        list: [
           'id',
           'title',
           'description',
           'image',
           'category',
           'link',
           'condition',
           'availability',
           'price',
           'brand',
           'gtin',
           'mpn',
           'gender',
           'age group',
           'shop code'
          ]
    };
});