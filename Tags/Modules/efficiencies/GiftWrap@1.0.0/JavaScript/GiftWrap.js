define('GiftWrap', [
    'ItemDetails.Model.GiftWrap',
    'ItemDetails.View.GiftWrap',
    'GiftWrap.SelectedOption.View',
    'GiftWrap.Item.Options.View',
    'GiftWrap.Configuration',
    'GiftWrap.ItemCollection',
    'GiftWrap.OrderLineItemModel',
    'underscore',
    'SC.Configuration'
], function GiftWrap(
    ItemDetailsModelGiftWrap,
    ItemDetailsViewGiftWrap,
    GiftWrapSelectedOptionView,
    GiftWrapItemOptionView,
    GiftWrapConfiguration,
    ItemCollection,
    OrderLineItem,
    _,
    Configuration
) {
    'use strict';


    Configuration.itemOptions = GiftWrapConfiguration.addGiftWrapOptions(Configuration.itemOptions || []);
    Configuration.searchApiMasterOptions = GiftWrapConfiguration.addToSearchApiMasterOptions(
        Configuration.searchApiMasterOptions
    );

    _.extend(Configuration, {GiftWrapConfig: GiftWrapConfiguration.GiftWrapConfig});

    return {
        GiftWrapSelectedOptionView: GiftWrapSelectedOptionView,
        ItemDetailsModelGiftWrap: ItemDetailsModelGiftWrap,
        ItemDetailsViewGiftWrap: ItemDetailsViewGiftWrap,
        GiftWrapConfiguration: GiftWrapConfiguration,
        ItemCollection: ItemCollection,
        GiftWrapItemOptionView: GiftWrapItemOptionView
    };
});