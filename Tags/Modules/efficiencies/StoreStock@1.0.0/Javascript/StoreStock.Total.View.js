/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.Total.View', [
    'Backbone',
    'store_stock_total.tpl'
], function StoreStockTotalView(
    Backbone,
    storeStockTotaltpl
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockTotaltpl,

        initialize: function initialize() {
            var matrixChildren = this.model.getSelectedMatrixChilds();
            this.itemModel = matrixChildren.length === 1 ? matrixChildren[0] : this.model;
        },

        getContext: function getContext() {
            var labels = this.options.application.getConfig('storeStock.badgeLabels');
            var quantityAvailable = this.itemModel.get('quantityavailable');
            var label = labels[ quantityAvailable > 0 ? 1 : 2 ];

            return {
                quantityAvailable: quantityAvailable,
                isDisplayAsBadge:
                    quantityAvailable > 0 ?
                        this.options.application.getConfig('storeStock.isDisplayAsBadge')
                        : true,
                badgeClass: label && label.htmlClass,
                badgeText: label && label.text
            };
        }
    });
});