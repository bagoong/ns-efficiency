/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.Configuration', [
    'underscore',
    'Utils'
], function StoreStockConfiguration(
    _
) {
    'use strict';

    return {
        isDisplayAsBadge: false,
        enableStoreFilterByState: false,
        badgeLabels: {
            1: {text: _('In Stock').translate(), htmlClass: 'store-stock-bg-in-stock'},
            2: {text: _('Out of Stock').translate(), htmlClass: 'store-stock-bg-out-stock'}, 
            3: {text: _('Low Stock').translate(), htmlClass: 'store-stock-bg-limited-stock'}
        }
    };
});

