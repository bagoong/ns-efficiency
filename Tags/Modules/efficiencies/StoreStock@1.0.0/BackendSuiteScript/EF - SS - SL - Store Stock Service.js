/**
 * Store Stock
 *
 * Version    Date            Author           Remarks
 * 1.00       25 Aug 2015     cparayno
 *
 */

var IN_STOCK = 1;
var OUT_OF_STOCK = 2;
var LIMITED_STOCK = 3;

function getStockValue(quantityAvailable, preferredStockLevel, safetyStockLevel) {
    'use strict';
    var stock = 0;
    var badge = 0;
    var valueType = 0;

    if (preferredStockLevel !== '') {						// return preferred stock level to display in PDP
        if (quantityAvailable >= preferredStockLevel) {		// e.g. More than [preferered stock level] items in stock
            valueType = 1;
            stock = preferredStockLevel;
            badge = IN_STOCK;
        }
    }

    // if limited stock, display the actual quantity available in PDP
    if (safetyStockLevel !== '') {
        // e.g. [quantity available] items in stock
        if (quantityAvailable < safetyStockLevel) {
            stock = quantityAvailable;
            badge = LIMITED_STOCK;
        }
    }

    if (preferredStockLevel !== '' && safetyStockLevel !== '') {
        if (quantityAvailable < preferredStockLevel && quantityAvailable >= safetyStockLevel) {
            stock = 0;
            badge = OUT_OF_STOCK;
        }
    }

    if (stock === 0) {
        valueType = 0;
        if (quantityAvailable > 0) {
            stock = quantityAvailable;
            badge = IN_STOCK;
        } else {
            stock = 0;
            badge = OUT_OF_STOCK;
        }
    }

    return [stock, badge, valueType];
}

function getStoreStock(id, storeId) {
    'use strict';
    var items;
    var result = [];
    var item = '';
    var quantityAvailable = 0;
    var preferredStockLevel = 0;
    var safetyStockLevel = 0;
    var itemDetail = {};
    var i;
    var stockValue;


    var filters = [
        new nlobjSearchFilter('isonline', null, 'is', 'T'),
        new nlobjSearchFilter('locationquantityavailable', null, 'greaterthan', 0),
        new nlobjSearchFilter('makeinventoryavailablestore', 'inventorylocation', 'is', 'T')
    ];
    var columns = [
        new nlobjSearchColumn('internalid'),
        new nlobjSearchColumn('displayname'),
        new nlobjSearchColumn('internalid', 'inventorylocation'),
        new nlobjSearchColumn('locationquantityavailable'),
        new nlobjSearchColumn('locationpreferredstocklevel'),
        new nlobjSearchColumn('locationsafetystocklevel')
    ];

    if (id) {
        filters.push(new nlobjSearchFilter('internalid', null, 'anyof', id.split(',')));
    }

    if (storeId) {
        filters.push(new nlobjSearchFilter('internalid', 'inventorylocation', 'anyof', storeId.split(',')));
    }

    try {
        items = nlapiSearchRecord('item', null, filters, columns);
        if (items) {
            for (i = 0; i < items.length; i++) {
                item = items[i];
                quantityAvailable = item.getValue('locationquantityavailable');
                preferredStockLevel = item.getValue('locationpreferredstocklevel');
                safetyStockLevel = item.getValue('locationsafetystocklevel');

                itemDetail = {
                    itemId: parseInt(item.getValue('internalid'), 10),
                    locationId: item.getValue('internalid', 'inventorylocation')
                };

                stockValue = getStockValue(quantityAvailable, preferredStockLevel, safetyStockLevel);

                itemDetail.stock = stockValue[0];
                itemDetail.badge = stockValue[1];

                if (stockValue[2] === 1) {
                    itemDetail.isValuePreferred = true;
                }

                result.push(itemDetail);
            }
        } else {
            // if no stock available on the selected store, return Out of Stock badge
            result.push({
                itemId: id,
                locationId: storeId,
                stock: 0,
                badge: OUT_OF_STOCK
            });
        }
    } catch (e) {
        result = {status: e.message};
    }

    return result;
}


/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function service(request, response) {
    'use strict';
    var result = {};
    var id;
    var storeId;

    try {
        if (request.getMethod() === 'GET') {
            id = request.getParameter('id');
            storeId = request.getParameter('stores');

            result = getStoreStock(id, storeId);
        }
    } catch (e) {
        result = {status: e.message};
    }

    response.setContentType('JSON');
    response.write(JSON.stringify(result));
}