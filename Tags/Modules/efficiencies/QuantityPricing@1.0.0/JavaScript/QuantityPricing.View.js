define('QuantityPricing.View', [
    'Backbone',
    'quantity_pricing_rows.tpl',
    'jQuery',
    'underscore'

], function QuantityPricingItemDetailsView(
    Backbone,
    quantityPricingTpl,
    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: quantityPricingTpl,
        getContext: function getContext() {
            return {
                pricingSchedule: this.model.get('_priceScheduleForTable', true)
            };
        }
    });
});
