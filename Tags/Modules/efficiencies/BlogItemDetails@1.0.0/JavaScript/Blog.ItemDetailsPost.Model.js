define('Blog.ItemDetailsPost.Model', [
    'Backbone',
    'underscore'
], function BlogPostModel(
    Backbone,
    _
) {
    'use strict';

    return Backbone.Model.extend({
        urlRoot: _.getAbsoluteUrl('services/Blog.ItemDetailsPost.Service.ss')
    });
});