define('InventoryDisplay.Global.Configuration', [
    'Configuration',
    'underscore'
], function CaseFileUploadConfiguration(
    Configuration,
    _
) {
    'use strict';

    var InventoryDisplayConfiguration = {
        /*
         Only InventoryPart have Correct information about quantity available
         Kits have a weird calculation that seems buggy
         */
        supportedItemTypes: ['InvtPart'],
        inStockMessages: {
            inStock: 'In Stock',
            dropShip: 'In Stock ',
            specialOrder: 'In Stock',
            backOrder: 'On Backorder'
        }
    };

    _.extend(InventoryDisplayConfiguration, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'InventoryDisplayGlobal_config',
        model: 'InventoryDisplay.Global.Configuration',
        call: 'get'
    });

    return InventoryDisplayConfiguration;
});