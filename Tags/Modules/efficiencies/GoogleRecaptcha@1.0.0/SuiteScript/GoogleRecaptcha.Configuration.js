define('GoogleRecaptcha.Configuration', [
    'Configuration',
    'underscore'
], function GoogleRecaptchaConfiguration(
    Configuration,
    _
) {
    'use strict';

    var GoogleRecaptchaConfig = {
        googleRecaptchaWidget: {
            // sitekey is serverkey is a sample. Please create a new one please follow this link. https://developers.google.com/recaptcha/intro?hl=en
            sitekey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
            serverkey: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe', // serverkey stands for secret key.

            verifyUrl: 'https://www.google.com/recaptcha/api/siteverify',
            theme: 'light', // can be light/dark
            apiUrl: 'https://www.google.com/recaptcha/api.js'
        }
    };

    _.extend(GoogleRecaptchaConfig, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'GoogleRecaptcha_config',
        model: 'GoogleRecaptcha.Configuration',
        call: 'get'
    });

    return GoogleRecaptchaConfig;
});