// @module Profile.MyStore
define('Profile.MyStore.StoreLocator.View', [
    'Backbone',
    'Store.Collection',
    'Profile.Model',
    'Store.Views.StoreLocator',
    'Profile.MyStore.ChooseStore.View',
    'profile_mystore_show_storelocator.tpl',
    'underscore',
    'jQuery',
    'jQuery.scPush'
], function ProfileMyStoreChooseStoreView(
    Backbone,
    Stores,
    Profile,
    StoreLocatorView,
    ProfileMyStoreButtonView,
    storeLocatorTpl,
    _,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: storeLocatorTpl,

        events: {
            'click [data-action="show-choose-store"]': 'showStoreLocator'
        },

        initialize: function initialize() {
            var self = this;
            this.once('afterViewRender', function afterViewRender() {
                _.defer(function deferred() {
                    self.$el.find('[data-action="pushable-map' + self.cid + '"]').scPush({target: 'tablet'});
                });
            });
        },


        getContext: function getContext() {
            return {
                btnText: this.model && this.model.get('internalid') ? 'Change Store' : 'Select Store',
                viewID: this.cid
            };
        },

        showStoreLocator: function showStoreLocator(e) {
            var showIn = jQuery(e.target).data('showin');
            var chooseStoreView;
            var storeLocatorView = new StoreLocatorView({
                application: this.options.application,
                collection: new Stores(null),
                isInMyStore: true,
                storeSearchText: ''
            });
            var modalTitle = (this.options && this.options.title) || 'Choose Store';

            storeLocatorView.title = _(modalTitle).translate();
            storeLocatorView.modalClass = 'profile-mystore-modal-storelocator';

            this.listenTo(storeLocatorView.eventBus, 'infoWindowRendered',
                _.bind( function appendChooseStoreButton(store) {
                    chooseStoreView = new ProfileMyStoreButtonView({
                        store: store,
                        application: this.options.application
                    });

                    chooseStoreView.setElement('#main #bot-cont').render();
                }, this)
            );

            if ( showIn === 'modal') {
                this.options.application.getLayout().showInModal(storeLocatorView);
            } else if ( showIn === 'pushpane') {
                storeLocatorView.setElement('*[data-view="SC.PUSHER.MAP"]').render();
            }
        }
    });
});