<a href="{{link}}" target="_blank"><img src="{{lowresasset}}" alt="{{username}} {{caption}}"></a>
{{#if displayUser}}
<div class="hashtag-gallery-user">
    {{#if userpic}}<div class="hashtag-gallery-user-pic"><img src="{{userpic}}" alt="{{username}}"></div>{{/if}}
    <div class="hashtag-gallery-user-link"><a href="https://instagram.com/{{username}}/" target="_blank">{{username}}</a></div>
</div>
{{/if}}
{{#if displayCaption}}
<small>{{caption}}</small>
{{/if}}
