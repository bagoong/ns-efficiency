// @module InventoryDisplay
define('InventoryDisplay.Status.Pickup.View', [
    'InventoryDisplay.Status.View',

    'Profile.Model',
    'Profile.MyStore.Model',
    'InventoryDisplay.InventoryDetailsHashMap.Model',
    'Store.Collection',

    'Store.Views.StoreLocator',
    'Profile.MyStore.ChooseStore.View',

    'underscore',
    'jQuery'
], function InventoryDisplayStatusPickupView(
    InventoryDisplayStatusView,

    Profile,
    ProfileMyStoreModel,
    InventoryDetailsHashMap,
    StoreCollection,

    StoreLocatorView,
    ProfileMyStoreButtonView,
    _,
    jQuery
) {
    'use strict';

    return InventoryDisplayStatusView.extend({
        orderType: 'pickup',
        events: {
            'click [data-action="show-storelocator"]': 'showStoreLocator'
        },

        initialize: function initialize() {
            this.storeStockHash = new InventoryDetailsHashMap();

            if (this.isInventoryItem()) {
                this.shouldRender = true;
            }

            this.listenTo(ProfileMyStoreModel.getInstance(), 'change', jQuery.proxy(this.changeStockModel, this));
            InventoryDisplayStatusView.prototype.initialize.apply(this, arguments);
        },

        fetchStockData: function fetchStockData() {
            var self = this;
            var myStore = ProfileMyStoreModel.getInstance();

            jQuery.when(Profile.getPromise()).then(function profileResolved() {
                if (myStore.get('internalid')) {
                    self.storeStockHash.fetch({
                        data: {
                            internalid: self.model.get('internalid'),
                            locationType: self.getModuleConfig().LOCATION_TYPE_STORE,
                            stores: myStore.get('internalid')
                        }
                    }).success(function success() {
                        self.changeStockModel();
                    });;
                }
            });
        },

        getMyStore: function getMyStore() {
            return ProfileMyStoreModel.getInstance();
        },

        changeStockModel: function changeStockModel() {
            var self = this;
            var myStore = ProfileMyStoreModel.getInstance();

            this.hasFetchedStockData = true;
            this.stockModel = this
                            .storeStockHash
                            .getStockForItemLocation(this.model.get('internalid'), myStore.get('internalid'));

            _.defer(function deferred() {
                self.once('afterViewRender', function afterViewRender() {
                    self.$el.find('[data-action="pushable-map' + self.cid + '"]').scPush({target: 'tablet'});
                });

                self.render();
            });
        },

        showStoreLocator: function showStoreLocator(e) {
            var isOpenInModal = this.options.application.getLayout().$containerModal;
            var showIn = jQuery(e.target).data('showin');
            var storeSearchText = this.$('[name=inventory-display-input-store]').val();
            var storeLocatorView = new StoreLocatorView({
                application: this.options.application,
                collection: new StoreCollection(null),
                isInStoreStock: true,
                itemId: this.model.id,
                storeSearchText: storeSearchText
            });
            storeLocatorView.title = _('Choose Store').translate();
            storeLocatorView.modalClass = 'inventory-display-modal-storelocator';

            // fetch store stock after store collection
            this.listenTo(storeLocatorView.collection, 'collectionRendered', _.bind(this.renderStockInStoreList, this));

            // append 'Choose Store' button in google map info window popup
            this.listenTo(storeLocatorView.eventBus, 'infoWindowRendered',
                _.bind( function appendChooseStoreButton(store) {
                    var chooseStoreView = new ProfileMyStoreButtonView({
                        store: store,
                        application: this.options.application,
                        itemDetailsView: this.options.itemDetailsView,
                        isOpenInModal: isOpenInModal
                    });

                    chooseStoreView.setElement('*[id="bot-cont"]').render();
                }, this));

            if ( showIn === 'modal') {
                this.options.application.getLayout().showInModal(storeLocatorView);
            } else if ( showIn === 'pushpane' ) {
                storeLocatorView.setElement('*[data-view="SC.PUSHER.MAP"]').render();
            }
        },

        renderStockInStoreList: function renderStockInStoreList(storeCollection) {
            var self = this;
            var stockView;
            var storeLocationIDs = storeCollection.map(function mapStoreLocationIds(store) {
                return store.get('internalid');
            });

            if (storeLocationIDs.length > 0) {
                this.storeStockHash.fetch({
                    data: {
                        internalid: self.model.id,
                        locationType: self.getModuleConfig().LOCATION_TYPE_STORE,
                        stores: _.compact(storeLocationIDs).join(',')
                    }
                }).success(function success() {
                    var stockForItem = self.storeStockHash.getAllLocationsByItem(self.model.id);

                    _.each(stockForItem, function appendToStoreLocator(modelStoreStock) {
                        var parentElement =
                            '*[data-view="StoreStock.Status.View' + modelStoreStock.get('location') + '"]';

                        stockView = new InventoryDisplayStatusView({
                            stockModel: modelStoreStock,
                            application: self.options.application
                        });

                        stockView.setElement(parentElement).render();
                    });
                });
            }
        }
    });
});