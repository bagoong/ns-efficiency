/**
 * Created by pzignani on 2/9/16.
 */
define('InventoryDisplay.InventoryDetailsHashMap.Model', [
    'Backbone',
    'Backbone.CachedModel',
    'underscore',
    'Utils'
], function InventoryDisplayInventoryDetailsHashMapModel(
    Backbone,
    BackboneCachedModel,
    _
) {
    'use strict';

    return BackboneCachedModel.extend({
        urlRoot: _.getAbsoluteUrl('services/InventoryDisplay.Service.ss'),

        getAllLocationsByItem: function getAllLocationsByItem(internalid) {
            var stockHash = this.get(internalid);
            return _.values(stockHash);
        },

        getStockForItemLocation: function getStockForItemLocation(internalid, location) {
            return this.get(internalid) ? this.get(internalid)[location] : null;
        },

        parse: function parse(response) {
            var hash = {};
            _.each(response, function eachResponseItem(locationMap, itemId) {
                hash[itemId] = {};
                _.each(locationMap, function eachLocation(value, locationId) {
                    hash[itemId][locationId] = new Backbone.Model(value);
                });
            });

            return hash;
        }
    });
});