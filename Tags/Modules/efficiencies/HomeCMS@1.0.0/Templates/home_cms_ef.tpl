{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home">
    <section class="home-cms-page-top" data-cms-area="home_top" data-cms-area-filters="path"></section>
    <section class="home-cms-page-main" data-cms-area="home_main" data-cms-area-filters="path"></section>
    <section class="home-cms-page-bottom" data-cms-area="home_bottom" data-cms-area-filters="path"></section>
</div>