define('Facets.Translator.Manual.Product.Sequencing', [
    'Facets.Translator',
    'SC.Configuration',
    'underscore',
    'Utils'
], function FacetsModelManualProductSequencing(
    FacetsTranslator,
    Configuration,
    _
) {
    'use strict';

    _.extend(FacetsTranslator.prototype, {
        // URL method
        getApiParams: _.wrap(FacetsTranslator.prototype.getApiParams, function(fn) {
            var ret = fn.apply(this, _.toArray(arguments).slice(1));
            var sequenceEnabled = (
                Configuration &&
                Configuration.modulesConfig.Categories.manualProductSequenceEnabled
                ) || false;
            // custom field for sort number
            var sequenceField = (
                    Configuration &&
                    Configuration.modulesConfig.Categories.manualProductSequenceSortField
                ) || 'custitem_manualproductsequence';
            // Sort order
            var sequenceOrder = (
                    Configuration &&
                    Configuration.modulesConfig.Categories.manualProductSequenceSortOrder
                ) || 'desc';
            var originalSort;

            var isManualSequenceConfigured =
                SC &&
                SC.ENVIRONMENT &&
                SC.ENVIRONMENT.siteSettings &&
                !!_.findWhere(SC.ENVIRONMENT.siteSettings.sortfield, {sortfieldname: sequenceField});


            if (sequenceEnabled && !isManualSequenceConfigured) {
                console && console.warn('Manual sequencing is not properly configured');
            }

            if (sequenceEnabled && isManualSequenceConfigured) {
                // Relevance sorting for Query string is a must. Avoid touching that
                if (typeof ret.q === 'undefined' || ret.q === '') {
                    if (!ret.sort) {
                        ret.sort = sequenceField + ':' + sequenceOrder;
                    } else {
                        originalSort = ret.sort.split(':');
                        // We only customize over the relevance. Not any other sort.
                        if (originalSort[0] === 'relevance') {
                            ret.sort = sequenceField + ':' + sequenceOrder;
                        }
                    }
                }
            }

            return ret;
        })
    });
});