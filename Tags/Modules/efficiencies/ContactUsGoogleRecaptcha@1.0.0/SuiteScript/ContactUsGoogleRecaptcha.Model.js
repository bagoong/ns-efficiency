define('ContactUsGoogleRecaptcha.Model', [
    'SC.Model',
    'ContactUs.Model',
    'GoogleRecaptcha.Model',

    'Application',
    'underscore'
], function ContactUsGoogleRecaptchaModel(
    SCModel,
    ContactUsModel,
    GoogleRecaptchaModel,
    Application,
    _
) {
    'use strict';
    var extendContactUs;

    Application.on('before:ContactUs.create', function ContactUsModelCreate(Model, data) {
        GoogleRecaptchaModel.validate(data['g-recaptcha-response']);
    });

    extendContactUs =  _.extend(ContactUsModel, {
        name: 'contactusgooglerecaptcha'
    });

    return extendContactUs;
});