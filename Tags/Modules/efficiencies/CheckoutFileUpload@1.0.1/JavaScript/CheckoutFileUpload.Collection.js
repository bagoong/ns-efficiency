/*
    © 2015 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module CheckoutFileUpload

define('CheckoutFileUpload.Collection', [
    'CheckoutFileUpload.Model',
    'Backbone',
    'underscore'

],  function CheckoutFileUploadCollection(
    Model,
    Backbone,
    _
) {
    'use strict';

    return Backbone.Collection.extend({
        model: Model,
        url: _.getAbsoluteUrl('services/CheckoutFileUpload.Service.ss')
    } );
} );

