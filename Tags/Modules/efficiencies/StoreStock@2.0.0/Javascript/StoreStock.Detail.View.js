/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.Status.View', [
    'Backbone',
    'underscore',
    'store_stock_detail.tpl'
], function StoreStockDetailView(
    Backbone,
    _,
    storeStocktpl
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStocktpl,

        getContext: function getContext() {
            var stockStatuses = this.options.application.getConfig('storeStock.stockStatus');
            var stockStatus = stockStatuses[this.model && this.model.get('badge') || 2];
            var stock = this.model && this.model.get('stock') || 0;

            return {
                isDisplayAsBadge: this.options.application.getConfig('storeStock.isDisplayAsBadge') || stock === 0,
                badgeClass: stockStatus && stockStatus.htmlClass,
                badgeText: stockStatus && stockStatus.badgeLabel,
                isValuePreferredStock: this.model && this.model.get('isValuePreferred'),
                value: stock,
                hasModel: !!this.model
            };
        }
    });
});