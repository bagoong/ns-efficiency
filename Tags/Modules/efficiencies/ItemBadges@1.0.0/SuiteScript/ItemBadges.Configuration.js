/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges.Configuration', [
    'Configuration',
    'underscore'
], function(
    GlobalConfiguration,
    _
) {
    'use strict';

    var Configuration = {
        badgeFacet: {
            showFacet: true,
            name: 'Badges'
        }
    };

    _.extend(Configuration, {
        get: function get() {
            return Configuration;
        }
    });

    _.extend(GlobalConfiguration, {
        itembadges: Configuration
    });

    GlobalConfiguration.publish.push({
        key: 'ItemBadges_config',
        model: 'ItemBadges.Configuration',
        call: 'get'
    });

    return Configuration;
});
