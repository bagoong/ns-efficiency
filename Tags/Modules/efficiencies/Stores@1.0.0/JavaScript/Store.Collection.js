define('Store.Collection', [
    'Store.Model',
    'Backbone.CachedCollection',
    'underscore',
    'Utils'
], function StoreCollection(
    Model,
    CachedCollection,
    _
) {
    'use strict';

    return CachedCollection.extend({
        url: _.getAbsoluteUrl('services/Store.Service.ss'),
        model: Model,
        sortByNearest: function sortByNearest(point) {
            var k;
            this.sortByDistanceCache = this.sortByDistanceCache || {};

            k = point.lat() + '-' + point.lng();

            if (!this.sortByDistanceCache[k]) {
                delete this.sortByDistanceCache;
                this.sortByDistanceCache = {}; // Limit cache to last point in a quick way

                this.sortByDistanceCache[k] = this.sortBy(function sorting(a) {
                    return a.distanceTo(point);
                });
            }

            return this.sortByDistanceCache[k];
        }
    });
});
