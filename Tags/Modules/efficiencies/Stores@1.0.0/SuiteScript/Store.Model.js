define('Store.Model', [
    'SC.Model',
    'SearchHelper',
    'underscore'
], function StoreModel(
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    // @class Store.Model
    // @extends SCModel
    return SCModel.extend({
        name: 'Store',
        record: 'customrecord_ef_st_store',
        fieldsets: {
            basic: [
                'internalid',
                'name',
                'location',
                'urlcomponent',
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'lat',
                'lon',
                'phone',
                'zipcode',
                'thumbnail',
                'marker',
                'shortDescription'
            ],
            details: [
                'internalid',
                'name',
                'location',
                'urlcomponent',
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'lat',
                'lon',
                'phone',
                'zipcode',
                'mainImage',
                'thumbnail',
                'marker',
                'shortDescription',
                'longDescription'
            ]
        },
        filters: [
            {fieldName: 'isinactive', operator: 'is', value1: 'F'}
        ],
        columns: {
            internalid: {fieldName: 'internalid'},
            name: {fieldName: 'name'},
            location: {fieldName: 'custrecord_ef_st_s_location'},
            urlcomponent: {fieldName: 'custrecord_ef_st_s_urlcomponent'},
            address1: {fieldName: 'custrecord_ef_st_s_address1'},
            address2: {fieldName: 'custrecord_ef_st_s_address2'},
            city: {fieldName: 'custrecord_ef_st_s_city'},
            state: {fieldName: 'custrecord_ef_st_s_state', type: 'text'},
            country: {fieldName: 'custrecord_ef_st_s_country', type: 'text'},
            lat: {fieldName: 'custrecord_ef_st_s_latitude'},
            lon: {fieldName: 'custrecord_ef_st_s_longitude'},
            phone: {fieldName: 'custrecord_ef_st_s_phone'},
            zipcode: {fieldName: 'custrecord_ef_st_s_zipcode'},
            mainImage: {fieldName: 'custrecord_ef_st_s_main_image', type: 'object'},
            thumbnail: {fieldName: 'custrecord_ef_st_s_thumbnail_image', type: 'object'},
            marker: {fieldName: 'custrecord_ef_st_s_marker_image', type: 'object'},
            shortDescription: {fieldName: 'custrecord_ef_st_s_short_description'},
            longDescription: {fieldName: 'custrecord_ef_st_s_short_description'}
        },
        list: function list() {
            var Search = new SearchHelper(this.record, this.filters, this.columns, this.fieldsets.basic).search();
            return Search.getResults();
        },
        listStoresForPurchases: function listStoresForPurchases() {
            var Search;
            var filters = _.clone(this.filters);
            filters.push({fieldName: 'custrecord_ef_st_s_location', operator: 'noneof', value1: '@NONE@'});
            filters.push({
                fieldName: 'makeinventoryavailablestore',
                joinKey: 'custrecord_ef_st_s_location',
                operator: 'is',
                value1: 'T'
            });
            Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.basic).search();
            return Search.getResults();
        },
        getByUrlcomponent: function getByUrlcomponent(urlcomponent) {
            var Search;
            var store;
            if (urlcomponent) {
                Search = new SearchHelper(this.record, this.filters, this.columns, this.fieldsets.details);

                Search.addFilter({
                    fieldName: this.columns.urlcomponent.fieldName,
                    operator: 'is',
                    value1: urlcomponent
                });
                Search.search();
                store = Search.getResult();

                if (!store) {
                    throw notFoundError;
                }
            } else {
                throw notFoundError;
            }

            return store;
        }
    });
});

