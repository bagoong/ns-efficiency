function service (request) {
    'use strict';

    var DisplayProductLeadTime = require('DisplayProductLeadTime.Model');
    var Application = require('Application');
    var method = request.getMethod();
    var data;

    try {
        data = {itemId: request.getParameter('itemId')};
    } catch (e) {
       return Application.sendError(methodNotAllowedError);
    }

    try {
        switch (method) {
            case 'GET':
                Application.sendContent(DisplayProductLeadTime.proxy(data, request));
                break;
            default:
                return Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}