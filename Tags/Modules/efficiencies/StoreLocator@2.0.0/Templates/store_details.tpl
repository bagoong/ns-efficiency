<div id="stores-store-locator-details" class="stores-store-locator-details-container">
    <div class="panel panel-default">
        <div class="panel-body" itemscope itemtype="http://schema.org/{{configuration.SchemaOrg}}">
            <div class="col-md-4 col-sm-6">
                <div class="col-xl-12 col print-hide">
                    {{#if isInMyStore}}
                    <h3 class="panel-title">
                        {{ translate 'My Store: ' }}
                    </h3>
                    {{/if}}

                    <h3 itemprop="name" class="panel-title panel-title-store">
                        {{ translate model.name }}
                    </h3>
                </div>

                <div class="col-xl-12 col print-hide">
                    <h4 class="panel-title h4-panel-title">{{ translate "STORE ADDRESS" }}</h4>
                    <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <div itemprop="streetAddress">
                            {{ translate model.address1}}
                        </div>
                        <div itemprop="streetAddress">
                            {{ translate model.address2 }}
                        </div>
                        <div>
                            <span itemprop="addressLocality">
                                {{ translate model.city}}
                            </span>
                            {{#if model.state}} , {{/if}}
                            <span itemprop="addressRegion">
                                {{ translate model.state }}
                            </span>
                            {{#if model.zipcode}} , {{/if}}

                            <span itemprop="postalCode">{{ translate model.zipcode}}</span>
                        </div>
                        <div>
                            <span itemprop="addressCountry">{{ translate model.country}}</span>
                        </div>
                    </address>
                </div>

                <div class="col-xl-12 print-hide">
                    {{#if model.phone}}
                        <div class="col">
                            <h4 class="panel-title h4-panel-title">{{ translate "PHONE" }}</h4>
                            <span itemprop="telephone" content="{{ model.phone }}">{{ model.phone }}</span>
                        </div>
                    {{/if}}
                    {{#if model.openingHours}}
                        <div class="col">
                            <h4 class="panel-title h4-panel-title">{{ translate "OPENING TIMES" }}</h4>
                            {{{ translate model.openingHours }}}
                        </div>
                    {{/if}}
                </div>


                <div class="col-xl-12 col" data-view="StoreLocator.DirectionForm"></div>
                    {{#unless isInMyStore}}
                    <div class="col-xl-12 col print-hide">
                        <a href="stores" class="store-locator-back-button">{{translate 'Back to store locator'}}</a>
                        <button id="printDirection" class="storelocator-print-direction-button" data-action="print-direction">
                            {{translate 'Print directions'}}
                        </button>
                    </div>
                    {{/unless}}
                </div>


                <div id="map" class="col-md-8 col-sm-12 map-container" data-view="StoreLocator.Map"></div>
                <div id="directions" class="col-md-8 col-sm-12 col-md-offset-4 col-xs-offset-0 store-detail-directions"></div>

            </div>
        </div>
    </div>
</div>