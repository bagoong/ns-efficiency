<div id="store-info-window-{{model.internalid}}" class="store-info-window">
    <div id="top-cont">
        <h4>{{ model.name }}</h4>
    </div>
    <div id="mid-cont">
        {{#if hasThumbnail }}
        <div id="store-img" class="infoWinWidth">
            <a href="{{baseUrl}}{{model.thumbnail.name}}" target="_blank">
                <img src="{{resizeImage storeImageThumbnail thumbnail}}" alt="{{model.name}}" class="store-img-thumbnail">
            </a>
        </div>
		{{/if}}
        <div id="store-address">
            <address>
                {{#if model.address1 }} {{ model.address1}}<br>  {{/if}}
				{{#if model.address2 }} {{ model.address2 }}<br> {{/if}}

                {{ model.city }}
                {{#if model.state}} , {{/if}}
                {{ model.state }}
                {{#if model.zipcode}} , {{ model.zipcode }} <br> {{/if}}

                {{#if model.country}} {{ model.country }}<br> {{/if}}
            </address>
        </div>
    </div>
    <div id="bot-cont">
        {{#unless hideButtons}}
        <ul>
            {{#if showStoreLink}}
            	<li>
					<a href="/stores/{{model.urlcomponent}}">{{translate 'Details'}}</a>
				</li>
			{{ else }}
            	<li>
					<a href="{{gMapsLink}}" target="_blank">{{translate 'Directions'}}</a>
				</li>
       		{{/if}}
        </ul>
        {{/unless}}
    </div>
</div>