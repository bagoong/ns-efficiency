<tbody class="matrix-multi-add-table-body">
    {{#if isDisplay}}
    <tr data-rowid="{{uniqueID}}" class="matrix-multi-add-row" data-action="mma-show-prices">
        <td class="matrix-multi-add-first-cell">
            {{#if thumbnailImage.url}}
                <img class="matrix-multi-add-thumb-image" src="{{thumbnailImage.url}}" alt="{{colsLabel}}">
            {{else}}
                {{#if cols}}<div class="matrix-multi-add-thumb-color" style="background-color: {{cols}}"></div>{{/if}}
            {{/if}}
            <span class="matrix-multi-add-cols-label">{{colsLabel}}</span>
        </td>
        {{#if rowsCollection}}
            {{#each rowsCollection}}
                {{#if internalid}}
                    {{#if isAvailable}}
                        <td class="matrix-multi-add-quantity">
                            <input data-field="input" type="number" name="{{label}}" min="0" class="multi-add-to-cart-quantity-value" data-rowid="{{internalid}}" placeholder="0" data-action="add-quantity" value="{{quantity}}">
                        </td>
                    {{else}}
                        <td class="matrix-multi-add-not-available">{{translate 'N/A'}}</td>
                    {{/if}}
                {{/if}}
            {{/each}}
        {{else}}
            {{#if uniqueID}}
                {{#if colsCollection.isAvailable}}
                    <td class="matrix-multi-add-quantity">
                        <input data-field="input" type="number" name="{{colsCollection.label}}" min="0" class="multi-add-to-cart-quantity-value" placeholder="0" data-action="add-quantity" value="{{colsCollection.quantity}}">
                    </td>
                {{else}}
                    <td class="matrix-multi-add-not-available">{{translate 'N/A'}}</td>
                {{/if}}
                    <td class="matrix-multi-add-spacer">&nbsp;</td>
            {{/if}}
        {{/if}}
    </tr>
    <tr data-priceid="{{uniqueID}}" class="matrix-multi-add-pricing matrix-multi-add-collapse">
        <td class="matrix-multi-add-spacer">&nbsp;</td>
        {{#each pricing}}
        <td>
            {{#if _priceScheduleForTable}}
            <table class="matrix-multi-add-quantity-pricing-table">
                {{#each _priceScheduleForTable}}
                <tr class="matrix-multi-add-quantity-pricing-table-row">
                    <td>
                        {{#if @last}}
                            {{minimum}} +
                        {{else}}
                            {{#if singleFigure}}
                                {{singleFigure}}
                            {{else}}
                                {{minimum}} - {{maximum}}
                            {{/if}}
                        {{/if}}
                    </td>
                    <td>{{price}}</td>
                </tr>
                {{/each}}
            </table>
            {{else}}
            <p class="matrix-multi-add-price">{{formatCurrency onlinecustomerprice}}</p>
            {{/if}}
        </td>
        {{/each}}
    </tr>

    {{else}}
    <!-- weird bug fix: prevents an empty div from being inserted into the HTML -->
    <tr style="display:none;"><td></td></tr>
    {{/if}}
</tbody>