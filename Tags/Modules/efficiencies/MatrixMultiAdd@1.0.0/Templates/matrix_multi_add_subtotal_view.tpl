<div class="matrix-multi-add-footer-subtotal"><span>{{translate 'Subtotal'}} {{total}}</span></div>
<button data-action="add-to-cart"
        class="matrix-multi-add-button"{{#unless isReadyForCart}} disabled{{/unless}}>{{translate 'Add to Cart'}}
</button>