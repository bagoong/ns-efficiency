 define('MatrixMultiAdd.Item.Model', [
     'ItemDetails.Model',
     'Backbone',
     'bignumber',
     'underscore',
     'Utils'
 ], function MatrixMultiAddItemModel(
     ItemDetailsModel,
     Backbone,
     BigNumber,
     _
) {
     'use strict';

     _.extend(ItemDetailsModel.prototype, {
         validateStocks: function validateStocks() {
             var itemName;
             var minimumQuantity;
             var itemStatus;
             var model;
             var stocks;
             var stockInfo;

             model = this.getSelectedMatrixChilds()[0];
             stockInfo = model.getStockInfo();
             itemName = model.get('_name');
             minimumQuantity = model.get('minimumquantity');
             stocks = stockInfo.stock;
             itemStatus = '';

             if (this.get('quantity') !== 0) {
                 // if item is out of stock and not backorderable
                 if (!model.get('_isPurchasable')) {
                     if (model.get('_showOutOfStockMessage')) {
                         itemStatus = _.translate(
                             '$(0) is $(1)',
                             itemName,
                             stockInfo.outOfStockMessage
                         );
                     } else {
                         itemStatus = _.translate(
                             '$(0) is not available',
                             itemName
                         );
                     }
                     this.set('quantity', 0);
                 } else {
                     if (model.get('_itemType') === 'InvtPart') {
                         if (!model.get('_isInStock')) {
                             if (model.get('_isBackorderable') && model.get('_showOutOfStockMessage')) {
                                 itemStatus = _.translate(
                                    '$(0) is $(1)',
                                    itemName,
                                    stockInfo.outOfStockMessage
                                 );
                             }
                         } else {
                             if (minimumQuantity > this.get('quantity')) {
                                 itemStatus = _.translate(
                                     '$(0) has a minimum quantity of $(1)',
                                     itemName,
                                     minimumQuantity
                                 );
                             }
                             // if qty is more than stocks
                             if (stocks < this.get('quantity')) {
                                 itemStatus = _.translate(
                                     '$(0) items of $(1) are not available. Only $(2) item(s) are in stock',
                                     this.get('quantity'),
                                     itemName,
                                     stocks
                                 );
                                 this.set('quantity', stocks);
                             }
                         }
                     }
                 }
             }

             return {
                 itemName: itemName,
                 minimumQuantity: minimumQuantity,
                 itemStatus: itemStatus,
                 stocks: stocks,
                 showOutOfStockMessage: stockInfo.showOutOfStockMessage
             };
         },

         getEstimatedRate: function getEstimatedRate() {
             var result;
             var priceSchedule = this.getSelectedMatrixChilds()[0].get('_priceSchedule');
             var qty = parseInt(this.get('quantity'), 10);
             _.each(priceSchedule, function eachPriceSchedule(data) {
                 if ((data.minimumquantity <= parseInt(qty, 10)) && (data.maximumquantity >= parseInt(qty, 10))) {
                     result = data.price;
                 }

                 if ((data.minimumquantity <= parseInt(qty, 10)) && (data.maximumquantity === undefined)) {
                     result = data.price;
                 }
             });
             if (typeof result === 'undefined') {
                 result = this.getSelectedMatrixChilds()[0].get('_price');
             }
             return result;
         },

         getEstimatedAmount: function getEstimatedAmount() {
             var result = 0;
             if (this.isReadyForCart()) {
                 if (!(this.getSelectedMatrixChilds()[0].get('minimumquantity') > this.get('quantity'))) {
                     result = BigNumber(this.getEstimatedRate()).times(this.get('quantity')).toNumber();
                 }
             }
             return  result;
         }
     });
 });