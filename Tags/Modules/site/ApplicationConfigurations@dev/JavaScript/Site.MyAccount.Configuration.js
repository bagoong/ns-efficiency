define('Site.MyAccount.Configuration', [
    'SC.MyAccount.Configuration',
    'Site.Global.Configuration',
    'underscore'
], function SiteCheckoutConfiguration(
    MyAccountConfiguration,
    GlobalConfiguration,
    _
) {
    'use strict';

    var SiteApplicationConfiguration = {

    };

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    MyAccountConfiguration.modulesConfig = MyAccountConfiguration.modulesConfig || {};
    _.extend(MyAccountConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(MyAccountConfiguration.modulesConfig, extraModulesConfig);

    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, MyAccountConfiguration);
        }
    };
});