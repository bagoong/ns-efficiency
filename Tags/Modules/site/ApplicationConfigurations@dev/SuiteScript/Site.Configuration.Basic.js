define('Site.Configuration.Basic', [
    'Configuration',
    'PSCategories.Configuration',
    'ContactUs.Configuration',
    'Newsletter.SignUp.Configuration'
], function SiteConfiguration(
    Configuration,
    CategoriesConfiguration,
    ContactUsConfiguration,
    NewsletterConfiguration
) {
    'use strict';


    /*
    Configuration.webFonts = {
        enabled: true,
        async: true,
        configuration: {
            google: { families: [ 'Roboto:400,700,300:latin' ] }
        }
    };
    */

    ContactUsConfiguration.formId = '2';
    ContactUsConfiguration.hash = 'AACffht_mA8Hmxe8BgsGadslJ0bfqNx3s6E%3D';

    NewsletterConfiguration.formId = '1';
    NewsletterConfiguration.hash = 'AACffht_QhKQDGEuuyUxvyjZCq-zAvNkC9s%3D';

    CategoriesConfiguration.secureCategories = true;

});