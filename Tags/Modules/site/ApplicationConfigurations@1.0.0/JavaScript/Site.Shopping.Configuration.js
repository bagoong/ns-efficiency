define('Site.Shopping.Configuration', [
    'SC.Shopping.Configuration',
    'Site.Global.Configuration',

    'facets_faceted_navigation_item_range.tpl',
    'facets_faceted_navigation_item_color.tpl',

    'underscore',
    'Utils'
], function SiteCheckoutConfiguration(
    ShoppingConfiguration,
    GlobalConfiguration,

    facetsFacetedNavigationItemRangeTemplate,
    facetsFacetedNavigationItemColorTemplate,

    _

) {
    'use strict';

    ShoppingConfiguration.lightColors.push('White');

    var SiteApplicationConfiguration = {
        itemDetails: [{
            name: _('Details').translate(),
            contentFromKey: 'storedetaileddescription',
            opened: true,
            itemprop: 'description'
        }],
        facets: [{
            id: 'category',
            name: _('Category').translate(),
            priority: 10,
            behavior: 'hierarchical',
            uncollapsible: true,
            titleToken: '$(0)',
            titleSeparator: ', ',
            showHeading: false
        }, {
            id: 'onlinecustomerprice',
            name: _('Price').translate(),
            priority: 20,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // this is for the case that onlinecustomerprice is not available in the account
            id: 'pricelevel5',
            name: _('Price').translate(),
            priority: 20,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_color',
            priority: 10,
            name: _('Color').translate(),
            template: facetsFacetedNavigationItemColorTemplate,
            colors: GlobalConfiguration.colors
        }]
    };

    ShoppingConfiguration.facetsSeoLimits.numberOfFacetsGroups = 0;

    _.extend(ShoppingConfiguration, GlobalConfiguration, SiteApplicationConfiguration);


    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, ShoppingConfiguration);
        }
    };
});