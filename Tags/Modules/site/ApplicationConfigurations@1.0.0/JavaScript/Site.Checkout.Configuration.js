define('Site.Checkout.Configuration', [
    'SC.Checkout.Configuration',
    'SC.Checkout.Configuration.Steps.OPC',
    'Site.Global.Configuration',
    'underscore'
], function SiteCheckoutConfiguration(
    CheckoutConfiguration,
    CheckoutConfigurationStepsOPC,
    GlobalConfiguration,
    _
) {
    'use strict';

    var SiteApplicationConfiguration = {
        checkoutSteps: CheckoutConfigurationStepsOPC
    };

    _.extend(CheckoutConfiguration, GlobalConfiguration, SiteApplicationConfiguration);


    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, CheckoutConfiguration);
        }
    };
});