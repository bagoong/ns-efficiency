# Look Book
Provides a Look Book feature, which displays a list of Look Pages. Look Pages allow a number of items to be associated with a look, and for those items to be added to cart without leaving the Look Page. Look Items will be displayed on the Product Detail Page in a section called 'Looks Good With' when the product has a Look Item within the same Look.

## Documentation
https://confluence.corp.netsuite.com/x/RJ3aAg
