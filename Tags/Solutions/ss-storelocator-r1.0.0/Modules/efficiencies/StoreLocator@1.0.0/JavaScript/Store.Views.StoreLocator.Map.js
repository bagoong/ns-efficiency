/**
 * Store.Views.StoreLocator.Map.js
 * Store Locator map - Create/setup Map region
 */
define('Store.Views.StoreLocator.Map', [
    'Backbone',
    'GoogleMapsLoader',
    'Store.Views.StoreLocator.InfoWindow',
    'storelocator_map.tpl',
    'underscore'
], function storeLocatorMap(Backbone, GoogleMapsLoader, InfoWindowView, Template, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            this.application = options.application;
            this.collection = options.collection;
            this.configuration = options.configuration;
            this.eventBus = options.eventBus;
        },

        render: function render() {
            this._render();

            GoogleMapsLoader.loadScript(this.configuration.googleMapsApiKey)
                .done(_.bind(this.mapInit, this));
        },

        mapInit: function mapInit() {
            this._infoWindow = new google.maps.InfoWindow;

            this.listenTo(this.options.eventBus, 'zoomToStoreLocation',
                _.bind(this.zoomToStoreLocation, this));
            this.listenTo(this.options.eventBus, 'locationChanged',
                _.bind(this.locationChanged, this));
        },

        zoomToStoreLocation: function zoomToStoreLocation(options) {
            var store = options.store;
            var infoWindow = this.getInfoWindow(store);
            var location;

            if (!this.map) {
                this.createGoogleMap();
            }

            if (!store) {
                this._infoWindow.close();
                return;
            }

            if (store) {
                infoWindow.open(this.map, store.get('marker'));
                location = new google.maps.LatLng(store.get('lat'), store.get('lon'));

                this.map.panTo(location);
                this.map.setZoom(this.configuration.map.selectedStoreZoom);
                this.map.setOptions({draggable: true});
            }else {
                infoWindow.close();
            }
        },

        locationChanged: function locationChanged(options) {
            var self = this;
            var location = options.location;
            var config = this.configuration;
            var marker;

            // Check if Map is already loaded else load Google maps
            if (!this.map) {
                this.loadDefaultMap();
            }

            this.currentOrigin = location;

            // Get new collection data and process data flow
            this.collection.fetch({
                data: {
                    limit: config.storeCountShow,
                    lat: location.lat(),
                    lng: location.lng()
                },
                killerid: this.application.killerid,
                success: function success() {
                    // Add stores to maps
                    self.addStores();

                    marker = self.createOriginMarker();

                    self.adjustZomm();

                    if (self.options.configuration.map.allowDrag) {
                        self._dragListener = google.maps.event.addListenerOnce(self._originMarker, 'dragend',
                            function addListenerOnce() {
                                self.options.eventBus.trigger('locationChanged', {
                                    location: marker.getPosition(),
                                    component: 'map',
                                    subcomponent: 'drag'
                                });
                            }
                        );
                    }
                }
            });
        },

        adjustZomm: function _adjustZomm() {
            var latlngbounds = new google.maps.LatLngBounds();

            this.collection.each(function each(store) {
                if (_.isObject(store.get('marker').map)) {
                    latlngbounds.extend(new google.maps.LatLng(store.get('lat'), store.get('lon')));
                }
            });

            latlngbounds.extend(new google.maps.LatLng(this.currentOrigin.lat(), this.currentOrigin.lng()));

            this.map.setCenter(latlngbounds.getCenter());
            this.map.fitBounds(latlngbounds);
        },

        // Add all Stores to Google maps
        addStores: function addStores() {
            var self = this;
            var markers = this.mapMarker || [];

            // Remove all store markers
            this.removeMarkers();

            this.collection.each(function storeCollection(store, key) {
                markers.push(self.addStoreMarker(store, key));
            });

            this.mapMarker = markers;
        },

        // Create Store markers to be added to the map
        addStoreMarker: function addStoreMarker(store, ctr) {
            var self = this;
            var marker = this.createMarker(store, ctr);

            if (marker.getMap() !== self.map) {
                marker.setMap(this.map);
            }

            store.set('marker', marker);

            marker.clickListener_ = google.maps.event.addListener(marker, 'click', function clickListener() {
                self.options.eventBus.trigger('zoomToStoreLocation', {
                    store: store,
                    component: 'map'
                });
            });

            return marker;
        },

        createMarker: function createMarker(store, ctr) {
            var location = store.getLocation();
            var imageData = this.options.configuration.storeMarker;
            var label = 'A';

            var markerOptions = {
                position: new google.maps.LatLng(location.lat, location.lng),
                title: store.get('name'),
                draggable: imageData.allowDrag,
                animation: google.maps.Animation[imageData.animation],
                label: String.fromCharCode(label.charCodeAt(0) + ctr)
            };

            if (imageData.icon) {
                markerOptions.icon = imageData.icon;
            }

            return new google.maps.Marker(markerOptions);
        },

        createOriginMarker: function createOriginMarker() {
            var imageData = this.configuration.originMarker;
            var mapData = this.configuration.map;

            var image = {
                url: imageData.icon,
                size: new google.maps.Size(imageData.size.x, imageData.size.y),
                origin: new google.maps.Point(imageData.origin.x, imageData.origin.y),
                anchor: new google.maps.Point(imageData.anchor.x, imageData.anchor.y)
            };

            var markerOptions = {
                position: this.currentOrigin,
                title: imageData.text,
                draggable: mapData.allowDrag,
                animation: google.maps.Animation[imageData.animation],
                icon: image
            };

            if (this._originMarker) {
                this._originMarker.setMap(null);
            }

            this._originMarker = new google.maps.Marker(markerOptions);
            this._originMarker.setMap(this.map);

            return this._originMarker;
        },

        // Remove all current store markers in the map
        removeMarkers: function removeMarkers() {
            if (this.mapMarker) {
                while (this.mapMarker.length) {
                    this.mapMarker.pop().setMap(null);
                }
            }
        },

        // Loads Google maps default settings
        loadDefaultMap: function loadDefaultMap() {
            var config = this.configuration;
            var mapConfig = config.map;
            var mapParams = {};
            var el = this.$('#storelocator-map-placeholder');
            var container = el[0];

            // addjust container height on map loads
            el.addClass('storelocator-mapShow');

            // configure map default settings
            mapParams = {
                zoom: mapConfig.defaultZoom,
                animation: google.maps.Animation[config.map.animation],
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: mapConfig.mapTypeControl,
                panControl: mapConfig.panControl,
                zoomControl: mapConfig.zoomControl,
                scaleControl: mapConfig.scaleControl,
                streetViewControl: mapConfig.streetViewControl,
                styles: config.mapStyles
            };

            this.map = new google.maps.Map(container, mapParams, config.mapStyles);

            return this;
        },

        getInfoWindow: function getInfoWindow(store) {
            var infoWindow = new InfoWindowView({
                model: store,
                origin: this._originMarker && this._originMarker.position
            });

            this._infoWindow.setContent(infoWindow.render().$el.html());

            return this._infoWindow;
        },

        destroy: function destroy() {
            this.stopListening();
            this._infoWindow.setMap(null);

            _.each(this._markerCache, function each(m) {
                m.setMap(null);
            });

            if (this._dragListener) {
                google.maps.event.removeListener(this._dragListener);
            }

            this._destroy();
        }
    });
});