define('Store.View.StoreLocator.DirectionSearch', [
    'Backbone',
    'GoogleMapsLoader',
    'storelocator_directionsearch.tpl',
    'jQuery',
    'underscore'
], function StoreViewStoreLocatorDirectionSearch(Backbone, GoogleMapsLoader, Template, jQuery, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        events: {
            'click [name=geolocate]': 'geolocate',
            'click .travelMode': 'travelMode',
            'submit form': 'submitSearch'
        },

        render: function render() {
            this._render();

            GoogleMapsLoader
                .loadScript(this.options.configuration.googleMapsApiKey)
                .done(_.bind(this.setupSearch, this));
        },

        getContext: function getContext() {
            return {
                showGeolocationButton: this.options.configuration.geolocationEnabled
            };
        },

        setupSearch: function setupSearch() {
            var self = this;
            var autocomplete = new google.maps.places.Autocomplete(this.$('#storelist-place')[0]);
            var place;

            this.geocoder = new google.maps.Geocoder;

            google.maps.event.addListener(autocomplete, 'place_changed', function(){
                place = autocomplete.getPlace();
                self.place = place;

                if (!self.mode) {
                    self.mode = 'DRIVING';
                }

                if (!place.geometry) {
                    return;
                }

                self.options.eventBus.trigger('direction', {
                    location: place.geometry.location,
                    mode: self.mode
                });
            });

            this.createDistanceFilter();
        },

        createDistanceFilter: function createDistanceFilter() {
            var elements = _.map(this.options.configuration.distanceFilters, function map(l) {
                return jQuery('<option />', {
                    'data-unit': l.unit,
                    value: l.value,
                    text: _.translate(l.text, l.value),
                    selected: l.isDefault ? 'selected' : null
                });
            });

            this.$('#storelist-distancefilter').append(elements);
        },

        travelMode: function travelMode(e) {
            var self = this;
            var classN = e.currentTarget.className;

            if (!self.place) {
                return;
            }

            jQuery('.' + classN).removeClass('active');
            jQuery(e.currentTarget).addClass('active');

            this.options.eventBus.trigger('direction', {
                location: self.place.geometry.location,
                mode: e.currentTarget.name
            });
        },

        submitSearch: function submitSearch(e) {
            var self = this;
            var address = this.$('#storelist-place').val();
            var request;

            e.preventDefault();
            e.stopPropagation();

            if (!address || address.length < 1) {
                return;
            }

            request = {
                address: address
            };

            this.geocoder && this.geocoder.geocode(request, function geocode(result, status) {
                if (status !== google.maps.GeocoderStatus.OK) {
                    return;
                }

                self.options.eventBus.trigger('direction', {
                    location: result[0].geometry.location,
                    mode: self.mode
                });
            });
        },

        geolocate: function geolocate() {
            var self = this;
            var location;

            if (window.navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function getCurrentPosition(pos) {
                    var $searchBox;
                    location = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

                    if (!self.mode) {
                        self.mode = 'DRIVING';
                    }

                    self.options.eventBus.trigger('direction', {
                        location: location,
                        mode: self.mode
                    });

                    self.geocoder.geocode({'latLng': location}, function geocode(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $searchBox = self.$('#storelist-place');
                                if (!$searchBox.val()) {
                                    $searchBox.val(results[0].formatted_address);
                                }
                            }
                        }
                    });
                },  undefined, /** @type GeolocationPositionOptions */({
                    maximumAge: 60 * 1000,
                    timeout: 10 * 1000
                }));
            }
        }
    });
});