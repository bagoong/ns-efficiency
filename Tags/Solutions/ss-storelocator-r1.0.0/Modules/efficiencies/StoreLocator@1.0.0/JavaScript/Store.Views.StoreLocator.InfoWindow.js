define('Store.Views.StoreLocator.InfoWindow', [
    'StoreLocator.Configuration',
    'Backbone',
    'storelocator_infowindow.tpl'
], function StoreViewsStoreLocatorInfoWindow(
    Configuration,
    Backbone,
    storelocatorInfowindowTpl
) {
    'use strict';
    return Backbone.View.extend({
        template: storelocatorInfowindowTpl,
        initialize: function initialize(options) {
            this.model = options.model;
            this.origin = options.origin;
        },

        getContext: function getContext() {
            var oAddress = this.origin && this.origin.lat ?
                ('saddr=' + this.origin.lat() + ',' + this.origin.lng() + '&') : '';

            var gMapsLink = 'http://maps.google.com/maps?' + oAddress +
                'daddr=' + this.model.get('lat') + ',' + this.model.get('lon');

            return {
                model: this.model,
                showStoreLink: Configuration.storePage && this.model.get('urlcomponent'),
                hasThumbnail: this.model.get('thumbnail') && this.model.get('thumbnail').name,
                gMapsLink: gMapsLink
            };
        }
    });
});