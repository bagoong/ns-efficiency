define('ItemDetails.View.LookItems', [
    'Backbone.CollectionView',
    'ItemDetails.View.LookItem',
    'ItemDetails.Collection.LookItems',
    'SC.Configuration',
    'Tracker',

    'itemdetails_lookitems.tpl',
    'itemdetails_lookitems_row.tpl',
    'itemdetails_lookitems_cell.tpl',

    'jQuery',
    'Backbone',
    'underscore',
    'Utils'
], function ItemDetailsViewLookItems(
    BackboneCollectionView,
    LookItemView,
    CollectionView,
    Configuration,
    Tracker,

    ItemDetailsLookItemsTpl,
    ItemDetailsLookItemsRowTpl,
    ItemDetailsLookItemsCellTpl,

    jQuery,
    Backbone,
    _
) {
    'use strict';

    return BackboneCollectionView.extend({

        initialize: function initialize() {
            var application = this.options.application;
            var collection = new CollectionView();
            var self = this;

            BackboneCollectionView.prototype.initialize.call(this, {
                collection: collection,
                viewsPerRow: Infinity,
                cellTemplate: ItemDetailsLookItemsCellTpl,
                rowTemplate: ItemDetailsLookItemsRowTpl,
                childView: LookItemView,
                template: ItemDetailsLookItemsTpl
            });

            self.collection.fetch({
                data: {
                    itemID: this.model.get('_id')
                }
            }).done(function done() {
                Tracker.getInstance().trackProductList(collection, 'Looks Good With');
                self.render();
                var carousel = self.$el.find('[data-type="carousel-look-items"]');
                if(_.isPhoneDevice() === false && application.getConfig('siteSettings.imagesizes')){
                    var img_min_height = _.where(application.getConfig('siteSettings.imagesizes'), {name: application.getConfig('imageSizeMapping.thumbnail')})[0].maxheight;

                    carousel.find('.look-item-link-image').css('minHeight', img_min_height);
                }
                _.initBxSlider(carousel, Configuration.bxSliderDefaults);
            });
        },
        getContext: function getContext() {
            return {
                name: this.model.get('_name'),
                showCells: !!this.collection.length
            };
        }
    });
});