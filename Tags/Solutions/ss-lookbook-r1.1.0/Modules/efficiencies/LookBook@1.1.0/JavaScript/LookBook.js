define('LookBook', [
    'LookBook.Router',
    'LookBook.Configuration'
], function LookBook(
    Router,
    Configuration
) {
    'use strict';
    return {
        mountToApp: function(application) {
            application.Configuration.lookBook = Configuration;
            return new Router(application);
        }
    };
});