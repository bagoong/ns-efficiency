define('StoreStock.Model', [
    'SC.Model'
], function StoreStockModel(SCModel) {
    'use strict';

    return SCModel.extend({
        name: 'StoreStock',

        listDetail: function listDetail(id, storeID) {
            var apiRequest;
            var serviceUrl = nlapiResolveURL(
                'SUITELET',
                'customscript_ef_sl_storestock',
                'customdeploy_ef_sl_storestock',
                true
            );

            serviceUrl = ( id ) ? serviceUrl + '&id=' + encodeURIComponent(id) : serviceUrl;
            serviceUrl = ( storeID ) ? serviceUrl + '&stores=' + storeID : serviceUrl;
            apiRequest = nlapiRequestURL(serviceUrl, null, null, 'GET');

            return apiRequest.getBody();
        }
    });
});