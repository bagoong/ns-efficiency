function main(callee,queueId)
{
    var output = '';
    var GOVERNANCE_THRESHOLD = 30;
    var context = nlapiGetContext();

    require.config(
        {
            "paths":{},
            "shim":{},
            "baseUrl":"",
            "configFile":null,
            "exclude":[],
            "excludeShallow":[],
            "findNestedDependencies":false,
            "loader":null,
            "preserveComments":false,
            "wrapShim":true
        }
    );

    var feedInstanceId = context.getSetting('SCRIPT', 'custscript_ef_gfg_sched_instance');
    var folderId = context.getSetting('SCRIPT', 'custscript_ef_gfg_feed_folder');

    var checkGovernance = function()
    {
        if( context.getRemainingUsage() < GOVERNANCE_THRESHOLD )
        {
            var state = nlapiYieldScript();
            if( state.status == 'FAILURE' )
            {
                nlapiLogExecution("ERROR","Failed to yield script, exiting: Reason = "+state.reason + " / Size = "+ state.size);
                throw "Failed to yield script";
            }
            else if ( state.status == 'RESUME' )
            {
                nlapiLogExecution("AUDIT", "Resuming script because of " + state.reason+".  Size = "+ state.size);
            }
            else {
                nlapiLogExecution("AUDIT", "STATE" + JSON.stringify(state) + ".  Size = "+ state.size);
            }
            // state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield
        }
    };

    var ConfigurationProvider = require('ConfigurationProvider');
    var FeedFieldFormatter = require('FeedFieldFormatter');
    var ItemApiQuerier = require('ItemApiQuerier');
    var FeedPersistor = require('FeedPersistor');
    var MatrixChildInterpreter = require('MatrixChildInterpreter');
    var EmployeeNotifier = require('EmployeeNotifier');
    var FeedInstanceModel = Application.getModel('FeedInstance');


    var feedModel = FeedInstanceModel.get(feedInstanceId);
    var configuration = new ConfigurationProvider(feedModel,folderId);
    var fieldFormatter = new FeedFieldFormatter(configuration);
    var itemApiQuerier = new ItemApiQuerier(
            configuration,
            fieldFormatter.getRequiredApiFields()
        );

    var feedPersistor = new FeedPersistor(configuration);
    var matrixChildInterpreter = new MatrixChildInterpreter(configuration);
    var employeeNotifier = new EmployeeNotifier(configuration);
    var items = [];
    var countPages = 0;

    context.setPercentComplete(0.00);

    FeedInstanceModel.setInProgress(feedInstanceId);

    //second argument is for debugging purposes, avoid running script for the full api
    while(itemApiQuerier.hasNext())
    {
        countPages++;
        checkGovernance();
        itemApiQuerier.getNext();

        var rawItems = itemApiQuerier.getItems(),
            processedItems = matrixChildInterpreter.processItems(rawItems),
            formattedItems = fieldFormatter.format(processedItems);
            feedPersistor.persist(formattedItems);

        //0 division warning :)
        if(itemApiQuerier.getTotal()/configuration.getItemsPerCall()) {
            var completionPercentage = 100 * countPages / (itemApiQuerier.getTotal() / configuration.getItemsPerCall());
            context.setPercentComplete(completionPercentage);
        }
    }

    feedPersistor.close();
    var files = feedPersistor.getFiles();



 FeedInstanceModel
    FeedInstanceModel.finish(feedInstanceId,files);
    employeeNotifier.notify(feedModel);
	context.setPercentComplete(100);
}