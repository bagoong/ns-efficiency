define('HashtagGallery.Configuration', function HashtagGalleryConfiguration() {
    'use strict';
    return {
        resultsPerPage: 8
    };
});