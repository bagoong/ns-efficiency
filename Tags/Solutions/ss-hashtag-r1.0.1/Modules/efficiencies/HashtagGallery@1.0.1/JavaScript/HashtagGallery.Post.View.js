define('HashtagGallery.Post.View', [
    'Backbone',
    'hashtag_gallery_post.tpl'
], function HashtagGalleryPostView(
    Backbone,
    hashtagGalleryPostTpl
) {
    'use strict';
    return Backbone.View.extend({
        template: hashtagGalleryPostTpl,
        getContext: function getContext() {
            return {
                username: this.model.get('userName'),
                asset: this.model.get('asset'),
                lowresasset: this.model.get('lowresAsset'),
                link: this.model.get('link'),
                caption: this.model.get('caption'),
                userpic: this.model.get('userPic'),
                displayUser: this.options.application.getConfig('hashtagGallery.displayUser'),
                displayCaption: this.options.application.getConfig('hashtagGallery.displayCaption')
            };
        }
    });
});