define('GuestOrderStatus.Configuration', [
    'Configuration',
    'underscore'
], function GuestOrderStatusConfiguration(
    GlobalConfiguration,
    _
) {
    'use strict';
    var configuration = {
        secondField: [{
            id: 'shipzip',
            name: 'Shipping Address Zip Code'
        }, {
            id: 'email',
            name: 'Email Address',
            validation: {
                pattern: 'email'
            }
        }]
    };

    _.extend(configuration, {
        get: function get() {
            return this;
        }
    });

    GlobalConfiguration.publish.push({
        key: 'GuestOrderStatus_config',
        model: 'GuestOrderStatus.Configuration',
        call: 'get'
    });

    return configuration;
});


