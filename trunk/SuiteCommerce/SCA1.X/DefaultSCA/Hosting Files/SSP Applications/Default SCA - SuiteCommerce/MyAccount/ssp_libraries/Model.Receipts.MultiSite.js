Application.extendModel('Receipts', {
    list: function (options)
    {
        'use strict';

        options = options || {};

        var reciept_type = this._getReceiptType(options.type)
            ,	isMultiCurrency = context.getFeature('MULTICURRENCY')
            ,	settings_site_id = session.getSiteSettings(['siteid'])
            ,	site_id = settings_site_id && settings_site_id.siteid
            ,	amount_field = isMultiCurrency ? 'fxamount' : 'amount'
            ,	filters = [
                new nlobjSearchFilter('entity', null, 'is', nlapiGetUser())
                ,	new nlobjSearchFilter('mainline', null, 'is', 'T')
                ,	new nlobjSearchFilter('type', null, 'anyof', reciept_type)
            ]

            ,	columns = [
                new nlobjSearchColumn('internalid')
                ,	new nlobjSearchColumn('tranid')
                ,	new nlobjSearchColumn('trandate')
                ,	new nlobjSearchColumn('status')
                ,	new nlobjSearchColumn('type')
                ,	new nlobjSearchColumn('closedate')
                ,	new nlobjSearchColumn('mainline')
                ,	new nlobjSearchColumn('duedate').setSort()
                ,	new nlobjSearchColumn(amount_field)
            ]
            ,	amount_remaining;

        if (isMultiCurrency)
        {
            amount_remaining = new nlobjSearchColumn('formulanumeric').setFormula('{amountremaining} / {exchangerate}');
        }
        else
        {
            amount_remaining = new nlobjSearchColumn('amountremaining');
        }

        columns.push(amount_remaining);

        // if the store has multiple currencies we add the currency column to the query
        if (isMultiCurrency)
        {
            columns.push(new nlobjSearchColumn('currency'));
        }

        // if the site is multisite we add the siteid to the search filter
        if (context.getFeature('MULTISITE') && site_id)
        {
            var websites = [site_id, '@NONE@'],
                otherSites = SC.projectConfig.site.showOrdesAlsoFromSites;

            websites = _.union(websites, otherSites);

            filters.push(new nlobjSearchFilter('website', null, 'anyof', websites));
        }

        if (options.status)
        {
            var self = this;

            filters.push(
                new nlobjSearchFilter('status', null, 'anyof', _.map(reciept_type, function (type)
                {
                    return self._getReceiptStatus(type, options.status);
                }))
            );
        }

        if (options.orderid)
        {
            filters.push(new nlobjSearchFilter('createdfrom', null, 'anyof', options.orderid));
        }

        if (options.internalid)
        {
            filters.push(new nlobjSearchFilter('internalid', null, 'anyof', options.internalid));
        }

        var results = Application.getAllSearchResults(options.type === 'invoice' ? 'invoice' : 'transaction', filters, columns)
            ,	now = new Date().getTime();


        return _.map(results || [], function (record)
        {

            var due_date = record.getValue('duedate')
                ,	close_date = record.getValue('closedate')
                ,	tran_date = record.getValue('trandate')
                ,	due_in_milliseconds = new Date(due_date).getTime() - now
                ,	total = toCurrency(record.getValue(amount_field))
                ,	total_formatted = formatCurrency(record.getValue(amount_field));

            return {
                internalid: record.getId()
                ,	tranid: record.getValue('tranid')
                ,	order_number: record.getValue('tranid') // Legacy attribute
                ,	date: tran_date // Legacy attribute
                ,	summary: { // Legacy attribute
                    total: total
                    ,	total_formatted: total_formatted
                }
                ,	total: total
                ,	total_formatted: total_formatted
                ,	recordtype: record.getValue('type')
                ,	mainline: record.getValue('mainline')
                ,	amountremaining: toCurrency(record.getValue(amount_remaining))
                ,	amountremaining_formatted: formatCurrency(record.getValue(amount_remaining))
                ,	closedate: close_date
                ,	closedateInMilliseconds: new Date(close_date).getTime()
                ,	trandate: tran_date
                ,	tranDateInMilliseconds: new Date(tran_date).getTime()
                ,	duedate: due_date
                ,	dueinmilliseconds: due_in_milliseconds
                ,	isOverdue: due_in_milliseconds <= 0 && ((-1 * due_in_milliseconds) / 1000 / 60 / 60 / 24) >= 1
                ,	status: {
                    internalid: record.getValue('status')
                    ,	name: record.getText('status')
                }
                ,	currency: {
                    internalid: record.getValue('currency')
                    ,	name: record.getText('currency')
                }
            };
        });

    }
});