Application.extendModel('PlacedOrder', {
    list: function (page) {
        'use strict';
        // if the store has multiple currencies we add the currency column to the query
        var isMultiCurrency = context.getFeature('MULTICURRENCY')
            , total_field = isMultiCurrency ? 'fxamount' : 'total'
            , filters = [
                new nlobjSearchFilter('entity', null, 'is', nlapiGetUser())
                , new nlobjSearchFilter('mainline', null, 'is', 'T')
            ]
            , columns = [
                new nlobjSearchColumn('internalid').setSort(true)
                , new nlobjSearchColumn('trackingnumbers')
                , new nlobjSearchColumn('trandate')
                , new nlobjSearchColumn('tranid')
                , new nlobjSearchColumn('status')
                , new nlobjSearchColumn(total_field)
            ];

        if (isMultiCurrency) {
            columns.push(new nlobjSearchColumn('currency'));
        }

        // if the site is multisite we add the siteid to the search filter
        if (context.getFeature('MULTISITE') && session.getSiteSettings(['siteid']).siteid) {
            var websites = [session.getSiteSettings(['siteid']).siteid, '@NONE@'],
                otherSites = SC.projectConfig.site.showOrdesAlsoFromSites;

            websites = _.union(websites, otherSites);

            filters.push(new nlobjSearchFilter('website', null, 'anyof', websites));
        }

        var result = Application.getPaginatedSearchResults({
            record_type: 'salesorder', filters: filters, columns: columns, page: page
        });

        result.records = _.map(result.records || [], function (record) {
            return {
                internalid: record.getValue('internalid'), date: record.getValue('trandate'), order_number: record.getValue('tranid'), status: record.getText('status'), summary: {
                    total: toCurrency(record.getValue(total_field)), total_formatted: formatCurrency(record.getValue(total_field))
                }
                // we might need to change that to the default currency
                , currency: isMultiCurrency ? {internalid: record.getValue('currency'), name: record.getText('currency')} : null
                // Normalizes tracking number's values
                , trackingnumbers: record.getValue('trackingnumbers') ? record.getValue('trackingnumbers').split('<BR>') : null, type: record.getRecordType()
            };
        });

        return result;
    }
});