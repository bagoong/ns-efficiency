define('StagingNoIndex', function() {

    'use strict';

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    var Module = {
        stagingDomain: '.netsuitestaging.com',
        isStagingDomain: function() {
            return (location.hostname.indexOf(this.stagingDomain, location.hostname.length - this.stagingDomain.length) !== -1);
        },
        mountToApp: function() {
            if(this.isStagingDomain()) {
                var $head = jQuery('head'),
                    $meta = $head.find('meta[name="robots"]');
                if($meta.length > 0) {
                    $meta.attr('content', 'noindex, nofollow');
                } else {
                    $meta = jQuery('<meta name="robots" content="noindex, nofollow">');
                    $head.append($meta);
                }
            }
        }
    };

    return Module;

});