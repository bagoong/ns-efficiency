Application.extendModel('LiveOrder', {

    submit: function () {
        'use strict';

        // retrieve from settings in the future
        var uses_3dsecure = true,
            payment = order.getPayment(),
            isPaypal = context.getSessionObject('paypal_complete') === 'T';

        var shipping_address = order.getShippingAddress()
        ,	billing_address = order.getBillingAddress()
        ,	shipping_address_id = shipping_address && shipping_address.internalid
        ,	billing_address_id = billing_address && billing_address.internalid
        ,	confirmation;

        if(!uses_3dsecure || !payment || payment.paymentterms === 'Invoice' || isPaypal) {
            confirmation = order.submit();

            // checks if necessary delete addresses after submit the order.
            this.removePaypalAddress(shipping_address_id, billing_address_id);

            context.setSessionObject('paypal_complete', 'F');
        }
        else {
            confirmation = order.submit({
                paymentauthorization: {
                    type: 'threedsecure',
                    noredirect: 'T',
                    termurl: session.getAbsoluteUrl('checkout', '../threedsecure.ssp')
                }
            });
        }

        return confirmation;
    }

});