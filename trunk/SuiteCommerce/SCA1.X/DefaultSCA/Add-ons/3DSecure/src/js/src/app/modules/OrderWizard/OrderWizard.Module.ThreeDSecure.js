// OrderWizard.Module.ThreeDSecure.js
// --------------------------------
// 
define('OrderWizard.Module.ThreeDSecure', ['Wizard.Module'], function (WizardModule) {

    'use strict';

    return WizardModule.extend({

        template: 'order_wizard_threedsecure_module',

        title: _('Credit Card Authentication').translate(),

        initialize: function(options) {

            this.application = options.application;
            this.deferred = options.deferred;

            WizardModule.prototype.initialize.apply(this, arguments);
        },
        render: function() {
            var result = WizardModule.prototype.render.apply(this, arguments);
            return result;
        },
        showInModal: function (options) {
            var self = this;
            self.render();
            var promise = self.application.getLayout().showInModal(self, _.extend({ keyboard: false, backdrop: 'static' }, options));
            promise.done(function() {
                self.listenForCallback();
                self.$containerModal.find('.modal-header button.close').remove();
                self.$('.threedsecure-reload').on('click', function() {
                    self.reload();
                    return false;
                });
            })
            return promise;
        },
        stripHTML: function(html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        },
        process3dSecure: function(confirmation) {
            var self = this;
            if(confirmation && confirmation.confirmationnumber) {
                self.error = null;
                self.model.set('confirmation', confirmation);
                self.success();
            }
            else {
                self.fail();
                if(confirmation.errorMessage) {
                    confirmation.errorMessage = self.stripHTML(confirmation.errorMessage);
                }
                self.error = confirmation;
                self.wizard.manageError(confirmation);
                self.model.set('3dsecure_error', confirmation);
                self.showInModal();
            }
        },
        listenForCallback: function() {
            var self = this;
            window.process3dSecure = function(data) {
                self.process3dSecure(data);
                window.process3dSecure = function(){};
            };
        },
        reload: function() {
            window.location.reload();
        },
        fail: function() {
            return this.deferred.reject();
        },
        success: function() {
            this.$containerModal.modal('hide');
            return this.deferred.resolve();
        },
        cancel: function () {
            this.$containerModal.modal('hide');
            return this.deferred.reject();
        }

    });
});