;(function(Application) {

    'use strict';

    // edit save method of the last step of the second to last step group

    var stepGroupsCount = Application.Configuration.checkoutSteps.length;

    if(stepGroupsCount > 0) {
        var stepGroupIndex = stepGroupsCount-1,
            stepsCount = Application.Configuration.checkoutSteps[stepGroupIndex].steps.length;
        if(stepsCount > 0) {
            var stepIndex = stepsCount === 1? 0 : stepsCount-2;

            Application.Configuration.checkoutSteps[stepGroupIndex].steps[stepIndex].save = function() {
                if (this.wizard.isPaypal() && !this.wizard.isPaypalComplete()) {
                    return this._save();
                }
                else {
                    var promise = this.wizard.model.submit();
                    /**
                     * CUSTOMIZATIONS FOR 3D SECURE
                     */
                    return this.wizard.start3dSecure(promise);
                }
            };
        }
    }
	
}(SC.Application('Checkout')));