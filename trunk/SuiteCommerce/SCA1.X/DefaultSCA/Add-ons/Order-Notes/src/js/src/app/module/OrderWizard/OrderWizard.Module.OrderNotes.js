define('OrderWizard.Module.OrderNotes', ['Wizard.Module'], function (WizardModule) {

	'use strict';

	return WizardModule.extend({

        optionName: 'custbody5',
		
		template: 'order_wizard_ordernotes_module',

        toogleOrderNotes: function() {
			var self = this,
                _options = self.model.get('options');
			
			if(_options && _options[self.optionName]) {
				self.$('textarea[name="'+self.optionName+'"]').val(_options[self.optionName]);
			}
            else {
				self.$('textarea[name="'+self.optionName+'"]').val('');
			}
		},
        isValid: function () {
            // as the textarea is optional, always resolve the promise
            return jQuery.Deferred().resolve();
        },
        render: function() {
			this._render();
			this.toogleOrderNotes();
		},
        submit: function(){
            
			var self = this,
                promise = jQuery.Deferred(),
                _options = self.model.get('options'),
                _value = self.$('textarea[name="'+self.optionName+'"]').val() || '';
			
			if(_options) {
                _options[self.optionName] = _value;
			}
			
			self.model.set('options', _options);
			
			this.isValid().done(function() {
				promise.resolve();
            }).fail(function(message) {
                promise.reject(message);
            });
			
            return promise;
        }
		
	});
});
