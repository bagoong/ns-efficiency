<?php

abstract class Factory {

    public static function getCombiner($type) {
        if($type == Types::STYLES) {
            return new CombinerCss();
        }
        else if($type == Types::JAVASCRIPT) {
            return new CombinerJs();
        }
        else if($type == Types::TEMPLATES) {
            return new CombinerTmpl();
        }
    }

}