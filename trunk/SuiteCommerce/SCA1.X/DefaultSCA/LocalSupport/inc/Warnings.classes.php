<?php

class Warning {

    /**
     * @var string
     */
    private $message;

    /**
     * @param string $message
     */
    public function setMessage($message) {
        $this->message = $message;
    }
    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    public function __construct($message) {
        $this->message = $message;
    }

    public function __toString() {
        return $this->getMessage();
    }
}
class MacroFormatWarning extends Warning {
    public function __construct($file) {
        parent::__construct("MACRO - filename is not standard for file \"".$file."\"");
    }
}
class TemplateFormatWarning extends Warning {
    public function __construct($file) {
        parent::__construct("TEMPLATE - filename is not standard for file \"".$file."\"");
    }
}