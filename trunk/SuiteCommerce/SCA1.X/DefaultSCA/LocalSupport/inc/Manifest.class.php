<?php

class Manifest {

    /********* attributes *********/

    /**
     * @var array
     */
    private $config;
    /**
     * @var string
     */
    private $dir;
    /**
     * @var string
     */
    private $content;
    /**
     * @var array
     */
    private $warnings;


    /********* getters & settes *********/

    /**
     * @param array $config
     */
    public function setConfig($config) {
        $this->config = $config;
    }

    /**
     * @param string $key (optional)
     * @param mixed $default (optional) Default value if $key is provided but not exists in config
     * @return mixed
     */
    public function getConfig($key = NULL, $default = NULL) {
        return is_string($key)? (array_key_exists($key, $this->config)? $this->config[$key] : $default) : $this->config;
    }
    /**
     * @param string $dir
     */
    public function setDir($dir) {
        $this->dir = $dir;
    }
    /**
     * @return string
     */
    public function getDir() {
        return $this->dir;
    }
    /**
     * @param string $content
     */
    public function setContent($content) {
        $this->content = $content;
    }
    /**
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
    /**
     * @param array $warnings
     */
    public function setWarnings($warnings) {
        $this->warnings = $warnings;
    }
    /**
     * @return array
     */
    public function getWarnings() {
        return $this->warnings;
    }


    /********* constructor *********/

    /**
     * @param string $dir Path to folder of manifest to use
     * @param string $content
     * @param array $warnings
     */
    public function __construct($dir, $content = "", $warnings = array()) {
        $this->dir = $dir;
        $this->config = $this->parseConfig();
        $this->content = $content;
        $this->warnings = $warnings;
    }


    /********* utilities *********/

    public function parseConfig() {
        return Config::getLocalConfig('manifest');
    }
    public function addWarning(Warning $warning) {
        $warnings = $this->getWarnings();
        $warnings[] = $warning;
        $this->setWarnings($warnings);
    }
    public function isMacro($path) {
        $filebasename = basename($path, ".txt");
        $isMacro = preg_match('/_macro$/', $filebasename) === 1;
        if(!$isMacro) {
            $isMacro = preg_match('/registerMacro/', file_get_contents($this->getDir().$path)) === 1;
            if($isMacro) {
                $this->addWarning(new MacroFormatWarning($path));
            }
        }
        return $isMacro;
    }
    public function findTemplateName($path) {
        $tmpl = basename($path, ".txt");
        $map = $this->getConfig('tmpl_map', array());
        if(array_key_exists($tmpl, $map)) {
            $tmpl = $map[$tmpl];
            $this->addWarning(new TemplateFormatWarning($path));
        }
        return $tmpl;
    }
    public function pathWithSlash($path) {
        return $path.(substr($path, -1) === "/"? "" : "/");
    }

    /********* manifest methods *********/

    public function appendContent($content) {
        $this->content .= $content;
    }
    public function appendLine($line) {
        if(substr($line, 0, 2) === './') {
            $line = substr_replace($line, '', 0, 2);
        }
        $this->appendContent((($this->getContent() !== "")? PHP_EOL : "").$line);
    }
    public function appendFolderLine($dirpath) {
        $this->appendLine($this->pathWithSlash($dirpath)." foldertype=\"site_templates\"");
    }
    public function appendFileLine($filepath, $metatag) {
        $this->appendLine($filepath." metataghtml=\"".$metatag."\"");
    }
    public function appendMacroLine($filepath) {
        $this->appendFileLine($filepath, "macro");
    }
    public function appendTemplateLine($filepath, $tmpl = NULL) {
        if($tmpl === NULL) {
            $tmpl = $this->findTemplateName($filepath);
        }
        $this->appendFileLine($filepath, $tmpl."_tmpl");
    }
    public function processFolder($parent, $is_templates = FALSE) {
        if(is_dir($this->getDir().$parent)) {
            $excluded = $this->getConfig('exclude', array());
            if(!is_array($excluded)) {
                $excluded = array();
            }
            if($parent !== '.') {
                $this->appendFolderLine($parent);
            }
            $files = scandir($this->getDir().$this->pathWithSlash($parent));
            $dirs = array();
            // first only files
            foreach($files as $file) {
                if($file != '.' && $file != '..') {
                    $path = $this->pathWithSlash($parent).$file;
                    if($is_templates && is_file($this->getDir().$path)) {
                        $is_excluded = FALSE;
                        foreach($excluded as $pattern) {
                            if(fnmatch($pattern, $path)) {
                                $is_excluded = TRUE;
                                break;
                            }
                        }
                        if(!$is_excluded) {
                            if(pathinfo($this->getDir().$path, PATHINFO_EXTENSION) === 'txt') {
                                if($this->isMacro($path)) {
                                    $this->appendMacroLine($path);
                                }
                                else {
                                    $this->appendTemplateLine($path);
                                }
                            }
                        }
                    }
                    else if(is_dir($this->getDir().$path) && !in_array($this->pathWithSlash($path), $excluded)) {
                        $dirs[] = $path;
                    }
                }
            }
            // then directories and recursion
            foreach($dirs as $dir) {
                $this->processFolder($dir, $is_templates);
            }
        }
    }
    public function process() {
        $folders = $this->getConfig('folders', array());
        if(is_array($folders) && count($folders) > 0) {
            foreach($folders as $folder) {
                $this->processFolder($folder['path'], $folder['templates']);
            }
        }
    }
    public function writeToFile() {
        $file = $this->getConfig('output', 'manifest-gen.txt');
        if($this->getConfig('backup', false) && file_exists($this->getDir().$file)) {
            $info = pathinfo($this->getDir().$file);
            rename($this->getDir().$file, $this->getDir().$info['filename'].'.backup.'.$info['extension']);
        }
        return file_put_contents($this->getDir().$file, $this->getContent()) !== FALSE;
    }

}