<?php
include_once 'include.php';

$app = Config::getAppFromUrl($_GET['app']);
$internalPath = $_GET['path'];

$localConfig = Config::getConfig('local');
$precedencesConfig = Config::getConfig('precedences');

$filePath = false;

if(array_key_exists($app, $precedencesConfig)) {
    $precedences = $precedencesConfig[$app];

    $basePath = __DIR__.'/../'.$localConfig['hosting_folder'].'SSP Applications/';

    $precedence = new Precedence($basePath, $precedences);

    $filePath = $precedence->getFilePath($internalPath);
}
else {
    $hostingPath = __DIR__.'/../'.$localConfig['hosting_folder'].$app.'/'.$internalPath;
    if(file_exists($hostingPath)) {
        $filePath = $hostingPath;
    }
    else {
        $hostingPath = __DIR__.'/../'.$app.'/'.$internalPath;
        if(file_exists($hostingPath)) {
            $filePath = $hostingPath;
        }
    }
}

if($filePath !== false) {

    header("Content-type: ".Utils::getMimeType($filePath));
    header('Content-Disposition: inline; filename="' . basename($filePath) . '"');
	header('X-Original-Path: '.$filePath);
	
    ob_clean();
    flush();
    readfile($filePath);

}
else {
    header('HTTP/1.0 404 Not Found');

    echo "File not found";
}