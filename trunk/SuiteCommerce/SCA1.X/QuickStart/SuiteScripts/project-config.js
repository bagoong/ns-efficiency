/*
 * Must also be valid JSON object after the ' = ', in order to work locally
 * Don't panic, new Object() is used instead of brackets for local parsing purposes
 * Therefore, DON'T USE BRACKETS BEFORE OR AFTER THE CONFIG ONES
 */
var SC = SC || new Object();
SC.projectConfig = {
    "local": {
        "host": "localhost/",
        "folder": "Efficiencies/SuiteCommerce/QuickStart/",
        "hosting_folder": "Hosting Files/"
    },
    "hosting_files_folder": "Live Hosting Files",
    "urlroots": {
        "global": "global",
        "shopflow": "shopflow-qs",
        "myaccount": "myaccount-qs",
        "checkout": "checkout-qs",
        "global-core": "global-core",
        "shopflow-core": "shopflow-core",
        "myaccount-core": "myaccount-core",
        "checkout-core": "checkout-core"
    },
    "site": {
        "categories": {
            "enable": true,
            "home_id": -130,
            "secure_enable": true,
            "secure_enable_subcategories": true
        },
        "content": {
            "enable": true,
            "secure_enable": true
        }
    },
    "precedences": {
        "global": [
            "SCA QuickStart - Basic/Global 1.06.1/"
        ],
        "shopflow-qs": [
            "SCA QuickStart - Reference/Reference ShopFlow 1.06.1/",
            "SCA QuickStart - Core/Global 1.06.1/",
            "SCA QuickStart - Core/ShopFlow 1.06.1/",
            "SCA QuickStart - Basic/Global 1.06.1/",
            "SCA QuickStart - Basic/ShopFlow 1.06.1/"
        ],
        "myaccount-qs": [
            "SCA QuickStart - Reference/Reference My Account 1.05.1/",
            "SCA QuickStart - Core/Global 1.06.1/",
            "SCA QuickStart - Core/MyAccount 1.05.1/",
            "SCA QuickStart - Basic/Global 1.06.1/",
            "SCA QuickStart - Basic/MyAccount 1.05.1/"
        ],
        "checkout-qs": [
            "SCA QuickStart - Reference/Reference Checkout 2.04.1/",
            "SCA QuickStart - Core/Global 1.06.1/",
            "SCA QuickStart - Core/Checkout 2.04.1/",
            "SCA QuickStart - Basic/Global 1.06.1/",
            "SCA QuickStart - Basic/Checkout 2.04.1/"
        ],
        "global-core": [
            "SCA QuickStart - Core/Global 1.06.1/"
        ],
        "shopflow-core": [
            "SCA QuickStart - Core/ShopFlow 1.06.1/"
        ],
        "myaccount-core": [
            "SCA QuickStart - Core/MyAccount 1.05.1/"
        ],
        "checkout-core": [
            "SCA QuickStart - Core/Checkout 2.04.1/"
        ]
    },
    "combiners": {
        "suitelet": {
            "script": "customscript_ns_sca_trigger_combiners",
            "deploy": "customdeploy_ns_sca_trigger_combiners"
        },
        "publisher": "SCA QuickStart - Basic",
        "applications": {
            "shopflow" : {
                "folder": "ShopFlow 1.06.1",
                "combine": ["js", "js/libs", "skins/standard", "templates"]
            },
            "myaccount" : {
                "folder": "MyAccount 1.05.1",
                "combine": ["js", "skins/standard", "templates"]
            },
            "checkout" : {
                "folder": "Checkout 2.04.1",
                "combine": ["js", "skins/standard", "templates"]
            }
        },
        "password": {
            "required": true,
            "value": "TriggerTh3m!"
        }
    }
};