_.extend(SC.Configuration.Efficiencies, {
    DuplicateCustomerManagement: {
        enabled: true,
        errorMessage: 'There is already an account with this email address. If you are sure that it is your email address, <a data-hashtag="#forgot-password" data-touchpoint="login" href="">click here</a> to retrieve your password.',
        criteria: {
            email: { type: "same" }, //Users with same email
            subsidiary: { type: "same" }, //Check only on same subsidiary
            isinactive: { type: "value", value: "F" } //check only between active users
        }
    }
});