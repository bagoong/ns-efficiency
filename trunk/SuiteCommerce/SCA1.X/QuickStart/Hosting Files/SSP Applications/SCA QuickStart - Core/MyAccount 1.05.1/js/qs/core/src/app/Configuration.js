(function (application) {

    'use strict';

    application.on('beforeStartCoreApp', function() {

        var configuration = application.Configuration;
        application.addModule('BackInStockNotificationAdmin');

        _.extend(configuration, {
        	
        });

    });

}(SC.Application('MyAccount')));