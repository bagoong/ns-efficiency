/* global CMS: false */
/**
 *  CMSadapter
 *  @summary Allows a SCA app to get content & interact with the CMS.
 *  @copyright (c) 2000-2014, NetSuite Inc.
 *
 *  NOTE: Assuming the main SCA app has included Underscore.js and left it on the global window object (Just like other SCA modules).
 */

define('CMSadapter', ['jquery'], function ($)
{
    'use strict';

    var startup_cms_load_done = false
        , adapter_config = {
            MERCHZONE_CONTENT_DEFAULT_TEMPLATE: 'merch_cms',
            IMAGE_CONTENT_DEFAULT_TEMPLATE: 'image_default'
        }
    // NOTE: Not polyfilling the addEventListener & removeEventListener methods in the browser
    // so libs like jQuery will continue to work without issue in legacy IE.
        , ie_prop_listeners = {}
        , addEventListener = function (type, callback)
        {
            var prop_fn = function (e)
            {
                if (e.propertyName === type)
                {
                    callback();
                }
            };
            // Modern browsers have addEventListener.
            if (document.addEventListener)
            {
                document.addEventListener(type, callback);
                // IE5-8 handled here.
                // The cms will update a property with the same name as the event on the documentElement.
            }
            else if (document.attachEvent)
            {
                document.documentElement.attachEvent('onpropertychange', prop_fn);
                ie_prop_listeners[type] = prop_fn; // Keep track of our new function for removal later.
            }
        }
        , removeEventListener = function (type, listener)
        {
            if (document.removeEventListener)
            {
                document.removeEventListener(type, listener);
            }
            else if (document.detachEvent)
            {
                if (ie_prop_listeners[type])
                {
                    document.documentElement.detachEvent('onpropertychange', ie_prop_listeners[type]);
                }
                ie_prop_listeners[type] = null;
            }
        }

        , CMSadapter = {
            // App start
            mountToApp: function (Application)
            {
                var self = this
                    , readyHandler = function ()
                    {
                        removeEventListener('cms:ready', readyHandler);
                        self.start(Application);
                    };
                // CMS start
                if (typeof CMS !== 'undefined' && CMS.api.is_ready())
                {
                    readyHandler();
                }
                else
                {
                    addEventListener('cms:ready', readyHandler);
                }
            }
            // Start - listen for events
            , start: function (Application)
            {
                var self = this
                    , loadHandler = function ()
                    {
                        removeEventListener('cms:load', loadHandler);
                        startup_cms_load_done = true;
                        self.getPage();
                    };

                // Initial load
                if (CMS.api.has_loaded())
                {
                    loadHandler();
                }
                else
                {
                    addEventListener('cms:load', loadHandler);
                }

                /* ==================
                 App listeners
                 ===================== */
                // afterAppendView fires when html is added to the page. Usually when a user has navigated to another page. But can also fire on things like modals for the "quick look" on products.
                // This is the only event we have currently to know that SCA might have loaded a page.
                // We wait until startup_cms_load_done is true before really listening to afterAppendView requests. This prevents extra requests during page startup due to race-conditions/timing of SCA and CMS.
                //@param view
                Application.getLayout().on('afterAppendView', function ()//(view)
                {
                    // Adapter needs to get new content in case it was a page load that happened.
                    // This is outside of the cms on purpose. When a normal user is not authenticated, the website still needs to get content.
                    if (startup_cms_load_done)
                    {
                        self.getPage();

                        // let the cms know
                        CMS.trigger('adapter:page:changed');
                    }
                });

                // App events:
                // beforeStart
                // afterModulesLoaded
                // afterStart
                Application.on('afterModulesLoaded', function ()
                {
                });


                /* ==================
                 CMS listeners - CMS tells us to do something, could fire anytime
                 ===================== */
                CMS.on('cms:get:setup', function ()
                {
                    // default values the cms needs upon startup
                    var setup = {
                        site_id: SC.ENVIRONMENT.siteSettings.id
                    };
                    // setup.site_id = SC.ENVIRONMENT.siteSettings.id;

                    CMS.trigger('adapter:got:setup', setup);
                });

                CMS.on('cms:new:content', function (content)
                {
                    // A content model is provided, and should be rendered on the current page.
                    var context = _.findWhere(content.context, {id: content.current_context})
                        , sequence = context.sequence
                        , area = $('[data-cms-area="'+context.area+'"]')
                        , children = area.children().not('.ns_ContentDropzone')
                    // NOTE: Currently, all new content should be blank by default
                        , new_html = '<div class="'+ self.generateContentHtmlClass(content) +'" id="'+ self.generateContentHtmlID(content) +'">'+ self.renderContentByType(content, true) +'</div>';

                    // TODO: Any need to verify context matches current page context (from sca)? Should always match but do we need to check?

                    // insert at proper index, assuming only children of area can be "content",
                    // even if they have been wrapped with new html (by cms internal code) it's safe to still inject based on index
                    if (sequence === 0 || children.length === 0)
                    {
                        area.prepend(new_html);
                    }
                    else if (sequence > (children.length - 1))
                    {
                        area.append(new_html);
                    }
                    else
                    {
                        children.eq(sequence).before(new_html);
                    }

                    CMS.trigger('adapter:rendered'); // let CMS know we're done
                });

                // Re-render an existing piece of content. Changing only the content values, NOT creating/re-creating the cms-content wrapper divs.
                CMS.on('cms:rerender:content', function (content)
                {
                    var selector = self.generateContentHtmlID(content, true);
                    $(selector).html(self.renderContentByType(content, true));
                    CMS.trigger('adapter:rendered');
                });

                CMS.on('cms:get:context', function ()
                {
                    var context = self.getPageContext();
                    CMS.trigger('adapter:got:context', context);
                });

                // This is for times when the CMS is telling the adapter to re-render the page, even though the adapter did not intiate the request for page data
                CMS.on('cms:render:page', function (page)
                {
                    self.renderPageContent(page);
                });
                /* ================== end CMS listeners  */

                CMS.trigger('adapter:ready');
            }

            , renderPageContent: function (page)
            {
                var self = this;
                $.each(page.areas, function (indx, area)
                {
                    var selector = '[data-cms-area="'+ area.name +'"]'
                        , ele_string = '';

                    // build html string for all content in the area
                    $.each(area.contents, function (sub_indx, content)
                    {
                        // Start new content div.
                        ele_string += '<div class="'+ self.generateContentHtmlClass(content) +'" id="'+ self.generateContentHtmlID(content) +'">';
                        ele_string += self.renderContentByType(content);
                        ele_string += '</div>'; // Close new content div.
                    });

                    // inject content
                    $(selector).html(ele_string);
                });
                CMS.trigger('adapter:rendered');
            }

            , renderMerchZone: function (content, data, merchzone)
            {
                // NOTE: merchzone is optional. Excluding it will cause the rendering to use data from the content object (should only be used for re-rendering/editing, when we know the data is fresh)
                var self = this
                    , selector = self.generateContentHtmlID(content, true)
                    , exclude_cart
                    , exclude_current
                    , exclude_without_images = content.fields.boolean_exclude_items_without_images // This is controlled by the cms, not the merchzone rule.
                    , merch_rule_count
                    , merch_rule_template;

                if (typeof merchzone === 'undefined')
                {
                    // Preview (re-render, editing) is essentialy just using the data available from the content object, it should be the latest merch data.
                    exclude_cart = content.fields.boolean_exclude_cart;
                    exclude_current = content.fields.boolean_exclude_current;
                    merch_rule_count = content.fields.number_merch_rule_count;
                    merch_rule_template = content.fields.string_merch_rule_template;
                }
                else
                {
                    // If this is not preview then depend on the merchzone data passed in, it will for sure be the latest (from an ajax call to get merchzones).
                    exclude_cart = merchzone.cart_item;
                    exclude_current = merchzone.current_item;
                    merch_rule_count = merchzone.number_of_results;
                    merch_rule_template = merchzone.template;
                }

                data.title = content.name;
                data.description = content.desc;
                data.application = SC.Application;
                data.rejected_items = self.getCurrentAndCartItems(exclude_cart, exclude_current);
                data.items = self.groomItemImagesForMerchZone(data.items, exclude_without_images);
                data.items = _.without(data.items, data.rejected_items).slice(0, merch_rule_count);

                if (SC.macros[merch_rule_template] !== undefined)
                {
                    $(selector).html(SC.macros[merch_rule_template](data));
                }
                else if (adapter_config.MERCHZONE_CONTENT_DEFAULT_TEMPLATE)
                {
                    $(selector).html(SC.macros[adapter_config.MERCHZONE_CONTENT_DEFAULT_TEMPLATE](data));
                }
                else
                {
                    $(selector).html('<div class=ns_merch_no_template_placeholder">No template found.</div>');
                }
            }

            , renderContentByType: function (content, is_edit_preview)
            {
                if (typeof is_edit_preview !== 'boolean')
                {
                    is_edit_preview = false;
                }

                var self = this
                    , content_type = content.type
                    , trimmed_text_content
                    , content_html = '';

                switch (content_type)
                {
                    case 'TEXT':
                        trimmed_text_content = $.trim(content.fields.clob_html);
                        if (trimmed_text_content !== '')
                        {
                            content_html = trimmed_text_content;
                        }
                        break;
                    case 'IMAGE':
                        if (content.fields.string_src)
                        {
                            content_html = SC.macros[adapter_config.IMAGE_CONTENT_DEFAULT_TEMPLATE](content);
                        }
                        break;
                    case 'MERCHZONE':
                        // NOTE: merchzone doesn't set content_html. Instead an ajax call is made to fill in content later.
                        if (content.fields.clob_merch_rule_url)
                        {
                            // Call items api.
                            // NOTE: It's ok to use fields.clob_merch_rule_url here instead of making an additonal request for merchzone data because this value should be the latest available when merchzone content is being edited.
                            if (is_edit_preview)
                            {
                                $.getJSON(decodeURIComponent(content.fields.clob_merch_rule_url),  function (data)
                                {
                                    self.renderMerchZone(content, data);
                                });
                            }
                            else
                            {
                                // NOTE: We need the most currently available items api url (so we get merchzone data), can't rely on content.fields.clob_merch_rule_url, it could be old (and we only use that for preview when editing anyway).
                                CMS.api.getMerchzone({
                                    merch_rule_id: content.fields.number_merch_rule_id
                                    , success: function (zone)
                                    {
                                        if (zone && zone.merch_zone && zone.merch_zone.query_string )
                                        {
                                            // Call items api.
                                            $.getJSON(decodeURIComponent(zone.merch_zone.query_string),  function (data)
                                            {
                                                self.renderMerchZone(content, data, zone.merch_zone);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        break;
                    default:
                    // do nothing
                }

                return content_html;
            }
            , getCurrentAndCartItems: function (cart, current)
            {
                var items = [];
                if (current)
                {
                    $('body').find('[data-item-id]').each(function () //(index, value)
                    {
                        var id = $(this).data('item-id');
                        if (items.indexOf(id) === -1)
                        {
                            items.push(id);
                        }
                    });
                }
                if (cart)
                {
                    _.each(SC.Application('Shopping').getCart().get('lines').models, function (value)
                    {
                        var id = value.get('item').id;
                        if (items.indexOf(id) === -1)
                        {
                            items.push(id);
                        }
                    });
                }
                return items;
            }
            , groomItemImagesForMerchZone: function (items, exclude_non_image_items)
            {
                // TODO:P2 - Figure out if we can use a more robust method to get at image.
                var i
                    , filter_items = [];

                _.each(items, function (item)
                {
                    var inner_scope = item.itemimages_detail;

                    item.cms = {};
                    item.cms.merch = {};
                    item.cms.merch.image = { // defaults
                        has_image: false
                        , url: '/c.3619616/ShopFlow/img/no_image_available.jpeg'
                        , alt_text: 'no image found'
                    };

                    for (i = 0; i < 10; i++)
                    {
                        if (inner_scope)
                        {
                            if (inner_scope.url)
                            {
                                item.cms.merch.image.url = inner_scope.url;
                                item.cms.merch.image.has_image = true;

                                if (inner_scope.altimagetext)
                                {
                                    item.cms.merch.image.alt_text = inner_scope.altimagetext;
                                }
                                else
                                {
                                    item.cms.merch.image.alt_text = '';
                                }
                                break;
                            }
                            inner_scope = inner_scope[_.keys(inner_scope)[0]];
                            continue;
                        }
                        break;
                    }
                    if (!exclude_non_image_items || item.cms.merch.image.has_image)
                    {
                        filter_items.push(item);
                    }
                });
                return filter_items;
            }
            , generateContentHtmlClass: function (content, include_dot)
            {
                if (typeof include_dot === 'undefined') { include_dot = false; }
                var str = include_dot ? '.': '';
                return str += 'cms-content cms-content-'+ content.type.toLowerCase();
            }
            , generateContentHtmlID: function (content, include_hash)
            {
                if (typeof include_hash === 'undefined') { include_hash = false; }
                var str = include_hash ? '#': '';
                return str += 'cms-content-'+ content.id +'-'+ content.current_context;
            }
            , getPage: function ()
            {
                var self = this;
                // CMS requests
                // get current page's content on load
                CMS.api.getPage({
                    page: self.getPageContext()
                    , success: function (page)
                    {
                        self.renderPageContent(page);
                    }
                    , error: function ()
                    {

                    }
                });
            }
            ,   getPagePath: function()
            {
                var canonical = Backbone.history.fragment
                    , index_of_query = canonical.indexOf('?');

                return !~index_of_query ? canonical : canonical.substring(0,index_of_query);
            }
            , getPageContext: function ()
            {
                // returning a new object everytime, so no need to clone
                return {
                        site_id: SC.ENVIRONMENT.siteSettings.id,
                    //, path: SC.Application('Shopping').getLayout().getCanonical()
                        page_type: this && this.application && this.application.getLayout().currentView && this.application.getLayout().currentView.el.id
                    //, page_type: SC.Application('Shopping').getLayout().currentView && SC.Application('Shopping').getLayout().currentView.el && SC.Application('Shopping').getLayout().currentView.el.id ? SC.Application('Shopping').getLayout().currentView.el.id : 'home-page'
                    , path: this.getPagePath()
                    // , page_type: this.application.getLayout().currentView.el.id
                };
            }
        };
    // NOTE: outside of mount to app and the CMSadapter class! - will run as soon as requirejs loads module
    return CMSadapter;
});