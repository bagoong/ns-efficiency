(function (application) {

    'use strict';

    application.on('beforeStartCoreApp', function() {

        var configuration = application.Configuration;

        application.addModule('RegularHeaders.OrderWizard.Step');

        _.extend(configuration, {

        });

    });


}(SC.Application('Checkout')));