define('NavigationHelper.Extensions', ['NavigationHelper','Session','UrlHelper'], function(NavigationHelper,Session,UrlHelper) {

    'use strict';


    return {
        mountToApp: function(application) {

            var Layout = application.getLayout();

            // Touchpoints navigation
            _.extend(Layout, {

                hrefApplicationPrefixes: ['mailto', 'tel'],

                isLinkWithApplicationPrefix: function(href) {
                    return ~_.indexOf(this.hrefApplicationPrefixes, href.split(':')[0]);
                },
                isKeepHref: function($element) {
                    return $element.attr('data-keep-href') === 'true';
                },

                clickEventListener: _.wrap(Layout.clickEventListener, function(fn, e) {

                    var anchor = jQuery(e.currentTarget),
                        href = this.getUrl(anchor) || '#';

                    if(this.isKeepHref(anchor)) {
                        return;
                    }

                    if(this.isLinkWithApplicationPrefix(href)) {
                        e.preventDefault();
                        window.location.href = href;
                    } else {
                        fn.apply(this, Array.prototype.slice.call(arguments, 1));
                    }

                })
                ,	fixTargetHost: function (url)
                {
                    var fixed_url = url;
                    // check if target is shopping (http) -> we might have to change this
                    if(!~url.indexOf('https:'))
                    {
                        var target_host = this.getTargetHost();

                        if(target_host)
                        {
                            fixed_url = fixed_url.replace(/(http:\/\/)([^/?#]*)([^>]*)/gi, function(all, protocol, host, rest){return protocol + target_host + rest;});
                            //CHANGE PZ: only pass parameters if we have a target host
                            fixed_url = SC.Utils.addParamsToUrl(fixed_url, SC.Utils.getSessionParams(application.getConfig('siteSettings.touchpoints.login')));
                        }
                    }

                    return fixed_url;
                },


                //Fix for 1.06 issue with nav helper
                getTargetTouchpoint: function ($target)
                {

                    var application = this.application
                        ,	touchpoints = Session.get('touchpoints')
                        ,	target_data = $target.data()
                        ,	target_touchpoint = (touchpoints ? touchpoints[target_data.touchpoint] : '') || ''
                        ,	hashtag = target_data.hashtag
                        ,	new_url = ''
                        ,	clean_hashtag = hashtag && hashtag.replace('#', '');

                    // If we already are in the target touchpoint then we return the hashtag or the original href.
                    // We don't want to absolutize this url so we just return it.
                    if (target_data.touchpoint === application.getConfig('currentTouchpoint'))
                    {
                        new_url = clean_hashtag ? ('#' + clean_hashtag) : this.getUrl($target);
                        new_url = target_data.keepOptions ? this.getKeepOptionsUrl($target) : new_url;
                    }
                    else
                    {
                        // if we are heading to a secure domain (myAccount or checkout), keep setting the language by url
                        if (target_touchpoint.indexOf('https:') >= 0)
                        {
                            var current_language = SC.ENVIRONMENT.currentLanguage;
                            if (current_language)
                            {
                                target_data.parameters = target_data.parameters ?
                                target_data.parameters + '&lang=' + current_language.locale :
                                'lang=' + current_language.locale;
                            }
                        }

                        if (target_data.parameters)
                        {
                            target_touchpoint += (~target_touchpoint.indexOf('?') ? '&' : '?') + target_data.parameters;
                        }

                        if (hashtag && hashtag !== '#' && hashtag !== '#/')
                        {
                            new_url = _.fixUrl(target_touchpoint + (~target_touchpoint.indexOf('?') ? '&' : '?') + 'fragment=' + clean_hashtag);
                        }
                        else
                        {
                            new_url = _.fixUrl(target_touchpoint);
                        }

                        // We need to make this url absolute in order for this to navigate
                        // instead of being triggered as a hash
                        if (new_url && !(~new_url.indexOf('http:') || ~new_url.indexOf('https:')))
                        {
                            new_url = location.protocol + '//' + location.host + new_url;
                        }

                        // Cross Domain Cookie Tracking:
                        // Trackers like Google Analytics require us to send special parameters in the url
                        // to keep tracking the user as one entity even when moving to a different domain
                        if (application.addCrossDomainParameters)
                        {
                            new_url = application.addCrossDomainParameters(new_url);
                        }
                    }

                    // check if we need to redirect to a diferent host based on the current language
                    new_url = this.fixTargetHost(new_url);

                    return new_url;
                },

                fixNoPushStateLink: _.wrap(Layout.fixNoPushStateLink, function(fn, e) {

                    var anchor = jQuery(e.currentTarget),
                        href = this.getUrl(anchor) || '#';

                    //additions for links
                    if(this.isLinkWithApplicationPrefix(href)) {
                        return;
                    }
                    if(this.isKeepHref(anchor)) {
                        return;
                    }
                    //END of additions for links

                    if (Backbone.history.options.pushState || href === '#' ||
                        href.indexOf('http://') === 0 || href.indexOf('https://') === 0 || href.indexOf('mailto:') === 0 || href.indexOf('tel:') === 0)
                    {
                        return;
                    }
                    else if (anchor.data('toggle') === 'show-in-modal')
                    {
                        anchor.data('original-href', href);
                        this.setUrl(anchor, window.location.href);
                        return;
                    }

                    var fixedHref;

                    if (window.location.hash)
                    {
                        fixedHref = window.location.href.replace(/#.*$/, '#' + href);
                    }
                    else if (window.location.href.lastIndexOf('#')  ===  window.location.href.length - 1)
                    {
                        fixedHref = window.location.href +  href;
                    }
                    else
                    {
                        fixedHref = window.location.href + '#' + href;
                    }

                    this.setUrl(anchor, fixedHref);
                })

            });

        }
    };

});