var Wizard = _.extend(Application, {

    settings: WizardSettings,

    status: {
        steps: {}
    },

    params: ['step', 'robots_move', 'website_id'],

    loadStatusFile: function() {
        return nlapiLoadFile(this.settings.webHostingName+'/'+this.settings.liveHostingName+'/' + this.settings.statusFilename);
    },
    createStatusFile: function() {
        var statusFile = nlapiCreateFile(this.settings.statusFilename, 'PLAINTEXT', JSON.stringify(this.status));
        statusFile.setFolder(this.settings.folderIds.liveHostingFiles);
        nlapiSubmitFile(statusFile);
        return statusFile;
    },
    loadStatus: function() {
        try {
            this.status = JSON.parse(this.loadStatusFile().getValue());
        } catch(e) {
            if(e.code === 'RCRD_DSNT_EXIST') {
                this.createStatusFile();
            } else {
                throw e;
            }
        }
    },
    saveStatus: function() {
        try {
            nlapiDeleteFile(this.loadStatusFile().getId());
            this.createStatusFile();
        } catch(e) {
            if(e.code === 'RCRD_DSNT_EXIST') {
                this.createStatusFile();
            } else {
                throw e;
            }
        }
    },

    show: function(data) {
        this.loadStatus();
        var info = this.getModel('Model').get(data);
        info.params = data;
        this.sendContent(info);
        this.saveStatus();
    },

    /*run: function(data) {
        this.loadStatus();
        var info = this.getModel('Model').save(data);
        info.params = data;
        this.sendContent(info);
        this.saveStatus();
    },*/

    sendContent: function (data, options) {
        'use strict';

        options = _.extend({status: 200, cache: false}, options || {});

        var content = Wizard.getModel('View').renderContent(data),
            statusCode = parseInt(options.status, 10).toString();

        this.send(data, content, statusCode, 'sendContent');
    },

    /*sendResult: function (data, options) {
        'use strict';

        options = _.extend({status: 200, cache: false}, options || {});

        var content = Wizard.getModel('View').renderResult(data),
            statusCode = parseInt(options.status, 10).toString();

        this.send(data, content, statusCode, 'sendResult');
    },*/

    sendError: function (e) {
        'use strict';

        var data = Application.processError(e),
            content = Wizard.getModel('View').renderError(data),
            statusCode = data.errorStatusCode;

        this.send(e, content, statusCode, 'sendError');
    },

    send: function(data, content, statusCode, method) {
        Application.trigger('before:Application.'+method, data);

        response.setHeader('Custom-Header-Status', statusCode);
        response.setContentType('HTMLDOC');
        response.write(content);

        Application.trigger('after:Application.sendError'+method, data);
    }

});

