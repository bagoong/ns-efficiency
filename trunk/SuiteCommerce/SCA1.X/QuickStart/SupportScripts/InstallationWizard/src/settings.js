var WizardSettings = {
    qsType: 'Basic',
    bundleId: 77135,
    statusFilename: 'qs-basic-installation.json',
    folderIds: {
        suiteBundles: -16,
        liveHostingFiles: 1
    },
    webHostingName: 'Web Site Hosting Files',
    liveHostingName: 'Live Hosting Files'
};