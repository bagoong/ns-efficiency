function wizard() {

    'use strict';

    try {
        var method = request.getMethod(),
            data = JSON.parse(request.getBody() || '{}');

        for(var param in Wizard.params) {
            data[Wizard.params[param]] = request.getParameter(Wizard.params[param]);
        }

        switch (method) {
            case 'GET':
            case 'POST':
                data.method = method;
                Wizard.show(data);
                break;

            default:
                Wizard.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Wizard.sendError(e);
    }

}