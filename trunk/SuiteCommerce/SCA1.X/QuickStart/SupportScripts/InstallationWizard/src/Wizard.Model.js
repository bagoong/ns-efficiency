Wizard.defineModel('Model', {

    steps: [
        'checkBundles',
        'assetsRobots',
        'website',
        'cdsMerch',
        'finished'
    ],

    get: function(params) {
        var data = {},
            step,
            status;
        for(var index in this.steps) {
            step = this.steps[index];
            status = this[step](params, data, Wizard.status.steps[step] || {});
            Wizard.status.steps[step] = status;
            if(!status.done) {
                break;
            }
        }
        return data;
    },

    /********************************
     ************ STEPS *************
     ********************************/

    checkBundles: function(params, data, status) {
        if(!status.done) {
            data.bundles = {
                cds: recordTypeExists('customrecord_ns_cd_content'),
                merchandising: recordTypeExists('customrecord_merch_rule'),
                reviews: recordTypeExists('customrecord_ns_pr_review'),
                prod_lists: recordTypeExists('customrecord_ns_pl_productlist')
            };
            status.data = data.bundles;
            for (var bundle in data.bundles) {
                if (!data.bundles[bundle]) {
                    status.done = false;
                    return status;
                }
            }
            status.done = true;
        } else {
            data.bundles = status.data;
        }
        return status;
    },

    assetsRobots: function(params, data, status) {
        if(status.done) {
            data.assets_robots = status.data;
        }
        else{
            var assetsFolders = this.getAssetsFolders(),
                assetsIdBundle = assetsFolders.bundle,
                assetsIdLive = assetsFolders.live;

            var robotsFiles = this.getRobotsFiles(),
                robotsFileBundle = robotsFiles.bundle,
                robotsFileLive = robotsFiles.live;

            if (params['step'] !== '2' || params.method === 'GET') {

                data.assets_robots = {
                    assets_moved: !assetsIdBundle && !!assetsIdLive,
                    robots_moved: !robotsFileBundle && !!robotsFileLive
                };

            } else {

                // statuses: success: 1, warning: 0, error: -1
                data.assets_robots = {
                    assets: {
                        status: (!assetsIdBundle && !!assetsIdLive)? 1 : -1,
                        code: 'SUCC_ASSETS_MOVED'
                    },
                    robots: {
                        status: (!robotsFileBundle && !!robotsFileLive)? 1 : -1,
                        code: 'SUCC_ROBOTS_MOVED'
                    }
                };

                var moveRobots = params['robots_move'];
                var liveHostingFilesId = Wizard.settings.folderIds.liveHostingFiles;;

                /* MOVE ASSETS */
                if(assetsIdBundle) {
                    if(!assetsIdLive) {
                        // move it to live hosting files
                        try {
                            nlapiSubmitField('folder', assetsIdBundle, 'parent', liveHostingFilesId);
                            // successfully moved
                            data.assets_robots.assets.status = 1;
                            data.assets_robots.assets.code = 'SUCC_ASSETS_MOVED';
                        } catch(e) {
                            // error moving
                            data.assets_robots.assets.status = -1;
                            data.assets_robots.assets.code = 'ERR_ASSETS_ON_MOVING';
                        }
                    } else {
                        // assets already exists
                        data.assets_robots.assets.status = 0;
                        data.assets_robots.assets.code = 'ERR_ASSETS_ALREADY_EXISTS';
                    }
                } else {
                    if(assetsIdLive) {
                        // already moved
                        data.assets_robots.assets.status = 1;
                        data.assets_robots.assets.code = 'SUCC_ASSETS_ALREADY_MOVED';
                    } else {
                        // no assets found
                        data.assets_robots.assets.status = -1;
                        data.assets_robots.assets.code = 'ERR_ASSETS_NOT_FOUND';
                    }
                }

                /* MOVE robots.txt */
                if(moveRobots === 'move') {
                    if(data.assets_robots.assets.status >= 0) {
                        if (robotsFileBundle) {
                            if (!robotsFileLive) {
                                // move it to live hosting files
                                robotsFileBundle.setFolder(liveHostingFilesId);
                                if(nlapiSubmitFile(robotsFileBundle) !== 0) {
                                    // successfully moved
                                    data.assets_robots.robots.status = 1;
                                    data.assets_robots.robots.code = 'SUCC_ROBOTS_MOVED';
                                } else {
                                    // error moving
                                    data.assets_robots.assets.status = -1;
                                    data.assets_robots.assets.code = 'ERR_ROBOTS_ON_MOVING';
                                }
                            } else {
                                // robots already exists
                                data.assets_robots.robots.status = 0;
                                data.assets_robots.robots.code = 'ERR_ROBOTS_ALREADY_EXISTS';
                            }
                        } else {
                            if (robotsFileLive) {
                                // already moved
                                data.assets_robots.robots.status = 1;
                                data.assets_robots.robots.code = 'SUCC_ROBOTS_ALREADY_MOVED';
                            } else {
                                // no robots found
                                data.assets_robots.robots.status = -1;
                                data.assets_robots.robots.code = 'ERR_ASSETS_NOT_FOUND';
                            }
                        }
                    } else {
                        // assets failed
                        data.assets_robots.robots.status = -1;
                        data.assets_robots.robots.code = 'ERR_ROBOTS_ASSETS_FAILED';
                    }
                    status.done = (data.assets_robots.robots.status >= 0);
                } else {
                    // do not move it
                    data.assets_robots.robots.status = 0;
                    data.assets_robots.robots.code = 'ERR_ROBOTS_NOT_MOVE';

                    status.done = (data.assets_robots.assets.status >= 0);
                }

                status.data = data.assets_robots;
            }
        }
        return status;
    },

    website: function(params, data, status) {
        if(status.done) {
            data.website = status.data || {};
        } else {
            if(params['step'] !== '3' || params.method == 'GET') {
                data.website = {
                    websites: []
                };

                // try to load websites
                for(var i = 0; i < 20; i++) {
                    var siteRecord = null;
                    try {
                        siteRecord = nlapiLoadRecord('website', i+1);
                    } catch (e) {
                    }
                    if(siteRecord) {
                        var type = siteRecord.getFieldValue('igniteedition');
                        if(type === 'ADVANCED') {
                            data.website.websites.push({
                                internalid: i+1,
                                internalname: siteRecord.getFieldValue('internalname'),
                                displayname: siteRecord.getFieldValue('displayname')
                            });
                        }
                    }
                }
            } else {

                var websiteId = parseInt(params['website_id'], 10),
                    websiteModel = Wizard.getModel('Model.WebSite'),
                    siteRecord = null;

                data.website = {
                    website_id: websiteId,
                    status: _.extend({
                        fieldsets: -1
                    }, status.data.status || {})
                };

                /* SET FIELDSETS */
                if(data.website.status.fieldsets < 0) {
                    siteRecord = nlapiLoadRecord('website', websiteId);
                    try {
                        var fieldsets = websiteModel.getFieldsets();
                        _.each(fieldsets, function (fieldset) {
                            siteRecord.selectNewLineItem('fieldset');
                            siteRecord.setCurrentLineItemValue('fieldset', 'fieldsetname', fieldset.name);
                            siteRecord.setCurrentLineItemValue('fieldset', 'fieldsetid', fieldset.id);
                            siteRecord.setCurrentLineItemValue('fieldset', 'fieldsetrecordtype', 'ITEM');
                            siteRecord.setCurrentLineItemValue('fieldset', 'fieldsetfields', fieldset.fields.join(','));
                            siteRecord.commitLineItem('fieldset');
                        });
                        nlapiSubmitRecord(siteRecord);
                        data.website.status.fieldsets = 1;
                    } catch (e) {
                    }
                }

                status.done = true;
            }
        }
        status.data = data.website;
        return status;
    },

    cdsMerch: function(params, data, status) {
        data.cds_merch = {};
        if(status.done) {
            data.cds_merch = status.data;
        } else {
            if(params['step'] !== '4' || params.method == 'GET') {
                // do nothing
            } else {
                data.cds_merch = {
                    cds: {
                        landings: -1,
                        enhanced: -1
                    },
                    merch: -1
                };

                var contentModel = Wizard.getModel('Model.Content'),
                    websiteId = data.website.website_id;

                if(!status.done_cds) {
                    // ---- CDS ----
                    var contents = contentModel.getCds();

                    // CREATE LANDING PAGES
                    var landings = _.where(contents, {type: 'LANDING'}),
                        landingsSuccess = [];
                    _.each(landings, function (content) {
                        var html = WizardCds[content.html],
                            title = 'QuickStart ' + Wizard.settings.qsType + ': ' + content.title;

                        var contentRecord = nlapiCreateRecord('customrecord_ns_cd_content');
                        contentRecord.setFieldValue('name', title);
                        contentRecord.setFieldValue('custrecord_ns_cdc_content', html);
                        contentRecord.setFieldValue('custrecord_ns_cdc_type', 5);
                        contentRecord.setFieldValue('custrecord_ns_cdc_approved', 'T');
                        var contentRecordId = nlapiSubmitRecord(contentRecord, true);

                        if (contentRecordId > 0) {
                            var pageRecord = nlapiCreateRecord('customrecord_ns_cd_page');
                            pageRecord.setFieldValue('custrecord_ns_cdp_type', 1);
                            pageRecord.setFieldValue('name', title);
                            pageRecord.setFieldValue('custrecord_ns_cdp_url', content.url);
                            pageRecord.setFieldValue('custrecord_ns_cdp_site', websiteId);
                            pageRecord.setFieldValue('custrecord_ns_cdp_mainbody', contentRecordId);
                            pageRecord.setFieldValue('custrecord_ns_cdp_title', content.title);
                            pageRecord.setFieldValue('custrecord_ns_cdp_pageheader', content.title);
                            pageRecord.setFieldValue('custrecord_ns_cdp_status', 1);
                            pageRecord.setFieldValue('custrecord_ns_cdp_approved', 'T');
                            var pageRecordId = nlapiSubmitRecord(pageRecord, true);
                            if (pageRecordId > 0) {
                                landingsSuccess.push(pageRecordId);
                            }
                        }
                    });
                    if (landings.length === landingsSuccess.length) {
                        data.cds_merch.cds.landings = 1;
                    }

                    // CREATE ENHANCED PAGES
                    var enhanced = _.where(contents, {type: 'ENHANCED'}),
                        enhancedSuccess = [];
                    _.each(enhanced, function (content) {
                        var pageTitle = 'QuickStart ' + Wizard.settings.qsType + ': ' + content.title;

                        var pageRecord = nlapiCreateRecord('customrecord_ns_cd_page');
                        pageRecord.setFieldValue('custrecord_ns_cdp_type', 2);
                        pageRecord.setFieldValue('name', pageTitle);
                        pageRecord.setFieldValue('custrecord_ns_cdp_site', websiteId);
                        pageRecord.setFieldValue('custrecord_ns_cdp_status', 1);
                        pageRecord.setFieldValue('custrecord_ns_cdp_approved', 'T');
                        var pageRecordId = nlapiSubmitRecord(pageRecord, true);

                        if (pageRecordId > 0) {
                            var success = true;
                            // ADD TARGETS
                            _.each(content.targets, function (target) {
                                var targetRecord = nlapiCreateRecord('customrecord_ns_cd_query');
                                targetRecord.setFieldValue('custrecord_ns_cdq_pageid', pageRecordId);
                                targetRecord.setFieldValue('custrecord_ns_cdq_query', target);
                                var targetRecordId = nlapiSubmitRecord(targetRecord, true);
                                if (!(targetRecordId > 0)) {
                                    success = false;
                                }
                            });

                            _.each(content.rules, function (rule) {
                                var title = 'QuickStart ' + Wizard.settings.qsType + ': ' + rule.name,
                                    html = WizardCds[rule.html];

                                var contentRecord = nlapiCreateRecord('customrecord_ns_cd_content');
                                contentRecord.setFieldValue('name', title);
                                contentRecord.setFieldValue('custrecord_ns_cdc_content', html);
                                contentRecord.setFieldValue('custrecord_ns_cdc_type', 5);
                                contentRecord.setFieldValue('custrecord_ns_cdc_approved', 'T');
                                contentRecord.setFieldValue('custrecord_ns_cdc_status', 1);
                                var contentRecordId = nlapiSubmitRecord(contentRecord, true);

                                if (contentRecordId > 0) {

                                    var contentPageRecord = nlapiCreateRecord('customrecord_ns_cd_pagecontent');
                                    contentPageRecord.setFieldValue('custrecord_ns_cdpc_pageid', pageRecordId);
                                    contentPageRecord.setFieldValue('custrecord_ns_cdpc_target', rule.target);
                                    contentPageRecord.setFieldValue('custrecord_ns_cdpc_contentid', contentRecordId);
                                    var contentRecordId = nlapiSubmitRecord(contentPageRecord, true);
                                    if (!(contentRecordId > 0)) {
                                        success = false;
                                    }
                                }
                            });
                            if (success) {
                                enhancedSuccess.push(pageRecordId);
                            }
                        }

                    });
                    if (enhanced.length === enhancedSuccess.length) {
                        data.cds_merch.cds.enhanced = 1;
                    }
                    status.done_cds = true;
                }

                if(!status.done_merch) {

                    var merchRules = contentModel.getMerchandising(),
                        merchRulesSuccess = [];

                    var websiteRecord = nlapiLoadRecord('website', websiteId),
                        websiteName = websiteRecord.getFieldValue('internalname');

                    _.each(merchRules, function(merchRule) {

                        var fieldsetsColumns = [
                            new nlobjSearchColumn('internalid')
                        ];
                        var fieldsetFilters = [
                            new nlobjSearchFilter('custrecord_fieldset_id', null, 'is', merchRule.fieldset),
                            new nlobjSearchFilter('custrecord_site_name_fieldsets', null, 'is', websiteName)
                        ];
                        var fieldsetsResults = Wizard.getAllSearchResults('customrecord_merch_website_fieldsets', fieldsetFilters, fieldsetsColumns);

                        if(fieldsetsResults && fieldsetsResults.length) {
                            var fieldsetId = fieldsetsResults[0].getValue(fieldsetsColumns[0]);

                            var merchRuleRecord = nlapiCreateRecord('customrecord_merch_rule');
                            merchRuleRecord.setFieldValue('name', merchRule.id);
                            merchRuleRecord.setFieldValue('custrecord_merch_title', merchRule.title);
                            merchRuleRecord.setFieldValue('custrecord_site', websiteId);
                            merchRuleRecord.setFieldValue('custrecord_site_name_rule', websiteName);
                            merchRuleRecord.setFieldValue('custrecord_fieldset', fieldsetId);
                            merchRuleRecord.setFieldValue('custrecord_no_of_results', merchRule.results);
                            merchRuleRecord.setFieldValue('custrecord_is_approved', 'T');
                            merchRuleRecord.setFieldValue('custrecord_current_item', 'T');
                            merchRuleRecord.setFieldValue('custrecord_item_cart', 'T');
                            var merchRuleRecordId = nlapiSubmitRecord(merchRuleRecord, true);
                            if(merchRuleRecordId > 0) {
                                merchRulesSuccess.push(merchRuleRecordId);
                            }
                        }

                    });
                    if (merchRules.length === merchRulesSuccess.length) {
                        data.cds_merch.merch = 1;
                    }

                    status.done_merch = true;
                }

                status.done = status.done_cds && status.done_merch;
                status.data = data.cds_merch;
            }
        }
        return status;
    },

    finished: function(params, data, status) {
        data.finished = {};
        status.done = true;
        return status;
    },


    /********************************
     ************ UTILS *************
     ********************************/

    getAssetsFolders: function() {
        var baseFolder = 'Bundle '+Wizard.settings.bundleId,
            liveHostingFilesId = Wizard.settings.folderIds.liveHostingFiles;

        /* MOVE ASSETS FOLDER */
        var columns = [
            new nlobjSearchColumn('internalid'),
            new nlobjSearchColumn('parent')
        ];
        var filters = [
            new nlobjSearchFilter('name', null, 'is', 'assets')
        ];
        var results = Wizard.getAllSearchResults('folder', filters, columns);

        // search for all assets folder, and extract bundle and live ones
        var assetsIdBundle = null;
        var assetsIdLive = null;
        _.each(results, function(result) {
            var id = result.getValue(columns[0]),
                parentId = result.getValue(columns[1]),
                parentText = result.getText(columns[1]);
            if(parentId.toString() === liveHostingFilesId.toString()) {
                assetsIdLive = id;
            } else if(parentText === baseFolder) {
                // TODO: check that folder is child of SuiteBundles
                assetsIdBundle = id;
            }
        });
        return {
            bundle: assetsIdBundle,
            live: assetsIdLive
        };
    },
    getRobotsFiles: function() {
        var baseFolder = 'Bundle '+Wizard.settings.bundleId,
            basePath = 'SuiteBundles/'+baseFolder+'/';
        var robotsFileBundle = null;
        var robotsFileLive = null;
        try {
            robotsFileBundle = nlapiLoadFile(basePath + 'robots.txt');
        } catch(e) {}
        try {
            robotsFileLive = nlapiLoadFile(Wizard.settings.webHostingName+'/'+Wizard.settings.liveHostingName+'/robots.txt');
        } catch(e) {}
        return {
            bundle: robotsFileBundle,
            live: robotsFileLive
        };
    }

});
