Wizard.defineModel('View', {

    mergeData: function (data) {
        return _.extend({
            settings: Wizard.settings,
            status: Wizard.status
        }, data);
    },

    parseHTML: function (data, template) {
        return WizardTemplates[template]({data: data});
    },

    renderContent: function (data) {
        return this.render(data, 'wizard');
    },

    renderResult: function (data) {
        return this.render(data, 'wizard');
    },

    renderError: function (data) {
        return this.render(data, 'error');
    },

    render: function (data, template) {
        data = this.mergeData(data);
        data.content = this.parseHTML(data, template);
        return this.parseHTML(data, "common");
    }
});