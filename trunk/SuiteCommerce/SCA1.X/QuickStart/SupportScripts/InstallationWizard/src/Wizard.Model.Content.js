Application.defineModel('Model.Content', {

    getCds: function() {
        return this.cds;
    },
    getMerchandising: function() {
        return this.merchandising;
    },

    merchandising: [
        {
            id: "home-merchandising",
            title: "Featured Items",
            fieldset: "search",
            results: 10
        }
    ],

    cds: [
        {
            type: "LANDING",
            title: "Contact Us",
            url: "/contactus",
            html: "contactus"
        },
        {
            type: "LANDING",
            title: "Contact Us - Thank you",
            url: "/contactus-thankyou",
            html: "contactus_thankyou"
        },
        {
            type: "LANDING",
            title: "Jobs",
            url: "/jobs",
            html: "jobs"
        },
        {
            type: "LANDING",
            title: "Our Guarantees",
            url: "/our_guarantees",
            html: "our_guarantees"
        },
        {
            type: "LANDING",
            title: "Who we are",
            url: "/who_we_are",
            html: "who_we_are"
        },
        {
            type: "ENHANCED",
            title: "Globals",
            targets: ["*", "/*"],
            rules: [
                {
                    target: ".content-toplogo",
                    name: "Content - Top Logo",
                    html: "toplogo"
                }
            ]
        },
        {
            type: "ENHANCED",
            title: "Home - Content",
            targets: ["/", "/?*"],
            rules: [
                {
                    target: "#home-slider",
                    name: "Home - Slider",
                    html: "slider"
                }
            ]
        },
        {
            type: "ENHANCED",
            title: "Demo Category",
            targets: ["/demo-category", "/demo-category?*"],
            rules: [
                {
                    target: "#banner-section-top",
                    name: "Demo Category - Banner Top",
                    html: "cat_banner_top"
                }
            ]
        }
    ]

});