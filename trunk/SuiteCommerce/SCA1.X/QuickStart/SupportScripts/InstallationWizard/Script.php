<?php
function getFileContent($fileName) {
    return implode("", file($fileName));
}

$config = json_decode(getFileContent("script.json"), true);

$content = "";

foreach($config["js"] as $js) {
    $content .= getFileContent($js)."\n\n";
}

// TEMPLATES

$content .= "\n\n"."var ".$config["views"]["var_name"]." = {\n";

$prefix = "\t";
foreach($config["views"]["templates"] as $name => $tmpl) {
    $content .= $prefix."\"".$name."\": _.template(".json_encode(str_replace("\t", "\t", getFileContent($config["views"]["folder"].$tmpl))).")";
    $prefix = ",\n\t";
}
$content .= "\n};";

// CDS

$content .= "\n\n"."var ".$config["cds"]["var_name"]." = {\n";

$prefix = "\t";
foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($config["cds"]["folder"])) as $file) {
    $filename = $file->getFilename();
    if($filename != "." && $filename != "..") {
        $content .= $prefix."\"".basename($filename, '.ejs')."\": ".json_encode(str_replace("\t", "\t", getFileContent($config["cds"]["folder"].$filename)));
        $prefix = ",\n\t";
    }
}
$content .= "\n};";

if(file_put_contents($config["name"].".js", $content) !== false) {
    echo "Success!";
}
else {
    echo "Error";
}