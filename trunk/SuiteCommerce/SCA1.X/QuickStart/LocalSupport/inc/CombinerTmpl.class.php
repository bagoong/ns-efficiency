<?php

class CombinerTmpl extends Combiner {

    private $files;

    public function getContentType() {
        return 'text/javascript';
    }
    public function getConfigFilename() {
        return "templates.config";
    }
    public function combine() {
        $this->files = array(
            'macros' => array()
        );
        $macros = array();
        $templatesPaths = array();
        $manifest = explode("\n", $this->precedence->getAppendedContent('manifest.txt'));
        $patterns = str_replace("\n", '', $this->config['Input-files']);
        foreach ($manifest as $line) {
            $tokens = explode(' ', $line);

            $file = $tokens[0];
            $tag = FALSE;

            foreach($tokens as $token) {
                if (stristr($token, 'metataghtml=')) {
                    $tag = str_ireplace(array('metataghtml=', '"', "'", "\n", "\r"), '', $token);
                    break;
                }
            }

            if($tag) {

                $file_absolute = $this->precedence->getFilePath($file);

                if(is_file($file_absolute)) {
                    /* check if file path matches any combiner pattern */
                    $included = FALSE;
                    foreach($patterns as $pattern) {
                        if($pattern == "*.txt") {
                            if(preg_match("/^templates\/[^\/]+\.txt$/", $file)) {
                                $included = TRUE;
                                break;
                            }
                        }
                        else {
                            if($pattern == "/*.txt") {
                                $pattern = "*.txt";
                            }
                            if(fnmatch('templates/'.$pattern, $file)) {
                                $included = TRUE;
                                break;
                            }
                        }
                    }

                    if($included) {
                        $file_content = str_replace("\t", "\t", implode("", file($file_absolute)));
                        if ($tag == 'macro') {
                            $macros[$file] = $file_content;
                        } else {
                            if(array_key_exists($tag, $templatesPaths)) {
                                if($templatesPaths[$tag] !== $file) {
                                    echo "The template in file: \n\t\"".$file."\" \noverrides the one in \n\t\"".$templatesPaths[$tag]."\", \nbut the paths are different.\n\nPlease fix it, because NetSuite's combiner will keep the first one it finds, instead of the one that overrides.";
                                    return;
                                }
                            } else {
                                $templatesPaths[$tag] = $file;
                            }
                            $this->files[$tag] = $file_content;
                        }
                    }
                }
            }
        }
        $this->files['macros'] = array_values($macros);
        $encoded = json_encode($this->files, $this->debug? JSON_PRETTY_PRINT : NULL);
        $this->result = $this->config['Variable-name'].' = '.$encoded.';';
    }
    public function addFile($relative, $absolute) {
    }

}