<?php

define('LOCAL_CONFIG', __DIR__.'/../../config.json');

/* Support for PHP < 5.4 */
if (!defined('JSON_UNESCAPED_SLASHES'))
    define('JSON_UNESCAPED_SLASHES', 64);
if (!defined('JSON_PRETTY_PRINT'))
    define('JSON_PRETTY_PRINT', 128);
if (!defined('JSON_UNESCAPED_UNICODE'))
    define('JSON_UNESCAPED_UNICODE', 256);

class Config {

    private static $localConfig = NULL;
    private static $config = NULL;

    private function __construct() {
    }
    private function __clone() {
    }

    private static function parseLocalConfig() {
        // if not in default dir, search for it
        if(is_file(LOCAL_CONFIG)) {
            $config_contents = file_get_contents(LOCAL_CONFIG);
            if($config_contents !== false) {
                $json = self::cleanJSON($config_contents);
                $config_array = json_decode($json, true);
                if($config_array === null && json_last_error() !== JSON_ERROR_NONE) {
                    throw new Exception("Local config file is invalid JSON.");
                }
                self::$localConfig = $config_array;
            }
            else {
                throw new Exception("Couldn't find configuration file.");
            }
        }
        else {
            throw new Exception("Please fix path to local config file.");
        }
    }
    private static function parseConfig() {
        if(self::$localConfig == NULL) {
            self::parseLocalConfig();
        }
        $localConfig = self::getLocalConfig();
        // if not in default dir, search for it
        $configPath = __DIR__.'/../../'.$localConfig['project-config']['folder'].$localConfig['project-config']['filename'];
        if(is_file($configPath)) {
            $config_contents = file_get_contents($configPath);
            if($config_contents !== false) {
                $start = strpos($config_contents, '{');
                $json = self::cleanJSON(substr($config_contents, $start, strrpos($config_contents, '}')-$start+1));
                $config_array = json_decode($json, true);
                if($config_array === null && json_last_error() !== JSON_ERROR_NONE) {
                    throw new Exception("Config file does not have a valid JSON-like JavaScript object.");
                }
                self::$config = $config_array;
            }
            else {
                throw new Exception("Couldn't find configuration file.");
            }
        }
        else {
            throw new Exception("Please fix path to project configuration file.");
        }
    }
    private static function cleanJSON($string) {
        return preg_replace('!/\*.*?\*/!s', '', $string);
    }

    public static function getKeyOrObject($array, $key, $default) {
        return is_string($key)? (array_key_exists($key, $array)? $array[$key] : $default) : $array;
    }
    public static function getLocalConfig($key = NULL, $default = NULL) {
        if(self::$localConfig == NULL) {
            self::parseLocalConfig();
        }
        return self::getKeyOrObject(self::$localConfig, $key, $default);
    }
    public static function getConfig($key = NULL, $default = NULL) {
        if(self::$config == NULL) {
            self::parseConfig();
        }
        return self::getKeyOrObject(self::$config, $key, $default);
    }

}