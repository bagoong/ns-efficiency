<?php

class CombinerJs extends Combiner {

    private $files;

    public function getContentType() {
        return 'text/javascript';
    }
    public function getConfigFilename() {
        return "combiner.config";
    }
    public function combine() {
        $this->result = '';
        $this->files = array();
        $added_files = array();
        foreach($this->config['Input-files'] as $file_name) {
            if(!empty($file_name)) {
                $matched_files = $this->precedence->globFilePathNetSuite($this->internalPath, $file_name, "*.js", GLOB_NOSORT);
                foreach($matched_files as $relative => $matched_file) {
                    if(substr($matched_file, 0, 2) === './') {
                        $matched_file = substr_replace($matched_file, '', 0, 2);
                    }
                    if(!in_array($matched_file, $added_files)) {
                        $added_files[] = $matched_file;
                        $this->addFile($relative, $matched_file);
                    }
                }
            }
        }
        if($this->debug && count($this->files) > 0) {
            $this->result = "loadScript([\n\t'".implode("',\n\t'", $this->files)."'\n]);";
        }
    }
    public function addFile($relative, $absolute) {
        if ($this->debug) {
            $path = $this->getBaseUrl().$relative;
            if($this->app === 'shopflow-qs') {
                $this->files[] = $path;
            }
            else {
                $this->result .= "document.write('<script type=\"text/javascript\" src=\"".$path."\"></script>');\n";
            }
        }
        else {
            $this->result .= file_get_contents($absolute);
        }
    }

}