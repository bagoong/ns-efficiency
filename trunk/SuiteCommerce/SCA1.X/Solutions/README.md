SOME FEATURES

- throw error when overriding template without using the same path (NetSuite doesn't handle this case well)
- mimic NetSuite behaviour in templates: *.txt includes only first-level templates, /*.txt includes all
- data-keep-href and mailto/tel links support
- CDS Cache Cleaner Add-on
- Categories integrated by enabling (settings: enable in checkout domain, disable subcategories in checkout domain)
- ApplicationSkeleton helpers for modules
- renderAllLayoutEnhancedPageContent and renderAllEnhancedPageContent events native
- All resources and scripts from Reference, Global and Customizations combined together
- Manifest generator
- Templates local combiner validates with combiner config file
- Folder precedence equal to SSP Applications in NetSuite
- URL roots redirect
- Exact folder structure as File Cabinet
- Single configuration file for all project, that generates local URLs in index files of all applications
- Separate CSS files when debugging locally
- CAT - Combiners Auto-Trigger
- Bundled with several performance optimizations
- Ready for upload
- Serve static files first from Hosting Files folder, then from root

HOW TO USE

Initial setup (before uploading)

1. Get Default SCA code using one of these methods:
    * Clone repository and delete git folder
    * Get the code separately
2. Rename project folder
3. Rename the publisher folders in Hosting Files/SSP Applications/
    * Default SCA - Reference -> (My project) - Reference
    * Default SCA - SuiteCommerce -> (My project) - SuiteCommerce
4. Edit config.php in root
5. Update Hosting Files/SSP Applications/(My project) - SuiteCommerce/Global/ssp_libraries/project-config.js with your new paths
6. Init new git repository in folder
7. Start development

Upload process

1. Replicate in File Cabinet the local structure
2. Add the project-config.js as SSP library for the 3 References and the 4 Customizations SSPs
3. Upload the customizations apps as they are
4. Go to http://project-domain.netsuitestaging.com/shopflow/combiners.ssp and trigger all combiners

Ready to go!

WHEN GOING LIVE
- add getWithCaching again in page.ss
- disable combiners suitelet