/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	fs = require("fs")
,	_ = require('underscore')
,   yargs = require('yargs')
,	package_manager = require('./gulp/package-manager')


process.gulp_dest = './../Hosting Files/SSP Applications/PS Efficiencies Group';


fs.readdirSync('./gulp/tasks').forEach(function(task_name)
{
	if (/\.js/.test(task_name))
	{
		require('./gulp/tasks/' + task_name.replace('.js', ''));
	}
});


if(yargs.argv.module){
    process.gulp_dest = './../Module/'+yargs.argv.module;
    var moduleExists = package_manager.moduleList &&  package_manager.moduleList[yargs.argv.module];

    if(moduleExists){
        var obj = {};
        obj[yargs.argv.module] = package_manager.moduleList[yargs.argv.module];
        package_manager.moduleList = obj;
        package_manager.combineBackend = false;
        package_manager.reset();
    }

}


gulp.task('efficiencies', [
    'efficiencies-ssp-libraries-configuration',
    'efficiencies-ssp-libraries',
    'efficiencies-backend-libraries',
    'efficiencies-backend-configuration',
    'efficiencies-javascript',
    'efficiencies-templates',
    'efficiencies-macros',
    'efficiencies-services',
    'efficiencies-styles',
    'efficiencies-backend-scripts',
    'efficiencies-copy',
    'efficiencies-manifests'
]);



gulp.task('efficiencies-watch', [
    'watch-efficiencies-javascript',
    'watch-efficiencies-templates',
    'watch-efficiencies-macros',
    'watch-efficiencies-styles',
    'watch-efficiencies-backend-libraries',
    'watch-efficiencies-backend-configuration',
    'watch-efficiencies-backend-scripts'
]);