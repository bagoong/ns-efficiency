/* jshint node: true */
'use strict';

var fs = require('fs')
,	path = require('path')
,	_ = require('underscore')
,	args = require('yargs').argv;

var PackageManager = module.exports = {

	contents: []

,	add: function(file_path, content)
	{
		this.contents.push({
			path: file_path
		,	baseDir: path.dirname(file_path)
		,	content: content
		});
	}

	,	getGlobsGroupedByModuleFor: function()
	{
		var keys = arguments;

		var result = {};
		this.contents.forEach(function(ns_pkg)
		{
			var pkg_keys = Object.keys(ns_pkg.content || {});
			var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });
			if (key_to_use && ns_pkg.content[key_to_use].length)
			{
				ns_pkg.content[key_to_use].forEach(function(glob)
				{
					result[ns_pkg.content.module_name] || (result[ns_pkg.content.module_name] = []);
					result[ns_pkg.content.module_name].push(ns_pkg.baseDir + '/' + glob);
				});
			}
		});

		return result;
	}
	,	getGlobsWithConfigByModuleFor: function()
	{
		var keys = arguments;

		var result = [];
		this.contents.forEach(function(ns_pkg)
		{
			var pkg_keys = Object.keys(ns_pkg.content || {});
			var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });
			if (key_to_use && ns_pkg.content[key_to_use].length)
			{
				ns_pkg.content[key_to_use].forEach(function(glob)
				{
					result.push([ns_pkg.content.module_name, ns_pkg.baseDir + '/' + glob.file,glob.file, glob.type]);
				});
			}
		});

		return result;
	}
	,	getGlobsByZ: function()
	{
		var keys = arguments;

		var result = [];
		this.contents.forEach(function(ns_pkg)
		{
			var pkg_keys = Object.keys(ns_pkg.content || {});
			var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });
			if (key_to_use && ns_pkg.content[key_to_use].length)
			{
				ns_pkg.content[key_to_use].forEach(function(glob)
				{
					glob.file = ns_pkg.baseDir + '/' + glob.file;
					result.push(glob);
				});
			}
		});

		return result;
	}
    ,	getGlobsByModuleFor: function()
    {
        var keys = arguments;

        var result = [];
        this.contents.forEach(function(ns_pkg)
        {
            var pkg_keys = Object.keys(ns_pkg.content || {});
            var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });
            if (key_to_use && ns_pkg.content[key_to_use].length)
            {
                ns_pkg.content[key_to_use].forEach(function(glob)
                {
                    result.push([ns_pkg.content.module_name, ns_pkg.baseDir + '/' + glob]);
                });
            }
        });

        return result;
    }
,	getGlobsFor: function()
	{
		var keys = arguments;

		var result = [];
		this.contents.forEach(function(ns_pkg)
		{
			var pkg_keys = Object.keys(ns_pkg.content || {});
			var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });
			if (key_to_use && ns_pkg.content[key_to_use].length)
			{
				ns_pkg.content[key_to_use].forEach(function(glob) 
				{
					result.push(ns_pkg.baseDir + '/' + glob);
				});
			}
		});

		return result;
	}

,	getFilesMapFor: function(key)
	{
		var result = {};
		this.contents.forEach(function(ns_pkg)
		{
			ns_pkg.content && ns_pkg.content[key] && Object.keys(ns_pkg.content[key]).length && Object.keys(ns_pkg.content[key]).forEach(function(file_path)
			{
				result[path.resolve(ns_pkg.baseDir + '/' + file_path)] = ns_pkg.content[key][file_path];
			});
		});
		return result;
	}
,	getFilesMapForWithApp: function()
    {
        var result = {};

        var keys = _.toArray(arguments);

        this.contents.forEach(function(ns_pkg)
        {
            var pkg_keys = Object.keys(ns_pkg.content || {});
            var key_to_use =_.find(keys, function(key){ return ~pkg_keys.indexOf(key); });

            if (key_to_use && _.keys(ns_pkg.content[key_to_use]).length)
            {
                Object.keys(ns_pkg.content[key_to_use]).forEach(function(file_path)
                {
                    result[path.resolve(ns_pkg.baseDir + '/' + file_path)] = ns_pkg.content[key_to_use][file_path];
                });
            }



        });
        return result;
    },
    reset: function(){
        var self = this;
        this.contents = [];
        Object.keys(PackageManager.moduleList)
            .map(function(m) { return './../EfficienciesModules/' + m + '@' + PackageManager.moduleList[m] + '/ns.package.json'; })
            .forEach(function(file)
            {
                try
                {
                    fs.existsSync(file) && self.add(file, JSON.parse(fs.readFileSync(file, {encoding: 'utf8'}).toString()).gulp);
                }
                catch(err)
                {
                    err.message = err.message + ' in file ' + file;
                    throw err;
                }
            });
    }
};

var distro = JSON.parse(fs.readFileSync(args.distro || './distro.json', {encoding: 'utf8'}));

PackageManager.isForDefaultSCA = distro.isForDefaultSCA;
PackageManager.applications = distro.applications;
PackageManager.combineBackend = distro.combineBackend;
PackageManager.moduleList = distro.modules;

Object.keys(distro.modules)
	.map(function(m) { return './../EfficienciesModules/' + m + '@' + distro.modules[m] + '/ns.package.json'; })
	.forEach(function(file)
	{
		try
		{
			fs.existsSync(file) && PackageManager.add(file, JSON.parse(fs.readFileSync(file, {encoding: 'utf8'}).toString()).gulp);
		}
		catch(err)
		{
			err.message = err.message + ' in file ' + file;
			throw err;
		}
	});
