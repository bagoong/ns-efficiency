/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	less = require('gulp-less')
,	rimraf = require('gulp-rimraf')
,	map = require('map-stream')
,	path = require('path')
,	package_manager = require('../package-manager');


gulp.task('less-files', function()
{
	// This should be temporal untill we find a better solution or move to a new css precompiler
	return gulp.src(package_manager.getGlobsFor('less'))
		.pipe(map(function(file, cb)
		{
			if (~file.path.indexOf('Modules/twitter-bootstrap'))
			{
				file.path = path.join(path.dirname(file.path), 'bootstrap', path.basename(file.path));
			}
			else if (~file.path.indexOf('Modules/EcommerceBaseStyles'))
			{
				var tokens = file.path.split(path.sep);
				tokens.splice(tokens.indexOf('site'), 0, 'site');
				file.path = tokens.join(path.sep);
			}

			cb(null, file);
		}))
		.pipe(gulp.dest(path.join(process.gulp_dest, 'less')));
});

gulp.task('less-compile', ['less-files'], function()
{
	return gulp.src(path.join(process.gulp_dest, 'less') + '/*.less')
		.pipe(less())
		.pipe(gulp.dest(path.join(process.gulp_dest, 'css')));
});

gulp.task('less', ['less-compile'], function()
{
	return gulp.src(path.join(process.gulp_dest, 'less'))
		.pipe(rimraf());
});
