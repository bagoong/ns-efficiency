/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	underscore = require('underscore')
,	map = require('map-stream')
,	vm = require('vm')
,	path = require('path')

,	uglify = require('gulp-uglify')
,	concat = require('gulp-concat')
,	watch = require('gulp-watch')
,	jst = require('gulp-jst')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');


var paths = module.exports = {
	myaccount: package_manager.getGlobsFor('myaccount:macros', 'macros')
,	checkout:package_manager.getGlobsFor('checkout:macros', 'macros')
,	shopping: package_manager.getGlobsFor('shopping:macros', 'macros')
};

var macros = {};
var sandbox = {
	_: underscore
,	context: {
		registerMacro: function(name, fn)
		{
			var original_source = fn.toString()
			,	prefix = '\\n\\n<!-- MACRO STARTS: ' + name + ' -->\\n'
			,	posfix = '\\n<!-- MACRO ENDS: ' + name + ' -->\\n'
				// Adds comment lines at the begining and end of the macro
				// The rest of the mumbo jumbo is to play nice with underscore.js
			,	modified_source = ';var __t, __p = \'\', __e = _.escape, __j = Array.prototype.join;try{var __p="' + prefix + '";' + original_source.replace(/^function[^\{]+\{/i, '').replace(/\}[^\}]*$/i, '') +';__p+="' + posfix + '";return __p;}catch(e){SC.handleMacroError(e,"'+ name +'")}' || []
				// We get the parameters from the string with a RegExp
			,	parameters = original_source.slice(original_source.indexOf('(') + 1, original_source.indexOf(')')).match(/([^\s,]+)/g) || [];
			
			parameters.push(modified_source);
			
			// Add the macro to SC.macros
			macros[name] = Function.apply(null, parameters);
		}
	}
};

Object.keys(paths).forEach(function(key)
{
	gulp.task(key + '-macros', function()
	{
		return gulp.src(paths[key])
			.pipe(jst())
			.pipe(map(function(file, cb)
			{
				vm.runInNewContext('var tmp = ' + file.contents.toString() + '; tmp(context)', sandbox);
				cb(null, file);
			}))
			.pipe(concat(key + '-macros.js', {newLine: ','}))
			.pipe(map(function(file, cb)
			{
				var result = 'SC.macros = {';

				Object.keys(macros).forEach(function(key)
				{
					result += '' + key + ': ' + macros[key].toString() + ',';
				});
				result = result.slice(0, - 1);
				result += '};';

				file.contents = new Buffer(result);
				
				cb(null, file);
			}))
			.pipe(helpers.notDev(uglify()))
			.pipe(gulp.dest(path.join(process.gulp_dest, 'javascript')));
	});

	gulp.task('watch-' + key + '-macros', function()
	{
		gulp.watch(paths[key], [key + '-macros']);
	});

});

gulp.task('macros', ['myaccount-macros', 'shopping-macros', 'checkout-macros']);
gulp.task('watch-macros', function()
{
	gulp.watch('Modules/**/*_macro.txt', ['macros']);
});