/* jshint node: true */
'use strict';

var gulp = require('gulp')
    ,	concat = require('gulp-concat')
    ,   rename = require('gulp-rename')
    ,	package_manager = require('../package-manager')
    ,	_ = require('underscore');

gulp.task('efficiencies-backend-scripts', ['clear-distribution'], function()
{
    //USE GLOBALS

    var globs = package_manager.getGlobsWithConfigByModuleFor('backend-scripts');

    var byModule = {};
    _.each(globs, function(line){
        if(!byModule[line[0]])
        {
            byModule[line[0]] = [];
        }
        byModule[line[0]].push([line[1],line[2],line[3]]);
    });

    _.forEach(byModule, function(srcs,module){


        _.each(srcs, function(script){
            var name = script[1].split('/');
            name = name[name.length-1];
            return gulp.src(script[0])
                .pipe(concat(name))
                .pipe(gulp.dest(process.gulp_dest + "/Scripts/Efficiencies/"+module+"/"+script[2]+"/"));
        });



    });




});

gulp.task('watch-efficiencies-backend-scripts', function()
{
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*.js', ['efficiencies-backend-scripts']);
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*/*.js', ['efficiencies-backend-scripts']);
});