/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	path = require('path')

,	uglify = require('gulp-uglify')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');


gulp.task('languages', function()
{
	return gulp.src(package_manager.getGlobsFor('languages'))
		.pipe(helpers.notDev(uglify()))
		.pipe(gulp.dest(path.join(process.gulp_dest, 'languages')));
});