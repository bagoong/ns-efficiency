/* jshint node: true */
'use strict';

var args   = require('yargs').argv
,	gulp = require('gulp')
,	ns = require('../ns-deploy');


gulp.task('deploy', function()
{
	var options = {};

	if (args.interactive)
	{
		options = {
			interactive: true
		};
	}
	else if (args.tag || args.description)
	{
		options = {
			tag: args.tag
		,	description: args.description
		};
	}

	return gulp.src(process.gulp_dest + '/**')
		.pipe(ns.deploy(options));

});


gulp.task('rollback', function(cb)
{
	ns.rollback(cb);
});

