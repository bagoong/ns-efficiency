/* jshint node: true */
'use strict';

var gulp = require('gulp')
    ,	concat = require('gulp-concat')
    ,   rename = require('gulp-rename')
    ,	package_manager = require('../package-manager')
    ,	_ = require('underscore');

gulp.task('efficiencies-backend-configuration', ['clear-distribution'], function()
{

    var modules = package_manager.getGlobsGroupedByModuleFor('backend-configuration')

    _.forEach(modules, function(scripts,modulename){
        return gulp.src(scripts)
            .pipe(concat(modulename +".backendlib.conf.js"))
            .pipe(gulp.dest(process.gulp_dest + "/Scripts/Efficiencies/"+modulename+"/"));
    });

});

gulp.task('watch-efficiencies-backend-configuration', function()
{
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*.js', ['efficiencies-backend-configuration']);
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*/*.js', ['efficiencies-backend-configuration']);
});