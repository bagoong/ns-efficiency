/* jshint node: true */
'use strict';

var gulp = require('gulp')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');

gulp.task('ssp-files', function()
{	
	var files_map = package_manager.getFilesMapFor('ssp-files');

	return gulp.src(Object.keys(files_map))
		.pipe(helpers.map_renamer(files_map))
		.pipe(gulp.dest(process.gulp_dest));
});
