/**
 * Created by pzignani on 19/10/2014.
 */
'use strict';

var gulp = require('gulp')
    ,	_ = require('underscore')
    ,	path = require('path')
    ,   yargs = require('yargs')
    ,	uglify = require('gulp-uglify')
    ,	concat = require('gulp-concat')
    ,	watch = require('gulp-watch')
    ,   zip = require('gulp-zip')
    ,	package_manager = require('../package-manager')
    ,	helpers = require('./helpers');


gulp.task('efficiencies-zip', function(cb)
{
    var apps = package_manager.applications;


    if(package_manager.isForDefaultSCA)
    {
        apps.global = "Global";
    }

    var prom =_.map(apps, function(appFolder,appName){
        var firstGlob = [process.gulp_dest + "/" + appFolder + "/**/*"];

        return gulp.src(firstGlob)
            .pipe(zip(appName+'.zip'))
            .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/"));


    });
    return prom;


});