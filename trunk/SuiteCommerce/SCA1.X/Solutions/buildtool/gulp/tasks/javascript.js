/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	_ = require('underscore')
,	path = require('path')

,	uglify = require('gulp-uglify')
,	concat = require('gulp-concat')
,	watch = require('gulp-watch')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');


var dest = path.join(process.gulp_dest, 'javascript');


var paths = module.exports = {
	myaccount: _.union(
		package_manager.getGlobsFor('myaccount:javascript', 'javascript')
	,	'./Modules/MyAccountApplication@*/JavaScript/Minas.starter.js'
	)
,	checkout: _.union(
		package_manager.getGlobsFor('checkout:javascript', 'javascript')
	,	'./Modules/CheckoutApplication@*/JavaScript/Minas.starter.js'
	)
,	shopping: _.union(
		package_manager.getGlobsFor('shopping:javascript', 'javascript')
	,	'./Modules/ShoppingApplication@*/JavaScript/Minas.starter.js'
	)
};


Object.keys(paths).forEach(function(key)
{
	gulp.task(key + '-javascript', function()
	{
		return gulp.src(paths[key])
			.pipe(concat(key + '-javascript.js'))
			.pipe(helpers.notDev(uglify()))
			.pipe(gulp.dest(dest));
	});

	gulp.task('watch-' + key + '-javascript', function()
	{
		gulp.watch(paths[key], [key + '-javascript'])
	});
});


gulp.task('javascript', ['myaccount-javascript', 'shopping-javascript', 'checkout-javascript']);
gulp.task('watch-javascript', function()
{
	gulp.watch('Modules/**/*.js', ['javascript'])
});