/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	concat = require('gulp-concat')
,	package_manager = require('../package-manager');

gulp.task('ssp-libraries', function()
{	
	return gulp.src(package_manager.getGlobsFor('ssp-libraries'))
		.pipe(concat('ssp_libraries.js'))
		.pipe(gulp.dest(process.gulp_dest));
});