/**
 * Created by pzignani on 19/10/2014.
 */
'use strict';

var gulp = require('gulp')
    ,	_ = require('underscore')
    ,	path = require('path')
    ,   yargs = require('yargs')
    ,	uglify = require('gulp-uglify')
    ,	concat = require('gulp-concat')
    ,	watch = require('gulp-watch')

    ,	package_manager = require('../package-manager')
    ,	helpers = require('./helpers');


gulp.task('efficiencies-manifests', ['clear-distribution'], function(cb)
{
    var apps = package_manager.applications;
    var globalGlobs = package_manager.getGlobsFor('global:manifest','manifest');

    if(package_manager.isForDefaultSCA)
    {
        apps.global = "Global";
    }

    var prom =_.map(apps, function(appFolder,appName){
        var globs = package_manager.getGlobsFor(appName+':manifest');
        if(!yargs.argv.module) {
            var firstGlob = [process.gulp_dest + "/" + appFolder + "/manifest.txt"];
        }
        else {
            firstGlob = [];
        }

        if(!package_manager.isForDefaultSCA)
        {
            var finalGlobs = firstGlob.concat(globalGlobs,globs);
        } else {
            var finalGlobs = firstGlob.concat(globs);
            if(appFolder ==='Global'){
                finalGlobs= finalGlobs.concat(globalGlobs);
            }
        }
        return gulp.src(finalGlobs)
            .pipe(concat('manifest.txt'))
            .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/"));

    });
    return prom;


});
