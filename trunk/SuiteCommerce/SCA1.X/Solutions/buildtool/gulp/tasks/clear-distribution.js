/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	_ = require('underscore')
,   yargs = require('yargs')
,   Q = require('q')
,   del = require('del')
,   dummy = require('gulp-empty')


gulp.task('clear-distribution',function (cb) {

    del.sync([process.gulp_dest],{force:true});
    if(!yargs.argv.module){
        return gulp.src('./../PS Efficiencies - SuiteCommerce - Base/**/*')
            .pipe(gulp.dest(process.gulp_dest));
    } else {
        console.log('what?');
        cb();
    }
});