/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	path = require('path')
,	package_manager = require('../package-manager');


gulp.task('images', function()
{
	return gulp.src(package_manager.getGlobsFor('images'))
		.pipe(gulp.dest(path.join(process.gulp_dest, 'img')));
});
