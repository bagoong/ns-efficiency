/* jshint node: true */
'use strict';

var gulp = require('gulp')

,	uglify = require('gulp-uglify')
,	concat = require('gulp-concat')
,	watch = require('gulp-watch')
,	jst = require('gulp-jst')
,	insert = require('gulp-insert')
,   htmlmin = require('gulp-htmlmin')

,	map = require('map-stream')
,	path = require('path')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');




function sc_templates()
{
	return map(function(file, cb)
	{
		file.contents = new Buffer('SC.templates[\'' + path.basename(file.path, '.js') + '\'] = (' + file.contents.toString() + ');');
		cb(null, file);
	});
}

var paths = module.exports = {
	myaccount: package_manager.getGlobsFor('myaccount:templates', 'templates')
,	checkout: package_manager.getGlobsFor('checkout:templates', 'templates')
,	shopping: package_manager.getGlobsFor('shopping:templates', 'templates')
};

Object.keys(paths).forEach(function(key)
{
	gulp.task(key + '-templates', function()
	{
		return gulp.src(paths[key])
            //.pipe(htmlmin())
			.pipe(jst())
			.pipe(sc_templates())
			.pipe(concat(key + '-templates.js'))
			.pipe(insert.prepend('SC.templates = {};'))
			.pipe(helpers.notDev(uglify()))
			.pipe(gulp.dest(path.join(process.gulp_dest, 'javascript')));
	});

	gulp.task('watch-' + key + '-templates', function()
	{
		gulp.watch(paths[key], [key + '-templates']);
	});
});


gulp.task('templates', ['myaccount-templates', 'shopping-templates', 'checkout-templates']);

gulp.task('watch-templates', function()
{
	gulp.watch('Modules/**/Templates/*.txt', ['templates']);
});