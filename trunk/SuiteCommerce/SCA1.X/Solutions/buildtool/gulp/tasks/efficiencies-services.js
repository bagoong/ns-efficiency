/**
 * Created by pzignani on 19/10/2014.
 */
/* jshint node: true */
'use strict';

var gulp = require('gulp')

,	path = require('path')
,	_ = require('underscore')
,	package_manager = require('../package-manager')
,	helpers = require('./helpers');

gulp.task('efficiencies-services', ['clear-distribution'], function()
{

    var apps = _.clone(package_manager.applications);

    if(package_manager.isForDefaultSCA)
    {
        apps.global = "Global";
    }

    var files_map_global = package_manager.getFilesMapForWithApp('global:services','services');

    _.each(apps, function(appFolder,appName){

        if(!package_manager.isForDefaultSCA)
        {
            gulp.src(Object.keys(files_map_global))
            .pipe(helpers.map_renamer(files_map_global))
            .pipe(gulp.dest(path.join(process.gulp_dest+'/'+appFolder+'/', 'services')));
        }
        var files_map = package_manager.getFilesMapForWithApp(appName+':services','services');

        return gulp.src(Object.keys(files_map))
            .pipe(helpers.map_renamer(files_map))
            .pipe(gulp.dest(path.join(process.gulp_dest+'/'+appFolder+'/', 'services')));
    });

});
