/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	concat = require('gulp-concat')
,   rename = require('gulp-rename')
,	package_manager = require('../package-manager')
,	_ = require('underscore');

gulp.task('efficiencies-backend-libraries', ['clear-distribution'], function()
{

    var modules = package_manager.getGlobsGroupedByModuleFor('backend-libraries')

    _.forEach(modules, function(scripts,modulename){
        return gulp.src(scripts)
            .pipe(concat(modulename +".backendlib.js"))
            .pipe(gulp.dest(process.gulp_dest + "/Scripts/Efficiencies/"+modulename+"/"));
    });

});

gulp.task('watch-efficiencies-backend-libraries', function()
{
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*.js', ['efficiencies-backend-libraries']);
    gulp.watch('./../EfficienciesModules/**/SuiteScript/*/*.js', ['efficiencies-backend-libraries']);
});