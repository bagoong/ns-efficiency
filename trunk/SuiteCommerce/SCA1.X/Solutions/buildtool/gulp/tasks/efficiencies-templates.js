/**
 * Created by pzignani on 19/10/2014.
 */
'use strict';

var gulp = require('gulp')
,	_ = require('underscore')
,	path = require('path')

,	uglify = require('gulp-uglify')
,	concat = require('gulp-concat')
,	watch = require('gulp-watch')

,	package_manager = require('../package-manager')
,	helpers = require('./helpers');


gulp.task('efficiencies-templates', ['clear-distribution'], function()
{
    var apps = package_manager.applications;
    var globalGlobs = package_manager.getGlobsByModuleFor('global:templates','templates');

    if(package_manager.isForDefaultSCA)
    {
        apps.global = "Global";
    }

    _.each(apps, function(appFolder,appName){
        var globs = package_manager.getGlobsByModuleFor(appName+':templates','templates');

        if(!package_manager.isForDefaultSCA)
        {
            _.each(globalGlobs, function (module)
            {
                return gulp.src(module[1])
                    //.pipe(concat('Efficiencies_configuration.js'))
                .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/templates/Efficiencies/" + module[0] + "/"));
            });
        } else {
            _.each(globalGlobs, function (module)
            {
                return gulp.src(module[1])
                    //.pipe(concat('Efficiencies_configuration.js'))
                    .pipe(gulp.dest(process.gulp_dest + "/Global/templates/Efficiencies/" + module[0] + "/"));
            });
        }
        _.each(globs, function (module)
        {
            return gulp.src(module[1])
                //.pipe(concat('Efficiencies_configuration.js'))
            .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/templates/Efficiencies/" + module[0] + "/"));
        });

    });
});

gulp.task('watch-efficiencies-templates', function()
{
    gulp.watch('./../EfficienciesModules/**/*.txt', ['efficiencies-templates']);
});