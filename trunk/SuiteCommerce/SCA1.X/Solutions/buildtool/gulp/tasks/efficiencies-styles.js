/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	concat = require('gulp-concat')
,   rename = require('gulp-rename')
,	less = require('gulp-less')
,	package_manager = require('../package-manager')
,	_ = require('underscore');

gulp.task('efficiencies-styles', ['clear-distribution'], function()
{
    var apps = package_manager.applications;
    var globalGlobs = package_manager.getGlobsByModuleFor('global:styles','styles');

    if(package_manager.isForDefaultSCA)
    {
        apps.global = "Global";
    }

    _.each(apps, function(appFolder,appName){
        var globs = package_manager.getGlobsByModuleFor(appName+':styles');

        if(!package_manager.isForDefaultSCA)
        {
            _.each(globalGlobs, function (module)
            {
                return gulp.src(module[1])
                .pipe(less())
                    //.pipe(concat('Efficiencies_configuration.js'))
                .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/skins/standard/modules/Efficiencies/" + module[0] + "/"));
            });
        } else {
            _.each(globalGlobs, function (module)
            {
                return gulp.src(module[1])
                    .pipe(less())
                    //.pipe(concat('Efficiencies_configuration.js'))
                    .pipe(gulp.dest(process.gulp_dest + "/Global/skins/standard/modules/Efficiencies/" + module[0] + "/"));
            });
        }
        _.each(globs, function (module)
        {
            return gulp.src(module[1])
            .pipe(less())
                //.pipe(concat('Efficiencies_configuration.js'))
            .pipe(gulp.dest(process.gulp_dest + "/" + appFolder + "/skins/standard/modules/Efficiencies/" + module[0] + "/"));
        });

    });
});

gulp.task('watch-efficiencies-styles', function()
{
    gulp.watch('./../EfficienciesModules/**/Styles/*.less', ['efficiencies-styles']);
});