/* jshint node: true */
'use strict';

var gulp = require('gulp')
    ,	concat = require('gulp-concat')
    ,   rename = require('gulp-rename')
    ,   copy = require('gulp-copy')
    ,	package_manager = require('../package-manager')
    ,	_ = require('underscore');

gulp.task('efficiencies-copy', ['clear-distribution'], function()
{
    //USE GLOBALS

    var globs = package_manager.getGlobsByZ('copy');


    return _.map(globs, function(glob){
        console.log(glob.file);
        var d = process.gulp_dest + '/' +glob.scope + '/'+ glob.destination;
        console.log(d)
        return gulp.src(glob.file)
            .pipe(rename('/'+ glob.destination))
            .pipe(gulp.dest(process.gulp_dest + '/' +glob.scope));
    });

});
