/* jshint node: true */
'use strict';

var fs = require('fs')
,	mime = require('mime');

var binnary_types = [
	'application/x-autocad'
,	'image/x-xbitmap'
,	'application/vnd.ms-excel'
,	'application/x-shockwave-flash'
,	'image/gif'
,	'application/x-gzip-compressed'
,	'image/ico'
,	'image/jpeg'
,	'message/rfc822'
,	'audio/mpeg'
,	'video/mpeg'
,	'application/vnd.ms-project'
,	'application/pdf'
,	'image/pjpeg'
,	'image/x-png'
,	'image/png'
,	'application/postscript'
,	'application/vnd.ms-powerpoint'
,	'video/quicktime'
,	'application/rtf'
,	'application/sms'
,	'image/tiff'
,	'application/vnd.visio'
,	'application/msword'
,	'application/zip'
];


module.exports = {

	read: function (deploy, cb)
	{
		fs.readFile('.nsdeploy', {encoding: 'utf8'}, function(err, file)
		{
			if (err && err.code !== 'ENOENT')
			{
				// unknown error 
				return cb(err);
			}
			else if (err && err.code === 'ENOENT')
			{
				// File does not exists
				return cb(null, deploy);
			}
			else
			{
				// file is present 
				deploy.info = JSON.parse(file);
				return cb(null, deploy);
			}
		});
	}

,	write: function(deploy, cb)
	{
		var password = deploy.info.password;
		delete deploy.info.password;

		fs.readFile('.nsdeploy', {encoding: 'utf8'}, function(err, file)
		{
			var result = {};
			if (err)
			{
				// File does not exists
				result = deploy.info;
			}
			else
			{
				// file is present 
				result = JSON.parse(file);

				Object.keys(deploy.info).forEach(function(key)
				{
					result[key] = deploy.info[key];
				});
			}

			fs.writeFile('.nsdeploy', JSON.stringify(result, '\t', 4), function()
			{
				deploy.info.password = password;
				return cb(null, deploy);
			});
		});
	}

,	processFiles: function(deploy, cb)
	{
		deploy.files = deploy.files.map(function(file)
		{
			var type = mime.lookup(file.path);

			return {
				path: file.path.replace(file.base, '')
			,	type: type
			,	contents: file.contents.toString(~binnary_types.indexOf(type) ? 'base64' : 'utf8')
			};
		});

		cb(null, deploy)
	}
};