/* jshint node: true */
'use strict';

var request = require('request')
,	through = require('through2')
,	Progress = require('progress')
,	Spinner = require('cli-spinner').Spinner
,	https = require('https')
,	fs = require('fs')
,	url = require('url');

function getAuthorizationHeader(deploy)
{
	return	'NLAuth nlauth_account=' + deploy.info.account + ', ' + 
			'nlauth_email=' + deploy.info.email + ', ' +
			'nlauth_signature=' + deploy.info.password + ', ' +
			'nlauth_role=' + deploy.info.role;
}

module.exports = {

	rollback: function(deploy, cb)
	{
		if (!deploy.rollback_revision)
		{
			cb(new Error("No backup selected"));
		}
		else
		{
			request.put(
				url.format({
					protocol: 'https'
				,	hostname: deploy.info.hostname
				,	pathname: '/app/site/hosting/restlet.nl'
				,	query: {
						script: deploy.info.script
					,	deploy: deploy.info.deploy
					,	t: Date.now()
					}
				})
			,	{
					headers: {
						'Content-Type': 'application/json'
					,	'Authorization': getAuthorizationHeader(deploy)
					}
				,	body: JSON.stringify({ rollback_to: deploy.rollback_revision.file_id })
				}
			,	function(err, request, response_body)
				{
					cb(null, deploy);
				}
			);
		}
	}

,	getVersions: function (deploy, cb)
	{
		if (deploy.revisions)
		{
			cb(null, deploy);
		}
		else
		{
			request.get(
				url.format({
					protocol: 'https'
				,	hostname: deploy.info.hostname
				,	pathname: '/app/site/hosting/restlet.nl'
				,	query: {
						script: deploy.info.script
					,	deploy: deploy.info.deploy
					,	t: Date.now()
					,	get: 'revisions'
					,	target_folder: deploy.info.target_folder
					}
				})
			,	{
					headers: {
						'Content-Type': 'application/json'
					,	'Authorization': getAuthorizationHeader(deploy)
					}
				}
			,	function(err, request, response_body)
				{
					if (err)
					{
						cb(err);
					}
					else
					{
						var response = JSON.parse(response_body);
						if (response.error)
						{
							cb(new Error(response.error.message));
						}
						else 
						{
							deploy.revisions = response;
							cb(null, deploy);
						}
					}
				}
			);
		}
	}

,	roles: function (deploy, cb)
	{
		if (deploy.info.role && deploy.info.account)
		{
			cb(null, deploy);
		}
		else if (deploy.info.email && deploy.info.password)
		{
			request.get(
				'https://rest.netsuite.com/rest/roles'
			,	{
					headers: {
						'Accept': '*/*'
					,	'Accept-Language': 'en-us'
					,	'Authorization': 'NLAuth nlauth_email=' + deploy.info.email + ', nlauth_signature=' + deploy.info.password
					}
				}
			,	function(err, request, response_body)
				{
					if (err)
					{
						return cb(err);
					}
					else
					{
						var response = JSON.parse(response_body);
						if (response.error)
						{
							var error = new Error(response.error.message);
							error.type = 'NETWORK_ROLES_ERROR';
							cb(error);
						}
						else 
						{
							deploy.roles = response;
							cb(null, deploy);
						}
					}
				}
			);
		}
		else 
		{
			var error = new Error('Missing email and password');
			error.type = 'MISSING_EMAIL_OR_PASSWORD';
			cb(error);
		}
	}

,	targetFolder: function (deploy, cb)
	{
		if (deploy.info.target_folder)
		{
			cb(null, deploy);
		}
		else
		{
			request.get(
				url.format({
					protocol: 'https'
				,	hostname: deploy.info.hostname
				,	pathname: '/app/site/hosting/restlet.nl'
				,	query: {
						script: deploy.info.script
					,	deploy: deploy.info.deploy
					,	t: Date.now()
					,	get: 'target-folders'
					}
				})
			,	{
					headers: {
						'Content-Type': 'application/json'
					,	'Authorization': getAuthorizationHeader(deploy)
					}
				}
			,	function(err, request, response_body)
				{
					if (err)
					{
						cb(err);
					}
					else
					{
						var response = JSON.parse(response_body);
						if (response.error)
						{
							cb(new Error(response.error.message));
						}
						else 
						{
							deploy.target_folders = response;
							cb(null, deploy);
						}
					}
				}
			);
		}
	}

,	postFiles: function(deploy, cb)
	{


		fs.writeFile(
			'payload.json'
		,	JSON.stringify({
				target_folder: deploy.info.target_folder
			,	backup_info: deploy.backup_info
			,	files: deploy.files
			})
		,	function()
			{
				fs.stat('payload.json', function(err, stat)
				{
					if (err)
					{
						return cb(err);
					}

					var spinner = new Spinner('Processing');
					var bar = new Progress('Uploading [:bar] :percent', {
						complete: '='
					,	incomplete: ' '
					,	width: 50
					,	total: stat.size
					,	callback: function()
						{
							spinner.start();
						}
					});

					fs.createReadStream('payload.json')
						.pipe(through(
							function(buff, type, cb)
							{
								bar.tick(buff.length);
								this.push(buff);
								return cb();
							}
						))
						.pipe(request.post(
							url.format({
								protocol: 'https'
							,	hostname: deploy.info.hostname
							,	pathname: '/app/site/hosting/restlet.nl'
							,	query: {
									script: deploy.info.script
								,	deploy: deploy.info.deploy
								}
							})
						,	{
								headers: {
									'Content-Type': 'application/json'
								,	'Authorization': getAuthorizationHeader(deploy)
								}
							}
						,	function(err, request, response_body)
							{
								spinner.stop();
								process.stdout.clearLine();
								process.stdout.cursorTo(0);


								var result = JSON.parse(response_body);

								console.log(result.files.length + ' Files Uploaded');
								cb(null, deploy);
							}
						)
					);

				});
			}
		);




		var options = {
				host: deploy.info.hostname,
				path: '/app/site/hosting/restlet.nl?script=customscript_sca_deployer&deploy=customdeploy_sca_deployer',
				headers: {
					'Content-Type': 'application/json'
				,	'Authorization': getAuthorizationHeader(deploy)
				},
				method: 'POST'
			}

		,	callback = function(response) 
			{
				var str = '';
				response.on('data', function (chunk) 
				{
					str += chunk;
				});

				response.on('end', function () 
				{
					cb(null, deploy);
				});
			};

		var req = https.request(options, callback);


	}
};
