/* jshint node: true */
'use strict';


var async = require('async')
,	through = require('through2')
,	gutil = require('gulp-util')

,	fs = require('./fs')
,	ui = require('./ui')
,	net = require('./net');

var default_info = {
	script: 'customscript_sca_deployer'
,	deploy: 'customdeploy_sca_deployer'
};

function deploy(params)
{
	var files = [];

	return through.obj(
		// each callbac
		function(file, enc, callback) 
		{
			/*jshint validthis:true */
			if (file.isNull()) 
			{
				this.push(file);
			}
			else if (file.isStream())
			{
				this.emit('error', new gutil.PluginError('gulp-ns-deploy', 'Stream content is not supported'));
			}
			else if (file.isBuffer()) 
			{
				files.push(file);
			}
			return callback();
		}
		// End Callbacka
	,	function()
		{
			/*jshint validthis:true */
			var self = this;
			async.waterfall([
					function (cb)
					{
						cb(null, {info: default_info, options: params, files: files});
					}
				,	fs.read
				,	ui.email
				,	ui.password
				,	net.roles
				,	ui.roles
				,	net.targetFolder
				,	ui.targetFolder
				,	fs.write
				,	ui.backup
				,	fs.processFiles
				,	net.postFiles
				]
			,	function (err)
				{
					if (err)
					{
						return self.emit('error', new gutil.PluginError('gulp-ns-deploy', err.message));
					}
					self.emit('end');
				}
			);
		}
	);
}
	
function rollback(cb)
{
	async.waterfall([
			function (cb)
			{
				cb(null, {info: default_info});
			}
		,	fs.read
		,	ui.email
		,	ui.password
		,	net.roles
		,	ui.roles
		,	net.targetFolder
		,	ui.targetFolder
		,	fs.write
		,	net.getVersions
		,	ui.rollback
		,	net.rollback
		]
	,	cb
	);

}


module.exports = {
	deploy: deploy
,	rollback: rollback
};
