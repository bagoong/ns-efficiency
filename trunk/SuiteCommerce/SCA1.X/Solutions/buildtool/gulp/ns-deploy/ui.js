/* jshint node: true */
'use strict';

var inquirer = require('inquirer');


module.exports = {

	rollback: function(deploy, cb)
	{
		if (deploy.rollback_revision)
		{
			cb(null, deploy);
		}
		else
		{
			var longest = 0;
			var choices = deploy.revisions
				.sort(function(a, b)
				{
					return b.deployment_time - a.deployment_time;
				})
				.map(function(revision) 
				{
					var name = '' + new Date(revision.deployment_time).toISOString().slice(0, 19).replace('T', ' ');
					if (revision.backup_info && revision.backup_info.tag)
					{
						name += ' - ' + revision.backup_info.tag;
					}
 
					longest = (longest < name.length) ? name.length : longest;
					return {
						name: name
					,	value: revision
					};
				});

			choices.push(new inquirer.Separator(new Array( longest + 1 ).join('-')));

			inquirer.prompt(
				{
					type: 'list'
				,	name: 'revision'
				,	message: 'Choose the backup you want to rollback to'
				,	choices: choices
				}
			,	function(answers)
				{
					deploy.rollback_revision = answers.revision;
					cb(null, deploy);
				}
			);
		}
	
	}

,	email: function(deploy, cb)
	{
		if (deploy.info.email)
		{
			cb(null, deploy);
		}
		else
		{
			inquirer.prompt(
				{
					type: 'input'
				,	name: 'email'
				,	message: 'Netsuite\'s email'
				,	validate: function(input)
					{
						var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						return re.test(input) || 'Please enter a valid email address';
					}
				}
			,	function(answers)
				{
					deploy.info.email = answers.email;
					cb(null, deploy);
				}
			);
		}
	}

,	password: function(deploy, cb)
	{
		if (deploy.info.password)
		{
			cb(null, deploy);
		}
		else
		{
			inquirer.prompt(
				{
					type: 'password'
				,	name: 'password'
				,	message: 'Netsuite\'s password'
				,	validate: function(input)
					{
						return input.length > 0 || 'Please enter a password';
					}
				}
			,	function(answers)
				{
					deploy.info.password = answers.password;
					cb(null, deploy);
				}
			);
		}
	}

,	roles: function(deploy, cb)
	{
		function setRole(role)
		{
			deploy.info.account = role.account.internalId;
			deploy.info.role = role.role.internalId;
			deploy.info.hostname = role.dataCenterURLs.restDomain.split('://')[1];

			delete deploy.roles;
			cb(null, deploy);
		}

		if (deploy.info.role && deploy.info.account)
		{
			cb(null, deploy);
		}
		else if (deploy.roles.length === 1)
		{
			setRole(deploy.roles[0]);
		}
		else
		{
			inquirer.prompt(
				{
					type: 'list'
				,	name: 'role'
				,	message: 'Choose your target account and role'
				,	choices: deploy.roles.map(function(role) { return {name: role.account.name + ' - ' + role.role.name, value: role }; })
				}
			,	function(answers)
				{
					setRole(answers.role);
				}
			);
		}
		
	}


,	targetFolder: function(deploy, cb)
	{
		if (deploy.info.target_folder)
		{
			cb(null, deploy);
		}
		else
		{
			inquirer.prompt(
				{
					type: 'list'
				,	name: 'hosting'
				,	message: 'Choose your Hosting Files folder'
				,	choices: deploy.target_folders.map(function(hosting) { return {name: hosting.name, value: hosting }; })
				}
			,	function(answers)
				{
					inquirer.prompt(
						{
							type: 'list'
						,	name: 'publisher'
						,	message: 'Choose your Application Publisher'
						,	choices: answers.hosting.publishers.map(function(publisher) { return {name: publisher.name, value: publisher }; })
						}
					,	function(answers)
						{
							inquirer.prompt(
								{
									type: 'list'
								,	name: 'application'
								,	message: 'Choose your SSP Application'
								,	choices: answers.publisher.application.map(function(application) { return {name: application.name, value: application.id }; })
								}
							,	function(answers)
								{
									deploy.info.target_folder = answers.application;
									cb(null, deploy);
								}
							);
						}
					);
				}
			);
		}
	}

,	backup: function(deploy, cb)
	{
		if (deploy.options.interactive)
		{

			inquirer.prompt(
				[
					{
						type: 'input'
					,	name: 'tag'
					,	message: 'Please, name this change'
					,	validate: function(input)
						{
							return !!input;
						}
					}
				,	{
						type: 'input'
					,	name: 'description'
					,	message: 'Please enter a description for this change'
					,	validate: function(input)
						{
							return !!input;
						}
					}
				]
			,	function(answers)
				{
					deploy.backup_info = answers;
					cb(null, deploy);
				}
			);
		}
		else if (deploy.options && deploy.options.tag)
		{
			deploy.backup_info = {
				tag: deploy.options.tag
			,	description: deploy.options.description
			};
			cb(null, deploy);
		}
		else
		{
			cb(null, deploy);
		}
	}

};