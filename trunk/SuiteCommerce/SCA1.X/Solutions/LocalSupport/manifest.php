<?php

/**
 * DONE:
 *  - allow comments in json
 *  - default value in getConfig
 *  - generate for current folder
 *  - exclude using wildcards
 *
 * TODO:
 *  - check duplicate templates and/or macros
 *  - check unused entries in templates map
 *  - check unused entries in exclude config
 *  - check unused entries in folders
 */

include_once __DIR__.'/include.php';

$app = Config::getAppFromUrl($_GET['app']);
$internalPath = $_GET['path'];

$localConfig = Config::getConfig('local');
$configPrecedences = Config::getConfig('precedences');

if(array_key_exists($app, $configPrecedences)) {
    $appPath = array_pop($configPrecedences[$app]);

    $basePath = __DIR__.'/../'.$localConfig['hosting_folder'].'SSP Applications/'.$appPath;

    $relToDir = $basePath.$internalPath;

    $manifest = new Manifest($relToDir);
    $manifest->process();

    if($manifest->writeToFile()) {
        $line_jump = "<br />"."\n";
        if($manifest->getConfig('backup', false)) {
            echo "Manifest backed up successfully".$line_jump.$line_jump;
        }
        $warnings = $manifest->getWarnings();
        if(is_array($warnings) && count($warnings) > 0) {
            echo "WARNINGS: ".$line_jump.$line_jump;
            foreach($warnings as $warning) {
                echo $warning.$line_jump;
            }
            echo $line_jump;
        }
        else {
            echo "NO WARNINGS!".$line_jump.$line_jump;
        }
        echo "Manifest generated successfully: \"".$manifest->getConfig('output', 'manifest-gen.txt')."\"".$line_jump.$line_jump;
        echo $manifest->getContent();
    }
    else {
        echo 'Error while generating manifest.';
    }
}
else {
    header('HTTP/1.0 404 Not Found');

    echo "File not found";
}