<?php
include_once __DIR__.'/inc/Config.class.php';
include_once __DIR__.'/inc/Types.enum.php';
include_once __DIR__.'/inc/Utils.class.php';
include_once __DIR__.'/inc/Warnings.classes.php';
include_once __DIR__.'/inc/Manifest.class.php';
include_once __DIR__.'/inc/Precedence.class.php';
include_once __DIR__.'/inc/Combiner.class.php';
include_once __DIR__.'/inc/CombinerCss.class.php';
include_once __DIR__.'/inc/CombinerJs.class.php';
include_once __DIR__.'/inc/CombinerTmpl.class.php';
include_once __DIR__.'/inc/Factory.class.php';