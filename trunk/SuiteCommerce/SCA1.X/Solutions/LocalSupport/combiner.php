<?php
ini_set('default_charset', 'UTF-8');

include_once __DIR__.'/include.php';

$app = Config::getAppFromUrl($_GET['app']);
$internalPath = $_GET['path'];
$combinerName = $_GET['combiner'];

$localConfig = Config::getConfig('local');
$configPrecedences = Config::getConfig('precedences');
$precedences = $configPrecedences[$app];

$basePath = __DIR__.'/../'.$localConfig['hosting_folder'].'SSP Applications/';

$precedence = new Precedence($basePath, $precedences);

$combiner = Factory::getCombiner(Types::infer($combinerName));
$combiner->initialize($precedence, $internalPath, $app);

$combiner->combine();
$combiner->sendHeaders();
$combiner->output();