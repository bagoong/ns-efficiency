var Handler = (function(){


    var context = nlapiGetContext();
    var INSTANCE_STATUS = {
        NOT_SCHEDULED : 1,
        SCHEDULED : 2, //On script deployment to run queue
        PROCESSING : 3, //Schedule Script running
        FINISHED_OK: 4,
        FINISHED_ERROR: 5,
        WAITING: 6 //This means there's another instance running. We will need to wait for the first one to finish.
    };

    var SCRIPT_ID = 'customscript_ef_pfg_schedule_script';
    var DEPLOY_ID = 'customdeploy_ef_pfg_schedule_from_script';


    function beforeLoad(pType, form, request){

        var type = pType.toString();

        if (type === 'create' || type === 'copy' || type === 'edit')
        {
            var localeField = form.addField('custpage_fake_locale', 'select', 'Locale');
            var currencyField = form.addField('custpage_fake_currency', 'select', 'Currency');
            form.insertField(localeField,'custrecord_ef_pfg_fi_template');
            form.insertField(currencyField,'custpage_fake_locale');

            if(request && context.getExecutionContext() === 'userinterface')
            {
                var record = nlapiGetNewRecord();


                var url = request.getURL();
                var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
                var domain = matches && matches[1];  // domain will be null if no match is found

                record.setFieldValue('custrecord_ef_pfg_fi_molecule',domain);
            }



        }


    }

    function beforeSubmit(type)
    {
        return true;
    }

    function afterSubmit(type)
    {

        //It has to be after submit because we need the id!
        var record = nlapiGetNewRecord();
        var action_triggered = record.getFieldValue('custrecord_ef_pfg_fi_action_trigger');
        var status = record.getFieldValue('custrecord_ef_pfg_fi_status');

        if( action_triggered && action_triggered.toString() === 'T' && parseInt( status && status.toString(),10) === INSTANCE_STATUS.NOT_SCHEDULED)
        {
            var data = {};
            var res = nlapiScheduleScript(SCRIPT_ID, DEPLOY_ID, {'custscript_ef_pfg_sched_instance': record.getId()});
            if(res && res.toString() === 'QUEUED')
            {
                data = {
                    'custrecord_ef_pfg_fi_status': INSTANCE_STATUS.SCHEDULED,
                    'custrecord_ef_pfg_fi_ss_instance': res
                };

            } else {
                data = {
                    'custrecord_ef_pfg_fi_status': INSTANCE_STATUS.WAITING,
                    'custrecord_ef_pfg_fi_ss_instance': res
                };
            }

            nlapiSubmitField('customrecord_ef_pfg_feed_instance', record.getId(), _.keys(data), _.values(data));


        }





    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };

}());