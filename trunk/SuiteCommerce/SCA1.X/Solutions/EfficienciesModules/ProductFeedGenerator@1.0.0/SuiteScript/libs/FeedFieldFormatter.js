define('FeedFieldFormatter', ['underscore','he','strip_tags'], function(_,he,strip_tags){


    var extraFeedFields = {};

    try {
        var r = require('CustomFormatter');
        extraFeedFields.transformations = r.transformations;
        extraFeedFields.magicfields = r.magicfields;
    } catch (e)
    {
        console.log('No Custom Formatting');
    }

    function itemImageFlatten (images)
    {
        if ('url' in images && 'altimagetext' in images)
        {
            return [images];
        }

        return _.flatten(_.map(images, function (item)
        {
            if (_.isArray(item))
            {
                return item;
            }

            return itemImageFlatten(item);
        }));
    }


    return function(pConfig){

        var transforms = {
            decode: function(value){
                return he.decode(value);
            },
            stripTags: function(value)
            {
                return strip_tags(value);
            },
            toLowerCase: function(value){
                return value && value.toString() && value.toString().toLocaleLowerCase();
            },
            toUpperCase: function(value){
                return value && value.toString() && value.toString().toLocaleUpperCase();
            }
        };
        var magicFields =  {
            /* ------------------------------------------------------------------------------------------------------ */
            _category: {
                conversor: function(item){
                    if(item.defaultcategory_detail && item.defaultcategory_detail.length){
                        return _.pluck(item.defaultcategory_detail.slice(1,item.defaultcategory_detail.length),'label').join('/');
                    } else {
                        return '';
                    }

                },
                fields: ['defaultcategory_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _categoryChild: {
                conversor: function(item){
                    if(item.defaultcategory_detail && item.defaultcategory_detail.length){
                        var c = _.pluck(item.defaultcategory_detail.slice(1,item.defaultcategory_detail.length),'label');
                        return c[c.length - 1];
                    } else {
                        return '';
                    }
                },
                fields: ['defaultcategory_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _categoryRoot: {
                conversor: function(item){
                    if(item.defaultcategory_detail && item.defaultcategory_detail.length){
                        var c = _.pluck(item.defaultcategory_detail.slice(1,item.defaultcategory_detail.length),'label');
                        return c[0];
                    } else {
                        return '';
                    }

                },
                fields: ['defaultcategory_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _childImage: {
                conversor: function(item){


                    var result = []
                        ,	selected_options = item.itemOptions
                        ,	itemimages_detail = item.itemimages_detail || {}
                        ,   itemimages_detail = itemimages_detail.media || itemimages_detail;

                    result = itemImageFlatten(itemimages_detail);

                    return result && result[0] && result[0].url;
                },
                fields: ['itemimages_detail','itemoptions_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _isChild: {
                conversor: function(item){
                    return item.child ? 'YES':'NO'
                },
                fields: []
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _mainImage: {
                conversor: function(item){


                    var result = []
                        ,	selected_options = item.itemOptions
                        ,	itemimages_detail = item.itemimages_detail || {}
                        ,   itemimages_detail = itemimages_detail.media || itemimages_detail;

                    result = itemImageFlatten(itemimages_detail);

                    return result && result[0] && result[0].url;
                },
                fields: ['itemimages_detail','itemoptions_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _parentId: {
                conversor: function(item){
                    return item.parent;
                },
                fields: []
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _price:
            {
                conversor: function(item){
                    return item.onlinecustomerprice_detail && item.onlinecustomerprice_detail.onlinecustomerprice;
                },
                fields: ['onlinecustomerprice_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _priceFormatted:
            {
                conversor: function(item){
                    return item.onlinecustomerprice_detail && item.onlinecustomerprice_detail.onlinecustomerprice_formatted;
                },
                fields: ['onlinecustomerprice_detail']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _urlcomponent: {
                conversor: function(item){
                    if(item.urlcomponent){
                        return 'http://' + config.getWebsite().currentDomain + '/' + item.urlcomponent;
                    } else {
                        return 'http://' + config.getWebsite().currentDomain + '/product/' + item.internalid;
                    }
                },
                fields: ['internalid','urlcomponent']
            },
            /* ------------------------------------------------------------------------------------------------------ */
            _urlcomponentRelative: {
                conversor: function(item){
                    if(item.urlcomponent){
                        return '/' + item.urlcomponent;
                    } else {
                        return '/product/' + item.internalid;
                    }
                },
                fields: ['internalid','urlcomponent']
            }


        };

        if(extraFeedFields)
        {
            _.extend(transforms, extraFeedFields.transformations);
            _.extend(magicFields, extraFeedFields.magicFields)
        }

        var isMagicField = function(field){
            return !!magicFields[field];
        };

        var config = pConfig,
            fieldsHashMap = function(fs){
                var fHash = {};
                _.each(fs, function(f){
                    fHash[f.fieldid] = f;
                });
                return fHash;
            }(pConfig.getFields()),
            fieldIds = _.pluck(fieldsHashMap,'fieldid');


        return {
            getRequiredApiFields: function(){
                var fieldsForApi = [],
                    magicFieldsPicked = _.intersection(_.keys(magicFields), fieldIds),
                    notMagicFields = _.difference(fieldIds,magicFieldsPicked);

                _.each(_.pick(magicFields,magicFieldsPicked), function(value){
                    fieldsForApi = fieldsForApi.concat(value.fields);
                });

                fieldsForApi = fieldsForApi.concat(notMagicFields);
                fieldsForApi = _.unique(fieldsForApi);

                return fieldsForApi;
            },
            format: function(items){
                return _.map(items, function(item){
                    var obj = {};
                    _.each(fieldsHashMap, function(field){
                        var value;


                        //Execute magic field transformations (one)
                        if(isMagicField(field.fieldid)){
                            value = magicFields[field.fieldid].conversor(item);
                        } else {
                            value = item[field.fieldid];
                        }
                        //safe call for all helpers;
                        value = value || '';

                        //Execute general transformations (N)
                        if(field.transform && field.transform.length)
                        {
                            value = _.reduce(field.transform, function(currentValue,transformName){
                                var fn = transforms[transformName];
                                if(fn){
                                    return fn(currentValue);
                                } else {
                                    return currentValue;
                                }
                            },value);
                        }

                        obj[field.title] = value;
                    });
                    return obj;
                });
            }
        }
    };
});

