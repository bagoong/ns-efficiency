define('ItemApiQuerier', ['underscore'],function(_){

    return function(pConfig,pFields){

        var items = [],
            fields = _.union(basicFields,pFields),
            config = pConfig,
            facets = config.getFacets(),
            website = config.getWebsite(),
            nextURL,
            total = 0;

        var basicFields = [
            'internalid',
            'matrixchilditems_detail',
            'itemoptions_detail'
        ];

        var getApiBaseHeaders = function(){
            return {
                "Accept-Encoding":"gzip, deflate",
                "Accept":"application/json"
            };
        };

        var addParamsToUrl = function(baseUrl, params)
        {
            if (params && _.keys(params).length)
            {
                var paramString = encodeParams(params)
                    ,	join_string = ~baseUrl.indexOf('?') ? '&' : '?';

                return baseUrl + join_string + paramString;
            }
            else
            {
                return baseUrl;
            }
        };

        var encodeParams = function( params ) {
            return _.map(params, function(v,k){
                return encodeURIComponent(k) + '=' + encodeURIComponent(v);
            }).join('&');
        };

        var getNextLink =  function(body){
            var link =_.findWhere(body, {rel: "next"});
            return link && link.href;
        };

        var getData = function(url)
        {
            var request,
                responseBody,
                parsedBody;

            try
            {
                request = nlapiRequestURL(url,null,getApiBaseHeaders());
            }
            catch (e)
            {
                throw nlapiCreateError('ERR_REQUESTING_URL',url);
            }

            responseBody = request.getBody();

            try
            {
                parsedBody = JSON.parse(responseBody);

            }
            catch(e)
            {
                throw nlapiCreateError('ERR_JSON',responseBody);
            }

            if(parsedBody.code != 200)
            {
                throw nlapiCreateError('ERR_API', JSON.stringify(parsedBody.errors));
            }

            items = parsedBody.items;
            total = parsedBody.total;

            nextURL = getNextLink(parsedBody.links);

        };

        var formURL = function(){
            var url = 'http://' + website.currentDomain + '/api/items?',
                baseParams = {
                    n: website.internalid,
                    c: website.companyId,
                    pricelevel: website.pricelevel,
                    language: website.currentLanguage,
                    currency: website.currentCurrency,
                    country: website.currentCountry
                },
                extraParams = {
                    limit: config.getItemsPerCall(),
                    offset: 0,
                    ssdebug: 'T'
                };

            var newUrl = addParamsToUrl(url, _.extend(baseParams,extraParams,facets))+'&fields=' + fields.join(',');


            return newUrl;
        };


        nextURL = formURL(website);



        return {
            getTotal: function(){
                return total;
            },
            getItems: function(){
                return items;
            },
            hasNext: function(){
                return !!nextURL;
            },
            getNext: function(){
                if(nextURL) {
                    return getData(nextURL);
                } else {
                    throw nlapiCreateError('ERR_FETCHING_API','End of iterator reached');
                }
            }
        }
    };

});
