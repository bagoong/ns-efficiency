define('ConfigurationProvider', [], function(){


    return function(feedTemplateInstance,pFolderId){

        var instance = feedTemplateInstance;
        var items_per_call = 100;
        var website = Application.getModel('Website').get(instance.website);
        var folderId = pFolderId;
        var facets = {};
        if(feedTemplateInstance.extraFacetsRaw){
            try {
                _.extend(facets, JSON.parse(feedTemplateInstance.extraFacetsRaw));
            } catch(e)
            {
                console.error('JSON PARSING', 'Error parsing extra facets JSON field');
            }
        }
        if( feedTemplateInstance.filterByFeed == 'T' )
        {
            _.extend(facets,{
                custitem_ef_pfg_templates:encodeURIComponent(feedTemplateInstance.template.name)
            });
        }



        return {
            getFileName: function(){
                return instance.internalid + '-' + instance.template.name;
            },
            getItemsPerCall: function(){
                return items_per_call;
            },
            getMolecule: function()
            {
                return instance.molecule;
            },
            getWebsite: function()
            {
                var realReturn = {
                    internalid: instance.website,
                    companyId: nlapiGetContext().getCompany(),
                    currentDomain: website.domain.domain,
                    currentCountry: instance.country,
                    currentCurrency: instance.currency,
                    currentLanguage: instance.language,
                    pricelevel: website.onlinepricelevel,
                    displayname: website.displayname
                };

                return realReturn;
            },
            getFields: function()
            {
                return instance.template.fields;
            },
            getFacets: function()
            {
                return facets;
            },
            getStorageFolder: function(){
                return folderId;
            }
        }
    };

});

