Application.defineModel('FeedField', {
    record: 'customrecord_ef_pfg_feed_field',
    columns: {
        'title': {fieldName: 'name'},
        'template': {fieldName: 'custrecord_ef_pfg_ff_template'},
        'order': {fieldName:'custrecord_ef_pfg_ff_order'},
        'realfield': {fieldName: 'custrecord_ef_pfg_ff_field_scriptid'},
        'magicfield': {fieldName: 'custrecord_ef_pfg_mf_function_ref', joinKey: 'custrecord_ef_pfg_ff_magic_field'},
        'transform': {fieldName:'custrecord_ef_pfg_ff_transform'}
    },
    filters: [
        {fieldName: 'isinactive', operator: 'is', value1: 'F'}
    ],
    getForTemplate: function(templateId){
        var Search = new SearchHelper(this.record, this.filters, this.columns),
            feedTemplate;

        Search.addFilter({fieldName: this.columns.template.fieldName, operator: 'is', value1: templateId});
        Search.search();
        var results = Search.getResults();

        _.each(results, function(result){
            result.fieldid = result.magicfield || ((result.realfield || '').toLowerCase());
        });

        results = _.sortBy(results, 'order');

        return results;

    }


});