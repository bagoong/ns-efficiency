define('FeedPersistor',['JsonToCsv'], function(JsonToCsv){

    var THRESHOLD = 4.5 * 1024 * 1024;

    return function(pConfig){


        var config = pConfig;
        var startDate = Date.now();



        var save = function(data,i){
            var file,
                fileId;


            var fileName = pConfig.getFileName() + ':' + i + '-' + startDate;

            file = nlapiCreateFile(fileName, 'CSV',JsonToCsv(data));
            file.setFolder(config.getStorageFolder());
            file.setEncoding('UTF-8');

            fileId = nlapiSubmitFile(file);

            return fileId;
        };

        var auxSizeOf = function(obj){

            var mObj = obj && obj.length && obj[0];
            var len = obj.length;
            var keyLength = JSON.stringify(_.keys(mObj)).length * len * 2;

            return JSON.stringify(obj).length * 2 - keyLength;
        };

        return {
            items : [],
            currentSize : 0,
            filesSavedCount : 0,
            filesSavedIds : [],
            persist: function(pItems){

                var pItemSize = auxSizeOf(pItems);

                if(pItemSize > THRESHOLD)
                {
                    throw nlapiCreateError('ERR_SAVING','Lines are too big, try smaller pages');
                }

                if( ( this.currentSize + pItemSize > THRESHOLD ) )
                {
                    this.filesSavedCount ++;
                    this.filesSavedIds.push(save(this.items,this.filesSavedCount));
                    this.currentSize = 0;
                    this.items = [];
                }

                this.items = this.items.concat(pItems);
                this.currentSize = this.currentSize + pItemSize;

            },
            close: function(){
                this.filesSavedCount ++;
                this.filesSavedIds.push(save(this.items,this.filesSavedCount));
                this.currentSize = 0;
                this.items = [];
            },
            getFiles: function()
            {
                return this.filesSavedIds;
            }
        }
    };
});

