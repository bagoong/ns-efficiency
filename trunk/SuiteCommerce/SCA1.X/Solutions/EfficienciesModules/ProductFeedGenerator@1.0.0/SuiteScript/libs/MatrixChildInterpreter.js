define('MatrixChildInterpreter', [], function(){

    return function(pConfig){


        return {
            processItems: function(items){
                var processedItems = [];

                _.each(items, function(item){
                    if(item.matrixchilditems_detail && item.matrixchilditems_detail){

                        var itemWithoutChilds = _.omit(item, ['matrixchilditems_detail']);
                        var childFlags = {
                            child: true,
                            parent: itemWithoutChilds.internalid
                        };
                        _.each(item.matrixchilditems_detail, function(child){
                            processedItems.push(_.extend({},itemWithoutChilds,child,childFlags));
                        });

                    } else {
                        processedItems.push(item);
                    }
                });
                return processedItems;
            }
        }
    }
});