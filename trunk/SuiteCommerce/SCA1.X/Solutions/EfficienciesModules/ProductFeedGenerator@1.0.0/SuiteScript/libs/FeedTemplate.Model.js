Application.defineModel('FeedTemplate', {
    record: 'customrecord_ef_pfg_feed_template',
    filters: [
        {fieldName: 'isinactive', operator: 'is', value1: 'F'}
    ],
    columns: {
        internalid: {fieldName:'internalid'},
        name: {fieldName: 'name'}
    },


    get: function(id,options)
    {
        var Search = new SearchHelper(this.record, this.filters, this.columns),
            feedTemplate,
            options = options || {};

        Search.addFilter({fieldName: this.columns.internalid.fieldName, operator: 'is', value1: id});
        Search.search();
        feedTemplate = Search.getResult();

        if (!feedTemplate)
        {
            throw notFoundError;
        }
        if(options.withFields)
        {
            var FeedFieldModel = Application.getModel('FeedField');
            feedTemplate.fields = FeedFieldModel.getForTemplate(feedTemplate.internalid);
        }

        return feedTemplate;
    }
});

