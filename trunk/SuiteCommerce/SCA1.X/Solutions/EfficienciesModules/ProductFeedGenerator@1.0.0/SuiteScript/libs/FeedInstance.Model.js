Application.defineModel('FeedInstance', {
    record: 'customrecord_ef_pfg_feed_instance',
    filters: [
        {fieldName: 'isinactive', operator: 'is', value1: 'F'}
    ],
    columns: {
        internalid: {fieldName:'internalid'},
        created: {fieldName: 'created'},
        website: {fieldName: 'custrecord_ef_pfg_fi_website'},
        country: {fieldName: 'custrecord_ef_pfg_fi_country'},
        currency: {fieldName: 'custrecord_ef_pfg_fi_currency'},
        language: {fieldName: 'custrecord_ef_pfg_fi_language'},
        template_id: {fieldName: 'custrecord_ef_pfg_fi_template'},
        employee: {fieldName: 'custrecord_ef_pfg_fi_user_triggered'},
        copyTo: {fieldName: 'custrecord_ef_pfg_fi_notify', type: 'values'},
        extraFacetsRaw: {fieldName: 'custrecord_ef_pfg_fi_custom_json'},
        filterByFeed: {fieldName: 'custrecord_ef_pfg_fi_filter_feed'},
        molecule: {fieldName: 'custrecord_ef_pfg_fi_molecule'}
    },
    get: function(id)
    {
        var Search = new SearchHelper(this.record, this.filters, this.columns),
            feed,
            options = options || {};

        Search.addFilter({fieldName: this.columns.internalid.fieldName, operator: 'is', value1: id});
        Search.search();
        feed = Search.getResult();

        if (!feed)
        {
            throw notFoundError;
        }

        var FeedTemplateModel = Application.getModel('FeedTemplate');
        feed.template = FeedTemplateModel.get(feed.template_id, {withFields: true});


        return feed;
    },
    setInProgress: function(id)
    {
        var data = {
            'custrecord_ef_pfg_fi_status': '3'
        };

        nlapiSubmitField('customrecord_ef_pfg_feed_instance', id, _.keys(data), _.values(data));
    },
    finish: function(id,files)
    {
        var data = {
            'custrecord_ef_pfg_fi_status': '4',
            'custrecord_ef_pfg_fi_ss_instance': ''
        };

        nlapiSubmitField('customrecord_ef_pfg_feed_instance', id, _.keys(data), _.values(data));

        _.each(files, function(file){
            var record = nlapiCreateRecord('customrecord_ef_pfg_feed_instance_file');
            record.setFieldValue('custrecord_ef_pfg_fif_file', file);
            record.setFieldValue('custrecord_ef_pfg_fif_feed_instance', id);
            nlapiSubmitRecord(record);
        });

    }
});