/* global nlapiCreateSearch: false, nlapiDeleteRecord:false */
function scheduled(type) {
    var buffSearch = nlapiCreateSearch('customrecord_ef_sp_search_proxy', null, null);
    var results = buffSearch.runSearch();

    results.forEachResult(function(eachResult){
        nlapiDeleteRecord('customrecord_ef_sp_search_proxy', eachResult.getId());
        return true;
    });
}
