var unexpectedError = {
    status: 500,
    code:'ERR_UNEXPECTED',
    message: 'An error ocurred while performing your request.'
};

Application.searchProxy = function(record,filters,columns) {

    var result = null,
        buffer = nlapiCreateRecord('customrecord_ef_sp_search_proxy'),
        id,
        loadedBuffer,
        resultJSON,
        isSuccess;

    buffer.setFieldValue('custrecord_ef_sp_sp_record', record);
    buffer.setFieldValue('custrecord_ef_sp_sp_columns', JSON.stringify(columns));
    buffer.setFieldValue('custrecord_ef_sp_sp_filters', JSON.stringify(filters));

    id = nlapiSubmitRecord(buffer);

    //loadedBuffer = nlapiLoadRecord('customrecord_ef_sp_search_proxy', id);

    loadedBuffer = nlapiLookupField('customrecord_ef_sp_search_proxy',id,['custrecord_ef_sp_sp_values','custrecord_ef_sp_sp_issuccessful'])
    resultJSON = loadedBuffer.custrecord_ef_sp_sp_values;
    isSuccess = loadedBuffer.custrecord_ef_sp_sp_issuccessful;

    if (isSuccess !== 'T')
    {
        throw unexpectedError;
    }

    try
    {
        result = JSON.parse(resultJSON);
    } catch(e)
    {
        result = {};
    }

    return result;

};