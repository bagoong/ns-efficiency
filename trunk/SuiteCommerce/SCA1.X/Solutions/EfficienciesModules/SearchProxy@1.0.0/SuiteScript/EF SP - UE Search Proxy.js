/* global nlapiGetFieldValue:false,nlapiSetFieldValue:false, SearchHelper:false */

function searchProxyBeforeSubmit (type)
{
    var recordType = nlapiGetFieldValue('custrecord_ef_sp_sp_record'),
        columnsJSON = nlapiGetFieldValue('custrecord_ef_sp_sp_columns'),
        filtersJSON = nlapiGetFieldValue('custrecord_ef_sp_sp_filters'),
        filters = null,
        columns = null,
        validFlag = true,
        returnValue = null;

    // checks if the fields are filled up
    if (!recordType || recordType.length === 0)
    {
        validFlag = false;
    }

    try
    {
        filters = JSON.parse(filtersJSON);
        columns = JSON.parse(columnsJSON);

    } catch (e)
    {
        validFlag = false;
    }

    if (validFlag)
    {
        try
        {
            // set the custrecord_buff_values with the output
            returnValue = JSON.stringify(new SearchHelper(recordType, filters, columns).search().getResults());
            nlapiSetFieldValue('custrecord_ef_sp_sp_values', returnValue);
            nlapiSetFieldValue('custrecord_ef_sp_sp_issuccessful', 'T');
        } catch (e)
        {
            nlapiLogExecution('ERROR', 'Error on search', e);
        }
    }

}
