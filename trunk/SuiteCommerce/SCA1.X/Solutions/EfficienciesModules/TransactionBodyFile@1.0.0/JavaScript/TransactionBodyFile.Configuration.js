/**
 This file is provided just for reference.
 Add the content to your Configuration file.
 Refer to Store.js for configuration options
 */
(function (application) {

    'use strict';

    //Required to be added to the configuration file
    application.Configuration.modules.push('TransactionBodyFiles');

    //Example of what to put on checkout array
    application.Configuration.checkoutSteps[0].steps[0].modules.push(['OrderWizard.Module.TransactionBodyFile',{fieldid:'uploadfield'}]);
    application.Configuration.checkoutSteps[0].steps[1].modules.push(['OrderWizard.Module.TransactionBodyFile',{fieldid:'uploadfield'}]);



}(SC.Application('Checkout')));
