define('OrderWizard.Module.TransactionBodyFile', ['Wizard.Module'], function (WizardModule) {
    'use strict';

    return WizardModule.extend({
        template: 'order_wizard_transactionbodyfield',
        fileRequiredMessage: _.translate('File is mandatory'),
        events: {
            'submit [data-action="upload-form"]': 'uploadAction',
            'click [data-action="delete-file"]': 'deleteFile'
        },
        errors: [
            'ERR_FILE_ID_UNRECOGNIZED',
            'ERR_FILE_SIZE',
            'ERR_FILE_MISSING',
            'ERR_FILE_TYPE',
            'ERR_FILE_UPLOAD_PROCESS',
            'ERR_FILE_SEC'
        ],
        initialize: function (options)
        {
            this.wizard = options.wizard;
            this.step = options.step;
            this.model = options.wizard.model;
            this.options = _.extend({
                //show_checkbox: false
            }, this.options || {});

            this.options.field = _.findWhere(SC.ENVIRONMENT.Efficiencies.TransactionBodyFile.fields, {internalid: options.fieldid});
        },
        deleteFile: function (e)
        {
            var self = this,
                internalid = jQuery(e.target).data('internalid'),
                file = this.model.get('files').findWhere({internalid: internalid});

            this.model.deleteFile(file, {
                success: function () {
                    self.render()
                }
            });
        },
        getFile: function ()
        {
            return this.model.get('files').findWhere({internalid: this.options.field.internalid});
        },
        isValid: function ()
        {
            var promise = jQuery.Deferred(),
                fileModel = this.getFile();

            if (!!fileModel || !this.options.field.required) {
                promise.resolve();
            } else {
                promise.reject(this.options.field.requiredMessage);
            }

            return promise;
        },
        submit: function (e)
        {
            var self = this;
            if (this.getFile() || !this.$('#file').val()) {
                return this.isValid();
            } else {
                var prom = jQuery.Deferred();

                this.upload('[data-action="upload-form"]', {
                    success: function () {
                        self.render();
                        prom.resolve();
                    }
                });
                return prom;
            }

        },

        uploadAction: function (e)
        {
            var self = this;
            e.preventDefault();
            this.upload(e.target, {
                success: function () {
                    self.render();
                }
            });
        },

        upload: function (form, options)
        {

            var self = this;
            jQuery(form).ajaxSubmit({
                type: 'json',
                method: 'POST',
                beforeSend: function ()
                {
                    /* override reference implementation beforeSend, that doesn't work on this case */
                },
                url: _.resolveSuiteletURL
                (
                    SC.ENVIRONMENT.Efficiencies.TransactionBodyFile.suitelet.script,
                    SC.ENVIRONMENT.Efficiencies.TransactionBodyFile.suitelet.deploy
                ),
                success: function (data)
                {
                    self.model.addFile(data, options);
                },
                error: function (a) {
                    // enable navigation buttons
                    self.wizard.getCurrentStep().enableNavButtons();
                    // enable inputs and buttons
                    self.$('input, button').prop('disabled', false);
                }
            });

        }

    });

});