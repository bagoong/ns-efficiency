define('Order.Model.TransactionBodyFiles', ['Order.Model'],function(OrderModel){

    console.log('hom m?');

    var OrderFile = {
        Model: Backbone.Model.extend({}),
        Collection: Backbone.Collection.extend({})
    };

    OrderModel.prototype.filesCollection = OrderFile.Collection;


    _.extend(OrderModel.prototype, {
        filesModel: OrderFile.Model,
        filesCollection: OrderFile.Collection,
        initialize: _.wrap(OrderModel.prototype.initialize, function(fn,attributes){
            var oldReturn = fn.apply(this,_.toArray(arguments).slice(1));

            this.on('change:files', function (model, files)
            {
                model.set('files', new model.filesCollection(files), {silent: true});
            });
            this.trigger('change:files', this, attributes && attributes.files || []);

            return oldReturn;
        })
    });

    return OrderModel;

});