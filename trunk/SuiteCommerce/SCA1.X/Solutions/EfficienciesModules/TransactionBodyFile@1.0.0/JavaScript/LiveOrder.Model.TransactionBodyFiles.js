define('LiveOrder.Model.TransactionBodyFiles', ['LiveOrder.Model','Order.Model.TransactionBodyFiles'], function(LiveOrder,Order){

    var LiveOrderFile = {};


    LiveOrderFile.Model = Order.prototype.filesModel.extend({
        urlRoot: _.getAbsoluteUrl('services/live-order-file.ss')
    });
    LiveOrderFile.Collection = Order.prototype.filesCollection.extend({
        model: LiveOrderFile.Model,
        url: _.getAbsoluteUrl('services/live-order-file.ss')
    });

    LiveOrder.prototype.filesCollection = LiveOrderFile.Collection;

    _.extend(LiveOrder.prototype, {
        addFile: function(file, options){
            return this.addFiles([file],options);
        },
        addFiles: function (files, options)
        {
            // Obtains the Collection constructor
            var FilesCollection = this.filesCollection,
            // Creates the Colection
                files_collection = new FilesCollection(files);

            // Saves it
            return files_collection.sync('create', files_collection, this.wrapOptionsSuccess(options));
        },
        deleteFile: function(file,options){
            var promise = file.destroy(this.wrapOptionsSuccess(options));
            return file.promise;
        }
    });


});