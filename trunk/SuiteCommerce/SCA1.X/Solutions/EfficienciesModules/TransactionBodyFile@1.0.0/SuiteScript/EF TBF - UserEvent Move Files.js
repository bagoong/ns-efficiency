var Handler = (function() {


    var main = function(type)
    {

        var executionContext = nlapiGetContext().getExecutionContext();
        if( executionContext != 'webstore')
            return true;

        var record = nlapiGetNewRecord();

        _.each(SC.Configuration.Efficiencies.TransactionBodyFile.fields, function(field){
            var fileId = record.getFieldValue(field.bodyFields.file);
            if(fileId)
            {

                try {
                    var newFileName = 'SO ' + record.getId() + ' ' + field.name;

                    var file = nlapiLoadFile(fileId);
                    var oldName = file.getName();


                    var type = oldName.substring(oldName.lastIndexOf('.'), oldName.length);
                    file.setFolder(field.folderId);
                    file.setName(newFileName +  type);

                    nlapiSubmitFile(file);
                    nlapiAttachRecord("file", fileId, "salesorder", record.getId());

                } catch(e){
                    nlapiLogExecution('ERROR','Error Moving File', e);
                }
            }
        });

    };

    return {
        main: main
    };
})();
