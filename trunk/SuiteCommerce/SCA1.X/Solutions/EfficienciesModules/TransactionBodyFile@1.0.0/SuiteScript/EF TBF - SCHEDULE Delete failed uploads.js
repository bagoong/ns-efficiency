var Handler = (function() {


    var GOVERNANCE_THRESHOLD = 30;

    var main = function(request, response)
    {

        /*
            DELETE ALL FILES CREATED MORE THAN A DAY AGO THAT ARE ORPHAN
         */
        var filters = [
            {fieldName: 'created', operator: 'before', value1: 'previousoneday'},
            {fieldName: 'folder', operator: 'is', value1: SC.Configuration.Efficiencies.TransactionBodyFile.temporalUploadFolderId}
        ];
        var columns =
            {
                    internalid: {
                    fieldName: 'internalid'
                }
            };

        var Search = new SearchHelper(
            'file',
            filters,
            columns
        ).search();

        var results = Search.getResults();
        var i;
        var count = results.length;
        for(i=0; i<results.length; i++)
        {
            checkGovernance();
            nlapiDeleteFile(results[i].internalid);
        }



    };

    var checkGovernance = function()
    {
        var context = nlapiGetContext();
        if( context.getRemainingUsage() < GOVERNANCE_THRESHOLD )
        {
            console.log('YIELDING');
            var state = nlapiYieldScript();
            if( state.status == 'FAILURE' )
            {
                nlapiLogExecution("ERROR","Failed to yield script, exiting: Reason = "+state.reason + " / Size = "+ state.size);
                throw "Failed to yield script";
            }
            else if ( state.status == 'RESUME' )
            {
                nlapiLogExecution("AUDIT", "Resuming script because of " + state.reason+".  Size = "+ state.size);
            }
            else {
                nlapiLogExecution("AUDIT", "STATE" + JSON.stringify(state)+".  Size = "+ state.size);
            }
            // state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield
        }
    };

    return {
        main: main
    };
})();

