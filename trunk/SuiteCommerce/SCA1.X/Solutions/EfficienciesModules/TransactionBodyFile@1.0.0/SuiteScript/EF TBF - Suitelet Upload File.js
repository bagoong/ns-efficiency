var Handler = (function() {

    var getNewFileName = function(file){
        var name = file.getName();
        var type = name.substring(name.lastIndexOf('.'), name.length);
        return nlapiGetUser() + "_" + new Date().getMilliseconds().toString() + parseInt(Math.random() * 10000000, 10).toString() + type;

    };

    var main = function(request, response)
    {


        try {
            var context = nlapiGetContext(),
                data = {},
                fileObject = request.getFile('file'),
                internalid,
                fileId,
                fileUrl,
                fileName;

            if(!fileObject) throw nlapiCreateError('ERR_FILE_NOT_PRESENT', 'No file uploaded');
            try {
                data = request.getBody();
                internalid = request.getParameter('internalid');

            } catch (e) {
                console.error('Error parsing data');
            }


            //Only for logged in users
            if (!parseInt(nlapiGetUser(), 10) > 0) {
                throw unauthorizedError;
            }

            //no field definition
            var field = _.findWhere(SC.Configuration.Efficiencies.TransactionBodyFile.fields, {internalid: internalid});
            if (!field) {
                throw nlapiCreateError('ERR_FILE_ID_UNRECOGNIZED', 'Field uploaded is unrecognized');
            }

            if (field.sizeLimit) {
                if (fileObject.getSize() > field.sizeLimit) {
                    throw nlapiCreateError('ERR_FILE_SIZE', 'File size limit exceeded');
                }
            }


            var oldName = fileObject.getName();


            fileObject.setName(getNewFileName(fileObject));
            fileObject.setFolder(SC.Configuration.Efficiencies.TransactionBodyFile.temporalUploadFolderId);

            if (_.isArray(field.allowTypes) && field.allowTypes.length > 0) {
                if (!_.contains(field.allowTypes, fileObject.getType())) {
                    throw nlapiCreateError('ERR_FILE_TYPE', 'File type not allowed');
                }
            }


            try {
                fileId = nlapiSubmitFile(fileObject);
                if (fileId) {
                    fileUrl = nlapiLookupField('file', fileId.toString(), 'url');
                }

            } catch (e) {
                if (e instanceof nlobjError) {
                    console.error(e.getCode(), e.getDetails());
                }

                throw nlapiCreateError('ERR_FILE_UPLOAD_PROCESS', 'Error ocurred while uploading');
            }

            var returnData = {
                internalid: field.internalid,
                file: fileId.toString(),
                link: fileUrl,
                name: oldName
            };

            var actionToken = new ActionToken(_InternalTBFConfig,40);
            var token = actionToken.generate(returnData);

            Application.sendContent(_.extend(returnData,{t:token}));

        } catch(e){
            Application.sendError(e);
        }

    };

    return {
        main: main
    };
})();
