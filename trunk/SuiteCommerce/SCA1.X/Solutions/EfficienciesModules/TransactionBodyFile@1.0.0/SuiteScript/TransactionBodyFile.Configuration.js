//Internal variables for validation of comunication between suitelet and models.js
var _InternalTBFConfig = {
    AESKEY: '26EE1A19AAFE25F21541AE95FCBE7F9E',//AES KEY to comunicate with suitelet
    SHA1KEY : 'Hx[pQ+#)tes5:g{{resource}}V-?3+,,4g]({{user_id}}', //SHA1 KEY to comunicate with suitelet
    AESMESSAGE: 'void::{{timestamp}}::{{random}}::{{user_id}}::{{random2}}::{{resource}}::void'

};

_.extend(SC.Configuration.Efficiencies, {
    TransactionBodyFile: {
        suitelet:
            {
                script: 'customscript_ef_tbf_upload_file',
                deploy: 'customdeploy_ef_tbf_upload_file'
            }
        ,
        temporalUploadFolderId: '4388',
        fields: [
            /*{
                internalid: 'profilepic',
                folderId: '5266',
                name: 'Profile pic',
                required: false,

                bodyFields: {
                    file: 'custbody_ef_tbf_picture1',
                    filename: 'custbody_ef_tbf_picture1_name',
                    link: 'custbody_ef_tbf_picture1_link'
                },
                allowTypes: [
                    'BMPIMAGE',
                    'GIFIMAGE',
                    'JPGIMAGE',
                    'PJPGIMAGE',
                    'PNGIMAGE',
                    'TIFFIMAGE'
                ],
                sizeLimit: 50*1024 //size in bytes
            },*/
            {
                internalid: 'uploadfield',
                name: 'File',
                folderId: '5267',
                required: true,
                requiredMessage: 'File is mandatory',
                bodyFields: {
                    file: 'custbody_ef_tbf_field1',
                    filename: 'custbody_ef_tbf_field1_name',
                    link: 'custbody_ef_tbf_field1_link'
                },
                allowTypes: [ //https://system.netsuite.com/help/helpcenter/en_US/Output/Help/chapter_N3264137.html
                    'PLAINTEXT',
                    'RTF',
                    'WORD',
                    'XMLDOC',
                    'PDF',
                    'HTMLDOC',
                    'MESSAGERFC',
                    'EXCEL'
                ]
            }
        ]
    }
});

if(SC.Configuration.Efficiencies.SuiteletService) {
    SC.Configuration.Efficiencies.SuiteletService.services.push(SC.Configuration.Efficiencies.TransactionBodyFile.suitelet);
}



