define('Facets.Translator.Categories', ['Facets.Translator'], function(Translator) {

    _.extend(Translator.prototype, {

        getApiParams: function ()
        {
            var params = {};

            console.log(this.facets);
            _.each(this.facets, function (facet)
            {

                if (facet.id === 'category') {
                    value = 'Home/'+ facet.value;
                }

                switch (facet.config.behavior)
                {
                    case 'range':
                        var value = (typeof facet.value === 'object') ? facet.value : {from: 0, to: facet.value};
                        params[facet.id + '.from'] = value.from;
                        params[facet.id + '.to'] = value.to;
                        break;
                    case 'multi':
                        params[facet.id] = facet.value.sort().join(',') ; // this coma is part of the api call so it should not be removed
                        break;
                    default:
                        params[facet.id] =  facet.value ;
                }
            });

            params.sort = this.options.order;
            params.limit = this.options.show;
            params.offset = (this.options.show * this.options.page) - this.options.show;

            params.q = this.options.keywords;

            return params;
        }

    });

});
