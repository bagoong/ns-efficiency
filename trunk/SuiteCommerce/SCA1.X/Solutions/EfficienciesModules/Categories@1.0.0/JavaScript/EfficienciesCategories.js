define('EfficienciesCategories', ['Categories','Facets.Translator.Categories'],function(Categories,Translator){
   return {
       mountToApp: function(application, configuration){
           if (SC.ENVIRONMENT.CATEGORIES)
           {
               Categories.reset(SC.ENVIRONMENT.CATEGORIES);

           }

           Categories.mountToApp(application,configuration);

       }
   }
});