(function (SC) {

    'use strict';

    var keys = _.keys(SC._applications),
        application = SC._applications[keys[0]],
        configuration = application.Configuration;

    configuration.modules.splice(1,0,['EfficienciesCategories',{ addToNavigationTabs:true, navigationAddMethod: 'prepend' }]);

    configuration.facets.splice(0,0,
        {
            id: 'category',
            name: _('Category').translate(),
            priority: 0,
            uncollapsible: true,
            collapsed: false,
            titleSeparator: ', '
        }
    );


}(SC));