var BigCache = function(name, splitSize) {
    return {
        cache: nlapiGetCache(name),

        put: function(key, data, ttl) {
            var step = splitSize || 100000,
                actual = 0,
                totalLength = data.length;

            while (step * actual < totalLength) {
                this.cache.put(key + actual, data.substring(step * actual, Math.min(totalLength, step * (actual+1))), ttl);
                actual++;
            }
        },

        get: function(key) {
            var i = 0,
                data = '',
                d;

            while (d = this.cache.get(key + i++)) {
                data += d;
            }

            return data;
        },

        remove: function(key) {
            var i = 0;

            while (this.cache.get(key + i)) {
                this.cache.remove(key + i++);
            }
        }
    };
};
