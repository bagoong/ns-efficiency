Application.defineModel('Category', {
    fieldsets: {
        bootstrapping: ['internalid','itemid','urlcomponent'],
        list: ['internalid','itemid','urlcomponent','description2','storedisplaythumbnail','storedisplayimage'],
        details: ['internalid','itemid','urlcomponent','pagetitle','storedetaileddescription','metataghtml']
    },
    allFieldsWithMapping: {
        internalid: 'internalid',
        itemid: 'name',
        description2: 'briefDescription',
        storedetaileddescription: 'description',
        storedisplaythumbnail: 'thumbnail',
        storedisplayimage: 'image',
        pagetitle: 'pageTitle',
        metataghtml: 'metataghtml',
        urlcomponent: 'urlcomponent'
    },
    generateHash: function(category,hash)
    {
        var self = this;
        if(category.categories)
        {
            var categories_ids = [];
            _.each(category.categories, function(value, key){
                self.generateHash(value,hash);
                categories_ids.push(value.internalid);
            });
            category.categories = categories_ids;
        }
        hash[category.internalid] = category;
    },
    parseTree: function(category,url){

        var c = {},
            self = this;

        //convert names
        _.each(this.allFieldsWithMapping, function(value,key){
            c[value] = category[key];
        });

        //add missing urlcomponents to default
        c.urlcomponent = c.urlcomponent || c.itemid;

        //add tab value
        if(c.internalid > 0) {
            c.url = (url ? url + c.urlcomponent : c.urlcomponent) + '/';
        } else {
            c.url = '/';
        }

        //subcategories
        if(category.categories){
            c.categories = {};
            _.each(category.categories, function(value){
                var subTree = self.parseTree(value,c.url);
                c.categories[subTree.urlcomponent] = subTree;
            });
        }

        return c;

    },

    getHash: function()
    {
        var cache = new BigCache('PS-Categories', SC.Configuration.Efficiencies.Categories.chunkSize),
            is_secure = request.getURL().indexOf('https') === 0,
            config = SC.projectConfig.site.categories,
            cacheTtl = is_secure ? SC.Configuration.Efficiencies.Categories.cache.checkout : SC.Configuration.Efficiencies.Categories.cache.shopping,
            cacheKey = 'hash-',
            hash;

        var hashCache = cache.get(cacheKey);
        if (false)
        {
            try
            {
                hash = JSON.parse(hashCache);
            }
            catch(e)
            {
                hash = null;
                console.error('CACHE','Error retrieving categories from cache');
            }
        }

        if(!hash)
        {
            hash = {};
            this.generateHash(this.getFullTree(),hash);
            cache.put(cacheKey, JSON.stringify(hash), cacheTtl);
        }
        return hash;
    },

    getFullTree: function(){

        var cache = new BigCache('PS-Categories', SC.Configuration.Efficiencies.Categories.chunkSize),
            is_secure = request.getURL().indexOf('https') === 0,
            config = SC.projectConfig.site.categories,
            cacheTtl = is_secure ? SC.Configuration.Efficiencies.Categories.cache.checkout : SC.Configuration.Efficiencies.Categories.cache.shopping,
            cacheKey = 'categories-',
            categories,
            apiCategoriesResponse;

        var categoriesCache = cache.get(cacheKey);
        if (false)
        {
            try
            {
                categories = JSON.parse(categoriesCache);
            }
            catch(e)
            {
                categories = null;
                console.error('CACHE','Error retrieving categories from cache');
            }
        }

        if(!categories)
        {
            apiCategoriesResponse = session.getSiteCategoryContents(true);
            console.log(JSON.stringify(apiCategoriesResponse));
            categories = this.parseTree(apiCategoriesResponse[0]);
            cache.put(cacheKey, JSON.stringify(categories), cacheTtl);
        }



        return categories;
    }
});
