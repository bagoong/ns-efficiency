/* Configuration for Store Locator */
_.extend(SC.Configuration.Efficiencies, {
    Categories: {
        chunkSize: 70000, //Max number of suggestions passed to frontend
        cache: {
            shopping: 2 * 60 * 60,
            checkout: 2 * 60 * 60
        }
    }
});

