/**
 * Copyright NetSuite, Inc. 2015 All rights reserved. 
 * The following code is a demo prototype. Due to time constraints of a demo,
 * the code may contain bugs, may not accurately reflect user requirements 
 * and may not be the best approach. Actual implementation should not reuse 
 * this code without due verification.
 * 
 * (Module description here. Whole header length should not exceed 
 * 100 characters in width. Use another line if needed.)
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Feb 2015     rfalcunit
 * 
 */
/*
 * Global Variables
 */
{
    var REC_ITEM_IMAGE_SET_UP = 'customrecord_rf_item_images_setup';
    var FLD_IIS_WEBSITE = 'custrecord_rf_iis_website';
    var FLD_IIS_SITE_URL = 'custrecord_rf_iis_site_url';
    var FLD_IIS_DISPLAY_NAME = 'custrecord_rf_iis_display_name';
    var FLD_IIS_RESIZE_ID = 'custrecord_rf_iis_resize_id';
    var FLD_IIS_RESIZE_WIDTH = 'custrecord_rf_iis_resize_width';
    var FLD_IIS_RESIZE_HEIGHT = 'custrecord_rf_iis_resize_height';
    var FLD_IIS_FOLDER = 'custrecord_rf_iis_folder';
    var FLD_IIS_FOLDER_NAME = 'custrecord_rf_iis_folder_name';
    
    var HC_RESIZE_WIDTH = '225';
    var HC_RESIZE_HEIGHT = '225';        
}

    


/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @return {void} Any output is written via response object
 */
function generateIISSL(request, response) {
    var recIISWS = nlapiCreateRecord(REC_ITEM_IMAGE_SET_UP);
    var fldWebsite = recIISWS.getField(FLD_IIS_WEBSITE);
    var aWebsites = fldWebsite.getSelectOptions();    
    for(var i = 0; aWebsites && i < aWebsites.length; i++) {
        var idWebsite = aWebsites[i].getId();
        if(!isWithIISWebsite(idWebsite)) { // Checks if Item Image Setup already exists for the website
            var recWebsite = nlapiLoadRecord('website', idWebsite);
            var recIIS = nlapiCreateRecord(REC_ITEM_IMAGE_SET_UP);
            recIIS.setFieldValue(FLD_IIS_WEBSITE, idWebsite);
            recIIS.setFieldValue(FLD_IIS_DISPLAY_NAME, recWebsite.getFieldValue('displayname'));
            recIIS.setFieldValue(FLD_IIS_FOLDER, recWebsite.getFieldValue('imagefolder'));
            recIIS.setFieldValue(FLD_IIS_FOLDER_NAME, recWebsite.getFieldText('imagefolder'));
            //Get the Resize ID
            var nResizeCount = recWebsite.getLineItemCount('imagesize');
            for(var j = 1; j <= nResizeCount; j++) {
                var nResizeW = recWebsite.getLineItemValue('imagesize', 'imagemaxwidth', j);
                var nResizeH = recWebsite.getLineItemValue('imagesize', 'imagemaxheight', j);                        
                if ((nResizeW == HC_RESIZE_WIDTH) && (nResizeH == HC_RESIZE_HEIGHT)) {
                    var idResize = recWebsite.getLineItemValue('imagesize', 'imagescale', j);
                    recIIS.setFieldValue(FLD_IIS_RESIZE_ID, idResize);
                    recIIS.setFieldValue(FLD_IIS_RESIZE_WIDTH, HC_RESIZE_WIDTH);
                    recIIS.setFieldValue(FLD_IIS_RESIZE_HEIGHT, HC_RESIZE_HEIGHT);
                    break;
                }
            }
            //Get the Main Domain Name
            var nDomainCount = recWebsite.getLineItemCount('shoppingdomain');
            for(var j = 1; j <= nDomainCount; j++) {
                var bIsPrimary = recWebsite.getLineItemValue('shoppingdomain', 'isprimary', j);
                if(bIsPrimary == 'T') {
                    var sDomainName = recWebsite.getLineItemValue('shoppingdomain', 'domain', j);
                    recIIS.setFieldValue(FLD_IIS_SITE_URL, sDomainName);
                    break;                
                }                    
            }
            
            var idIIS = nlapiSubmitRecord(recIIS, true, true);
        }
        
    }
    
    //redirect to IIS Records
    gotoIISRecords();
    
    
}

function isWithIISWebsite(idWebsite) {
    var aFilters = [new nlobjSearchFilter(FLD_IIS_WEBSITE, null, 'anyof', idWebsite)];
    var aResults = nlapiSearchRecord(REC_ITEM_IMAGE_SET_UP, null, aFilters);
    
    return (aResults && aResults.length > 0);
}



function gotoIISRecords() {
    var aSearch = nlapiCreateSearch(REC_ITEM_IMAGE_SET_UP);
    var aCols = new Array();
    aCols.push(new nlobjSearchColumn(FLD_IIS_SITE_URL));
    aCols.push(new nlobjSearchColumn(FLD_IIS_DISPLAY_NAME));
    aSearch.setColumns(aCols);
    aSearch.setRedirectURLToSearchResults();
}


