/* PSG ECOMMERCE translate function (found in fronted code) in a single file to be used in non web contexts
Only difference is this one requires the dictionary. This can be changed using _.partial */

function translate (dictionary,text)
{
    if (!text)
    {
        return '';
    }

    text = text.toString();
    // Turns the arguments object into an array
    var args = Array.prototype.slice.call(arguments)

    // Checks the translation table
        ,	result = dictionary && dictionary[text] ? dictionary[text] : text;

    if (args.length && result)
    {
        // Mixes in inline variables
        result = result.format.apply(result, args.slice(2));
    }

    return result;
}

(function ()
{
    'use strict';

    String.prototype.format = function ()
    {
        var args = arguments;

        return this.replace(/\$\((\d+)\)/g, function (match, number)
        {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };

})();