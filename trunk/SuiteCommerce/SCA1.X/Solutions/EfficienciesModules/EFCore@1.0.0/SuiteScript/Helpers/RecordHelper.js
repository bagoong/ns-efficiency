/* Same concept as SearchHelper but for records that sadly have to be loaded whole to get correct info
* Try to avoid it's use
* */

function RecordHelper (record, fields, fieldset) {
    'use strict';
    this.setRecord(record);
    this.setFields(fields);
    this.setFieldset(fieldset);
}


RecordHelper.prototype.setFieldset = function (fieldset)
{
    'use strict';
    this._fieldset = _.clone(fieldset);
    return this;
};

RecordHelper.prototype.setFields = function (value)
{
    'use strict';
    this._fields = _.clone(value);
    return this;
};


RecordHelper.prototype.addField = function (value)
{
    'use strict';
    this._fields = this._fields || [];
    this._fields.push(value);
    return this;
};


RecordHelper.prototype.setRecord = function (value)
{
    'use strict';
    this._record = value;
    return this;
};

RecordHelper.prototype.getResult = function ()
{
    'use strict';
    return this._lastResult && this._lastResult.length === 1 && this._lastResult[0];
};

RecordHelper.prototype.getResults = function ()
{
    'use strict';
    return this._lastResult;
};

RecordHelper.prototype._mapResult = function (list)
{
    'use strict';

    var self = this,
        props = _.clone(this._fields);

    return (list && list.length && _.map(list, function (record)
        {

            var ret = _.reduce(props, function (o, v, k)
            {

                //Not in fieldset, move along
                if(self._fieldset && !_.contains(self._fieldset,k)){
                    return o;
                }

                switch(v.type)
                {
                    case 'listrecordToObject':
                    case 'file':
                    case 'object':
                        o[k] = {
                            internalid: record.getFieldValue(v.fieldName),
                            name: record.getFieldText(v.fieldName)
                        };

                        break;

                    case 'objects':
                        var values = [];
                        var ids = record.getFieldValues(v.fieldName);
                        var names = record.getFieldTexts(v.fieldName);

                        _.each(ids, function(anId,index){
                            values.push({
                                internalid: ids[index],
                                name: names[index]
                            })
                        });
                        o[k] = values;

                        break;

                    case 'getText':
                    case 'text':
                        o[k] = record.getFieldText(v.fieldName);
                        break;

                    case 'getTexts':
                    case 'texts':
                        o[k] = record.getFieldTexts(v.fieldName);
                        break;

                    case 'getValues':
                    case 'values':
                        o[k] = record.getFieldValues(v.fieldName);
                        break;

                    //case 'getValue':
                    default:
                        o[k] = record.getFieldValue(v.fieldName);
                        break;
                }

                if (v.applyFunction)
                {
                    o[k] = v.applyFunction(record, v, k);
                }

                return o;
            }, {});
            ret.internalid = record.getId() + '';
            return ret;
        })) || [];
};


RecordHelper.prototype.get = function(id){
    this.search([id]);
};

RecordHelper.prototype.getRecord = function(id){
    return this._lastResultRecords[id];
}

RecordHelper.prototype.search = function (ids)
{
    'use strict';

    var self = this;
    var results = [];
    this._lastResultRecords = [];


    _.each(ids, function(id){
        if(_.isObject(id)){

            var temp = nlapiLoadRecord(id.type, id.id);
        } else {
            var temp = nlapiLoadRecord(self._record, id); //Cheating for "subclasses" like items heh.
        }
        results.push(temp);
        self._lastResultRecords[temp.getId()] = temp;

    });

    this._lastResult = this._mapResult(results);


    return this;
};