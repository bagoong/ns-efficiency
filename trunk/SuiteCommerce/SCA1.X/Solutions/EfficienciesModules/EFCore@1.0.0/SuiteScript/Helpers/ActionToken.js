var ActionToken = function (keys,timeInMinutes){
    this.keys = _.clone(keys);
    this.time = timeInMinutes;
};

ActionToken.prototype.generate =  function(data){

    var encrypted_data = nlapiEncrypt(
        this.keys.AESMESSAGE.replace('{{timestamp}}',new Date().getTime())
            .replace('{{random}}',Math.random())
            .replace('{{user_id}}',nlapiGetUser())
            .replace('{{random2}}',Math.random())
            .replace('{{resource}}',JSON.stringify(data)),
        'aes', this.keys.AESKEY);

    var resourceHashed = nlapiEncrypt(this.keys.SHA1KEY.replace('{{resource}}',JSON.stringify(data)).replace('{{user_id}}',nlapiGetUser()),'sha1');

    return {
        signature: resourceHashed,
        data: encrypted_data
    };

    return url;
};

ActionToken.prototype.read = function(data){
    var ts = data.data;
    var r = data.signature;
    var now = new Date();


    if(!ts || !r){
        throw nlapiCreateError('ERR_SL_COM', 'Need params',false);
    }

    var tsArray = nlapiDecrypt(ts, 'aes', this.keys.AESKEY).split('::'),
        timestamp = tsArray[1],
        user_id = tsArray[3],
        data = tsArray[5],
        ssp_request_date = new Date(parseInt(timestamp,10)),
        diffInMinutes = (now.valueOf() - ssp_request_date.valueOf())/1000/60,
        key = this.keys.SHA1KEY.replace('{{resource}}',data).replace('{{user_id}}',user_id),
        hash = nlapiEncrypt(key,'sha1');

    // SECURITY VALIDATIONS
    if( hash !== r){
        throw nlapiCreateError('ERR_SL_COM', 'Invalid call');
    }

    // TIME VALIDATIONS
    if(isNaN(diffInMinutes) ||  diffInMinutes > this.time){
        throw nlapiCreateError('ERR_SL_COM', 'Action expired',false);
    }

    if(!user_id || !data){
        throw nlapiCreateError('ERR_SL_COM', 'Invalid call',false);
    }
    try {
        nlapiLogExecution('ERROR', 'data', data);
        var dataJSON = JSON.parse(data)
    } catch(e){
        throw nlapiCreateError('ERR_SL_COM', 'Invalid data',false);
    }

    return {
        userId: user_id,
        data: dataJSON
    };
}

