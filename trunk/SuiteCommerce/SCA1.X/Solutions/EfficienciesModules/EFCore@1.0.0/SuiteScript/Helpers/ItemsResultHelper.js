/**
 * Created by pzignani on 21/10/2014.
 * Given a search result from SearchHelper, with an item property, an item column, an item property, and the item type property,
 * grabs the results and appends ITEMS (order-fieldset complete items, in frontend expected format)
 * Record: is the record from where the search comes from
 * RecordItemColumn: the column name where the item is linked
 * ResultItemProperty: on the results, the property id where the itemid resides
 * ResultItemTypeProperty: on the results, the property id where the item type resides
 */
function ItemsResultHelper (record, recordItemColumn,resultItemProperty,resultItemTypeProperty)
{
    'use strict';
    this.record = record;
    this.recordItemColumn = recordItemColumn;
    this.resultItemProperty = resultItemProperty;
    this.resultItemTypeProperty = resultItemTypeProperty;
}

ItemsResultHelper.prototype.processResults = function(results){

    var self = this,
        storeItemModel = Application.getModel('StoreItem'),
        items_to_preload = [];

    _.each(results, function(result){
        items_to_preload.push({
            id: result[self.resultItemProperty],
            type: result[self.resultItemTypeProperty]
        });
    });

    storeItemModel.preloadItems(items_to_preload);

    _.each(results, function(result)
    {
        var itemStored = storeItemModel.get(result[self.resultItemProperty], result[self.resultItemTypeProperty]);
        var itemsToQuery = [];


        if (!itemStored || typeof itemStored.itemid === 'undefined')
        {
            itemsToQuery.push({
                id: result[self.resultItemProperty],
                type: result[self.resultItemTypeProperty]
            });
        }
        else
        {
            _.extend(result, {item: itemStored});
            delete result.itemType;
        }
    });



};