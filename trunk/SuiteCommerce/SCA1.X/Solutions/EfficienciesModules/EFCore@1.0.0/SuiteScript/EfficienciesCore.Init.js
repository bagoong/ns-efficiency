/* global Application:true */

/*
 INIT FILE for PS Efficiencies modules. To extend backend configs and pass them to frontend.
 */

//Add ONCE listener to events on models
_.extend(Application,{
    once: function(name, callback, context) {
        //if (!eventsApi(this, 'once', name, [callback, context]) || !callback) return this;
        var self = this;
        var once = _.once(function() {
            self.off(name, once);
            callback.apply(this, arguments);
        });
        once._callback = callback;
        return this.on(name, once, context);
    }
});

Application.getEnvironment = Application.wrapFunctionWithEvents('Application.getEnvironment', Application, Application.getEnvironment);
_.extend(SC.Configuration, {
    Efficiencies: SC.Configuration.Efficiencies || {}
});

//Mount efficiencies config
Application.on('after:Application.getEnvironment', function(obj,result){
    'use strict';
    _.extend(result,{Efficiencies: SC.Configuration.Efficiencies});
});

