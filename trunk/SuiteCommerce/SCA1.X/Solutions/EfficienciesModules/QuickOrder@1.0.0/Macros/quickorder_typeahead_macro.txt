<% registerMacro('quickOrderTypeAhead', function (item, query, application) { %>
<div class="typeahead-item clearfix">
	<div class="typeahead-item-head">
		<% var thumbnail = item.get('_thumbnail'); %>
		<!--<img class="typeahead-image" src="<%- application.resizeImage(thumbnail.url, 'thumbnail') %>" alt="<%- thumbnail.altimagetext %>">-->
		<div class="typeahead-title"><%= _.highlightKeyword(item.get('_name'), query) %></div>
		<%= _('SKU: #$(0)').translate(
		'<span itemprop="sku">' + item.get('_sku', true) + '</span>'
		) %>
	</div>
</div>
<%})%> 