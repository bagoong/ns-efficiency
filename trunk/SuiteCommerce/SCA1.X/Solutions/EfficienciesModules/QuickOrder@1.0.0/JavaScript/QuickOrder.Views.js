// QuickOrder.View.js
// -----------------------
// View for order's details
define('QuickOrder.View', [], function ( )
{
	'use strict';

	/* Bootstrap typeahead configuration. Pretty similar to SiteSearch module one */
	var typeaheadConfig = {
		source: function (query, process)
		{
			var self = this;
			self.ajaxDone = false;

			this.model = this.model || this.options.model;
			this.labels = this.labels || this.options.labels;
			this.results = this.results || this.options.results;
			this.application = this.application || this.options.application;

			query = jQuery.trim(query);

			this.model.fetch(
				{
					data: {
						keyword: query
					}
					,	killerId: _.uniqueId('ajax_killer_')
				}
				,	{
					silent: true
				}
			).done(function ()
				{
					self.ajaxDone = true;
					self.results = {};
					self.labels = [];


					self.model.get('items').each(function (item)
					{
						// In some occasions the search term meay not be in the itemid
						self.results[item.get('_id') + query] = item;
						self.labels.push(item.get('_id') + query);
					});

					if(self.model.get('items').length===0)
					{
						self.labels.push('placeholder-' + query)
					}


					process(self.labels);
					self.$element.trigger('processed', self);
				});
		}

		// matcher:
		// Method used to match the query within a text
		// we lowercase and trim to be safe
		// returns 0 only if the text doesn't contains the query
		,	matcher: function (text)
		{
			return true;
		}

		// highlighter:
		// method to generate the html used in the dropdown box bellow the search input
		,	highlighter: function (itemid)
		{

			var template = ''
				,	macro = this.options.macro
				,	item = this.results[itemid];

			if (item)
			{
				// if we have macro, and the macro exists, we use that for the html
				// otherwise we just highlith the keyword in the item id
				// _.highlightKeyword is in file Utils.js
				template = macro && SC.macros[macro] ? SC.macros[macro](item, this.query, this.application) : _.highlightKeyword(itemid, this.query);
			}
			else
			{
				if (_.size(this.results))
				{
					// 'See All Results' label
					template = '';
				}
				else if (this.ajaxDone)
				{
					template = '<strong>' + this.options.noResultsLabel + '<span class="hide">' + this.query + '</span></strong>';
				}
				else
				{
					template = '<strong>' + this.options.searchingLabel + '<span class="hide">' + this.query + '</span></strong>';
				}
			}

			return template;
		}

		// its supposed to return the selected item
		,	updater: function (itemid)
		{
			this.$element.trigger('selected',[this.$element, this.results[itemid]]);
		}

		,	labels: []
		,	results: {}
		,	seeAllLabel: _('See all results').translate()
		,	noResultsLabel: _('No results').translate()
		,	searchingLabel: _('Searching...').translate()
	};

	return Backbone.View.extend({
		typeaheadConfig: typeaheadConfig,
		template: 'quickorder',
		events : {
			'selected input': 'selected',
			'click [data-action="remove"]': 'removeLine',
			'focus [data-type="itemid"]': 'focusEventHandler',
			'click button[data-action="addEmptyLine"]' : 'addLine',
			'change input[data-field="qty"]' : 'updateQuantity',
			'keyup [data-field="qty"]': 'updateQuantity',
			'click button[data-action="multiadd"]' : 'addToCart',
			'click button[data-action="reset"]' : 'removeAll'
		},
		errors : {
			'ERR_QUICKORDER_INVALID_ITEM' : 'Sorry, the item $(0) is invalid.',
			'ERR_QUICKORDER_ADDED' : 'Something went wrong when we were trying to add your items to the cart, please confirm and try again.',
			'ERR_QUICKORDER_NOITEMS' : 'You don\'t have any items in the list.'
		},
		messages : {
			'MSG_QUICKORDER_CLEAR' : 'All your items were removed from the list.',
			'MSG_QUICKORDER_ITEM_REMOVED' : 'The item $(0) was removed from the list.',
			'MSG_QUICKORDER_ITEMS_ADDED' : 'Your list was added to the cart.'
		},
		title: _('Quick Order Entry').translate(),
		page_header: _('Quick Order Entry').translate(),

		initialize: function(options)
		{
			var self = this;
			this.application = options.application;
			this.model = options.model;
			this.configuration = this.application.Configuration.QuickOrder;
			this.dataFields = {};
			this.positions = this.configuration.defaultFormLines;

		},

		focusEventHandler: function (e)
		{
			jQuery(e.target).typeahead('lookup');
		},

		addLine: function(e)
		{
			this.positions++;
			this.showContent({dontScroll: true});
		},

		removeLine: function(e)
		{
			var $element = jQuery(e.target);
			this.removeItem($element.data('position'));
		},

		selected: function(e,element,item)
		{
			var self = this
				,	elementParent = element.parents(".control-group")
				,	elementIndex = elementParent.data("line")
				,	quantity = elementParent.find("[data-field='qty']")
				,	quantity_value = quantity.val();

			if ( quantity_value == "" ) {
				quantity_value = 1;
			}

			if ( !item ) {
				return self.removeItem(elementIndex);
			}

			item.set('index', elementIndex)
				.set('quantity', parseInt(quantity_value));

			self.dataFields[elementIndex] = item;

			self.toggleQty(elementParent, false);

			if( elementIndex + 1 === this.positions )
			{
				this.positions++;
			}


			self.showContent({dontScroll: true}).done(function(){
				self.$('[name="' + elementIndex + '-qty"]').select();
			});
		},

		render : function()
		{

			_.extend(this.typeaheadConfig, {
					application: this.application,
					model: this.model
			},this.configuration.typeahead);

			this._render();
			this.$('[data-type="itemid"]').typeahead(this.typeaheadConfig);
		},

		addToCart : function ()
		{

			var self = this;

			self.$('[data-type="alert-placeholder"]').empty();

			if ( _.isEmpty(this.dataFields) ) {
				return self.$('[data-type="alert-placeholder"]').append(SC.macros.message( _(self.errors['ERR_QUICKORDER_NOITEMS']).translate() , 'error', true));
			}

			this.application.getCart().addItems(_.values(this.dataFields)).success(function(){

				self.removeAll();
				self.showContent();
				self.$('[data-type="alert-placeholder"]').append(SC.macros.message( _(self.messages['MSG_QUICKORDER_ITEMS_ADDED']).translate() , 'success', true));

			}).fail(function(){
				self.$('[data-type="alert-placeholder"]').append(SC.macros.message( _(self.errors['ERR_QUICKORDER_ADDED']).translate() , 'error', true));
			});

		},

		// if we are getting the item from the backend we will disable all the qty fields to avoid errors
		toggleQty : function (elementParent, disable)
		{

			this.toggleAddToCart(disable);

			if ( disable ){
				elementParent.find("[data-field='qty']").attr("disabled", "disabled");
			}
			else {
				elementParent.find("[data-field='qty']").removeAttr("disabled");
			}

		},

		toggleAddToCart : function (disable)
		{
			if ( disable ){
				this.$(".btn-addtocart").attr("disabled", "disabled");
			}
			else {
				this.$(".btn-addtocart").removeAttr("disabled");
			}
		},

		clearError : function ()
		{
			this.$('[data-type="alert-placeholder"]').empty();
		},

		removeItem : function (elementIndex) {
			var self = this
			,	itemName = this.dataFields[elementIndex].get("_name") + ' (' + this.dataFields[elementIndex].get("_sku") + ')';

			this.dataFields[elementIndex] = null;
			this.showContent({dontScroll: true});

			return self.$('[data-type="alert-placeholder"]').append(
				SC.macros.message( _(self.messages['MSG_QUICKORDER_ITEM_REMOVED']).translate(itemName) , 'success', true)
			);
		},

		removeAll : function (noMessage)
		{
			delete this.dataFields;
			this.dataFields = {};
			this.positions = 1;

			this.showContent({dontScroll: true});
			this.$('[data-type="alert-placeholder"]').append(SC.macros.message( _(this.messages['MSG_QUICKORDER_CLEAR']) , 'success', true));
		},

		updateQuantity : _.debounce(function (event)
		{


			this.clearError();

			var self = this
			,	quantity = jQuery(event.target)
			,	elementParent = quantity.parents(".control-group")
			,	elementIndex = elementParent.data("line")
			,	item = self.dataFields[elementIndex]
			,	newQty = parseInt(quantity.val())
			,	oldQty = item && item.get('quantity');

			if(item) {
				if(newQty != oldQty)
				{
					item.set('quantity', newQty);
					self.showContent({dontScroll: true}).done(function () {
						var $elem = self.$('[name="' + elementIndex + '-qty"]')
						$elem.focus().val($elem.val());
					});
				}
			}
		}, 100)
	});

});
