define('QuickOrder', ['QuickOrder.Router', 'QuickOrder.Model', 'QuickOrder.View'], function (Router, Model, View)
{
	'use strict';


	return	{

		model : Model,
		view : View,
		router : Router,

        mountToApp: function (application)
		{
			//Prioritize frontend config, then backend config (comes from ssp_libraries)
			application.Configuration.QuickOrder = _.defaults(application.Configuration.QuickOrder, SC.ENVIRONMENT.Efficiencies.QuickOrder);
			return new Router({
                application : application
            });
		}
	};
	
});
