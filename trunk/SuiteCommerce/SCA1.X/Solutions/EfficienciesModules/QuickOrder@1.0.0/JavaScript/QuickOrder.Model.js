// QuickOrder.Model.js
// -----------------------
// Model for handling QuickOrder
define('QuickOrder.Model', ['Facets.Model','ItemDetails.Collection','ItemDetails.Model'], function (FacetModel,ItemCollection,ItemModel)
{

	'use strict';

	var originalFetch = FacetModel.prototype.fetch;


	/* Item model with an initializer for MatrixChild work */
	var QuickOrderItemModel = ItemModel.extend({

		initialize: function ()
		{
			this.itemOptions = {};
			var self = this;
			/*
			Code to generate the correct "Parent, with X and Y item option selected
			instead of getting the direct child without knowing which options are set
			This workaround had to be done because of how itemoptions come in responses from cart lines when shown
			(line.options) instead of on the item, and this is not a line but just an item.
			 */
			if(this.get('matrix_parent') && this.get('matrix_parent').matrixchilditems_detail)
			{
				//HACK to make the ItemOptionsHelper recognize parent options
				this.set('matrixchilditems_detail',this.get('matrix_parent').matrixchilditems_detail);

				var internalid = this.get('internalid'),
						itemInChilds = _.findWhere(this.get('matrix_parent').matrixchilditems_detail, {internalid:internalid}),
						options = _.where(this.get('matrix_parent').itemoptions_detail.fields,{ismatrixdimension:true}),
						optionValues = {};

					_.each(options, function(option,index){
						optionValues[option.internalid] = itemInChilds[option.sourcefrom];
					});
					_.each(optionValues, function(value,name){
						var option = self.getPosibleOptionByUrl(name);
						if (option)
						{
							if (option.values)
							{
								// We check for both Label and internal id because we detected that sometimes they return one or the other
								value = _.where(option.values, {label: value})[0] || _.where(option.values, {internalid: value})[0];
								self.setOption(option.cartOptionId, value);
							}
							else
							{
								self.setOption(option.cartOptionId, value);
							}
						}
				});
			}
		}
	});
	var QuickOrderItemCollection = ItemCollection.extend({
		model: QuickOrderItemModel
	});

	/* 'Facets' Model extension,as it is a model with an "items" response attribute with a collection of items */
	return FacetModel.extend({
		url: _.getAbsoluteUrl('services/quickorder.ss'),
		initialize: function ()
		{
			// Listen to the change event of the items and converts it to an ItemDetailsCollection
			this.on('change:items', function (model, items)
			{
				if (!(items instanceof QuickOrderItemCollection))
				{
					// NOTE: Compact is used to filter null values from response
					model.set('items', new QuickOrderItemCollection(_.compact(items)));
				}
			});
		}
	});
});