/**
 This file is provided just for reference.
 Add the content to your Configuration file.
 */
(function (SC) {

    'use strict';

    var keys = _.keys(SC._applications),
        application= SC._applications[keys[0]];

    application.Configuration.modules.push('QuickOrder');

    application.Configuration.QuickOrder = {
        typeahead:{
            minLength: 3, //Minimum length of the search query before doing an ajax call to suggest results
            macro: 'quickOrderTypeAhead', //Macro of the displayed suggestions
            items: 10 //Max quantity of suggested results
        },
        defaultFormLines: 1 //Lines that come automatically drawn in the template

    };


}(SC));