// QuickOrder.Router.js
// -----------------------
// Router for handling QuickOrder
define('QuickOrder.Router', ['QuickOrder.View', 'QuickOrder.Model'], function (View, Model)
{
	return Backbone.Router.extend({

		routes: {
			'quickorder': 'quickorder'
		}

	,	initialize : function (options) 
		{
			this.application = options.application;
		}

	,	quickorder: function ()
		{

			var view = new View({
				model : new Model()
			,	application: this.application
			});

			view.showContent();

		}

	});

});