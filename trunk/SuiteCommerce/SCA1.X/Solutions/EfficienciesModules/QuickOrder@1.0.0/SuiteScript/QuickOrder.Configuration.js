/* Configuration for Store Locator */
_.extend(SC.Configuration.Efficiencies, {
    QuickOrder: {
        maxSuggestions: 10, //Max number of suggestions passed to frontend
        matchField: 'itemid' //Field to match on the SearchProxy call
    }
});



