/*

 */
Application.defineModel('QuickOrder',{
    filters: [
        {fieldName: 'isonline', operator: 'is', value1:'T'},
        {fieldName: 'isinactive', operator: 'is', value1:'F'},
        {fieldName: 'matrix', operator: 'is', value1:'F'} //This avoids matrix PARENTS and looks just for childs or orphans
    ],
    columns : {
        id: {fieldName: 'internalid'},
        parent: {fieldName: 'parent'},
        type: {fieldName: 'type'},
        subtype: {fieldName: 'subtype'}
    },
    get: function(keyword){
        var filters = _.clone(this.filters),
            columns = _.clone(this.columns),
            ST = Application.getModel('StoreItem'),
            results,
            results_filtered = [],
            firstXSorted = [];


        //Append to the default columns, the column specified in the configuration (itemid by default)
        if(!columns[SC.Configuration.Efficiencies.QuickOrder.matchField]){
            columns[SC.Configuration.Efficiencies.QuickOrder.matchField] = {fieldName:SC.Configuration.Efficiencies.QuickOrder.matchField}
        }

        //Filter by website
        filters.push({fieldName:'website',operator:'anyof',value1:session.getSiteSettings(['siteid']).siteid});

        //Append to the default filters, the filters specified in the configuration (itemid by default)
        filters.push({fieldName: SC.Configuration.Efficiencies.QuickOrder.matchField ,operator:'contains',value1:keyword});


        //Dependency on SearchProxy function
        results = Application.searchProxy('item',filters,columns);

        //This filters results that are because of matrix naming conventions.
        //Looks like parent itemid is propagated ON FILTERS to the child itemid. Not on columns, so we filter it in frontend.
        results_filtered = [];
        _.each(results, function(result){
            if(result && result[SC.Configuration.Efficiencies.QuickOrder.matchField] && result[SC.Configuration.Efficiencies.QuickOrder.matchField].toLowerCase().indexOf(keyword.toLowerCase()) !== -1){
                results_filtered.push(result);
            }
        });

        //Grab the best 10, and sort them by length;
        firstXSorted =  _.first(_.sortBy(results_filtered, function(result){ return result[SC.Configuration.Efficiencies.QuickOrder.matchField].length; }),SC.Configuration.Efficiencies.QuickOrder.maxSuggestions);

        ST = Application.getModel('StoreItem');
        ST.preloadItems(firstXSorted);

        //Respond with "ItemAPI" formatted items
        return {
            items: _.map(firstXSorted,function(result){
                return ST.get(result.id,result.type);
            })
        };

        //TODO: Possible point of failure: too many items in searchproxy response (too big, fails)
        //TODO: Use nlapicache for responses
    }
});