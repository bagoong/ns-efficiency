Application.defineModel('BackInStockNotification.Location', {
    record: 'location',
    fieldsets: {
        basic: [
            'internalid',
            'name',
            'makeinventoryavailablestore'
        ]
    },
    filters: [
        {fieldName: 'isinactive', operator: 'is', value1: 'F'},
    ],
    columns: {
        internalid: {fieldName: 'internalid'},
        makeinventoryavailablestore: {fieldName: 'makeinventoryavailablestore'},
        name: {fieldName: 'name'}
    },
    getWebstoreAvailableLocations: function()
    {
        //get all the locations that expose stock to the webstore
        var filters = _.clone(this.filters);
        filters.push({fieldName: 'makeinventoryavailablestore', operator: 'is', value1: 'T'});

        var Search = new SearchHelper(
            this.record,
            filters,
            this.columns
        ).search();

        return Search.getResults();
    }
});