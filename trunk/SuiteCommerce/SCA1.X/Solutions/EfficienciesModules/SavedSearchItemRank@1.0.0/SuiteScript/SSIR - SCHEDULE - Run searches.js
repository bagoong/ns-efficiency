/**
 * Copyright NetSuite, Inc. 2014 All rights reserved.
 * The following code is a demo prototype. Due to time constraints of a demo,
 * the code may contain bugs, may not accurately reflect user requirements
 * and may not be the best approach. Actual implementation should not reuse
 * this code without due verification.
 *
 * (Module description here. Whole header length should not exceed
 * 100 characters in width. Use another line if needed.)
 *
 * Version    Date            Author           Remarks
 * 1.00       18 Mar 2014     rfalcunit
 * 2.00       19 Sep 2014     hdaguerre			Added to support for Matrix parent items,
 * 												so that it cascades the ranking value to its child items
 *
 * 3.00       Dec 2014        psefficiencies    code re-write with nlapiYieldScript and "the efficiencies way"
 */

var Handler = (function(){

    var GOVERNANCE_THRESHOLD = 250;
    var FLD_ITEM_TYPE = 'type';
    var itemRecTypeMapping =
    {
        'InvtPart' : 'inventoryitem',
        'NonInvtPart' : 'noninventoryitem',
        'Service' : 'serviceitem',
        'Assembly' : 'assemblyitem',
        'GiftCert' : 'giftcertificateitem',
        'Kit' : 'kititem'
    };


    var  getChildItems = function(sItemTypeID, idItem){
        return nlapiSearchRecord(
            sItemTypeID,
            null,
            [new nlobjSearchFilter('parent', null, 'is', idItem)]
        );
    };



    var fnIsEmpty = function(val) {
        return ((val == null) || (val == '') || (!val));
    };

    var main = function()
    {
        var SavedSearchItemRankConfigurationModel = Application.getModel('SavedSearchItemRankConfiguration');
        var list = SavedSearchItemRankConfigurationModel.getActive();
        var i;

        for(i=0; i<list.length; i++){
            processRanking(list[i]);
            checkGovernance();
        }
    };


    function isMatrixItem(itemType, itemId){
        return !fnIsEmpty(nlapiSearchRecord(itemType,
            null,
            [
                new nlobjSearchFilter('internalid', null, 'is', itemId),
                new nlobjSearchFilter('matrix', null, 'is', 'T')
            ],
            null
        ));


    }


    var processRanking = function(config)
    {
        var savedSearchResults = nlapiSearchRecord(null,config.savedsearch);
        var j;

        var columns = savedSearchResults && savedSearchResults[0] && savedSearchResults[0].getAllColumns();
        for(j=0; savedSearchResults && j<savedSearchResults.length; j++)
        {

            processItem(config,savedSearchResults[j],j,columns);
            checkGovernance();
        }
    };

    var processItem = function(ssConfig, itemLine, rank,allColumns)
    {

        var itemId = itemLine.getId();
        if (fnIsEmpty(itemId)) {
            itemId = itemLine.getValue(allColumns[0]); //Make sure that the first column of the results is the Internal ID if the columns are grouped
        }

        var itemFields = nlapiLookupField('item', itemId, [FLD_ITEM_TYPE,'isserialitem', ssConfig.storagefieldid]);
        var oldRanking = itemFields[ssConfig.storagefieldid];
        if(itemFields[oldRanking] != rank) //Not the same value
        {
            console.log('Ranking', 'Old Value: '+ oldRanking +' => New Value: '+ rank);

            var sItemTypeID = itemRecTypeMapping[itemFields[FLD_ITEM_TYPE]];
            var sItemTypeIDForSearch = sItemTypeID;
            if(sItemTypeID === 'inventoryitem' && itemFields.isserialitem == 'T'){
                sItemTypeID = 'serializedinventoryitem';
            }

            nlapiSubmitField(sItemTypeID, itemId, ssConfig.storagefieldid, rank);

            if (isMatrixItem(sItemTypeID,itemId) == 'T')
            {
                //Fetches all of its child items
                var childItems = getChildItems(sItemTypeID, itemId);
                if (childItems && childItems.length){

                    for (var k = 0; k< childItems.length; k++) {
                        nlapiSubmitField(sItemTypeID, childItems[k].getId(), ssConfig.storagefieldid, rank);
                    }
                }
            }
        }


    };

    var checkGovernance = function()
    {
        var context = nlapiGetContext();
        if( context.getRemainingUsage() < GOVERNANCE_THRESHOLD )
        {
            var state = nlapiYieldScript();
            if( state.status == 'FAILURE' )
            {
                nlapiLogExecution("ERROR","Failed to yield script, exiting: Reason = "+state.reason + " / Size = "+ state.size);
                throw "Failed to yield script";
            }
            else if ( state.status == 'RESUME' )
            {
                nlapiLogExecution("AUDIT", "Resuming script because of " + state.reason+".  Size = "+ state.size);
            }
            else {
                nlapiLogExecution("AUDIT", "STATE" + JSON.stringify(state)+".  Size = "+ state.size);
            }
            // state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield
        }
    };

    return {
        main: main
    };

}());