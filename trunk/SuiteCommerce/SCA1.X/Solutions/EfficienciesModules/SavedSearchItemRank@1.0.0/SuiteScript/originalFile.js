/**
 * Copyright NetSuite, Inc. 2014 All rights reserved.
 * The following code is a demo prototype. Due to time constraints of a demo,
 * the code may contain bugs, may not accurately reflect user requirements
 * and may not be the best approach. Actual implementation should not reuse
 * this code without due verification.
 *
 * (Module description here. Whole header length should not exceed
 * 100 characters in width. Use another line if needed.)
 *
 * Version    Date            Author           Remarks
 * 1.00       18 Mar 2014     rfalcunit
 * 2.00       19 Sep 2014     hdaguerre			Added to support for Matrix parent items,
 * 												so that it cascades the ranking value to its child items
 *
 */
/*
 * Global Variables
 *  
 */
{
    var REC_MULTI_SITE_RANKING = 'customrecord_lr_rf_multisite_ranking';
    var FLD_MR_SAVED_SEARCH = 'custrecord_lr_rf_mr_saved_search';
    var FLD_MR_CUSTOM_FIELD = 'custrecord_lr_rf_mr_custom_field';

    var REC_ITEM = 'item';
    var REC_INVENTORY_ITEM = 'inventoryitem';
    var FLD_ITEM_TYPE = 'type';

    var USAGE_LIMIT_THRESHOLD = 250;

    var i, j, k;
    var DEF_LOOP_INDEX = 0;

    //hdaguerre29OCT14: Maps item types against json object, instead of switch/case clause
    var oItemRecTypeMapping =
    {
        'InvtPart' : 'inventoryitem',
        'NonInvtPart' : 'noninventoryitem',
        'Service' : 'serviceitem',
        'Assembly' : 'assemblyitem',
        'GiftCert' : 'giftcertificateitem',
        'Kit' : 'kititem'
    };
}

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @return {void}
 */
function multisiteRankingSS(type)
{

    var aMultiRanking = getAllMR(),
        context = nlapiGetContext(),
        nLpIndexCR = context.getSetting('SCRIPT', 'custscript_ranking_looping_custom_record'),
        nLpIndexSS = context.getSetting('SCRIPT', 'custscript_ranking_looping_saved_search'),
        nLpIndexCI = context.getSetting('SCRIPT', 'custscript_ranking_looping_child_items');

    try {
        for (i = (nLpIndexCR?nLpIndexCR:DEF_LOOP_INDEX); ( (aMultiRanking) && (i < aMultiRanking.length) ); i++) {
            //Gets values of columns in result set
            var idSavedSearch = aMultiRanking[i].getValue(FLD_MR_SAVED_SEARCH);
            var sCustFieldName = aMultiRanking[i].getText(FLD_MR_CUSTOM_FIELD);

            //Gets internal id of the custom field
            var sFieldID = getFieldInternalId(sCustFieldName);

            //Runs and fetches saved search results
            var aItems = getSavedSearchResults(idSavedSearch);
            nlapiLogExecution('DEBUG', 'Saved Search Results', JSON.stringify(aItems));

            if (aItems && aItems.length > 0) {

                for (j = (nLpIndexSS?nLpIndexSS:DEF_LOOP_INDEX); (j < aItems.length); j++) {
                    //var sItemType = aItems[j].getValue(FLD_ITEM_TYPE);
                    var aCols = aItems[j].getAllColumns();
                    var idItem = aItems[j].getId();
                    if (fnIsEmpty(idItem)) {
                        idItem = aItems[j].getValue(aCols[0]); //Make sure that the first column of the results is the Internal ID if the columns are grouped
                    }

                    //Retrieves item type and custom ranking value
                    var aItemFields = nlapiLookupField(REC_ITEM, idItem, [FLD_ITEM_TYPE, sFieldID]);

                    //Maps returned item type with valid internal type
                    //var sItemTypeID = getItemRecordType(aItemFields[FLD_ITEM_TYPE]);
                    var sItemTypeID = oItemRecTypeMapping[aItemFields[FLD_ITEM_TYPE]];

                    var nRank = Number(j) + 1;

                    nlapiLogExecution('DEBUG', 'Ranking', 'Item Id: '+ idItem);
                    //Only overwrites Ranking value if not equal
                    if (Number(aItemFields[sFieldID]) !== nRank) {

                        nlapiSubmitField(sItemTypeID, idItem, sFieldID, nRank);

                        if (reachUsageLimit(context, aItems, j))
                            return true;

                        nlapiLogExecution('DEBUG', 'Ranking', 'Old Value: '+ aItemFields[sFieldID] +' => New Value: '+ nRank);

                        /* hdaguerre 19SEP14: The following structure is used for cascading the resulting ranking number
                         to child items, if dealing with a matrix parent */

                        //Item is parent matrix item
                        if (!fnIsEmpty(isMatrixItem(sItemTypeID, idItem))){

                            //Fetches all of its child items
                            var aChildItems = getChildItems(sItemTypeID, idItem);
                            if (aChildItems){

                                for (k = (nLpIndexCI?nLpIndexCI:DEF_LOOP_INDEX); (k < aChildItems.length); k++) {
                                    nlapiSubmitField(sItemTypeID, aChildItems[k].getId(), sFieldID, nRank);
                                    //checkUsageLimit('Exec Units Logging');

                                    if (reachUsageLimit(context, aChildItems, k))
                                        return true;

                                }
                                nlapiLogExecution('DEBUG', 'Remaining Usage', nlapiGetContext().getRemainingUsage() +' Units Left');

                            }
                        }
                    }
                }

            }

        }

    } catch (exception) {
        nlapiLogExecution('ERROR', 'Exception Caught', exception.getCode()+ ': ' +exception.getDetails());
    }
}

function reachUsageLimit(context, aObjects, index){
    if ((context.getRemainingUsage() <= USAGE_LIMIT_THRESHOLD) && ((index+1) < aObjects.length)) {

        return nlapiScheduleScript(
            context.getScriptId(),
            context.getDeploymentId(),
            {
                'custscript_ranking_looping_custom_record': i,
                'custscript_ranking_looping_saved_search': j,
                'custscript_ranking_looping_child_items': k
            }
        );
        if (status == 'QUEUED')
            return true;
    }
    return false;
}

function isMatrixItem(sItemTypeID, idItem){
    return nlapiSearchRecord(sItemTypeID,
        null,
        [
            new nlobjSearchFilter('internalid', null, 'is', idItem),
            new nlobjSearchFilter('matrix', null, 'is', 'T')
        ],
        null
    );
}

function getChildItems(sItemTypeID, idItem){
    return nlapiSearchRecord(
        sItemTypeID,
        null,
        [new nlobjSearchFilter('parent', null, 'is', idItem)]
    );
}

function getAllMR()
{
    var aCols = [],
        aFilters = [];
    aCols.push(new nlobjSearchColumn(FLD_MR_SAVED_SEARCH));
    aCols.push(new nlobjSearchColumn(FLD_MR_CUSTOM_FIELD));
    aFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

    return nlapiSearchRecord(REC_MULTI_SITE_RANKING, null, aFilters, aCols);
}

function getSavedSearchResults(idSavedSearch)
{
    nlapiLogExecution('DEBUG', 'Saved Search', idSavedSearch);
    return nlapiSearchRecord(null, idSavedSearch);
}

//Cannot source the Script ID of the Custom Field of Field Type so get all field ids
//then get the field then compare the label.
function getFieldInternalId(sFieldName)
{
    var recItem = nlapiCreateRecord(REC_INVENTORY_ITEM);
    var aFields = recItem.getAllFields(); //returns all the field ids of an inventory record

    for(var i in aFields)
    {
        var fld = recItem.getField(aFields[i]);
        if(fld.getLabel() == sFieldName)
        {
            return aFields[i];
        }
    }

    return null;
}

/*function getItemRecordType(sItemType)
 {
 switch (sItemType)  // Compare item type to its record type counterpart
 {
 case 'InvtPart':
 recordtype = 'inventoryitem';
 break;
 case 'NonInvtPart':
 recordtype = 'noninventoryitem';
 break;
 case 'Service':
 recordtype = 'serviceitem';
 break;
 case 'Assembly':
 recordtype = 'assemblyitem';
 break;
 case 'GiftCert':
 recordtype = 'giftcertificateitem';
 break;
 case 'Kit':
 recordtype = 'kititem';
 break;
 }

 return recordtype;
 }*/

function fnIsEmpty(val) {
    return ((val == null) || (val == '') || (!val));
}