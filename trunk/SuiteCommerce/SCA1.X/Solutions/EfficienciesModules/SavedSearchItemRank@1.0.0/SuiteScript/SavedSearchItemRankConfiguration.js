Application.defineModel('SavedSearchItemRankConfiguration', {
    record: 'customrecord_ef_ssir_config',
    columns: {
        internalid: {fieldName: 'internalid'},
        name: {fieldName: 'name'},
        savedsearch : {fieldName: 'custrecord_ef_ssir_c_saved_search'},
        storagefieldname: {fieldName: 'custrecord_ef_ssir_c_storage_field'},
        storagefieldid: {fieldName: 'custrecord_ef_ssir_c_sf_scriptid'},
        isinactive: {fieldName: 'isinactive'}
    },
    filters : {
        base: [{fieldName: 'isinactive', operator: 'is', value1: 'F'}]
    },
    getActive: function(){
        var Search = new SearchHelper(
            this.record,
            this.filters.base,
            this.columns
        ).search();

        return Search.getResults();
    }
});