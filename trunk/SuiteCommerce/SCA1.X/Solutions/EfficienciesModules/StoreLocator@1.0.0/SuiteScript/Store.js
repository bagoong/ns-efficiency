/* global notFoundError:false, SearchHelper:false, Application:false */
Application.defineModel('Store',{
    record: 'customrecord_ef_sl_store',
    fieldsets: {
        basic: [
            'internalid',
            'name',
            'location',
            'urlcomponent',
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'lat',
            'lon',
            'phone',
            'zipcode',
            'thumbnail',
            'marker',
            'shortDescription'
        ],
        details: [
            'internalid',
            'name',
            'location',
            'urlcomponent',
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'lat',
            'lon',
            'phone',
            'zipcode',
            'mainImage',
            'thumbnail',
            'marker',
            'shortDescription',
            'longDescription'
        ]
    },
    filters: [
        {fieldName: 'isinactive', operator: 'is', value1: 'F'},
        {fieldName: 'custrecord_ef_sl_s_latitude', operator: 'isnotempty'},
        {fieldName: 'custrecord_ef_sl_s_longitude', operator: 'isnotempty'}
    ],
    columns: {
        internalid: {fieldName:'internalid'},
        name: {fieldName: 'name'},
        location: {fieldName: 'custrecord_ef_sl_s_location'},
        urlcomponent: {fieldName: 'custrecord_ef_sl_s_urlcomponent'},
        address1: {fieldName: 'custrecord_ef_sl_s_address1'},
        address2: {fieldName: 'custrecord_ef_sl_s_address2'},
        city: {fieldName: 'custrecord_ef_sl_s_city'},
        state: {fieldName: 'custrecord_ef_sl_s_state', type: 'text'},
        country: {fieldName: 'custrecord_ef_sl_s_country', type: 'text'},
        lat: {fieldName: 'custrecord_ef_sl_s_latitude'},
        lon: {fieldName: 'custrecord_ef_sl_s_longitude'},
        phone: {fieldName: 'custrecord_ef_sl_s_phone'},
        zipcode: {fieldName: 'custrecord_ef_sl_s_zipcode'},
        mainImage: {fieldName: 'custrecord_ef_sl_s_main_image', type:'object'},
        thumbnail: {fieldName: 'custrecord_ef_sl_s_thumbnail_image', type:'object'},
        marker: {fieldName: 'custrecord_ef_sl_s_marker_image', type:'object'},
        shortDescription: {fieldName: 'custrecord_ef_sl_s_short_description'},
        longDescription: {fieldName: 'custrecord_ef_sl_s_short_description'}
    },
    list: function()
    {
        var Search = new SearchHelper(this.record, this.filters, this.columns, this.fieldsets.basic).search();

        return Search.getResults();
    },
    getByUrlcomponent: function(urlcomponent)
    {
        if (urlcomponent) {

            var Search = new SearchHelper(this.record, this.filters, this.columns, this.fieldsets.details),
                store;

            Search.addFilter({fieldName: this.columns.urlcomponent.fieldName, operator: 'is', value1: urlcomponent});
            Search.search();
            store = Search.getResult();

            if (!store)
            {
                throw notFoundError;
            }

            return store;
        }
    }
});

