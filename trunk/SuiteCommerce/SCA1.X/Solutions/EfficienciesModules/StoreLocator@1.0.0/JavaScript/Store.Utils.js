(function ()
{
    'use strict';
    var toRadians = function (degrees)
    {
        return degrees * Math.PI / 180;
    };

    var extensions = {
        toRadians: toRadians
    };

    _.extend(SC.Utils, extensions);
    _.mixin(extensions);

})();