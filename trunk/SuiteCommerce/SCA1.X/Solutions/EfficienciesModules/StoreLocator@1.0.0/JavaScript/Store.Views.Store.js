define('Store.Views.Store', function ()
{
    'use strict';

    return Backbone.View.extend({
        template: 'store',
        attributes: {
            'id': 'store',
            'class': 'view store'
        },

        getTitle: function ()
        {
            return  _('Store: $(0)').translate(this.model.get('name'));
        },

        initialize: function (options)
        {
            this.options = options;
            this.application = options.application;
            this.model = options.model;
            this.configuration = this.application.getConfig('storeLocator');
        },

        getBreadcrumb: function ()
        {
            return [
                {
                    href: '/', text: _('Home').translate()
                },
                {
                    href: '/stores', text: _('Store Locator').translate()
                },
                {
                    href: '/stores/' + this.model.get('urlcomponent'), text: this.model.get('name')
                }
            ];
        }
    });
});