define('GoogleMapsLoader', ['Session'], function (Session)
{
    'use strict';

    var GoogleMapsLoader = {
        loadedPromise: jQuery.Deferred(),
        initialized: false,
        loadScript: function (apiKey)
        {
            var language = (Session.get('language.locale') || '').split('_')[0],
                region = ((Session.get('language.locale') || '_').split('_')[1]).toLowerCase(),
                key = apiKey, baseUrl = '//maps.googleapis.com/maps/api/js?sensor=false&libraries=places&callback=_gMapsCallback',
                url = _.addParamsToUrl(baseUrl, {
                    language: language,
                    region: region,
                    key: key
                });

            if (!this.initialized)
            {
                if (SC.ENVIRONMENT.jsEnvironment === 'browser')
                {
                    jQuery.ajax({
                        url: url,
                        dataType: 'script',
                        cache: true, //always call this script without appending timestamp params to the url
                        preventDefault: true // scripts like this one are probably third party. Don't throw internal errors/404 because of third party integration
                    });
                } else
                {
                    this.loadedPromise.rejectWith('Google Maps is a Browser Script only');
                }

                this.initialized = true;
            }

            return this.loadedPromise;
        }
    };
    window._gMapsCallback = function ()
    {
        GoogleMapsLoader.loadedPromise.resolve();
    };
    return GoogleMapsLoader;
});