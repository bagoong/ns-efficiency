define('Store.Collection', ['Store.Model'], function (Model)
{
    'use strict';
    //HOLA
    return Backbone.CachedCollection.extend({
        url: _.getAbsoluteUrl('services/store.ss'),
        model: Model,
        sortByNearest: function (point)
        {

            this.sortByDistanceCache = this.sortByDistanceCache || {};
            var k = point.lat() + '-' + point.lng();

            if (!this.sortByDistanceCache[k])
            {

                delete this.sortByDistanceCache;
                this.sortByDistanceCache = {}; //Limit cache to last point in a quick way

                this.sortByDistanceCache[k] = this.sortBy(function (a)
                {
                    return a.distanceTo(point);
                });
            }

            return this.sortByDistanceCache[k];
        }
    });
});
