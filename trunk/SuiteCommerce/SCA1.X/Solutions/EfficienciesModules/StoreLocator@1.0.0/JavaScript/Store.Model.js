define('Store.Model', [], function ()
{
    'use strict';

    return Backbone.CachedModel.extend({
        urlRoot: _.getAbsoluteUrl('services/store.ss'),
        getUrl: function ()
        {
            if (this.get('urlcomponent'))
            {
                return '/store/' + this.get('urlcomponent');
            }
        },

        getLocation: function ()
        {
            return {
                lat: this.get('lat'),
                lng: this.get('lon')
            };
        },

        distanceTo: function (point)
        {

            //TODO: cache should be cleaned on lat/lng changes
            this.distanceCache = this.distanceCache || {};
            var k = point.lat() + '-' + point.lng();

            if (!this.distanceCache[k])
            {

                delete this.distanceCache;
                this.distanceCache = {}; //Limit cache to last address in a quick way

                var R = 6371, // mean radius of earth
                    location = this.getLocation(),
                    lat1 = _.toRadians(location.lat),
                    lon1 = _.toRadians(location.lng),
                    lat2 = _.toRadians(point.lat()),
                    lon2 = _.toRadians(point.lng()),
                    dLat = lat2 - lat1,
                    dLon = lon2 - lon1,
                    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2),
                    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                this.distanceCache[k] = R * c;
            }

            return this.distanceCache[k];
        }
    });

});
