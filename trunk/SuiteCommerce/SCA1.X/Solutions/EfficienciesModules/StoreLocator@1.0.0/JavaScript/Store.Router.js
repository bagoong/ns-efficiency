define('Store.Router', [
    'Store.Model',
    'Store.Collection',
    'Store.Views.StoreLocator',
    'Store.Views.Store'
], function (Model, Collection, StoreLocatorView,StoreView)
{
    'use strict';

    return Backbone.Router.extend({
        routes: {
            'stores': 'storeLocator',
            'stores?*': 'storeLocator'
        },

        initialize: function (application)
        {
            this.application = application;
            // Routing of store pages is optional. Refer to backend Store.Config to set it up
            if(this.application.getConfig('storeLocator.storePage'))
            {
                this.route('stores/:urlcomponent', 'store');
                this.route('stores/:urlcomponent?*', 'store');
            }
        },

        storeLocator: function ()
        {
            var collection = new Collection(null, {
                    comparator: this.application.getConfig('storeLocator.list.defaultSort')
                }),
                self = this,
                view = new StoreLocatorView({
                    application: this.application,
                    collection: collection
                });

            collection.fetch({
                killerId: self.application.killerId
            }).success(function ()
            {
                view.showContent();
            });
        },

        store: function (urlcomponent)
        {
            var model = new Model(),
                view = new StoreView({
                    application: this.application,
                    model: model
                });

            model.fetch({
                data: {
                    urlcomponent: urlcomponent
                },
                killerid: this.application.killerid,
                success: function ()
                {
                    view.showContent();
                }
            });
        }
    });
});