/**
 This file is provided just for reference.
 Add the content to your Configuration file.
 Refer to Store.js for configuration options
 */
(function (application) {

    'use strict';
    application.Configuration.modules.push('Store');

    _.extend(application.Configuration, {
        storeLocator : {geolocateOnInit:false}
    });

}(SC.Application('Shopping')));
