define('Store.Views.StoreLocator.List', function ()
{
    'use strict';

    return Backbone.View.extend({
        template: 'storelocator_list',
        events: {
            'click li[data-type=stores-list-store]': 'selectStore',
            'click a.store-link': 'preventSelectStore'
        },
        currentDistanceRatio: null,
        initialize: function (options)
        {
            this.collection = options.collection;
            this.listenTo(options.eventBus, 'storeSelected', _.bind(this.storeSelected, this));
            this.listenTo(options.eventBus, 'originChanged', _.bind(this.originChanged, this));
        },

        preventSelectStore: function(e)
        {
            e.stopPropagation();
        },

        originChanged: function (options)
        {
            this.currentOrigin = null;

            if (options.location)
            {
                this.currentOrigin = options.location;
            }

            if (options.distanceRatio)
            {
                this.currentDistanceRatio = options.distanceRatio;
            }

            this.render();
        },

        //Used by the template to get the stores
        getStoresByProximity: function ()
        {
            var self = this,
                nearest,
                sorted;

            if (!this.currentOrigin)
            {
                return {
                    nearest: this.collection.models
                };

            } else
            {
                sorted = this.collection.sortByNearest(this.currentOrigin, this.currentDistanceRatio);

                if (this.currentDistanceRatio)
                {
                    nearest = _.filter(sorted, function (f)
                    {
                        return f.distanceTo(self.currentOrigin) < self.currentDistanceRatio;
                    });
                    return {
                        nearest: nearest,
                        alsoNear: _.first(_.rest(sorted, nearest.length), self.options.configuration.list.maxSuggestionsOutOfLimits)
                    };
                } else
                {
                    return {
                        nearest: sorted
                    };
                }
            }
        },

        storeSelected: function (options)
        {
            /* cleanup first */
            var $element;
            this.$('li[data-type=stores-list-store]').removeClass('store-highlighted');

            /* then work if store selected */
            if (!options.store)
            {
                return;
            }

            $element = options.$element || this.$('li[data-id=' + options.store.id + ']');
            $element.addClass('store-highlighted');
        },

        selectStore: function (e)
        {
            var $element = jQuery(e.currentTarget),
                selectedId = $element.data('id'),
                store = this.collection.get(selectedId);

            this.options.eventBus.trigger('storeSelected', {
                store: store,
                $element: $element,
                component: 'list'
            });
        },

        destroy: function ()
        {
            this._destroy();
        }
    });
});