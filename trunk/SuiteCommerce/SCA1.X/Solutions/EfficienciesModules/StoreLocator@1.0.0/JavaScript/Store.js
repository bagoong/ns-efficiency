define('Store', [
    'Store.Model',
    'Store.Collection',
    'Store.Router'
], function (Model, Collection, Router)
{
    'use strict';

    return {
        Model: Model,
        Collection: Collection,
        Router: Router,
        mountToApp: function (application)
        {
            var unitsHandlers = {
                miles: {
                    precision: 0,
                    id: 'miles',
                    shortName: 'Mi.',
                    name: _.translate('Miles'),
                    conversionFromKilometers: function (value)
                    {
                        return value * 0.621371192;
                    }, conversionToKilometers: function (value)
                    {
                        return 1.609344 * value;
                    }
                },
                kilometers: {
                    precision: 0,
                    id: 'KILOMETERS',
                    shortName: _.translate('Kilometers'),
                    name: _.translate('Kilometers'),
                    conversionFromKilometers: function (value)
                    {
                        return value;
                    },
                    conversionToKilometers: function (value)
                    {
                        return value;
                    }
                }
            };

            var config = application.Configuration;
            config.storeLocator = _.defaults(config.storeLocator, SC.ENVIRONMENT.Efficiencies.StoreLocator, {
                unitsHandlers: unitsHandlers,
                geolocateOnInit: true,
                geolocationEnabled: window.navigator && navigator.geolocation,
                googleMapsApiKey: 'XXX',

                map: {
                    defaultZoom: 12,
                    selectedStoreZoom: 14,
                    mapTypeControl: false,
                    panControl: false,
                    zoomControl: true,
                    scaleControl: true,
                    animation: 'DROP', //https://developers.google.com/maps/documentation/javascript/reference#Animation
                    streetViewControl: false, //TODO: handle streetViewControl for nice store view
                    allowDrag: true, //Allow the icons to be dragged
                    defaultCenter: { //Center of the map by default. It's now on USA
                        lat: 39.912811,
                        lng: -101.465781
                    }
                },
                //This is for example, for removing other bussiness from our map
                mapStyles: [ //https://developers.google.com/maps/documentation/javascript/reference#MapTypeStyleFeatureType
                    {
                        featureType: 'poi.business',
                        elementType: 'labels',
                        stylers: [
                            { visibility: 'off' }
                        ]
                    }
                ],
                storeMarker: {
                    animation: 'DROP',
                    size: {
                        x: 32,
                        y: 32
                    },
                    origin: {
                        x: 0,
                        y: 0
                    },
                    anchor: {
                        x: 15,
                        y: 32
                    },
                    icon: '//www.google.com/mapfiles/ms/micons/red-dot.png' // Select image from https://sites.google.com/site/gmapsdevelopment/
                },
                originMarker: {
                    animation: 'DROP',
                    text: _('You\'re here').translate(),
                    size: {
                        x: 39,
                        y: 34
                    },
                    origin: {
                        x: 0,
                        y: 0
                    },
                    anchor: {
                        x: 11,
                        y: 33
                    },
                    icon: '//www.google.com/mapfiles/arrow.png' // Select image from https://sites.google.com/site/gmapsdevelopment/
                },
                radiusCircle: {
                    draw: true,
                    style: {
                        strokeWeight: 1,
                        strokeColor: '#0000FF',
                        fillColor: '#0000FF',
                        strokeOpacity: 0.5,
                        fillOpacity: 0.08
                    }
                },
                list: {
                    maxSuggestionsOutOfLimits: 5,
                    defaultSort: 'name' //You could have a Country>State>City sort. this goes to a comparator: http://backbonejs.org/#Collection-comparator
                },

                SchemaOrg: 'Store', //http://schema.org/Store You could change it to a child of store.
                units: 'miles',
                distanceFilters: [
                    { text: _('$(0) Miles').translate('5'), unit: 'miles', value: 5},
                    { text: _('$(0) Miles').translate('50'), unit: 'miles', value: 50, isDefault: true},
                    { text: _('$(0) Miles').translate('100'), unit: 'miles', value: 100}
                    //{ text: _('10 Kilometers').translate(), unit: 'kilometers', value: 10},
                    //{ text: _('20 Kilometers').translate(), unit: 'kilometers', value: 20, isDefault: true},
                    //{ text: _('50 Kilometers').translate(), unit: 'kilometers', value: 50},
                    //{ text: _('100 Kilometers').translate(), unit: 'kilometers', value: 100}
                ]
            });

            config.storeLocator.unitsHandler = config.storeLocator.unitsHandlers[config.storeLocator.units];
            if (!config.storeLocator.unitsHandler)
            {
                throw Error('Units Handler needed');
            }

            return new Router(application);
        }
    };
});