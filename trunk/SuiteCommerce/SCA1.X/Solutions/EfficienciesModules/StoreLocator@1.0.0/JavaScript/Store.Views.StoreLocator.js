define('Store.Views.StoreLocator', [
    'Store.Views.StoreLocator.Map',
    'Store.Views.StoreLocator.List',
    'Store.Views.StoreLocator.Search'
], function (Map, List, Search)
{
    'use strict';

    return Backbone.View.extend({

        template: 'storelocator',
        attributes: {
            'id': 'store-locator',
            'class': 'view store-locator'
        },
        getTitle: function ()
        {
            return  _('Store Locator').translate();
        },

        initialize: function (options)
        {
            this.options = options;
            this.application = options.application;
            this.collection = options.collection;
            this.configuration = this.application.getConfig('storeLocator');
            this.eventBus = _.extend({}, Backbone.Events);

            var childrenOpts = {
                application: this.application,
                collection: this.collection,
                configuration: this.configuration,
                eventBus: this.eventBus
            };

            this.childrenViews = {
                map: new Map(childrenOpts),
                list: new List(childrenOpts),
                search: new Search(childrenOpts)
            };
        },

        getBreadcrumb: function ()
        {
            return [
                {
                    href: '/', text: _('Home').translate()
                },
                {
                    href: '/stores', text: _('Store Locator').translate()
                }
            ];
        },

        showContent: _.wrap(Backbone.View.prototype.showContent, function (fn)
        {
            var ret = fn.apply(this, _.toArray(arguments).slice(1));
            ret.done(_.bind(this.renderSubViews, this));
            return ret;
        }),

        renderSubViews: function ()
        {
            this.childrenViews.map.setElement(this.$('#map-placeholder')).render();
            this.childrenViews.list.setElement(this.$('#list-placeholder')).render();
            this.childrenViews.search.setElement(this.$('#search-placeholder')).render();
        },

        destroy: function ()
        {
            _.each(this.childrenViews, function (v)
            {
                v.destroy();
            });
            this.childrenViews = null;
            this._destroy();
        }
    });
});

