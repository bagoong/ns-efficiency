/*global google:true */
define('Store.Views.StoreLocator.Map', ['GoogleMapsLoader'], function (GoogleMapsLoader)
{
    'use strict';

    return Backbone.View.extend({
        _markerCache: {},
        template: 'storelocator_map',
        initialize: function (options)
        {
            this.collection = options.collection;
        },

        createOriginMarker: function (location)
        {
            if (this._originMarker)
            {
                this._originMarker.setMap(null);
            }

            var imageData = this.options.configuration.originMarker,
                image = {
                    url: imageData.icon,
                    size: new google.maps.Size(imageData.size.x,imageData.size.y),
                    origin: new google.maps.Point(imageData.origin.x,imageData.origin.y),
                    anchor: new google.maps.Point(imageData.anchor.x,imageData.anchor.y)
                },
                markerOptions = {
                    position: location,
                    title: this.options.configuration.originMarker.text,
                    draggable: this.options.configuration.map.allowDrag,
                    animation: google.maps.Animation[this.options.configuration.originMarker.animation],
                    icon: image
                };

            this._originMarker = new google.maps.Marker(markerOptions);
            this._originMarker.setMap(this.map);

            return this._originMarker;
        },

        originChanged: function (options)
        {
            if (!options.location)
            {
                if (this._searchRadiusCircle)
                {
                    this._searchRadiusCircle.setMap(null);
                }
                if (this._originMarker)
                {
                    this._originMarker.setMap(null);
                }
                this.currentOrigin = null;
                this.centerMapToStoreBounds(this.collection);
                this._infoWindow.close();
                return;
            }

            var location = options.location,
                distanceRatio = options.distanceRatio,
                self = this,
                marker = this.createOriginMarker(location),
                radiusCircle = this.createRadiusCircle(location, distanceRatio),
                bounds,
                nearest = _.filter(this.collection.sortByNearest(location), function (f)
                {
                    return f.distanceTo(location) < distanceRatio;
                });

            this.currentOrigin = options.location;

            if (nearest.length)
            {
                bounds = this.getBounds(_(nearest), location);
            } else
            {
                bounds = radiusCircle.getBounds();
            }

            if (options.subcomponent !== 'drag')
            {
                this.map.panTo(location);
                this.map.fitBounds(bounds);
            } else
            {
                this.map.fitBounds(radiusCircle.getBounds());
            }

            if (this.options.configuration.map.allowDrag)
            {
                this._dragListener = google.maps.event.addListenerOnce(this._originMarker, 'dragend', function ()
                {

                    // TODO: REVIEW IF WRITING SEARCH BOX IS NECESSARY
                    /*
                    self._geocoder.geocode({
                        latLng: marker.getPosition()
                    }, function (results, status)
                    {
                        if (status === google.maps.GeocoderStatus.OK)
                        {

                            self.options.eventBus.trigger('originChanged', {
                                location: results[0].geometry.location,
                                component: 'search',
                                distanceRatio: distanceRatio
                            });
                        }
                    });
                    */

                    self.options.eventBus.trigger('originChanged', {
                        location: marker.getPosition(),
                        distanceRatio: distanceRatio,
                        component: 'map',
                        subcomponent: 'drag'
                    });

                });
            }
        },

        createRadiusCircle: function (location, ratio)
        {
            var radiusConfig = this.options.configuration.radiusCircle;
            //Clean previous
            if (this._searchRadiusCircle)
            {
                this._searchRadiusCircle.setMap(null);
            }

            this._searchRadiusCircle = new google.maps.Circle(_.defaults(radiusConfig.style, {
                strokeWeight: 2,
                strokeColor: '#0000FF',
                fillColor: '#0000FF',
                strokeOpacity: 0.5,
                fillOpacity: 0.1
            }));
            this._searchRadiusCircle.setCenter(location);
            this._searchRadiusCircle.setRadius(ratio * 1000);
            if (radiusConfig.draw)
            {
                this._searchRadiusCircle.setMap(this.map);
            }
            return this._searchRadiusCircle;
        },

        getInfoWindow: function (store)
        {
            if (!store)
            {
                return this._infoWindow;
            }
            var div = SC.macros.storeLocatorStoreInfoWindow(store, this.options.configuration, this._originMarker && this._originMarker.position);
            this._infoWindow.setContent(div);

            return this._infoWindow;

        },

        storeSelected: function (options)
        {
            if (!options.store)
            {
                this._infoWindow.close();
                return;
            }

            var store = options.store,
                map = this.map,
                infoWindow = this.getInfoWindow(store),
                loc,
                bounds;

            if (store)
            {
                if (store.get('marker'))
                {
                    infoWindow.open(map, store.get('marker'));
                } else
                {
                    infoWindow.setPosition(new google.maps.LatLng(store.get('lat'), store.get('lon')));
                    infoWindow.open(map);
                }

                loc = new google.maps.LatLng(store.get('lat'), store.get('lon'));

                if (this.currentOrigin && options.component === 'list' /*if clicking on map zoom on location*/)
                {
                    bounds = new google.maps.LatLngBounds();
                    bounds.extend(this.currentOrigin);
                    bounds.extend(loc);

                    map.panTo(loc);
                    map.fitBounds(bounds);

                } else
                {
                    map.panTo(loc);
                    map.setZoom(this.options.configuration.map.selectedStoreZoom);
                }

            } else
            {
                infoWindow.close();
            }
        },

        render: function ()
        {
            this._render();
            GoogleMapsLoader.loadScript(this.options.configuration.googleMapsApiKey).done(_.bind(this.mapSetup, this));
        },

        mapSetup: function ()
        {
            this.listenTo(this.options.eventBus, 'originChanged', _.bind(this.originChanged, this));
            this.listenTo(this.options.eventBus, 'storeSelected', _.bind(this.storeSelected, this));

            this._infoWindow = new google.maps.InfoWindow;
            this._geocoder = new google.maps.Geocoder;

            this.createMap();
            this.drawStores(this.collection);
            this.setEvents();
            this.centerMapToStoreBounds(this.collection);
        },

        createMap: function ()
        {
            var config = this.options.configuration,
                defaultCenter = config.map.defaultCenter;

            this.map = new google.maps.Map(this.$('#storelocator-map-placeholder')[0], {
                zoom: config.map.defaultZoom,
                animation: google.maps.Animation[config.map.animation],
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(defaultCenter.lat, defaultCenter.lng),
                mapTypeControl: config.map.mapTypeControl,
                panControl: config.map.panControl,
                zoomControl: config.map.zoomControl,
                scaleControl: config.map.scaleControl,
                streetViewControl: config.map.streetViewControl,
                styles: config.mapStyles
            }, config.mapStyles);
        },
        getBounds: function (stores, location)
        {

            var bounds = new google.maps.LatLngBounds();
            stores.each(function (s)
            {
                bounds.extend(new google.maps.LatLng(s.get('lat'), s.get('lon')));
            });

            if (location)
            {
                bounds.extend(location);
            }

            return bounds;

        },
        centerMapToStoreBounds: function (stores)
        {
            var bounds = this.getBounds(stores);
            this.map.setCenter(bounds.getCenter());
            this.map.fitBounds(bounds);
        },
        setEvents: function ()
        {
            var self = this;

            google.maps.event.addListener(this._infoWindow, 'closeclick', function ()
            {
                self.options.eventBus.trigger('storeSelected', {component: 'map'});
            });

            google.maps.event.addListener(this.map, 'click', function ()
            {
                self.options.eventBus.trigger('storeSelected', {component: 'map'});
            });
        },

        drawStores: function (stores)
        {
            var self = this;
            stores.each(function (s)
            {
                self.addStoreToMap(s);
            });
        },

        addStoreToMap: function (store)
        {

            var marker = this.getMarker(store),
                self = this;
            store.set('marker', marker);

            if (marker.getMap() !== this.map)
            {
                marker.setMap(this.map);
            }

            marker.clickListener_ = google.maps.event.addListener(marker, 'click', function ()
            {
                self.options.eventBus.trigger('storeSelected', {
                    store: store,
                    component: 'map'
                });
            });
        },

        getMarker: function (store)
        {
            var cache = this._markerCache || {},
                key = store.id;

            if (!cache[key])
            {
                cache[key] = this.createMarker(store);
            }

            return cache[key];
        },

        createMarker: function (store)
        {

            var location = store.getLocation(),
                imageData = this.options.configuration.storeMarker,
                image = {
                    url: store.get('markerImage') || imageData.icon,
                    origin: new google.maps.Point(imageData.origin.x,imageData.origin.y),
                    size: new google.maps.Size(imageData.size.x,imageData.size.y),
                    anchor: new google.maps.Point(imageData.anchor.x,imageData.anchor.y)
                },
                markerOptions = {
                    position: new google.maps.LatLng(location.lat, location.lng),
                    title: this.options.configuration.originMarker.text,
                    draggable: false,
                    animation: google.maps.Animation[this.options.configuration.storeMarker.animation],
                    icon: image
                };

            return new google.maps.Marker(markerOptions);
        },

        destroy: function ()
        {
            this.stopListening();
            this.collection.each(function (s)
            {
                s.set('marker', null);
            });

            this._infoWindow.setMap(null);

            _.each(this._markerCache, function (m)
            {
                m.setMap(null);
            });

            if (this._dragListener)
            {
                google.maps.event.removeListener(this._dragListener);
            }
            this._destroy();
        }
    });
});