Gift Wrap Module:

Requisites:
Item Field: custitem_ef_gw_is_giftwrap
Item Field: custitem_ef_gw_gwlist
Item Option: custcol_ef_gw_message
Item Option: custcol_ef_gw_giftwrap
Transaction Column: custcol_ef_gw_id
(They all come on the bundle)

Website->Facet: custitem_ef_gw_is_giftwrap. Needs to be set up manually

Setup:
0) Put "custitem_ef_gw_is_giftwrap" as facet field.

1) Create "Gift Wrap Items"
They can have charge or not, and they can be many (and you'll get a dropdown on the PDP) or one (and you'll get a checkbox)
Check "Is gift Wrap" on SuiteCommerce Extensions

2) Go to every item that is gift wrappeable, and update the multiselect field "custitem_ef_gw_gwlist",
selecting which gift wraps apply to that particular item.

