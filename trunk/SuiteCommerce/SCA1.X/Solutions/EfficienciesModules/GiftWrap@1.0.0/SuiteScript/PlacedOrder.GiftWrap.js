Application.on('before:PlacedOrder.setLines', function (Model, placed_order, ongoing_result) {

    //I needed the input, in order to format the output. That's why on the before i save the placed order reference
    Application.once('after:PlacedOrder.setLines', function (Model)
    {
        ongoing_result.lines = Model.rearrangeLinesForGiftWrap(placed_order, ongoing_result.lines);
    });
});

if(Application.originalModels.PlacedOrder) {
    Application.extendModel('PlacedOrder', {
        /*
         We want to move the gift wrap lines so they are childs of the item lines
         In that way we can then show them everywhere as children instead of extra lines that don't make sense without their parents
         */
        rearrangeLinesForGiftWrap: function (placed_order, lines) {

            var giftKeys = {},
                offsets = 0,
                giftWrapsHash = {},
                i,
                j;

            //Find out for every item of the order, the transaction column field value
            for (j = 1; j <= placed_order.getLineItemCount('item'); j++) {
                giftKeys[placed_order.getLineItemValue('item', 'id', j)] = placed_order.getLineItemValue('item', 'custcol_ef_gw_id', j);
            }


            for (i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (line.options) {
                    var option = giftKeys[line.internalid],
                        type = option && option.substr(0, 2);

                    if (type) {
                        var key = option.replace(type, '');
                        if (type == 'G:') {
                            giftWrapsHash[key] = giftWrapsHash[key] || {};
                            giftWrapsHash[key].giftwrap = i;
                        }
                        if (type == 'P:') {
                            giftWrapsHash[key] = giftWrapsHash[key] || {};
                            giftWrapsHash[key].parent = i;
                        }
                    }
                }
            }

            //Move every giftWrap to its parent line and remove it from lines
            _.each(giftWrapsHash, function (value, key) {

                if (value && !_.isUndefined(value.giftwrap) && !_.isUndefined(value.parent)) {

                    lines[value.parent - offsets].giftWrap = lines[value.giftwrap - offsets];
                    lines.splice(value.giftwrap - offsets, 1);

                    offsets++;

                } else {
                    console.error('GiftWrap', 'Error with:' + JSON.stringify(key) + ' ' + JSON.stringify(value));
                }

            });


            return lines;
        }
    });
}

