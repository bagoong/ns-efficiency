define('GiftWrap.ItemCollection', ['ItemDetails.Collection', 'Session'], function (ItemCollection, Session) {

    /*
     This defines a collection of all gift wrap items
     */
    return ItemCollection.extend({
        url: function ()
        {
            return _.addParamsToUrl(
                '/api/items'
                //API URL NEEDS: searchApiMasterOptions, session params, order fieldset, and only gift wrap items
                , _.extend(
                    {
                        fieldset: 'order'
                    }
                    , this.searchApiMasterOptions
                    , Session.getSearchApiParams()
                    , {
                        custitem_ef_gw_is_giftwrap: true
                    }
                )
            );

        }
    })
});

