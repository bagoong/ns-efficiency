define('GiftWrap', ['GiftWrap.ItemCollection','Item.Model.GiftWrap','OrderLineModel.GiftWrap'],function(GiftWrapCollection, Item, Line) {

    //These are extended, and it's methods overrided.
    Item,Line;

    return {
        Collection: GiftWrapCollection,
        mountToApp: function(application){

        }
    }
});