(function (SC) {

    'use strict';

    var keys = _.keys(SC._applications),
        application = SC._applications[keys[0]],
        configuration = application.Configuration;

    //ItemOptions is not defined on Some Apps by default!
    configuration.itemOptions = application.Configuration.itemOptions || [];

    //Push the module
    configuration.modules.push('GiftWrap');


    //For this you need to create the facet on the backend
    // First exclude giftwrap items
    // Then also exclude the facet from the response, it's not something that the user will filter by
    var apiOptions = configuration.searchApiMasterOptions;
    _.each(apiOptions, function (value)
    {
        _.extend(value, {
            'custitem_ef_gw_is_giftwrap' : false,
            'facet.exclude' : value['facet.exclude']? value['facet.exclude']+',' + 'custitem_ef_gw_is_giftwrap' : 'custitem_ef_gw_is_giftwrap'
        });
    });

    configuration.itemOptions = application.Configuration.itemOptions || [];
    configuration.itemOptions.push({
        cartOptionId: 'custcol_ef_gw_id',
        label: 'void',
        url: 'gwid',
        macros: {
            selector: 'void',
            selected: 'void'
        }
    });

    configuration.itemOptions.push({
        cartOptionId: 'custcol_ef_gw_giftwrap',
        label: _('Gift Wrap').translate(),
        url: 'gw',
        macros: {
            selector: 'itemDetailsOptionGiftWrap',
            selected: 'shoppingCartOptionGiftWrap'
        }
    });

    configuration.itemOptions.push({
        cartOptionId: 'custcol_ef_gw_message',
        label: _('Gift Wrap Message').translate(),
        url: 'gwmsg',
        macros: {
            selector: 'itemDetailsOptionGiftMessage',
            selected: 'shoppingCartOptionGiftWrapMessage'
        }
    });



    _.extend(configuration.itemKeyMapping, {
        //For an item to be gift wrappable, it needs to have at least one "gift wrap item" possible on the itemoption custcol_ef_gw_giftwrap
        _isGiftWrappable: function (item) {
            var option = item.getPosibleOptionByCartOptionId('custcol_ef_gw_giftwrap');
            return option && option.values && option.values.length > 1;
        },
        //Is it a gift wrap item? (Default:false)
        _isGiftWrap: 'custitem_is_giftwrap'
    });


}(SC));