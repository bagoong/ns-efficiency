define('OrderLineModel.GiftWrap', ['OrderLine.Model'], function(OrderLineModel){

    _.extend(OrderLineModel.prototype, {
        initialize: _.wrap(OrderLineModel.prototype.initialize, function(fn,attributes)
        {
            fn.apply(this, _.toArray(arguments).slice(1));

            //when the line has a gift wrap, we'll re-format the line in order to improve how to show it.
            //Because of some macro issues (Show item options) we're also hacking the lineOption Value
            this.on('change:giftwrap', function (model, gw) {

                var line = new (OrderLineModel)(gw, {silent:true} );
                model.set('giftwrap', line, {silent:true} );
                model.get('item').set('giftwrap',line);

                var lineOption = _.findWhere(this.get('options'),{id:'CUSTCOL_EF_GW_GIFTWRAP'});
                _.extend(lineOption,{
                    displayvalue:  line.get('item').get('_name') + ' ' + line.get('item').getPrice().price_formatted
                });

            });
            if(attributes.giftWrap){
                this.trigger('change:giftwrap', this, attributes.giftWrap);
            }
        })
    });
});

