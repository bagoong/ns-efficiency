define('Item.Model.GiftWrap', ['ItemDetails.Model', 'GiftWrap.ItemCollection'], function (ItemModel, GiftWrapCollection) {


    _.extend(ItemModel.prototype, {
        initialize: _.wrap(ItemModel.prototype.initialize, function (fn)
        {
            fn.apply(this, _.toArray(arguments).slice(1));
            this.giftWrapItems = new GiftWrapCollection();
        }),
        shouldEnableGiftMessage: function ()
        {
            //This is a shorthand to find out if we should ask for a Gift Message (it's only if there is a giftwrap set)
            return this.getOption('custcol_ef_gw_giftwrap');
        },
        fetch: _.wrap(ItemModel.prototype.fetch, function (fn, options)
        {

            var originalResponse = fn.apply(this, [options]),
                giftWrapItemsResponse = this.giftWrapItems.fetch();

            //Return a promise that is a combination of fetching the original item and the gift wrap collection
            return jQuery.when(originalResponse, giftWrapItemsResponse);

        })
    });

})