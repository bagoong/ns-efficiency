Module to be used specially by customers to create content delivery services with add to cart buttons/banners
It's not the recommended way to add products (you should fetch the items from the Item API).

YOUR HTML SHOULD LOOK LIKE:
<div>
    <form data-form-type="addToCartFromStaticHTML">
        <input name="item" type="hidden" value="984">
        <select data-item-selector="select" name="item-options">
            <option value='{"custcol3":"1"}'>Medium</option>
            <option value='{"custcol3":"2"}'>Large</option>
            <option value='{"custcol3":"3"}'>Extra Large</option>
        </select>
        <input name="item-quantity" value="1" type="number">
        <input value="Add to Cart" type="submit">
 </form>
 </div>

Form: data-form-type=addToCartFromStaticHTML
Item id (hidden input) name=item
Item Options, if Needed: name=item-options, value: json stringified property-value object
Quantity: name: item-quantity