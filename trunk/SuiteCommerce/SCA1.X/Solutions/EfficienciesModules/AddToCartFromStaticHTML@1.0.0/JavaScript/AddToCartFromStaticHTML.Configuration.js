;(function (SC) {

    'use strict';
    var keys = _.keys(SC._applications),
        application = SC._applications[keys[0]],
        configuration = application.Configuration;

    configuration.modules.push('AddToCartFromStaticHTML');

}(SC));
