define('AddToCartFromStaticHTML', ['ItemDetails.Model'], function(ItemDetails){
    'use strict';

    var helper = {
        addToCartHandler: function(application,e) {
            //avoid real form submit
            e.preventDefault();
            var cart =  application.getCart(),
                layout = application.getLayout(),
                $form = jQuery(e.target),
                $item = $form.find('[name=item]'),
                $options = $form.find('[name=item-options]'),
                itemID = $item.val(),
                itemOptions = {},
                qty = parseInt($form.find('[name=item-quantity]').val()),
                item = new ItemDetails({internalid: itemID});


            try
            {
                itemOptions = JSON.parse($options.find(':selected').val());
            } catch(e)
            {
                console.error('Error parsing itemoptions')
            }

            //Generate a mock "itemDetails" object for add to cart
            item.itemOptions = {};
            _.each(itemOptions,function(value,key){
                item.itemOptions[key] = {
                    internalid:value,
                    label:value
                };
            });
            item.set('quantity',qty);


            //Add to cart
            cart.addItem(item).success(function ()
            {
                if (cart.getLatestAddition())
                {
                    layout.showCartConfirmation();
                }
                else
                {
                    self.showError();
                }
            });

        }
    };

    return {
        Helper: helper,
        mountToApp: function(application) {
            //Listen to forms of that new data-form-type
            var layout =  application.getLayout();
            layout.events['submit form[data-form-type=addToCartFromStaticHTML]'] = 'addToCartFromStaticHTML';
            layout.addToCartFromStaticHTML = _.partial(helper.handler,application);
        }
    };

});

