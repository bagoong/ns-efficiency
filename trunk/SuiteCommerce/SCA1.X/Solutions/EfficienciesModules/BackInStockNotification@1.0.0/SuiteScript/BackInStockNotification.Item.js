/* Model that leverages the concept of an item in backend and in frontend
There are a series of processes that an item goes through before being exposed on a website.
Sadly all that transformations, that mean associations between the item properties
and the website properties, are not available in an Schedule Script Context
So we have to recreate all that logic by ourselves.
Meaning chosing the correct price to show (from the webstore pricelevel)
and regenerating the path to the image folder, or the correct item url
 */
Application.defineModel('BackendStoreItem', {
    mapping: {
        'InvtPart': ['inventoryitem'],
        'Kit': ['kititem']
    },
    record: 'item',
    fields: {
        //basic fields exposed. Add your custom fields here!
        internalid: {fieldName: 'internalid'},
        itemid: {fieldName: 'itemid'},
        type: {fieldName: 'type'},
        itemtype: {fieldName: 'itemtype'},
        matrixtype: {fieldName: 'matrixtype'},
        parent: {fieldName: 'parent'},
        urlcomponent: {fieldName: 'urlcomponent'}

    },
    // Returns a collection of items with the items iformation
    // the 'items' parameter is an array of objects {id,type}
    //Almost same code as the reference implementation preloadItems
    preloadItems: function (items, context)
    {
        this.context = this.context || context;

        'use strict';

        var self = this
            ,	items_by_id = {}
            ,	parents_by_id = {};

        items = items || [];

        this.preloadedItems = this.preloadedItems || {};

        items.forEach(function (item)
        {
            if (!item.id || !item.type || item.type === 'Discount')
            {
                return;
            }
            if (!self.preloadedItems[item.id])
            {
                items_by_id[item.id] = {
                    internalid: new String(item.id).toString()
                    ,	itemtype: item.type
                };
            }
        });

        if (!_.size(items_by_id))
        {
            return this.preloadedItems;
        }

        var items_details = this.getItemFieldValues(items_by_id);

        // Generates a map by id for easy access. Notice that for disabled items the array element can be null
        _.each(items_details, function (item)
        {
            if (item && typeof item.itemid !== 'undefined')
            {
                if (item.matrixtype === 'CHILD')
                {
                    parents_by_id[new String(item.parent).toString()] = {
                        internalid: new String(item.parent).toString()
                        ,	itemtype: item.itemtype
                    };
                }

                self.preloadedItems[item.internalid] = item;
            }
        });

        if (_.size(parents_by_id))
        {
            var parents_details = this.getItemFieldValues(parents_by_id);

            _.each(parents_details, function (item)
            {
                if (item && typeof item.itemid !== 'undefined')
                {
                    self.preloadedItems[item.internalid] = item;
                }
            });
        }

        // Adds the parent inforamtion to the child
        _.each(this.preloadedItems, function (item)
        {
            if (item && item.matrixtype === 'CHILD')
            {
                item.matrix_parent = self.preloadedItems[item.parent];
            }
        });

        return this.preloadedItems;
    },

    get: function (id, type, context)
    {
        'use strict';
        this.context = this.context || context;
        this.preloadedItems = this.preloadedItems || {};

        if (!this.preloadedItems[id])
        {
            this.preloadItems([{
                id: id
                ,	type: type
            }], context);
        }
        return this.preloadedItems[id];
    },

    set: function (item)
    {
        'use strict';

        this.preloadedItems = this.preloadedItems || {};

        if (item.internalid)
        {
            this.preloadedItems[item.internalid] = item;
        }
    },

    /* This is where for all items that we grabbed, we post-process information with context and append it to the response */
    getItemFieldValues: function (items_by_id)
    {
        'use strict';

        var self = this;
        var	item_ids = _.values(items_by_id);
        var search = new RecordHelper(this.record, this.fields);
        var results = search.search(_.map(items_by_id, function(item) {
            return {
                id: item.internalid,
                type: self.mapping[item.itemtype]
            };
        })).getResults();

        _.each(results, function(result) {
            _.extend(result,{
                urlcomponent: self._urlcomponent(search.getRecord(result.internalid)),
                price: self._price(search.getRecord(result.internalid)),
                translations: self._texts(search.getRecord(result.internalid)),
                images: self._images(search.getRecord(result.internalid)),
                itemoptions: self._itemOptions(search.getRecord(result.internalid))
            });
        });

        return results;

    },

    //Urlcomponent needs to use the website
    _urlcomponent: function(record)
    {
        var urlcomponent = record.getFieldValue('urlcomponent');
        if(urlcomponent)
        {
            return this.context.domain.fulldomain + urlcomponent
        } else return null;

    },

    /*
    ItemOptions are not easily exposed!
    This is a nasty workaround to get itemoptions and their values, that might be translated!
    We need to load the custitem and custoption, and for that we need field id's. For that we have to use a custom record
    with "getSelectOptions" to cheat to give us scriptid for fields!
     */

    _itemOptions: function(itemRecord)
    {

        var self = this;
        var itemOptions = [];

        if(itemRecord.getFieldValue('matrixtype')==='CHILD')
        {
            var optionsTexts = itemRecord.getFieldTexts('itemoptions'),
                optionsValues = itemRecord.getFieldValues('itemoptions');


            _.each(optionsTexts, function(option, index){

                //BEWARE: Once you do a getSelectOptions, it doesn't seem possible to reuse it, that's why the creation is inside the each.
                var helper = nlapiCreateRecord('customrecord_ef_bs_helper',{recordmode: 'dynamic'}),
                    helperField = helper.getField('custrecord_ef_bs_h_field'),
                    candidates = helperField.getSelectOptions(option, 'is');

                //We're selecting by NAME, so we might have XXX "Name" matches. Now we have to iterate through them, to find the correct one
                _.each(candidates, function(candidate,key){
                    helper.setFieldValue('custrecord_ef_bs_h_field',candidate.id);
                    
                    var candidateScriptId = helper.getFieldValue('custrecord_ef_bs_h_field_scriptid').toLowerCase(),
                        itemOptionScriptId = optionsValues[index].toLowerCase();

                    if( candidateScriptId === itemOptionScriptId)
                    {
                        itemOptions.push({
                            internalid: optionsValues[index].toLowerCase(),
                            label: option,
                            scriptid: candidate.id,
                            index: index
                        });
                    }
                });


            });

            _.each(itemOptions, function(option){
                var itemOptionRecord = nlapiLoadRecord('itemoptioncustomfield',option.scriptid);
                var onlineLanguages = self.context.languages,
                    defaultLanguage = self.context.defaultLanguage,
                    translations = {},
                    languageCount = itemOptionRecord.getLineItemCount('translations'),
                    keysToTranslate = [
                        'label'
                    ],
                    defaultLangKeys = {};

                _.each(keysToTranslate, function (key) {
                    defaultLangKeys[key] = itemOptionRecord.getFieldValue(key)
                });

                translations[defaultLanguage.locale] = defaultLangKeys;

                for (var i = 1; i <= languageCount; i++) {

                    var locale = itemOptionRecord.getLineItemValue('translations', 'locale', i);
                    if (_.findWhere(self.context.languages, {locale: locale})) {
                        var langTranslations = {};
                        _.each(keysToTranslate, function (key) {
                            if (itemOptionRecord.getLineItemValue('translations', key, i) !== null) {
                                langTranslations[key] = itemOptionRecord.getLineItemValue('translations', key, i)
                            }

                        });
                        langTranslations =
                            translations[locale] = _.defaults(langTranslations, defaultLangKeys);
                    }
                }

                var sourceFrom = itemOptionRecord.getFieldValue('sourcefrom').toLowerCase();
                _.extend(option, defaultLanguage,{
                    translations: translations,
                    sourcefrom: sourceFrom,
                    sourcelistid: itemOptionRecord.getFieldValue('sourcefromrecordtype'),
                    value: {
                        internalid:itemRecord.getFieldValue('matrixoption'+sourceFrom),
                        label: itemRecord.getFieldValue('matrixoption'+sourceFrom+'_display')
                    }
                });


                var itemFieldRecord = nlapiLoadRecord('customlist',option.sourcelistid);
                var valueCount = itemFieldRecord.getLineItemCount('customvalue');

                for (var i = 1; i <= valueCount; i++)
                {

                    if(itemFieldRecord.getLineItemValue('customvalue','valueid',i) === option.value.internalid){

                        var extendedValue = {translations:{}};

                        _.extend(extendedValue, {
                            label: itemFieldRecord.getLineItemValue('customvalue','value',i),
                            internalid: itemFieldRecord.getLineItemValue('customvalue','valueid',i),
                            abbr: itemFieldRecord.getLineItemValue('customvalue','abbreviation',i)
                        });
                        _.each(self.context.languages, function(lang){

                            extendedValue.translations[lang.locale] = itemFieldRecord.getLineItemValue('customvalue','value_sname_'+lang.locale,i);
                        });

                        //UGHHHHH!!!!!
                        extendedValue.translations[self.context.defaultLanguage.locale] = itemFieldRecord.getLineItemValue('customvalue','value',i)

                        if(extendedValue.internalid){
                            option.value = extendedValue;
                        }


                    }
                }
                
            });
        }
        return itemOptions;

    },
    //images need their full path
    //TODO: use a resizeid
    _images: function(itemRecord)
    {
        var images = [];
        var imageCount = itemRecord.getLineItemCount('itemimages');


        for( var i=1; i <= imageCount; i++ )
        {
            images.push({
                internalid: itemRecord.getLineItemValue('itemimages', 'nkey', i),
                alt: itemRecord.getLineItemValue('itemimages', 'altTagCaption', i),
                name: itemRecord.getLineItemValue('itemimages', 'name', i),
                url: this.context && this.context.imageurlbase + itemRecord.getLineItemValue('itemimages', 'name', i)
            })
        }
        return images;
    },

    //Texts: we need all translatable fields to become translated in every language that is available on the website
    _texts: function(itemRecord)
    {

        if(this.context && this.context.languages && this.context.defaultLanguage)
        {
            var onlineLanguages = this.context.languages,
                defaultLanguage = this.context.defaultLanguage,
                translations = {},
                languageCount = itemRecord.getLineItemCount('translations'),
                keysToTranslate = [
                    'displayname',
                    'featureddescription',
                    'nopricemessage',
                    'outofstockmessage',
                    'pagetitle',
                    'salesdescription',
                    'storedescription',
                    'storedisplayname',
                    'storedetaileddescription'
                ],
                defaultLangKeys = {};

            _.each(keysToTranslate, function (key) {
                defaultLangKeys[key] = itemRecord.getFieldValue(key)
            });

            translations[defaultLanguage.locale] = defaultLangKeys;

            for (var i = 1; i <= languageCount; i++) {

                var locale = itemRecord.getLineItemValue('translations', 'locale', i);
                if (_.findWhere(this.context.languages, {locale: locale})) {
                    var langTranslations = {};
                    _.each(keysToTranslate, function (key) {
                        if (itemRecord.getLineItemValue('translations', key, i) !== null) {
                            langTranslations[key] = itemRecord.getLineItemValue('translations', key, i)
                        }

                    });
                    langTranslations =
                        translations[locale] = _.defaults(langTranslations, defaultLangKeys);
                }
            }

            return translations;
        }


    },
    /* Price is really tricky to grab, as you can have many scenarios
    The most simple one, is you don't have multicurrency, pricelevels(multiprice) or quantity pricing.
    Then you can have combinations of these features on or off
    In case you have pricelevels, we should grab the one configured as website pricelevel on the website record.
    In case you have quantity pricing, we need to grab the price for the first step (qty 0)
    In case you have multicurrency, we need to grab currencies for all exposed languages

     */
    _price: function(itemRecord){

        var isMultiCurrency = nlapiGetContext().getFeature('MULTICURRENCY'),
            isMultiPrice = nlapiGetContext().getFeature('MULTPRICE'),
            hasQuantityPricing = nlapiGetContext().getFeature('QUANTITYPRICING');


        var prices;
        var pricesByCurrency = {};


        if(!isMultiCurrency && !isMultiPrice && !hasQuantityPricing && this.context && this.context.defaultCurrency)
        {
            //TODO: test this
            prices = {
                price: itemRecord.getFieldValue('rate'),
                price_formatted: formatCurrency(itemRecord.getFieldValue('rate'),this.context.defaultCurrency.symbol,this.context.defaultCurrency.locale_metadata)
            };

        } else {
            if(this.context && this.context.currencies) {

                var context = this.context,
                    currencies = this.context.currencies;

                if (isMultiCurrency) {
                    _.each(currencies, function (currency) {

                        //WE NEED TO SEARCH FOR THE CORRECT LINE OF THE PRICELEVEL, TO PASS IT TO "GETLINEITEMMATRIXVALUE" grrrr
                        var pricelevelCount = itemRecord.getLineItemCount('price' + currency.internalid);
                        var webstorePriceLevelIndex = 1;

                        while (itemRecord.getLineItemValue('price' + currency.internalid, 'pricelevel', webstorePriceLevelIndex) != context.onlinepricelevel) {
                            webstorePriceLevelIndex++;
                        }

                        var tmp = itemRecord.getLineItemMatrixValue('price' + currency.internalid, 'price', webstorePriceLevelIndex, 1); //pricelevel 5, qty 0
                        pricesByCurrency[currency.internalid] = {
                            price: tmp,
                            price_formatted: formatCurrency(tmp, currency.displaysymbol, currency.locale_metadata)
                        };
                    });
                } else {

                    //TODO: test this. Need an acct without multicurrency
                    var pricelevelCount = itemRecord.getLineItemCount('price');
                    var webstorePriceLevelIndex = 1;

                    while (itemRecord.getLineItemValue('price', 'pricelevel', webstorePriceLevelIndex) != context.onlinepricelevel) {
                        webstorePriceLevelIndex++;
                    }

                    var tmp = itemRecord.getLineItemMatrixValue('price', 'price', webstorePriceLevelIndex, 1); //pricelevel 5, qty 0
                    prices = {
                        price: tmp,
                        price_formatted: formatCurrency(tmp, currency.displaysymbol, currency.locale_metadata)
                    };
                }
            }

        }

        if(prices)
        {
            return prices;
        } else
        {
            return pricesByCurrency;
        }


    },

    reset: function()
    {
        delete this.preloadedItems;
    }
});
