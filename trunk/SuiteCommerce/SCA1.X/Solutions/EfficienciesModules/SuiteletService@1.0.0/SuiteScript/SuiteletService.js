Application.on('after:Application.getEnvironment', function(obj,result){
    'use strict';
    var SModel = Application.getModel('SuiteletService');
    result.Efficiencies.SuiteletService.resolvedServices = SModel.resolveServices();

});

Application.defineModel('SuiteletService', {
    resolveServices: function()
    {
        var r = {};
        if(! (request.getURL().indexOf('https:') === 0)) return r;
        _.each(SC.Configuration.Efficiencies.SuiteletService.services, function(service){
            try {
                r[service.script + ',' + service.deploy] = nlapiResolveURL('suitelet', service.script, service.deploy);
            } catch(e)
            {
                nlapiLogExecution('ERROR', e.getCode(), e.getDetails());
                r[service.script + ',' + service.deploy] = null;
            }
        });
        return r;
    }
});
