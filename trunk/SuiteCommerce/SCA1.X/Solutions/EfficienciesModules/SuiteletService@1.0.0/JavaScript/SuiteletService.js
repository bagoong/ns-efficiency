(function ()
{
    'use strict';

    function resolveSuiteletURL(script,deploy) {
        var url = '',
            base = '',
            svcs = SC.ENVIRONMENT.Efficiencies.SuiteletService.resolvedServices,
            key = script + ',' + deploy;


        if (svcs && svcs[key]) {
            base = svcs[script + ',' + deploy];
        }
        else {
            console.error('Misconfigured Suitelet');
        }

        url = base + (~base.indexOf('?') ? '&' : '?') + jQuery.param({
                // Account Number
                c: SC.ENVIRONMENT.companyId
                // Site Number
            ,	n: SC.ENVIRONMENT.siteSettings.siteid
        });

        return url;
    }

    _.extend(SC.Utils, {resolveSuiteletURL: resolveSuiteletURL });
    _.mixin({resolveSuiteletURL: resolveSuiteletURL });

})();