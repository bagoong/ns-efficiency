/* Configuration for Store Locator */
_.extend(SC.Configuration.Efficiencies, {
    OrderStatus: {
        secondField: [
            {
                id: 'shipzip',
                name: 'Ship Zip',
                validation: {
                    pattern:'number'
                }
            },
            {
                id: 'email',
                name: 'Email',
                validation: {
                    pattern: 'email'
                }
            }
        ]
    }
});



