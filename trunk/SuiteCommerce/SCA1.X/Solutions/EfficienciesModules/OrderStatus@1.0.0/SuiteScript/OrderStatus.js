/* global notFoundError:false, Application:false */
Application.defineModel('OrderStatus', {
    record: 'salesorder',
    filters: [
        {fieldName: 'mainline', join: null, operator: 'is', value1: 'T'}
    ],
    columns: {
        orderid: {fieldName: 'tranid'},
        status: {fieldName: 'status', type: 'text'},
        trackingnumbers: {fieldName: 'trackingnumbers'},
        shipmethod: {fieldName: 'shipmethod', type: 'text'}
    },
    get: function (data)
    {
        var orderId = data.orderid,
            secondField = data.secondField,
            secondFieldValue = data[secondField],
            secondFieldConfig,
            result;

        if (!orderId || (!secondFieldValue))
        {
            throw notFoundError;
        }

        secondFieldConfig = _.findWhere(SC.Configuration.Efficiencies.OrderStatus.secondField, {id:secondField});

        if (!secondFieldConfig)
        {
            throw notFoundError;
        }

        this.columns[secondField] = { fieldName: secondFieldConfig.id };
        this.filters.push({fieldName: this.columns.orderid.fieldName, operator: 'is', value1: orderId });
        this.filters.push({fieldName: secondField, operator: 'is', value1: secondFieldValue});

        result = Application.searchProxy(this.record, this.filters, this.columns);
        if (!result || result.length !== 1)
        {
            throw notFoundError;
        }

        //For form purposes.
        result[0].secondField = secondField;

        return result[0];
    }
});