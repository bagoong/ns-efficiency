define('OrderStatus.Model', [], function ()
{
    'use strict';

    return Backbone.Model.extend({
        urlRoot: _.getAbsoluteUrl('services/orderstatus.ss'),
        validation: {
            orderid: { required: true, msg: _('Order Id is required').translate() }
        },

        initialize: _.wrap(Backbone.Model.prototype.initialize, function(fn,data,options)
        {
            this.application = options.application;
            this.setupValidation();
            return fn.apply(this, _.toArray(arguments).slice(1));
        }),

        setupValidation: function()
        {
            var config = this.application.getConfig('OrderStatus.secondField'),
                self = this,
                validator;

            _.each(config, function(value){
                validator = {required: self.validationFn};
                _.extend(validator, value.validation);
                self.validation[value.id] = validator;
            });
        },

        validationFn:function(value, attr, values)
        {
            if(!values.secondField || !values[values.secondField])
            {
                return _('Required').translate();
            }
        }
    });
});