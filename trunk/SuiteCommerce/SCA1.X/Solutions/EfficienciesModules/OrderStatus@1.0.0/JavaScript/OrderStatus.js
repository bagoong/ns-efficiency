define('OrderStatus', [
    'OrderStatus.Model',
    'OrderStatus.View',
    'OrderStatus.Router'
], function (Model, View, Router)
{
    'use strict';

    return {
        Model: Model,
        Router: Router,
        View: View,
        mountToApp: function (application)
        {
            _.extend(application.Configuration, {
                OrderStatus: SC.ENVIRONMENT.Efficiencies.OrderStatus
            });
            return new Router(application);
        }
    };

});