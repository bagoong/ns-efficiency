define('OrderStatus.Router', ['OrderStatus.View'], function (View)
{
    'use strict';

    return Backbone.Router.extend({
        routes: {
            'orderstatus': 'orderStatus'
        },

        initialize: function (application)
        {
            this.application = application;
        },

        orderStatus: function ()
        {
            new View({application: this.application}).showContent();
        }
    });
});