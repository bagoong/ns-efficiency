var Handler = (function(){

    var dups;

    /* Private */
    var onCreate = function(newRecord)
    {

        var context = nlapiGetContext(),
            executionContext = context.getExecutionContext();

        //Prevent executing the script if not on WEBSTORE && SCA with SSP_Lib that says "enable this"
        if(executionContext != 'webstore' || (!(context.getSessionObject('EF_DCM_UE_RUN') === 'T')))
            return true;

        //If module is enabled by config
        if(SC.Configuration.Efficiencies.DuplicateCustomerManagement.enabled){


            //If we are on webstore, or it was specified that this should run not only on webstore but everywhere
            var DuplicateCustomerManagement = Application.getModel('DuplicateCustomerManagement.Customer');

            var registrationData = {};
            /*JSON.parse(JSON.stringify(newRecord) has all the info that netsuite defaulted for the customer registration
            Includes, still in JSON, not netsuite format (see booleans), fields like campaigns, giveaccess, credit hold override
            And all the properties that get defaulted when creating a customer
            */

            _.each(JSON.parse(JSON.stringify(newRecord)), function (value, key) {
                var tempValue = value.internalid || value;
                if (_.isBoolean(tempValue)) {
                    tempValue = (tempValue === true ? 'T' : 'F'); //For some reason, here we had true booleans. And we need netsuite bools.
                }
                registrationData[key] = tempValue;

            });

            //Guest Shoppers cannot be checked against duplicates, when registered they usually have no e-mail
            //Paypal Express shoppers arrive to the checkout with email, but no access, and they also shouldn't be tested against duplicates.
            //Conversion of guests to people with access has a NATIVE duplication check. GO FIGURE!
            if(!registrationData.email || registrationData.giveaccess === 'F') return;

            dups = DuplicateCustomerManagement.getDuplicated(registrationData);
            if (dups && dups.length) {
                throw nlapiCreateError('-100', SC.Configuration.Efficiencies.DuplicateCustomerManagement.errorMessage, true);
            }

        }

    };

    /* Public */
    var beforeSubmit  = function(type){

        var record = nlapiGetNewRecord();

        switch(type.toString()){
            case 'create':
                onCreate(record);
                break;
        }
    };

    return {
        beforeSubmit: beforeSubmit
    };

}());