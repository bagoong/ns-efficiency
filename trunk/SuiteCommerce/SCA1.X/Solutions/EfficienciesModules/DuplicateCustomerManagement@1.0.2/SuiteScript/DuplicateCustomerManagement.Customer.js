Application.defineModel('DuplicateCustomerManagement.Customer', {
    record: 'customer',
    columns: {
        internalid: {fieldName: 'internalid'},
        giveaccess: {fieldName: 'giveaccess'}
    },
    /*
     data: user to register data
     ignoreKeys: (optional) to avoid in specific case

     SC.Configuration.Efficiencies.DuplicateCustomerManagement.duplicate_criteria
     */
    getDuplicated: function (data, ignoreKeys) {

        var context = nlapiGetContext();
        //Protect us from getting nasty undefined errors
        if(SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.subsidiary && !context.getFeature('SUBSIDIARIES')){
            delete SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.subsidiary;
        }

        //Protect us from getting nasty undefined errors
        if(SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.language && !context.getFeature('MULTILANGUAGE')){
            delete SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.language;
        }

        //Protect us from getting nasty undefined column errors
        if(SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.currency && context.getFeature('MULTICURRENCY')){
            delete SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria.currency;
        }

        //add all criteria columns - just for debug purposes.
        //if search column and search field don't match for a field, this can be a problem
        var columns = _.clone(this.columns);
        _.each(SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria, function(value,key){
            columns[key] = {fieldName: key};
        });


        var Search = new SearchHelper(this.record, null, columns),
            self = this;

        _.each(SC.Configuration.Efficiencies.DuplicateCustomerManagement.criteria, function (value, key) {
            if (!_.contains(ignoreKeys, key)) {

                //Check for the SAME value as the new registered user
                if(value.type === 'same'){
                    Search.addFilter({fieldName: key, operator: 'is', value1: data[key]});
                }

                //Check for the value specified on the configuration
                if(value.type === 'value'){
                    Search.addFilter({fieldName: key, operator: 'is', value1: value.value});
                }
            }
        });

        //Duplicate email registration by default MUST check for giveaccess.
        //But we can use this module as a dependency for others, and then perhaps we don't need to check for dups.
        if(ignoreKeys && !_.contains(ignoreKeys,'giveaccess'))
        {
            Search.addFilter({fieldName: self.columns.giveaccess.fieldName, operator: 'is', value1: 'T'});
        }

        Search.setSort('createdat').setSortOrder('asc');

        return Search.search().getResults();

    }
});