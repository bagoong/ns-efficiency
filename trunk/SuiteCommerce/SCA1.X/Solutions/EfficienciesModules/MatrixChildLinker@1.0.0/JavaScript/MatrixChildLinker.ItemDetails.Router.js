define('MatrixChildLinker.ItemDetails.Router', ['ItemDetails.Router'], function(Router){
    _.extend(Router.prototype, {
        productDetails: function (api_query, base_url, options)
        {
            // Decodes url options
            _.each(options, function (value, name)
            {
                options[name] = decodeURIComponent(value);
            });

            var application = this.application
                ,	model = new this.Model()
            // we create a new instance of the ProductDetailed View
                ,	view = new this.View({
                    model: model
                    ,	baseUrl: base_url
                    ,	application: this.application
                });

            model.fetch({
                data: api_query
                ,	killerId: this.application.killerId
            }).then(
                // Success function
                function ()
                {
                    if (!model.isNew())
                    {
                        if (api_query.id && model.get('urlcomponent') && SC.ENVIRONMENT.jsEnvironment === 'server')
                        {
                            nsglobal.statusCode = 301;
                            nsglobal.location = model.get('_url');
                        }
                        /* START CUSTOMIZATIONS FOR SUPPORTING MATRIX ?child=ID url */

                        //If "Child parameter (internalid of a matrix child) is found
                        if(options && options.child)
                        {
                            //First we configure the model so it becomes parent + options setted
                            var found = model.parseQueryStringWithChilds(options),
                                queryString,
                                url;

                            if (found)
                            {
                                //Then we get the parameters for the options, to form an url parent + options
                                queryString = _.parseUrlOptions(model.getQueryString());
                                if(queryString.quantity ==="1")
                                {
                                    //removing qty mostly for SEO
                                    delete queryString.quantity;
                                }
                                url = model.get('_url')+ '?' + jQuery.param(queryString);

                                //And we redirect the user to that parent+options url
                                if(!SC.ENVIRONMENT.jsEnvironment === 'server')
                                {
                                    //Replace, it's just a cosmetic change, no state changes
                                    Backbone.history.navigate(url, {replace: true});
                                }

                            }
                            else
                            {
                                //Wrong child id, throw 404
                                application.getLayout().notFound();
                                return;
                            }

                        }

                        /* END CUSTOMIZATIONS FOR SUPPORTING MATRIX ?child=ID */

                        // once the item is fully loadede we set its options
                        model.parseQueryStringOptions(options);

                        if (!(options && options.quantity))
                        {
                            model.set('quantity', model.get('_minimumQuantity'));
                        }

                        // we first prepare the view
                        view.prepView();

                        // then we show the content
                        view.showContent();
                    }
                    else
                    {
                        // We just show the 404 page
                        application.getLayout().notFound();
                    }
                }
                // Error function
                ,	function (model, jqXhr)
                {
                    // this will stop the ErrorManagment module to process this error
                    // as we are taking care of it
                    jqXhr.preventDefault = true;

                    // We just show the 404 page
                    application.getLayout().notFound();
                }
            );
        }
    });
    return Router;
});
