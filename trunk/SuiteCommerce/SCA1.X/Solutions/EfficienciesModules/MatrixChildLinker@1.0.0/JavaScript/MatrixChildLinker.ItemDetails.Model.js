define('MatrixChildLinker.ItemDetails.Model', ['ItemDetails.Model'], function(Model){
    'use strict';


    _.extend(Model.prototype, {

        /* parseQueryStringWithChilds:
        Looks up for the parameter child in the option (that should be the internal id of the matrix child
        Searches on the parent item for the child, and sets the correct options (as if the user would have clicked them)
        So we end up with the selected child ready.

        Returns true if the child id exists
         */
        parseQueryStringWithChilds: function(options)
        {

            var childId = parseInt(options.child,10),
                self = this;
            delete options.child;

            this.parseQueryStringOptions(options);


            var child = this.get('_matrixChilds').findWhere({internalid:childId});
            if(!child ) return false;

            var fields = this.get('_optionsDetails') && this.get('_optionsDetails').fields;
            fields = _.where(fields,{ismatrixdimension: true});

            var optionsValues = {};
            _.each(fields, function(option){
                optionsValues[option.internalid] = child.get(option.sourcefrom);
            });

            _.each(optionsValues, function(value,name){
                var option = self.getPosibleOptionByUrl(name);
                if (option)
                {
                    if (option.values)
                    {
                        // We check for both Label and internal id because we detected that sometimes they return one or the other
                        value = _.where(option.values, {label: value})[0] || _.where(option.values, {internalid: value})[0];
                        self.setOption(option.cartOptionId, value);
                    }
                    else
                    {
                        self.setOption(option.cartOptionId, value);
                    }
                }
            });
            return true;
        }
    });
});