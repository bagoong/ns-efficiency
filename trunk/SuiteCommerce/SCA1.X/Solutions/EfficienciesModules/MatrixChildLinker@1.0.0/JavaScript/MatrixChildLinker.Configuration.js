/**
 This file is provided just for reference.
 Add the content to your Configuration file.
 */
(function (application) {

    'use strict';
    application.Configuration.modules.push('MatrixChildLinker');

}(SC.Application('Shopping')));