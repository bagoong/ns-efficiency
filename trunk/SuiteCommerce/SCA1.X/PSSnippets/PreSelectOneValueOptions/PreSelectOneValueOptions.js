define('PreSelectOneValueOptions',['ItemDetails.View'],function(View){

    _.extend(View.prototype,{
        render: _.wrap(View.prototype.render,function(fn){

            var self = this;

            _.each(this.model.getPosibleOptions(),function(option){

                if(_.size(option.values) === 2 && option.values[0].label === '- Select -'){

                    var cart_option_id = option.cartOptionId,
                        value = option.values[1].internalid;

                    try
                    {
                        self.model.setOption(cart_option_id, value);
                    }
                    catch (error)
                    {
                        // Clears all matrix options
                        _.each(self.model.getPosibleOptions(), function (option)
                        {
                            option.isMatrixDimension && self.model.setOption(option.cartOptionId, null);
                        });
                        // Sets the value once again
                        self.model.setOption(cart_option_id, value);
                    }
                }

            });
            fn.apply(this, _.toArray(arguments).slice(1));
        })
    });
});