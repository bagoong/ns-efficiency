define('Images',function(){

    function itemImageFlatten (images)
    {
        if ('url' in images && 'altimagetext' in images)
        {
            return [images];
        }

        return _.flatten(_.map(images, function (item)
        {
            if (_.isArray(item))
            {
                return item;
            }

            return itemImageFlatten(item);
        }));
    }

    return {

        mountToApp: function(application){


            _.extend(application.getConfig().itemKeyMapping, {

                _images: function (item) {
                    var result = []
                        , selected_options = item.itemOptions
                        , item_images_detail = item.get('itemimages_detail') || {}
                        , swatchLabel
                        , keys
                        , primarySwatch = selected_options && selected_options[application.getConfig('multiImageOption')] || null
                        , secondarySwatch = selected_options && selected_options[application.getConfig('secondaryMultiImageOption')] || null;

                    item_images_detail = item_images_detail.media || item_images_detail;


                    if(primarySwatch && secondarySwatch){
                        swatchLabel = primarySwatch.label + '|' + secondarySwatch.label;
                        if(item_images_detail[swatchLabel]) {
                            result = itemImageFlatten(item_images_detail[swatchLabel]);
                        } else {
                            result = itemImageFlatten(item_images_detail);
                        }
                    }else if(primarySwatch && !secondarySwatch){
                        _.each(item_images_detail,function(val,key){
                            keys = key.split('|');
                            swatchLabel = keys[0];
                            if(primarySwatch.label == swatchLabel) {
                                result = _.union(itemImageFlatten(val),result);
                            }
                        });
                        if(!result.length){
                            result = itemImageFlatten(item_images_detail);
                        }
                    }else if(!primarySwatch && secondarySwatch){
                        _.each(item_images_detail,function(val,key){
                            keys = key.split('|');
                            swatchLabel = keys.length == 2 ? keys[1] : key;
                            if(secondarySwatch.label == swatchLabel) {
                                result = _.union(itemImageFlatten(val),result);
                            }
                        });
                        if(!result.length){
                            result = itemImageFlatten(item_images_detail);
                        }
                    }
                    else {
                        result = itemImageFlatten(item_images_detail);
                    }


                    if (result.length) {
                        result = _.sortBy(result, function (i) {
                            var option = i.url.split('-'),
                                order = parseInt(option[option.length - 1]);
                            return isNaN(order) ? 1000 : parseInt(order);
                        });
                    }

                    return result.length ? result : [{
                        url: item.get('storedisplayimage') || application.Configuration.imageNotAvailable
                        , altimagetext: item.get('_name')
                    }];
                },
                _thumbnail: function (item) {

                    var item_images_detail = item.get('itemimages_detail') || {}
                        , item_images_detail_parent = item.get('_matrixParent').get('itemimages_detail') || {}
                        , selected_options = item.itemOptions
                        , swatchLabel
                        , keys
                        , primarySwatch = selected_options && selected_options[application.getConfig('multiImageOption')] || null
                        , secondarySwatch = selected_options && selected_options[application.getConfig('secondaryMultiImageOption')] || null;

                    if (_.size(item_images_detail_parent)) {
                        item_images_detail = item_images_detail_parent;
                    }
                    var sImage;


                    if(primarySwatch && secondarySwatch){
                        swatchLabel = primarySwatch.label + '|' + secondarySwatch.label;
                        var retImages = item_images_detail[swatchLabel] && itemImageFlatten(item_images_detail[swatchLabel]);

                        if(retImages && retImages[0]){
                            return retImages[0];
                        }
                    }

                    var currentView = application.getLayout().currentView;

                    if(currentView) {
                        var translator = currentView.translator;
                        if (translator && translator.facets.length) {

                            primarySwatch = application.getConfig('multiImageOptionFacet');
                            secondarySwatch = application.getConfig('secondaryMultiImageOptionFacet');


                            var selected_primary_facet = _.findWhere(translator.facets, {id: primarySwatch});
                            var selected_secondary_facet = _.findWhere(translator.facets, {id: secondarySwatch});

                            if(selected_primary_facet && selected_secondary_facet){
                                swatchLabel = selected_primary_facet.value + '|' + selected_secondary_facet.value;
                                if(item_images_detail[swatchLabel]){
                                    sImage = itemImageFlatten(item_images_detail[swatchLabel]);
                                }

                            }else if(selected_primary_facet && !selected_secondary_facet){
                                _.each(item_images_detail,function(val,key){
                                    keys = key.split('|');
                                    swatchLabel = keys[0];
                                    if(selected_primary_facet.value == swatchLabel) {
                                        sImage = _.union(itemImageFlatten(val),sImage);
                                    }
                                });

                            }else if(!selected_primary_facet && selected_secondary_facet){
                                _.each(item_images_detail,function(val,key){
                                    keys = key.split('|');
                                    swatchLabel = keys.length == 2 ? keys[1] : key;
                                    if(selected_secondary_facet.value == swatchLabel) {
                                        sImage = _.union(itemImageFlatten(val),sImage);
                                    }
                                });
                            }

                            if(sImage && sImage[0]){
                                return sImage[0];
                            }
                        }
                    }

                    item_images_detail = item.get('_images') || {};

                    // If you generate a thumbnail position in the itemimages_detail it will be used
                    if (item_images_detail.thumbnail) {
                        return item_images_detail.thumbnail;
                    }

                    // otherwise it will try to use the storedisplaythumbnail
                    if (SC.ENVIRONMENT.siteType && SC.ENVIRONMENT.siteType === 'STANDARD' && item.get('storedisplaythumbnail')) {
                        return {
                            url: item.get('storedisplaythumbnail')
                            , altimagetext: item.get('_name')
                        };
                    }
                    // No images huh? carry on

                    var parent_item = item.get('_matrixParent');
                    // If the item is a matrix child, it will return the thumbnail of the parent
                    if (parent_item && parent_item.get('internalid')) {
                        return parent_item.get('_thumbnail');
                    }

                    var images = itemImageFlatten(item_images_detail);
                    // If you using the advance images features it will grab the 1st one
                    if (images.length) {
                        return images[0];
                    }

                    // still nothing? image the not available
                    return {
                        url: application.Configuration.imageNotAvailable
                        , altimagetext: item.get('_name')
                    };
                }
            });
        }

    }
});