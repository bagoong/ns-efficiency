define('Images',function(){

    function itemImageFlatten (images)
    {
        if ('url' in images && 'altimagetext' in images)
        {
            return [images];
        }

        return _.flatten(_.map(images, function (item)
        {
            if (_.isArray(item))
            {
                return item;
            }

            return itemImageFlatten(item);
        }));
    }


    return {

        mountToApp: function(application){


            _.extend(application.getConfig().itemKeyMapping, {

                _images: function (item) {
                    var result = []
                        , otherResults = []
                        , selected_options = item.itemOptions
                        , item_images_detail = item.get('itemimages_detail') || {}
                        , multiImageOption = application.getConfig('multiImageOption')
                        , itemOptions = _.map(item.getPosibleOptions(), function(o){return o.cartOptionId;})
                        , validOptions = _.intersection(multiImageOption,itemOptions)
                        , optionsSelected = [];

                    item_images_detail = item_images_detail.media || item_images_detail;


                    _.each(validOptions,function(option){
                        var selected = selected_options && selected_options[option];
                        if(selected){
                            optionsSelected.push(option+':'+selected.label);
                        }
                    });


                    if(optionsSelected.length){
                        _.each(item_images_detail,function(image,key){

                            var parsedKey = _.map(key.split("|"), function(key,index){
                                return validOptions[index]+':'+key;
                            });

                            var intersection = _.intersection(parsedKey,optionsSelected);


                            if(intersection.length == optionsSelected.length){
                                result.push(itemImageFlatten(image));
                            }
                            else if(intersection.length && intersection[0].split(':')[0] == validOptions[0]){
                                otherResults.push(itemImageFlatten(image));
                            }
                        });

                        result = _.flatten(result);
                        otherResults = _.flatten(otherResults);

                        if(!result.length){
                            result = otherResults;
                        }

                        result = _.uniq(result);

                    }

                    if(!result.length && item_images_detail['Main'] && application.getConfig('showOnlyMainImages')){
                        result = itemImageFlatten(item_images_detail['Main']);
                    }

                    if(!result.length){
                        result = itemImageFlatten(item_images_detail);
                    }

                    if (result.length) {
                        result = _.sortBy(result, function (i) {
                            var option = i.url.split('-'),
                                order = parseInt(option[option.length - 1]);

                            return isNaN(order) ? 1000 : parseInt(order);
                        });
                    }

                    return result.length ? result : [{
                        url: item.get('storedisplayimage') || application.Configuration.imageNotAvailable
                        , altimagetext: item.get('_name')
                    }];
                },
                _thumbnail: function (item) {

                    var item_images_detail = item.get('itemimages_detail') || {}
                        , item_images_detail_parent = item.get('_matrixParent').get('itemimages_detail') || {};

                    if (_.size(item_images_detail_parent)) {
                        item_images_detail = item_images_detail_parent;
                    }
                    var sImage = [],sImageOther = [];


                    var currentView = application.getLayout().currentView;

                    if(currentView) {
                        var translator = currentView.translator;
                        if (translator && translator.facets.length) {

                            var multiImageOption = application.getConfig('multiImageOption')
                                , validOptions = _.compact(_.map(item.getPosibleOptions(), function(o){ return _.indexOf(multiImageOption,o.cartOptionId) > -1 ? o.itemOptionId: null}))
                                , optionsSelected = [];


                            _.each(validOptions,function(option){
                                var selected = _.findWhere(translator.facets, {id: option});
                                if(selected){
                                    optionsSelected.push(option+':'+selected.value);
                                }
                            });


                            if(optionsSelected.length){
                                _.each(item_images_detail,function(image,key){

                                    var parsedKey = _.map(key.split("|"), function(key,index){
                                        return validOptions[index]+':'+key;
                                    });

                                    var intersection = _.intersection(parsedKey,optionsSelected);


                                    if(intersection.length == optionsSelected.length){
                                        sImage.push(itemImageFlatten(image));
                                    }
                                    else if(intersection.length && intersection[0].split(':')[0] == validOptions[0]){
                                        sImageOther.push(itemImageFlatten(image));
                                    }
                                });

                                sImage = _.flatten(sImage);
                                sImageOther = _.flatten(sImageOther);


                                if(!sImage.length){
                                    sImage = sImageOther;
                                }

                                sImage = _.uniq(sImage);

                                if(sImage && sImage[0]){
                                    return sImage[0];
                                }

                            }
                        }
                    }

                    item_images_detail = item.get('_images') || {};

                    // If you generate a thumbnail position in the itemimages_detail it will be used
                    if (item_images_detail.thumbnail) {
                        return item_images_detail.thumbnail;
                    }

                    // otherwise it will try to use the storedisplaythumbnail
                    if (SC.ENVIRONMENT.siteType && SC.ENVIRONMENT.siteType === 'STANDARD' && item.get('storedisplaythumbnail')) {
                        return {
                            url: item.get('storedisplaythumbnail')
                            , altimagetext: item.get('_name')
                        };
                    }
                    // No images huh? carry on

                    var parent_item = item.get('_matrixParent');
                    // If the item is a matrix child, it will return the thumbnail of the parent
                    if (parent_item && parent_item.get('internalid')) {
                        return parent_item.get('_thumbnail');
                    }

                    var images = itemImageFlatten(item_images_detail);
                    // If you using the advance images features it will grab the 1st one
                    if (images.length) {
                        return images[0];
                    }

                    // still nothing? image the not available
                    return {
                        url: application.Configuration.imageNotAvailable
                        , altimagetext: item.get('_name')
                    };
                }
            });
        }

    }
});