define('InfiniteItems',['Facets.Views'],function(Views){

    var FacetBrowse = Views.Browse.prototype,
        $lastItem = null,
        $firstItem = null,
        nextPage,
        prevPage,
        originalPage,
        infiniteConfig;

    _.extend(FacetBrowse, {
        isLoading: false,
        event: _.extend(FacetBrowse.events,{
            'click [data-toggle="infinite-next-page"]':'loadNextPage',
            'click [data-toggle="infinite-prev-page"]':'loadPrevPage'
        }),

        updateUrl: function(){
            var $dividers = this.$('.infinite-divider'),
                page = null;

            _.each($dividers,function(d){
                var $d = jQuery(d);
                if(isElementPartiallyInViewport($d) && !page){
                    page = $d.data('page');
                }
            });
            if(page){
                Backbone.history.navigate(this.translator.cloneForOption('page', page).getUrl(),{trigger:false,replace:true});
            }
        },
        updateItems: function(){
            $lastItem = jQuery(_.last(this.application.getLayout().$('.item-cell')));
            $firstItem = jQuery(_.first(this.application.getLayout().$('.item-cell')));
            this.isLoading = false;
        },
        loadPrevPage: function(){
            var newPage = prevPage;

            newPage--;

            if(newPage >= 0){

                if(newPage == 1){
                    this.$('.infinite-prev').remove();
                }

                prevPage = newPage;

                this.$('.infinite-loading').show();

                this.renderPage(false,newPage);

            }
        },
        loadNextPage: function(){

            var newPage = nextPage;

            newPage++;

            if(newPage <= this.totalPages){

                if(newPage == this.totalPages){
                    this.$('.infinite-next').remove();
                }

                nextPage = newPage;

                this.$('.infinite-loading-next').show();

                this.renderPage(true,newPage);

            }

        },
        renderPage: function(next,page){

            var self = this,
                displayOption = _.find(self.application.getConfig('itemsDisplayOptions'), function (option)
                {
                    return option.id === self.translator.getOptionValue('display');
                })
                ,	cellWrap = function cellWrap (item)
                {
                    return SC.macros[displayOption.macro](item, self);
                };

            this.translator.options.page = page;

            this.model.fetch({
                data: this.translator.getApiParams(),
                killerId: this.application.killerId,
                success: function() {
                    var newHtml = '<div class="infinite-divider" data-page="'+page+'">'+SC.macros.displayInRows(self.model.get('items').models, cellWrap, displayOption.columns)+'</div>';

                    if(next){
                        jQuery(newHtml).insertAfter($lastItem.closest('.infinite-divider'));
                    }else{
                        jQuery(newHtml).insertBefore($firstItem.closest('.infinite-divider'));
                        self.updateUrl();
                    }

                    self.updateItems();

                    self.$('.infinite-loading-next').hide();
                }
            });
        }
    });

    return {
        mountToApp: function(application){

            var Layout = application.getLayout(),
                self = this;

            infiniteConfig = application.getConfig('infiniteConfig');

            Layout.on('afterAppendView',function(view){
                if(view instanceof Views.Browse){
                    view.updateItems();

                    originalPage = prevPage = nextPage = view.translator.getOptionValue('page');

                    view.$('.infinite-divider').attr('data-page',originalPage);

                    jQuery(window).off('scroll');
                    jQuery(window).on('scroll',function(){

                        if(infiniteConfig.loadItemsAutomatic && jQuery(window).width() > 767){

                            if(isElementInViewport($lastItem)){
                                if(!view.isLoading){
                                    view.isLoading = true;
                                    view.loadNextPage();
                                }
                            }
                        }

                        view.updateUrl();

                    })
                }else{
                    jQuery(window).off('scroll');
                }


            });
        }
    }

    function isElementPartiallyInViewport(el)
    {
        if (typeof jQuery !== 'undefined' && el instanceof jQuery) el = el[0];

        var rect = el.getBoundingClientRect(),
            windowHeight = (window.innerHeight || document.documentElement.clientHeight),
            windowWidth = (window.innerWidth || document.documentElement.clientWidth),
            vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0),
            horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

        return (vertInView && horInView);
    }


    function isElementInViewport (el)
    {
        if (typeof jQuery !== 'undefined' && el instanceof jQuery) el = el[0];

        var rect = el.getBoundingClientRect(),
            windowHeight = (window.innerHeight || document.documentElement.clientHeight),
            windowWidth = (window.innerWidth || document.documentElement.clientWidth);

        return (
        (rect.left >= 0)
        && (rect.top >= 0)
        && ((rect.left + rect.width) <= windowWidth)
        && ((rect.top + rect.height) <= windowHeight)
        );

    }
    //
})