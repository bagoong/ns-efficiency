Author: mprieto@netsuite.com
For SCA 1
Help: https://confluence.corp.netsuite.com/display/SUIT/SCA+Infinite+Scroll+Module
//////////////////////////
You also need to add:
Less:

.infinite-loading-next{
    text-align: center;
    font-size: 18px;
    padding: 20px;
    width: 100%;
    color: #000000;
    display: none;
    img {
        margin-right: 10px;
    }
}

//////////////////////////
And include on config file:

infiniteConfig = {
    loadItemsAutomatic: true,
    showPreviousPageButton: true
};

//////////////////////////
Features:
Allow to load next page automatically or with a button (“Show More”)
You can choose if you want to show a button to load previous page
When you scroll the page updates the url to be SEO friendly   - http://googlewebmastercentral.blogspot.ca/2014/02/infinite-scroll-search-friendly.html?m=1
On mobile only works with the Show More button for usability. If a site has a lot of pages you will not able to reach the footer.