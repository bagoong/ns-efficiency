define('ItemDetailsRouter500Error',['ItemDetails.Router'],function(Router){

    _.extend(Router.prototype,{

        productDetails: function (api_query, base_url, options)
        {
            // Decodes url options
            _.each(options, function (value, name)
            {
                options[name] = decodeURIComponent(value);
            });

            var application = this.application
                ,	model = new this.Model()
            // we create a new instance of the ProductDetailed View
                ,	view = new this.View({
                    model: model
                    ,	baseUrl: base_url
                    ,	application: this.application
                });

            model.fetch({
                data: api_query
                ,	killerId: this.application.killerId
            }).then(
                // Success function
                function ()
                {
                    if (!model.isNew())
                    {
                        if (api_query.id && model.get('urlcomponent') && SC.ENVIRONMENT.jsEnvironment === 'server')
                        {
                            nsglobal.statusCode = 301;
                            nsglobal.location = model.get('_url');
                        }

                        // once the item is fully loadede we set its options
                        model.parseQueryStringOptions(options);

                        if (!(options && options.quantity))
                        {
                            model.set('quantity', model.get('_minimumQuantity'));
                        }

                        // we first prepare the view
                        view.prepView();

                        // then we show the content
                        view.showContent();
                    }
                    else
                    {
                        // We just show the 404 page
                        application.getLayout().notFound();
                    }
                }
                // Error function
                ,	function (jqXhr)
                {
                    // this will stop the ErrorManagment module to process this error
                    // as we are taking care of it
                    jqXhr.preventDefault = true;

                    // We just show the 404 page
                    application.getLayout().notFound();
                }
            );
        }
    })
})