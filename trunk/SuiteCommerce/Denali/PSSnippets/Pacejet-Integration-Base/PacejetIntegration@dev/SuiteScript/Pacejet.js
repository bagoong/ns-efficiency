define(
'Pacejet',
['Application','Pacejet.Model','underscore','Utils'],
function(Application, PacejetModel, _, Utils) {

    var mapPacejetPrices = function(shippingMethods) {
        var result = PacejetModel.get();
        if (result.success) {
            _.each(result.response, function(pacejetShip) {
                var shippingMethod = _.findWhere(shippingMethods, {name: pacejetShip.netsuiteName});
                if (shippingMethod) {
                    shippingMethod.rate = pacejetShip.recipientRate;
                    shippingMethod.rate_formatted = Utils.formatCurrency(pacejetShip.recipientRate);
                    shippingMethod.is_pacejet = true;
                }
            });
        }
        else {
            //TODO: Throw error in calculations
        }
    };

    var isUpdating = false;

        // @class LiveOrder.Model
    Application.on('after:LiveOrder.get', function(Model, response) {
        mapPacejetPrices(response.shipmethods);

        var shipMethod = _.find(response.shipmethods, function(ship) { return ship.internalid == response.shipmethod});

        if (shipMethod && shipMethod.is_pacejet &&  parseFloat(shipMethod.rate) != (response.summary && response.summary.shippingcost)
        && !isUpdating) {
            isUpdating = true;
            order.setCustomFieldValues({custbody_ns_ps_shipping_price: shipMethod.rate });
            response = Model.get();
            isUpdating = false;
        }

        return response;
    });

    Application.on('after:LiveOrder.update', function(Model, data, response) {

        var shipMethod = _.find(response.shipmethods, function(ship) { return ship.internalid == response.shipmethod});
        if (shipMethod && shipMethod.is_pacejet) {
            order.setCustomFieldValues({custbody_ns_ps_shipping_price: shipMethod.rate });
        }
        else {
            order.setCustomFieldValues({custbody_ns_ps_shipping_price: '0.00' });
        }
    });

});