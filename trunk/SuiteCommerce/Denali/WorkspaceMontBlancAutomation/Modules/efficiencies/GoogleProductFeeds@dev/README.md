# Google Feed Generator
Generates the product feeds required for Google Shopping and Google Local in the correct formats, ready to be added into Google Merchant Centre. Google Local Product Feed, Google Local Online Product Inventory Update Feed and Google Shopping Products Feed are supported.

## Documentation
https://confluence.corp.netsuite.com/x/ykL3Ag
