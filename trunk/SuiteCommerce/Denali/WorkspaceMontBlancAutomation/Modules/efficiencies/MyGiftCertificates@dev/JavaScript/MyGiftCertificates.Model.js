define('MyGiftCertificates.Model', [
    'Backbone',
    'underscore'
], function GiftCertsModel(
    Backbone,
    _
) {
    'use strict';

    return Backbone.Model.extend({
        urlRoot: _.getAbsoluteUrl('services/MyGiftCertificates.Service.ss')
    });
});