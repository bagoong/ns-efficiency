define('MyGiftCertificates.List.View', [
    'Backbone',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'MyGiftCertificates.Details.View',
    'giftcert_list.tpl'
], function GiftCertsListView(
    Backbone,
    CollectionView,
    CompositeView,
    GiftCertDetailsView,
    giftcertListTpl
) {
    'use strict';

    return Backbone.View.extend({
        initialize: function initGiftCertView(options) {
            CompositeView.add(this);
            this.application = options.application;
            this.collection = options.collection;
        },

        childViews: {
            'MyGiftCertificates.Collection': function giftCertChildViews() {
                return new CollectionView({
                    'childView': GiftCertDetailsView,
                    'collection': this.collection,
                    'viewsPerRow': 1
                });
            }
        },

        getBreadcrumbPages: function giftCertBreadCrumb() {
            return [{text: 'Gift Certificates', href: '/giftcertificates'}];
        },

        getSelectedMenu: function expandNav() {
            return 'giftcertificates';
        },
        template: giftcertListTpl
    });
});