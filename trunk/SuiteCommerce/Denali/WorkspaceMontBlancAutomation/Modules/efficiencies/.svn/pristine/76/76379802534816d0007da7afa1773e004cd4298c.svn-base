/*
    Configuration - Load Product Comparison internal_id <> Label Name mapping
*/
define('ProductComparison.Configuration', [
    'Configuration',
    'underscore'
], function ProductComparisonConfiguration(
    GlobalConfiguration,
    _
) {
    'use strict';

    var ProductComparisonConfig = {
        itemProperties: [
            {
                isTypeImage: true,
                id: '_images',
                label: '',
                isOptional: false,
                rowClass: 'productcomparison-table-head'
            },
            {
                isTypeControlActions: true,
                id: ['_url', 'internalid'],
                label: '',
                isOptional: false,
                rowClass: 'productcomparison-table-head'
            },
            {
                id: 'storedisplayname2',
                label: '',
                isOptional: false,
                rowClass: 'productcomparison-table-head'
            },
            {
                id: 'pricelevel5_formatted',
                label: 'Price',
                isOptional: false
            },
            {
                id: 'relateditems_details',
                label: 'Related Items Details',
                isOptional: true
            },
            {
                id: 'isinstock',
                label: 'Is In Stock',
                isOptional: true
            },
            {
                id: 'outofstockbehavior',
                label: 'Out of Stock Behavior',
                isOptional: true
            },
            {
                id: 'itemtype',
                label: 'Item Type',
                isOptional: true
            },
            {
                id: 'ispurchasable',
                label: 'Is Purchaseable',
                isOptional: true
            },
            {
                id: 'quantityavailable',
                label: 'Quantity Avaliable',
                isOptional: true
            },
            {
                id: 'custitem_test_label',
                label: 'Test Label',
                isOptional: true
            }
        ]
    };

    _.extend(ProductComparisonConfig, {
        get: function get() {
            return this;
        }
    });

    _.extend(GlobalConfiguration, {
        optionListLabels: ProductComparisonConfig
    });

    GlobalConfiguration.publish.push({
        key: 'ProductComparison_config',
        model: 'ProductComparison.Configuration',
        call: 'get'
    });

    return ProductComparisonConfig;
});
