<div class="homepage-popup">
    <div class="homepage-popup-cms-intro" data-cms-area="homepage_popup_intro" data-cms-area-filters="path"></div>
    <div class="homepage-popup-cms-main" data-cms-area="homepage_popup_main" data-cms-area-filters="path"></div>

    {{#if showHosts}}
    <div class="homepage-popup-settings">
        <h4 class="homepage-popup-settings-title">{{translate 'Site Settings'}}</h4>
            <div class="homepage-popup-language">
                <div class="homepage-popup-language-label-wrapper">
                    <label class="homepage-popup-language-label" for="setlanguage">
                        {{translate 'Languages'}}
                    </label>
                </div>
                <div class="homepage-popup-language-input-wrapper">
                    <select class="homepage-popup-language-input" name="setlanguage" id="setlanguage">
                        <option>{{translate 'Please select...'}}</option>

                        {{#each availableHosts}}

                            {{#if hasLanguages}}
                                <!-- uncomment the optgroup tag if grouping of regions is required
                                where regions could contain the same languages -->
                                <!-- <optgroup label="{{title}}"> -->
                                    {{#each languages}}
                                        <option value="{{host}}" data-locale="{{locale}}"
                                        {{#if hasSingleCurrency}} data-singlecurrency="{{singleCurrency}}"{{/if}}
                                        {{#if isSelected}} selected{{/if}}>{{displayName}}</option>
                                    {{/each}}
                                <!-- </optgroup> -->
                            {{/if}}

                        {{/each}}
                    </select>
                </div>
            </div>
            <div class="homepage-popup-currency" id="homepage-popup-currency">
                <div class="homepage-popup-currency-label-wrapper">
                    <label class="homepage-popup-currency-label" for="setcurrency">
                        {{translate 'Currencies'}}
                    </label>
                </div>
                <div class="homepage-popup-currency-input-wrapper">
                <select class="homepage-popup-currency-input" name="setcurrency" id="setcurrency">
                    <option id="homepage-popup-option">{{translate 'Please select...'}}</option>
                </select>
                </div>
            </div>
    </div>
    {{/if}}
    <div class="homepage-popup-cms-footer" data-cms-area="homepage_popup_footer" data-cms-area-filters="path"></div>
</div>