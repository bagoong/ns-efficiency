describe('Categories Test Automation', function()
{
    it ('Should Dispay Categories', function(done)
    {
        var client = this.client;
        client
            .url('http://muldrow.demo.efficiencies.cdn.netsuitestaging.com')
            .waitForAjax()
            .pause(1000)
            .click('.header-menu-level1-anchor')
            .pause(1000)
            .waitForAjax()
            .getText('.header-menu-level1-anchor', function(err, level1AnchorText) {
                if (level1AnchorText !== undefined) {
                    client
                        .url('http://muldrow.demo.efficiencies.cdn.netsuitestaging.com/Women')
                        .waitForAjax()
                        .pause(2000)
                        .getText('.category-main-description', function(err, categoryDescription) {
                            expect(categoryDescription).not.toBe(undefined);
                        })
                        .pause(1000)
                        .call(done)
                    ;
                }

            })
            .pause(1000)
            .call(done)
        ;
    });
});