/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/
// @module ProductComparison
define('ProductComparison.Collection', [
    'Backbone.CachedCollection',
    'ItemDetails.Collection',
    'Session',
    'underscore',
    'Utils'
], function ProductComparisonCollection(
		BackboneCachedCollection,
        ItemDetailsCollection,
        Session,
        _
    ) {
    'use strict';

    return ItemDetailsCollection.extend({
        url: function Url() {
            var productIDs = [];
            var idString = '';
            var url = '';
            var tmpVal;

			// Retreieving from URL queryStr from options
            var queryStrParams = this.options;
            if ( queryStrParams ) {
                _.each(queryStrParams, function eachQueryParams(value, key) {
                    tmpVal = parseInt(value, 10);
                    if ( key < 4 ) {
                        productIDs.push(tmpVal);
                        idString += 'id=' + tmpVal + '&';
                    } else { return; }
                });
                url = '/api/items?' + idString + 'fieldset=productcomparison';
            }
            return url;
        },

        // @method initialize
        initialize: function initialize(options) {
            this.options = options;
            this.itemProperties = this.getModuleConfig().itemProperties;
        },

        // @method getModuleConfig
        getModuleConfig: function getModuleConfig() {
            return SC.ENVIRONMENT &&
				SC.ENVIRONMENT.published &&
				SC.ENVIRONMENT.published.ProductComparison_config;
        },

        // @method getMappedValuesToProperties
        getMappedValuesToProperties: function getMappedValuesToProperties() {
            var self = this;
            var productValues = [];
            var itemValue;
            var tmpItemProperties = _.clone(this.itemProperties);
            _.map(tmpItemProperties, function tmpItemPropertiesMapping(itemProperty, key) {
                productValues = [];
                self.each( function eachProperty(item) {
                    if ( itemProperty ) {
                        if ( itemProperty.isTypeControlActions ) {
                            itemValue = {};
                            _.each(itemProperty.id, function eachItemPropertyId(itemPropertyID) {
                                itemValue[itemPropertyID] = item.get(itemPropertyID);
                            });
                        } else {
                            itemValue = item.get(itemProperty.id);
                        }
                        if (itemProperty.id === '_images') {
                            itemValue = _.first(itemValue);
                        }
                        productValues.push(itemValue);
                    }
                });

				// if the product values is not empty, add it in the item property else remove
                if (_.compact(productValues).length !== 0) {
                    itemProperty.productValues = productValues;
                } else {
                    delete tmpItemProperties[key];
                }
            });
            return _.compact(tmpItemProperties);
        }
    });
});
