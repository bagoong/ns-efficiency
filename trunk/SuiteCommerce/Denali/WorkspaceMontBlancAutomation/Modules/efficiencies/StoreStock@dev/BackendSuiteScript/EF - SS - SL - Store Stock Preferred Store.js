/**
 * Store Stock
 *
 * Version    Date            Author           Remarks
 * 1.00       04 January 2016     cparayno
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function service(request, response) {
    'use strict';
    var result;
    var customerId;
    var storeId;
    var customer;

    try {
        if (request.getMethod() === 'POST') {
            customerId = request.getParameter('id');
            storeId = request.getParameter('storeid');

             customer = nlapiLoadRecord('customer', customerId);
             customer.setFieldValue('custentity_isp_preferred_store', storeId);

             result = nlapiSubmitRecord(customer);
        }
    } catch (e) {
        result = {status: e.message};
    }

    response.setContentType('JSON');
    response.write(JSON.stringify(result));
}