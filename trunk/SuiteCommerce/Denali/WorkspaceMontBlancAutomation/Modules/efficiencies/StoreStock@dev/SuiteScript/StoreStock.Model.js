define('StoreStock.Model', [
    'SC.Model'
], function StoreStockModel(SCModel) {
    'use strict';

    return SCModel.extend({
        name: 'StoreStock',

        listDetail: function listDetail(id, storeID) {
            var apiRequest;
            var serviceUrl = nlapiResolveURL(
                'SUITELET',
                'customscript_ef_sl_storestock',
                'customdeploy_ef_sl_storestock',
                true
            );

            serviceUrl = ( id ) ? serviceUrl + '&id=' + encodeURIComponent(id) : serviceUrl;
            serviceUrl = ( storeID ) ? serviceUrl + '&stores=' + storeID : serviceUrl;
            apiRequest = nlapiRequestURL(serviceUrl, null, null, 'GET');

            return apiRequest.getBody();
        },

        preferredStoreUpdate: function preferredStoreUpdate(storeID) {
            var userId = nlapiGetUser();
            var apiRequest;
            var serviceUrl = nlapiResolveURL(
                'SUITELET',
                'customscript_ef_sl_preferredstore',
                'customdeploy_ef_sl_preferredstore',
                true
            );

            if (userId) {
                serviceUrl = serviceUrl + '&id=' + encodeURIComponent(userId);
                serviceUrl = serviceUrl + '&storeid=' + storeID;
                apiRequest = nlapiRequestURL(serviceUrl, null, null, 'POST');
            }

            return apiRequest.getBody();
        }
    });
});