/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.View', [
    'Backbone',
    'Backbone.CompositeView',

    'StoreStock.PickupTab.View',

    'store_stock.tpl',
    'jQuery'
], function StoreStockView(
    Backbone,
    BackboneCompositeView,

    StoreStockPickupTabView,

    storeStockTpl,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockTpl,
        events: {
            'click [data-action="store-stock-tabheader-click"]': 'setActiveTab'
        },

        initialize: function initialize() {
            var matrixChildren = this.model.getSelectedMatrixChilds();
            this.itemModel = matrixChildren.length === 1 ? matrixChildren[0] : this.model;

            if (! jQuery.cookie('activeTab') ) {
                jQuery.cookie('activeTab', '#shipping');
            }

            console.log('this.itemModel', this.itemModel);

            BackboneCompositeView.add(this);
        },

        childViews: {
            'PickupTab.Content.View': function PickupTabContentView() {
                return new StoreStockPickupTabView({
                    model: this.itemModel,
                    application: this.options.application
                });
            }
        },

        getContext: function getContext() {
            var stockStatuses = this.options.application.getConfig('storeStock.stockStatus');
            var quantityAvailable = this.itemModel.get('quantityavailable');
            var stockStatus = stockStatuses[ quantityAvailable > 0 ? 1 : 2 ];

            return {
                isSelectionComplete: this.model.isSelectionComplete(),
                quantityAvailable: quantityAvailable,
                isDisplayAsBadge:
                    quantityAvailable > 0 ?
                        this.options.application.getConfig('storeStock.isDisplayAsBadge')
                        : true,
                badgeClass: stockStatus && stockStatus.htmlClass,
                badgeText: stockStatus && stockStatus.badgeLabel,
                activeTabShipping: jQuery.cookie('activeTab') && jQuery.cookie('activeTab') === '#shipping',
                activeTabPickup: jQuery.cookie('activeTab') && jQuery.cookie('activeTab') === '#pickup'
            };
        },

        setActiveTab: function setActiveTab(e) {
            jQuery.cookie('activeTab', jQuery(e.target).data('target') );
        }
    });
});