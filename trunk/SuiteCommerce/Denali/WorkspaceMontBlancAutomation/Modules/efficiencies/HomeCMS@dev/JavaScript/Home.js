define('HomeCMS', [
    'HomeCMS.Router'
], function defineHome(
    HomeRouter
) {
    'use strict';
    return {
        mountToApp: function mountToApp(application) {
            return new HomeRouter(application);
        }
    };
});
