define('BOPIS.OrderWizard.Module.ShippingPackage', [
    'BOPIS.OrderWizard.Module.PackageBase',
    'BOPIS.OrderWizard.Module.Items.Line.Actions.View',
    'underscore',
    'Utils'
], function BOPISOrderWizardModuleBOPISPackageCreation(
    PackageBase,
    Actions,
    _
) {
    'use strict';

    return PackageBase.extend({
        currentStatus: 'shipping',
        oppositeStatus: 'pickup',

        getActionsView: function getActionsView() {
            return Actions.extend({
                statusToSet: this.oppositeStatus,
                btnText: _('Change to Pickup').translate(),
                keyMappingCondition: '_ispickable'
            });
        },

        getCollection: function getCollection() {
            return this.wizard.model.getLinesForShipping();
        },
        getPackageSubtitle: function getPackageSubtitle() {
            return _('Items For Shipping').translate();
        }
    });
});