define('BOPIS.OrderWizard.Module.Items.Line.Cell.View',	[
    'ItemViews.Price.View',
    'ItemViews.SelectedOption.View',

    'InventoryDisplay.View',
    'InventoryDisplay.Status.View',

    'Backbone.CollectionView',
    'Backbone.CompositeView',

    'bopis_order_wizard_items_line_cell.tpl',
    'item_views_cell_actionable_selected_options_cell.tpl',

    'Backbone',
    'underscore'
],	function BOPISItemViewsCellActionablePackageView(
    ItemViewsPriceView,
    ItemViewsSelectedOptionView,

    InventoryDisplayView,
    InventoryDisplayStatusView,

    BackboneCollectionView,
    BackboneCompositeView,

    itemViewsCellTpl,
    itemViewsCellItemSelectedOptions,

    Backbone,
    _
) {
    'use strict';

    // @class ItemViews.Actionable.View @extend Backbone.View
    return Backbone.View.extend({

        template: itemViewsCellTpl,

        initialize: function initialize(options) {
            this.options = options;
            this.application = options.application;
            this.model = options.model;

            BackboneCompositeView.add(this);
        },

        childViews: {
            'Item.Price': function ItemPrice() {
                this.model.get('item').set('quantity_total', this.model.get('quantity_total'));

                return new ItemViewsPriceView({
                    model: this.model.get('item'),
                    linePrice: this.model.get('rate'),
                    linePriceFormatted: this.model.get('rate_formatted'),
                    hideComparePrice: true
                });
            },

            'Item.SelectedOptions': function ItemSelectedOptions() {
                return new BackboneCollectionView({
                    collection: new Backbone.Collection(this.model.get('item').getPosibleOptions()),
                    childView: ItemViewsSelectedOptionView,
                    cellTemplate: itemViewsCellItemSelectedOptions,
                    viewsPerRow: 1,
                    childViewOptions: {
                        cartLine: this.model
                    }
                });
            },
            'Item.Summary.View': function ItemSummaryView() {
                return new this.options.SummaryView(_.extend({
                    model: this.model,
                    application: this.application
                }, this.options.summaryOptions || {}));
            },
            'Item.Actions.View': function ItemActionsView() {
                return new this.options.ActionsView(_.extend({
                    model: this.model,
                    application: this.application
                }, this.options.actionsOptions || {}));
            },
            'ItemViews.Stock.View': function InventoryDisplayStatusViewConstruct() {
                var stockModelPickup = this.model.get('stockInfo') && this.model.get('stockInfo').pickup;
                var stockModelShipping = this.model.get('stockInfo') && this.model.get('stockInfo').shipping;

                return new InventoryDisplayView({
                    model: this.model.get('item'),
                    application: this.application,
                    stockModel: {
                        pickup: (!_.isUndefined(stockModelPickup)) ? new Backbone.Model(stockModelPickup) : null,
                        shipping: (!_.isUndefined(stockModelShipping)) ? new Backbone.Model(stockModelShipping) : null
                    }
                });
            }
        },


        getContext: function getContext() {
            var item = this.model.get('item');
            var thumbnail = item.get('_thumbnail');

            // @class ItemViews.Actionable.View.Context
            return {

                line: this.model,

                lineId: this.model.get('internalid'),

                item: this.model.get('item'),

                itemId: item.get('internalid'),

                linkAttributes: item.get('_linkAttributes'),

                imageUrl: this.application.resizeImage(thumbnail.url, 'thumbnail'),

                isNavigable: !!this.options.navigable && !!item.get('_isPurchasable'),

                altImageText: thumbnail.altimagetext,

                showCustomAlert: !!item.get('_cartCustomAlert'),

                customAlertType: item.get('_cartCustomAlertType') || 'info',

                showActionsView: !!this.options.ActionsView,

                showSummaryView: !!this.options.SummaryView,

                showAlert: !_.isUndefined(this.options.showAlert) ? !!this.options.showAlert : true,

                showGeneralClass: !!this.options.generalClass,

                generalClass: this.options.generalClass
            };
        }
    });
});
