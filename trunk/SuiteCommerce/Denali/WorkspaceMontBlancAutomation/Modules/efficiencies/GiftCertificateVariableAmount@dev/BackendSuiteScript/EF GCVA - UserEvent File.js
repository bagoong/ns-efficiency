//          Create Date      2014/07/09
//          FileName		 scriptableGC_L41.js
//          Developer		 Somsak Ri
//          E-Mail			 risomsak@gmail.com


var gcLeatherItem = '49150';
var gcPaperItem = '49149';
var leatherShippingValue = 7.00;
var paperShippingValue = 3.00;
var shipmethod = 1687;

function onRecalc(type, action){
    if ( type != 'item' ) {

        return;

    }

    try{
        if(nlapiGetContext().getExecutionContext() !== 'webstore') {
            return;
        }
        var processing = nlapiGetFieldValue('custbody_processing');

        nlapiLogExecution('debug', 'processing', processing);

        if ( processing != null && processing == 'T' ) {

            return;

        }


        setVariableAmount(type, action);


    }catch(err){

        nlapiLogExecution('debug', 'error', err);

    }finally{

        nlapiSetFieldValue('custbody_processing', 'F');
        return true;

    }
}


function setVariableAmount(type, action) {
    if(nlapiGetContext().getExecutionContext() !== 'webstore') return true;

    if ((type === 'item') && (action == 'commit'))
    {
        try {
            if (nlapiGetFieldValue('custbody_is_processing') === 'T') {
                return;
            }
            nlapiSetFieldValue('custbody_is_processing', 'T', false, true);
            setAmount();
        } catch(e) {
            nlapiLogExecution('ERROR', 'Scriptable cart error',  e);
        } finally {
            nlapiLogExecution('ERROR', 'end');
            nlapiSetFieldValue('custbody_is_processing', 'F', false, true);
        }
    }
}

function setAmount() {
    var totalQty = nlapiGetLineItemCount('item'),
        line;

    for (line = 1; line <= totalQty; line++){

        var itemId = nlapiGetLineItemValue('item','item',line),
            isVariableAmount = nlapiGetLineItemValue('item','custcol_is_variable_amount',line);

        if (isVariableAmount === 'T') {
            var amount = nlapiGetLineItemValue('item','custcol_certificate_value',line),
                currentAmount = nlapiGetLineItemValue('item', 'amount', line);

            if (currentAmount != amount) {
                nlapiSelectLineItem('item', line);
                nlapiSetCurrentLineItemValue('item', 'rate', amount, false, true);
                nlapiSetCurrentLineItemValue('item', 'amount', amount, false, true);
                nlapiCommitLineItem('item');
            }
        }
    }
}