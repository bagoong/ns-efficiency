define('SiteManagementToolRegions',[
    'underscore',
    'Footer.View',
    'SC.Configuration',
    'GlobalViews.Message.View'
], function(
    _,
    FooterView,
    Configuration,
    GlobalViewsMessageView
){
    'use strict';

    return {
        mountToApp: function(application) {

            FooterView.prototype.getContext = function(){
                /**
                 * Returns current year and
                 * site display name (from Website Settings)
                 * for use in copyright statement
                 */
                return {
                    currentYear: new Date().getFullYear(),
                    displayName: Configuration.get('siteSettings.displayname')
                };
            }

        }
    }
});