{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home">
	<!-- Top header banner -->
	<section class="home-banner-top" data-cms-area="qs-home-banner-top" data-cms-area-filters="path">
		<!-- Dummy content - will be replaced by CMS -->
		<p>Use promo code <strong>SCADEMO</strong> for <strong>30%</strong> off your purchase</p>
		<!-- End Dummy content -->
	</section>

	<!--Main Banner -->
	<section class="home-main" data-cms-area="qs-home-main" data-cms-area-filters="path">
		<!-- Dummy content - will be replaced by CMS -->
		<div class="home-slider-container">
			<div class="home-image-slider">
				<ul data-slider class="home-image-slider-list">
					{{#each carouselImages}}
						<li>
							<div class="home-slide-main-container">
								<img src="{{resizeImage this ../imageHomeSize}}" alt="" />
								<div class="home-slide-caption">
									<h2 class="home-slide-caption-title">SAMPLE HEADLINE</h2>
									<p>Example descriptive text displayed on multiple lines.</p>
									<div class="home-slide-caption-button-container">
										<a href="/search" class="home-slide-caption-button">Shop Now</a>
									</div>
								</div>
							</div>
						</li>
					{{/each}}
				</ul>
			</div>
		</div>
		<!-- End Dummy content -->
	</section>

	<!--Banner / Promo-->
	<section class="home-bottom-content">
		<div class="home-banner-bottom" data-cms-area="qs-home-banner1" data-cms-area-filters="path"></div>

		<div class="home-banner-bottom" data-cms-area="qs-home-banner2" data-cms-area-filters="path"></div>

		<div class="home-banner-bottom" data-cms-area="qs-home-banner3" data-cms-area-filters="path"></div>

		<div class="home-banner-bottom-main" data-cms-area="qs-home-banner-bottom-main" data-cms-area-filters="path">
			<!-- Dummy content - will be replaced by CMS -->
			<div class="home-banner-main">
				{{#each bottomBannerImages}}
		      	<div class="home-banner-main-cell-nth{{@index}}">
			      	<div class="home-banner-main-cell-bg">
			        	<img src="{{resizeImage this ../imageHomeSizeBottom}}" alt="" >
			        	<div class="home-banner-main-cell-text">EXAMPLE TEXT</div>
			        </div>
		      	</div>
		    	{{/each}}
		    </div>
		    <!-- End Dummy content -->
		</div>
	</section>

	<!--Merchandising zone-->
	<section class="home-merchandizing-zone" data-cms-area="qs-home-merchandizing-zone" data-cms-area-filters="path"></section>
</div>