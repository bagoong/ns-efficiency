define('BOPIS.LiveOrder.Model', [
    'LiveOrder.Model',
    'Store.Model',
    'underscore'
], function LiveOrderModelInStorePickup(
    LiveOrderModel,
    StoreModel,
    _
) {
    'use strict';

    _.extend(LiveOrderModel.prototype, {
        getPickUpAbleLines: function getPickUpAbleLines() {
            return this.get('lines').filter(function filter(line) {
                return line.get('item').get('_ispickable');
            });
        },
        hasPickUpAbleLines: function hasPickUpAbleLines() {
            var lines = this.getPickUpAbleLines();
            return lines && lines.length > 0;
        },
        getShippableLines: function getShippableLines() {
            return this.get('lines').filter(function filter(line) {
                return line.get('item').get('_isshippable');
            });
        },
        hasShippableLines: function hasShippableLines() {
            var lines = this.getShippableLines();
            return lines && lines.length > 0;
        },
        /*
        updateLineDelivery: function updateLineDelivery(line, options) {
            line.urlRoot = _.getAbsoluteUrl('services/BOPIS.LiveOrder.Line.Delivery.Service.ss');
            line.ongoingPromise = line.save({}, this.wrapOptionsSuccess(options));

            return line.ongoingPromise;
        },*/
        changeDeliveryForLine: function updateDeliveryForLine(line, deliveryMethod) {
            var lineShipping = 0;
            var linePickup = 0;
            line.set('deliverymethod', deliveryMethod);
            this.get('lines').each(function eachCheckedLine(l) {
                if (l.get('deliverymethod') === 'pickup') {
                    linePickup ++;
                } else {
                    lineShipping ++;
                }
            });

            if (lineShipping > 0 && linePickup > 0) {
                this.set('deliverymethod', 'partial');
            } else if (linePickup > 0) {
                this.set('deliverymethod', 'pickup');
            } else {
                this.set('deliverymethod', 'shipping');
            }
            this.trigger('linedeliverychange');
        },

        getLinesForShipping: function getLinesForShipping() {
            return this.get('lines').filter(function filter(line) {
                return line.get('deliverymethod') === 'shipping' && line.get('item').get('_isfulfillable');
            });
        },
        getLinesForPicking: function getLinesForPicking() {
            return this.get('lines').filter(function filter(line) {
                return line.get('deliverymethod') === 'pickup' && line.get('item').get('_isfulfillable');
            });
        },
        getLinesOnlyShipping: function getLinesOnlyShipping() {
            return this.get('lines').filter(function filter(line) {
                return line.get('item').get('_isonlyshippable');
            });
        },

        getIfThereAreDeliverableItems: function getIfThereAreDeliverableItems() {
            return this.getLinesForShipping().length > 0;
        },
        // @method shippingAddressIsRequired Checks if the shipping address is require and if
        // all items in the order are nonshippable. @return {Boolean}
        shippingAddressIsRequired: function shippingAddressIsRequired() {
            return this.getIfThereAreDeliverableItems();
                /* Configuration.get('siteSettings.requireshippinginformation', 'F') === 'T' */
        },
        parse: _.wrap(LiveOrderModel.prototype.parse, function wrappedParse(fn) {
            var originalRet = fn.apply(this, _.toArray(arguments).slice(1));
            if (originalRet) {
                originalRet.store = new StoreModel(originalRet.store, {parse: true});
                this.currentModelState = JSON.stringify(originalRet);
            }
            return originalRet;
        })
    });
});