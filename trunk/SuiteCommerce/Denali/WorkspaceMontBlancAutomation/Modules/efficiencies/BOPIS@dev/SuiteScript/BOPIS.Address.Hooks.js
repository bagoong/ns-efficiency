define('BOPIS.Address.Hooks', [
    'Application',
    'Address.Model',
    'BOPIS.Address'
], function BOPISAddressHooks(
    Application
) {
    'use strict';

    /**
     * Avoid returning Store Addresses as possible Addresses for anything
     */
    Application.on('after:Address.list', function afterAddressList(Model, list) {
        Model.filterStoreAddress(list);
    });
});