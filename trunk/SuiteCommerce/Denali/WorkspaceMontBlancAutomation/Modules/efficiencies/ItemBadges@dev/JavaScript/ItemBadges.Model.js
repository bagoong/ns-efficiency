/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges.Model', [
    'Backbone.CachedModel',
    'underscore',
    'Utils'
], function ItemBadgesModel(CachedModel, _) {
    'use strict';

    return CachedModel.extend({
        urlRoot: _.getAbsoluteUrl('services/ItemBadges.Service.ss')
    });
});