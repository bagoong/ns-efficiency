<div class="storelocator-direction-buttons">
    <h4 class="panel-title h4-panel-title">{{ translate "DIRECTIONS" }}</h4>
    <button type="button" name="DRIVING" class="travelMode active">
        <i class="driving-icon"></i>
    </button>
    <button type="button" name="WALKING" class="travelMode">
        <i class="walking-icon"></i>
    </button>
    <button type="button" name="BICYCLING" class="travelMode">
        <i class="bicycling-icon"></i>
    </button>
    <button type="button" name="TRANSIT" class="travelMode">
        <i class="transit-icon"></i>
    </button>
</div>
<form role="form" class="stores-store-locator-search-form">
    <fieldset class="stores-store-locator-search-form-fieldset">
        <div class="control-group">
            <label for="storelist-place" class="stores-store-locator-search-form-label">{{translate 'From:'}} </label>
            <div class="stores-store-locator-search-form-controls">
                <input type="text" class="stores-store-locator-search-form-input" name="storelist-place" id="storelist-place" data-storelist-place="store-list-place">
            </div>
        </div>

        <div class="control-group">
            <button type="submit" class="storelocator-submit storelocator-button"> {{translate 'Go'}} </button>
            {{#if showGeolocationButton}}
                <button type="button" name="geolocate" class="storelocator-geolocate storelocator-button">
                    {{translate 'Use Current Location'}}
                </button>
            {{/if}}
        </div>

    </fieldset>

</form>