/*
    StoreLocator.Configuration - Load default configuration for Google map and StoreLocator
*/
define('StoreLocator.Configuration', ['underscore', 'Utils'], function StoreLocatorConfiguration(_) {
    'use strict';

    var unitsHandlers = {
        miles: {
            precision: 0,
            id: 'miles',
            shortName: 'Mi.',
            name: 'Miles',
            toKilometers: function toKilometers(value) {
                return value * 1.609344;
            }
        },
        kilometers: {
            precision: 0,
            id: 'Kilometers',
            shortName: 'km',
            name: 'Kilometers',
            toMiles: function toMiles(value) {
                return value * 0.621371192;
            }
        }
    };

    return {
        unitsHandlers: unitsHandlers,
        /*  geolocateOnInit
            Default value : false
            Description : Set to True if auto locate current position is required on store locator load
        */
        geolocateOnInit: false,
        geolocationEnabled: window.navigator && navigator.geolocation,
        /*
            storeCountShow
            Default value : 5
            Description: Number of nerest stores to be show
         */
        storeCountShow: 5,
        /*
            SchemaOrg
            default value : Store
            Reference : https://schema.org/Store
         */
        SchemaOrg: 'Store',
        units: 'miles',
        /*
            map / mapStyles
            Description : Default setting for google map API
            Reference : https://developers.google.com/maps/documentation/javascript/reference#release-version
         */
        map: {
            defaultZoom: 12,
            selectedStoreZoom: 14,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            scaleControl: true,
            animation: 'DROP',
            streetViewControl: false,
            allowDrag: true,
            defaultCenter: {
                lat: 39.912811,
                lng: -101.465781
            }
        },
        mapStyles: [
            {
                featureType: 'poi.business',
                elementType: 'labels',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            }
        ],
        /*
            errorDirection
            Description : text to show when direction is not applicable
         */
        errorDirection: {
            text: 'No direction found'
        },
        /*
            storeMarker
            Description : Stores marker setting
            Custom image setup :
                1.  Upload icon image to images folder under the module
                2.  Add new property icon: _.getAbsoluteUrl('img/<image file name>')
                3.  Adjust the size property the same as the new image width and height
         */
        storeMarker: {
            showLabel: true,
            animation: 'DROP',
            size: {
                x: 22,
                y: 40
            },
            origin: {
                x: 0,
                y: 0
            },
            anchor: {
                x: 15,
                y: 32
            },
            allowDrag: false
        },
        /*
            originMarker
            Description : Origin marker setting
            Custom image setup :
                1.  Upload icon image to images folder under the module
                2.  Add new property icon: _.getAbsoluteUrl('img/<image file name>')
                3.  Adjust the size property the same as the new image width and height
         */
        originMarker: {
            animation: 'DROP',
            text: 'You\'re here',
            size: {
                x: 39,
                y: 34
            },
            origin: {
                x: 0,
                y: 0
            },
            anchor: {
                x: 11,
                y: 33
            },
            icon: '//www.google.com/mapfiles/arrow.png'
        },

        radiusCircle: {
            distanceFilters: 500,
            draw: true,
            style: {
                strokeWeight: 1,
                strokeColor: '#0000FF',
                fillColor: '#0000FF',
                strokeOpacity: 0.5,
                fillOpacity: 0.08
            }
        }
    };
});