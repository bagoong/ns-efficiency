var _ = require('underscore');

defineHelper('CreditCard.Expects', 
{

	checkExpectedCreditCardIsListed: function (expected_credit_card, cb)
	{
		var client = this;
		var status = false;
		this
			.CreditCard.getCreditCardsDataType(function(err, found_credit_cards)
			{
				found_credit_cards.forEach(function(found_credit_card)
				{
					client.call(function()
					{
						if((found_credit_card.cardType.toUpperCase() == expected_credit_card.cardType.toUpperCase())
						&& (found_credit_card.cardEnding == expected_credit_card.cardEnding)
						&& (found_credit_card.expMonth == expected_credit_card.expMonth)
						&& (found_credit_card.expYear == expected_credit_card.expYear)
						&& (found_credit_card.name.toUpperCase() == expected_credit_card.name.toUpperCase()))
						{
							if(found_credit_card.defaultCreditCard || expected_credit_card.ccDefault){
								if((found_credit_card.defaultCreditCard || expected_credit_card.ccDefault) == (expected_credit_card.ccDefault || expected_credit_card.defaultCreditCard))
								{
									status = true;		
								}
							}
							else
							{
								status = true;
							}
						}	
					});
					
				})
			})

			.call(function()
			{
				expect(status).toBeTruthy();
				cb(null, status);
			})
		;
	}

,	checkExpectedCreditCardListSize: function (expected_size, cb)
	{
		var status = false;
		this
			.CreditCard.getCreditCardsDataType(function(err, found_credit_cards) {
				expect(found_credit_cards.length).toEqual(expected_size)
			})

			.call(cb)
		;
	}

,	checkExpectedErrorsAreInErrorList: function(expected_errors, cb)
	{
		this
			.CreditCard.getErrorList(function(err, errorList)
			{
				var status = false;
				expected_errors.forEach(function(error){
					if(_.contains(errorList, error))
					{
						status = true;
						return false;
					}
				})
				expect(status).toBeTruthy();
			})
			.call(cb)
		;
	}

,	checkCreditCardPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.CreditCard.creditCardPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Credit Card page is not present.');
        	})
        	.call(cb)
        ;
	}

});