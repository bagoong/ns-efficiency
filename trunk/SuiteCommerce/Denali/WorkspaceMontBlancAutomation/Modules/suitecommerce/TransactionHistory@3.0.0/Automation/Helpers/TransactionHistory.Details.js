defineHelper('TransactionHistory.Details', 
{
	openAccordions: function (cb)
	{
		var client = this;
		this
			.waitForAjax()
			.isExisting(Selectors.TransactionHistory.detailHeadAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.TransactionHistory.detailHeadAccordion)
						.waitForAjax() 
					;
				}
			})
			.isExisting(Selectors.TransactionHistory.detailSecondaryAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.TransactionHistory.detailSecondaryAccordion)
						.waitForAjax() 
						.isExisting(Selectors.TransactionHistory.detailSecondaryAccordionCollapsed).then(function (isCollapsed) 
						{
					        if(isCollapsed)
					        {	        	
					        	client
									.click(Selectors.TransactionHistory.detailSecondaryAccordionCollapsed)
									.waitForAjax() 
								;
					        }
					    })
					;
				}
			})
			.isExisting(Selectors.TransactionHistory.detailSecondaryShippingAccordion).then(function (isExisting)
			{
				if(isExisting)
				{
					client
						.click(Selectors.TransactionHistory.detailSecondaryShippingAccordion)
						.waitForAjax() 
						.isExisting(Selectors.TransactionHistory.detailSecondaryShippingAccordionCollapsed).then(function (isCollapsed) 
						{
					        if(isCollapsed)
					        {	        	
					        	client
									.click(Selectors.TransactionHistory.detailSecondaryShippingAccordionCollapsed)
									.waitForAjax() 
								;
					        }
					    })
					;
				}
			})

			.call(cb)
		;
	}

,	getTrakingValue: function(fulfillmentId, cb)
	{
		var parent_element = Selectors.TransactionHistory.fulfillmentId.replace('%s', fulfillmentId) + ' ';
		var trackingNumber = '';
		var client = this;
		client
			.pause(5000)
			.isExisting(parent_element + Selectors.TransactionHistory.fulfillmentTrackingNumberListButton).then(function (exists){
				if(exists)
				{
					client
						.click(parent_element + Selectors.TransactionHistory.fulfillmentTrackingNumberListButton)
						.getText(parent_element + Selectors.TransactionHistory.fulfillmentTrackingNumbers).then(function (text){
							trackingNumber = text;
						})
						.call(function()
						{
							cb(null, trackingNumber);
						})
					;
				}
				else
				{
					client
						.isExisting(parent_element + Selectors.TransactionHistory.fulfillmentTrackingNumbers).then(function (exists){
							if(exists)
							{
								client
									.getText(parent_element + Selectors.TransactionHistory.fulfillmentTrackingNumbers).then(function (text){
										trackingNumber = text;
									})
									.call(function()
									{
										cb(null, trackingNumber);
									})
								;
							}
							else
							{
								client
									.call(function()
									{
										cb(null, '');
									})
								;
							}
						})
					;
				}
			})
		;
	}

,  clickDownloadPDF: function (cb)
	{
		this		
			.click(Selectors.TransactionHistory.buttonDownloadPDF)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickRequestReturn: function(cb)
	{
		this
			.waitForAjax()	
			.waitForExist(Selectors.TransactionHistory.requestReturnButton, 5000)

			.click(Selectors.TransactionHistory.requestReturnButton)

			.call(cb)
		;
	}

});