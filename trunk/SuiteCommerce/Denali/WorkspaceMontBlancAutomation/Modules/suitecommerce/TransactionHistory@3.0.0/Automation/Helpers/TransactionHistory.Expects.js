defineHelper('TransactionHistory.Expects',
{

	checkCorrectPage: function (cb)
	{
		this
			.isExisting(Selectors.TransactionHistory.pageContainer, function (err, is_existing)
			{
				expect(is_existing).toBeTruthy();
			})
			.call(cb)
		;
	}

,	verifyCorrectNumberOfTransactions: function(param, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.TransactionHistory.transactionList, 5000)
			.getText(Selectors.TransactionHistory.transactionList, function(err, text) {
				expect(text.length).toBe(param.expectedAmount);
			})
			.call(cb)
		;
	}	

,	checkTransactionHistoryPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.TransactionHistory.transactionHistoryPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Transaction History page is not present.');
        	})
        	.call(cb)
        ;
	}

,	verifyTransactionsAreDisplayedInTransactionHistory: function (transactionItem,cb)
	{
		this
			
			.TransactionHistory.getTransactionDetailsById(transactionItem.transactionId).then(function (found_trans)
			{
            	expect(found_trans).toHaveEqualFields(transactionItem);
        	})
        	.call(cb)
        ;
	}

});