defineHelper('Overview.Profile', 
{
	verifyProfileInformation: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Overview.profileCompany, 5000)

			.getText(Selectors.Overview.profileCompany, function(err, text){
				expect(params.company).toMatch(text);
			})
			.getText(Selectors.Overview.profileName, function(err, text){
				expect(params.fullName).toMatch(text);
			})
			.getText(Selectors.Overview.profileEmail, function(err, text){
				expect(params.user).toMatch(text);
			})

			.call(cb)
		;
	}
});