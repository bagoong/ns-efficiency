defineHelper('Overview.Payment', 
{

	verifyDefaultCreditCardOverview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Overview.paymentCard, 5000)

			.getText(Selectors.Overview.paymentCard, function(err, text){
				expect(text).not.toMatch('We have no default credit card on file for this account.');
			})

			.call(cb)
		;
	}
		
,	verifyNotDefaultCreditCardOverview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Overview.paymentCard, 5000)

			.getText(Selectors.Overview.paymentCard, function(err, text){
				expect(text).toMatch('We have no default credit card on file for this account.');
			})

			.call(cb)
		;
	}	

});