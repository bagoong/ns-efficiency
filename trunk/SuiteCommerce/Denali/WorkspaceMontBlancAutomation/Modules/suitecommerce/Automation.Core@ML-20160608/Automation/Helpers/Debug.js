registerHelper('debug',
{

	// @module automation @class Client @method startVerbose
	startVerbose: function (cb)
	{
		console.log("\n", "VERBOSE STARTED", "\n");
		this.automationSettings.enableVerbose = true;
		this.call(cb);
	}

	// @module automation @class Client @method endVerbose
,	endVerbose: function (cb)
	{
		console.log("\n", "VERBOSE FINISHED", "\n");
		this.automationSettings.enableVerbose = false;
		this.call(cb);
	}

	// @module automation @class Client @method startDebug
,	startDebug: function (cb)
	{
		console.log("\n", "DEBUG STARTED", "\n");
		this.automationSettings.enableDebug = true;
		this.call(cb);		
	}

	// @module automation @class Client @method startDebug
,	endDebug: function (cb)
	{
		console.log("\n", "DEBUG FINISHED", "\n");
		this.automationSettings.enableDebug = false;
		this.call(cb);		
	}

});
