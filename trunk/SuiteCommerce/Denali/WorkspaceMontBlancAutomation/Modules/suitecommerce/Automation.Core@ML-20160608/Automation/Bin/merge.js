var fs = require('fs')
,   path = require('path')
,   _ = require('underscore')
,   args = require('yargs').argv;


var AutomationCoreDirPath = path.join(__dirname, '../');
var AutomationResultMerger = require(path.join(AutomationCoreDirPath, 'Lib/AutomationResult/Merger'));
var Reporter = require(path.join(AutomationCoreDirPath, 'Lib/AutomationReporter'));

var testRunName = args['test-run'] || args['suite'] || "default";

var merger = new AutomationResultMerger({
    testRun: testRunName
});

var testRunDirPath = path.join('automation/automan/', testRunName);

merger.on('newSpecFound', function(spec)
{
    console.log(spec.description, spec.location, spec.status);
});

merger.mergeFilesFromDirPath(testRunDirPath);

var mergedResultsPath = path.join(testRunDirPath, 'mergedResult.json');

merger.saveResults(mergedResultsPath);

var reporter = new Reporter({
    savePath: './automation/results'
,   statusFilePath: mergedResultsPath
,   fileName: testRunName
});

reporter.saveResults();