
expectAndEnd = null;

beforeEach(function ()
{
	// @module automation @class JasmineContext 
	// @method expectAndEnd @param {Boolean} actual
	expectAndEnd = function (actual)
	{
		var expectation = expect(actual);

		expectation.addExpectationResult = function(pass, result)
		{
			if (!pass)
			{
				throw new Error (result.message);
			}
		}

		return expectation;
	}
});
    
