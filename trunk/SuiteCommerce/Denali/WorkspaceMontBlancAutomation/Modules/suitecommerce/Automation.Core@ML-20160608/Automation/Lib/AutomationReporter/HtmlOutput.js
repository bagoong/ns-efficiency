
var	path = require('path')
,	fs = require('fs')
,	_ = require('underscore')
,	Handlebars = require('handlebars')
;


Handlebars.registerHelper('formatDate', function(context)
{
	var formattedDate = (new Date(context)).toISOString().slice(0,19);
	formattedDate = formattedDate.replace("T"," ");

	return formattedDate;
});


Handlebars.registerHelper('json', function(context)
{
	var jsonString = JSON.stringify(context, null, 4);
	jsonString = jsonString.replace(/\\n/g, "\n    ");

	return jsonString;
});

Handlebars.registerHelper('nl2br', function(context)
{
	if (typeof context === 'string')
	{
		context = context.trim();
		context = context.replace(/\r?\n/g, '<br>');
		context = new Handlebars.SafeString(context);
	}

	return context;
});


var HtmlOutput = function (outputFilePath)
{
	var self = this;

	outputFilePath = /\.html?$/i.test(outputFilePath) ? outputFilePath : outputFilePath + '.htm';
	self.outputFilePath = outputFilePath;


	self.getResultsHtml = function(automationReporterResults)
	{
		var htmlReportTemplatePath = path.join(__dirname, '../../Templates/HtmlReport.Body.tpl');
		
		var source = String(
			fs.readFileSync(htmlReportTemplatePath)
		);

		var template = Handlebars.compile(source);

		var html = template(automationReporterResults);

		//console.log(html);
		return html;
	}


	self.saveResults = function(automationReporterResults)
	{
		var html = self.getResultsHtml(automationReporterResults);

		fs.writeFileSync(self.outputFilePath, html);
	}
}


module.exports = HtmlOutput;