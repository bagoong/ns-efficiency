var async = require('async')
,	_ = require('underscore')
,	findStrategy = require('webdriverio/lib/helpers/find-element-strategy.js')
,	args = require('yargs').argv
,	fs = require('fs')
,	nthExpression = /:nth\(([0-9]+)\)/g;

function parseSelector(value, is_single_find)
{
	var components = [];

	//takes care of removing empty tokens (at the start/end)
	var tokens = value.split(nthExpression).filter(function(token)
	{
		return token.trim().length > 0;
	});

	for (var i = 0; i < tokens.length; i += 2) {
		var selector = tokens[i];
		var index = tokens[i + 1];

		components.push({
			using: 'css selector',
			value: selector.trim(),
			index: index ? parseInt(index, 10) : undefined 
		});
	}

	return components;
}

function findElements(client, options, previous_result, cb)
{

	var url = '/session/:sessionId/elements';
	if (previous_result && previous_result.seleniumParent)
	{
		url = '/session/:sessionId/element/' + previous_result.seleniumParent + '/elements';
	}

	client.requestHandler.create(
		url,
		{ using: options.using, value: options.value },
		function(err, result)
		{
			if (err) return cb(err);

			if (options.index !== undefined)
			{
				result.seleniumParent = result.value[options.index].ELEMENT;
				result.value = result.value[options.index];
			}

			cb(null, result);
		}
	);
}

function overrideElement(is_single_find)
{
	return function()
	{
		var client = this
		,	find_strategy = findStrategy(arguments)
		,	selectors = [find_strategy];

		//CASE WHEN: not using :nth() pseudo selector, return default strategy
		if (find_strategy.using === 'css selector' || nthExpression.test(find_strategy.value))
		{
			selectors = parseSelector(find_strategy.value);
		}

		var fxs = selectors.map(function(selector)
		{
			return async.apply(findElements, client, selector);
		});
		fxs[0] = async.apply(fxs[0], null);

		async.waterfall(fxs, function(err, result)
		{
			if (err) return find_strategy.callback(err);

			//handles if it needs to return single value or multi value
			if ( _.isArray(result.value) && is_single_find)
			{
				result.value = result.value[0];
			}

			if ( !_.isArray(result.value) && !is_single_find)
			{
				result.value = [result.value];
			}

			find_strategy.callback(null, result);
		});
	};
}


function overrideClick (client)
{
	client.addCommand('defaultClick', client.click);

	client.addCommand('simulateClick', function(cssSelector, cb)
	{
		this
			.execute(function(cssSelector)
			{
				var clickEvent = document.createEvent("MouseEvent");
				clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

				var elements = document.querySelectorAll(cssSelector);
				for(var i = 0; i < elements.length; ++i) {
				    elements[0].dispatchEvent(clickEvent);
				}
			}, cssSelector, cb)
			.call(cb)
		;
	});


	return function(selector, cb) {
		var client = this;

		this
			.call(function()
			{
				if(args.coverage)
				{
					client.execute(function()
					{
						return getCoverage()
					}).then(function (ret)
					{
						fs.writeFileSync("automation/dumpCoverage/dumpCoverage"+(new Date()).getTime()+".json",JSON.stringify(ret.value, null, 4))
					});
				}
			})
			.waitForExist(selector, 5000)
			.isExisting(selector, function(err, exists)
			{
				if (exists)
				{
					client
						.call(function()
						{
							var mobileBrowsers = ['android', 'IOS'];

							if (global.WEBDRIVER_BROWSER && mobileBrowsers.indexOf(global.WEBDRIVER_BROWSER) !== -1)
							{
								client.simulateClick(selector);
							}
							else
							{
								client.defaultClick(selector);
							}
						})
						.call(cb)
					;
				} else {
					
					client
						.call(function()
						{
							expect("element " + selector).toBe(" present in page");
						})
						.call(client.endTest)
					;
				}
			})
		;
	}
}

var client = this;

global.installWebDriverOverrides = function(client)
{
	//see add command at webdriverio.js
	client.addCommand('element', overrideElement(true));
	client.addCommand('elements', overrideElement(false));

	client.addCommand('click', overrideClick(client));
};