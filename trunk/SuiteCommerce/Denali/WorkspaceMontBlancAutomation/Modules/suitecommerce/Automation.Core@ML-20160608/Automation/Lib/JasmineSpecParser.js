var fs = require('fs')
,	esprima = require('esprima');


module.exports = {

	extractJasmineSpecs: function(filePath)
	{
		var self = this;

		var specs = [];

		var fileCode = fs.readFileSync(filePath);
		var tokens = esprima.tokenize(fileCode);

		var catchSpec = false;
		var catchDescribe = false;

		var parentDescribe = null;

		tokens.forEach(function(tok)
		{
			if (tok.type === "Identifier")
			{
				if (['it', 'xit', 'fit'].indexOf(tok.value) !== -1)
				{
					catchSpec = true;
				}
				else if (['describe', 'xdescribe', 'fdescribe'].indexOf(tok.value) !== -1)
				{
					catchDescribe = true;
				}
			}
			else if (tok.type === 'String')
			{
				if (catchDescribe)
				{
					parentDescribe = self.cleanJsTokenString(tok.value);
					catchDescribe = false;
				}
				else if (catchSpec)
				{
					var specName = self.cleanJsTokenString(tok.value);
					
					specs.push({
						describe: parentDescribe
					,	it: specName
					});

					catchSpec = false;
				}
			}
		});

		return specs;
	}

,	cleanJsTokenString: function(tokVal)
	{
		tokVal = tokVal.replace(/^[\'"]?(.*?)[\'"]?$/, "$1");
		tokVal = tokVal.replace(/\\/g, '');
		return tokVal;
	}

};