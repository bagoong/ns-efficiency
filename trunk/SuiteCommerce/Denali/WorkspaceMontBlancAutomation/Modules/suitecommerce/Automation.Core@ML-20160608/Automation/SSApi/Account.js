/*
	IMPORTANT, PLEASE INSTALL THE FOLLOWING PACKAGES THAT AREN'T IN 
	THE GULP DISTRO SETUP:
	
	npm install request
	npm install tough-cookie

*/

'use strict';

var request = require('request');
var async = require('async');


var Account = function(params)
{
	['siteNumber', 'siteLocation', 'accountNumber'].forEach(function(val)
	{
		if (typeof params[val] === 'undefined') {
			throw val + " param is required to initialize account requests";
		}
	});

	this.siteNumber = params.siteNumber;
	this.siteLocation = params.siteLocation;
	this.accountNumber = params.accountNumber;
	this.touchPoints = {};

	if (params.rawUrl) {
		this.baseUrl = params.rawUrl;
	} else {
		this.baseUrl = 'https://checkout.netsuite.com/c.' + this.accountNumber + '/' + this.siteLocation;
	}

	this.request = request.defaults(
	{
		jar: true
	});
}


Account.prototype.getLocationCode = function (text, codes)
{
	text = text.trim();

	if (/^[A-Za-z]{2}$/.test(text))
	{
		text = text.toUpperCase();
		return text in codes;
	}

	var found_code = false;

	Object.keys(codes).forEach(function(code)
	{
		var code_text = codes[code];

		if (text === code_text)
		{
			found_code = code;
			return false;
		}
	});

	return found_code;
}


Account.prototype.getCountryCode = function (text) {
	var countries = {'AF':'Afghanistan', 'AX':'Aland Islands', 'AL':'Albania', 'DZ':'Algeria', 'AS':'American Samoa', 'AD':'Andorra', 'AO':'Angola', 'AI':'Anguilla', 'AQ':'Antarctica', 'AG':'Antigua and Barbuda', 'AR':'Argentina','AM':'Armenia','AW':'Aruba','AU':'Australia','AT':'Austria','AZ':'Azerbaijan','BS':'Bahamas','BH':'Bahrain','BD':'Bangladesh','BB':'Barbados','BY':'Belarus','BE':'Belgium','BZ':'Belize','BJ':'Benin','BM':'Bermuda','BT':'Bhutan','BO':'Bolivia','BQ':'Bonaire, Saint Eustatius and Saba','BA':'Bosnia and Herzegovina','BW':'Botswana','BV':'Bouvet Island','BR':'Brazil','IO':'British Indian Ocean Territory','BN':'Brunei Darussalam','BG':'Bulgaria','BF':'Burkina Faso','BI':'Burundi','KH':'Cambodia','CM':'Cameroon','CA':'Canada','IC':'Canary Islands','CV':'Cape Verde','KY':'Cayman Islands','CF':'Central African Republic','EA':'Ceuta and Melilla','TD':'Chad','CL':'Chile','CN':'China','CX':'Christmas Island','CC':'Cocos (Keeling) Islands','CO':'Colombia','KM':'Comoros','CD':'Congo, Democratic Republic of','CG':'Congo, Republic of','CK':'Cook Islands','CR':'Costa Rica','CI':'Cote d\'Ivoire','HR':'Croatia/Hrvatska','CU':'Cuba','CW':'Curaçao','CY':'Cyprus','CZ':'Czech Republic','DK':'Denmark','DJ':'Djibouti','DM':'Dominica','DO':'Dominican Republic','TL':'East Timor','EC':'Ecuador','EG':'Egypt','SV':'El Salvador','GQ':'Equatorial Guinea','ER':'Eritrea','EE':'Estonia','ET':'Ethiopia','FK':'Falkland Islands','FO':'Faroe Islands','FJ':'Fiji','FI':'Finland','FR':'France','GF':'French Guiana','PF':'French Polynesia','TF':'French Southern Territories','GA':'Gabon','GM':'Gambia','GE':'Georgia','DE':'Germany','GH':'Ghana','GI':'Gibraltar','GR':'Greece','GL':'Greenland','GD':'Grenada','GP':'Guadeloupe','GU':'Guam','GT':'Guatemala','GG':'Guernsey','GN':'Guinea','GW':'Guinea-Bissau','GY':'Guyana','HT':'Haiti','HM':'Heard and McDonald Islands','VA':'Holy See (City Vatican State)','HN':'Honduras','HK':'Hong Kong','HU':'Hungary','IS':'Iceland','IN':'India','ID':'Indonesia','IR':'Iran (Islamic Republic of)','IQ':'Iraq','IE':'Ireland','IM':'Isle of Man','IL':'Israel','IT':'Italy','JM':'Jamaica','JP':'Japan','JE':'Jersey','JO':'Jordan','KZ':'Kazakhstan','KE':'Kenya','KI':'Kiribati','KP':'Korea, Democratic People\'s Republic','KR':'Korea, Republic of','XK':'Kosovo','KW':'Kuwait','KG':'Kyrgyzstan','LA':'Lao People\'s Democratic Republic','LV':'Latvia','LB':'Lebanon','LS':'Lesotho','LR':'Liberia','LY':'Libya','LI':'Liechtenstein','LT':'Lithuania','LU':'Luxembourg','MO':'Macau','MK':'Macedonia','MG':'Madagascar','MW':'Malawi','MY':'Malaysia','MV':'Maldives','ML':'Mali','MT':'Malta','MH':'Marshall Islands','MQ':'Martinique','MR':'Mauritania','MU':'Mauritius','YT':'Mayotte','MX':'Mexico','FM':'Micronesia, Federal State of','MD':'Moldova, Republic of','MC':'Monaco','MN':'Mongolia','ME':'Montenegro','MS':'Montserrat','MA':'Morocco','MZ':'Mozambique','MM':'Myanmar (Burma)','NA':'Namibia','NR':'Nauru','NP':'Nepal','NL':'Netherlands','AN':'Netherlands Antilles (Deprecated)','NC':'New Caledonia','NZ':'New Zealand','NI':'Nicaragua','NE':'Niger','NG':'Nigeria','NU':'Niue','NF':'Norfolk Island','MP':'Northern Mariana Islands','NO':'Norway','OM':'Oman','PK':'Pakistan','PW':'Palau','PS':'Palestinian Territories','PA':'Panama','PG':'Papua New Guinea','PY':'Paraguay','PE':'Peru','PH':'Philippines','PN':'Pitcairn Island','PL':'Poland','PT':'Portugal','PR':'Puerto Rico','QA':'Qatar','RE':'Reunion Island','RO':'Romania','RU':'Russian Federation','RW':'Rwanda','BL':'Saint Barthélemy','SH':'Saint Helena','KN':'Saint Kitts and Nevis','LC':'Saint Lucia','MF':'Saint Martin','VC':'Saint Vincent and the Grenadines','WS':'Samoa','SM':'San Marino','ST':'Sao Tome and Principe','SA':'Saudi Arabia','SN':'Senegal','RS':'Serbia','CS':'Serbia and Montenegro (Deprecated)','SC':'Seychelles','SL':'Sierra Leone','SG':'Singapore','SX':'Sint Maarten','SK':'Slovak Republic','SI':'Slovenia','SB':'Solomon Islands','SO':'Somalia','ZA':'South Africa','GS':'South Georgia','SS':'South Sudan','ES':'Spain','LK':'Sri Lanka','PM':'St. Pierre and Miquelon','SD':'Sudan','SR':'Suriname','SJ':'Svalbard and Jan Mayen Islands','SZ':'Swaziland','SE':'Sweden','CH':'Switzerland','SY':'Syrian Arab Republic','TW':'Taiwan','TJ':'Tajikistan','TZ':'Tanzania','TH':'Thailand','TG':'Togo','TK':'Tokelau','TO':'Tonga','TT':'Trinidad and Tobago','TN':'Tunisia','TR':'Turkey','TM':'Turkmenistan','TC':'Turks and Caicos Islands','TV':'Tuvalu','UM':'US Minor Outlying Islands','UG':'Uganda','UA':'Ukraine','AE':'United Arab Emirates','GB':'United Kingdom (GB)','US':'United States','UY':'Uruguay','UZ':'Uzbekistan','VU':'Vanuatu','VE':'Venezuela','VN':'Vietnam','VG':'Virgin Islands (British)','VI':'Virgin Islands (USA)','WF':'Wallis and Futuna','EH':'Western Sahara','YE':'Yemen','ZM':'Zambia','ZW':'Zimbabwe'};
	return this.getLocationCode(text, countries)
}


Account.prototype.getUsStateCode = function (text)
{
	var us_states = {'AL':'Alabama','AK':'Alaska','AZ':'Arizona','AR':'Arkansas','AE':'Armed Forces Europe','AP':'Armed Forces Pacific','CA':'California','CO':'Colorado','CT':'Connecticut','DE':'Delaware','DC':'District of Columbia','FL':'Florida','GA':'Georgia','HI':'Hawaii','ID':'Idaho','IL':'Illinois','IN':'Indiana','IA':'Iowa','KS':'Kansas','KY':'Kentucky','LA':'Louisiana','ME':'Maine','MD':'Maryland','MA':'Massachusetts','MI':'Michigan','MN':'Minnesota','MS':'Mississippi','MO':'Missouri','MT':'Montana','NE':'Nebraska','NV':'Nevada','NH':'New Hampshire','NJ':'New Jersey','NM':'New Mexico','NY':'New York','NC':'North Carolina','ND':'North Dakota','OH':'Ohio','OK':'Oklahoma','OR':'Oregon','PA':'Pennsylvania','PR':'Puerto Rico','RI':'Rhode Island','SC':'South Carolina','SD':'South Dakota','TN':'Tennessee','TX':'Texas','UT':'Utah','VT':'Vermont','VA':'Virginia','WA':'Washington','WV':'West Virginia','WI':'Wisconsin','WY':'Wyoming'}
	return this.getLocationCode(text, us_states);
}


Account.prototype.url = function(rel_path)
{
	var url = this.baseUrl;

	url += rel_path.replace(/#[^\/]+$/, '');
	url += (/\?/.test(rel_path))? '&' : '?';

	url += 'c=' + this.accountNumber + '&n=' + this.siteNumber;

	//console.log(url);

	return url;
}


Account.prototype.login = function(params, cb)
{
	var self = this;
	var done = cb;
	var login_service_url = self.url('/services/Account.Login.Service.ss');

	var options = {
			uri: login_service_url
		, method: 'POST'	
		, json: {
			"email": params.email || params.user || ""
		,	"password": params.password || ""
		,	"redirect": params.redirect || "true"
		}
	};

	self.request(options, function (error, response, body) {
		var login_success = false;

		if (!error && response.statusCode == 200) {

			if (body && body.errorStatusCode) 
			{
				console.log(login_service_url);
				console.log(body);
				login_success = false;
			}
			else
			{
				login_success = true;
				console.log("Login con exito");
			}			
		}

		done(null, login_success);
	});
}


Account.prototype.createCreditCard = function (params, cb) 
{
		var self = this;
		var done = cb;

		var options = {
				uri: self.url('/services/CreditCard.Service.ss')
			, method: 'POST'
			, json: {
				"expmonth": params.expmonth || ''
			,	"expyear": params.expyear || ''
			,	"paymentmethod": params.paymentmethod || "8"
			,	"ccnumber": params.ccnumber || ''
			,	"ccname": params.ccname || ''
			,	"ccdefault": (params.ccdefault)? "T" : "F"
			}
		};

		self.request(options, function (error, response, body) {
			if (!error && response.statusCode == 200)
			{
				//console.log(body) // Print the shortened url.
				console.log("CreditCard created");

				if (done) done(error, true);
			}
		});
}


Account.prototype.createAddress = function(address, cb) 
{
	var self = this;

	var country = address.country || '';
	var state = address.state || '';

	if (country)
	{
		country = this.getCountryCode(country);

		if (country === 'US' && state)
		{
			state = this.getUsStateCode(state);
		}
	}
	
	var options = {
		uri: self.url('/services/Address.Service.ss'),
		method: 'POST',
		json: {
			"fullname": address.fullname || "",
			"company": address.company || "",
			"addr1": address.addr1 || "",
			"addr2": address.addr2 || "",
			"city": address.city || "",
			"country": country || "",
			"state": state || "",
			"zip": address.zip || "",
			"phone": address.phone || "",
			"defaultshipping": (address.defaultshipping)? "T" : "F",
			"isresidential": "F",
			"defaultbilling": (address.defaultbilling)? "T" : "F"
		}
	};

	self.request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			//console.log(body) // Print the shortened url.
			console.log("Address created");

			if (cb)
			{
				cb(error, true);
			}
			
		}
	});
}


Account.prototype.createAccount = function(params, cb) {
	var self = this;
	var done = cb;

	var options = {
		uri: self.url('/services/Account.Register.Service.ss')
		, method: 'POST'
		, json: {
				"firstname": params.firstname || "Tom"
			,	"lastname": params.firstname || "Mason"
			,	"company": params.company || ""
			,	"email": params.email || ""
			,	"password" : params.password || ""
			,	"password2": params.password || ""
			,	"redirect":"true"
		}
	};
	
	self.request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Account created");
			done(error, response, body);
		}
	});
}

var exports = module.exports = function(params)
{
	return new Account(params);
}

