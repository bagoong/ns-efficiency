<html>
	<head>
		<style>
			html { font-family: sans-serif; }

			body { margin: 0; }

			h2 { margin-top: 40px; }

			table {
				border-spacing: 0;
				border-collapse: collapse;
			}

			td, th { padding: 0; }

			.text-left { text-align: left; }

			.container {
				padding-right: 15px;
				padding-left: 15px;
				margin-right: auto;
				margin-left: auto; 
			}

			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
			}
			.table > thead > tr > th,
			.table > tbody > tr > th,
			.table > tfoot > tr > th,
			.table > thead > tr > td,
			.table > tbody > tr > td,
			.table > tfoot > tr > td {
				padding: 4px 8px;
				line-height: 1.42857143;
				vertical-align: top;
				border-top: 1px solid #ddd;
			}
			.table > thead > tr > th {
				vertical-align: bottom;
				border-bottom: 2px solid #ddd;
			}
			.table > caption + thead > tr:first-child > th,
			.table > colgroup + thead > tr:first-child > th,
			.table > thead:first-child > tr:first-child > th,
			.table > caption + thead > tr:first-child > td,
			.table > colgroup + thead > tr:first-child > td,
			.table > thead:first-child > tr:first-child > td {
				border-top: 0;
			}
			.table > tbody + tbody {
				border-top: 2px solid #ddd;
			}
			.table .table {
				background-color: #fff;
			}

			.table-bordered {
				border: 2px solid #ddd;
			}
			.table-bordered > thead > tr > th,
			.table-bordered > tbody > tr > th,
			.table-bordered > tfoot > tr > th,
			.table-bordered > thead > tr > td,
			.table-bordered > tbody > tr > td,
			.table-bordered > tfoot > tr > td {
				border: 1px solid #ddd;
			}
			.table-bordered > thead > tr > th,
			.table-bordered > thead > tr > td {
			 	border-bottom-width: 2px;
			}

			.summary-box { width: 20%; }

			.test-case-head { font-weight: normal; text-align: left; }

			.table-cell-property-name { width: 15%; background: #F3F3F3; }

			.bg-passed, .bg-success { background-color: #2cbc00; }
			.table-passed, .table-success { border-color: #2cbc00; }
			.table-passed th, .table-success th { color: white; background-color: #2cbc00; }

			.bg-failed, .bg-error { background-color: #D80A00; }
			.table-failed, .table-error { border-color: #D80A00; }
			.table-failed th, .table-error th { color: white; background-color: #D80A00; }

			.bg-skipped, .bg-pending { background-color: #FFD250; }
			.table-skipped, .table-pending { border-color: #FFD250; }
			.table-skipped th, .table-pending th { background-color: #FFD250; }

			.expect-block { margin-bottom: 20px; }

			.expect-block:last-child { margin-bottom: 0px; }

			.expect-block-details {
				font-size: 0.8em;
				white-space: pre-wrap;
				display: none;
				border: 1px dashed #D6B973;
				background: #FFF9C9;
				padding: 12px;
				margin-top: 10px;
				margin-bottom: 30px;
			}
		</style>

		<script>
			var toggleExpectDetails = function(el)
			{
				var expectDetails = el.parentElement.querySelector(".expect-block-details")

				if (expectDetails.style.display === "block") {
					el.textContent = el.textContent.replace("hide", "view");
					expectDetails.style.display = "none";
				}
				else
				{
					el.textContent = el.textContent.replace("view", "hide");
					expectDetails.style.display = "block";
				}
			}
		</script>
	</head>

	<body>
		<div class="container">
			<h1>Automation Execution Report</h1>


			<div class="summary-box">
				<table class="table table-bordered">
					{{#if suite.name}}
					<tr>
						<td class="table-cell-property-name">Suite:</td>
						<td>{{suite.name}}</td>
					</tr>
					{{/if}}
					<tr>
						<td class="table-cell-property-name">Sources:</td>
						<td>
							{{#each suite.sourceGlobs}}
								{{this}}<br>
							{{/each}}
						</td>
					</tr>
				</table>
			</div>
			
			<h2>Summary</h2>

			<div class="summary-box">
				<table class="table table-bordered">
					{{#each summary}}
					<tr>
						<td class="table-cell-property-name">{{@key}}:</td>
						<td>{{this}}</td>
					</tr>
					{{/each}}
				</table>
			</div>
			
			<h2>Test Cases Overview</h2>

			{{#each specs}}
			<table class="table table-bordered table-{{status}}">
				<tr>
					<th colspan="2" class="test-case-head">
						{{description}}
					</th>
				<tr>

				{{#if location}}
				<tr>
					<td class="table-cell-property-name">Location:</td>
					<td>{{location}}</td>
				</tr>
				{{/if}}

				{{#if host}}
				<tr>
					<td class="table-cell-property-name">Host:</td>
					<td>{{host}}</td>
				</tr>
				{{/if}}

				<tr>
					<td class="table-cell-property-name">Browser:</td>
					<td>{{browser}}</td>
				</tr>

				<tr>
					<td class="table-cell-property-name">Resolution:</td>
					<td>{{resolution}}</td>
				</tr>

				<tr>
					<td class="table-cell-property-name">Status:</td>
					<td>{{status}}</td>
				</tr>

				{{#if pendingReason}}
				<tr>
					<td class="table-cell-property-name">Pending reason:</td>
					<td>{{pendingReason}}</td>
				</tr>
				{{/if}}

				<tr>
					<td class="table-cell-property-name">Duration:</td>
					<td>{{duration}}</td>
				</tr>

				<tr>
					<td class="table-cell-property-name">Executed at:</td>
					<td>{{formatDate executedAt}}</td>
				</tr>

				{{#if finishedAt}}
				<tr>
					<td class="table-cell-property-name">Finished at:</td>
					<td>{{formatDate finishedAt}}</td>
				</tr>
				{{/if}}

				{{#if failedExpects }}
				<tr>
					<td class="table-cell-property-name">Failed expects:</td>
					<td>
						<div style="overflow-x: auto;">
						{{#each failedExpects}}
							<div class="expect-block">
								{{nl2br this.message}}
								<a href="javascript:;" onclick="toggleExpectDetails(this);">[view details]</a>
								<br>
								<div class="expect-block-details">{{json this}}</div>
							</div>
						{{/each}}
						</div>
					</td>
				{{/if}}
			</table>
			{{/each}}
			
		</div>

	</body>
</html>