var fs = require('fs');


module.exports = {

	readJsonFileSync: function(filePath)
	{
		if (this.fileExistsSync(filePath))
		{
			return JSON.parse(
				fs.readFileSync(filePath)
			);
		}

		return null;
	}

,	fileExistsSync: function(filePath)
	{
		try
		{ 
			fs.accessSync(filePath);
			return true;
		} 
		catch (e) 
		{
			if (e.code === "ENOENT")
			{
				return false;
			}
			else
			{
				throw e;
			}
		}
	}
}