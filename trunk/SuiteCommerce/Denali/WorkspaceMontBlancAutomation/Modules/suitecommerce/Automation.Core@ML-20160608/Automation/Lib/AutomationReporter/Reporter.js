
var	path = require('path')
,	fs = require('fs')
,	_ = require('underscore')
,	fsUtil = require('../FsUtil')
,	HtmlOutput = require('./HtmlOutput')
,	JsonOutput = require('./JsonOutput')
;


var AutomationReporter = function(params)
{
	var self = this;


	var initialize = function()
	{
		self.outputDirPath  = params.savePath || params.outputDir || '.';
		self.outputFileName = params.filePrefix || params.fileName || params.filename || 'default';
		self.outputFilePath = path.join(self.outputDirPath, self.outputFileName);

		if (params.statusFilePath)
		{
			self.statusDirPath  = path.dirname(params.statusFilePath);
			self.statusFilePath = params.statusFilePath;
		}
		else
		{
			self.statusDirPath  = params.statusPath || params.statusDir || self.outputDirPath;
			self.statusFilePath = path.join(self.statusDirPath, 'status.' + process.pid + '.json')
		}

		initializeStatus();

		initializeOutputs();
	}


	var initializeStatus = function ()
	{
		if (fsUtil.fileExistsSync(self.statusFilePath))
		{
			self.results = fsUtil.readJsonFileSync(
				self.statusFilePath
			);
		}
		else
		{
			self.suite = params.suite || {};

			var suiteInfo = _.pick(
				self.suite
			,	['name', 'browser', 'resolution', 'sourceGlobs']
			);

			self.results = {
				suite: suiteInfo
			,	summary: {
					success : 0
				,	failed  : 0
				,	skipped : 0
				,	disabled: 0
				,	enqueued: 0
				}
			,	specs: []
			};
		}
	}


	var initializeOutputs = function ()
	{
		self.outputFormats = ['html', 'json'];

		var formats = params.format || params.formats;

		if (_.isString(formats))
		{
			self.outputFormats = formats.split(",");
		}
		else if (Array.isArray(formats))
		{
			self.outputFormats = formats;
		}

		var defaultOutputHandlers = {
			'html': HtmlOutput
		,	'json': JsonOutput
		};

		var outputHandlers = _.extend(
			defaultOutputHandlers, params.formatHandlers || params.outputHandlers || {}
		);

		self.output = {};

		self.outputFormats.forEach(function(formatKey)
		{
			var formatHandler = outputHandlers[formatKey];

			if (formatHandler)
			{
				self.output[formatKey] = new formatHandler(self.outputFilePath);
			}
		})
	}


	self.getOutputFiles = function()
	{
		var filePaths = [];

		_.each(self.output, function(outputHandler)
		{
			if (outputHandler.outputFilePath)
			{
				filePaths.push(outputHandler.outputFilePath);
			}
		});

		return filePaths;
	}


	self.saveResults = function()
	{
		// DUMP STATUS FILE
		fs.writeFileSync(
			self.statusFilePath, JSON.stringify(self.results, null, 4)
		);

		// DUMP RESULTS REPORTING
		Object.keys(self.output).forEach(function(outputFormat)
		{
			self.output[outputFormat].saveResults(
				self.results
			);
		});
	}


	self.updateSummary = function()
	{
		var counters = {
			success : 0
		,	failed  : 0
		,	skipped : 0
		,	disabled: 0
		,	enqueued: 0
		,	total: 0
		}

		self.results.specs.forEach(function(specData)
		{
			switch (specData.status)
			{
				case 'pending':
					counters.skipped++ ; break;
				case 'passed':
					counters.success++ ; break;
				case 'failed':
					counters.failed++; break;
			}
		});
	
		var checkedCount = counters.success + counters.failed + counters.skipped + counters.disabled; 

		counters.total = self.results.summary.total ? self.results.summary.total : checkedCount;
		counters.enqueued = counters.total - checkedCount;
		_.extend(self.results.summary, counters);
	}


	self.jasmineStarted = function(suiteInfo)
	{
		//console.log("JASMINE STARTED")
		//console.log(suiteInfo);
		self.results.specs.forEach(function(specData)
		{
			specData.specDone = false;
		});

		self.results.summary.total = suiteInfo.totalSpecsDefined;
		self.updateSummary();
		self.saveResults();
	}


	self.suiteStarted = function(result)
	{
		//console.log("SUITE STARTED")
		//console.log(result);
	}


	self.getSpecEntry = function(specFullName)
	{
		return _.find(self.results.specs, function(specEntry)
		{
			return !specEntry.specDone && (specEntry.description === specFullName);
		});
	}


	self.specStarted = function(specData)
	{
		var previousExecution = self.getSpecEntry(specData.fullName);

		if (previousExecution && previousExecution.status !== 'failed')
		{
			return false;
		}

		var testEntry = {
			description: specData.fullName
		,	location: null
		,	host: global.WEBDRIVER_HOST
		,	browser: global.WEBDRIVER_BROWSER
		,	resolution: 'desktop'
		,	status: 'enqueued'
		,	duration: '0 secs'
		,	executedAt: new Date().toISOString()
		,	finishedAt: null
		}

		
		var specLocations = _.chain(global)
			.result('AutomationEnvironment')
			.result('specLocations')
			.value() || [];


		var specLocation = _.find(specLocations, function(specLocationEntry)
		{
			return !specLocationEntry.specDone && (specLocationEntry.fullName === specData.fullName);
		});

		if (specLocation)
		{
			testEntry.location = specLocation.location;
		}

		if (global.WEBDRIVER_RESOLUTION)
		{
			var resolution = global.WEBDRIVER_RESOLUTION;
			testEntry.resolution = resolution.width + "x" + resolution.height;

			if (global.WEBDRIVER_RESOLUTION_KEYNAME)
			{
				testEntry.resolution = WEBDRIVER_RESOLUTION_KEYNAME + ' (' + testEntry.resolution + ')';
			}
		}

		if (previousExecution)
		{
			_.extend(previousExecution, testEntry);
			self.updateSummary();
		}
		else
		{
			self.results.specs.push(testEntry);
		}

		self.saveResults();
	}


	self.lookAndSetCustomPendings = function(specResult)
	{
		specResult.failedExpectations.forEach(function(failedExpect)
		{
			if (/@pending/gi.test(failedExpect.message))
			{
				specResult.status = 'pending';
				specResult.pendingReason = failedExpect.message.replace(/@pending\s*/ig, '');
				specResult.failedExpectations = [];
			}
		});		
	}


	self.specDone = function(result)
	{
		self.lookAndSetCustomPendings(result);

		var testEntry = self.getSpecEntry(result.fullName);
		testEntry.specDone = true;

		// if it's a previously run test with success, just quit
		if (testEntry.status === 'passed')
		{
			return false;
		}
	
		testEntry.status = result.status;
		testEntry.duration = result.duration;
		testEntry.finishedAt = new Date().toISOString();

		testEntry.failedExpects = result.failedExpectations;
		testEntry.passedExpects = result.passedExpectations;

		if (result.pendingReason)
		{
			testEntry.pendingReason = result.pendingReason;
		}

		self.updateSummary();

		self.saveResults();
	}


	self.suiteDone = function(result)
	{
		//console.log("SUITE DONE");
		//console.log(result);
	}

	self.jasmineDone = function(suiteInfo)
	{
		//console.log("JASMINE DONE");
	}

	initialize();
}



module.exports = AutomationReporter;