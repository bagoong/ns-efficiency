// LOADS CAPABILITIES.JSON

var fs = require('fs');
var path = require('path')


var capabilitiesJsonPath = path.join(path.dirname(__filename), '..', 'Data', 'capabilities.json');
var capabilitiesJson =  fs.readFileSync(capabilitiesJsonPath);

global.Capabilities = JSON.parse(capabilitiesJson);
