window.getCoverage = function()
{
  var coverage = {};

  // excludes index file
  _.chain(__coverage__)
  	.keys()
  	.filter(function(key)
  	{
  		return !/Starter.*.index.js$/.test(key);
  	})
  	.each(function(key)
  	{
  		coverage[key] = __coverage__[key];
  	});

  return coverage;
}

window.dumpCoverage = function () {
  console.log('Generating coverage dump');

  var coverage = getCoverage();

  var blob = new Blob([JSON.stringify(coverage, null, 2)], {type : 'application/json'});
  var url = URL.createObjectURL(blob);

  console.log('Generating coverage dumped');

  var element = document.createElement('a');
  element.setAttribute('href', url);
  element.setAttribute('download', 'coverage.json');

  
  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();
  
  console.log(element);

  document.body.removeChild(element);
};

function dump()
{
  var request = new XMLHttpRequest();
  request.open('POST', 'http://localhost:3000/', false);
  request.setRequestHeader('Content-type','application/json');
  request.send(JSON.stringify(getCoverage()));
}

window.onbeforeunload = dump;