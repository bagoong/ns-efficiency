// @module automation @class Client @property {Client.UtilComponent}
// @class Client.UtilComponent this components contain miscelaneus static utilities, accessible using client.util
registerComponent('util', {

	// @method generateRandomString @param {Number} length @return {String}
	generateRandomString: function(length)
	{
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		var text = '';

	    for( var i=0; i < length; i++ )
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		return text;
	}
	// @method textToFloat @param {String} text @return {Number}
,	textToFloat: function(text)
	{
		if (!text)
		{
			return null;
		}

		text = text.replace(/[^0-9\.]+/g, '');
		return parseFloat(text);
	}
,	moneyToFloat: function(text)
	{
		var siteSettings = Preconditions.Configuration.website.siteSettings || {};
		var decimalSeparator = siteSettings.decimalseparator || '.';
		var replaceRegex = new RegExp("[^\\d\\" + decimalSeparator + "]", 'g');
		text = text.replace(replaceRegex, '');
		text = parseFloat(text);
		return text;
	}
	// @method stripTags @param {String} text
,	stripTags: function(text)
	{
		return text.replace(/[\s\n\r]*<[^>]+>[\s\n\r]*/gi, "").trim();
	}

	// @method cloneData @param <T> data @return {T}
,	cloneData: function(data)
	{
		if (typeof data === 'undefined') {
			throw '\'undefined\' is not an allowed data value to clone';
		}

		return JSON.parse(JSON.stringify(data));
	}

	// @method timestamp @return {Number}
,	timestamp: function()
	{
		return new Date().getTime();
	}

});