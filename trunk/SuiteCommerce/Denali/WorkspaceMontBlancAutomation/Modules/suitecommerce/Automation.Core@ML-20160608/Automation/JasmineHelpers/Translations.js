
var _ = require("underscore");


var Translator = function() {
	this.lang = "en_US";
	this.entries= {}
	this.langFiles = {}
}


Translator.prototype.loadLanguage = function(langIdentifier)
{

}

Translator.prototype.loadLanguageFiles = function() {

}


var formatString = function(text)
{
	var args = Array.prototype.slice.call(arguments).slice(1);

	return text.replace(/\$\((\d+)\)/g, function (match, number)
	{
		return typeof args[number] !== 'undefined' ? args[number] : match;
	});
};


global.translate = function(text) {
	if (!text)
	{
		throw new Error("Translation String '" + keyString + "' not found.");
	}

	text = text.toString();
	// Turns the arguments object into an array
	var args = Array.prototype.slice.call(arguments)

	// Checks the translation table
	,	result = this.entries && this.entries[text] ? this.entries[text] : text;

	if (args.length && result)
	{
		// Mixes in inline variables
		var parameters = _.map(args.slice(1), function(param)
		{
			return _.escape(param);
		});

		parameters = [result].concat(parameters);

		result = formatString.apply(this, parameters);
	}

	return result;
}