var _ = require('underscore')
,	async = require('async');

defineHelper('Expects',
{

	textToBe: function(selector, expectedText, ignoreCase, cb)
	{
		this
			.waitForExist(selector, 5000)
			.getText(selector).then(function(text)
			{
				var expectation = ignoreCase ? text.toLowerCase() : text
				,	expected = expectedText ? expectedText.toLowerCase() : expectedText;
				expect(expectation).toBe(expected);
			})
			.call(cb);
	}
,	numericPartToBe: function(selector, whatShouldBe, cb)
	{
		this
			.waitForExist(selector, 5000)
			.getText(selector, function(err, text)
			{
				var whatIs = this.util.moneyToFloat(text);
				console.log("whatShouldBe: " + whatShouldBe);
				console.log("whatIs: " + whatIs);
				expect(whatIs).toBe(whatShouldBe);
			})
			.call(cb);
	}
,	selectorsExist: function(selectors, timeout, cb)
	{
		selectors = _.isArray(selectors) ? selectors : [selectors];
		var self = this;

		async.eachLimit(selectors, 1, function(selector, each_cb)
		{
			self
				.waitForExist(selector.value, timeout)
				.isExisting(selector.value, function (err, exists)
				{
					expect(exists).toBe(selector.shouldExist);
				})
				.call(each_cb);

		}, cb);
	}

});