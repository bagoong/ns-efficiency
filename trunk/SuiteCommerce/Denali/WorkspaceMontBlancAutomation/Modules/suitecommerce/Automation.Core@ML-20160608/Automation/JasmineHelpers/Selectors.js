// LOADS SELECTORS.JSON

var fs = require('fs');
	_ = require('underscore')
;

global.Selectors = {};

var selectorsJson = fs.readFileSync('automation/selectors.json');
var rawSelectors = JSON.parse(selectorsJson);


beforeAll(function(done)
{
	var orderedSelectors = {};

	var responsiveEntryKeys = ['tablet', 'mobile'];
	var	activeResponsiveEntryKeys = [];

	if (_.isObject(global.WEBDRIVER_RESOLUTION) && global.WEBDRIVER_RESOLUTION.width)
	{
		var resolutionWidth = parseInt(global.WEBDRIVER_RESOLUTION.width);

		if (resolutionWidth < 1024)
		{
			activeResponsiveEntryKeys.push('tablet');
		}

		if (resolutionWidth < 768)
		{
			activeResponsiveEntryKeys.push('mobile');
		}
	}

	Object.keys(rawSelectors).forEach(function (moduleName)
	{
		var baseModuleSelectors = _.omit(
			rawSelectors[moduleName], responsiveEntryKeys
		);

		var responsiveModuleSelectors = _.pick(
			rawSelectors[moduleName], activeResponsiveEntryKeys
		);

		var moduleSelectors = _.extend(
			baseModuleSelectors, responsiveModuleSelectors
		);

		Object.keys(moduleSelectors).forEach(function(section)
		{
			_.extend(moduleSelectors, moduleSelectors[section]);
			delete moduleSelectors[section]; 
		});

		var flatModuleName = moduleName.replace(/\./g, '');

		orderedSelectors[flatModuleName] = moduleSelectors;
	});

	extendSelectorsDictionary(
		global.Selectors, orderedSelectors
	);

	done();
});


var extendSelectorsDictionary = function(existingSelectorsDict, newSelectors)
{
	Object.keys(newSelectors).forEach(function(moduleName)
	{
		existingSelectorsDict[moduleName] = existingSelectorsDict[moduleName] || {};
	
		_.extend(
			existingSelectorsDict[moduleName]
		,	newSelectors[moduleName]
		);
	});
}


Selectors.extendEntry = function(entryName, newSelectors, errorOnExistingEntries)
{
	errorOnExistingEntries= _.isUndefined(errorOnExistingEntries) ? true : errorOnExistingEntries;

	if (_.isUndefined(this[entryName]))
	{
		this[entryName] = {};
	}

	var existingElements = _.intersection(
		Object.keys(this[entryName])
	,	Object.keys(newSelectors)
	);

	if (errorOnExistingEntries && existingElements.length)
	{
		var errMessage = 'Existing elements on Selectors.' + entryName;
		errMessage += ' : ' + JSON.stringify(existingElements, null, 4);
		throw new Error(errMessage);
	}

	_.extend(this[entryName], newSelectors);
};
