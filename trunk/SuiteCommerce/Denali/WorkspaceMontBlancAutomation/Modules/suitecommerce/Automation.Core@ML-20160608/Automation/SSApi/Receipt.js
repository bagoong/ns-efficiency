/*
	IMPORTANT, PLEASE INSTALL THE FOLLOWING PACKAGES THAT AREN'T IN 
	THE GULP DISTRO SETUP:
	
	npm install request
	npm install tough-cookie

*/

'use strict';

var request = require('request');
var async = require('async');


var Receipt = function(params)
{
	['siteNumber', 'siteLocation', 'accountNumber'].forEach(function(val)
	{
		if (typeof params[val] === 'undefined') {
			throw val + " param is required to initialize account requests";
		}
	});

	this.siteNumber = params.siteNumber;
	this.siteLocation = params.siteLocation;
	this.accountNumber = params.accountNumber;

	if (params.rawUrl) {
		this.baseUrl = params.rawUrl;
	} else {
		this.baseUrl = 'https://checkout.netsuite.com/c.' + this.accountNumber + '/' + this.siteLocation;
	}

	this.request = request.defaults(
	{
		jar: true
	});
}

Receipt.prototype.url = function(rel_path)
{
	var url = this.baseUrl;

	url += rel_path.replace(/#[^\/]+$/, '');
	url += (/\?/.test(rel_path))? '&' : '?';

	url += 'c=' + this.accountNumber + '&n=' + this.siteNumber;

	//console.log(url);

	return url;
}

Receipt.prototype.getReceiptList = function(params, cb) {
	var self = this;
	var done = cb;

	var options = {
		uri: self.url('/services/Receipt.Service.ss?c=' + this.accountNumber + '&n=' + this.siteNumber + '&type=' + params.type + '&status=' + params.status + '&_=1434563938203')
		//, method: 'POST'
	};
	
	self.request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("List downloaded");
			if (typeof done === 'function')
			{
				var list_parsed = JSON.parse(body);
				list_parsed.forEach(function(item_parsed){
					params.dataId.push(item_parsed.internalid);
				})		
				done(error, response, body);
			}
		}
	});
}

var exports = module.exports = function(params)
{
	return new Receipt(params);
}