
var args = require('yargs').argv
,	_ = require('underscore')
,	spawn = require('child_process').spawnSync	
,	fs = require('fs')
,	path = require('path')
;

var AutomationCoreDirPath = path.join(__dirname, '../');

var AutomationResultMerger = require(path.join(AutomationCoreDirPath, 'Lib/AutomationResult/Merger'));
var Reporter = require(path.join(AutomationCoreDirPath, 'Lib/AutomationReporter'));

var mainProcessPid = process.pid;
var mainProcessExtraArgs = process.argv.slice(2);

var automanDataDirPath = 'automation/automan';

var testRunName = args['test-run'] || args['suite'] || 'default';
var testRunDirPath = path.resolve(path.join(automanDataDirPath, testRunName));

var specFile = path.join(automanDataDirPath, mainProcessPid + '.spec.json');
var tempSpecFilePath = path.join(automanDataDirPath, mainProcessPid + '.temp.spec.json');


var merger = new AutomationResultMerger({
    testRun: testRunName
});

var mergedResultsPath = path.join(testRunDirPath, 'mergedResult.json');


var dumpResults = function()
{
	var reporter = new Reporter({
	    savePath: './automation/results'
	,   statusFilePath: mergedResultsPath
	,   fileName: testRunName + '.automan'
	});

	merger.saveResults(mergedResultsPath);
	reporter.saveResults();
}


var generateMainSpecFile = function()
{
	var dumpSpecArgs = [
		'node_modules/gulp/bin/gulp.js', 'automation'
	];

	dumpSpecArgs = dumpSpecArgs.concat(
		mainProcessExtraArgs, ['--dump-spec', specFile]
	);

	var proc = spawn(
		process.execPath, 
		dumpSpecArgs
	);

	if (!fileExistsSync(specFile))
	{
		console.log(proc.stdout.toString());
		process.exit(1);
	}
}


var executeMainSpecFile = function()
{
	var specFileData = readJsonFileSync(specFile);

	fs.writeFileSync(
		path.join(testRunDirPath, 'spec.json')
	,	JSON.stringify(specFileData, null, 4)
	);

	// init merger
	merger.mergeFilesFromDirPath(testRunDirPath);
	dumpResults();

	//console.log(specFileData);
	var currentTestCase = 0;

	specFileData.spec_files.forEach(function(testCaseFilePath)
	{
		//console.log(testCaseFilePath);
		// do a deep clone
		var childSpecData = JSON.parse(JSON.stringify(specFileData));

		childSpecData.spec_files = [testCaseFilePath];

		childSpecData.suite.testCases = childSpecData.suite.testCases.filter(function(entry)
		{
			return entry.filePath === testCaseFilePath; 
		})

		var testCaseEntry = childSpecData.suite.testCases[0];

		var retries = parseInt(args.retries || args.retry || args.loop) || 1;

		for (var i=0; i < retries; i++)
		{
			console.log("****************************************************************");
			console.log(" " + testCaseEntry.location);

			if (retries > 0)
			{
				console.log(" Executing run " + (i+1) + " of " + retries + "" );	
			}

			console.log("****************************************************************\n");

			executeChildSpecFile(childSpecData);
		}
	});
}


var executeChildSpecFile = function(childSpecData)
{
	var testCaseEntry = childSpecData.suite.testCases[0];

	//console.log(childSpecData.suite.testCases);
	fs.writeFileSync(tempSpecFilePath, JSON.stringify(childSpecData, null, 4));

	var automationCorePath = getAutomationCorePath();
	var jasmineBinPath = path.join(automationCorePath, 'Automation/Bin/jasmine');

	var nodePath = process.execPath;

	var subProcessExtraArgs = _.clone(mainProcessExtraArgs);

	var jasmineProcessArgs = [jasmineBinPath].concat(
		[tempSpecFilePath], subProcessExtraArgs
	);

	var jasmineProcess = spawn(
		nodePath
	,	jasmineProcessArgs
	,	{
			stdio: [
				0, // use parents stdin for child
				1, // use parent's stdout stream - IMPORTANT if we dont do this things like the spinner will break the automation.
				2  // fs.openSync('err.out', 'w') // direct child's stderr to a file
			]
		}
	);

	var statusFileName = args['test-run'] || jasmineProcess.pid
	var originalStatusJson = path.join('automation/execution/status.' + statusFileName + '.json');
	
	var subProcessOutputJson = path.join(
		testRunDirPath, testCaseEntry.location.replace(/\//g, '.')
	);

	subProcessOutputJson = upCountFilePath(
		subProcessOutputJson, '.json'
	);

	fs.renameSync(
		originalStatusJson, subProcessOutputJson
	);

	merger.mergeStatusData(
		readJsonFileSync(subProcessOutputJson)
	);

	dumpResults();

	fs.unlinkSync(tempSpecFilePath);
	//process.exit();
	//console.log(jasmineProcess.stdout.toString())
}


var readJsonFileSync = function(filePath)
{
	if (fileExistsSync(filePath))
	{
		return JSON.parse(
			fs.readFileSync(filePath)
		);
	}

	return null;
}


var fileExistsSync = function(filePath)
{
	try
	{ 
		fs.accessSync(filePath);
		return true;
	} 
	catch (e) 
	{
		if (e.code === "ENOENT")
		{
			return false;
		}
		else
		{
			throw e;
		}
	}
}


var getOcurrencesFromCountedFilePath = function(filePath, extension)
{
	var fileBaseName = path.basename(filePath, extension);
	var fileRegexp = new RegExp(fileBaseName + '\\.(\\d+)');

	var fileDirPath = path.dirname(filePath);

	var maxCount = 0;
	var ocurrences = [];

	fs.readdirSync(fileDirPath).forEach(function(fname)
	{
		var match = fileRegexp.exec(fname);

		if (match && parseInt(match[1]) > maxCount)
		{
			maxCount = parseInt(match[1]);
			ocurrences.push(
				path.join(fileDirPath, fname)
			)
		}
	});

	return {
		dirPath: fileDirPath
	,	baseName: fileBaseName
	,	maxCount: maxCount
	,	occurrences: ocurrences
	}
}


var upCountFilePath = function(filePath, extension)
{
	var ocurrencesData = getOcurrencesFromCountedFilePath(filePath, extension);

	var upCount = ocurrencesData.maxCount + 1;

	var upCountedPath = path.join(
		ocurrencesData.dirPath
	,	ocurrencesData.baseName + '.' + upCount + extension
	);

	return upCountedPath;
}


var initDirectories = function()
{
	if (!fileExistsSync(automanDataDirPath))
	{
		fs.mkdirSync(automanDataDirPath);
	}

	if (!fileExistsSync(testRunDirPath))
	{
		fs.mkdirSync(testRunDirPath);
	}
}


var getAutomationCorePath = function()
{
	var automationCorePath = null;

	var searchDirs = [
		'./Modules/suitecommerce/'
	];

	searchDirs.forEach(function(dirPath)
	{
		var matchingDir = _.find(fs.readdirSync(dirPath), function(dirName)
		{
			return /Automation\.?Core/i.test(dirName);
		});

		if (matchingDir)
		{
			automationCorePath = path.resolve(
				path.join(dirPath, matchingDir)
			);
		}
	});

	return automationCorePath;
}

var cleanFiles = function()
{
	var pathsToDelete = [
	//	specFile
		tempSpecFilePath
	]

	pathsToDelete.forEach(function(garbagePath)
	{
		if (fileExistsSync(garbagePath))
		{
			fs.unlinkSync(garbagePath);
		}
	});
}

//do something when app is closing
process.on('exit', cleanFiles);

//catches ctrl+c event
process.on('SIGINT', cleanFiles);

//catches uncaught exceptions
process.on('uncaughtException', function(err)
{
	throw err;
	cleanFiles();	
});


var main = function()
{
	initDirectories();
	generateMainSpecFile();
	executeMainSpecFile();
}

// FINALLY RUN THE MAIN FUNCTION
main();