var args = require('yargs').argv
,	Jasmine = require('jasmine')
,	fs = require('fs')
,	path = require('path')
,	_ = require("underscore")
;

var configFilePath = 'spec/support/jasmine.json';

if (args._.length)
{
	configFilePath = args._[0];
}

try 
{
	fs.statSync(configFilePath, fs.F_OK);
}
catch (e)
{
	console.log("\nFile not found: " + configFilePath);
	console.log("Specify create the file or pass another as first argument\n");
	process.exit(1);
}

var jasmine = new Jasmine();

jasmine.loadConfigFile(
	configFilePath
);

// LOAD CONFIGURATION INTO A global
var configData = JSON.parse(fs.readFileSync(configFilePath));
global.AutomationEnvironment = configData;


jasmine.configureDefaultReporter({
	// WE INITIALIZE THE DEFAULT REPORTER AS EMPTY, SO WE CAN ADD ANOTHER REPORTER LATER
	print: function() {}
});

jasmine.execute();