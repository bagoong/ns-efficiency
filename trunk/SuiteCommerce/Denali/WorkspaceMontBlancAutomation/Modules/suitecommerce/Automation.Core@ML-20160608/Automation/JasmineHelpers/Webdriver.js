var webdriverio = require('webdriverio')
,	inquirer = require('inquirer')
,	_ = require('underscore')
,	args = require('yargs').argv
,	async = require('async')
,	fs = require('fs')
;

process.setMaxListeners(50);


function isSingleInstanceBrowser()
{
	return global.SINGLE_INSTANCE_BROWSER ||
		_.chain(Preconditions)
		.result('Configuration')
		.result('globals')
		.result('SINGLE_INSTANCE_BROWSER')
		.value();
}

 // INITIALIZE BROWSER AND LOAD HELPERS

beforeAll(function(done)
{
	if (isSingleInstanceBrowser())
	{
		initBrowser.call(this, done);
	}
	else
	{
		done();
	}
})


beforeEach(function(done) {
	var self = this;

	var preventBrowserStart = global.IGNORE_DEFAULT_BROWSER || isSingleInstanceBrowser();
	preventBrowserStart = preventBrowserStart || !global.isCurrentSpecExecutable();

	if (preventBrowserStart)
	{
		done();
	}
	else
	{
		initBrowser.call(this, function()
		{
			self.client
				// THIS ALLOWS TO PRELOAD THE SITE AND PREVENT NON PRIMARY DOMAIN ISSUES LIKE 
				// BROKEN TOUCHPOINTS ERRORS (WHITE SCREEN) WHEN GOING FROM CART TO CHECKOUT
				.url(Preconditions.Configuration.website.baseUrl)
				.waitForAjax()
				.call(done)
			;
		});
	}
});


afterEach(function(done) {
	var noBrowserStarted = !this.client;

	if (noBrowserStarted || global.IGNORE_DEFAULT_BROWSER
		|| global.JS_DISABLED_MODE || isSingleInstanceBrowser()
	)
	{
		done();
	}
	else
	{
		this.client
			.call(function()
			{
				if(args.coverage)
				{
					client.execute(function()
					{
						return getCoverage()
					}).then(function (ret)
					{
						fs.writeFileSync("automation/dumpCoverage/dumpCoverage"+(new Date()).getTime()+".json",JSON.stringify(ret.value, null, 4))
					});
				}
			})

			// Function added so the browser properly closes after the execution of a test.
			// This is needed for Istanbul to capture the last domain results
			.execute(
				function ()
				{
					if (window && window.onbeforeunload)
					{
						window.onbeforeunload();
					}
				}
			,	function (err, ret)
				{

				}
			)
			.end(done)
		;
	}
});

afterAll(function(done) {
	if (isSingleInstanceBrowser())
	{
		this.client.end(done);
	}
	else
	{
		done();
	}
});



var initBrowser = function (done)
{
	var jasmine_context = this;

	var automationSettings = initAutomationSettings();

	console.log("  Browser: " + automationSettings.browser);

	global.getBrowserInstall(automationSettings.browser)(jasmine_context, function()
	{
		var client = jasmine_context.client;

		if (typeof client.JS_DISABLED_MODE === 'undefined')
		{
			client.JS_DISABLED_MODE = false;
		}

		// INSTALL THE BASIC COMPONENTS
		installComponents(client, "util");

		// INSTALL FIRST LEVEL, FOR GENERAL USE CORE COMMANDS (like WaitForAjax)
		installCoreCommands(client, "generic", "debug", "utilities");

		//OVERRIDES
		installWebDriverOverrides(client);

		// INSTALL ALL HELPERS
		var install_helpers_args = [client].concat(Object.keys(global.HelpersRegistry));
		installHelpers.apply(client, install_helpers_args);


		client.automationSettings = automationSettings;

		client.automationTrace = {
			lastEvent: null
		,	events: []
		};

		jasmine_context.client.on('command', function(e)
		{
			var formattedEvent = webdriverIOFormatEvent(e);

			if (client.automationSettings.enableVerbose)
			{
				webdriverIODumpEvent(client, formattedEvent);
			}

			else if (client.automationSettings.enableDebug)
			{
				webdriverIODebug(client, formattedEvent);
			}

			client.automationTrace.events.push(formattedEvent);
			client.automationTrace.lastEvent = formattedEvent;
		});


		jasmine_context.client.on('error', function(e) {

			var errorClassName = e.body.value.class + '';

			if (client.isEnded || /NoAlertPresentException/.test(errorClassName))
			{
				return false;
			}

			if (!/NoSuchWindowException|WebDriverException/.test(errorClassName))
			{
				console.log(errorClassName);
				console.log(e.body.value.message);
			}

			//expectAndEnd("WebdriverIO").toBe(" without errors");

			client.isEnded = true;
			client.endTest();
			//process.exit(1);
		});


		done();
	});
}



var initAutomationSettings = function()
{
	var settings = {
		browser: "chrome"
	,	enableDebug : false
	,	enableVerbose: false
	}

	if (args) {
		settings.browser = global.WEBDRIVER_BROWSER || args.browser || settings.browser;
		settings.enableDebug = args.debug || args.wait || args.await;
		settings.enableVerbose = args.verbose;
	}

	return settings;
}


var webdriverIOFormatEvent = function(e)
{
	//console.log(e);

	e = JSON.parse(JSON.stringify(e));

	var command_match = e.uri.path.match(/\/([a-z_\-]+)$/);

	if (command_match) {
		e.data.command = command_match[1];
	}

	if ("script" in e.data)
	{
		if (~e.data.script.indexOf("jQuery.active")) {
			e.data = { command: "waitForAjax" };

		} else if (~e.data.script.indexOf("return window.scrollTo")) {
			e.data = { command: "scroll", args: e.data.args };
		}
	}

	return e;
}


var webdriverIODumpEvent = function(client, e)
{
	if (!_.isEqual(e.data, {}) && !_.isEqual(e, client.automationTrace.lastEvent))
	{
		if (Object.keys(e.data).length === 2 && "value" in e.data && Array.isArray(e.data.value))
		{
			e.data.value = e.data.value.join("");
		}

		var data_string = JSON.stringify(e.data, null, 4);

		console.log("\n" + data_string);
	}
}


var webdriverIODebug = function(client, e)
{
	webdriverIODumpEvent(client, e);

	if (client.automationPrompt)
	{
		return false;
	}

	inquirer.prompt(
		{
				type: 'input'
			,	name: 'user_input'
			,	message: '>'
		}
	,	function(answer)
		{
			//console.log(answer.user_input);
			if (answer.user_input.length > 2)
			{
				//console.log(answer.user_input);
				eval(answer.user_input);
			}

			client.automationPrompt = false;
		}
	);

	client.automationPrompt = true;

	async.until(
		function()
		{
			return !client.automationPrompt;
		}
		, function(cb)
		{
			client.pause(500, cb);
		},
		function() {
			//console.log("SALIEEENDO");
		}
	);
};


beforeEach(function(done)
{
	var isSafari = global.WEBDRIVER_BROWSER === 'safari';
	var isSCA = Preconditions.Configuration.website.siteType === 'ADVANCED';
	var isSB = Preconditions.Configuration.website.siteType === 'STANDARD';

	var client = this.client;
		
	if (isSafari && isSCA)
	{
		client
			.url(Preconditions.Configuration.website.baseUrl)

			.getText('.header-profile-welcome-link-name', function(err, userName)
			{
				if (userName) { client.Header.logout(); client.waitForAjax(); }
			})

			.call(done)
		;
	}
	else if (isSafari && isSB)
	{
		var signOutSelector = "//a[contains(.,'Sign')]";

		client
			.url(Preconditions.Configuration.website.baseUrl)

			.getText(signOutSelector, function(err, signOut)
			{
				if (signOut) { client.click(signOutSelector) }
			})

			.call(done)
		;
	}
	else
	{
		done();
	}
});
