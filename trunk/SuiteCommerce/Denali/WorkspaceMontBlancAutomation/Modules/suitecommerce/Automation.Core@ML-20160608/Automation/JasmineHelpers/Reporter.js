var args = require('yargs').argv
,	path = require('path')
,	fs = require('fs')
,	_ = require('underscore')
,	JasmineReporters = require('jasmine-reporters')
,	SpecReporter = require('jasmine-spec-reporter')
,	AutomationReporter = require('../Lib/AutomationReporter')
;

var testSuite = _.chain(global)
	.result('AutomationEnvironment')
	.result('suite')
	.value() || {};


var testRunName = args['test-run'] || args["testrun"];
var resultsFileBaseName = testRunName || testSuite.name || 'default';


var wrapSpecDoneWithPendingSupport = function(reporter)
{
	reporter._specDone = reporter.specDone;

	reporter.specDone = function (spec)
	{
		var previousExecution = getCurrentSpecPreviousExecutionData();

		if (previousExecution && previousExecution.status !== 'failed')
		{
			return false;
		}

		spec.failedExpectations.forEach(function(failedExpect)
		{
			if (/@pending/gi.test(failedExpect.message))
			{
				spec.status = 'pending';
				spec.pendingReason = failedExpect.message.replace(/@pending\s*/ig, '');
				spec.failedExpectations = [];  
			}
		});

		this._specDone(spec);
	}
}


/* CLI REPORTER */

var CustomCliReporterProcessor = function(options)
{
	this.displaySuite = function (suite, log) {
		return log;
	};

	this.displaySuccessfulSpec = function (spec, log) {
		return log;
	};

	this.displayFailedSpec = function (spec, log) {
		return log;
	};

	this.displayPendingSpec = function (spec, log) {
		log += '\n      ' + spec.pendingReason;
		return log;
	};
}

var cliReporter = new SpecReporter({
	displayStacktrace: true
,	displayFailuresSummary: false
,	displayPendingSpec: true
,	colors: {
		success: 'green',
		failure: 'red',
		pending: 'yellow'
	}
,	customProcessors: [CustomCliReporterProcessor]
});

wrapSpecDoneWithPendingSupport(cliReporter);
jasmine.getEnv().addReporter(cliReporter);


/* OTHER REPORTERS LIKE XML, JSON, ETC */

var resultsOutput = args['output'] || args['output-file'] || args['report-file'];
resultsOutput = typeof resultsOutput === 'string' ? resultsOutput : ''; 


var resultsFormatMatch = resultsOutput.match(/(.+)\.(xml|json|html?)$/i);


if (resultsFormatMatch)
{
	resultsFileBaseName = resultsFormatMatch[1];
	resultsOutput = resultsFormatMatch[2];
}
else if (resultsOutput)
{
	resultsFileBaseName = resultsOutput
}


if (['xml', 'junit', 'xunit', 'teamcity'].indexOf(resultsOutput.toLowerCase()) !== -1)
{
	var xmlReporter = new JasmineReporters.JUnitXmlReporter({
		savePath: './automation/results'
	,	filePrefix: resultsFileBaseName
	});

	wrapSpecDoneWithPendingSupport(xmlReporter);
	jasmine.getEnv().addReporter(xmlReporter);
}


var automationReporter = new AutomationReporter({
	savePath: './automation/results'
,	statusFilePath: global.AutomationEnvironment.executionStatusFilePath
,	fileName: resultsFileBaseName
,	suite: testSuite
});


jasmine.getEnv().addReporter(automationReporter);


process.on('exit', function()
	{
	var outputFiles = automationReporter.getOutputFiles();

	if (!_.isEmpty(outputFiles))
	{
		console.log("");
		console.log("Results saved at:");

		outputFiles.forEach(function(outputFile)
		{
			console.log("  " + outputFile);
		});

		console.log("");
	}
})


