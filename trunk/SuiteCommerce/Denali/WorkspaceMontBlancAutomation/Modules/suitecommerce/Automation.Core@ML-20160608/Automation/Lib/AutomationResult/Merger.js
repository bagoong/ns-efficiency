var fs = require('fs')
,   path = require('path')
,	events = require('events')
,   _ = require('underscore')
;

var Merger = function(params)
{
	var self = this;

	var defaults = {
		strategy: 'BEST_RESULT'
	,	testRun: 'default'
	,	suite: null
	};

	params = _.extend(defaults, params);


	self.initialize = function()
	{
		self.testRun = params.testRun
		self.results = self.createResultsTemplate();
		self.suite = params.suite || null;
		self.emitter = new events.EventEmitter();
	}


	self.createResultsTemplate = function()
	{
		var resultsTemplate = {
			suite: {
				name: self.testRun
			,	sourceGlobs: []
			}
		,	summary: {}
		,	specs: []
		};

		return resultsTemplate;
	}


	self.mergeFilesFromDirPath = function(sourceDirPath)
	{
		//I do the comparision in every file that matches the pattern,
		//TODO: narrow down the files to parse according to the spec.json file and
		//the last execution of every spec using the counter in the file name of every status.
		files = fs.readdirSync(sourceDirPath);

		files = _.filter(files, function (file)
		{
			return /\.\d+\.json$/.test(file);
		});

		if (files.length > 0)
		{
			files.forEach(function (fileName, index)
			{
				var statusFilePath = path.join(sourceDirPath, fileName);

				var newStatusData = JSON.parse(
					fs.readFileSync(statusFilePath)
				);        
				
				self.mergeStatusData(newStatusData);
			});
		}
	}


	self.mergeStatusData = function(statusData)
	{
		self.results.suite.sourceGlobs = _.union(
			self.results.suite.sourceGlobs, statusData.suite.sourceGlobs
		);
				
		statusData.specs.forEach(function(spec, index){

			var existingSpec = _.find(self.results.specs, function(specMerge)
			{
					// console.log(JSON.stringify(specMerge, null, 4));
					return specMerge.description === spec.description  
						&& specMerge.location === spec.location
						&& spec.status; 
			})

			if (spec.status === 'enqueued')
			{
				return false;
			}

			// In case spec is not present in merged results, update status counter
			// and push spec to merged results.
			if (!existingSpec)
			{
					spec.TEST = "New spec it was not present before in merged results";
					
					self.emit('newSpecFound', spec);

					self.results.specs.push(spec);
			}
			// In case that the same spec is already in the merged result, 
			// I keep the best result or the latest.
			else
			{
				var specMergedResult = _.find(self.results.specs, function(specMerge) 
				{
					// console.log(JSON.stringify(specMerge, null, 4));
					return specMerge.description === spec.description  
						&& specMerge.location === spec.location
						&& spec.status; 
				});

				if (spec.status === "passed" 
					|| ( (spec.status === "failed") && (["failed", "passed"].indexOf(specMergedResult.status) === -1)))
				{
					_.extend(specMergedResult, spec);
					specMergedResult.TEST = "specStatus passed or, specstatus failed and specMergedResult not failed";
				}
				else if ( (spec.status === "failed")
					&& (specMergedResult.status === "failed") )
				{
					var dateMerged = new Date(specMergedResult.finishedAt);
					var dateSpec = new Date(spec.finishedAt);

					if (dateMerged.getTime() < dateSpec.getTime())
					{
						_.extend(specMergedResult, spec);
						specMergedResult.TEST = "keep last failed result";
					}
				}
			}

			self.updateSummary();
		});
	}


	self.calculateSummary = function(results)
	{
		var counters = {
			success : 0
		,   failed  : 0
		,   skipped : 0
		,   disabled: 0
		,   enqueued: 0
		,   total: results.specs.length
		}

		var counterTotal = 0;

		results.specs.forEach(function(specData)
		{
			switch (specData.status)
			{
				case 'pending':
					counters.skipped++ ; counterTotal++; break;
				case 'passed':
					counters.success++ ; counterTotal++; break;
				case 'failed':
					counters.failed++  ; counterTotal++; break;
				case 'enqueued':
					counters.enqueued++; counterTotal++; break;
				case 'disabled':
					counters.disabled++; counterTotal++; break;
				case 'skipped':
					counters.skipped++ ; counterTotal++; break;    
			}
		});
		
		if (counters.total !== counterTotal)
		{
			throw new Error(
				"Total count of specs: " + results.specs.length + " is different than total of valid statuses: " + counterTotal
			);
		}

		return counters;
	}


	self.updateSummary = function ()
	{
		self.results.summary = self.calculateSummary(self.results);
	}


	self.saveResults = function(outputFilePath)
	{
		fs.writeFileSync(
			outputFilePath
		,	JSON.stringify(self.results, null, 4)
		);
	}

	self.save = self.saveResults;


	var emitterMethods = [
		'on', 'once', 'emit'
	]

	emitterMethods.forEach(function(functionName)
	{
		self[functionName] = function()
		{
			self.emitter[functionName].apply(
				self.emitter
			,	_.toArray(arguments)
			);
		}
	});

	self.initialize();
}


module.exports = Merger;