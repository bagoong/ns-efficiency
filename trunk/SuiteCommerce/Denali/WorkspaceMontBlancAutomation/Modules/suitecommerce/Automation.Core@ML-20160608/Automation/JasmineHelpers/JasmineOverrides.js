var	_ = require('underscore')
,	args = require('yargs').argv
;

pending = null;

jasmineSpecDone = null;

// WRAP beforeAll() FUNCTION SO WE CAN SCHEDULE A pending()
// STATE FOR SUBSEQUENT SPECS INSIDE THE DESCRIBE.

var wrapJasmineBeforeFunction = function(originalFunction, options)
{
	options = options || {};

	return function()
	{
		var self = this;
		var userFunction = arguments[0];

		arguments[0] = function(done) 
		{
			var userFunctionContext = this;

			if (done)
			{
				pending = function(msg)
				{
					msg = (msg)? msg : 'Pending with undefined reason.';

					var pendingKey = options.pendingKey || 'pendingForNext';

					userFunctionContext[pendingKey] = msg;
					done();
				}
			}

			return userFunction.apply(this, arguments);
		}

		return originalFunction.apply(this, arguments);
	}
}

_beforeAll = beforeAll;
beforeAll = wrapJasmineBeforeFunction(beforeAll, { pendingKey: 'pendingForAll' });

_beforeEach = beforeEach;
beforeEach = wrapJasmineBeforeFunction(beforeEach, { pendingKey: 'pendingForNext' });


// WRAP it() AND fit() FUNCTIONS, SO WE CAN HAVE A REFERENCE TO END TEST FROM 
// INSIDE WEBDRIVERIO HELPERS

var wrapJasmineSpecFunction = function(originalFunction)
{
	return function()
	{
		var self = this;
		var testFunction = arguments[1];
		var specName = arguments[0];

		arguments[1] = function(done) 
		{
			this.specName = specName;

			var previousExecution = global.getCurrentSpecPreviousExecutionData();

			if (done)
			{
				if (previousExecution && previousExecution.status === 'passed')
				{
					return done();
				}

				if (this.client)
				{
					this.client.endTest = done;
				}
				
				jasmineSpecDone = done;
			
				pending = function(msg, pendingCallback)
				{
					msg = (msg)? msg : 'Pending with undefined reason.';
					expect().toFailWith('@pending ' + msg);
					Preconditions.clearZombieTasks();

					if (typeof pendingCallback === 'function')
					{
						pendingCallback(new Error(msg));
					}

					done();
				}

				if (previousExecution && previousExecution.status === 'pending')
				{
					return pending(previousExecution.pendingReason);
				}
			}

			if (typeof pending === 'function')
			{
				if (this.pendingForAll)
				{
					return pending(this.pendingForAll);
				}
				else if (this.pendingForNext)
				{
					var pendingMsg = this.pendingForNext;
					delete this.pendingForNext;
					return pending(pendingMsg);
				}
			}

			return testFunction.apply(this, arguments);
		}

		return originalFunction.apply(this, arguments);
	}
}

_it = it;
it = wrapJasmineSpecFunction(it);

_fit = fit;
fit = wrapJasmineSpecFunction(fit);