var fs = require('fs')
,   async = require('async')
,   path = require('path')
,   _ = require('underscore')
,   args = require('yargs').argv;


var tr = args['test-run'] || "default";

var base_path = 'automation/automan/' + tr + "/";

// jasmineSpecParser = require('../Lib/JasmineSpecParser') 14:20 
// jasmineSpecParser.extractJasmineSpecs(filePath); 


function calculateSummary (results)
    {
        var counters = results.summary = {
            success : 0
        ,   failed  : 0
        ,   skipped : 0
        ,   disabled: 0
        ,   enqueued: 0
        ,   total: results.specs.length
        }

        var counterTotal = 0;

        results.specs.forEach(function(specData)
        {
            switch (specData.status)
            {
                case 'pending':
                    counters.skipped++ ; counterTotal++; break;
                case 'passed':
                    counters.success++ ; counterTotal++; break;
                case 'failed':
                    counters.failed++; counterTotal++; break;
                case 'enqueued':
                    counters.enqueued++; counterTotal++; break;
                case 'disabled':
                    counters.disabled++; counterTotal++; break;
                case 'skipped':
                    counters.skipped++; counterTotal++; break;    
            }
            

        });
        if (results.summary.total !== counterTotal){
            console.log("Total count of specs:", results.specs.length, " is different than total of valid statuses:", counterTotal);
        }
        
    }

function mergeStatuses (suite, base_path, outputPath)
{
    var singleResult, mergedResult;

    //I do the comparision in every file that matches the pattern,
    //TODO: narrow down the files to parse according to the spec.json file and
    //the last execution of every spec using the counter in the file name of every status.
    files = fs.readdirSync(base_path);
    files = _.filter(files, function (file)
    {
        return /\.\d+\.json$/.test(file);
    });

    if (files.length > 0)
    {
        //Initialize merged results
        mergedResult = {};
        mergedResult.suite = {};
        mergedResult.suite.name = suite;
        mergedResult.summary = {};
        mergedResult.suite.sourceGlobs = [];
        mergedResult.specs = [];

        files.forEach(function (file, index)
        {
            singleResult = JSON.parse(fs.readFileSync(base_path + file));        
            mergedResult.suite.sourceGlobs = _.union(mergedResult.suite.sourceGlobs, singleResult.suite.sourceGlobs);
            
            singleResult.specs.forEach(function(spec, index){
                if (!_.find(mergedResult.specs,function(specMerge) 
                    {
                        // console.log(JSON.stringify(specMerge, null, 4));
                        return specMerge.description === spec.description  
                            && specMerge.location === spec.location
                            && spec.status; 
                    }))
                    //In case spec is not present in merged results, update status counter
                    //and push spec to merged results.
                {
                        spec.TEST = "New spec it was not present before in merged results";
                        console.log(spec.description, spec.location, spec.status);
                        mergedResult.specs.push(spec);
                    
                }
                else
                    //In case that the same spec is already in the merged result, 
                    //I keep the best result or the latest.
                {
                    var specMergedResult = _.find(mergedResult.specs,function(specMerge) 
                    {
                        // console.log(JSON.stringify(specMerge, null, 4));
                        return specMerge.description === spec.description  
                            && specMerge.location === spec.location
                            && spec.status; 
                    });
                    if (spec.status === "passed" 
                    || (spec.status === "failed" && specMergedResult.status !== "failed"))
                    {
                        specMergedResult = spec;
                        specMergedResult.TEST = "specStatus passed or, specstatus failed and specMergedResult not failed";
                    }
                    else if (spec.status === "failed" 
                         && specMergedResult.status === "failed")
                    {
                        var dateMerged = new Date(specMergedResult.finishedAt);
                        var dateSpec = new Date(spec.finishedAt);
                        if (dateMerged < dateSpec){
                            specMergedResult = spec;

                            specMergedResult.TEST = "keep last failed result";
                        }
                    }
                }
                calculateSummary(mergedResult);
            });
        });
    }
    fs.writeFileSync(base_path + outputPath, JSON.stringify(mergedResult, null, 4));
}
mergeStatuses(tr, base_path, "mergedResult.json");