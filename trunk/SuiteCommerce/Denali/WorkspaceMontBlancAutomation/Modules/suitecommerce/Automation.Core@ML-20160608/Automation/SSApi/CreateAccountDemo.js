'use strict';

var request = require('request');
var async = require('async');
var account = require('./Account');


var test_dataset = {
	
	'site': {
		'baseUrl': 'http://chksf.crovetto.me/'
	,	'accountNumber': '3946236'	
	,	'siteNumber': '2'
	,	'siteLocation': 'DB96-UYQA-Automation-3-3946236-PSG_QA/mvdsite'
	}	

,	'customer': {
			'email': 'standardflow_with_defaults@automationbacon.com'  
		,	'password': 'automation'
	}

,	'defaultShippingAddress': {
			'fullname': 'My Shipping Name'
		,	'company': 'My Shipping Company'
		,	'addr1': 'Mings Street 123'
		,	'addr2': ''
		,	'city': 'Mongo City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '12345678901234'
	}

,	'defaultBillingAddress': {
			'fullname': 'My Billing Name'
		,	'company': 'My Billing Company'
		,	'addr1': 'Mindafggs Street 123'
		,	'addr2': ''
		,	'city': 'Billing City'
		,	'country': 'United States'
		,	'state': 'New York'
		,	'zip': 'J1934'
		,	'phone': '1234901234'
	}

,	'creditCard': {
			'id': '123'
		,	'cardTypeId': '8'
		,	'cardType': 'visa'
		,	'cardNumber': '4111111111111111'
		,	'cardEnding': '1111'
		,	'expMonth': '7'
		,	'expYear': '2018'
		,	'securityNumber': '123'
		,	'name': 'Flash Gordon'
	}	
};


describe('Create account with defaults DEMO CODE', function() 
{
	// former checkoutAsReturningCustomerSCA1
	it('WITH defaults', function(done)
	{
		var client = this.client;

		installComponents(client, 'ssapi');

		var my_account = client.ssapi.account(
			test_dataset.site
		);

		test_dataset.customer.email = client.util.generateRandomString(6) + "@automationbacon.com";

		my_account.createAccount(test_dataset.customer, function()
		{
			my_account.login(
				test_dataset.customer
				, function() 
				{
					var shipping_address = client.util.cloneData(test_dataset.defaultShippingAddress);
					shipping_address.defaultshipping = true;
					my_account.createAddress(shipping_address);

					var billing_address = client.util.cloneData(test_dataset.defaultBillingAddress);
					billing_address.defaultbilling = true;
					my_account.createAddress(billing_address);

					var credit_card = test_dataset.creditCard;

					my_account.createCreditCard({
						"expmonth": credit_card.expMonth
					,	"expyear": credit_card.expYear
					,	"paymentmethod": credit_card.cardTypeId
					,	"ccnumber": credit_card.cardNumber
					,	"ccname": credit_card.name
					,	"ccdefault": true
					});

					doTest(done, client, test_dataset);
				}
			);
		}); 

		
	});
});


