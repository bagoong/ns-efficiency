var args = require('yargs').argv
,	_ = require('underscore')
,	spawn = require('child_process').spawnSync	
,	fs = require('fs')
,	path = require('path')
,	fsutil = require('../Lib/FsUtil')
,	jasmineSpecParser = require('../Lib/JasmineSpecParser')
;

var AutomationCoreDirPath = path.join(__dirname, '../');

var mainProcessPid = process.pid;
var mainProcessExtraArgs = process.argv.slice(2);


var getSpecData = function()
{
	var specFile = process.pid + ".spec.temp.json";

	var dumpSpecArgs = [
		'node_modules/gulp/bin/gulp.js', 'automation'
	];

	dumpSpecArgs = dumpSpecArgs.concat(
		mainProcessExtraArgs, ['--dump-spec', specFile]
	);

	var proc = spawn(
		process.execPath, 
		dumpSpecArgs
	);

	if (!fsutil.fileExistsSync(specFile))
	{
		console.log(proc.stdout.toString());
		process.exit(1);
	}

	var specFileData = fsutil.readJsonFileSync(specFile);

	fs.unlink(specFile);

	return specFileData;
}


var main = function()
{
	var specData = getSpecData();

	if (!specData.suite.testCases)
	{
		console.log("No spec files found for search");
		process.exit(1);
	}

	//var fileEntries = specData.
	//console.log(specData);
	var testFileEntries = specData.suite.testCases;

	//console.log(testFileEntries);

	var allSpecs = [];

	testFileEntries.forEach(function(entry)
	{
		var specsFound = jasmineSpecParser.extractJasmineSpecs(entry.filePath);
		//console.log(specsFound);
		allSpecs = allSpecs.concat(specsFound);
	});

	console.log("\nTotal ITs: " + allSpecs.length);
}


main();