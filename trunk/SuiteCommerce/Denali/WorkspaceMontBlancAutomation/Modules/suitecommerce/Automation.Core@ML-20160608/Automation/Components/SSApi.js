
registerComponent('ssapi', {

	account: function(params) {
		return require('../SSApi/Account')(params);
	}

,	checkout: function(params) {
		return require('../SSApi/Checkout')(params);
	}	

,	receipt: function(params) {
		return require('../SSApi/Receipt')(params);
	}		

});