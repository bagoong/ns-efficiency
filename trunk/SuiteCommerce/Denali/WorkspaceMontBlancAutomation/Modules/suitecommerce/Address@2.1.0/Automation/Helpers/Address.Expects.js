var _ = require('underscore');


var expectSameAddress = function (foundAddress, expectedAddress)
{
	if (!foundAddress.internalId)
	{
		expectedAddress = _.omit(expectedAddress, ['internalId']);
	}

	expect(foundAddress).toHaveEqualFields(expectedAddress);
}



defineHelper('Address.Expects', 
{

	checkExpectedAddress: function (expectedAddress, cb)
	{
		this
			.Address.getAddressDataType(function(err, foundAddress) {
				expectSameAddress(foundAddress, expectedAddress);
			})

			.call(cb)
		;
	}


,	checkExpectedAddressIsListed: function (expectedAddress, cb)
	{
		var status = false;
		this
			.Address.getAddressesDataType(function(err, foundAddresses) {
				foundAddresses.forEach(function(foundAddress){
					if(_.isEqual(foundAddress, expectedAddress))
					{
						status = true;
						return false;
					}	
				})
				expect(status).toBeTruthy();
			})

			.call(cb)
		;
	}


,	checkExpectedShippingAddress: function(expectedAddress, cb)
	{
		this
			.Address.getAddressDataType(
				{
					parentElementXPath: Selectors.Address.shippingParent
				}
				, function(err, foundAddress) {
					expectSameAddress(foundAddress, expectedAddress);
				}
			)

			.call(cb)
		;
	}


,	checkExpectedBillingAddress: function(expectedAddress, cb)
	{
		this
			.Address.getAddressDataType(
				{
					parentElementXPath: Selectors.Address.billingParent
				}
				, function(err, foundAddress) {
					expectSameAddress(foundAddress, expectedAddress);
				}
			)

			.call(cb)
		;
	}


,	checkExpectedBillingAddressFieldError: function(params, cb)
	{
		var ext_params = {
			cssPrefix: Selectors.Address.prefixBilling
		,	name: params.name
		,	errorLabel: params.errorLabel
		}
		this.Address.Expects.checkExpectedFieldError(ext_params, cb)
	}

,	checkExpectedShippingAddressFieldError: function(params, cb)
	{
		var ext_params = {
			cssPrefix: Selectors.Address.prefixShipping
		,	name: params.name
		,	errorLabel: params.errorLabel
		}
		this.Address.Expects.checkExpectedFieldError(ext_params, cb)
	}

,	checkExpectedFieldError: function(params, cb)
	{
		var css_prefix = (params.cssPrefix)? params.cssPrefix + ' ': '';

		this
			.waitForExist( css_prefix + Selectors.Address.errorBlock.replace('%s', params.name), 5000)

			.getText( css_prefix + Selectors.Address.errorBlock.replace('%s', params.name), function(err, text)
			{
				expect(text).toBe( params.errorLabel );
			})

			.call(cb)
		;
	}

,	checkAddressBookPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Address.addressBookPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Address Book page is not present.');
        	})
        	.call(cb)
        ;
	}

});

