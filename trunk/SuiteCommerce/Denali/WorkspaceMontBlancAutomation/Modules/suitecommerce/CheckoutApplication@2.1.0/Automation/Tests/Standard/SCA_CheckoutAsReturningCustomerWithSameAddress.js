// CHECKOUT AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES


describe('Checkout as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA2
	it('WITH existing addresses, WITH existing creditcard, WITH same address checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				}

				doTest(done, client, dataset);
			}
		);
	});
	
});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})
		
		.Address.Expects.checkExpectedAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.Address.markSameShippingAddress()

		// PAYMENT METHOD
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()
		
		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultShippingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}




