// CHECKOUT SKIP LOGIN AS RETURNING CUSTOMER WITH CREATE ADDRESS

describe('Checkout Skip Login as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA7
	it('Create new shipping address and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_shipping_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newShippingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newShippingAddress': newShippingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});

});



var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)
		
		.Cart.clickProceedToCheckout()
		
		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.fillLogin({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		.LoginRegister.clickSubmitLogin()
		
		// SHIPPING ADDRESS PAGE

		.OrderWizard.Module.Address.clickChangeShippingAddressIfPreviousSelected()

		.Address.removePreviousAddresses(
			dataset.newShippingAddress
		)

		.OrderWizard.Module.Address.clickAddNewShippingAddress()
		
		.Address.fillAddress(
			dataset.newShippingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickShipToThisAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)		

		.OrderWizard.clickContinue()

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


