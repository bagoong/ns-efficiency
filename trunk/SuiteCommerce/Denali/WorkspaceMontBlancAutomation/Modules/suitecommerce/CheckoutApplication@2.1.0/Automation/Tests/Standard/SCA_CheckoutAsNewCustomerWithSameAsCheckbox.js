// CHECKOUT AS NEW CUSTOMER TESTS

describe('Checkout as New Customer', function() 
{
	// former checkoutAsNewCustomerSCA1
	it('WITH same shipping-billing address checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	shippingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode	
				,	'newCustomer': newCustomer
				,	'address': shippingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function(done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()
		
        .LoginRegister.clickCreateAccount()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		.Address.fillAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.markSameShippingAddress()

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE

		.Address.Expects.checkExpectedBillingAddress(
			dataset.address
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.address
		)

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}