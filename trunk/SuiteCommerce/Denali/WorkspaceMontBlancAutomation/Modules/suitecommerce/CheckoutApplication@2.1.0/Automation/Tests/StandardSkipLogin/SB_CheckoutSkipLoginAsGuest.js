// CHECKOUT AS GUEST TESTS

describe('Site Builder - Checkout Skip Login as Guest', function() 
{
	// former checkoutAsGuestSB1
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_standard_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});

});

var doTest = function (done, client, dataset)
{
	'use strict';

	// TEST FLOW
    client
		.ItemDetails.SB.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()
		
		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		.OrderWizard.clickContinue() // POSSIBLE ISSUE HERE (SKIP LOGIN MESSAGE DISSAPEARS, 2 CLICKS TO GO)
		
		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})	

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.Module.Register.fillGuestEmail(
			dataset.guest.email
		)
		
		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


