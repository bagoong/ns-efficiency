// CHECKOUT AS GUEST TESTS

describe('Site Builder - Checkout Skip Login as Guest', function() 
{
	// former checkoutAsGuestSB2
	it('WITH promo, WITH same shipping-billing checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_standard_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'promoCode': promoCode	
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});	
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.SB.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()
		
		.OrderWizard.Expects.checkExpectedSkipLoginMessage()
		
		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		.OrderWizard.clickContinue() // POSSIBLE ISSUE HERE (SKIP LOGIN MESSAGE DISSAPEARS, 2 CLICKS TO GO)

		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})	

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.Module.Register.fillGuestEmail(
			dataset.guest.email
		)
		
		.Address.markSameShippingAddress()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


