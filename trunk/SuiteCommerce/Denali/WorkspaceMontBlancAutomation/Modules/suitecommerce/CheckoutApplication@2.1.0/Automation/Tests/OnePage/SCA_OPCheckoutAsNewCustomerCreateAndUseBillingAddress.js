// ONE PAGE CHECKOUT AS NEW CUSTOMER TESTS

describe('One Page Checkout as New Customer', function() 
{
	// former checkoutAsNewCustomerSCA3
	it('Create new billing address and USE it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_new_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	shippingAddress
			,	billingAddress
			,	newBillingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'newCustomer': newCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'newBillingAddress': newBillingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE

		.LoginRegister.clickCreateAccount()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// INFORMATION PAGE

		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)


		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.Module.Shipmethod.chooseMethod()		

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER AND GO BACK TO EDIT

		.OrderWizard.clickBack()

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.OrderWizard.Module.Address.clickAddNewBillingAddress()
		
		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// HERE WE -> USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickSelectThisAddress(
			dataset.newBillingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER FINALLY

		.Address.Expects.checkExpectedBillingAddress(
			dataset.newBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}

