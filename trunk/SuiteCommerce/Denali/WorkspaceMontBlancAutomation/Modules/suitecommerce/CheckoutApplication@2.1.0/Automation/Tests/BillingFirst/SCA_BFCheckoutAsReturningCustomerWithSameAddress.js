// BILLING FIRST CHECKOUT AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES

describe('Billing First Checkout as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA2
	it('WITH existing addresses, WITH existing creditcard, WITH same address checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				}

				//console.log(dataset);
				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.loginAs(
			dataset.customer
		)

		// RESET ADDRESS TO BILLING

		.OrderWizard.Module.Address.hasSelectedBillingAddress(function(err, has_selected)
		{
			if (has_selected) 
			{
				client.OrderWizard.Module.Address.clickChangeBillingAddress();
			}
		})

		.Address.clickSelectThisAddress(
			dataset.defaultBillingAddress
		)

		//.pause(100000)
		
		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.OrderWizard.clickContinue()

		// BILLING ADDRESS

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		//.pause(1000000)

		.OrderWizard.clickContinue()

		// DELIVERY
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT AND BILLING ADDRESS
		.Address.markSameShippingAddress()

		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)
		
		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER
		//.pause(10000000)

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultShippingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}

