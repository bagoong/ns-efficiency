// CHECKOUT LOGIN TESTS

describe('Checkout Login', function() 
{
	// validLoginTest
	it('Makes a correct login', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration'
		,	'get_one_inventory_non_matrix_item'
		,	'create_one_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	customer
			)
			{
				client
					.ItemDetails.addProducts(
						inventoryItem
					)

					.Cart.clickProceedToCheckout()
					
					.LoginRegister.loginAs(
						customer
					)

					.OrderWizard.Expects.checkExpectedOrderWizardPage()

					.call(done)
				;
			}
		);
	});

});