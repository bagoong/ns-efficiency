// CHECKOUT AS NEW CUSTOMER TESTS

describe('Checkout as New Customer', function() 
{
	// former checkoutAsNewCustomerSCA3
	it('Create new billing address and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'generate_one_shipping_address_data'
		,	'generate_one_new_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	shippingAddress
			,	newBillingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode	
				,	'newCustomer': newCustomer
				,	'shippingAddress': shippingAddress
				,	'newBillingAddress': newBillingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function(done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.addProducts(
			dataset.items
		)
		
		.Cart.clickProceedToCheckout()

        .LoginRegister.clickCreateAccount()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.Module.Address.clickAddNewAddress()
		
		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS, INSTEAD WE USE THE SAVED SHIPPING
		.Address.clickSelectThisAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}



