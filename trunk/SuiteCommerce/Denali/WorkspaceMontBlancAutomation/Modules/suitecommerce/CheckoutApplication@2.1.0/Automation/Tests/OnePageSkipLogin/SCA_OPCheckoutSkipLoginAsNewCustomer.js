// ONE PAGE CHECKOUT AS NEW CUSTOMER TESTS

describe('One Page Checkout Skip Login as New Customer', function() 
{
	// NEW TEST, not present in former checkoutAsNewCustomerSCA test suite
	it('WITH minimum and mandatory steps', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'newCustomer': newCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.clickRegisterLinkInLoginModal()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// INFORMATION PAGE

		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)

		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})			

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.billingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
