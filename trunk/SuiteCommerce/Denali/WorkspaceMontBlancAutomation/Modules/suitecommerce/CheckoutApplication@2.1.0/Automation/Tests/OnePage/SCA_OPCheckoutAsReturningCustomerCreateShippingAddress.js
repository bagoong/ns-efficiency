// ONE PAGE CHECKOUT AS RETURNING CUSTOMER TESTS

describe('One Page Checkout as Returning Customer', function() 
{
	// former checkoutAsReturningCustomerSCA7
	it('Create new shipping address and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_one_page_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_shipping_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newShippingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newShippingAddress': newShippingAddress
				}

				//console.log(dataset);
				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE

		.LoginRegister.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		// INFORMATION PAGE

		.OrderWizard.Module.Address.clickChangeShippingAddressIfPreviousSelected()

		.Address.removePreviousAddresses(
			dataset.newShippingAddress
		)

		.OrderWizard.Module.Address.clickAddNewShippingAddress()

		.Address.fillAddress(
			dataset.newShippingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickShipToThisAddress(
			dataset.defaultShippingAddress
		)

		// WAITING FOR DELIVERY METHOD ENABLED IN OPC IS REALLY SLOW!
		.OrderWizard.Module.Shipmethod.waitForDeliveryMethodEnabled()

		.OrderWizard.Module.Shipmethod.chooseMethod({
			index: 1
		})

		//.CreditCard.fillCreditCard(
		//	dataset.creditCard
		//)
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		// PREVIEW STEP (IT REDIRECTS TO REVIEW IF EVERYTHING IT'S OK)

		.waitForAjax()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

		.call(done)
	;
}



