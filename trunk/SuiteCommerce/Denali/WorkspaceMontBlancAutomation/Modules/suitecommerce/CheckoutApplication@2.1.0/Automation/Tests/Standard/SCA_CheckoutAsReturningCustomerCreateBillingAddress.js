// CHECKOUT AS RETURNING CUSTOMER WITH CREATE ADDRESS


describe('Checkout as Returning Customer', function() 
{

	// former checkoutAsReturningCustomerSCA3
	it('Create new billing address and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'
		,	'generate_one_new_billing_address_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			,	newBillingAddress
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				,	'newBillingAddress': newBillingAddress
				}

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.addProducts(
			dataset.items
		)

		//.ItemDetails.addProduct('DownloadItem')
		
		.Cart.clickProceedToCheckout()
		
		.LoginRegister.loginAs(
			dataset.customer
		)
		
		.OrderWizard.Module.Address.clickChangeShippingAddressIfPreviousSelected()

		.Address.clickShipToThisAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// BILLING ADDRESS

		.OrderWizard.Module.Address.clickChangeBillingAddressIfPreviousSelected()

		.Address.removePreviousAddresses(
			dataset.newBillingAddress
		)

		.OrderWizard.Module.Address.clickAddNewBillingAddress()
		
		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS
		.Address.clickSelectThisAddress(
			dataset.defaultBillingAddress
		)

		// PAYMENT METHOD
		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.Address.Expects.checkExpectedBillingAddress(
			dataset.defaultBillingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}




