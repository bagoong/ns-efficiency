// BILLING FIRST CHECKOUT SKIP LOGIN AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES

describe('Billing First Checkout Skip Login as New Customer', function()
{
	// former checkoutAsNewCustomerSCA1
	it('WITH same shipping-billing address checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_billing_first_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	billingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'newCustomer': newCustomer
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()
		
		// LOGIN PAGE

		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.clickRegisterLinkInLoginModal()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// BILLING ADDRESS
		
		.Address.fillAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.Address.markSameShippingAddress()

		.OrderWizard.clickContinue()

		// DELIVERY
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		// PAYMENT
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})
		
		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.billingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
