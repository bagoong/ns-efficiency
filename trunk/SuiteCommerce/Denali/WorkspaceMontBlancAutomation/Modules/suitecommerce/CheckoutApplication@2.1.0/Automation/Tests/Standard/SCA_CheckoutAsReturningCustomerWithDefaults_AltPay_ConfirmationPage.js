// CHECKOUT AS RETURNING CUSTOMER TESTS WITH EXISTING ADDRESSES


describe('Checkout as Returning Customer', function()
{
	// former checkoutAsReturningCustomerSCA1
	it('Validate Confirmation Page after standard checkout as a returning customer', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'create_one_customer'
		,	'add_default_shipping_address_to_customer'
		,	'add_default_billing_address_to_customer'
		,	'add_default_credit_card_to_customer'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	customer
			,	defaultShippingAddress
			,	defaultBillingAddress
			,	defaultCreditCard
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'customer': customer
				,	'defaultShippingAddress': defaultShippingAddress
				,	'defaultBillingAddress': defaultBillingAddress
				,	'creditCard': defaultCreditCard
				}

				doTest(done, client, dataset);
			}
		);
	});

});


var doTest = function (done, client, dataset)
{
	'use strict';

	client
		.ItemDetails.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()

		.LoginRegister.loginAs({
			user: dataset.customer.email,
			password: dataset.customer.password
		})

		.Address.Expects.checkExpectedAddress(
			dataset.defaultShippingAddress
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.Address.Expects.checkExpectedAddress(
			dataset.defaultBillingAddress
		)

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillSecurityNumber(
			dataset.creditCard.securityNumber
		)

		.OrderWizard.clickContinue()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedThankYouPage()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedContinueButton()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedDownloadPDFButton()

		.OrderWizard.Module.Confirmation.Expects.checkExpectedSummaryPresence()

		.call(done)
	;
}
