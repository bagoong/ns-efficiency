// BILLING FIRST CHECKOUT SKIP LOGIN AS GUEST

describe('Site Builder - Billing First Checkout Skip Login as Guest', function() 
{
	// former checkoutAsGuestSB2
	it('WITH promo, WITH same shipping-billing checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_billing_first_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	billingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'billingAddress': billingAddress
				,	'creditCard': creditCard
				,	'promoCode': promoCode
				}

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
    	.ItemDetails.SB.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()
		
		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		// BILLING ADDRESS
		
		.OrderWizard.Module.Address.fillBillingAddress(
			dataset.billingAddress
		)

		.OrderWizard.clickContinue()

		// SHIPPING ADDRESS

		.OrderWizard.Module.Address.fillShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// DELIVERY

		.OrderWizard.Module.Shipmethod.chooseMethod({
			index: 1
		})

		.OrderWizard.clickContinue()

		// PAYMENT
		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)

		.OrderWizard.Module.Register.fillGuestEmail(
			dataset.guest.email
		)

		.Address.markSameShippingAddress()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.clickContinue()

		// REVIEW YOUR ORDER

		.Address.Expects.checkExpectedBillingAddress(
			dataset.shippingAddress
		)

		.Address.Expects.checkExpectedShippingAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// THANK YOU PAGE
		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


