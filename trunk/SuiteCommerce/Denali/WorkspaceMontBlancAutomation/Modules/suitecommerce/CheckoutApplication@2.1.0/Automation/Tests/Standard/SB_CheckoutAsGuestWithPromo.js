// @polarion PSGE-1490

// CHECKOUT AS GUEST TESTS

describe('Site Builder - Checkout as Guest', function() 
{
	// former checkoutAsGuestSB2
	it('WITH promo, WITH same shipping-billing checkbox', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sb_configuration_with_standard_checkout'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_valid_promocode_with_percentage_rate'		
		,	'generate_one_shipping_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	promoCode
			,	shippingAddress
			,	creditCard
			,	guestCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem
					]
				,	'guest': guestCustomer
				,	'shippingAddress': shippingAddress
				,	'creditCard': creditCard
				,	'promoCode': promoCode
				}

				doTest(done, client, dataset);
			}
		);
	});	
});


var doTest = function (done, client, dataset)
{
	'use strict';

    client
		.ItemDetails.SB.addProducts(
			dataset.items
		)

		.Cart.clickProceedToCheckout()
		
		.LoginRegister.proceedAsGuest(
			dataset.guest
		)
		
		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod()

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.Address.markSameShippingAddress()

		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}
