// CHECKOUT SKIP LOGIN AS NEW CUSTOMER TESTS

describe('Checkout Skip Login as New Customer', function() 
{
	// former checkoutAsNewCustomerSCA3
	it('Create new billing address and DON\'T use it', function(done)
	{
		var client = this.client;

		Preconditions.start(
			'sca_configuration_with_standard_checkout_and_skip_login'
		,	'get_one_inventory_non_matrix_item'
		,	'get_one_download_item'
		,	'get_one_valid_promocode_with_percentage_rate'	
		,	'generate_one_shipping_address_data'
		,	'generate_one_new_billing_address_data'
		,	'generate_one_visa_credit_card_data'
		,	'generate_one_customer_registration_data'

		,	function (
				error
			,	configuration
			,	inventoryItem
			,	downloadItem
			,	promoCode
			,	shippingAddress
			,	newBillingAddress
			,	creditCard
			,	newCustomer
			)
			{
				var dataset = {
					'baseUrl': configuration.website.baseUrl
				,	'items': [
						inventoryItem, downloadItem
					]
				,	'promoCode': promoCode	
				,	'newCustomer': newCustomer
				,	'shippingAddress': shippingAddress
				,	'newBillingAddress': newBillingAddress
				,	'creditCard': creditCard
				}

				//Preconditions.Item.showItemData(dataset.items);

				doTest(done, client, dataset);
			}
		);
	});
});


var doTest = function (done, client, dataset)
{
	'use strict';

	// TEST FLOW
    client
    	.ItemDetails.addProducts(
    		dataset.items
    	)

		.Cart.clickProceedToCheckout()

		// LOGIN PAGE
		.OrderWizard.Expects.checkExpectedSkipLoginMessage()

		.OrderWizard.clickLoginLinkFromSkipLoginMessage()

		.LoginRegister.clickRegisterLinkInLoginModal()

		.LoginRegister.fillRegisterUser(
			dataset.newCustomer
		)

		.LoginRegister.clickSubmitRegisterUser()

		// INFORMATION PAGE

		.Address.fillAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()
		
		.OrderWizard.Module.Shipmethod.chooseMethod(
			dataset.shippingMethod
		)

		.OrderWizard.clickContinue()

		.OrderWizard.Module.PaymentMethod.choosePaymentMethod(
		{
			value: "creditcard"
		})		

		.CreditCard.fillCreditCard(
			dataset.creditCard
		)
		
		.OrderWizard.Module.CartSummary.fillPromoCode(
			dataset.promoCode
		)

		.OrderWizard.Module.Address.clickAddNewAddress()
		
		.Address.fillAddress(
			dataset.newBillingAddress
		)

		.Address.clickSaveAddress()

		// WE -> DON'T USE <- THE RECENTLY CREATED ADDRESS, INSTEAD WE USE THE SAVED SHIPPING
		.Address.clickSelectThisAddress(
			dataset.shippingAddress
		)

		.OrderWizard.clickContinue()

		// REVIEW PAGE HERE, WE PASS IT

		.OrderWizard.clickContinue()

		//.pause(1000000)

		.OrderWizard.Expects.checkExpectedThankYouPage()

        .call(done)
	;
}


