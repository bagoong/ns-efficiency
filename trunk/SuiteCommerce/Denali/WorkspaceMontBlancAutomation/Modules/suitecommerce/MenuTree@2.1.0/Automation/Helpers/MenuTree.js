defineHelper('MenuTree', 
{

	enterOverview: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeNode, 5000)
			.click(Selectors.MenuTree.buttonHome)
			.waitForAjax()  
			.call(cb)
		;
	}

,   openOrders: function(cb)
	{
		var client = this;
		this
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeOrders, 5000)
			.getAttribute(Selectors.MenuTree.treeOrders, 'class', function(err, text){
				var patt = new RegExp("collapse in");
				if(!patt.test(text)){
					client
						.click(Selectors.MenuTree.buttonOrders)
						.waitForAjax()
					;
				}
			})
		client.call(cb);
	}

,	enterOrdersOrderHistory: function (cb)
	{
		this	
			.MenuTree.openOrders()
			.waitForExist(Selectors.MenuTree.buttonOrderHistory, 5000)
			.click(Selectors.MenuTree.buttonOrderHistory)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterOrdersReturns: function (cb)
	{
		this	
			.MenuTree.openOrders()
			.waitForExist(Selectors.MenuTree.buttonReturns, 5000)
			.click(Selectors.MenuTree.buttonReturns)
			.waitForAjax()
			.call(cb)
		;
	}	

,	enterOrdersReciepts: function (cb)
	{
		this	
			.MenuTree.openOrders()
			.waitForExist(Selectors.MenuTree.buttonReciepts, 5000)
			.click(Selectors.MenuTree.buttonReciepts)
			.waitForAjax()
			.call(cb)
		;
	}	

,	enterOrdersReorderItems: function (cb)
	{
		this	
			.MenuTree.openOrders()
			.waitForExist(Selectors.MenuTree.buttonReorderItems, 5000)
			.click(Selectors.MenuTree.buttonReorderItems)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterOrdersQuotes: function (cb)
	{
		this	
			.MenuTree.openOrders()
			.waitForExist(Selectors.MenuTree.buttonQuotes, 5000)
			.click(Selectors.MenuTree.buttonQuotes)
			.waitForAjax()
			.call(cb)
		;
	}	

,   openWishList: function(cb)
	{
		var client = this;
		this
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeWishList, 5000)
			.getAttribute(Selectors.MenuTree.treeWishList, 'class', function(err, text){
				var patt = new RegExp("collapse in");
				if(!patt.test(text)){
					client
						.click(Selectors.MenuTree.buttonWishList)
						.waitForAjax()
					;
				}
			})
		client.call(cb);
	}

,   openMyListSLM: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeButtonWishListSLM, 5000)
			.click(Selectors.MenuTree.treeButtonWishListSLM)
			.waitForAjax()
			.call(cb);
	}

,	enterWishListAllMyLists: function (cb)
	{
		this	
			.MenuTree.openWishList()
			.waitForExist(Selectors.MenuTree.buttonAllMyLists, 5000)
			.click(Selectors.MenuTree.buttonAllMyLists)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterWishList: function (wishlist, cb)
	{
		this	
			.MenuTree.openWishList()
			.waitForExist(Selectors.MenuTree.buttonMyLists.replace('%s', wishlist), 5000)
			.click(Selectors.MenuTree.buttonMyLists.replace('%s', wishlist))
			.waitForAjax()
			.call(cb)
		;
	}

,	openBilling: function(cb)
	{
		var client = this;
		this		
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeBilling, 5000)
			.getAttribute(Selectors.MenuTree.treeBilling, 'class', function(err, text){
				var patt = new RegExp("collapse in");
				if(!patt.test(text)){
					client
						.click(Selectors.MenuTree.buttonBilling)
						.waitForAjax()
					; 
				}
			})
		client.call(cb);
	}	

,	enterBillingAccountBalance: function (cb)
	{
		this	
			.MenuTree.openBilling()
			.waitForExist(Selectors.MenuTree.buttonAccountBalance, 5000)
			.click(Selectors.MenuTree.buttonAccountBalance)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterBillingInvoices: function (cb)
	{
		this	
			.MenuTree.openBilling()
			.waitForExist(Selectors.MenuTree.buttonInvoices, 5000)
			.click(Selectors.MenuTree.buttonInvoices)
			.waitForAjax()
			.call(cb)
		;
	}	

,	enterBillingTransactionHistory: function (cb)
	{
		this	
			.MenuTree.openBilling()
			.waitForExist(Selectors.MenuTree.buttonTransactionHistory, 5000)
			.click(Selectors.MenuTree.buttonTransactionHistory)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterBillingPrintAStatement: function (cb)
	{
		this	
			.MenuTree.openBilling()
			.waitForExist(Selectors.MenuTree.buttonPrintStatement, 5000)
			.click(Selectors.MenuTree.buttonPrintStatement)
			.waitForAjax()
			.call(cb)
		;
	}

,   openSettings: function(cb)
	{
		var client = this;
		this		
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeSettings, 5000)
			.getAttribute(Selectors.MenuTree.treeSettings, 'class', function(err, text){
				var patt = new RegExp("collapse in");
				if(!patt.test(text)){
					client
						.click(Selectors.MenuTree.buttonSettings)
						.waitForAjax()
					; 
				}
			})
		client.call(cb);
	}		

,	enterSettingsProfileInformation: function (cb)
	{
		this	
			.MenuTree.openSettings()
			.waitForExist(Selectors.MenuTree.buttonProfileInformation, 5000)
			.click(Selectors.MenuTree.buttonProfileInformation)
			.waitForAjax()
			.call(cb)
		;
	}	

,	enterSettingsEmailPreferences: function (cb)
	{
		this	
			.MenuTree.openSettings()
			.waitForExist(Selectors.MenuTree.buttonEmailPreferences, 5000)
			.click(Selectors.MenuTree.buttonEmailPreferences)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterSettingsAddressBook: function (cb)
	{
		this	
			.MenuTree.openSettings()
			.waitForExist(Selectors.MenuTree.buttonAddressBook, 5000)
			.click(Selectors.MenuTree.buttonAddressBook)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterSettingsCreditCards: function (cb)
	{
		this	
			.MenuTree.openSettings()
			.waitForExist(Selectors.MenuTree.buttonCreditCards, 5000)
			.click(Selectors.MenuTree.buttonCreditCards)
			.waitForAjax()
			.call(cb)
		;
	}

,	enterSettingsUpdateYourPassword: function (cb)
	{
		this	
			.MenuTree.openSettings()
			.waitForExist(Selectors.MenuTree.buttonUpdateYourPassword, 5000)
			.click(Selectors.MenuTree.buttonUpdateYourPassword)
			.waitForAjax()
			.call(cb)
		;
	}

,   openCases: function (cb)
	{
		var client = this;
		this		
			.waitForAjax()
			.waitForExist(Selectors.MenuTree.treeCases, 5000)
			.getAttribute(Selectors.MenuTree.treeCases, 'class', function(err, text){
				var patt = new RegExp("collapse in");
				if(!patt.test(text)){
					client
						.click(Selectors.MenuTree.buttonCases)
						.waitForAjax()
					; 
				}
			})
			.call(cb)
		;
	}	

,	enterCasesSupportCases: function (cb)
	{
		this	
			.MenuTree.openCases()
			.waitForExist(Selectors.MenuTree.buttonCasesAll, 5000)
			.click(Selectors.MenuTree.buttonCasesAll)
			.waitForAjax()
			.call(cb)
		;
	}		

,	enterCasesSubmitNewCase: function (cb)
	{
		this	
			.MenuTree.openCases()
			.waitForExist(Selectors.MenuTree.buttonNewCase, 5000)
			.click(Selectors.MenuTree.buttonNewCase)
			.waitForAjax()
			.call(cb)
		;
	}				
	
});