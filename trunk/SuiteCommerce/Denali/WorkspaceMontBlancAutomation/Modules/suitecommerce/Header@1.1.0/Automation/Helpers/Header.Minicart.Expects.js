defineHelper('Header.Minicart.Expects',
{
	checkMiniCartVisibility: function (visible, cb)
	{
		this
			.isVisible(Selectors.Header.miniCart, function(err, isVisible)
			{
				expect(isVisible).toEqual(visible);
			})
			.call(cb)
		;
	}

,	checkItemData: function (data, item, cb)
	{
		data.name && expect(data.name).toBeSameText(item.name);
		data.quantity && expect(data.quantity).toEqual(item.quantity);
		data.itemTotal && expect(data.itemTotal).toBeSameMoney(item.itemTotal);
		data.imageUrl && expect(unescape(item.imageUrl)).toContain(data.imageUrl);

		if (data.options)
		{
			expect(data.options.length).toEqual(Object.keys(item.options).length);

			data.options.forEach(function(value, index)
			{
				var option = item.options[value.label];

				expect(option.label).toEqual(value.label + ':');
				expect(option.value).toEqual(value.value);
			});
		}

		this
			.call(cb)
		;
	}

,	isMiniCartEmpty: function(cb)
	{
		this
			.waitForAjax()
			.click(Selectors.Header.miniCartIcon)
			.getText(Selectors.Header.cartEmpty, function (err, text){
				var empty_regex = /is empty/i;
				cb(null, empty_regex.test(text))
			})
		;
	}

,	checkMiniCartIsEmpty: function(cb)
	{
		this
			.Header.Minicart.Expects.isMiniCartEmpty( function (err, isEmpty)
			{
				expect(isEmpty).toBeTrueOrFailWith('Minicart is not empty.');
			})
			.call(cb)
		;
	}

,	checkMiniCartIsNotEmpty: function(cb)
	{
		this
			.Header.Minicart.Expects.isMiniCartEmpty( function (err, isEmpty)
			{
				expect(isEmpty).toBeFalsy();
			})
			.call(cb)
		;
	}
});
