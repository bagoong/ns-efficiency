var _ = require('underscore');

defineHelper('Header.Minicart',
{
	getMiniCartItemDataTypeById: function(item_id, cb){
		var prefix = Selectors.Header.prefix.replace('%s', item_id);
		var dataType = {
			'Name': ''
		,	'ItemPrice': ''
		,	'Quantity': ''
		};

		this
			.waitForAjax()
			.waitForExist(Selectors.Header.cartLink,10000)
			.click(Selectors.Header.cartLink)
			.waitForExist(prefix + Selectors.Header.subtotal, 10000)
			.getText(prefix + Selectors.Header.title, function(err, text){
				dataType.Name = text;
			})
			.getText(prefix + Selectors.Header.price, function(err, text){
				dataType.ItemPrice = text;
			})
			.getText(prefix + Selectors.Header.quantity, function(err, text){
				dataType.Quantity = text;
			})

			.click(Selectors.Header.cartLink)
			.waitForAjax()
			.call(function()
			{	
				cb(null, dataType)
			})
		;
	}

,	getItemDataType: function (item, cb)
	{
		var prefix = '[data-view="Header.MiniCartItemCell"] [data-item-id="' + item.id + '"] ';

		this
			.waitForAjax()
			.Header.clickMiniCartLink()
			.waitForExist(prefix + '.header-mini-cart-item-cell-title-navigable', 10000)
			.getText(prefix + '.header-mini-cart-item-cell-title-navigable', function(err, text){
				item.name = text;
			})
			.getText(prefix + '.header-mini-cart-item-cell-product-price', function(err, text){
				item.itemTotal = text;
			})
			.getText(prefix + '.header-mini-cart-item-cell-quantity-value', function(err, text){
				item.quantity = text;
			})
			.getAttribute(prefix + 'a img', 'src', function(err, text){
				item.imageUrl = text;
			})
			.Header.Minicart.getOptionsSelected(item)
			.call(cb)
		;
	}

,	getOptionsSelected: function(item, cb)
	{
		if (item.options)
		{
			var prefix = '[data-view="Header.MiniCartItemCell"] [data-item-id="' + item.id + '"] .item-views-selected-option'
			,	client = this;

			client
				.waitForExist(prefix, 10000)
				.getAttribute(prefix, 'name', function(err, names)
				{
					names = !_.isArray(names) ? [names] : names
					names.forEach(function(name)
					{
						client.getText(prefix + '[name="' + name + '"] .item-views-selected-option-label', function(err, label)
						{
							client.getText(prefix + '[name="' + name + '"] .item-views-selected-option-value', function(err, value)
							{
								item.options[name] = { 'label': label,  'value': value };
							});
						});
					});
				})
				.call(cb)
			;
		}
		else
		{
			this.call(cb)
		}
	}

,	getIdsFromMiniCart: function(cb)
	{
		var items = [];

		this
			.getAttribute('.header-mini-cart li.header-mini-cart-item-cell', 'data-item-id', function(err, ids)
			{
				ids = !_.isArray(ids) ? [ids] : ids;
				ids.forEach(function (id)
				{
					items.push(id);
				})

				cb(err, items);
			})
		;
	}

,	clickCheckout: function(cb)
	{
		this
			.click(".header-mini-cart-menu-cart-icon")
			.waitForAjax()
			.click(".header-mini-cart-button-checkout")
			.call(cb)
		;
	}
});
