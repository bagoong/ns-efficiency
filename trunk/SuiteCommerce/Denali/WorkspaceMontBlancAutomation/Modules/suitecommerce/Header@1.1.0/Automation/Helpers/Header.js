defineHelper('Header',
{

	logout: function (cb)
	{
		this
			.click(Selectors.Header.welcomeLink)
			.click(Selectors.Header.signOutLink)
			.call(cb);
	}

,	openNavigation: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.welcome, 5000)
			.click(Selectors.Header.welcome)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToMyAccount: function(cb)
	{
		this
			.Header.openNavigation()
			.waitForExist(Selectors.Header.myAccount, 5000)
			.click(Selectors.Header.myAccount)
			.call(cb)
		;
	}

,	goToHome: function(cb)
	{
		this
			.waitForExist(Selectors.Header.home, 5000)
			.click(Selectors.Header.home)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToShopping: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.shop, 5000)
			.click(Selectors.Header.shop)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToOrderHistory: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.orderHistory)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToReturns: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.returns)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToReciepts: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.receipts)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToReorderItems: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.reorder)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToQuotes: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.quotes)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToProductList: function(productList, cb)
	{
		this
			.waitForAjax()
			.Header.openNavigation()
			.waitForExist(Selectors.Header.productList.replace('%s', productList), 5000)
			.click(Selectors.Header.productList.replace('%s', productList))
			.waitForAjax()
			.call(cb)
		;
	}

,	goToAccountBalance: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.balance)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToInvoices: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.invoice)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToTransactionsHistory: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.transaction)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToPrintAStatement: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.print)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToProfileInformation: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.profile)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToEmailPreferences: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.email)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToAddressBook: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.address)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToCreditCard: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.creditCard)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToUpdateYourPassword: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.password)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToSupportCases: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.cases)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToSubmitNewCase: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.newCase)
			.waitForAjax()
			.call(cb)
		;
	}

,	signOut: function(cb)
	{
		this
			.Header.openNavigation()
			.click(Selectors.Header.signOut)
			.waitForAjax()
			.call(cb)
		;
	}

,	goToCart: function(cb)
	{
		var client = this
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.cartLink, 10000)
			.click(Selectors.Header.cartLink)
			.waitForAjax()
			.isExisting(Selectors.Header.cartEmpty, function(err, isVisible)
			{
				if(isVisible)
				{
					client
						.pause(3000)
						.click(Selectors.Header.cartEmptyLink)
						.waitForAjax()
				}
				else
				{
					client
						.click(Selectors.Header.viewCart)
						.waitForAjax()
				}
			})
			.call(cb)
		;
	}

,	clickHeaderLogo: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.headerLogo, 5000)
			.click(Selectors.Header.headerLogo)
			.call(cb)
		;
	}

,	clickLoginLink: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.headerLogo, 5000)
			.click(Selectors.Header.loginLink)
			.call(cb)
		;
	}

,	clickMiniCartLink: function(cb)
	{
		var client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.Header.cartLink, 5000)
			.scroll(Selectors.Header.cartLink)
			.isVisible(Selectors.Header.miniCart, function(err, visible)
			{
				if (!visible)
				{
					client
						.click(Selectors.Header.cartLink)
						.pause(500)
					;
				}
			})
			.call(cb)
	}
,	clickRegisterLink: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Header.registerLink, 5000)
			.click(Selectors.Header.loginLink, 5000)
			.call(cb)
		;
	}

});
