var _ = require('underscore');

defineHelper('Profile.Expects', 
{

	checkSuccessfullySaved: function(cb)
	{
		this
			.waitFor(Selectors.Profile.successSaving, 10000)
			.getText(Selectors.Profile.successSaving, function(err, text)
			{
				expect(text.trim()).toEqual('Email Preferences successfully saved!');
			})
			.call(cb)
		;
	}

,	checkPasswordUpdated: function(cb)
	{
		this
			.waitFor(Selectors.Profile.successSaving, 10000)
			.getText(Selectors.Profile.successSaving, function(err, text)
			{
				expect(text.trim()).toEqual('Password successfully updated!');
			})
			.call(cb)
		;
	}	

,	checkSubscriptions: function(params, cb)
	{
		var client = this;
		this
			.isSelected(Selectors.Profile.subscriptionTwo, function(err, text)
			{
				if(params.Billing)
				{
					expect(text).toBeTruthy()
				}
				else
				{
					expect(text).toBeFalsy()					
				}
			})
			.isSelected(Selectors.Profile.subscriptionOne, function(err, text)
			{
				if(params.Marketing)
				{
					expect(text).toBeTruthy()
				}
				else
				{
					expect(text).toBeFalsy()					
				}
			})
			.isSelected(Selectors.Profile.subscriptionFour, function(err, text)
			{
				if(params.Newsletter)
				{
					expect(text).toBeTruthy()
				}
				else
				{
					expect(text).toBeFalsy()				
				}
			})
			.isSelected(Selectors.Profile.subscriptionFive, function(err, text)
			{
				if(params.ProductUpdates)
				{
					expect(text).toBeTruthy()
				}
				else
				{
					expect(text).toBeFalsy()					
				}
			})
			.isSelected(Selectors.Profile.subscriptionThree, function(err, text)
			{
				if(params.Surveys)
				{
					expect(text).toBeTruthy()
				}
				else
				{
					expect(text).toBeFalsy()					
				}
			})
			.call(cb)
		;
	}



,	checkErrorMessage: function(params, cb)
	{
		var client = this
		this
			.waitForAjax()

			.call(function()
			{
				if(params == 'Current')
				{
					client
						.getText(Selectors.Profile.passCurrentP, function(err, text)
						{
							expect(text).toEqual('Current password is required')
						})
					;
				}
				else if(params == 'New')
				{
					client
						.getText(Selectors.Profile.passNewP, function(err, text)
						{
							expect(text).toEqual('New password is required')
						})
					;
				}
				else if(params == 'Confirm')
				{
					client
						.getText(Selectors.Profile.passConfirmP, function(err, text)
						{
							expect(text).toEqual('Confirm password is required')
						})
					;
				}
				else if(params == 'Different')
				{
					client
						.getText(Selectors.Profile.passConfirmP, function(err, text)
						{
							expect(text).toEqual('New Password and Confirm Password do not match')
						})
					;
				}
			})
			.call(cb)
		;
	}

,	checkProfileInformationPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Profile.profileInformationPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Profile Information page is not present.');
        	})
        	.call(cb)
        ;
	}

,	checkEmailPreferencesPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Profile.emailPreferencesPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Email Preferences page is not present.');
        	})
        	.call(cb)
        ;
	}

,	checkUpdateYourPasswordPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Profile.updateYourPasswordPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Update Your Password page is not present.');
        	})
        	.call(cb)
        ;
	}

});