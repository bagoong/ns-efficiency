defineHelper('ItemViews.Expects', {
	verifyItemsInRecetlyView: function (dataset, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.firstItemHref),5000)
			.isExisting(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.firstItemHref)).then(function(isExisting){
				expect(isExisting).toBeTrue();
			})

			.waitForExist(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.secondItemHref),5000)
			.isExisting(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.secondItemHref)).then(function(isExisting){
				expect(isExisting).toBeTrue();
			})

			.waitForExist(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.thirdItemHref),5000)
			.isExisting(Selectors.ItemViews.itemViewsRelatedThumbnail.replace('%s', dataset.thirdItemHref)).then(function(isExisting){
				expect(isExisting).toBeTrue();
			})
		.call(cb);
	}
,	verifyMesageOfLoginToSeePrices: function(itemid, cb)
	{
		this
			.waitForAjax()
			.getText('[data-item-id="'+ itemid +'"] .item-views-price-login-to-see-prices', function(err, text)
			{
				expect(text).toBeSameText("Log in to see price");
			})
			.call(cb);
	}
,	verifyMesageOfLoginToSeePricesOnPDP: function(cb)
	{
		this
			.waitForAjax()
			.getText('.item-views-price-login-to-see-prices', function(err, text)
			{
				expect(text).toBeSameText("Please log in to see price or purchase this item");
			})
			.call(cb);
	}
,	verifyMesageOfLoginToSeePricesOnQuickView: function(cb)
	{
		this
			.waitForAjax()
			.getText('.item-views-price-login-to-see-prices', function(err, text)
			{
				expect(text).toBeSameText("Please log in to see price or purchase this item");
			})
			.call(cb);
	}
});