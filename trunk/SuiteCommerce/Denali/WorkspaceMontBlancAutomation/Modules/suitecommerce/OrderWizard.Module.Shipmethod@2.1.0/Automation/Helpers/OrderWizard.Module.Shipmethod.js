
var _ = require('underscore');

defineHelper('OrderWizard.Module.Shipmethod', 
{

	clickGoBackToEditShippingInformation: function(cb)
	{
		this
			.click('.order-wizard-showshipments-module-shipping-details-address-link')
			.call(cb)
		;
	}	


,	waitForDeliveryMethodEnabled: function(cb)
	{
		var client = this;

		this
			.isExisting('.order-wizard-shipmethod-module-message', function(err, is_visible)
			{
				if (is_visible)
				{
					client
						.waitForAjax(10000)
						// WAIT FOR NO EXIST
						.waitForExist('.order-wizard-shipmethod-module-message', 10000, true)
					;
				}
			})
			// PAUSE REQUIRED FOREVER, THERE IS NO CLEAR AJAX CALL TO WAIT FOR DELIVERY METHODS CHANGE
			.pause(5000)
			.waitForAjax(10000)
			.call(cb)
		;
	}


,	getMethodSelectionStyle: function(cb)
	{
		var client = this;
		var selection_style = null;

		this
			.OrderWizard.Module.Shipmethod.waitForDeliveryMethodEnabled()

			.isExisting('select[data-action=select-delivery-option]', function(err, existing_combobox)
			{
				if (existing_combobox)
				{
					selection_style = "select";
				}
			})
			
			.isExisting("[data-action='select-delivery-option-radio']", function(err, existing_radio)
			{
				if (existing_radio)
				{
					selection_style = "radio";
				}
			})

			.call(function()
			{
				cb(null, selection_style);
			})
		;
	}


,	chooseMethod: function(params, cb)
	{
		var client = this;

		if (typeof params === 'function') {
			cb = params;
			params = null;
		}

		this.OrderWizard.Module.Shipmethod.getMethodSelectionStyle(function(err, selection_style)
		{
			switch (selection_style)
			{
				case "select":
					client.OrderWizard.Module.Shipmethod.chooseMethodFromSelect(params, cb);
					break;

				case "radio":
					client.OrderWizard.Module.Shipmethod.chooseMethodFromRadioButtons(params, cb);
					break;

				default:
					client.call(function()
					{
						cb('ShipMethod Module not present in page.', 'ShipMethod Module not present in page.');
					});
			}
		});

	}	


,	chooseMethodFromRadioButtons: function(params, cb) 
	{
		if (typeof params === 'function') {
			cb = params;
			params = null;
		}

		var client = this;
		var delivery_selector = ".order-wizard-shipmethod-module-option[data-value='%s'] input";
		var delivery_methods = [];

		this
			.OrderWizard.Module.Shipmethod.getMethodDataTypes(function(err, found_methods) {
				delivery_methods = found_methods;
			})

			.call(function()
			{
				var method_id = null;

				if (!params)
				{
					params = {}
					method_id = _.sample(delivery_methods).id;			
				};				

 				if (params.index)
				{
					method_id = delivery_methods[params.index - 1].id;

				}
				else if (params.name)
				{
					delivery_methods.forEach(function(shipmethod_data, shipmethod_index)
					{
						name_matcher = new RegExp(params.name, 'i');

						if (name_matcher.test(shipmethod_data.name)) {
							method_id = shipmethod_data.id;
							return false;
						}
					});
				}
				else if (params.value)
				{
					method_id = params.value;
				}


				if (method_id)
				{
					delivery_selector = delivery_selector.replace("%s", method_id);
					client
						.scroll(delivery_selector)
						.radioButtonClick(delivery_selector)
						.waitForAjax()
					;
				}

			})
			
			.waitForAjax()
			
			.call(function()
			{
				var selected_method = null;

				if (typeof cb === 'function')
				{
					cb(null, selected_method);
				}
			})
		;
	}


,	chooseMethodFromSelect: function(params, cb) 
	{
		if (typeof params === 'function') {
			cb = params;
			params = null;
		}
		
		var client = this;
		var delivery_selector = 'select[data-action=select-delivery-option]';
		var delivery_methods = [];

		this
			.waitForExist(delivery_selector, 10000)
			.waitForAjax()

			.OrderWizard.Module.Shipmethod.getMethodDataTypes(function(err, found_methods) {
				delivery_methods = found_methods;
			})

			.waitForAjax()

			.call(function()
			{
				if (!params)
				{
					params = _.sample(delivery_methods);		
				}

				if (params.name)
				{
					delivery_methods.forEach(function(shipmethod_data, shipmethod_index)
					{
						name_matcher = new RegExp(params.name, 'i');

						if (name_matcher.test(shipmethod_data.name)) {
							params.index = shipmethod_index;
						}
					});
				}
				
				if (params.index)
				{
					client.selectByIndex(delivery_selector, params.index);
				}
				else if (params.text)
				{
					client.selectByVisibleText(delivery_selector, params.text)
				}
				else if (params.value)
				{
					client.selectByValue(delivery_selector, params.value);
				}
			})
			
			.waitForAjax()
			
			.call(function()
			{
				var selected_method = null;

				if (typeof cb === 'function')
				{
					cb(null, selected_method);
				}
			})
		;
	}


,	getMethodDataTypes: function(cb)
	{
		var client = this;

		this.OrderWizard.Module.Shipmethod.getMethodSelectionStyle(function(err, selection_style)
		{
			switch (selection_style)
			{
				case "select":
					client.OrderWizard.Module.Shipmethod.getMethodDataTypesFromSelect(cb);
					break;

				case "radio":
					client.OrderWizard.Module.Shipmethod.getMethodDataTypesFromRadioButtons(cb);
					break;

				default:
					client.call(function()
					{
						cb('ShipMethod Module not present in page.', 'ShipMethod Module not present in page.');
					});
			}
		});
	}


,	getMethodDataTypesFromRadioButtons: function(cb)
	{

		var client = this;
		var delivery_methods = [];

		this
			.waitForAjax(10000)
			.waitForExist('.order-wizard-shipmethod-module-option', 10000)
			.waitForAjax(10000)

			.getAttribute('.order-wizard-shipmethod-module-option', 'data-value', function(err, options_values)
			{
				if (!options_values) return false;

				options_values = (Array.isArray(options_values))? options_values : [options_values]

				options_values.forEach(function(method_id){
					var delivery_method = {
						'id': method_id
					,	'text': ''	
					,	'name': ''
					,	'cost': ''
					};

					client
						.getText(".order-wizard-shipmethod-module-option[data-value='" + method_id + "'] .order-wizard-shipmethod-module-option-name", function(err, text)
						{
							delivery_method.text = text.replace('\n', ' ');
							delivery_method.name = text.replace(/\$.*/, '').trim();
						})

						.getText(".order-wizard-shipmethod-module-option[data-value='" + method_id + "'] .order-wizard-shipmethod-module-option-price", function(err, text)
						{
							delivery_method.cost = client.util.textToFloat(text);

							if (isNaN(delivery_method.cost)) 
							{
								delivery_method.cost = 0;
							}
						})
					;

					delivery_methods.push(delivery_method);
				})

			})

			.call(function() {
				cb(null, delivery_methods);
			})
		;		
	}


,	getMethodDataTypesFromSelect: function(cb)
	{
		var client = this;
		var delivery_selector = 'select[data-action=select-delivery-option]';

		var delivery_methods = [];

		this
			.waitForAjax(10000)
			.waitForExist(delivery_selector, 10000)
			.waitForAjax(10000)

			.forEachWebdriverId(delivery_selector + ' option', function(element_id)
			{
				delivery_method = {
					'id': ''
				,	'text': ''	
				,	'name': ''
				,	'cost': ''
				};

				client
					.elementIdText(element_id, function(err, val) {
						delivery_method.text = val.value.trim();
						delivery_method.name = delivery_method.text;

						var text_parts = delivery_method.text.split(" - ")

						if (text_parts.length === 2)
						{
							delivery_method.cost = client.util.textToFloat(text_parts[0]);

							if (isNaN(delivery_method.cost)) 
							{
								delivery_method.cost = 0;
							}

							delivery_method.name = text_parts[1];
						}
					})

					.elementIdAttribute(element_id, 'value', function(err, val) {
						delivery_method.id = val.value;
					})
					.call(function()
					{
						if (/^\d+$/.test(delivery_method.id))
						{
							delivery_methods.push(delivery_method);
						}
					})
				;
			})

			.call(function() {
				cb(null, delivery_methods);
			})

		;
	}
});