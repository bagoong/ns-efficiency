defineHelperDependencies('GlobalViews.PaymentMethod.Expects', 'GlobalViews.PaymentMethod')

defineHelper('GlobalViews.PaymentMethod.Expects', 
{
	checkExpectedCreditCard: function(expected_credit_card, cb)
	{
		this
			.getAttribute(Selectors.GlobalViews.paymentMethodCreditCardIcon, 'alt',function(err, text){
				var card_type_matcher = new RegExp('^' + expected_credit_card.cardType + '$', 'i')
				expect(text).toMatch(card_type_matcher);
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardNumber, function(err, text){
				var check_number = expected_credit_card.cardNumber || expected_credit_card.cardEnding;
				var regexCC = check_number.match(/\d{4}$/i);

				expect(text).toMatch(regexCC[0]);
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardExpDate, function(err, text){
				var date_part = text.match(/\d{1,2}\/\d{4}/)

				if (date_part) text = date_part[0];

				var match_regex = expected_credit_card.expMonth + '/' + expected_credit_card.expYear;
				match_regex = match_regex.replace(/^0/, "0?"); // with this we contemplate non zeroed month

				expect(text).toMatch(match_regex);
			})

			.getText(Selectors.GlobalViews.paymentMethodCreditCardName, function(err, text){
				expect(text).toMatch(expected_credit_card.name);
			})

			.call(cb)
		;
	}

,	checkExpectedInvoice: function(expected_invoice, cb)
	{
		this
			.GlobalViews.PaymentMethod.getInvoiceDataType(function(err, found_invoice)
			{
				expect(found_invoice).toHaveEqualFields(expected_invoice);
			})
			.call(cb)
		;
	}

});