// @module ListHeader
var async = require('async')
,	_ = require('underscore');

// @class expectSorting Verifies the sorting of a list depending on the type of data used
var expectSorting = {

	// @method expectSortingValuesSTRING Verifies the correct sorting depending on the order for strings
	// @params {Number} order
	// @params {String} currentValue
	// @params {String} nextValue
	// @return {Void}
	expectSortingValuesSTRING: function (order, currentValue, nextValue)
	{
		order === 1 ?
			expect(currentValue.localeCompare(nextValue) >= 0).toBeTrue() :
			expect(currentValue.localeCompare(nextValue) <= 0).toBeTrue();
	}

	// @method expectSortingValuesDATE Verifies the correct sorting depending on the order for dates
	// @params {Number} order
	// @params {Date} currentValue
	// @params {Date} nextValue
	// @return {Void}
,	expectSortingValuesDATE: function (order, currentValue, nextValue)
	{
		currentValue = new Date(currentValue);
		nextValue = new Date(nextValue);
		order === 1 ?
			expect(currentValue).not.toBeBefore(nextValue) :
			expect(currentValue).not.toBeAfter(nextValue);
	}

	// @method expectSortingValuesNUMBER Verifies the correct sorting depending on the order for numbers
	// @params {Number} order
	// @params {String} currentValue
	// @params {String} nextValue
	// @return {Void}
,	expectSortingValuesNUMBER: function (order, currentValue, nextValue)
	{
		currentValue = parseInt(currentValue.replace(/[^\d\-]/g, ''));
		nextValue = parseInt(nextValue.replace(/[^\d\-]/g, ''));
		order === 1 ?
			expect(currentValue >= nextValue).toBeTrue() :
			expect(currentValue <= nextValue).toBeTrue();
	}

};

defineHelper('ListHeader.Expects',
{

	// @method expectReorderItemsTitle Verifies if you are in the Reorder Items page
	// @return {Void}
	expectReorderItemsTitle: function(cb)
	{
		this
			.isExisting(Selectors.ListHeader.reorderItemsTitle, function (err, exists)
			{
				expect(exists).toBeTrue();
			})
			.call(cb)
		;
	}

	// @method correctSorting Depending on the type of sorting generates the call to the proper method to verify it
	// @param {Automation.ListHeader.SortingOptions} options
	// @return {Void}
	// @usage:
	// .ListHeader.Expects.correctSorting({
	//		// The type of the field you are verifying, the possible values are: date, number, string
	// 		type: dataset.sortData.type
	//		// The selector for the getText to capture the value
	// 	,	selector: dataset.sortData.selector
	//		// The order the list should be sorted in: 1 for forward and -1 for backwards
	// 	,	order: 1
	// })
,	correctSorting: function (options, cb)
	{
		var assert_fn = expectSorting['expectSortingValues' + options.type.toUpperCase()];

		if (!_.isFunction(assert_fn))
		{
			throw new Error('  You are not comparing a correct data type.');
		}

		options.assertFn = assert_fn;

		this
			.ListHeader.Expects.checkOrder(options)
			.call(cb)
		;
	}

	// @method checkOrder Obtains the values of the list and uses the proper verification of order
	// @param {Automation.ListHeader.SortingOptions} options
	// @return {Void}
,	checkOrder: function (options, cb)
	{
		var currentValue
		,	nextValue;

		this
			.waitForAjax()
			.waitForExist(options.selector)
			.getText(options.selector)
			.then(function (values)
			{
				values.forEach(function (value, index, values)
				{
					if (values[index + 1])
					{
						options.assertFn(options.order, value, values[index + 1]);
					}
				})
			})
			.call(cb)
		;
	}

	/*
	@method correctFiltering
	@param {Object} options see below detailed info
	@return void
	@usage: the example below will iterate over each key in the filter_dictionary, set the option,
			and validate that only elements that match the option are displayed.


	//1) define filter_dictionary, which contains all the options/values of the select displayed on the page under test.
	var filter_dictionary = {
			all: '0'
		,	closedLost: '14'
		,	qualified: '7'
		,	inDiscussion: '8'
		,	identifiedDecisionMakers: '9'
		,	proposal: '10'
		,	inNegotiation: '11'
		,	purchasing: '12'
	};

	//2) define a selectorMapFunc, which given an object can return the associate selector to find it on the page.
	function selectorMapFunc(quote_obj)
	{
		return Selectors.Quote.overviewTitle.replace('%s', quote_obj.id);
	}

	//The actual test
	function doTest(done, client, dataset)
	{
		client
			...
			.ListHeader.Expects.correctFiltering({
					collection: dataset.quotes
				,	filterDictionary: filter_dictionary
				,	selectorMapFunc: selectorMapFunc
			})
			...
		;
	}

	//3) define a dataset array of elements to consider in the test with its corresponding status.
	var test_dataset = {
		...
		quotes: [
			{id: 6994, status: '8'},
			{id: 6993, status: '11'},
			{id: 6992, status: '12'},
			{id: 4390, status: '10'},
		]
		...
	}


	*/

,	correctFiltering: function(options, cb)
	{
		var client = this
		,	filter_ids = Object.keys(options.filterDictionary);

		async.eachLimit(filter_ids, 1, function(filter_id, each_cb)
		{
			var filter_value = options.filterDictionary[filter_id]
			,	selector_data = [];

			options.collection.forEach(function(result_obj)
			{
				var result_selector = options.selectorMapFunc(result_obj);

				if (result_obj.entityStatusInternalId === filter_value || filter_value === options.filterDictionary.all)
				{
					selector_data.push({value: result_selector, shouldExist: true});
				}
				else
				{
					selector_data.push({value: result_selector, shouldExist: false});
				}
			});

			client
				.ListHeader.setFilterByValue(filter_value)
				.Expects.selectorsExist(selector_data, 50)
				.call(each_cb);

		}, cb);
	}

});