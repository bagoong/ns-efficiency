defineHelper('ReturnAuthorization.Details.Expects', 
{

	verifyReturnDetails: function(returnAuthorization, cb)
	{
		var client = this;
		client
			.waitForAjax()

			.waitForExist(Selectors.ReturnAuthorization.summaryTotal, 5000)	

			.ReturnAuthorization.Details.Expects.verifyReturnHeader(returnAuthorization.header)

			.ReturnAuthorization.Details.Expects.verifyReturnItems(returnAuthorization.itemList)

			.ReturnAuthorization.Details.Expects.verifyComment(returnAuthorization.comments)

			.ReturnAuthorization.Details.Expects.verifySummary(returnAuthorization.summary)
		;
			if (returnAuthorization.appliedToTransactions)
			{
				client
					.ReturnAuthorization.Details.Expects.verifyAppliedToTransactions(returnAuthorization.appliedToTransactions)
				;
			}
		client
			.call(cb)
		;
	}

,	verifyReturnHeader: function (returnHeader, cb)
	{
		this			
			.getText(Selectors.ReturnAuthorization.returnHeaderNumber).then(function (text)
			{
				expect(text).toMatch(returnHeader.returnNo);
			})

			.getText(Selectors.ReturnAuthorization.returnHeaderDate).then(function (text)
			{
				expect(text).toMatch(returnHeader.returnDate);
			})

			.getText(Selectors.ReturnAuthorization.returnHeaderTotal).then(function (text)
			{
				expect(text).toMatch(returnHeader.returnAmount);
			})

			.getText(Selectors.ReturnAuthorization.returnHeaderStatus).then(function (text)
			{
				expect(text).toMatch(returnHeader.returnStatus);
			})


			.call(cb)
		;
	}

,	verifyReturnItems: function (returnItemList, cb)
	{
		var client = this;
		returnItemList.forEach(function (item)
		{
			client
				.getAttribute(Selectors.ReturnAuthorization.itemId, 'data-id').then(function (text){
					expect(text).toMatch(item.internalId);
				})

				.isExisting(Selectors.ReturnAuthorization.itemName.replace('%s', item.internalId)).then(function (exists)
				{
					expect(exists).toBeTrueOrFailWith('Item ' + item.name + ' is not present in return.');
					if(exists)
					{
						client
							.getText(Selectors.ReturnAuthorization.itemName.replace('%s', item.internalId)).then(function (text){
								expect(text.toLowerCase()).toMatch(item.name.toLowerCase());
							})

							.isExisting(Selectors.ReturnAuthorization.itemSku.replace('%s', item.internalId)).then(function (isExisting)
							{
								expect(isExisting).toBeTrueOrFailWith('Item SKU is not present');
							})
							.getText(Selectors.ReturnAuthorization.itemSku.replace('%s', item.internalId)).then(function (text){
								expect(text.toLowerCase()).toMatch(item.sku.toLowerCase());
							})

							.isExisting(Selectors.ReturnAuthorization.itemPrice.replace('%s', item.internalId)).then(function (isExisting)
							{
								expect(isExisting).toBeTrueOrFailWith('Item price is not present');
							})
							.getText(Selectors.ReturnAuthorization.itemPrice.replace('%s', item.internalId)).then(function (text){
								expect(text).toMatch(item.price);
							})

							.isExisting(Selectors.ReturnAuthorization.itemQuantity.replace('%s', item.internalId)).then(function (isExisting)
							{
								expect(isExisting).toBeTrueOrFailWith('Item quantity is not present');
							})
							.getText(Selectors.ReturnAuthorization.itemQuantity.replace('%s', item.internalId)).then(function (text){
								expect(text).toMatch(item.quantity);
							})

							.isExisting(Selectors.ReturnAuthorization.itemAmount.replace('%s', item.internalId)).then(function (isExisting)
							{
								expect(isExisting).toBeTrueOrFailWith('Item amount is not present');
							})
							.getText(Selectors.ReturnAuthorization.itemAmount.replace('%s', item.internalId)).then(function (text){
								expect(text).toMatch(item.amount);
							})

							.isExisting(Selectors.ReturnAuthorization.itemReason.replace('%s', item.internalId)).then(function (isExisting)
							{
								expect(isExisting).toBeTrueOrFailWith('Item return reason is not present');
							})
							.getText(Selectors.ReturnAuthorization.itemReason.replace('%s', item.internalId)).then(function (text){
								expect(text).toMatch(item.reason);
							})
						;
						if (item.options)
						{
							item.options.forEach(function (option)
							{
								client
									.isExisting(Selectors.ReturnAuthorization.itemOptionLabel.replace('%s', item.internalId).replace('%d', option.title)).then(function (isExisting)
									{
										expect(isExisting).toBeTrueOrFailWith('Option ' + option.title + ' is not present in item ' + item.name);
										if(isExisting)
										{
											client
												.getText(Selectors.ReturnAuthorization.itemOptionValue.replace('%s', item.internalId).replace('%d', option.title)).then(function (text){
													expect(text).toMatch(option.value);
												})
											;
										}
									})

								;
							})
						}
					}
				})
			;
		})

		client
			.call(cb)
		;
	}

,	verifyComment: function (returnComment, cb)
	{
		var client = this;
		if(returnComment)
		{
			client			
				.getText(Selectors.ReturnAuthorization.returnComment).then(function (text)
				{
					expect(text).toBe(returnComment);
				})
			;
		}
		client
			.call(cb)
		;
	}

,	verifyAppliedToTransactions: function(appliedTransactions, cb)
	{
		var client = this;
		client
			.getText(Selectors.ReturnAuthorization.appliedToTransaction).then(function (text)
			{
				expect(text).toMatch('Applied to Transactions');
			})
		;

		if (appliedTransactions.message)
		{
			client
				.getText(Selectors.ReturnAuthorization.noTransactionsApplied).then(function (text)
				{
					expect(text).toMatch(appliedTransactions.message);
				})
				.call(cb)
			;
		} 
		else
		{
			appliedTransactions.transactions.forEach(function (transaction)
			{
				client
					.isExisting(Selectors.ReturnAuthorization.transaction.replace('%s', transaction.transactionId)).then(function (isExisting)
					{
						expect(isExisting).toBeTrueOrFailWith('Transactions ' + transaction.transactionName + ' is not present');
						if (isExisting)
						{
							client
							.getText(Selectors.ReturnAuthorization.transactionName).then(function (text)
							{
								expect(text).toMatch(transaction.transactionName);
							})
							.getText(Selectors.ReturnAuthorization.transactionDate).then(function (text)
							{
								expect(text).toMatch(transaction.transactionDate);
							})
							.getText(Selectors.ReturnAuthorization.transactionAmount).then(function (text)
							{
								expect(text).toMatch(transaction.transactionAmount);
							})
						;
						}
					})
				;
			})
			client
				.isExisting(Selectors.ReturnAuthorization.transactionsFooter).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Applied to transactions footer is not present');
					if (isExisting)
					{
						client
							.getText(Selectors.ReturnAuthorization.appliedSubtotal).then(function (text)
							{
								expect(text).toMatch(appliedTransactions.footer.appliedSubtotal);
							})
							.getText(Selectors.ReturnAuthorization.remainingSubtotal).then(function (text)
							{
								expect(text).toMatch(appliedTransactions.footer.remainingSubtotal);
							})
						;
					}
				})
				.call(cb)
			;
		}
	}

,	verifySummary: function (returnSummary, cb)
	{
		var client = this;

		if (returnSummary.discountTotal)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryDiscount).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Discount is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryDiscount).then(function (text)
				{
					expect(text).toMatch(params.discountTotal);
				})
			;
		}
		
		if (returnSummary.shippingTotal)
		{
			client
				.isExisting(Selectors.ReturnAuthorization.summaryShipping).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Shipping total is not present in summary');
				})
				.getText(Selectors.ReturnAuthorization.summaryShipping).then(function (text)
				{
					expect(text).toMatch(returnSummary.shippingTotal);
				})
			;
		}

		if (returnSummary.handlingTotal)
		{
			client
				.isExisting(Selectors.ReturnAuthorization.summaryHandling).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Handling total is not present in summary');
				})
				.getText(Selectors.ReturnAuthorization.summaryHandling).then(function (text)
				{
					expect(text).toMatch(returnSummary.handlingTotal);
				})
			;
		}

		this
			.isExisting(Selectors.ReturnAuthorization.summarySubtotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal is not present in summary');
			})
			.getText(Selectors.ReturnAuthorization.summarySubtotal).then(function (text)
			{
				expect(text).toMatch(returnSummary.subTotal);
			})

			.isExisting(Selectors.ReturnAuthorization.summarySubtotalItems).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal Items is not present in summary');
			})
			.getText(Selectors.ReturnAuthorization.summarySubtotalItems).then(function (text)
			{
				expect(text).toMatch(returnSummary.subTotalItems);
			})

			.isExisting(Selectors.ReturnAuthorization.summaryTax).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Tax total is not present in summary');
			})
			.getText(Selectors.ReturnAuthorization.summaryTax).then(function (text)
			{
				expect(text).toMatch(returnSummary.taxTotal);
			})

			.isExisting(Selectors.ReturnAuthorization.summaryTotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Total is not present in summary');
			})
			.getText(Selectors.ReturnAuthorization.summaryTotal).then(function (text)
			{
				expect(text).toMatch(returnSummary.total);
			})

			.call(cb)
		;
	}
});