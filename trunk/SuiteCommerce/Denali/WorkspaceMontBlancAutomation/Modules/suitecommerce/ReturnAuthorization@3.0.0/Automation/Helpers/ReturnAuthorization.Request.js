var moment = require('moment');

defineHelper('ReturnAuthorization.Request', 
{
	fillAndSubmitReturnRequestForm: function(params, cb)
	{
		var client = this;

		client
			.waitForAjax()
			.waitForExist(Selectors.ReturnAuthorization.returnForm, 5000)

		;
		params.itemList.forEach(function (item)
		{
			client
				.isExisting(Selectors.ReturnAuthorization.returnItemBySku.replace('%s', item.sku)).then (function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Item ' + item.name + ' is not present to be returned.');
					if (isExisting)
					{
						client
							.ReturnAuthorization.Request.selectReturnLine(item)
							
							.ReturnAuthorization.Request.fillReturnLine(item)
						;
					}
				})
			;
		})
		client
			.setValue(Selectors.ReturnAuthorization.returningComments, params.comments)

			.click(Selectors.ReturnAuthorization.returnSubmit)
			
			.call(cb)
		;
	}	

,	selectReturnLine: function(item, cb)
	{
		var client = this;
		client
			.isExisting(Selectors.ReturnAuthorization.selectReturnLine.replace('%s', item.internalId)).then( function (isComboExisting)
			{
				expect(isComboExisting).toBeTrueOrFailWith('Checkbox for item ' + item.name + ' is not present at return page.');
				client.isExisting(Selectors.ReturnAuthorization.selectReturnLineCheck.replace('%s', item.internalId)).then( function (isClicked)
				{
					if(!isClicked)
					{
						client
						.click(Selectors.ReturnAuthorization.selectReturnLine.replace('%s', item.internalId))	
					}
				})
			})
			.call(cb)
		;
	}

,	fillReturnLine: function(item, cb)
	{
		this
			.doubleClick(Selectors.ReturnAuthorization.returnItemQuantityBySku.replace('%s', item.sku))
			/*
			* This awful delete keys can be removed as soon as testcase use preconditions in order to change xpath selector to css one 
			*/
			.keys("\uE017")
			.keys("\uE017")
			.keys("\uE017")
			.keys("\uE017")
			.keys(String(item.quantity))

			.click(Selectors.ReturnAuthorization.returnAddComment)

			.selectByVisibleText(Selectors.ReturnAuthorization.returnItemReasonBySku.replace('%s', item.sku), item.reason)

			.call(cb)
		;
	}

,	getReturnData: function (header, cb)
	{
		var client = this;

		client
			.getText(Selectors.ReturnAuthorization.confirmationReturnNo).then( function (text)
			{
				header.returnNo = text.split('#')[1]
				header.returnDate = moment().format('MM/DD/YYYY');
			})

			.call(function()
			{
				cb(null, header);
			})
		;
	}

,	goToCreatedReturn: function (cb)
	{
		this
			.click(Selectors.ReturnAuthorization.confirmationReturnNo)
			.call(cb)
		;
	}
});