// @module QuickAdd
defineHelper('QuickAdd',
{
	// @method clickAddItem Clicks Add Item
	// @return {Void}
	clickAddItem: function (cb)
	{
		this
			.waitForAjax()
			.click(Selectors.QuickAdd.btnAddItem)
			.call(cb)
		;
	}

	// @method selectFirstResult Selects the first result in quick add item searcher
	// @return {Void}
,	selectFirstResult: function (cb)
	{
		this
			.waitForAjax()
			.click(Selectors.QuickAdd.dropdownResults)
			.call(cb)
		;
	}

	// @method searchAndAddFirstResultl Search an item and adds the first
	// @params {String} item
	// @return {Void}
,	searchAndAddFirstResultl: function (item, cb)
	{
		this
			.ItemsSearcher.writeValue(item)
			.QuickAdd.selectFirstResult()
			.QuickAdd.clickAddItem()
			.call(cb)
		;
	}

});