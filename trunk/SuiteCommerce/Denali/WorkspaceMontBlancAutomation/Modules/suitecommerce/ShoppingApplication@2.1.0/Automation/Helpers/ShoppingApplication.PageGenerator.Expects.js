defineHelper('ShoppingApplication.PageGenerator.Expects', 
{
	checkMetaTags: function (expected_tags, cb) {
		var client = this;

		expected_tags.forEach(function(tag)
		{
			client.call(function(){
				client.isExisting(tag, function(err, tag_exists) 
				{
					if (!tag_exists) {
						expect(tag + " meta tag").toBe(" present.");
					}
				});
			});
		});

		client.call(cb);
	}


,	checkHomePageMetaTags: function(cb)
	{
		var home_page_metatags = [
			"meta[name='description']"
		,	"meta[name='keywords']"
		];

		this	
			.ShoppingApplication.PageGenerator.Expects.checkMetaTags(home_page_metatags)
			.call(cb)
		;
	}


,	checkShoppingPageMetaTags: function(cb)
	{
		var shopping_page_metatags = [
			"meta[name='description']"
		,	"meta[name='keywords']"
		];

		this	
			.ShoppingApplication.PageGenerator.Expects.checkMetaTags(shopping_page_metatags)
			.call(cb)
		;
	}


,	checkProductPageMetaTags: function(cb)
	{
		var product_page_metatags = [
			"[itemtype='http://schema.org/Product']"
		,	"meta[name='description']"
		,	"meta[name='keywords']"
		];

		this	
			.ShoppingApplication.PageGenerator.Expects.checkMetaTags(product_page_metatags)
			.call(cb)
		;
	}

});
