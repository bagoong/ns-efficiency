var _ = require('underscore');

var suiteTalkOperations = [
	'getDataCenterUrls'
,	'search'
,	'searchBasic'
,	'searchMoreWithId'
,	'add'
,	'addList'
,	'update'
,	'updateList'
,	'upsert'
,	'upsertList'
,	'delete'
,	'deleteList'
,	'get'
,	'getAll'
,	'getCustomizationId'
,	'initialize'
,	'initializeList'
]


var extractOperations = function(suiteTalkClient)
{
	var operations = {};

	suiteTalkOperations.forEach(function(operationName)
	{
		if (typeof suiteTalkClient.client[operationName] === 'function')
		{
			var client = suiteTalkClient.client;
			var operationFunction = suiteTalkClient.client[operationName];
			//operations[operationName] = operationFunction.bind(client);
			operations[operationName] = function()
			{
				var args = _.toArray(arguments);
				var callback = _.last(args) === 'function' ? _.last(args) : null;

				if (callback)
				{
					return operationFunction.apply(client, arguments).nodeify(callback);
				}
				else
				{
					return operationFunction.apply(client, arguments);
				}
			}
		}
	});

	return operations;
}


module.exports = function(suiteTalk)
{
	_.extend(
		suiteTalk
	,	extractOperations(suiteTalk)
	);
}