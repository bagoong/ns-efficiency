var _ = require('underscore')
	SuiteTalkResponse = require('./Response.js')
;

module.exports = {

	formatGetResponse: function(response)
	{
		response = SuiteTalkResponse.formatResponseRecord(response);
		
		var record = _.chain(response)
			.result('record')
			.result(0)
			.value()
		;

		return record;
	}

}