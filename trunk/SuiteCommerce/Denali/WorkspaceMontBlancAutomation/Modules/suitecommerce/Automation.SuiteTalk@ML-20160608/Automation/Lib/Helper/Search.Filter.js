var _ = require("underscore");

module.exports = {
	
	generateSearchFilters: function(fieldsAvailable, fieldsValues)
	{
		var self = this;

		var filters = {};

		var filterTypeHandlers = {
			'boolean'  : self.generateBooleanSearchFilter
		,	'enum'     : self.generateEnumSearchFilter
		,	'recordRef': self.generateRecordRefSearchFilter
		,	'string'   : self.generateStringSearchFilter
		,	'long'     : self.generateLongSearchFilter
		,	'integer'  : self.generateLongSearchFilter
		,	'double'   : self.generateDoubleSearchFilter
		}

		_(fieldsAvailable).forEach(function(filterType, filterName)
		{
			var filterParams = fieldsValues[filterName];

			if (_.isUndefined(filterParams))
			{
				return false;
			}

			var filterHandler = filterTypeHandlers[filterType];

			if (_.isUndefined(filterHandler))
			{
				throw '"' + filterType + '" is an invalid filter type.';
			}

			var generatedFilter = filterHandler.apply(self, [filterParams]);

			//console.log(generatedFilter); process.exit(0);

			filters[filterName] = generatedFilter;
		});

		return filters;
	}


,	generateBooleanSearchFilter: function(value)
	{
		return {
			searchValue: JSON.stringify(value)// true or false string
		}
	}

,	generateStringSearchFilter: function(params)
	{
		var value = params.value || params;

		// POSSIBLE OPERATORS:
		// contains, doesNotContain, doesNotStartWith, empty, hasKeywords, is, isNot, notEmpty, startsWith
		var operator = params.operator || 'is';

		return {
			type: 'SearchStringField'
		,	operator: operator
		,	searchValue: value
		};
	}


,	generateMultiSelectSearchFilter: function(params)
	{
		var values = params.value || params.values || params;
		var recordType = params.type || params.recordType || 'RecordRef';

		values = Array.isArray(values)? values : [ values ];

		// POSSIBLE OPERATORS: anyOf, noneOf
		var operator = params.operator || 'anyOf';

		return {
			type: 'SearchMultiSelectField'
		,	operator: operator
		,	searchValue: _(values).map(function(internalId)
			{
				return { type: recordType,	internalId: internalId + '' }
			})
		};
	}


,	generateRecordRefSearchFilter: function(values)
	{
		var params = {
			recordType: 'RecordRef'
		,	values: values
		}

		return this.generateMultiSelectSearchFilter(params);
	}


,	generateEnumSearchFilter: function(values)
	{
		values = Array.isArray(values)? values : [ values ];

		// POSSIBLE OPERATORS: anyOf, noneOf
		var operator = values.operator || 'anyOf';

		return {
			type: 'SearchEnumMultiSelectField'
		,	operator: operator
		,	searchValue: values
		};
	}

,	generateDoubleSearchFilter: function(params)
	{
		return this.generateNumberSearchFilter(params, 'SearchDoubleField')
	}

,	generateLongSearchFilter: function(params)
	{
		return this.generateNumberSearchFilter(params, 'SearchLongField')
	}

,	generateNumberSearchFilter: function(params)
	{
		if (!_(params).isObject())
		{
			params = {
				value: params
			}
		}

		var fieldType = params.fieldType || 'SearchLongField';

		// POSSIBLE OPERATORS:
		// equalTo, between, empty, greaterThan, greaterThanOrEqualTo, lessThan, lessThanOrEqualTo
		// notBetween, notEmpty, notEqualTo, notGreaterThan, notGreaterThanOrEqualTo, notLessThan, notLessThanOrEqualTo
		var operator = params.operator || 'equalTo';

		var record = {
			type: fieldType
		,	operator: operator
		};

		if (['empty', 'notEmpty'].indexOf(record.operator) === -1)
		{
			record.searchValue = params.value + '';
		}

		return record;
	}

}