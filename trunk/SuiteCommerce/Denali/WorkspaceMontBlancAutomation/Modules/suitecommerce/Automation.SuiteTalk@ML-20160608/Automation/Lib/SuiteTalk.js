
var suitetalk4node = require('suitetalk')
,	_ = require('underscore')
,	glob = require('glob').sync
,	path = require('path')
;

var SuiteTalk = function()
{
	var self = this;

	self.client = suitetalk4node;

	self.setCredentials = function(credentials)
	{
		this.client.setCredentials(credentials);
	}

	self.getCredentials = function()
	{
		return this.client.credentials;
	}

	Object.defineProperty(self, 'credentials', {
		get: self.getCredentials
	,	set: self.setCredentials
	})

	// map client operations to main object
	require('./Operation')(self);

	self.installModule = function(moduleName, moduleInstance)
	{
		moduleInstance.client = self.client;
		require('./Operation')(moduleInstance);
		self[moduleName] = moduleInstance;
	}

	var modules = glob(path.join(__dirname, 'Module/**/*.js'));

	modules.forEach(function(modulePath)
	{
		var suiteTalkModule = require(modulePath);

		self.installModule(
			path.basename(modulePath, '.js')
		,	suiteTalkModule
		);
	});

	//process.exit();
}


module.exports = SuiteTalk;