
var _ = require('underscore')
,	async = require('async')
,	SearchFilterHelper = require('../Helper/Search.Filter')
,	SearchPaginator = require('../Helper/Search.Paginator')
,	SearchResponseHelper = require('../Helper/Search.Response')
;


module.exports = {

	itemTypesForSearchEnum: [
			'assembly'               // "Assembly"    // Includes: Simple, LotNumbered, Serialized
		,	'inventoryItem'          // "InvtPart"    // Includes: Simple, LotNumbered, Serialized
		,	'nonInventoryItem'       // "NonInvtPart" // Includes: For Sale, For Resale
		,	'giftCertificateItem'    // "GiftCert"
		,	'downloadItem'           // "DwnLdItem"
		,	'discount'               // "Discount"    // Not supported with Website and isOnline filters
		,	'kit'                    // "Kit"
		,	'service'                // "Service"     // Incorrectly documented as serviceForSaleItem
	]


,	outOfStockBehaviorEnum: [
		"_default"
	,	"_allowBackOrdersButDisplayOutOfStockMessage"
	,	"_allowBackOrdersWithNoOutOfStockMessage"
	,	"_disallowBackOrdersButDisplayOutOfStockMessage"
	,	"_removeItemWhenOutOfStock"
	]


,	searchItems: function(params, resultCb)
	{
		var self = this;

		var searchFunction = SearchPaginator.createPaginatedSearch(
			self.initSearch.bind(self)
		,	self.paginateSearch.bind(self)
		);

		searchFunction(params, function(err, res)
		{
			var mergedResults = SearchPaginator.mergePaginatedSearchResponses(res);
			resultCb(err, mergedResults);
		});
	}


,	makeSearchFilters: function(params)
	{
		return SearchFilterHelper.generateSearchFilters(
			{
				'isInactive'     : 'boolean'
			,	'isOnline'       : 'boolean'
			,	'matrix'         : 'boolean'
			,	'matrixChild'    : 'boolean'
			,	'isSerialItem'   : 'boolean'
			,	'isLotItem'      : 'boolean'
			,	'isFulfillable'  : 'boolean'
			,	'isTaxable'      : 'boolean'
			,	'internalId'     : 'recordRef'
			,	'webSite'        : 'recordRef'
			,	'parent'         : 'recordRef'
			,	'subsidiary'     : 'recordRef'
			,	'itemId'         : 'string'
			,	'itemUrl'        : 'string'
			,	'urlComponent'   : 'string'
			,	'displayName'    : 'string'
			,	'upcCode'        : 'string'
			,	'type'           : 'enum'
			,	'minimumQuantity': 'long'
			}
		,	params
		);
	}


,	initSearch: function(params, cb)
	{
		var self = this;

		var filters = self.makeSearchFilters(params);

		return self.searchBasic(
			{
				recordType: 'item'
			,	filters: filters
			,	searchPreferences: {
					pageSize: params.searchPageSize || params.pageSize || 50
				}
			}
		,	function(error, response)
			{
				response = SearchResponseHelper.formatSearchResponse(response);
				cb(error, response);
			}
		);
	}

,	paginateSearch: function(params, cb)
	{
		var self = this;

		return self.searchMoreWithId(
			{
				id: params.searchId || params.id
			,	page: params.page
			}
		,	function(error, response)
			{
				response = SearchResponseHelper.formatSearchResponse(response);
				cb(error, response);
			}
		);
	}

}