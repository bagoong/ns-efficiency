var _ = require('underscore')
	SuiteTalkResponse = require('./Response.js')
;

module.exports = {

	formatSearchResponse: function(response)
	{
		response = SuiteTalkResponse.formatResponseRecord(response);
		
		var recordList = _.chain(response)
			.result('recordList')
			.result(0)
			.result('record')
			.value()
		;

		if (recordList)
		{
			response.recordList = recordList;
		}
		else if (!_.isUndefined(response))
		{
			if (!_.isUndefined(response.recordList))
			{
				response.recordList = [];
			}
		}

		return response;
	}

}