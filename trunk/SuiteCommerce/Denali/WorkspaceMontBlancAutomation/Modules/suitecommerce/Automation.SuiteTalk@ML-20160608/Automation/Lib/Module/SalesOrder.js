
var _ = require('underscore')
,	async = require('async')
,	GetResponseHelper = require('../Helper/Get.Response')
;


module.exports = {

	getSalesOrder: function (internalId, cb)
	{
		var self = this;

		self.get(
			{
				'internalId': internalId
			,	'recordType': 'salesOrder'
			}
		,	function(err, res)
			{
				res = GetResponseHelper.formatGetResponse(res);
				cb(err, res);
			}
		);
	}

}