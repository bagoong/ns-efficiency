
var args = require('yargs').argv;
var SuiteTalkClient = require('../Lib/SuiteTalk');

global.SuiteTalk = new SuiteTalkClient();

var initializeSuiteTalk = function(done)
{
	if (global.Preconditions)
	{
		SuiteTalk.setCredentials(
			Preconditions.Configuration.credentials
		);

		if (args['debug-suitetalk'] || args['debug-suite-talk'])
		{
			SuiteTalk.client.debug = true;
		}
	}

	done();
}

beforeEach(initializeSuiteTalk);

beforeAll(initializeSuiteTalk);
