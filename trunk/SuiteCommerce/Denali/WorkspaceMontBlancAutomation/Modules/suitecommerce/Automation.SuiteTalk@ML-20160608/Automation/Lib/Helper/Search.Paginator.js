
var _ = require('underscore')
,	async = require('async')
;

module.exports = {

	createPaginatedSearch: function(initSearchFunction, searchMoreFunction)
	{
		var self = this;

		return function(params, resultCb)
		{
			var resultsLimit = params.searchResultsLimit || params.limit || 500
			,	resultsCount = 0
			,	currentPage = 0
			,	totalPages = 1
			;

			var searchResults = []
				searchId = null
			;

			async.whilst(
				function ()
				{
					currentPage++;
					return (resultsCount < resultsLimit) && (currentPage <= totalPages);
				}

			,	function (cb) {
					if (currentPage === 1)
					{
						initSearchFunction(params, function(err, res)
						{
							if (err)
							{
								return cb(err);
							}

							totalPages = res.totalPages;
							searchId = res.searchId;
							resultsCount += parseInt(res.pageSize);
							searchResults.push(res);
							cb();
						});
					}
					else
					{
						var paginationParams = { id: searchId, page: currentPage };

						searchMoreFunction(paginationParams, function(err, res)
						{
							if (err)
							{
								return cb(err);
							}

							resultsCount += parseInt(res.pageSize);
							searchResults.push(res);
							cb();
						});
					}
				}

			,	function (err) {
					resultCb(err, searchResults);
				}
			);
		}
	}

,	mergePaginatedSearchResponses: function(pageResponseList)
	{
		mergedResponse = null;

		if (!_.isEmpty(pageResponseList))
		{
			mergedResponse = _.clone(pageResponseList[0]);

			if (!Array.isArray(mergedResponse.recordList))
			{
				mergedResponse.recordList = [];
			}

			for (var i=1; i < pageResponseList.length; i++)
			{
				var searchPageResponse = pageResponseList[i];

				if (!Array.isArray(searchPageResponse.recordList))
				{
					return false;
				}

				mergedResponse.recordList = mergedResponse.recordList.concat(
					searchPageResponse.recordList
				);
			}
		}

		return mergedResponse;
	}


}