var _ = require('underscore');

module.exports = {

	formatResponseRecord: function(response)
	{
		var self = this;
		response = self.unwrapResponse(response);
		response = self.formatResponseFields(response);
		return response;
	}

,	unwrapResponse: function(response)
	{
		var unwrappedResponse = response;

		var wrapperElementRegex = [
			/^[A-Za-z]+Response/
		,	/^[A-Za-z]+(Response|Result)/
		];

		wrapperElementRegex.forEach(function(reg)
		{
			var innerElement = _.find(unwrappedResponse, function(value, key)
			{
				return reg.test(key) && Array.isArray(value) && (value.length === 1);
			});
			
			if (innerElement)
			{
				unwrappedResponse = innerElement[0];
			}
		});


		return unwrappedResponse;
	}


,	formatResponseFields: function (record)
	{
		var self = this;
		var cleanrecord;

		//console.log(record);
		//console.log(typeof(record));

		if (Array.isArray(record))
		{
			if (record.length === 1 && typeof record[0] === "string")
			{
				cleanrecord = self.formatResponseFields(record[0]);
			}
			else
			{
				var cleanrecord = [];

				record.forEach(function(subrecord)
				{
					cleanrecord.push(self.formatResponseFields(subrecord));
				});
			}

		}
		else if (typeof record === "object")
		{
			cleanrecord = {}

			Object.keys(record).forEach(function(key)
			{
				cleanrecord[key] = self.formatResponseFields(record[key]);
			})

			if (!record.internalId && record.$ && record.$.internalId)
			{
				cleanrecord.internalId = (/^\d+$/.test(record.$.internalId))? parseInt(record.$.internalId) : record.$.internalId;
			}

		}
		else
		{
			if (typeof record === "string")
			{
				try
				{
					record = JSON.parse(record);
				}
				catch (err) {}
			}

			cleanrecord = record;
		}

		return cleanrecord;
	}

}