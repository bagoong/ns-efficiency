defineHelper('PaymentWizard', 
{

	clickEditPayment: function(cb)
	{
		this
			.waitForExist(Selectors.PaymentWizard.editPaymentButton, 5000)
			.click(Selectors.PaymentWizard.editPaymentButton)
			.waitForAjax()
			.call(cb)
		;
	}

,	changePaymentAmount: function(params, cb)
	{
		this
			.waitForExist(Selectors.PaymentWizard.paymentAmount, 5000)
			.setValue(Selectors.PaymentWizard.paymentAmount, params.paymentAmount)
			.click(Selectors.PaymentWizard.saveAmount)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickContinue: function(cb)
	{
		this
			.waitForExist(Selectors.PaymentWizard.buttonContinue, 5000)
			.click(Selectors.PaymentWizard.buttonContinue)
			.waitForAjax()
			.call(cb)
		;	
	}

,	clickSubmit: function(cb)
	{
		this
			.waitForExist(Selectors.PaymentWizard.buttonSubmit, 5000)
			.click(Selectors.PaymentWizard.buttonSubmit)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickMakeAnotherPayment: function(cb)
	{
		this
			.waitForExist(Selectors.PaymentWizard.buttonMakeAnotherPayment, 5000)
			.click(Selectors.PaymentWizard.buttonMakeAnotherPayment)
			.waitForAjax()
			.call(cb)
		;
	}	

});