defineHelper('OrderWizard.Module.PaymentMethod.Expects',
{
	checkExpectedCreditCard: function(expected_credit_card, cb)
	{
		this
			.GlobalViews.PaymentMethod.Expects.checkExpectedCreditCard(expected_credit_card)
			.call(cb)
		;
	}

,	checkExpectedInvoice: function(expected_invoice, cb)
	{
		this
			.GlobalViews.PaymentMethod.Expects.checkExpectedInvoice(expected_invoice)
			.call(cb)
		;
	}

,	checkErrorMessageIsDisplayed: function (cb)
	{

		this
			.waitForAjax()
			.waitForExist(Selectors.OrderWizardModulePaymentMethod.errorMessageExternalGW, 5000)
			.call(cb)
		;
	}

,	checkExpectedExternalPayment: function (external_name, cb)
	{
		this
			.waitForAjax()
			.getText(Selectors.OrderWizardModulePaymentMethod.externalPaymentName).then(function (name)
			{
				expect(name).toBe(external_name);
			})
			.call(cb)
		;
	}

});

