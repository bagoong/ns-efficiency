
defineHelper('OrderWizard.Module.PaymentMethodExternal',
{
	chooseStatus: function(params, cb)
	{
		this
			.selectByValue(Selectors.OrderWizardModulePaymentMethod.statusSelectExternalGW, params)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickConfirmOrder: function(cb)
	{
		this.click(Selectors.OrderWizardModulePaymentMethod.confirmExternalGW, cb);
	}

});

