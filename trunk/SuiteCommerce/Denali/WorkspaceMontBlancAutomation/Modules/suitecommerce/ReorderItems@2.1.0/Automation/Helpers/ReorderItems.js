defineHelper('ReorderItems', 
{

	addToCartByDataId: function (params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ReorderItems.addToCartByDataId.replace('%s', params.data_id), 5000)
	    	.click(Selectors.ReorderItems.addToCartByDataId.replace('%s', params.data_id))
	    	.waitForAjax()  
			.call(cb)
	}

,	changeQuantity: function(params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.ReorderItems.changeQuantity.replace('%s', params.data_id), 5000)	
			.setValue(Selectors.ReorderItems.changeQuantity.replace('%s', params.data_id), params.quantity)
			.waitForAjax()
			.call(cb)
	}	


});