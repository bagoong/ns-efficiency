// @module RequestQuoteWizard

var _ = require('underscore');

defineHelper('RequestQuoteWizard.Expects',
{
	// @method requestAQuoteLink Expects the Request a Quote link to be present in the page
	// @return {Void}
	requestAQuoteLink: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.RequestQuoteAccessPoints.linkRequestAQuote)
			.isExisting(Selectors.RequestQuoteAccessPoints.linkRequestAQuote)
			.then(function (err, existing)
			{
				expect(existing).toBeTrue;
			})
			.call(cb)
		;
	}

	// @method itemCounter Verifies the correct item count in the header
	// @params {Number} count Is the number of item added
	// @return {Void}
,	itemCounter: function (count, cb)
	{
		client = this;
		// This waitForAjax is necessary
		this.waitForAjax();
		if (count === 0)
		{
			client
				.getText(Selectors.RequestQuoteWizard.textItemCounterEmpty)
				.then(function (text)
				{
					expect(text).toBe("No Items Yet");
				})
			;
		}
		else if (count === 1)
		{
			client
				.getText(Selectors.RequestQuoteWizard.textItemCounter)
				.then(function (text)
				{
					expect(text).toBe(count + " Item");
				})
			;
		}
		else
		{
			client
				.getText(Selectors.RequestQuoteWizard.textItemCounter)
				.then(function (text)
				{
					expect(text).toBe(count + " Items");
				})
			;
		}
		client.call(cb);
	}

	// @method itemsData Verifies the correct items data
	// @params {Array<Automation.Items.Model>} items
	// @return {Void}
,	itemsData: function (items, cb)
	{
		client = this;

		if (!_.isArray(items))
		{
			items = [items];
		}

		_.each(items, function (item)
		{
			//console.log(item);
			client
				.getText(Selectors.RequestQuoteWizard.textSku.replace('%s', item.internalId))
				.then(function (text)
				{
					expect(text).toBe(item.itemId);
				})
			;
		})
		client.call(cb);
	}

	// @method thankYouPage Verifies the thank you page
	// @return {Void}
,	thankYouPage: function (cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.RequestQuoteWizard.titleThankYou)
			.getText(Selectors.RequestQuoteWizard.titleThankYou)
			.then(function (text)
			{
				expect(text).toBeSameText('THANK YOU!');
			})
			.call(cb)
		;
	}

});