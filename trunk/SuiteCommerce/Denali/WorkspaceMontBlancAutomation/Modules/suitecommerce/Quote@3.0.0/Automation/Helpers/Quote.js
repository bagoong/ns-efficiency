defineHelper('Quote', 
{

	enterQuoteById: function (params, cb)
	{
		var client = this;
		this.isExisting(Selectors.Quote.dataId.replace('%s', params.quoteId)).then(function (isExisting)
		{
			if (isExisting)
			{
				client
					.click(Selectors.Quote.dataId.replace('%s', params.quoteId))
					.waitForAjax()
					.call(cb)
				;
			}
			else
			{
				client.GlobalViews.Pagination.nextPageExistence().then(function (nextPageExists)
				{
					if (nextPageExists)
					{
						client
							.GlobalViews.Pagination.clickGoToNextPage()
							.Quote.enterQuoteById(params)
							.call(cb)
						;
					}
					else
					{
						throw 'Quote not found, ending test...';
					}
				});
			}
		});
	}

,	clickReviewAndPlaceOrderButton: function (cb)
	{
		this
			.click(Selectors.Quote.buttonReviewAndPlaceOrder)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickButtonBacktoListofQuotes: function (cb)
	{
		this
			.click(Selectors.Quote.buttonBacktoListofQuotes)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickLinkToOrder: function (cb)
	{
		this
			.click(Selectors.Quote.linkToOrder)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickHeader: function (cb)
	{
		this
			.click(Selectors.Quote.headerQTSOW)
			.waitForAjax()
			.call(cb)
		;
	}

,	clickPlaceOrderButton: function (cb)
	{
		this
			.click(Selectors.Quote.buttonPlaceOrder)
			.waitForAjax()
			.call(cb)
		;
	}


,	openAccordeons: function(cb)
	{
		var client = this;
		this
			.waitForExist(Selectors.Quote.detailAccordeon, 5000)
			.isExisting(Selectors.Quote.detailAccordeonCollapsed, function (err, isExisting)
			{
		        if(isExisting)
		        {	    	
					client.Quote.openClosedAccordeon();
		        }
		    })
			.isExisting(Selectors.Quote.detailAccordeonCollapsed, function (err, isExisting)
			{
		        if(isExisting)
		        {	    	
					client.Quote.openClosedAccordeon();
		        }
		    })
			.isExisting(Selectors.Quote.detailAccordeonCollapsed, function (err, isExisting)
			{
		        if(isExisting)
		        {	    	
					client.Quote.openClosedAccordeon();
				}
			})

			.call(cb)
					;
		        }

,	openClosedAccordeon: function (cb)
	{
		this
			.click(Selectors.Quote.detailAccordeonCollapsed)
			.waitForAjax()
			.call(cb)
		;
	}	

});	