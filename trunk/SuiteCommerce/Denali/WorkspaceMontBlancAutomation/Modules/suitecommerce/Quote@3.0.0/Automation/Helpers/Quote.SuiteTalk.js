// @module Quote

// @class Quote.SuiteTalk
defineHelper('Quote.SuiteTalk',
{
	// @method getCustomerQuotes Returns the quotes from certain customer
	// @params {Automation.Customer.Model} customer
	// @return {Void}
	getCustomerQuotes: function (customer, cb)
	{
		Preconditions.Quote.getQuotesByCustomer(
			customer
		,	Preconditions.Configuration.credentials
		,	cb
		);
	}
});