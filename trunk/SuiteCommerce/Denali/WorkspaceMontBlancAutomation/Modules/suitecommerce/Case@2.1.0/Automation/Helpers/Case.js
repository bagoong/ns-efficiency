defineHelper('Case', 
{
	fillsNewCase: function (params, cb)
	{
		this
			.waitForExist(Selectors.Case.caseTitle, 5000)
			.setValue(Selectors.Case.caseTitle, params.caseSubject)
			.selectByVisibleText(Selectors.Case.caseCategory, params.caseType)
			.setValue(Selectors.Case.caseMessage, params.caseMessage)
			.call(cb)
		;
	}

,	clickSubmitCase: function(cb)
	{
		this
			.waitForExist(Selectors.Case.submitButton, 5000)
			.click(Selectors.Case.submitButton)
			.call(cb)
		;
	}

,	enterOrderByCaseId: function(params, cb)
	{
		this
			.waitForExist(Selectors.Case.overviewDataId.replace('%s', params.caseId), 5000)
			.click(Selectors.Case.overviewDataId.replace('%s', params.caseId))
			.waitForAjax()
			.call(cb)
		;
	}		

,	writeAndSendAReply: function(params, cb)
	{
		this
			.waitForExist(Selectors.Case.replyText, 5000)
			.setValue(Selectors.Case.replyText, params.caseMessage)
			.click(Selectors.Case.replyButton)
			.waitForAjax()
			.call(cb)
		;
	}

,	takeCaseId: function(params, cb)
	{
		this
			.waitForExist(Selectors.Case.idAfterSuccess, 5000)
			.getAttribute(Selectors.Case.idAfterSuccess, 'href', function(err, text){
				params.caseId = text.replace(/\D/g, '');
			})
			.call(cb)
		;
	}	

,	clickCloseCase: function(cb)
	{
		this
			.waitForExist(Selectors.Case.closeCaseButton, 5000)
			.click(Selectors.Case.closeCaseButton)
			.waitForAjax()
			.call(cb)
		;
	}

});	