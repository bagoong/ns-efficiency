defineHelper('Case.Expects',
{
	expectsSuccessMessage: function (params, cb)
	{
		this
			.waitForExist(Selectors.Case.successMessage, 5000)
			.getText(Selectors.Case.successMessage, function(err, text){
				expect(text).toMatch(params.caseSuccess);
			})
			.call(cb)
		;
	}

,	expectsCorrectInformation: function (params, cb)
	{
		this
			.waitForExist(Selectors.Case.overviewDataId, 5000)

			.getText(Selectors.Case.overviewDataId.replace('%s', params.caseId), function(err, text){
				expect(text).toBeSameText(params.caseNumber);
			})
			.getText(Selectors.Case.overviewStatus.replace('%s', params.caseId), function(err, text){
				expect(text).toMatch(params.caseStatus);
			})
			.getText(Selectors.Case.overviewCreationDate.replace('%s', params.caseId), function(err, text){
				expect(text).toMatch(params.caseCreationDate);
			})
			.getText(Selectors.Case.overviewDate.replace('%s', params.caseId), function(err, text){
				expect(text).toMatch(params.caseLastMessageDate);
			})
			.getText(Selectors.Case.overviewSubject.replace('%s', params.caseId), function(err, text){
				expect(text).toMatch(params.caseSubject);
			})

			.call(cb)
		;
	}

,	expectsCorrectInformationDetail: function (params, cb)
	{
		this
			.waitForExist(Selectors.Case.detailOrder, 5000)

			.getText(Selectors.Case.detailOrder, function(err, text){
				expect(text).toBeSameText(params.caseNumber);
			})
			.getText(Selectors.Case.detailStatus, function(err, text){
				expect(text).toMatch(params.caseStatus);
			})
			.getText(Selectors.Case.detailCreationDate, function(err, text){
				expect(text).toMatch(params.caseCreationDate);
			})
			.getText(Selectors.Case.detailLastMessageDate, function(err, text){
				expect(text).toMatch(params.caseLastMessageDate);
			})
			.getText(Selectors.Case.detailSubject, function(err, text){
				expect(text).toBeSameText(params.caseSubject);
			})
			.getText(Selectors.Case.detailType, function(err, text){
				expect(text).toMatch(params.caseType);
			})
			.getText(Selectors.Case.detailMessage, function(err, text){
				expect(text).toMatch(params.caseMessage);
			})

			.call(cb)
		;
	}

,	newCaseFormToExist: function(cb)
	{
		this
			.Expects.selectorsExist([
				{ value: Selectors.Case.caseTitle,  shouldExist: true },
				{ value: Selectors.Case.caseCategory,  shouldExist: true },
				{ value: Selectors.Case.caseMessage,  shouldExist: true },
				{ value: Selectors.Case.includeEmail,  shouldExist: true }
			], 1000)

			.call(cb);
	}

,	expectsCorrectStatus: function (params, cb)
	{
		this
			.waitForExist(Selectors.Case.detailStatus, 5000)
			.getText(Selectors.Case.detailStatus, function(err, text){
				expect(text).toMatch(params.caseStatus);
			})
			.call(cb)
		;
	}

,	checkSupportCasesPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Case.supportCasesPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Support Cases page is not present.');
        	})
        	.call(cb)
        ;
	}

,	checkSubmitNewCasePageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Case.submitNewCasePageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Submit New Case page is not present.');
        	})
        	.call(cb)
        ;
	}
});