defineHelperDependencies('OrderWizard.Module.Confirmation', 'GlobalViews.Modal')

defineHelper('OrderWizard', 
{

	clickContinue: function(cb) 
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderWizard.continueButton, 5000)
			.scroll(Selectors.OrderWizard.continueButton)
	    	.click(Selectors.OrderWizard.continueButton)
	    	.waitForAjax()
	    	.call(cb)
		;
	}


,	clickBack: function(cb)
	{
		this
			.waitForExist(Selectors.OrderWizard.backButton, 5000)
			.waitForEnabled(Selectors.OrderWizard.backButton, 10000)
			.scroll(Selectors.OrderWizard.backButton)
	    	.click(Selectors.OrderWizard.backButton)
	    	.waitForAjax()
	    	.call(cb)
		;	
	}

,	clickLoginLinkFromSkipLoginMessage: function(cb)
	{
		this
			.waitForExist(Selectors.OrderWizard.skipLoginLink, 5000)
			.click(Selectors.OrderWizard.skipLoginLink)
			.GlobalViews.Modal.waitForModal()
			.call(cb)
		;
	}

});