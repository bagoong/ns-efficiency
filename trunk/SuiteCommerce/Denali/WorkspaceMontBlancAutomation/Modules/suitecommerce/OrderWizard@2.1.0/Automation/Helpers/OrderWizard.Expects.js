defineHelper('OrderWizard.Expects', 
{

	checkExpectedSkipLoginMessage: function(cb)
	{
		this
			.getText('.order-wizard-step-guest-message', function(err, text)
			{
				expect(text).toMatch(/Checking out as a guest.*?please login/i)
			})
			.call(cb)
		;
		
	}

,	checkExpectedOrderWizardPage: function(cb)
	{
		this
			.getText('.wizard-header-title', function(err, text)
			{
				expect(text).toMatch(/Checkout/i, 'Current page is not Checkout Order Wizard');
			})
			.call(cb)
		;
	}

,	checkExpectedThankYouPage: function (cb) 
	{
		this
			.getText('.order-wizard-confirmation-module-title', function(err, text)
	        {
	            expect(text).toMatch(/THANK YOU FOR SHOPPING/i, 'Current page is not Thank You Page');
	        })
	        .call(cb)
		;
	}

});