var _ = require('underscore');


function validateInitialState(page_state)
{
	expect(page_state.title).toBe('products');

	expect(page_state.urlPath).toBe('/search');
	expect(Object.keys(page_state.urlFacets).length).toBe(0);

	expect(page_state.pageFacets.length).toBe(0);

	expect(page_state.hasGlobalClear).toBe(false);
	expect(page_state.productCount).toBeGreaterThan(0);
}

defineHelper('Facets.Filter.Expects', 
{

	validateAppliedFacets: function(page_state, applied_facets, cb)
	{
		var url_facet_ids = Object.keys(page_state.urlFacets);

		//validate facets from user input against facets obtained from the url
		expect(url_facet_ids.length).toBe(applied_facets.length);
		applied_facets.forEach(function(facet)
		{
			expect(page_state.urlFacets[facet.id]).toBe(facet.value);
		});

		url_facet_ids.forEach(function(facet_id)
		{
			var facet_value = page_state.urlFacets[facet_id];

			//validate facets obtained from the url against facets displayed on the website title
			expect(page_state.title).toContain(facet_value);

			//validate facets obtained from the url against facets displayed on the page
			expect(page_state.pageFacets).toContain(facet_value);
		});

		expect(url_facet_ids.length).toBe(page_state.pageFacets.length);
		cb(null);
	}

,	validateAfterApply: function(dataset, applied_facets, states, cb)
	{
		if (states.length === 0)
		{
			return cb(null);
		}

		if (states.length === 1)
		{
			validateInitialState(states[0]);
			return cb(null);
		}

		applied_facets = applied_facets.map(function(facet)
		{
			return {id: facet.id, value: facet.value.toLowerCase()};
		});

		var previous_state = states[states.length - 2];
		var current_state = states[states.length - 1];

		this
			.Facets.Filter.Expects.validateAppliedFacets(current_state, applied_facets)
			.call(function()
			{
				expect(current_state.title).not.toBe(previous_state.title);
				expect(current_state.hasGlobalClear).toBe(true);
				expect(current_state.pageFacets.length).toBe(states.length - 1);
				expect(current_state.productCount).not.toBeGreaterThan(previous_state.productCount);
			})
			.call(cb);
	}

,	validateAgainstPastState: function(dataset, applied_facets, states, cb)
	{
		//past_current_state and current_state should be the same!
		var client = this
		,	past_current_state = states[states.length-1];

		client
			.Facets.Filter.getState(function(err, current_state)
			{
				expect(current_state.title).toBe(past_current_state.title);
				expect(current_state.hasGlobalClear).toBe(past_current_state.hasGlobalClear);
				expect(current_state.pageFacets.length).toBe(past_current_state.pageFacets.length);
				expect(current_state.productCount).toBe(past_current_state.productCount);

				client
					.Facets.Filter.Expects.validateAppliedFacets(current_state, applied_facets);

			})
			.call(cb);
	}



,	checkProduct: function(product_id, exists, cb)
	{
		var selector = Selectors.Facets.productById.replace('%s', product_id);
		this
			.GlobalViews.Pagination.searchBySelector(selector, true, function(err, result)
			{
				expect(result.success).toBe(exists);
			})
			.call(cb);
	}

	//@method checkNumericOptions
	//@param {facetGroupSelector:String,facetOptionsSelector:String, numberFormat: RegEx} selectors
	//@param cb
,	checkNumericOptions: function(options, cb)
	{
		var client = this;

		var options_selector = options.facetGroupSelector + ' ' + options.facetOptionsSelector;

		client

			.getText(options_selector, function(err, result)
			{
				result = _.isUndefined(result) ? [] : result;
				result = _.isArray(result) ? result : [result];

				result.forEach(function(option)
				{
					expect(option).toMatch(options.numberFormat);
				});
			})

			.call(cb);
	}
});