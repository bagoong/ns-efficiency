defineHelper('ItemDetails.Expects',
{
	correctlyAddedToWishList : function(cb)
	{
		this
			.ItemDetails.correctlyAddToWishlist(function (err, confirmationText){
    			expect(confirmationText).toEqual("Good! You added this item to your product list");
    		})
    		.call(cb)
    	;
	}

,	isItemDetailsCorrectly: function(params, cb)
	{
		var client = this;

		this
			.waitForAjax()
			.waitForExist(Selectors.ItemDetails.headerTitle, 10000)
			.getText(Selectors.ItemDetails.headerTitle).then(function (text) {
				if(params.Name)
				{
					expect((text).toLowerCase()).toBe((params.Name).toLowerCase())
				}
				else
				{
					console.log("Why are we not expecting name? Why? Why?")
				}

			})

			.getText(Selectors.ItemDetails.price).then(function (text) {
				if(params.price)
				{
					expect(client.util.moneyToFloat(text)).toBe(params.price)
				}
				else
				{
					console.log("Why are we not expecting price? Why? Why?")
				}

			})
			.getText(Selectors.ItemDetails.sku).then(function (text) {
				if(params.sku)
				{
					expect(text).toEqual(params.sku);
				}
				else
				{
					console.log("Why are we not expecting sku? Why? Why?")
				}

			})

			.call(cb)
		;
	}

,	isAddToCartButtonDisable: function(disable, cb)
	{
		this
			.getAttribute('[data-type="add-to-cart"]', 'disabled', function(err, disabled)
			{
				var isDisable = disabled ? true : false;
				expect(isDisable).toBe(disable);
			})
			.call(cb)
		;
	}
})
