var suitetalk = require('suitetalk')
,	_ = require('underscore');

// NS TRANSACTIONS DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/estimate.html
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/salesorder.html

// @class Preconditions.Transaction
Preconditions.Transaction = {

	searchTransactions: function (params, cb)
	{
		var self = this;

		suitetalk.setCredentials(Preconditions.Configuration.credentials);

		var searchParams = {
			recordType: 'transaction'
		}

		params.type = 'creditMemo';

		searchParams.filters = Preconditions.Helpers.generateSearchFilters(
			{
				'entityStatus'   : 'recordRef'
			,	'internalId'     : 'recordRef'
			}
		,	params
		);

		if (params.customer)
		{
			var customerFilters = Preconditions.Helpers.generateSearchFilters(
			{
				'internalId' : 'recordRef'
			,	'email'      : 'string'
			}
			,	params.customer
			);

			searchParams.joins = [{
				recordType: 'customer'
			,	filters: customerFilters
			}];
		}

		suitetalk.search(
			searchParams
		)

		.then(function (response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			var records = response.records;

			records = _(records).map(self.formatTransactionRecordResponseFields, self);

			cb(null, records);
		})
		.catch(function (error)
		{
			cb(error);
		});
	}


,	formatTransactionRecordResponseFields: function (record)
	{
		var self = this;

		if (record.entityStatus)
		{
			record.entityStatusInternalId = record.entityStatus[0].internalId;
			record.entityStatusName = record.entityStatus[0].name.toString();
		}

		record.total = '$' + record.total.toFixed(2);
		record.subTotal = '$' + record.subTotal.toFixed(2);

		record.shippingCost = record.shippingCost ? '$' + record.shippingCost : '';
		
		record.taxTotal === 0 ?
			record.taxTotal = '$0.00' :
			record.taxTotal = '$' + record.taxTotal.toString();
		
		record.tranId = record.tranId.toString();

		record.message = record.message ? record.message.replace(/<br>/g, '\n') : '';

		if (record.shippingAddress)
		{
			var rawShippingAddress = record.shippingAddress[0];
			record.shippingAddress = Preconditions.Address.wrapAddress(rawShippingAddress);
			delete record.shippingAddress.$;
		}

		if (record.billingAddress)
		{
			var rawBillingAddress = record.billingAddress[0];
			record.billingAddress = Preconditions.Address.wrapAddress(rawBillingAddress);
			delete record.billingAddress.$;
		}

		var dateFields = [
			'dueDate', 'tranDate', 'createdDate', 'lastModifiedDate', 'expectedCloseDate'
		]

		dateFields.forEach(function (fieldName)
		{
			if (record[fieldName])
			{
				record[fieldName] = new Date(record[fieldName]).toISOString().substring(0,10);				
			}
		})
		
		record.itemList = self.formatTransactionRecordResponseItemList(record.itemList);

		return record;
	}


,	formatTransactionRecordResponseItemList: function (itemList)
	{
		itemList = itemList[0].item;

		itemList = _.map(itemList, function(wrappedItem)
		{
			wrappedItem = _.clone(wrappedItem);

			var itemEntry = wrappedItem.item[0];

			wrappedItem.internalId = itemEntry.internalId;
			wrappedItem.name = itemEntry.name;

			if (Array.isArray(wrappedItem.taxCode))
			{
				var taxCodeEntry = wrappedItem.taxCode[0];
				wrappedItem.taxCode = taxCodeEntry.internalId;
				wrappedItem.taxCodeName = taxCodeEntry.name;
			}

			if (Array.isArray(wrappedItem.price))
			{
				var priceEntry = wrappedItem.price[0];
				wrappedItem.priceLevel = priceEntry.internalId;
				wrappedItem.priceLevelName = priceEntry.name;
				delete wrappedItem.price;
			}


			if (_.isArray(wrappedItem.options))
			{
				var opts = wrappedItem.options[0].customField;

				wrappedItem.options = _.map(opts, function(optEntry)
				{
					return {
						'optionInternalId': optEntry.$.internalId
					,	'optionScriptId': optEntry.$.scriptId
					,	'valueInternalId': optEntry.value[0].internalId
					,	'valueName': optEntry.value[0].name
					}
				});
			}

			return wrappedItem;
		});

		return itemList;
	}	


,	createTransaction: function (params, cb)
	{
		this.createTransactions(
			[params]
		,	function(err, res)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, res[0]);
				}
			}
		);
	}


,	createTransactions: function (params, cb)
	{
		var self = this;

		var recordType = params.type || params.recordType || params.transactionType;

		var credentials = self.checkCredentials(params);

		var records, recordDefaults;

		if (Array.isArray(params))
		{
			records = params;
			recordDefaults = {};
		}
		else
		{
			records = params.transactions || params.records;
			recordDefaults = params;
		}

		var formattedRecords = _(records).map(function(record)
		{
			record = _(record).defaults(recordDefaults);

			// example: _estimate, _salesOrder, etc
			var localRecordType = record.type || record.recordType || record.transactionType || recordType;

			var recordNamespace = 'nstransactions';

			if (localRecordType === 'creditMemo')
			{
				recordNamespace = 'nscustomers';
			}

			record.recordNamespace = recordNamespace;

			var formattedRecord = {
				recordType: localRecordType
			,	recordNamespace: recordNamespace
			,	fields: self.formatTransactionAddRequestFields(record)
			};

			return formattedRecord;
		});

		suitetalk.setCredentials(credentials);

		suitetalk.addList({
			recordType: recordType
		,	recordNamespace: formattedRecords[0].recordNamespace //'nstransactions'
		,	records: formattedRecords

		}).then(function (response)
		{
			var responseData = response.addListResponse[0].writeResponseList[0];
			//TODO: check errors

			if (responseData.status[0].$.isSuccess === 'true')
			{
				var results = [];

				responseData.writeResponse.forEach(function (responseDataEntry)
				{
					results.push({
						internalId: responseDataEntry.baseRef[0].$.internalId
					,	type: responseDataEntry.baseRef[0].$.type
					});
				})

				cb(null, results);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function (err)
		{
			cb(err, null);
		})
	}


,	formatTransactionAddRequestFields: function (params)
	{
		var transactionType = params.type || params.recordType || params.transactionType;

		var transactionTypeName = transactionType.replace(/^_/, '');
		transactionTypeName = transactionTypeName[0].toUpperCase() + transactionTypeName.substring(1, transactionTypeName.length);

		var fields = [];

		params.entity = params.entity || params.customer;

		if (transactionType !== 'itemFulfillment')
		{
			params.taxItem = params.taxItem || params.tax || params.salesTaxItem || Preconditions.Tax.getNullTax();
		}
		
		//params.entityStatus = params.entityStatus || '10'

		var recordRefFieldNames = [
			'entity'
		,	'taxItem'
		,	'entityStatus'
		,	'salesRep'
		,	'promoCode'
		,	'discountItem'
		,	'messageSel'
		,	'shipMethod'
		,	'location'    // used for all transactions except Estimate
		,	'createdFrom' // used from transactions created with initialize()
		]

		recordRefFieldNames.forEach(function (fieldName)
		{
			if (typeof(params[fieldName]) !== 'undefined') {
				var recordRefVal = params[fieldName];

				if (recordRefVal && recordRefVal.internalId)
				{
					recordRefVal = recordRefVal.internalId;
				}

				fields.push({
					name: fieldName
				,	internalId: recordRefVal + ''
				,	nstype: 'RecordRef'
				});
			}
		});

		var valueFieldNames = [
			'tranId', 'tranDate', 'dueDate', 'probability'
		,	'orderStatus' // used for SalesOrder, refer to enumerate this.salesOrderStatuses
		,	'createdFromShipGroup' // used for item fulfillment
		,	'shipStatus'  // used from itemFulfillment
		];

		valueFieldNames.forEach(function (fieldName)
		{
			if (typeof params[fieldName] !== 'undefined' && params[fieldName] !== null)
			{
				fields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				//,	namespace: 'nscommon'
				});
			}
		});

		var wrapped_item_list = [];

		params.items = params.items || params.itemList;
		params.items = (Array.isArray(params.items)) ? params.items : [params.items];
		params.items = params.items.filter(function(item) { return !_.isEmpty(item) });

		params.items.forEach(function (item)
		{
			var itemQuantity = item.quantity || 1;

			if (item.isMatrix && !_(item.matrixChilds).isEmpty())
			{
				item = _.sample(item.matrixChilds);
			}

			var wrappedItem = {
				name: 'item'
			,	nstype: transactionTypeName + 'Item'
			,	fields: [
					{
						name: 'item'
					,	internalId: item.internalId
					,	nstype: 'RecordRef'
					}
				,	{
						name: "quantity"
					,	value: itemQuantity
					}
				,	{
						name: 'location'
					,	internalId: item.tranLocation
					,	nstype: 'RecordRef'						
					}
				]
			}

			var extraItemValueFields = ["orderLine", "quantityRemaining", "itemIsFulfilled"];

			extraItemValueFields.forEach(function(extraItemValueField)
			{
				if (!_.isUndefined(item[extraItemValueField]))
				{
					wrappedItem.fields.push({
						name: extraItemValueField
					,	value: '' + item[extraItemValueField]
					})
				}
			});

			wrapped_item_list.push(wrappedItem);
		});

		if (!_.isEmpty(wrapped_item_list))
		{
			fields.push({
				name: 'itemList'
			,	nstype: transactionTypeName + 'ItemList'
			,	fields: wrapped_item_list
			});
		}

		return fields;
	}


,	initializeItemFulfillment: function(params, cb)
	{
		var self = this;
		var credentials = self.checkCredentials(params);

		var referenceRecord = params.record || params.reference || params.salesOrder;

		if (params.salesOrder)
		{
			referenceRecord.type = "salesOrder";
		}

		suitetalk
			.initialize({
				type: 'itemFulfillment'
			,	reference: referenceRecord
			})
			.then(function(response)
			{
				var record = response.initializeResponse[0].readResponse[0].record[0];
				record = Preconditions.Helpers.formatResponseRecord(record);
				cb(null, record);
			})
			.fail(function(err)
			{
				cb(err);
			})
		;
	}


,	fulfillTransaction: function(params, cb)
	{
		var self = this;
		var credentials = self.checkCredentials(params);

		Preconditions.Transaction.initializeItemFulfillment(params, function(err, initializedRecord)
		{
			if (err)
			{
				cb(err);
			}
			else
			{

				var createParams = {
					customer: initializedRecord.entity[0].internalId
				,	recordType: 'itemFulfillment'
				,	tranDate: initializedRecord.tranDate
				,	createdFrom: initializedRecord.createdFrom[0].internalId
				,	createdFromShipGroup: initializedRecord.createdFromShipGroup
				,	shipStatus: '_shipped'
				}

				var itemList = self.formatTransactionRecordResponseItemList(
					initializedRecord.itemList
				)

				createParams.items = itemList;

				self.createTransaction(createParams, cb);
			}
		});
	}


,	salesOrderStatuses: [
		"_pendingApproval"
	,	"_pendingFulfillment"
	,	"_cancelled"
	,	"_partiallyFulfilled"
	,	"_pendingBillingPartFulfilled"
	,	"_pendingBilling"
	,	"_fullyBilled"
	,	"_closed"
	,	"_undefined"
	]

,	itemFulfillmentShipStatuses: [
		"_packed"
	,	"_picked"
	,	"_shipped"
	]

,	checkCredentials: function(params)
	{
		var credentials = params.credentials || Preconditions.Configuration.credentials;

		if (_.isUndefined(credentials))
		{
			throw new Error("SuiteTalk credentials should be defined for transaction creation.");
		}

		return credentials;
	}

}