var suitetalk = require('suitetalk');

Preconditions.Employee = {

	searchEmployee: function (params, cb)
	{
		var credentials = Preconditions.Configuration.credentials;
		var self = this;

		var filters = Preconditions.Helpers.generateSearchFilters(
			{
				'email': 'string'
			,	'internalId': 'recordRef'
			,	'salesRep': 'boolean'
			,	'subsidiary': 'recordRef'
			}
		,	params
		);

		suitetalk.setCredentials(credentials);

		suitetalk.searchBasic({
			recordType: 'employee'
		,	filters: filters
		})
		.then(function (response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			var records = response.records;

			cb(null, records);
		})
		.catch(function (error)
		{
			console.log(error);
			cb(error);
		});
	}

}