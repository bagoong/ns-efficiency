
// This is an extension to Customer.js to manage permissions for customers
// Permissions should be created manually and defined in the configuration file

var	suitetalk = require('suitetalk')
,	_ = require('underscore');

Preconditions.Permissions = {

	getPermissionsTemplate: function (cb)
	{
		template = {
			quotes_00: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'None'
				,	'estimates': 'None'
				}
			}
		,	quotes_10: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'View'
				,	'estimates': 'None'
				}
			}
		,	quotes_20: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'Create'
				,	'estimates': 'None'
				}
			}
		,	quotes_30: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'Edit'
				,	'estimates': 'None'
				}
			}
		,	quotes_01: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'None'
				,	'estimates': 'View'
				}
			}
		,	quotes_11: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'View'
				,	'estimates': 'View'
				}
			}
		,	quotes_21: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'Create'
				,	'estimates': 'View'
				}
			}
		,	quotes_31: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'Edit'
				,	'estimates': 'View'
				}
			}
		,	quotes_02: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'None'
				,	'estimates': 'Create'
				}
			}
		,	quotes_12: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'View'
				,	'estimates': 'Create'
				}
			}
		,	quotes_22: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'None'
				,	'estimates': 'Create'
				}
			}
		,	quotes_32: {
				'internalId': null
			,	permissions: {
					'salesOrder': 'Edit'
				,	'estimates': 'Create'
				}
			}
		};

		return template;
	}

,	updateAccessRole: function (params, credentials, cb)
	{
		var customer = params.customer
		,	accessRole = params.accessRole.internalId;

		if (accessRole === null)
		{
			return pending('  ' + params.accessRole.name + ' permission internalId is not defined in the Configuration file.');
		}

		suitetalk.setCredentials(credentials);

		suitetalk.update({
			recordType: 'customer'
		,	internalId: customer.internalId
		,	fields: [
				{
					name: 'accessRole'
				,	nstype: 'RecordRef'
				,	type: 'customerStatus'
				,	internalId: accessRole
				}
			]
		})
		.then(function (response)
		{
			//var records = response.searchResponse[0].searchResult[0].recordList[0].record;
			//console.log(JSON.stringify(response, null, 4))
			response = Preconditions.Helpers.formatUpdateResponse(response);
			cb(null, response);
		})
		.catch(function (error)
		{
			cb(error, null);
		});
	}
}

Preconditions.add(
		'permissions'
	,	['configuration']
	,	function (config, cb)
	{
		Preconditions.Configuration.addSection(
			'accessRoles'
		,	Preconditions.Permissions.getPermissionsTemplate()
		);

		var perm = Preconditions.Configuration.accessRoles;
		_.each(perm, function (value, key)
		{
			perm[key].name = key;
		});
		cb(null, perm);
});

Preconditions.add(
		'create_customer_quotes_00'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_00;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_01'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_01;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_02'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_02;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_10'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_10;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_11'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_11;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_12'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_12;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_20'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_20;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_21'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_21;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_22'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_22;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_30'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_30;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_31'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_31;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);

Preconditions.add(
		'create_customer_quotes_32'
	,	[
			'configuration'
		,	'create_one_customer'
		,	'permissions'
		]
	,	function (configuration, customer, permissions, cb)
		{
			var credentials = configuration.credentials
			,	permission = permissions.quotes_32;

			Preconditions.Permissions.updateAccessRole(
				{
					customer: customer
				,	accessRole: permission
				}
			,	credentials
			,	function ()
				{
					cb(null, customer)
				}
			);
		}
	);