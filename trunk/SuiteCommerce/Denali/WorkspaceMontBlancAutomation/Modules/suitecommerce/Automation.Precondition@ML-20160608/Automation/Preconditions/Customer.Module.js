var	suitetalk = require('suitetalk')
,	_ = require('underscore');


Preconditions.Customer = {

	customerRoles: {
		admin: 3
	,	customerCenter: 14
	}

,	customerStatuses: {
		ClosedWon: "13"
	,	ClosedLost: "14"
	,	Qualified:  "7"
	,	InDiscussion:  "8"
	,	IdentifiedDecisionMakers: "9"
	,	Proposal: "10"
	,	InNegotiation: "11"
	,	Purchasing: "12"
	}


,	addCustomerExtraFields: function(record)
	{
		var self = this;

		record.addressList = Preconditions.Address.getFormatedAddressListFromCustomer(record);
		record.defaultShippingAddress = null;
		record.defaultBillingAddress = null;

		record.addressList.forEach(function (address)
		{
			if (address.defaultShipping)
			{
				record.defaultShippingAddress = address;
			}

			if (address.defaultBilling)
			{
				record.defaultBillingAddress = address;
			}
		});

		record.creditCardsList = record.creditCardsList[0].creditCards;
		record.creditCardsList.forEach(function (creditCard)
		{
			creditCard.cardEnding = creditCard.ccNumber.substr(creditCard.ccNumber.length - 4);
			creditCard.expMonth = creditCard.ccExpireDate.split('-')[1]; 
			creditCard.expYear = creditCard.ccExpireDate.split('-')[0];
			creditCard.name = creditCard.ccName;
			creditCard.cardType = creditCard.paymentMethod[0].name;
			if(creditCard.ccDefault)
			{
				record.defaultCreditCard = creditCard;
			}
		});

		return record;
	}

,	formatSearchResponse: function(response)
	{
		var self = this;

		var records = response.searchResponse[0].searchResult[0].recordList[0].record;
		var is_success = response.searchResponse[0].searchResult[0].status[0].$.isSuccess;

		if (Array.isArray(records))
		{
			records = Preconditions.Helpers.formatResponseRecordList(records);
			records.forEach(function(record)
			{
				self.addCustomerExtraFields(record);
			});
		}
		else
		{
			records = [];
		}

		return {
			success: is_success
		,	records: records
		}
	}


,	searchCustomers: function (params, credentials, cb)
	{
		var self = this;
		var filters = {};

		if (params.email)
		{
			filters.email = {
				type: 'SearchStringField'
			,	operator: 'is'
			,	searchValue: params.email
			};
		}


		suitetalk.setCredentials(credentials);

		suitetalk.searchBasic({
			recordType: 'customer'
		,	filters: filters
		})
		.then(function (response)
		{
			response = self.formatSearchResponse(response);
			var records = response.records;

			cb(null, records);
		})
		.catch(function (error)
		{
			console.log(error);
			cb(error);
		});
	}


,	getCustomerByEmail: function (email, credentials, cb) {
		this.searchCustomers({
			email: email
		}, credentials, function(err, res) {

			if (!err  && res.length > 0) {
				cb(err, res[0]);
			} else {
				cb(err, null);
			}
		});
	}

,	createCustomer: function(params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		params.firstName = params.firstName || "Automation";
		params.lastName = params.lastName || "User";
		params.companyName = params.companyName || "AutomatedUberBot Inc.";
		params.isCompany = params.isCompany || false;
		params.isPerson = !params.isCompany;
		params.password2 = params.password;
		params.phone = params.phone || '1231231234';

		params.giveAccess = params.giveAccess || true;
		params.accessRole = params.accessRole || 14;
		params.entityStatus = params.entityStatus || 13;

		if (Preconditions.Configuration.website.subsidiary)
		{
			params.subsidiary = Preconditions.Configuration.website.subsidiary;
		}

		var fields = [];

		// IMPORTANT!!!! entityId param doesn't works in all accounts
		// so, it was removed.
		var valueFieldNames = [
			"password", "password2", "giveAccess"/*, "entityId"*/
		,	"firstName", "lastName", "companyName", "email"
		,	"isPerson", "phone"
		]

		valueFieldNames.forEach(function(fieldName)
		{
			var paramValue = params[fieldName];

			if (_.isNull(paramValue) || _.isUndefined(paramValue)) return false;

			fields.push({
				name: fieldName
			,	value: paramValue + ''
			})
		})

		var recordRefFieldNames = [
			"accessRole", "entityStatus", "subsidiary"
		]

		recordRefFieldNames.forEach(function(fieldName)
		{
			fields.push({
				name: fieldName
			,	internalId: params[fieldName] + ''
			,	nstype: 'RecordRef'
			});
		});

		//console.log(JSON.stringify(fields, null, 4));

		suitetalk.add({
			recordType: 'customer'
		,	fields: fields

		}).then(function(response)
		{
			var responseData = response.addResponse[0].writeResponse[0];
			//TODO: check errors
			//console.log(JSON.stringify(responseData, null, 4));

			if (responseData.status[0].$.isSuccess === "true")
			{
				//console.log(responseData.baseRef[0].$["internalId"]);
				var customer = {
					internalId: responseData.baseRef[0].$.internalId
				}

				cb(null, customer);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function(err)
		{
			//console.log(err);
			cb(err, null);
		})
	}
}
