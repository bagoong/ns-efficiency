
var	_ = require('underscore')
,	request = require('request')
,	async = require('async')
,	vm = require('vm')
;

request.defaults(
{
	rejectUnauthorized: false
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;


Preconditions.Website = {

	ENVIRONMENT: {}

,	fixUrl: function(baseUrl)
	{
		if (typeof baseUrl !== 'string')
		{
			return baseUrl;
		}

		var fixedUrl = baseUrl.trim();

		if (!/^https?:\/\//i.test(baseUrl))
		{
			fixedUrl = 'http://' + fixedUrl;
		}

		return fixedUrl;
	}

,	getConfigurationFromWebsite: function(baseUrl, done)
	{
		var self = this;

		if (!baseUrl)
		{
			return done('undefined_baseurl', null);
		}

		async.waterfall([
			function(cb)
			{
				request(baseUrl, function (error, response, body)
				{
					// Previous check was response.statusCode !== 200
					// but it didn't cover working sites with page generator error
					// that returned error 500. So, it's better to check against body content.

					if (error || !body)
					{
						return done('invalid_site', null);
					}

					var sspEnvironmentUrl = null;

					var shoppingEnvironmentRegexp = new RegExp('src\s*=\s*[\"\']([^\"\']+/shopping.environment.ssp[^\"\']+)[\"\']', 'i');
					var shoppingEnvironmentMatch = shoppingEnvironmentRegexp.exec(body);

					var checkoutSspRegexp = new RegExp('[\"\']([^\"\']+/checkout.ssp[^\"\']+)[\"\']', 'i');
					var checkoutSspMatch = checkoutSspRegexp.exec(body);

					if (!checkoutSspMatch)
					{
						var checkoutCssRegexp = new RegExp('href\s*=\s*["\'](http.*?/)css/checkout.css');
						var checkoutCssMatch = checkoutCssRegexp.exec(body);
						checkoutSspMatch = checkoutCssMatch ? ['', checkoutCssMatch[1] + 'checkout.ssp'] : checkoutSspMatch;
					}

					var inCodeEnviromentRegexp = new RegExp('<script[^>]*>(.*?window\.SC.*?)</script>', 'i');
					var inCodeEnvironmentMatch = inCodeEnviromentRegexp.exec(body);

					if (shoppingEnvironmentMatch || checkoutSspMatch)
					{
						if (shoppingEnvironmentMatch)
						{
							sspEnvironmentUrl = baseUrl + '/' + shoppingEnvironmentMatch[1];
							sspEnvironmentUrl = sspEnvironmentUrl.replace('shopping.environment.ssp', 'checkout.environment.ssp');
						}
						else
						{
							sspEnvironmentUrl = checkoutSspMatch[1].replace('checkout.ssp', 'checkout.environment.ssp');
						}

						return self.getAndEvalConfigurationFromUrl(
							sspEnvironmentUrl, cb
						);
					}
					else if (inCodeEnvironmentMatch)
					{
						//console.log(inCodeEnvironmentMatch);
						var scriptCode = inCodeEnvironmentMatch[1];
						var onlineConfiguration = self.evalConfigurationSourceCode(scriptCode);
						cb(null, onlineConfiguration);
					}
					else
					{
						done('invalid_site', null);
					}

					//console.log(body);
				});
			}

		,	function(onlineConfiguration)
			{
				onlineConfiguration.baseUrl = baseUrl;
				done(null, onlineConfiguration);
			}

		]);
	}


,	getAndEvalConfigurationFromUrl: function(sspEnvironmentUrl, cb)
	{
		var self = this;

		sspEnvironmentUrl = sspEnvironmentUrl.replace(/&amp;/ig, '&');

		request(sspEnvironmentUrl, function (error, response, scriptCode)
		{
			if (error || !scriptCode)
			{
				// Previous check was response.statusCode !== 200
				// but it didn't cover working sites with http error cache
				// that returned error 500. So, it's better to check against content.
				return cb(error, response);
			}

			var onlineConfiguration = self.evalConfigurationSourceCode(scriptCode);

			cb(null, onlineConfiguration);
		});
	}


,	evalConfigurationSourceCode: function(scriptCode)
	{
		var self = this;
		var onlineConfiguration = {};

		var sandbox = {
			window: {}
		,	SC: {
				ENVIRONMENT: {}
			}
		};

		vm.createContext(sandbox);

		try
		{
			vm.runInContext(scriptCode, sandbox);
		}
		catch (e) {}


		if (sandbox.window.SC)
		{
			_(sandbox.SC).extend(sandbox.window.SC);
		}

		if (sandbox.SC.SESSION)
		{
			_(onlineConfiguration).extend({
				passwordProtectedSite: sandbox.SC.SESSION.passwordProtectedSite || null
			});
		}

		if (sandbox.SC.ENVIRONMENT.siteSettings)
		{
			self.ENVIRONMENT = sandbox.SC.ENVIRONMENT;

			var siteSettings = sandbox.SC.ENVIRONMENT.siteSettings;

			var subsidiary = _(siteSettings.subsidiaries).find(function(entry)
			{
				return entry.isdefault === "T" && entry.internalid;
			});

			if (subsidiary)
			{
				subsidiary = subsidiary.internalid;
			}

			_(onlineConfiguration).extend({
				siteSettings: null || siteSettings
			,	siteId: null || siteSettings.siteid || siteSettings.site_n
			,	companyId: null || sandbox.SC.ENVIRONMENT.companyId || siteSettings.company
			,	checkoutSkipLogin: null || sandbox.SC.ENVIRONMENT.checkout_skip_login
			,	siteType: null || siteSettings.sitetype
			,	subsidiary: null || subsidiary
			,	order: null || siteSettings.order
			,	configuration: null || sandbox.SC.CONFIGURATION
			,	environment: null || _.omit(sandbox.SC.ENVIRONMENT, ['siteSettings', 'settings'])
			});
		}

		return onlineConfiguration;
	}


	// 'checkout', 'javascript/checkout.js'
,	getAbsoluteUrl: function(touchpoint, relativePath)
	{
		var self = this;
		var siteSettings = self.ENVIRONMENT.siteSettings;

		if (_.isUndefined(siteSettings.touchpoints[touchpoint]))
		{
			throw new Error(touchpoint + ' is not a valid touchpoint name');
		}

		var touchPointUrl = siteSettings.touchpoints[touchpoint];
		var touchPointBase = touchPointUrl.replace(/\/[^\/]+\.ssp.*/ig, '/');

		if (!/^http/i.test(touchPointBase))
		{
			var baseUrl = /checkout/i.test(touchpoint) ? 'https://' : 'http://';
			baseUrl += self.ENVIRONMENT.currentHostString + '/';
			touchPointBase = baseUrl + touchPointBase;
		}

		var absoluteUrl = touchPointBase + relativePath.replace(/^\//g, '');

		return absoluteUrl;
	}


,	getCheckoutSteps: function(cb)
	{
		var self = this;

		if (self.ENVIRONMENT.checkoutSteps)
		{
			return cb(null, self.ENVIRONMENT.checkoutSteps);
		}

		var checkoutJsUrl = self.getAbsoluteUrl('checkout', 'javascript/checkout.js');

		request(checkoutJsUrl, function (error, response, body)
		{
			if (error)
			{
				return cb(error, null);
			}

			var checkoutSteps = null;
			var checkoutStepsMatch = body.match(/SCM\[["']SC\.Checkout\.Configuration\.Steps\.([A-Za-z_]+)["']/);

			if (checkoutStepsMatch)
			{
				checkoutSteps = checkoutStepsMatch[1];
			}
			else
			{
				error = new Error('Unable to find checkout steps in ' + checkoutJsUrl);
			}

			self.ENVIRONMENT.checkoutSteps = checkoutSteps;

			return cb(error, checkoutSteps);
		});
		//SCM["SC.Checkout.Configuration.Steps.Standard"]
	}

}