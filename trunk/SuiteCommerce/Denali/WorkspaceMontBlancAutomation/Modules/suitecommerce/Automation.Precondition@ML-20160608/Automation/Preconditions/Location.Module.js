// @module AutomationCore

var	suitetalk = require('suitetalk')
,	_ = require('underscore')
,	request = require('request')
,	async = require('async');


// NS ESTIMATE DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/schema/record/location.html
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/location.html

Preconditions.Location = {

	// @method getFilteredLocations Gets all the locations and filter them with your params
	// @param {Array<String>} filters These are the filters used for the search
	// @param {Number} distance The radius to filter stores
	// @param {String} type The type of store '1' for stores, '2' for warehouses, '1,2' for stores and warehouses
	// @param {String} currentLocation Any string that can be searched in google
	// @return {Array<{Automation.Location.Model}>}
	getFilteredLocations: function (filters, distance, type, currentLocation, finalCallback)
	{
		var self = this;

		this.searchLocations(filters, function (err, stores) {
			async.waterfall(
				[
					function (cb)
					{
						if (type)
						{
							self.filterByType(type, stores, cb);
						}
						else
						{
							cb(null, stores)
						}
					}
				,	function (stores, cb)
					{
						if (distance && currentLocation)
						{
							self.filterByDistance(distance, currentLocation, stores, function (err, stores) {
								cb(null, stores)
							});
						}
						else
						{
							cb(null, stores)
						}
					}
				]
			,	finalCallback
			);
		});
	}

	// @method searchLocations It searches all the locations in your account
	// @param {Array} filter Filter to apply on the SuiteTalk Search
	// @return {Array<{Automation.Location.Model}>}
,	searchLocations: function (filter, cb)
	{
		var self = this
		,	filters;

		if (filter) {
			filters = Preconditions.Helpers.generateSearchFilters(
				{
					'internalId' : 'recordRef'
				,	'isInactive' : 'boolean'
				}
			,	filter
			);
		}

		SuiteTalk.searchBasic({
			recordType: 'location'
		,	filters: filters
		})
		.then(function (response)
		{
			stores = Preconditions.Helpers.formatSearchResponse(response);
			cb(null, stores.records);
		})
		.catch(function (error)
		{
			cb(error);
		});
	}

	// @method getCurrentIP Get the external IP
	// @return {String} External IP from pc where automation is running
,	getCurrentIP: function (cb)
	{
		request('https://api.ipify.org/', function (error, response, body)
		{
			if (!error && response.statusCode == 200)
			{
				cb(null, body);
			}
			else
			{
				cb(error);
			}
		});
	}

	// @method getCurrentIP Get current coordinates from IP
	// @return {String} Current coordinates
,	getCurrentLocation: function (cb)
	{
		var coords;
		Preconditions.Location.getCurrentIP(function (err, IP)
		{
			request('http://www.geoplugin.net/json.gp?ip=' + IP, function (error, response, body)
			{
				if (!error && response.statusCode == 200)
				{
					coords = JSON.parse(body).geoplugin_latitude + ',' + JSON.parse(body).geoplugin_longitude;
					cb(null, coords);
				}
				else
				{
					cb(error);
				}
			});
		});
	}

	// @method filterByDistance It filters the stores outside the radius (in km)
	// @param {Number} distance The distance (in KM) you want the filter radius to be
	// @param {String} currentLocation Your current location, can be a city, country, zipcode, address, coords
	// @param {Array<{Automation.Store.Model}>} stores The list of stores to be filtered
	// @return {Array<{Automation.Store.Model}>}
,	filterByDistance: function (distance, currentLocation, stores, cb)
	{
		var self = this;

		this.googleCoords(currentLocation, function (err, currentCoords) {
			_.each(stores, function (store)
			{
				if (store.latitude && store.longitude)
				{
					self.calculateDistance(currentCoords, store, function (err, distance)
					{
						store.distance = distance;
					});
				}
			});

			storesReturned = _.filter(stores, function (store) {
				return store.distance <= distance;
			});

			if (storesReturned.length === 0)
			{
				storesReturned = _.sortBy(stores, function (store) {
					return store.distance;
				})
				storesReturned = storesReturned.slice(0,3);
			}

			cb(null, storesReturned);
		});
	}

	// @method filterByType Filters a collection of stores by the type
	// @param {String} type The type of store '1' for stores, '2' for warehouses, '1,2' for stores and warehouses
	// @param {Array<{Automation.Store.Model}>} stores The list of stores to be filtered
	// @return {Array<{Automation.Store.Model}>}
,	filterByType: function (type, stores,  cb)
	{
		var siteData = Preconditions.Configuration
		,	finalUrl = siteData.website.baseUrl + '/c.' + siteData.credentials.account
			+ siteData.website.siteSettings.touchpoints.viewcart.replace('goToCart.ssp','')
			+ 'services/StoreLocator.Service.ss?c=' + siteData.credentials.account
			+ '&n=' + siteData.website.siteId + '&page=all&locationType=1,2';

		// This should NOT be done using the service created by DEV, but at the moment, SuiteTalk doesn't have the locationType field exposed
		// Issue 386101 was created for this and it will be exposed for 2016.2
		request(finalUrl, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				body = JSON.parse(body);

				_.each(stores, function (store)
				{
					sameStore = _.findWhere(body, {internalid: store.internalId + ''});
					if (sameStore)
					{
						store.locationType = sameStore.locationtype;
					}
				});

				stores = _.filter(stores, function (store) {
					if (store.locationType)
					{
						return store.locationType.match(type.replace('\'|"',''));
					}
					else
					{
						return false;
					}
				});

				cb(null, stores);
			}
			else
			{
				cb(error);
			}
		});
	}

	// @method googleCoords Gets the coordinates from google
	// @param {String} currentLocation Any string that can be searched in google
	// @return {Array<{Automation.Coords.Model}>}
,	googleCoords: function (currentLocation, cb)
	{
		request('http://maps.google.com/maps/api/geocode/json?address=' + currentLocation + '&sensor=false', function (error, response, body) {
			if (!error && response.statusCode == 200) {
				coords = JSON.parse(body).results[0].geometry.location;
				cb(null, coords);
			}
			else
			{
				cb(error);
			}
		});
	}

	// @method calculateDistance It calculates the distance between 2 points
	// @param {Array} currentCoords Coordinates where you are located
	// @param {Automation.Store.Model} store Store which you want to know the distance to, it needs to have latitude and longitude in it
	// @return {Number}
,	calculateDistance: function (currentCoords, store, cb)
	{
		var PI = Math.PI
        ,   R = 3959 // 6371 for KM, 3959 for miles
        ,   currentLat = currentCoords.lat * PI / 180
        ,   currentLon = currentCoords.lng * PI / 180
        ,	storeLat = store.latitude * PI / 180
        ,	storeLon = store.longitude * PI / 180
        ,	distance = R  * (2 * Math.atan2(Math.sqrt((Math.sin(( currentLat -  storeLat ) / 2) *
        	Math.sin(( currentLat -  storeLat ) / 2) + Math.cos( storeLat ) * Math.cos( currentLat ) *
        	Math.sin(( currentLon -  storeLon ) /2) *Math.sin(( currentLon -  storeLon ) /2))),
        	Math.sqrt(1 - (Math.sin(( currentLat -  storeLat ) / 2) *Math.sin(( currentLat -  storeLat ) / 2) +
        	Math.cos( storeLat ) * Math.cos( currentLat ) * Math.sin(( currentLon -  storeLon ) /2) *
        	Math.sin(( currentLon -  storeLon ) /2)))));

        distance = Math.round(distance*10)/10;

		cb(null, distance);
	}

	// @method addLocation Adds one location
	// @param {Automation.Location.AddRequest} location
	// @return {utomation.Store.Model.internalId}
,	addLocation: function (location, cb)
	{
		this.addLocations(
			[location]
		,	function(err, response)
			{
				if (err)
				{
					cb(err, null);
				}
				else
				{
					cb(null, response[0]);
				}
			}
		);
	}

	// @method addLocations Adds an array of locations
	// @param {Array<Automation.Location.AddRequest>} locations
	// @return {Array<Automation.Store.Model.internalId>}
,	addLocations: function (locations, cb)
	{
		var self = this;
		var records, recordDefaults;

		if (Array.isArray(locations))
		{
			records = locations;
			recordDefaults = {};
		}
		else
		{
			records = locations.location;
			recordDefaults = locations;
		}

		var formattedRecords = _(records).map(function(record)
		{
			record = _(record).defaults(recordDefaults);

			var formattedRecord = {
				recordType: 'location'
			,	recordNamespace: 'nscommon'
			,	fields: self.formatAddRequestFields(record)
			};

			return formattedRecord;
		});

		requestData = {
			recordType: 'location'
		,	recordNamespace: 'nsaccounting'
		,	records: formattedRecords

		};

		SuiteTalk.addList(requestData).then(function (response)
		{
			var responseData = response.addListResponse[0].writeResponseList[0];

			if (responseData.status[0].$.isSuccess === 'true')
			{
				var results = [];

				responseData.writeResponse.forEach(function (responseDataEntry)
				{
					results.push({
						internalId: responseDataEntry.baseRef[0].$.internalId
					});
				})

				cb(null, results);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function (err)
		{
			cb(err, null);
		})
	}

	// @method formatAddRequestFields Formats the request fields
	// @param {Automation.Location.AddRequest.Fields} params
	// @return {Automation.Location.AddRequest.Fields}
,	formatAddRequestFields: function (params)
	{
		var fields = [];

		var recordRefFieldNames = [
			'subsidiary'
		]

		recordRefFieldNames.forEach(function (fieldName)
		{
			if (typeof(params[fieldName]) !== 'undefined') {
				var recordRefVal = params[fieldName];

				if (recordRefVal && recordRefVal.internalId)
				{
					recordRefVal = recordRefVal.internalId;
				}

				fields.push({
					name: fieldName
				,	internalId: recordRefVal + ''
				,	nstype: 'RecordRef'
				});
			}
		});

		// locationType CAN'T be used here because it's not in the xds
		var valueFieldNames = [
			'name', 'latitude', 'longitude', 'automaticLatLongSetup', 'isInactive'
		];

		valueFieldNames.forEach(function (fieldName)
		{
			if (typeof params[fieldName] !== 'undefined' && params[fieldName] !== null)
			{
				fields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				});
			}
		});

		return fields;
	}
}