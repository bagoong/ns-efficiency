
var	suitetalk = require('suitetalk')
,	async = require('async')
,	_ = require('underscore')
;

Preconditions.FileCabinet = {


	searchFile: function(params, cb)
	{
		suitetalk.setCredentials(Preconditions.Configuration.credentials);

		params.name = params.fileName || params.name;

		var filters = Preconditions.Helpers.generateSearchFilters(
			{
				'availableWithoutLogin': 'boolean'
			,	'created'              : 'date'
			,	'dateViewed'           : 'date'
			,	'description'          : 'string'
			,	'documentSize'         : 'long'
			,	'externalId'           : 'recordRef'
			,	'externalIdString'     : 'string'
			,	'fileType'             : 'enum'
			,	'folder'               : 'recordRef'
			,	'internalId'           : 'recordRef'
			,	'internalIdNumber'     : 'long'
			,	'isAvailable'          : 'boolean'
			,	'isLink'               : 'boolean'
			,	'modified'             : 'date'
			,	'name'                 : 'string'
			,	'owner'                : 'recordRef'
			,	'url'                  : 'string'
			}
		,	params
		);

		suitetalk.searchBasic(
			{
				recordType: 'file'
			,	filters: filters
			}
		,	function(err, res)
			{
				res = Preconditions.Helpers.formatSearchResponse(res);
				//console.log(JSON.stringify(res, null, 4));
				cb(err, res);		
			}
		);
	}


,	getFile: function(internalId, cb)
	{
		var self = this;

		suitetalk.setCredentials(Preconditions.Configuration.credentials);

		suitetalk.get(
			{
				recordType: 'file'
			,	internalId: internalId
			}
		,	function(err, res)
			{
				cb(err, res);
			}
		);
	}


,	getFileContents: function(internalId, cb)
	{
		var self = this;

		self.getFile(internalId, function(err, res)
		{
			if (err)
			{
				cb(err);
			}
			else
			{
				var base64Data = res.getResponse[0].readResponse[0].record[0].content[0];
				var decodedContent = self.base64decode(base64Data);

				cb(err, decodedContent);
			}
		});
	}


,	base64decode: function(base64str)
	{
		return new Buffer(base64str, 'base64');
	}


}