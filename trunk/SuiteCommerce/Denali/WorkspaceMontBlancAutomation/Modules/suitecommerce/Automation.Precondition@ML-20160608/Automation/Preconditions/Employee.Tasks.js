var _ = require("underscore")

Preconditions.add(
		'get_one_sales_rep'
	,	['configuration']
	,	function (configuration, cb)
	{

	var subsidiary = configuration.website.subsidiary || null;

	Preconditions.Employee.searchEmployee({
		salesRep: true
	,	subsidiary: subsidiary
	}
	,	function (err, results)
	{
		if (_(results).isEmpty())
		{
			pending('Site has sales represeentatives');
		}

		cb(err, _(results).sample());
	});
});