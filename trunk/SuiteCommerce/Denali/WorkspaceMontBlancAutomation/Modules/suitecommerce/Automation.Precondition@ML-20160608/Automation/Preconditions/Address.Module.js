

var	suitetalk = require('suitetalk')
,	_ = require('underscore');



Preconditions.Address = {
	
	createCustomerAddress: function (params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		var customer = params.customer;

		var addresses = params.addresses || [];

		if (params.address)
		{
			addresses = (Array.isArray(params.address))? params.address : [params.address];
		}

		if (addresses.length)
		{
			addresses = _.map(addresses, self.getAddressBookFieldset, this);
		}

		var fields = [
			{
				name: 'addressbookList'
			,	nstype: 'CustomerAddressbookList'
			,	replaceAll: params.replaceAll || false
			,	fields: addresses
			}
		];

		//console.log(JSON.stringify(fields, null, 4));

		suitetalk.update({
			recordType: 'customer'
		,	internalId: customer.internalId	
		//,	recordNamespace: 'nscommon'	
		,	fields: fields

		}).then(function(response)
		{
			var responseData = response.updateResponse[0].writeResponse[0];
			//TODO: check errors
			//console.log(JSON.stringify(responseData, null, 4));

			if (responseData.status[0].$.isSuccess === "true")
			{
				//console.log(responseData.baseRef[0].$["internalId"]);
				var customer = {
					internalId: responseData.baseRef[0].$.internalId
				}
				cb(null, customer);
			}
			else
			{
				cb(responseData, null);
			}

		}).catch(function(err)
		{
			cb(err, null);
		})
	}


,	getAddressBookFieldset: function (params)
	{
		var self = this;

		params = self.unwrapAddress(params);
		params.addrPhone = params.addrPhone || params.phone;
		//params.override = params.override || true;

		var addressFields = [];

		var valueFieldNames = [
			'attention', 'addressee', 'addr1', 'addr2', 'addr3'
		,	'city', 'state', 'zip', 'addrPhone', 'override'
		];

		valueFieldNames.forEach(function(fieldName)
		{
			if (typeof params[fieldName] !== "undefined" && params[fieldName] !== null)
			{
				addressFields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				,	namespace: 'nscommon'
				});
			}
		});

		addressFields.push({
			name: 'country'
		,	value: this.getCountryConstant(params.country)
		,	nstype: 'Country'
		,	namespace: 'nscommon'
		});


		var addressBookFields = [
			{
				name: 'addressbookAddress'
			,	namespace: 'nsrelationships'
			,	nstype: 'Address'
			,	nstypeNamespace: 'nscommon'
			,	fields: addressFields
			}
		];

		var addressBookValueFieldNames = [
			'defaultBilling', 'defaultShipping', 'isResidential', 'label'
		];

		addressBookValueFieldNames.forEach(function(fieldName)
		{
			if (typeof params[fieldName] !== "undefined" && params[fieldName] !== null)
			{
				addressBookFields.push({
					name: fieldName
				,	value: params[fieldName] + ''
				,	namespace: 'nsrelationships'
				});
			}
		});

		var addressBookFieldset = {
			name: 'addressbook'
		,	nstype: 'CustomerAddressbook'
		,	fields: addressBookFields
		};

		return addressBookFieldset;
	}


,	getCountryConstant: function (country_name)
	{
		var country_constant = "";

		var country_words = country_name.split(/\s+/);

		country_words.forEach(function(word, pos)
		{
			word = word.toLowerCase();

			if (pos === 0)
			{
				word = "_" + word;
			}
			else
			{
				word = word[0].toUpperCase() + word.substring(1, word.length);
			}

			country_constant += word;
		});

		return country_constant;
	}


,	getCountryNameFromConstant: function (constant_name)
	{
		var country_name = constant_name;

		if (country_name)
		{
			country_name = country_name.replace('_', '').replace(/([A-Z])/g, ' $1');
			country_name = country_name[0].toUpperCase() + country_name.substring(1, country_name.length);
		}

		return country_name;
	}


,	getLocationCode: function (text, codes)
	{
		var found_code = false;

		text = text.trim();

		if (/^[A-Za-z]{2}$/.test(text))
		{
			text = text.toUpperCase();

			if (text in codes)
			{
				found_code = text;
			}
		}

		Object.keys(codes).forEach(function(code)
		{
			var code_text = codes[code];

			if (text === code_text)
			{
				found_code = code;
				return false;
			}
		});

		return found_code;
	}


,	getCountryCode: function (text)
	{
		var countries = {'AF':'Afghanistan', 'AX':'Aland Islands', 'AL':'Albania', 'DZ':'Algeria', 'AS':'American Samoa', 'AD':'Andorra', 'AO':'Angola', 'AI':'Anguilla', 'AQ':'Antarctica', 'AG':'Antigua and Barbuda', 'AR':'Argentina','AM':'Armenia','AW':'Aruba','AU':'Australia','AT':'Austria','AZ':'Azerbaijan','BS':'Bahamas','BH':'Bahrain','BD':'Bangladesh','BB':'Barbados','BY':'Belarus','BE':'Belgium','BZ':'Belize','BJ':'Benin','BM':'Bermuda','BT':'Bhutan','BO':'Bolivia','BQ':'Bonaire, Saint Eustatius and Saba','BA':'Bosnia and Herzegovina','BW':'Botswana','BV':'Bouvet Island','BR':'Brazil','IO':'British Indian Ocean Territory','BN':'Brunei Darussalam','BG':'Bulgaria','BF':'Burkina Faso','BI':'Burundi','KH':'Cambodia','CM':'Cameroon','CA':'Canada','IC':'Canary Islands','CV':'Cape Verde','KY':'Cayman Islands','CF':'Central African Republic','EA':'Ceuta and Melilla','TD':'Chad','CL':'Chile','CN':'China','CX':'Christmas Island','CC':'Cocos (Keeling) Islands','CO':'Colombia','KM':'Comoros','CD':'Congo, Democratic Republic of','CG':'Congo, Republic of','CK':'Cook Islands','CR':'Costa Rica','CI':'Cote d\'Ivoire','HR':'Croatia/Hrvatska','CU':'Cuba','CW':'Curaçao','CY':'Cyprus','CZ':'Czech Republic','DK':'Denmark','DJ':'Djibouti','DM':'Dominica','DO':'Dominican Republic','TL':'East Timor','EC':'Ecuador','EG':'Egypt','SV':'El Salvador','GQ':'Equatorial Guinea','ER':'Eritrea','EE':'Estonia','ET':'Ethiopia','FK':'Falkland Islands','FO':'Faroe Islands','FJ':'Fiji','FI':'Finland','FR':'France','GF':'French Guiana','PF':'French Polynesia','TF':'French Southern Territories','GA':'Gabon','GM':'Gambia','GE':'Georgia','DE':'Germany','GH':'Ghana','GI':'Gibraltar','GR':'Greece','GL':'Greenland','GD':'Grenada','GP':'Guadeloupe','GU':'Guam','GT':'Guatemala','GG':'Guernsey','GN':'Guinea','GW':'Guinea-Bissau','GY':'Guyana','HT':'Haiti','HM':'Heard and McDonald Islands','VA':'Holy See (City Vatican State)','HN':'Honduras','HK':'Hong Kong','HU':'Hungary','IS':'Iceland','IN':'India','ID':'Indonesia','IR':'Iran (Islamic Republic of)','IQ':'Iraq','IE':'Ireland','IM':'Isle of Man','IL':'Israel','IT':'Italy','JM':'Jamaica','JP':'Japan','JE':'Jersey','JO':'Jordan','KZ':'Kazakhstan','KE':'Kenya','KI':'Kiribati','KP':'Korea, Democratic People\'s Republic','KR':'Korea, Republic of','XK':'Kosovo','KW':'Kuwait','KG':'Kyrgyzstan','LA':'Lao People\'s Democratic Republic','LV':'Latvia','LB':'Lebanon','LS':'Lesotho','LR':'Liberia','LY':'Libya','LI':'Liechtenstein','LT':'Lithuania','LU':'Luxembourg','MO':'Macau','MK':'Macedonia','MG':'Madagascar','MW':'Malawi','MY':'Malaysia','MV':'Maldives','ML':'Mali','MT':'Malta','MH':'Marshall Islands','MQ':'Martinique','MR':'Mauritania','MU':'Mauritius','YT':'Mayotte','MX':'Mexico','FM':'Micronesia, Federal State of','MD':'Moldova, Republic of','MC':'Monaco','MN':'Mongolia','ME':'Montenegro','MS':'Montserrat','MA':'Morocco','MZ':'Mozambique','MM':'Myanmar (Burma)','NA':'Namibia','NR':'Nauru','NP':'Nepal','NL':'Netherlands','AN':'Netherlands Antilles (Deprecated)','NC':'New Caledonia','NZ':'New Zealand','NI':'Nicaragua','NE':'Niger','NG':'Nigeria','NU':'Niue','NF':'Norfolk Island','MP':'Northern Mariana Islands','NO':'Norway','OM':'Oman','PK':'Pakistan','PW':'Palau','PS':'Palestinian Territories','PA':'Panama','PG':'Papua New Guinea','PY':'Paraguay','PE':'Peru','PH':'Philippines','PN':'Pitcairn Island','PL':'Poland','PT':'Portugal','PR':'Puerto Rico','QA':'Qatar','RE':'Reunion Island','RO':'Romania','RU':'Russian Federation','RW':'Rwanda','BL':'Saint Barthélemy','SH':'Saint Helena','KN':'Saint Kitts and Nevis','LC':'Saint Lucia','MF':'Saint Martin','VC':'Saint Vincent and the Grenadines','WS':'Samoa','SM':'San Marino','ST':'Sao Tome and Principe','SA':'Saudi Arabia','SN':'Senegal','RS':'Serbia','CS':'Serbia and Montenegro (Deprecated)','SC':'Seychelles','SL':'Sierra Leone','SG':'Singapore','SX':'Sint Maarten','SK':'Slovak Republic','SI':'Slovenia','SB':'Solomon Islands','SO':'Somalia','ZA':'South Africa','GS':'South Georgia','SS':'South Sudan','ES':'Spain','LK':'Sri Lanka','PM':'St. Pierre and Miquelon','SD':'Sudan','SR':'Suriname','SJ':'Svalbard and Jan Mayen Islands','SZ':'Swaziland','SE':'Sweden','CH':'Switzerland','SY':'Syrian Arab Republic','TW':'Taiwan','TJ':'Tajikistan','TZ':'Tanzania','TH':'Thailand','TG':'Togo','TK':'Tokelau','TO':'Tonga','TT':'Trinidad and Tobago','TN':'Tunisia','TR':'Turkey','TM':'Turkmenistan','TC':'Turks and Caicos Islands','TV':'Tuvalu','UM':'US Minor Outlying Islands','UG':'Uganda','UA':'Ukraine','AE':'United Arab Emirates','GB':'United Kingdom (GB)','US':'United States','UY':'Uruguay','UZ':'Uzbekistan','VU':'Vanuatu','VE':'Venezuela','VN':'Vietnam','VG':'Virgin Islands (British)','VI':'Virgin Islands (USA)','WF':'Wallis and Futuna','EH':'Western Sahara','YE':'Yemen','ZM':'Zambia','ZW':'Zimbabwe'};
		return this.getLocationCode(text, countries)
	}


,	getUsStateCode: function (text)
	{
		var us_states = {'AL':'Alabama','AK':'Alaska','AZ':'Arizona','AR':'Arkansas','AE':'Armed Forces Europe','AP':'Armed Forces Pacific','CA':'California','CO':'Colorado','CT':'Connecticut','DE':'Delaware','DC':'District of Columbia','FL':'Florida','GA':'Georgia','HI':'Hawaii','ID':'Idaho','IL':'Illinois','IN':'Indiana','IA':'Iowa','KS':'Kansas','KY':'Kentucky','LA':'Louisiana','ME':'Maine','MD':'Maryland','MA':'Massachusetts','MI':'Michigan','MN':'Minnesota','MS':'Mississippi','MO':'Missouri','MT':'Montana','NE':'Nebraska','NV':'Nevada','NH':'New Hampshire','NJ':'New Jersey','NM':'New Mexico','NY':'New York','NC':'North Carolina','ND':'North Dakota','OH':'Ohio','OK':'Oklahoma','OR':'Oregon','PA':'Pennsylvania','PR':'Puerto Rico','RI':'Rhode Island','SC':'South Carolina','SD':'South Dakota','TN':'Tennessee','TX':'Texas','UT':'Utah','VT':'Vermont','VA':'Virginia','WA':'Washington','WV':'West Virginia','WI':'Wisconsin','WY':'Wyoming'}
		return this.getLocationCode(text, us_states);
	}


,	wrapAddress: function (address)
	{
		address = _.clone(address);

		if (address.attention && address.addressee)
		{
			address.fullname = address.attention;
			address.company = address.addressee;
		}
		else
		{
			address.fullname = address.addressee;
			address.company = null;
		}

		delete address.attention;
		delete address.addressee;

		address.phone = address.addrPhone;
		delete address.addrPhone;

		address.country = this.getCountryNameFromConstant(address.country);

		return address;
	}


,	unwrapAddress: function (address)
	{
		address = _.clone(address);
		
		if (address.company && address.company.trim().length > 0)
		{
			address.attention = address.fullname;
			address.addressee = address.company;
		}
		else
		{
			address.addressee = address.fullname;
			address.attention = null;
		}

		address.addrPhone = address.addrPhone || address.phone;

		delete address.phone;
		delete address.fullname;
		delete address.company;
		delete address.check;

		return address;
	}


,	getFormatedAddressListFromCustomer: function (customer)
	{
		var self = this;
		var address_list = [];

		if (!Array.isArray(customer.addressbookList))
		{
			return address_list;
		}

		address_book_list = customer.addressbookList[0].addressbook;


		address_book_list.forEach(function(address_book_entry)
		{
			var address = _.clone(address_book_entry.addressbookAddress[0]);

			address = self.wrapAddress(address);

			address.internalId = address_book_entry.internalId;
			address.defaultShipping = address_book_entry.defaultShipping;
			address.defaultBilling = address_book_entry.defaultBilling;
			address.isResidential = address_book_entry.isResidential;

			delete address.$;
			delete address.addrText;
			delete address.override;

			address_list.push(address);
		});

		return address_list;
	}	
}

