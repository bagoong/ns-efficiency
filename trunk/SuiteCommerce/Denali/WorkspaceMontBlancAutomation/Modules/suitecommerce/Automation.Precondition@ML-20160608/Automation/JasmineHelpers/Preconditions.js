// MAKE PRECONDITIONS FRAMEWORK AVAILABLE TO ALL TESTS
var args = require('yargs').argv;

global.Preconditions = require('preconditions');
Preconditions = global.Preconditions;

if (args.verbose)
{
	Preconditions.getOrchestrator().on('task_start', function(evt)
	{
		console.log('  Preconditions - Started ' + evt.task);
	});
}

Preconditions.getOrchestrator().on('err', function(eventData)
{
	var e = eventData.err;
	global.triggerAutomationError(e);
});


if (args['mega-cache'] || args['megacache'])
{
	global.Preconditions.start = require('./MegaCache').wrapPreconditionsStart(Preconditions.start);
}


beforeAll(function(done)
{
	Preconditions.Configuration.loadConfiguration(done);
});