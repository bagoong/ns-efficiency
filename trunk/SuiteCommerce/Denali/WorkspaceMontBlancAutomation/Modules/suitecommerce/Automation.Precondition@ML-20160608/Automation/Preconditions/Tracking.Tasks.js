var Preconditions = require('preconditions')
,	_ = require("underscore")
;


var trackingTemplate = {
	googleTagManager: {
		id: ''
	,	dataLayerName: 'dataLayer'
	}
,	googleUniversalAnalytics: {
		id: '' 
	}
,	googleAdWordsConversion: {
		id: ''
	,	label: ''
	,	value: null
	}
}


Preconditions.add('tracking_configuration', ['configuration'], function(configuration, cb)
{
	if (!Preconditions.Configuration.tracking)
	{
		Preconditions.Configuration.addSection(
			'tracking'
		,	trackingTemplate
		);

		return pending(
			'tracking configuration section is empty in ' + Preconditions.Configuration.configurationFilePath
			+ ' please define it and try again' 
		);
	}

	cb(null, Preconditions.Configuration.tracking);
});



Preconditions.add('google_tag_manager_account', ['tracking_configuration'], function(trackingConf, cb)
{
	if (_.isEmpty(trackingConf.googleTagManager && trackingConf.googleTagManager.id))
	{
		return pending(
			'tracking.googleTagManager.id is empty in ' + Preconditions.Configuration.configurationFilePath +
			' please define it and try again' 
		);
	}

	cb(null, trackingConf.googleTagManager.id);
});


Preconditions.add('google_analytics_account', ['tracking_configuration'], function(trackingConf, cb)
{
	if (_.isEmpty(trackingConf.googleUniversalAnalytics && trackingConf.googleUniversalAnalytics.id))
	{
		return pending(
			'tracking.googleUniversalAnalytics.id is empty in ' + Preconditions.Configuration.configurationFilePath +
			' please define it and try again' 
		);
	}

	cb(null, trackingConf.googleUniversalAnalytics.id);
});