var _ = require("underscore")

Preconditions.add(
		'create_one_sales_order_pending_approval'
	,	['configuration', 'create_one_full_customer', 'get_one_service_item']
	,	function (configuration, customer, serviceItem, cb)
	{

	var subsidiary = configuration.website.subsidiary || null;

	Preconditions.Transaction.createTransaction
	(
		{ 
			customer: customer
		,	recordType: 'salesOrder'
		,	shipMethod: null
		,	items: [ serviceItem ]
		,	orderStatus: '_pendingApproval' 
		,	location: subsidiary 
		}
		,	cb
	);
});


Preconditions.add(
		'create_one_sales_order_fulfilled'
	,	['configuration', 'create_one_full_customer', 'get_one_non_inventory_item']
	,	function (configuration, customer, nonInventoryItem, cb)
	{

	Preconditions.Transaction.createTransaction
	(
		{ 
			customer: customer
		,	recordType: 'salesOrder'
		,	shipMethod: null
		,	items: [ nonInventoryItem ]
		,	orderStatus: '_pendingFulfillment' 
		}

	,	function(err, salesOrder)
		{
			if (err)
			{
				return pending("Unable to create sales order.", cb);
			}

			Preconditions.Transaction.fulfillTransaction(
				{
					record: salesOrder
				,	type: "salesOrder" 
				}
			,	function(err, res)
				{
					if (err)
					{
						return pending("Unable to fulfill sales order.", cb);
					}

					SuiteTalk.SalesOrder.getSalesOrder(salesOrder.internalId, cb);
				}
			);
		}

	);
});


Preconditions.add(
		'create_one_sales_order_fulfilled_one_item_non_inventory_non_matrix_quantity_pricing'
	,	['configuration', 'create_one_full_customer', 'get_one_non_inventory_non_matrix_item_quantity_pricing']
	,	function (configuration, customer, nonInventoryItem, cb)
	{

	var quantity = 1111;
	
	Object.keys(nonInventoryItem.pricingMatrix[0].priceList[0].price).forEach(function(key)
	{
		quantity = nonInventoryItem.pricingMatrix[0].priceList[0].price[key].quantity;
	})	

	nonInventoryItem.quantity = quantity;

	Preconditions.Transaction.createTransaction
	(
		{ 
			customer: customer
		,	recordType: 'salesOrder'
		,	shipMethod: null
		,	items: [ nonInventoryItem ]
		,	orderStatus: '_pendingFulfillment' 
		}

	,	function(err, salesOrder)
		{
			if (err)
			{
				return pending("Unable to create sales order.", cb);
			}

			Preconditions.Transaction.fulfillTransaction(
				{
					record: salesOrder
				,	type: "salesOrder" 
				}
			,	function(err, res)
				{
					if (err)
					{
						return pending("Unable to fulfill sales order.", cb);
					}

					SuiteTalk.SalesOrder.getSalesOrder(salesOrder.internalId, cb);
				}
			);
		}

	);
});

Preconditions.add(
		'create_one_credit_memo'
	,	['configuration', 'scis_create_one_full_customer', 'get_one_inventory_non_matrix_item', 'get_location_id']
	,	function (configuration, customer, inventoryItem, locationId, cb)
	{

	customer.type = "customer";

	SuiteTalk
		.initialize({
			type: 'creditMemo'
		,	reference: customer
		})
		.then(function(response)
		{
			var record = response.initializeResponse[0].readResponse[0].record[0];
			record = Preconditions.Helpers.formatResponseRecord(record);
			
			var subsidiary = configuration.website.subsidiary || null;

			Preconditions.Transaction.createTransaction
			(
				{ 
					customer: customer
				,	recordType: 'creditMemo'
				,	items: [ inventoryItem ]
				,	location: locationId
				,	postingPeriod: record.postingPeriod.internalId
				}
				,	function(err, res)
					{
						cb(err, res);
					}
			);
		})
		.fail(function(err)
		{
			console.log(JSON.stringify(err, null, 4));
			cb(err);
		})
	;

});

Preconditions.add(
		'get_credit_memo_info'
	,	['create_one_credit_memo']
	,	function (creditMemo, cb)
	{
		Preconditions.Transaction.searchTransactions
		(
			creditMemo
		,	function(err, res)
			{
				if (err)
				{
					return cb(err);
				}
				cb(err, res);
			}
		);

});