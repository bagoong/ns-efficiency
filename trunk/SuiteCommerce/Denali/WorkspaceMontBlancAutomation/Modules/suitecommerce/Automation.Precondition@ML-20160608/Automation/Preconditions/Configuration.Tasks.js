
Preconditions.add('configuration', function(cb)
{
	if (Preconditions.Configuration.configurationComplete)
	{
		return cb(null, Preconditions.Configuration.getConfiguration());
	}

	Preconditions.Configuration.loadConfiguration(cb);
});


// SUITECOMMERCE ADVANCED SITE

Preconditions.add('sca_configuration', ['configuration'], function(configuration, cb)
{
	var siteType = configuration.website.siteType;

	if (siteType === 'STANDARD')
	{
		pending('This test runs only on SuiteCommerce Advanced but a SiteBuilder website was found.')
	}
	else if (siteType !== 'ADVANCED')
	{
		pending('This test runs only on SuiteCommerce Advanced but no siteType was found.');
	}

	cb(null, configuration);
});


// SITEBUILDER SITE

Preconditions.add('sb_configuration', ['configuration'], function(configuration, cb)
{
	var siteType = configuration.website.siteType;

	if (siteType === 'ADVANCED')
	{
		pending('This test runs only on Site Builder but a SuiteCommerce Advanced website was found.')
	}
	else if (siteType !== 'STANDARD')
	{
		pending('This test runs only on Site Builder but no siteType was found.');
	}

	cb(null, configuration);
});



// SUITECOMMERCE CHECKOUT CONFIGURATIONS


// SUITECOMMERCE SKIP LOGIN

Preconditions.add('check_skip_login_enabled', ['configuration'], function(configuration, cb)
{
	if (configuration.website.checkoutSkipLogin)
	{
		cb(null, true);
	}
	else if (configuration.website.checkoutSkipLogin === false)
	{
		pending('This test requires Skip Login Checkout to be enabled.');
	}
	else
	{
		pending('This test requires Skip Login Checkout to be enabled (Not found in checkout.js).');
	}
});


Preconditions.add('check_skip_login_disabled', ['configuration'], function(configuration, cb)
{
	if (configuration.website.checkoutSkipLogin)
	{
		pending('This test requires Skip Login Checkout to be disabled.');
	}
	else
	{
		cb(null, true);
	}
});


var checkCheckoutSteps = function(params, cb)
{
	var stepsName = params.stepsName;
	var stepsRegexp = new RegExp(params.stepsMatch, 'i')

	Preconditions.Website.getCheckoutSteps(function(err, stepsFound)
	{
		if (stepsFound && !stepsRegexp.test(stepsFound))
		{
			pending('This test requires ' + stepsName + ' configuration. ' + stepsFound + ' Checkout detected.');
		}

		cb(err, stepsFound);
	});
}

// SUITECOMMERCE CHECKOUT STEPS

Preconditions.add('check_standard_checkout_steps', ['configuration'], function(configuration, cb)
{
	checkCheckoutSteps({
		stepsName : 'Standard Checkout'
	,	stepsMatch: 'standard'
	}, cb);
});

Preconditions.add('check_one_page_checkout_steps', ['configuration'], function(configuration, cb)
{
	checkCheckoutSteps({
		stepsName : 'One Page Checkout'
	,	stepsMatch: 'opc'
	}, cb);
});

Preconditions.add('check_billing_first_checkout_steps', ['configuration'], function(configuration, cb)
{
	checkCheckoutSteps({
		stepsName : 'Billing First Checkout'
	,	stepsMatch: 'billing'
	}, cb);
});


// SUITECOMMERCE STANDARD CHECKOUT

Preconditions.add(
	'sb_configuration_with_standard_checkout'
,	['sb_configuration', 'check_skip_login_disabled', 'check_standard_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sb_configuration_with_standard_checkout_and_skip_login'
,	['sb_configuration', 'check_skip_login_enabled', 'check_standard_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_standard_checkout'
,	['sca_configuration', 'check_skip_login_disabled', 'check_standard_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_standard_checkout_and_skip_login'
,	['sca_configuration', 'check_skip_login_enabled', 'check_standard_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


// SUITECOMMERCE ONE PAGE CHECKOUT


Preconditions.add(
	'sb_configuration_with_one_page_checkout'
,	['sb_configuration', 'check_skip_login_disabled', 'check_one_page_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sb_configuration_with_one_page_checkout_and_skip_login'
,	['sb_configuration', 'check_skip_login_enabled', 'check_one_page_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_one_page_checkout'
,	['sca_configuration', 'check_skip_login_disabled', 'check_one_page_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_one_page_checkout_and_skip_login'
,	['sca_configuration', 'check_skip_login_enabled', 'check_one_page_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


// SUITECOMMERCE BILLING FIRST CHECKOUT


Preconditions.add(
	'sb_configuration_with_billing_first_checkout'
,	['sb_configuration', 'check_skip_login_disabled', 'check_billing_first_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sb_configuration_with_billing_first_checkout_and_skip_login'
,	['sb_configuration', 'check_skip_login_enabled', 'check_billing_first_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_billing_first_checkout'
,	['sca_configuration', 'check_skip_login_disabled', 'check_billing_first_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);


Preconditions.add(
	'sca_configuration_with_billing_first_checkout_and_skip_login'
,	['sca_configuration', 'check_skip_login_enabled', 'check_billing_first_checkout_steps']
,	function(configuration, sl, steps, cb) { cb(null, configuration);}
);

// @automationTask locator_configuration verify the AUTOMATIC LOCATION ASSIGNMENT flag in SetUp->Company->Enable Features
// @return {configuration}
Preconditions.add('locator_configuration', ['configuration'], function(configuration, cb)
{
	var autoLocationAssignament = configuration.website.siteSettings.autolocationassignment
	,	applicationId = configuration.credentials.applicationId;

	if (!autoLocationAssignament)
	{
		pending('This test runs only with autolocationassignment flag enabled.')
	}
	else if (!applicationId)
	{
		pending('This test runs only with applicationId configured.')
	}

	cb(null, configuration);
});

// @automationTask populate_configuration verify if pre-populate forms flag is enabled.
// @return {configuration}
Preconditions.add('populate_configuration', ['configuration'], function(configuration, cb)
{
	//SET THIS AFTER CONFIGURATION SSP IS CREATED
	var enablePrepopulatedForms = null;

	if (!enablePrepopulatedForms)
	{
		pending('This test runs only with pre-populate forms flag enabled.')
	}

	cb(null, configuration);
});
