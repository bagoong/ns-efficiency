
var	suitetalk = require('suitetalk')
,	_ = require('underscore');


Preconditions.PaymentMethod = {
	

	getAvailableCreditCards: function (credentials, cb)
	{
		this.searchPaymentMethods(
			{
				'isInactive': false
			,	'isOnline': true
			,	'creditCard': true
			}
		,	credentials
		,	function(err, records)
			{
				if (err) return cb(err);
				cb(null, records);
			}
		);
	}


,	searchPaymentMethods: function (params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		var filters = Preconditions.Helpers.generateSearchFilters(
			{
				'name'       : 'string'
			,	'isInactive' : 'boolean'
			,	'creditCard' : 'boolean'
			,	'isDebitCard': 'boolean'
			,	'internalId' : 'recordRef'
			}
		,	params
		);

		//console.log(filters);

		suitetalk.searchBasic({
			recordType: 'paymentMethod'
		,	filters: filters
		})
		.then(function(response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			var records = response.records;
			
			cb(null, records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}	


,	getAvailableInvoiceTerms: function (credentials, cb)
	{
		this.searchInvoiceTerms(
			{
				'isInactive': false
			,	'preferred' : true
			}
		,	credentials
		,	function(err, records)
			{
				if (err) return cb(err);
				cb(null, records);
			}
		);
	} 


,	searchInvoiceTerms: function (params, credentials, cb)
	{
		var self = this;

		suitetalk.setCredentials(credentials);

		var filters = Preconditions.Helpers.generateSearchFilters(
			{
				'internalId' : 'recordRef'
			,	'isInactive' : 'boolean'
			,	'preferred'  : 'boolean'
			}
		,	params
		);

		suitetalk.searchBasic({
			recordType: 'term'
		,	filters: filters
		})
		.then(function(response)
		{
			response = Preconditions.Helpers.formatSearchResponse(response);
			cb(null, response.records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}

}
