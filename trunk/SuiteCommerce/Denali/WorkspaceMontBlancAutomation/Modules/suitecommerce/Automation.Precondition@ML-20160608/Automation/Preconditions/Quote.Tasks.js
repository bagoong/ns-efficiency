var _ = require('underscore')
,	moment = require('moment');

Preconditions.add(
		'create_buyable_quote_for_new_customer'
	,	['configuration'
		,	'get_one_inventory_item'
		,	'create_one_full_customer'
		,	'get_one_sales_rep'
		]
	,	function (configuration, item, customer, salesRep, cb)
		{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '12'
		,	salesRep: salesRep
		,	shipMethod: configuration.website.siteSettings.defaultshippingmethod
		}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_closed_lost_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '14'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_qualified_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '7'
	,	salesRep: salesRep
	,	shipMethod: configuration.website.siteSettings.defaultshippingmethod
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_in_discussion_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '8'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_identified_decision_makers_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '9'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_proposal_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '10'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_in_negotiation_quote_for_new_customer'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '11'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_one_customer_with_two_quotes'
	,	['configuration'
	,	'create_one_full_customer'
	,	'get_one_inventory_item'
	,	'get_one_sales_rep']
	,	function (configuration, customer, item, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuotes({
		customer: customer
	,	quotes: [
			{
				entityStatus: '12'
			,	items: item
			,	salesRep: salesRep
			}
		,	{
				entityStatus: '11'
			,	items: item
			,	salesRep: salesRep
			}
		]
	}
	,	credentials
	,	cb
	);
});

Preconditions.add(
		'create_one_customer_with_fifty_quotes'
	,	['configuration'
	,	'create_one_full_customer'
	,	'get_one_inventory_item'
	,	'get_one_sales_rep']
	,	function (configuration, customer, item, salesRep, cb)
	{
	var credentials = configuration.credentials;
	var allQuotes = [];

	for (i = 0; i <= 50; i++)
	{
		allQuotes.push({entityStatus:'12', items: item,	salesRep: salesRep});
	}

	Preconditions.Quote.createQuotes({
		customer: customer
	,	quotes: allQuotes
	}
	,	credentials
	,	cb
	);
});

// @automationTask create_fifty_quotes_return_first Allow the creation of fifty quotes and returns the first one
// @return {Automation.Quote.Model}
Preconditions.add(
	'create_fifty_quotes_return_first'
,	[
		'configuration'
	,	'create_one_customer_with_fifty_quotes'
	]
,	function (configuration, quotes, cb)
	{
		var credentials = configuration.credentials
		,	firstQuote = _.first(quotes);

		Preconditions.Quote.searchQuotes(
			firstQuote
		,	credentials
		,	function (err, quotes)
			{
				if (err)
				{
					return cb(err);
				}

				cb(null, _.first(quotes));
			}
		);
	}
);

// @automationTask create_set_random_quotes Allow the creation of 5 to 15 quotes for a customer with a casual status, item, tranDate and dueDate
// @return {Array<{internalId:Number}>}
Preconditions.add(
	'create_set_random_quotes'
,	[
		'configuration'
	,	'create_one_full_customer'
	,	'get_inventory_items'
	,	'get_one_sales_rep'
	]
,	function (configuration, customer, items, salesRep, cb)
	{
		var credentials = configuration.credentials
		,	possibleStatuses = [7, 8, 9, 10, 11, 12, 14]
		,	dates = [-100, -75, -50, -25, -10, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 10, 25, 50, 75, 100]
		,	quotesAmount = Math.random() * (15 - 5) + 5
		,	allQuotes = [];

		for (i = 0; i <= quotesAmount; i++)
		{
			allQuotes.push(
				{
					entityStatus: _.sample(possibleStatuses)
				,	items: _.sample(items)
				,	tranDate: moment().add(_.sample(dates), 'days').format()
				,	dueDate: moment().add(_.sample(dates), 'days').format()
				,	salesRep: salesRep
				,	message: 'Testing message'
				,	memo: 'Testing memo'
				}
			);
		}

		Preconditions.Quote.createQuotes(
			{
				customer: customer
			,	quotes: allQuotes
			}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_qualified_quote_for_new_customer_with_gift_certificate Allow the creation of a qualified quote for a customer without billing information and a gift certificate as the item
// @return {internalId:Number}
Preconditions.add(
	'create_qualified_quote_for_new_customer_with_gift_certificate'
,	[
		'configuration'
	,	'get_one_gift_certificate_item'
	,	'create_one_customer'
	,	'get_one_sales_rep'
	]
,	function (configuration, item, customer, salesRep, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '7'
		,	salesRep: salesRep
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_qualified_quote_for_new_customer_with_gift_certificate_and_no_sales_rep Allow the creation of a qualified quote for a customer without billing information, no sales rep and a gift certificate as the item
// @return {internalId:Number}
Preconditions.add(
	'create_qualified_quote_for_new_customer_with_gift_certificate_and_no_sales_rep'
,	[
		'configuration'
	,	'get_one_gift_certificate_item'
	,	'create_one_customer'
	]
,	function (configuration, item, customer, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '7'
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_purchasing_quote_for_new_customer_with_gift_certificate Allow the creation of a purchasing quote for a customer with a gift certificate as the item
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_for_new_customer_with_gift_certificate'
,	[
		'configuration'
	,	'get_one_gift_certificate_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep'
	]
,	function (configuration, item, customer, salesRep, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '12'
		,	salesRep: salesRep
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_purchasing_quote_for_new_customer_without_sales_rep Allow the creation of a purchasing quote for a customer without sales rep
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_for_new_customer_without_sales_rep'
,	[
		'configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	]
,	function (configuration, item, customer, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '12'
		,	shipMethod: configuration.website.siteSettings.defaultshippingmethod
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_qualified_quote_for_new_customer_with_no_addresses (Quote no sales approval, no shipping method, no shipping address, no payment information)
// @return {internalId:Number}
Preconditions.add(
	'create_qualified_quote_for_new_customer_with_no_addresses'
,	[
		'configuration'
	,	'get_one_inventory_item'
	,	'create_one_customer'
	,	'get_one_sales_rep'
	]
,	function (configuration, item, customer, salesRep, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '7'
		,	shipMethod: null
		,	salesRep: salesRep
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_purchasing_quote_for_new_customer_with_no_addresses_no_shipping_method (Quote no shipping method, no shipping address, no payment information)
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_for_new_customer_with_no_addresses_no_shipping_method'
,	[
		'configuration'
	,	'get_one_inventory_item'
	,	'create_one_customer'
	,	'get_one_sales_rep'
	]
,	function (configuration, item, customer, salesRep, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '12'
		,	shipMethod: null
		,	salesRep: salesRep
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_purchasing_quote_for_new_customer_with_no_addresses (Quote no shipping method, no shipping address, no payment information)
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_for_new_customer_with_no_addresses'
,	[
		'configuration'
	,	'get_one_inventory_item'
	,	'create_one_customer'
	,	'get_one_sales_rep'
	]
,	function (configuration, item, customer, salesRep, cb)
	{
		var credentials = configuration.credentials;

		Preconditions.Quote.createQuote({
			customer: customer
		,	items: item
		,	entityStatus: '12'
		,	shipMethod: configuration.website.siteSettings.defaultshippingmethod
		,	salesRep: salesRep
		}
		,	credentials
		,	cb
		);
	}
);

// @automationTask create_purchasing_quote_for_new_customer_with_billing_address (Quote no shipping address)
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_for_new_customer_with_billing_address'
,	[
		'configuration'
	,	'create_purchasing_quote_for_new_customer_with_no_addresses'
	,	'generate_one_billing_address_data'
	]
,	function (configuration, quote, addr, cb)
	{
		var credentials = configuration.credentials
		,	address = _.clone(addr);

		Preconditions.Quote.updateQuoteAddress(
			{
			// @class Automation.Preconditions.Quote.Address
			// @property {Number} quote
				quote: quote
			// @property {Automation.Preconditions.Address} address
			,	address: address
			// @property {String} addressType Possible values are: billingAddress || shippingAddress
			,	addressType: 'billingAddress'
			}
		,	credentials
		,	function (err, quote)
			{
				if (err)
				{
					return cb(err);
				}
				cb(null, quote);
			}
		);
	}
);

// @automationTask create_purchasing_quote_with_shipping_address (Quote no payment information)
// @return {internalId:Number}
Preconditions.add(
	'create_purchasing_quote_with_shipping_address'
,	[
		'configuration'
	,	'create_purchasing_quote_for_new_customer_with_no_addresses'
	,	'generate_one_shipping_address_data'
	]
,	function (configuration, quote, addr, cb)
	{
		var credentials = configuration.credentials
		,	address = _.clone(addr);

		if (configuration.website.siteSettings.defaultshippingmethod === null) return pending('Site has no default shipping method');

		Preconditions.Quote.updateQuoteAddress(
			{
				quote: quote
			,	address: address
			,	addressType: 'shippingAddress'
			,	shipMethod: configuration.website.siteSettings.defaultshippingmethod
			}
		,	credentials
		,	function (err, quote)
			{
				if (err)
				{
					return cb(err);
				}
				cb(null, quote);
			}
		);
	}
);

// @automationTask create_purchasing_quote_without_shipping_method (Quote no shipping method)
// @return {internalId:Number}
Preconditions.add(
		'create_purchasing_quote_without_shipping_method'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_one_full_customer'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '12'
	,	shipMethod: null
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});

// @automationTask create_quote_for_customer_with_no_permissions
// @return {internalId:Number}
Preconditions.add(
		'create_quote_for_customer_with_no_permissions'
	,	['configuration'
	,	'get_one_inventory_item'
	,	'create_customer_quotes_00'
	,	'get_one_sales_rep']
	,	function (configuration, item, customer, salesRep, cb)
	{
	var credentials = configuration.credentials;

	Preconditions.Quote.createQuote({
		customer: customer
	,	items: item
	,	entityStatus: '12'
	,	salesRep: salesRep
	}
	,	credentials
	,	cb
	);
});