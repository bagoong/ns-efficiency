
var	suitetalk = require('suitetalk')
,	_ = require('underscore');


Preconditions.Promotion = {

	useTypes: [
		'_multipleUses', '_singleUse'
	]

,	applyDiscountTo: [
		'_allSales', '_firstSaleOnly'
	]


,	addPromotionCodeExtraFields: function(record)
	{
		var self = this;

		record.percentageRate = null;
		record.flatRate = null;

		if (typeof record.rate === 'string')
		{
			var percentage_match = record.rate.match(/^([0-9\.]+)\%$/);

			if (percentage_match)
			{
				record.percentageRate = parseFloat(percentage_match[1]);
			}
		}
		else if (record.rate)
		{
			record.flatRate = record.rate;
		}

		record.itemSpecific = false;

		if (Array.isArray(record.implementation))
		{
			record.itemSpecific = _(record.implementation).some(function(subrecord)
			{
				return subrecord.internalId === 'CUSTOMSCRIPT_ADVPROMO_ITEM_SPECIFIC';
			});
		}

		if (Array.isArray(record.itemsList))
		{
			record.itemSpecific = record.itemSpecific || record.itemsList[0].items.length > 0;
		}

		record.allItems = !record.excludeItems && !record.itemSpecific;
		record.isPercentageRate = record.percentageRate && record.percentageRate > 0;
		record.isFlatRate = record.flatRate && record.flatRate > 0;


		record.isInvalid = false;

		var current_date = new Date().toISOString().slice(0, 10);
		var current_date_ts = new Date(current_date).getTime();

		if (record.startDate && /^\d\d\d\d/.test(record.startDate))
		{
			var start_date_ts = new Date(record.startDate).getTime();
			record.isInvalid = record.isInvalid || (start_date_ts > current_date_ts);
			//console.log(start_date_ts);
		};

		if (record.endDate && /^\d\d\d\d/.test(record.endDate))
		{
			var end_date_ts = new Date(record.endDate).getTime();
			record.isInvalid = record.isInvalid || (end_date_ts < current_date_ts);
			//console.log(end_date_ts);
		};


		return record;
	}


,	formatSearchResponse: function(response)
	{
		var self = this;
		var records = response.searchResponse[0].searchResult[0].recordList[0].record;

		if (Array.isArray(records))
		{
			records = Preconditions.Helpers.formatResponseRecordList(records);

			records.forEach(function(record)
			{
				self.addPromotionCodeExtraFields(record);
			});
		}
		else
		{
			records = [];
		}

		return {
			status: 'success'
		,	records: records
		}
	}


,	filterSearchRecords: function(response, params)
	{
		var filteredResponse = response;

		var promocode_filter_fields = [
			'useType', 'applyDiscountTo', 'isPercentageRate', 'isFlatRate', 'allItems', 'isInvalid'
		];

		promocode_filter_fields.forEach(function(field_name)
		{
			if (_.isUndefined(params[field_name]))
			{
				return false;
			}

			filteredResponse = _(filteredResponse).filter(function(promoRecord)
			{
				return promoRecord[field_name] === params[field_name];
			});
		})

		return filteredResponse;
	}

,	searchPromotionCodes: function (params, credentials, cb) {
		var self = this;

		params.isInactive = params.isInactive || false;
		params.isPublic = params.isPublic || true;

		var filters = {};

		var promocode_enum_filters = [
			"applyDiscountTo"
		];

		var promocode_boolean_filters = [
			"isInactive", "isPublic", "displayLineDiscounts"
		];

		promocode_boolean_filters.forEach(function(filter_name)
		{
			if (!_.isUndefined(params[filter_name]))
			{
				filters[filter_name] = {
					searchValue: JSON.stringify(params[filter_name])// true or false string
				}
			}
		});

		var promocode_date_filters = [
			'startDate', 'endDate'
		]

		promocode_date_filters.forEach(function(filter_name)
		{
			if (!_.isUndefined(params[filter_name]))
			{
				filters[filter_name] = {
					type: 'SearchDateField'
				,	operator: params[filter_name].operator //"after", "before", "empty", "notAfter", "notBefore", "notEmpty", "notOn", "notOnOrAfter", "notOnOrBefore", "notWithin", "on", "onOrAfter", "onOrBefore", "within"
				,	searchValue: new Date(params[filter_name].value).toISOString()
				};
			}
		});


		suitetalk.setCredentials(credentials);

		suitetalk.searchBasic({
			recordType: 'promotionCode'
		,	filters: filters
		})
		.then(function(response)
		{
			//console.log(response);
			var clean_response = self.formatSearchResponse(response);

			records = self.filterSearchRecords(clean_response.records, params);

			cb(null, records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}
}


Preconditions.add(
	'get_valid_promocodes_with_percentage_rate'
,	function(cb)
	{
		Preconditions.Promotion.searchPromotionCodes(
		{
			'useType': '_multipleUses'
		,	'applyDiscountTo': '_allSales'
		,	'allItems': true
		,	'isPercentageRate': true
		,	'isInvalid': false
		}

	,	Preconditions.Configuration.credentials

	,	function(err, res)
		{
			cb(err, res);
		}
	);
});


Preconditions.add(
	'get_one_valid_promocode_with_percentage_rate'
,	['get_valid_promocodes_with_percentage_rate']
,	function(promoCodes, cb)
	{
		//console.log(promoCodes[0].code);
		//console.log(promoCodes[0].isInvalid);
		//console.log(promoCodes[0].startDate);
		//console.log(promoCodes[0].endDate);
		cb(null, _.sample(promoCodes));
	}
);


Preconditions.add(
	'get_expired_promocodes_with_percentage_rate'
,	function(cb)
	{
		Preconditions.Promotion.searchPromotionCodes(
		{
			'useType': '_multipleUses'
		,	'applyDiscountTo': '_allSales'
		,	'allItems': true
		,	'endDate' : { operator: "before", value: new Date() }
		}

	,	Preconditions.Configuration.credentials

	,	function(err, res)
		{
			cb(err, res);
		}
	);
});

Preconditions.add(
	'get_one_expired_promocode_with_percentage_rate'
,	['get_expired_promocodes_with_percentage_rate']
,	function(promoCodes, cb)
	{
		//console.log(promoCodes[0].code);
		//console.log(promoCodes[0].isInvalid);
		//console.log(promoCodes[0].startDate);
		//console.log(promoCodes[0].endDate);
		cb(null, _.sample(promoCodes));
	}
);


