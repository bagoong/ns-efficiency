var _ = require('underscore')
,	crypto = require('crypto')
,	fs = require('fs')
,	path = require('path')
;

var wrapPreconditionsStart = function(originalPreconditionsStartFn)
{
	return function()
	{
		var ags = _.toArray(arguments)
		var tasks = ags.slice(0, ags.length - 1);

		var startCb = ags[ags.length - 1];

		var cacheKey = JSON.stringify(tasks);

		shasum = crypto.createHash('sha1');

		shasum.update(cacheKey);
		cacheKey = shasum.digest('hex');
		
		var cacheDir = path.join(process.cwd(), 'cache');
		var cacheFile = path.join(cacheDir, cacheKey);

		var hasCacheFile = false;

		try {
			fs.accessSync(cacheFile);
			hasCacheFile = true;
		} catch (e) {}

		if (hasCacheFile)
		{
			startCb.apply(Preconditions, _.toArray(JSON.parse(
				fs.readFileSync(cacheFile).toString()
			)));			
		}
		else
		{
			ags[ags.length - 1] = function()
			{
				fs.writeFileSync(cacheFile, JSON.stringify(arguments));
				startCb.apply(Preconditions, _.toArray(arguments));
			}

			originalPreconditionsStartFn.apply(Preconditions, ags);
		}
	}
}

module.exports = {
	wrapPreconditionsStart: wrapPreconditionsStart
}