var	suitetalk = require('suitetalk')
,	_ = require('underscore');

// NS ESTIMATE DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/schema/record/salestaxitem.html
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/salestaxitem.html


Preconditions.Tax = {


	getNullTax: function() {
		return {
			'itemId': '-Not Taxable-'
		,	'rate': 0
		,	'internalId': -7
		}
	}


,	addTaxExtraFields: function(record)
	{
		var self = this;

		if (typeof record.zip === 'string')
		{
			var sep_regexp = /\s*(<br[^>]*>|,)\s*/g;
			record.zip = record.zip.split(sep_regexp);
			record.zip = _(record.zip).filter(function (zip_part)
			{
				return !sep_regexp.test(zip_part);
			});
		}

		record.zip = record.zip || null;

		return record;
	}


,	filterSearchRecords: function(records, params)
	{
		var filtered_records = records;

		var tax_record_fields = [
			'state', 'internalId', 'tax', 'itemId', 'displayName', 'isInactive'
		];

		tax_record_fields.forEach(function(field_name)
		{
			if (_.isUndefined(params[field_name]))
			{
				return false;
			}

			filtered_records  = _(filtered_records).filter(function(tax_record)
			{
				var field_value = tax_record[field_name];
				var param_value = params[field_name];

				//console.log(field_value, param_value);

				return (field_value + '') === (params[field_name] + '');
			});
		});

		if (params.zip)
		{
			filtered_records = _(filtered_records).filter(function(record)
			{
				if (Array.isArray(record.zip) && record.zip.length)
				{
					return _.map(record.zip, String).indexOf(params.zip + '') !== -1;
				}

				return true;
			});
		}

		return filtered_records;
	}


,	getTaxForAddress: function (address, credentials, cb)
	{
		var self = this;

		var params = {
			'zip': address.zip
		,	'state': address.state
		}

		var country_code = Preconditions.Address.getCountryCode(address.country);

		if (country_code === "US")
		{
			var state = Preconditions.Address.getUsStateCode(params.state);

			if (state)
			{
				params.state = state;
			}
		}


		this.searchTaxes(params, credentials, function(err, res)
		{
			if (res.length)
			{
				res = res[0];
			}
			else
			{
				res = self.getNullTax();
			}

			cb(err, res);
		});
	}


,	searchTaxes: function(params, credentials, cb)
	{
		var self = this;
		suitetalk.setCredentials(credentials);


		return suitetalk.getAll({
			recordType: 'salesTaxItem'
		})
		.then(function(response)
		{
			var records = response.getAllResponse[0].getAllResult[0].recordList[0].record;
			records = Preconditions.Helpers.formatResponseRecordList(records);

			records.forEach(function(record)
			{
				self.addTaxExtraFields(record);
			});

			records = self.filterSearchRecords(records, params);

			cb(null, records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}

}
