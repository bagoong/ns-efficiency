var Preconditions = require('preconditions')
,	suitetalk = require('suitetalk')
,	path = require('path')
,	_ = require('underscore')
,	async = require('async')
,	request = require('request')
;

/*
	SuiteTalk Schema Browser docs for Item:
	https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/item.html

	Valid item types are located in RecordType section from core XSD:
	https://webservices.na1.netsuite.com/xsd/platform/v2015_1_0/coreTypes.xsd
*/

Preconditions.Item = {

	itemTypesForSearchEnum: [
			'assembly'               // 'Assembly'    // Includes: Simple, LotNumbered, Serialized
		,	'inventoryItem'          // 'InvtPart'    // Includes: Simple, LotNumbered, Serialized
		,	'nonInventoryItem'       // 'NonInvtPart' // Includes: For Sale, For Resale
		,	'giftCertificateItem'    // 'GiftCert'
		,	'downloadItem'           // 'DwnLdItem'
		,	'discountItem'           // 'Discount'    // Not supported (?)
		,	'kit'                    // 'Kit'
		,	'service'                // 'Service'     // Incorrectly documented as serviceForSaleItem
	]


,	outOfStockBehaviorEnum: [
		'_default'
	,	'_allowBackOrdersButDisplayOutOfStockMessage'
	,	'_allowBackOrdersWithNoOutOfStockMessage'
	,	'_disallowBackOrdersButDisplayOutOfStockMessage'
	,	'_removeItemWhenOutOfStock'
	]


,	getItemMatrixAttributes: function(itemRecord)
	{
		var result = {};

		if (itemRecord.isMatrix && Array.isArray(itemRecord.customFieldList))
		{
			result.matrixDimension = 0;
			result.matrixFields = [];

			var customFields = itemRecord.customFieldList[0].customField;

			customFields.forEach(function(customField)
			{
				if (customField.$['xsi:type'] === 'platformCore:MultiSelectCustomFieldRef')
				{
					result.matrixDimension++;
					result.matrixFields.push(customField);
				}
			});
		}

		return result;
	}


,	getItemMatrixSelectedOptions: function(itemRecord)
	{
		var result = {};

		if (itemRecord.isMatrixChild)
		{
			result.matrixSelectedOptions = [];

			var rawMatrixOptionList = itemRecord.matrixOptionList[0].matrixOption;

			rawMatrixOptionList.forEach(function(rawOption)
			{
				result.matrixSelectedOptions.push({
					'optionScriptId': rawOption.$.scriptId
				,	'label' : ''
				,	'valueInternalId': rawOption.value[0].internalId
				,	'value': rawOption.value[0].name
				});
			});
		}

		return result;
	}

,	getItemPublicationStatus: function(itemRecord)
	{
		var config = Preconditions.Configuration;

		var result = {
			isPublished: true
		,	isOutOfStock: false || itemRecord.stockOnlineTotal === 0
		,	displayOutOfStockMessage: config.website.displayOutOfStockMessage || true
		,	allowOutOfStockBackOrder: config.website.allowOutOfStockBackOrder || false
		};

		switch(itemRecord.outOfStockBehavior)
		{
			case '_default':
				break;

			case '_allowBackOrdersButDisplayOutOfStockMessage':
				result.displayOutOfStockMessage = true;
				result.allowOutOfStockBackOrder = true;
				break;

			case '_allowBackOrdersWithNoOutOfStockMessage':
				result.displayOutOfStockMessage = false;
				result.allowOutOfStockBackOrder = true;
				break;

			case '_disallowBackOrdersButDisplayOutOfStockMessage':
				result.displayOutOfStockMessage = true;
				result.allowOutOfStockBackOrder = false;
				break;
		}

		if (/inventory/i.test(itemRecord.type) && result.isOutOfStock && !config.website.displayOutOfStockItems)
		{
			result.isPublished = false;
		}

		//console.log(config.website);
		//console.log(result);

		return result;
	}


,	getItemPricing: function(itemRecord)
	{
		var config = Preconditions.Configuration;
		var priceLevel = parseInt(config.website.priceLevel) || null;

		var result = {
			priceOnline: null
		,	priceOnlineDiscount: 0
		};

		if (Array.isArray(itemRecord.pricingMatrix))
		{
			result.pricingMatrix = itemRecord.pricingMatrix[0].pricing;

			result.pricingMatrix.forEach(function(pricingEntry)
			{
				//console.log(pricingEntry);
				var isGiftCertificate = itemRecord.type === 'GiftCertificateItem';

				if (isGiftCertificate && !result.priceOnline)
				{
					result.priceOnline = pricingEntry.priceList[0].price[0].value;
				}
				else if (!_(pricingEntry.priceLevel).isEmpty())
				{
					var pricingEntryPriceLevel = pricingEntry.priceLevel[0].internalId;
					var isBasePriceLevel = pricingEntryPriceLevel === 1;
					var isWebsitePriceLevel = pricingEntryPriceLevel === priceLevel;

					if (isBasePriceLevel || isWebsitePriceLevel)
					{
						result.priceOnlineLevel = pricingEntry.priceLevel[0].internalId;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}

				result.priceOnline = pricingEntry.priceList[0].price[0].value;
				result.priceOnlineByQuantity = pricingEntry.priceList[0].price;

				if (pricingEntry.discount)
				{
					result.priceOnlineDiscount = pricingEntry.discount;
				}
			});
		}

		return result;
	}

,	formatItemSearchRecord: function(itemRecord)
	{
		var self = this;

		itemRecord.type = itemRecord.$['xsi:type'].replace(/^.*?:/, '');

		itemRecord.isPublished = null;
		itemRecord.isMatrix = (itemRecord.matrixType === '_parent') || false;
		itemRecord.isMatrixChild = (itemRecord.matrixType === '_child') || false;

		var stockOnlineTotal = 0;

		if (Array.isArray(itemRecord.locationsList))
		{
			itemRecord.stockOnlineTotal = 0;

			if (!_.isEmpty(itemRecord.locationsList))
			{
				itemRecord.locationsList = itemRecord.locationsList[0].locations;

				itemRecord.locationsList.forEach(function(locationEntry)
				{
					if (locationEntry.quantityAvailable)
					{
						itemRecord.stockOnlineTotal += locationEntry.quantityAvailable;
					}

					if (!_.isEmpty(locationEntry.locationId) && locationEntry.locationId[0].internalId)
					{
						locationEntry.internalId = locationEntry.locationId[0].internalId;
						locationEntry.locationId = locationEntry.internalId;
					}

				});
			}
		}

		_(itemRecord).extend(
			self.getItemPricing(itemRecord)
		,	self.getItemMatrixAttributes(itemRecord)
		,	self.getItemMatrixSelectedOptions(itemRecord)
		,	self.getItemPublicationStatus(itemRecord)
		);

		return itemRecord;
	}


,	filterSearchResponse: function(response, params)
	{
		var self = this;

		var statusDetail = response.status;
		var records = response.recordList;

		var filteredRecords = [];

		//if (statusDetail)
		if (false)
		{
			statusDetail = Preconditions.Helpers.formatResponseRecord(statusDetail[0]);
			console.log('\n' + statusDetail.code + '\n' + statusDetail.message + '\n');
			return filteredRecords;
		}

		records.forEach(function(record)
		{
			record = self.formatItemSearchRecord(record);

			// FILTER BAD CONFIGURED MATRIX ITEMS
			var matrixWithoutCustomFields = record.isMatrix && !record.customFieldList;
			var matrixWithZeroDim = record.isMatrix && record.matrixDimension === 0;

			if (matrixWithoutCustomFields || matrixWithZeroDim)
			{
				return false;
			}
			else if (record.type && /InventoryItem/i.test(record.type) && !record.locationsList)
			{
				return false;
			}

			if (params.matrixDimension && record.isMatrix && (params.matrixDimension !== record.matrixDimension))
			{
				return false;
			}

			filteredRecords.push(record);
		});

		return filteredRecords;
	}


,	_searchItems: function(params, cb)
	{
		var self = this;

		if (params.type !== 'discount')
		{
			params.webSite = params.webSite || params.website || Preconditions.Configuration.website.siteId;

			if (_.isUndefined(params.webSite))
			{
				throw new Error('siteId should be defined for item search.');
			}

			params.isOnline = params.isOnline || true;
		}

		params.matrixChild = params.matrixChild || false;
		params.isInactive = params.isInactive || false;

		if (Preconditions.Configuration.website.subsidiary)
		{
			params.subsidiary = Preconditions.Configuration.website.subsidiary;
		}

		return SuiteTalk.Item.searchItems(params, function(error, response)
		{
			if (error) { return cb(error); }

			var filteredRecords = self.filterSearchResponse(response, params);
			cb(null, filteredRecords);
		});
	}


,	_getFromSearchAPI: function(params, cb)
	{
		var baseUrl = 'https://system.netsuite.com/api/items';

		var molecule = Preconditions.Configuration.credentials.molecule;

		if (molecule)
		{
			baseUrl = 'https://system.' + molecule + '.netsuite.com/api/items';
		}

		var urlParams = [
			'c=' + Preconditions.Configuration.credentials.account
		,	'n=' + Preconditions.Configuration.website.siteId
		,	'fieldset=' + (params.fieldset || 'search')
		];

		if (!_.isEmpty(params.id))
		{
			urlParams.push('id=' + params.id);
		}

		var requestUrl = baseUrl + '?' + urlParams.join('&');

		request.get(
			requestUrl
		,	function(err, request, body)
			{
				if (err || !body)
				{
					return cb(err, null);
				}

				cb(null, JSON.parse(body));
			}
		);
	}


,	_getItemExtraAttributesFromSearchAPI: function(items, cb)
	{
		var self = this;
		var attributesByItemId = {};

		var params = {
			id: items.map(function(item) {
				return item.internalId;
			}).join(',')
		};

		self._getFromSearchAPI(params, function(err, data)
		{
			if (err || !(data && data.items))
			{
				return cb(err, {});
			}

			data.items.forEach(function(itemEntry)
			{
				var attrs = {};
				var itemOptions = [];
				var matrixOptions = [];

				if (!_.isEmpty(itemEntry.itemoptions_detail) && !_.isEmpty(itemEntry.itemoptions_detail.fields))
				{
					itemOptions = [];
					matrixOptions = [];

					itemEntry.itemoptions_detail.fields.map(function(rawOptionsEntry)
					{
						if (!rawOptionsEntry.values)
						{
							return false;
						}

						var optionEntryValues = [];

						rawOptionsEntry.values.forEach(function(optionEntryRawVal)
						{
							if (!optionEntryRawVal.internalid)
							{
								return false;
							}

							optionEntryValues.push({
								name: optionEntryRawVal.label
							,	internalId: optionEntryRawVal.internalid
							});
						});

						var optionsEntry = {
							internalId: rawOptionsEntry.internalid
						,	scriptId: rawOptionsEntry.sourcefrom
						,	label: rawOptionsEntry.label
						,	values: optionEntryValues
						,	isMandatory: rawOptionsEntry.ismandatory
						};

						if (rawOptionsEntry.ismatrixdimension)
						{
							matrixOptions.push(optionsEntry);
						}
						else
						{
							itemOptions.push(optionsEntry);
						}
					});
				}

				if (!_.isEmpty(matrixOptions))
				{
					attrs.matrixOptions = matrixOptions;
				}

				if (!_.isEmpty(itemOptions))
				{
					attrs.itemOptions = itemOptions;
				}

				attributesByItemId[itemEntry.internalid] = attrs;
			});

			cb(null, attributesByItemId);
		});
	}


,	searchItems: function(params, cb)
	{
		var self = this;

		// adds automatic jasmine's test pending() if result is empty
		if (params.pendingOnEmptyResult)
		{
			cb = self.emptyResultHandler(params.pendingOnEmptyResult, cb);
		}

		async.waterfall([
			function(cb)
			{
				self._searchItems(params, function(err, items)
				{
					var resultHasMatrixItems = _.some(_.pluck(items, 'isMatrix'));

					if (!err && resultHasMatrixItems)
					{
						params.items = items;

						self.appendMatrixChildsToSearchResult(params, function(childErr, fullItems)
						{
							fullItems = _.filter(fullItems, function(item) { return item.isPublished; });
							return cb(childErr, fullItems);
						});
					}
					else
					{
						items = _.filter(items, function(item) { return item.isPublished; });
						return cb(err, items);
					}
				});
			}

		,	function(items, cb)
			{
				self._getItemExtraAttributesFromSearchAPI(items, function(error, extraAtributesByItemId)
				{
					_.each(items, function(item)
					{
						if (extraAtributesByItemId[item.internalId])
						{
							_.extend(item, extraAtributesByItemId[item.internalId]);
						}
					});

					cb(null, items);
				});
			}

		,	function(items, cb)
			{
				_.each(items, function(item)
				{
					if (!Array.isArray(item.matrixOptions) || !Array.isArray(item.matrixChilds))
					{
						return true;
					}

					var matrixOptionsByScriptId = _.indexBy(item.matrixOptions, 'scriptId');

					item.matrixChilds.forEach(function(matrixItemChild)
					{
						matrixItemChild.matrixSelectedOptions.forEach(function(matrixItemChildOpts)
						{
							var matrixOptionEntry = matrixOptionsByScriptId[matrixItemChildOpts.optionScriptId];
							matrixItemChildOpts.label = matrixOptionEntry ? matrixOptionEntry.label : matrixItemChildOpts.label;
						});
					});

				});

				cb(null, items);
			}

		],	cb);
	}


,	appendMatrixChildsToSearchResult: function(params, cb)
	{
		var self = this;

		var matrixChildsParams = params;
		var parentItems = params.items;

		var itemsById = _.indexBy(parentItems, 'internalId');

		matrixChildsParams = _.pick(
			params
		,	[ 'webSite', 'isInactive', 'isOnline', 'type', 'subsidiary']
		);

		matrixChildsParams.matrix = false;
		matrixChildsParams.matrixChild = true;
		matrixChildsParams.parent = _.map(Object.keys(itemsById), String);

		// SET A HIGHER SEARCH RESPONSE LIMIT TO FIX ISSUES WITH
		// BIG STORES WITH 10+ MATRIX CHILDS
		matrixChildsParams.limit = 500;

		self._searchItems(matrixChildsParams, function(childErr, childRes)
		{
			childRes.forEach(function(childItem)
			{
				var parentInternalId = childItem.parent[0].internalId;
				// console.log(parentInternalId);

				if (!itemsById[parentInternalId].matrixChilds)
				{
					itemsById[parentInternalId].matrixChilds = [];
				}

				if (childItem.stockOnlineTotal)
				{
					itemsById[parentInternalId].stockOnlineTotal += childItem.stockOnlineTotal;
				}

				itemsById[parentInternalId].matrixChilds.push(childItem);
				_.extend(itemsById[parentInternalId], self.getItemPublicationStatus(itemsById[parentInternalId]));
			});

			cb(childErr, _.toArray(itemsById));
		});
	}


,	searchMatrixItems: function(params, cb)
	{
		params.matrix = true;
		this.searchItems(params, cb);
	}


,	emptyResultHandler: function(message, cb)
	{
		return function(error, result)
		{
			if (_(result).isEmpty())
			{
				pending(message);
			}
			else
			{
				cb(null, result);
			}
		};
	}


,	showItemData: function(resultset)
	{
		if (_.isEmpty(resultset))
		{
			console.log('showItemData: empty item data "' + JSON.stringify(resultset) + '" found.');
			return false;
		}

		if (resultset && !Array.isArray(resultset))
		{
			resultset = [resultset];
		}

		var properties = [
			'internalId', 'itemId', 'displayName', 'urlComponent', 'type'
		,	'outOfStockBehavior', 'stockOnlineTotal', 'isPublished'
		,	'minimumQuantity', 'isFulfillable', 'isTaxable', 'isMatrix'
		,	'parent', 'matrixChilds', 'matrixDimension', 'matrixSelectedOptions'
		, 	'priceOnline', 'priceOnlineLevel', 'priceOnlineByQuantity'
		];

		resultset.forEach(function (itemRecord)
		{
			if (_.isEmpty(itemRecord))
			{
				console.log('showItemData: empty value "' + JSON.stringify(itemRecord) + '" is not an item.');
				return false;
			}

			var itemRecordFiltered = _(itemRecord).pick(properties);

			if (Array.isArray(itemRecord.matrixChilds) && itemRecord.matrixChilds.length)
			{
				itemRecordFiltered.matrixChilds = _(itemRecord.matrixChilds).map(function(matrixChild)
				{
					return _(matrixChild).pick(properties);
				});
			}
			// THIS CONSOLE LOG IS PART OF THE FUNCTION, DON'T REMOVE IT
			console.log(JSON.stringify(itemRecordFiltered, null, 4));
		});
	}

};

