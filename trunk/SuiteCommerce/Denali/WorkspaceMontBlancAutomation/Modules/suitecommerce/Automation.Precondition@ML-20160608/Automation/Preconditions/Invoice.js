var suitetalk = require('suitetalk');

// NS ESTIMATE DOCUMENTATION
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/schema/record/invoice.html
// https://system.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2015_1/script/record/invoice.html

module.exports = {

	getInvoicesByCustomer: function(customer, credentials, cb)
	{
		suitetalk.setCredentials(credentials);

		suitetalk.search({
				recordType: 'transaction'
			,	filters: {
					type: {
						type: 'SearchEnumMultiSelectField'
					,	operator: 'anyOf'
					,	searchValue: [
							"_invoice"
						]
					}
				}

			,	joins: [{
					recordType: 'customer'
				,	filters: {
						internalId: {
							type: 'SearchMultiSelectField'
						,	operator: 'anyOf'
						,	searchValue: [{
									type: 'RecordRef'
								,	internalId: customer.internalId
							}]
						}
					}
				}]
			}
		)

		.then(function(response)
		{
			//console.log(response);
			var records = response.searchResponse[0].searchResult[0].recordList[0].record;

			if (Array.isArray(records)) {
				records.forEach(function(val, idx)
				{
					val.internalId = val.$.internalId;
				});
			}

			cb(null, records);
		})
		.catch(function(error)
		{
			console.log(error);
			cb(error);
		});
	}

}