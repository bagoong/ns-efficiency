
'use strict';

defineHelper('OrderWizard.Module.CartSummary',
{

	clickEditCart: function (cb)
	{
		this.click('.order-wizard-cartitems-module-edit-cart-link', cb);
	}

,	getSummaryDataType: function (include_products, cb)
	{
		var client = this;

		if (typeof include_products === 'function')
		{
			cb = include_products;
			include_products = false;
		}

		var datatype = {
			shipping: 0.0
		,	tax: 0.0
		,	handling: 0.0
		,	discount: 0.0
		,	subtotal: 0.0
		, 	total: 0.0
		};


		this
			.OrderWizard.Module.CartSummary.getShipping(function (err, text)
			{
				datatype.shipping = client.util.textToFloat(text);
			})

			.OrderWizard.Module.CartSummary.getHandling(function (err, text)
			{
				if (text)
				{
					datatype.handling = client.util.textToFloat(text);
				}
			})

			.OrderWizard.Module.CartSummary.getTax(function (err, text)
			{
				datatype.tax = client.util.textToFloat(text);
			})

			.OrderWizard.Module.CartSummary.getDiscount(function (err, text)
			{
				if (text)
				{
					datatype.discount = client.util.textToFloat(text);
				}
			})

			.OrderWizard.Module.CartSummary.getSubtotal(function (err, text)
			{
				datatype.subtotal = client.util.textToFloat(text);
			})

			.OrderWizard.Module.CartSummary.getTotal(function (err, text)
			{
				datatype.total = client.util.textToFloat(text);
			})

			.call(function ()
			{
				cb(null, datatype);
			})
		;

	}


,	getShipping: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-shipping-cost-formatted", cb)
		;
    }


,	getTax: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-tax-total-formatted", cb)
		;
    }

,	getHandling: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-handling-cost-formatted", cb)
		;
	}

,	getDiscount: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-discount-total", cb)
		;
    }


,	getPromoCode: function (cb)
    {
		this
			.getText(".order-wizard-cart-summary-shipping-cost-applied span", cb)
		;
    }


,	getSubtotal: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-subtotal span.order-wizard-cart-summary-grid-right", cb)
		;
	}


,	getTotal: function (cb)
	{
		this
			.getText(".order-wizard-cart-summary-total span", cb)
		;
	}


,	fillPromoCode: function (params, cb)
	{
		var client = this;

		this
			.waitFor('[aria-controls=order-wizard-promocode]', 10000)
			.click('[aria-controls=order-wizard-promocode]')

			.waitForExist('#promocode')
			.setValue('#promocode', params.code)

			.isExisting('.cart-promocode-form-summary-button-apply-promocode', function (err, existing)
			{
				client.click('.cart-promocode-form-summary-button-apply-promocode');
			})

			.waitForAjax()
		;

		this.call(cb);
	}


,	isPromoSet: function (cb)
	{
		this
			isVisible(".order-wizard-cart-summary-promo-code-applied", cb)
		;
    }


,	clickRemovePromoCode: function (cb)
    {
    	this
    		.click('[data-action=remove-promocode]')
    		.waitForAjax()
    		.call(cb)
    	;
    }


,	getPromoCodeLabel: function (cb)
	{
		/*
        return isPromoSet() ? helper.getTextIdentifiedBy(selectorsDictionary.orderSummaryModule_promoLabel.selectorValue) : "";
    	*/
    }


,	getPromoCodeDataType: function (cb)
    {
    	/*
        if(isPromoSet()){
            return new PromoDT(helper.getTextIdentifiedBy(selectorsDictionary.orderSummaryModule_promoLabel.selectorValue), helper.currencyToNumber(helper.getTextIdentifiedBy(selectorsDictionary.orderSummaryModule_discount.selectorValue).replaceAll("\\\$","").replaceAll(" Discount Total","")), helper.getTextIdentifiedBy(locator_promo).replaceAll("-",""));
        }
        */
    }

});
