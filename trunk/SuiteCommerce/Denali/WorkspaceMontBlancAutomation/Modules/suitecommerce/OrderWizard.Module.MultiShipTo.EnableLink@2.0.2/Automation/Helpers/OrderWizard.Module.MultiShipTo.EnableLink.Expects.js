
defineHelper('OrderWizard.Module.MultiShipTo.EnableLink.Expects', 
{
	checkIfMultiShipToLinkIsPresent: function(cb)
	{
		this
			.getText(Selectors.OrderWizardModuleMultiShipToEnableLink.ChangeShippingModeLink, function(err, text)
			{
				expect(text).toEqual('I want to ship to multiple addresses')
				
			})
			.call(cb)
		;
	}

,	checkIfSingleShipToLinkIsPresent: function(cb)
	{
		this
			.getText(Selectors.OrderWizardModuleMultiShipToEnableLink.ChangeShippingModeLink, function(err, text)
			{
				expect(text).toEqual('I want to ship to a single address')
				
			})
			.call(cb)
		;
	}


});

