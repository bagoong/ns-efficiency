defineHelperDependencies('OrderWizard.Module.Address', 'Address', 'GlobalViews.Modal')

defineHelper('OrderWizard.Module.Address', 
{
	
	hasSelectedShippingAddress: function(cb)
	{
		this
			.isExisting(Selectors.OrderWizardModuleAddress.selectedShippingAddress, cb)
		;
	}


,	hasSelectedBillingAddress: function(cb)
	{
		this
			.isExisting(Selectors.OrderWizardModuleAddress.selectedBillingAddress, cb)
		;
	}


,	clickChangeShippingAddress: function(cb)
	{
		var params = {parentElementXPath: Selectors.OrderWizardModuleAddress.shippingAddressContainerXPath};
		this.Address.clickChangeAddress(params, cb);
	}	


,	clickChangeBillingAddress: function(cb)
	{
		var params = {parentElementXPath: Selectors.OrderWizardModuleAddress.billingAddressContainerXPath};
		this.Address.clickChangeAddress(params, cb);
	}	


,	clickChangeShippingAddressIfPreviousSelected: function(cb)
	{
		var client = this;
		
		this
			.OrderWizard.Module.Address.hasSelectedShippingAddress(function(err, has_selected)
			{
				if (has_selected) 
				{
					client.OrderWizard.Module.Address.clickChangeShippingAddress();
				}
			})
			.call(cb)
		;
	}


,	clickChangeBillingAddressIfPreviousSelected: function(cb)
	{
		var client = this;

		this
			.OrderWizard.Module.Address.hasSelectedBillingAddress(function(err, has_selected)
			{
				if (has_selected) 
				{
					client.OrderWizard.Module.Address.clickChangeBillingAddress();
				}
			})
			.call(cb)
		;
	}	


,	clickAddNewBillingAddress: function(cb)
	{
		var params = {parentElementXPath: Selectors.OrderWizardModuleAddress.billingAddressContainerXPath};
		this.OrderWizard.Module.Address.clickAddNewAddress(params, cb);
	}


,	clickAddNewShippingAddress: function(cb)
	{
		var client = this;

		this
			.isExisting(Selectors.OrderWizardModuleAddress.addNewMultiShipToAddressButton, function(err, exists_multi_ship_to_button)
			{
				if (exists_multi_ship_to_button)
				{
					client
						.click(Selectors.OrderWizardModuleAddress.addNewMultiShipToAddressButton)
						.call(cb)
					;
				}
				else
				{
					var params = {parentElementXPath: Selectors.OrderWizardModuleAddress.shippingAddressContainerXPath};
					client.OrderWizard.Module.Address.clickAddNewAddress(params, cb);
				}
			})
		;
	}


,	clickAddNewAddress: function(params, cb)
	{
		if (typeof params === 'function') {
			cb = params;
			params = {};
		}

		var parent_element = params.parentElementXPath || "";
		var new_address_xpath = parent_element + Selectors.OrderWizardModuleAddress.addNewAddressButtonXPath;

		this
			.waitForExist(new_address_xpath, 5000)
			.click(new_address_xpath)
			.GlobalViews.Modal.waitForModal()
			.call(cb)
		;
	}

,	fillShippingAddress: function(params, cb)
	{
		params = this.util.cloneData(params);
		params.parentElementXPath = Selectors.OrderWizardModuleAddress.shippingAddressContainerXPath;
		this.Address.fillAddress(params, cb);
	}

,	fillBillingAddress: function(params, cb)
	{
		params = this.util.cloneData(params);
		params.parentElementXPath = Selectors.OrderWizardModuleAddress.billingAddressContainerXPath;
		this.Address.fillAddress(params, cb);
	}	

});
