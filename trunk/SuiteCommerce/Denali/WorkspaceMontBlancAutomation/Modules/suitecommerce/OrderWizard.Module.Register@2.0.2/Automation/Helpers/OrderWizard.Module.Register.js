
defineHelper('OrderWizard.Module.Register', 
{
	
	fillGuestEmail: function(guest_email, cb)
	{
		this
			.setValue('.order-wizard-registeremail-module-edit-fields-group-input', guest_email)
			.waitForAjax()
			.call(cb)
		;
	}

});

