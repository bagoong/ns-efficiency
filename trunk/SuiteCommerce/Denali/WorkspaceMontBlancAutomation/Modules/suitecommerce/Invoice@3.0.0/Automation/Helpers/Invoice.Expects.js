defineHelper('Invoice.Expects',
{

	invoiceListTitle: function(cb)
	{
		this
			.waitForExist(Selectors.Invoice.listTitle)
			.getText(Selectors.Invoice.listTitle, function(err, text){
				expect(text).toBeSameText('INVOICES');
			})
			.call(cb)
		;
	}

,	checkInvoiceDetailPageOpened: function(cb)
	{
		this
			.isExisting(Selectors.Invoice.detailPageContainer, function(err, is_existing)
			{
				expect(is_existing).toBeTruthy();
			})
			.call(cb)
		;
	}

,	paidInvoiceExists: function (params, cb)
	{
		this
			.isExisting(Selectors.Invoice.enterOrder.replace('%s', params.invoiceId), function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })

			.call(cb)
		;
	}

,	paidInvoiceOverviewInformation: function (params, cb)
	{
		this
			.waitForExist(Selectors.Invoice.paidInvoiceNo.replace('%s', params.invoiceId), 5000)
			.getText(Selectors.Invoice.paidInvoiceNo.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceNo);
			})

			.getText(Selectors.Invoice.paidInvoiceDate.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceDate);
			})

			.getText(Selectors.Invoice.paidInvoiceCloseDate.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceCloseDate);
			})

			.getText(Selectors.Invoice.paidInvoiceAmount.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceAmount);
			})
			.call(cb)
		;
	}

,	openInvoiceOverviewInformation: function (params, cb)
	{
		this
			.waitForExist(Selectors.Invoice.openInvoiceNo.replace('%s', params.invoiceId), 5000)
			.getText(Selectors.Invoice.openInvoiceNo.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceNo);
			})

			.getText(Selectors.Invoice.openInvoiceDate.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceDate);
			})

			.getText(Selectors.Invoice.openInvoiceDueDate.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceDueDate);
			})

			.getText(Selectors.Invoice.openInvoiceAmount.replace('%s', params.invoiceId), function(err, text){
				expect(text).toMatch(params.invoiceAmount);
			})
			.call(cb)
		;
	}

,	openInvoiceExists: function (params, cb)
	{
		this
			.isExisting(Selectors.Invoice.selectableRow.replace('%s', params.invoiceId), function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })

			.call(cb)
		;
	}

,	overdueFlag: function (params, cb)
	{
		this
			.isExisting(Selectors.Invoice.overdueFlag.replace('%s', params.invoiceId), function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })

			.call(cb)
		;
	}

,	makeAPaymentButton: function (params, cb)
	{
		this
			.waitForAjax()
			.isExisting(Selectors.Invoice.makeAPaymentButton, function(err, isExisting) {
		        expect(isExisting).toBe(params.makeAPayment);
		    })

			.call(cb)
		;
	}

,	invoicesToPayTitle: function(cb)
	{
		this
			.waitForAjax()
			.isExisting(Selectors.Invoice.paymentWizardTitle, function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })

			.call(cb)
		;
	}

,	sectionsLoaded: function(cb)
	{
		this
			.waitForAjax()
			// DETAILS
			.isExisting(Selectors.Invoice.invoiceDetails, function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })
			// SUMMARY
			.isExisting(Selectors.Invoice.invoiceSummary, function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })
			// CONTENT
			.isExisting(Selectors.Invoice.invoiceContent, function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })
			.call(cb)
		;
	}

,	checkInvoiceInformation: function(params, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.Invoice.invoiceNo, 5000)
			.getText(Selectors.Invoice.invoiceNo, function(err, text){
				expect(text).toMatch(params.invoiceNo);
			})
			.getText(Selectors.Invoice.invoiceDate, function(err, text){
				expect(text).toMatch(params.invoiceDate);
			})
			.getText(Selectors.Invoice.invoiceOrder, function(err, text){
				expect(text).toMatch(params.invoiceOrder);
			})
			.getText(Selectors.Invoice.invoiceDueDate, function(err, text){
				expect(text).toMatch(params.invoiceDue);
			})
			.getText(Selectors.Invoice.invoiceTerm, function(err, text){
				expect(text).toMatch(params.invoiceTerm);
			})
			.getText(Selectors.Invoice.invoiceMemo, function(err, text){
				expect(text).toMatch(params.invoiceMemo);
			})
			.getText(Selectors.Invoice.invoiceItems, function(err, text){
				expect(text).toMatch(params.invoiceItems);
			})
			.getText(Selectors.Invoice.invoiceTax, function(err, text){
				expect(text).toMatch(params.invoiceTax);
			})
			.getText(Selectors.Invoice.invoiceShipping, function(err, text){
				expect(text).toMatch(params.invoiceShipping);
			})
			.getText(Selectors.Invoice.invoiceHandling, function(err, text){
				expect(text).toMatch(params.invoiceHandling);
			})
			.getText(Selectors.Invoice.invoiceAmountDue, function(err, text){
				expect(text).toMatch(params.invoiceAmountDue);
			})
			.getText(Selectors.Invoice.invoiceStatus, function(err, text){
				expect(text).toMatch(params.invoiceStatus);
			})
			.call(cb)
		;
	}

,	checkInvoiceOverviewDataDonChangeInInvoiceDetails: function(params, cb)
	{
		var client = this;
		this
			.Invoice.getInvoiceDataTypeFromOverview(params, function(err, overviewDT)
			{
				client
					.Invoice.openInvoiceClick(params)
					.Invoice.getInvoiceDataTypeFromDetails(function(err, detailsDT)
					{
						expect(overviewDT.invoiceNo).toMatch(detailsDT.invoiceNo);
						expect(overviewDT.invoiceDate).toMatch(detailsDT.invoiceDate);
						expect(overviewDT.invoiceDueDate).toMatch(detailsDT.invoiceDueDate);
						expect(overviewDT.invoiceAmount).toEqual(detailsDT.invoiceAmount);
						expect(overviewDT.invoiceStatus).toMatch(detailsDT.invoiceStatus);
					})
				;
			})
			.call(cb)
		;
	}

,	checkAmountDueCorrectlyCalculated: function(cb)
	{
		var client = this
		var summary =
		{
			'ItemPrice' : 0
		,	'Tax'       : 0
		,	'Shipping'  : 0
		,	'Handling'  : 0
		,	'AmountDue' : 0
		}
		this
			.Invoice.getInvoiceDataTypeFromDetails(function(err, detailsDT)
			{
				summary.ItemPrice = client.util.textToFloat(detailsDT.invoiceItemsPrice);
				summary.Tax       = client.util.textToFloat(detailsDT.invoiceTax);
				summary.Shipping  = client.util.textToFloat(detailsDT.invoiceShippingPrice);
				summary.Handling  = client.util.textToFloat(detailsDT.invoiceHandlingPrice);
				summary.AmountDue = client.util.textToFloat(detailsDT.invoiceAmount);
			})
			.call(function()
			{
				expect(summary.ItemPrice + summary.Tax + summary.Shipping + summary.Handling).toEqual(summary.AmountDue)
			})
			.call(cb)
		;
	}

,	expectPaidInvoiceByName: function (params, cb)
	{
		this
			.isExisting(Selectors.Invoice.invoiceByName.replace('%s', params.invoiceName), function(err, isExisting) {
		        expect(isExisting).toBeTrue();
		    })

			.call(cb)
		;
	}

,	compareListWithDisplayedOpenInvoice: function(params, cb)
	{
		this
			.waitForExist(Selectors.Invoice.dataIdValue, 5000)
			.getAttribute(Selectors.Invoice.dataIdValue,'data-id' ,function(err, text){
				params.sort();
				expect(text).toEqual(params);
			})
			.call(cb)
		;
	}

,	compareListWithDisplayedPaidInvoice: function(params, cb)
	{
		this
			.waitForExist(Selectors.Invoice.dataIdValuePaid, 5000)
			.getAttribute(Selectors.Invoice.dataIdValuePaid,'data-item-id' ,function(err, text){
				params.sort();
				expect(text).toEqual(params);
			})
			.call(cb)
		;
	}

,	checkingListWithDateSelected: function (date, cb) {
		var client = this
		,	date_vale
		,	date_vale_ts
		,	date_from = new Date(date.from)
		,	date_from_ts = date_from.getTime()
		,	date_to = new Date(date.to)
		,	date_to_ts = date_to.getTime();

		this

			.getAttribute('.recordviews-row', 'data-item-id', function(err, data_item_ids) {

				if (data_item_ids !== undefined)
				{
					data_item_ids.forEach(function(data_id)
					{
						client
							.getText('.recordviews-row[data-item-id=\'' + data_id + '\'] [data-name=\'date\']', function(err, val)
							{

								date_vale = new Date(val);
								date_vale_ts = date_vale.getTime();

								if (date_from_ts === date_to_ts) {

									expect(date_from_ts === date_vale_ts).toEqual(true, 'Error: List have incorrect data')

								}else if (date_from_ts < date_to_ts) {

									if (date_vale_ts !== date_from_ts)  {
										expect(date_vale).toBeAfter(date_from)
									}

									if (date_vale_ts !== date_to_ts) {
										expect(date_vale).toBeBefore(date_to)
									}
								}
							})

						;
					});
				}else {
					client
						.isExisting(Selectors.Invoice.invoicePaidInFullNoRecords, function(err, exists) {
							expect(exists).toEqual(true, 'Error: Selector ' + Selectors.Invoice.invoicePaidInFullNoRecords + ' not exist')
						})
				}
			})
		.call(cb)
	}

,	checkInvoicePageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Invoice.invoicePageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Invoice page is not present.');
        	})
        	.call(cb)
        ;
	}

});