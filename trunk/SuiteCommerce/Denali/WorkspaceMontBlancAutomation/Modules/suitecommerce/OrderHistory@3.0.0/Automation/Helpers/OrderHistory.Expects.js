defineHelper('OrderHistory.Expects', 
{
	verifyOrderHistoryIsEmpty: function(cb)
	{
		this
			.waitForExist(Selectors.OrderHistory.emptyOrders, 5000)
			.getText(Selectors.OrderHistory.emptyOrders).then(function (text)
			{
				expect(text).toMatch('You don\'t have any purchases in your account right now.');
			})
			.call(cb)
		;
	}

,	verifyCorrectNumberOfItems: function(param, cb)
	{
		this
			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.recordViews, 10000)
			.getText(Selectors.OrderHistory.recordViews).then(function (text) 
			{
				expect(text.length).toBe(param.expectedAmount);
			})
			.call(cb)
		;
	}	

,	verifyPurchaseIsDisplayedInPurchaseHistory: function (purchase,cb)
	{
		this
			.OrderHistory.getOrderDetailsById(purchase.purchaseId).then(function (found_order)
			{
            	expect(found_order).toHaveEqualFields(purchase);
        	})
        	.call(cb)
        ;
	}

,	verifyPurchaseIsDisplayedInPurchaseHistoryWithoutSCISBundle: function (purchase,cb)
	{
		this
			.OrderHistory.getOrderDetailsWithoutSCISBundleById(purchase.purchaseId).then(function (found_order)
			{
            	expect(found_order).toHaveEqualFields(purchase);
        	})
        	.call(cb)
        ;
	}

,	checkOrderHistoryPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.OrderHistory.orderHistoryPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Order History page is not present.');
        	})
        	.call(cb)
        ;
	}

,	verifyPurchaseCanceledCorrectly: function (cb)
	{
		this
			.waitForExist(Selectors.OrderHistory.cancelMessageSucces, 5000)
			.isExisting(Selectors.OrderHistory.cancelMessageSucces).then(function (isExisting)
			{
            	expect(!isExisting).toBeTrueOrFailWith('Message succesfully cancel purchase not present.');
        	})

			.waitForAjax()
			.waitForExist(Selectors.OrderHistory.detailStatus, 5000)
        	.getText(Selectors.OrderHistory.detailStatus).then(function (text)
			{
				expect(text).toMatch('Cancelled');
			})

        	.call(cb)
        ;
	}

});


