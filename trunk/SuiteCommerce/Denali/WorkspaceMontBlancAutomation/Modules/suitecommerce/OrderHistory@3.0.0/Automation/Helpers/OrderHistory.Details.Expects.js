defineHelper('OrderHistory.Details.Expects', 
{

	checkPurchaseHeader: function (params, cb)
	{
		this			
			.getText(Selectors.OrderHistory.detailNumber).then(function (text)
			{
				expect(text).toMatch(params.purchaseNo);
			})

			.getText(Selectors.OrderHistory.detailDate).then(function (text)
			{
				expect(text).toMatch(params.date);
			})

			.getText(Selectors.OrderHistory.detailTotal).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.call(cb)
		;
	}
,   checkShippingInformation: function (params, cb)
	{
		var client = this;
		if(params)
		{
			params.forEach(function (shippment)
			{
				client
					.isExisting(Selectors.OrderHistory.shippment.replace('%s', shippment.shippmentId)).then( function (isExisting)
					{
						expect(isExisting).toBeTrueOrFailWith('Shippment to ' + shippment.shippingAddress.fullname + ' is not present');
						if(isExisting)
						{
							client
								.OrderHistory.Details.Expects.checkShippingAddress(shippment)

						        .OrderHistory.Details.Expects.checkDeliveryMethod(shippment)

						        .OrderHistory.Details.Expects.checkFulfillments(shippment)

						        .OrderHistory.Details.Expects.checkItems(shippment)
							;
						}
					})
			    ;	
			})
		}
		client
			.call(cb)
		;
	}

,   checkShippingAddress: function (params, cb)
	{
		var client = this;
		var shippmentSelector = '';
		if(params.shippmentId)
		{
			client
				.getAttribute(Selectors.OrderHistory.shippmentId, 'data-id').then(function (text){
					expect(text).toMatch(params.shippmentId);
					shippmentSelector = Selectors.OrderHistory.shippment.replace('%s', params.shippmentId);	
					client
						.Address.getAddressDataType(
							{
								parentElementXPath: shippmentSelector + Selectors.OrderHistory.shippingAddress
							}
							, function (err, found_address) 
							{
								expect(found_address).toHaveEqualFields(params.shippingAddress);
							}
						)
						.call(cb)
					;
				})
			;
		}
		else
		{
			client
				.Address.getAddressDataType(
					{
						parentElementXPath: shippmentSelector + ' ' + Selectors.OrderHistory.shippingAddress
					}
					, function (err, found_address) {
						expect(found_address).toHaveEqualFields(params.shippingAddress);
					}
				)
				.call(cb)
			;
		}
	}

,	checkDeliveryMethod: function (params, cb)
	{
		var client = this;
		var shippmentSelector = '';
		if(params.shippmentId)
		{
			client
				.getAttribute(Selectors.OrderHistory.shippmentId, 'data-id').then(function (text){
					expect(text).toMatch(params.shippmentId);
					shippmentSelector = Selectors.OrderHistory.shippment.replace('%s', params.shippmentId);
				})
			;

		}
		client 
			.getText(Selectors.OrderHistory.detailDeliveryMethod).then(function (text)
			{
				expect(text).toMatch(params.deliveryMethod.deliveryMethod);
			})

			.call(cb)
		;
	}

,   checkFulfillments: function (shippment, cb)
	{
		var client = this;
		if(shippment.fulfillments)
		{
			shippment.fulfillments.forEach(function (fulfillment)
			{
				client
					.OrderHistory.Details.Expects.checkFulfillmentHeader(fulfillment)

			        .OrderHistory.Details.Expects.checkItems(fulfillment)
			    ;	
			})
		}
		client
			.call(cb)
		;
	}

,	checkFulfillmentHeader: function (params, cb)
	{
		var fulfillmentSelector = Selectors.OrderHistory.fulfillmentId.replace('%s', params.fulfillmentId) + ' ';
		this
			.getAttribute(Selectors.OrderHistory.fulfillment, 'data-id').then(function (text){
				expect(text).toMatch(params.fulfillmentId);
			})

			.getText(fulfillmentSelector + Selectors.OrderHistory.fulfillmentStatus).then(function (text)
			{
				expect(text).toMatch(params.header.status);
			})

			.getText(fulfillmentSelector + Selectors.OrderHistory.fulfillmentDeliveryMethod).then(function (text)
			{
				expect(text).toMatch(params.header.deliveryMethod);
			})

			.getText(fulfillmentSelector + Selectors.OrderHistory.fulfillmentDate).then(function (text)
			{
				expect(text).toMatch(params.header.date);
			})
			
			.OrderHistory.Details.getTrakingValue(params.fulfillmentId, function (err, values){
				expect(values).toEqual(params.header.trackPackages);
			})

			.call(cb)
		;
	}
    
,   checkItems: function (params, cb)
	{
		var client = this;
		if(params.itemList)
		{
			params.itemList.forEach(function (item)
			{
				client
					.getAttribute(Selectors.OrderHistory.itemId, 'data-item-id').then(function (text){
						expect(text).toMatch(item.internalId);
					})

					.isExisting(Selectors.OrderHistory.itemName.replace('%s', item.internalId)).then(function (isExisting)
					{
						expect(isExisting).toBeTrueOrFailWith('Item ' + item.name + ' is not present');
						if(isExisting)
						{
							client
								.getText(Selectors.OrderHistory.itemName.replace('%s', item.internalId)).then(function (text){
									expect(text.toLowerCase()).toMatch(item.name.toLowerCase());
								})

								.getText(Selectors.OrderHistory.itemSku.replace('%s', item.internalId)).then(function (text){
									expect(text.toLowerCase()).toMatch(item.sku.toLowerCase());
								})

								.getText(Selectors.OrderHistory.itemPrice.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.price);
								})

								.getText(Selectors.OrderHistory.itemQuantity.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.quantity);
								})

								.getText(Selectors.OrderHistory.itemAmount.replace('%s', item.internalId)).then(function (text){
									expect(text).toMatch(item.amount);
								})

								if(item.options)
								{
									item.options.forEach(function (option)
									{
										client
											.getText(Selectors.OrderHistory.itemOptionLabel.replace('%s', item.internalId)).then(function (text){
												expect(text).toMatch(option.title);
											})

											.getText(Selectors.OrderHistory.itemOptionValue.replace('%s', item.internalId)).then(function (text){
												expect(text).toMatch(option.value);
											})
										;
									})
								}
							;
						}
					})
				;
			})
		}

		client
			.call(cb)
		;
	}

,   checkPaymentInformation: function (params, cb)
	{
		var client = this;

		client
			.waitForAjax()
			.OrderHistory.Details.Expects.checkPaymentMethod(
	            params.paymentMethod
	        )
    	;
    	if(params.billingAddress)
    	{
    		client
		        .OrderHistory.Details.Expects.checkBillingAddress(
		            params.billingAddress
		        )
		    ;
    	}    
    	client    
			.call(cb)
		;
	}
    
,   checkPaymentMethod: function (paymentMethod, cb)
	{
		var client = this;
		paymentMethod.forEach(function (payment)
		{
			
			if (payment.paymentType === 'Credit Card') {
	            client
	                .OrderHistory.Details.Expects.checkCreditCard(payment)
	            ;
	        }
	        else if (payment.paymentType === 'Deposit' || payment.paymentType === 'Credit Memo')
	        {
	        	client
        			.OrderHistory.Details.Expects.checkOtherPayment(payment)
        		;
	        }
	        else if (payment.paymentType === 'Cash' || payment.paymentType === 'Check' || payment.paymentType === 'Paypal')
	        {
	        	if(payment.internalId)
	        	{
	        		client
	        			.OrderHistory.Details.Expects.checkPayment(payment);
	        	}
	        	else
	        	{
		            client
		            	.isExisting(Selectors.OrderHistory.paymentMethod).then(function (isExisting)
						{
							expect(isExisting).toBeTrueOrFailWith('Payment Method is not present');
						})
						.getText(Selectors.OrderHistory.paymentMethod).then(function (text)
						{
							expect(text).toMatch(payment.paymentInfo);
						})
					;
	        	}

	        }
		})
		this
			.call(cb)
		;
	}



,   checkCreditCard: function (creditCard, cb)
	{
		this
			.getAttribute(Selectors.OrderHistory.creditCardIcon, 'alt').then(function (text)
			{
				expect(text).toMatch(creditCard.ccName);
			})

			.isExisting(Selectors.OrderHistory.creditCardNumber).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card Number is not present');
			})
			.getText(Selectors.OrderHistory.creditCardNumber).then(function (text)
			{
				var regexCC = creditCard.ccNumber.match(/\d{4}$/i); 
				expect(text).toMatch(regexCC[0]);
			})

			.isExisting(Selectors.OrderHistory.creditCardExpDate).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card ExpDate is not present');
			})
			.getText(Selectors.OrderHistory.creditCardExpDate).then(function (text)
			{
				expect(text).toMatch(creditCard.ccExpireMonth + '/' + creditCard.ccExpireYear);
			})

			.isExisting(Selectors.OrderHistory.creditCardName).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Credit Card Name is not present');
			})
			.getText(Selectors.OrderHistory.creditCardName).then(function (text)
			{
				expect(text).toMatch(creditCard.nameInCard);
			})			

			.call(cb)
		;
	}
    
,   checkBillingAddress: function (params, cb)
	{
		var client = this;
		client 
			.isExisting(Selectors.OrderHistory.billingAddress).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Billing Address is not displayed');
				if(isExisting){
					client
						.Address.getAddressDataType(
							{
								parentElementXPath: Selectors.OrderHistory.billingAddress
							}
							, function (err, found_address) 
							{	
								if(found_address)
								{
									expect(params.fullname.toLowerCase()).toBe(found_address.fullname.toLowerCase());
									if(params.company || (found_address.company != ''))
									{
										expect(params.company.toLowerCase()).toBe(found_address.company.toLowerCase());
									}
									expect(params.addr1).toBe(found_address.addr1);
									expect(params.addr2).toBe(found_address.addr2);
									expect(params.city.toLowerCase()).toBe(found_address.city.toLowerCase());
									expect(params.country.toLowerCase()).toBe(found_address.country.toLowerCase());
									expect(params.state.toLowerCase()).toBe(found_address.state.toLowerCase());
									expect(params.zip).toBe(found_address.zip);
									if(params.phone || (found_address.phone != undefined))
									{
										expect(params.phone).toBe(found_address.phone);
									}						
								}
							}
						)
					;	
				}
			})

			.call(cb)
		;
	}

,	checkOtherPayment: function (payment, cb)
	{
		var client = this;
		client
			.isExisting(Selectors.OrderHistory.otherPaymentNumber.replace('%s', payment.internalId)).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Other Payment Method ' + payment.number + ' is not present');
			})

			.getText(Selectors.OrderHistory.otherPaymentNumber.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.number);
			})

			.getText(Selectors.OrderHistory.otherPaymentDate.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.date);
			})

			.getText(Selectors.OrderHistory.otherPaymentAmount.replace('%s', payment.internalId)).then(function (text)
			{
				expect(text).toMatch(payment.amount);
			})
			
			.call(cb)
		;
	}

,	checkPayment: function (payment, cb)
	{
		var client = this;

		client
			.Configuration.isSiteBuilderBasic(function (err, isBasic)
			{
				if(isBasic)
				{
					client
						.isExisting(Selectors.OrderHistory.paymentNumberLabel.replace('%s', payment.internalId)).then(function (isExisting)
						{
							expect(isExisting).toBeTrueOrFailWith('Payment Method ' + payment.number + ' is not present');
						})

						.getText(Selectors.OrderHistory.paymentNumberLabel.replace('%s', payment.internalId)).then(function (text)
						{
							expect(text).toMatch(payment.number);
						})
					;
				}
				else
				{
					client
						.isExisting(Selectors.OrderHistory.paymentNumber.replace('%s', payment.internalId)).then(function (isExisting)
						{
							expect(isExisting).toBeTrueOrFailWith('Payment Method ' + payment.number + ' is not present');
						})

						.getText(Selectors.OrderHistory.paymentNumber.replace('%s', payment.internalId)).then(function (text)
						{
							expect(text).toMatch(payment.number);
						})
					;
				}
				client
					.getText(Selectors.OrderHistory.paymentDate.replace('%s', payment.internalId)).then(function (text)
					{
						expect(text).toMatch(payment.date);
					})

					.getText(Selectors.OrderHistory.paymentAmount.replace('%s', payment.internalId)).then(function (text)
					{
						expect(text).toMatch(payment.amount);
					})

					.getText(Selectors.OrderHistory.paymentInfoCard.replace('%s', payment.internalId) + ' ' + Selectors.OrderHistory.paymentMethod).then(function (text)
					{
						expect(text).toMatch(payment.paymentType);
					})
				;
			})
			
			.call(cb)
		;
	}

,   checkReturns: function (params, cb)
	{
		if(params)
		{
			//TODO
		}
		this
			.call(cb)
		;
	}
    
,   checkSummary: function (params, cb)
	{
		var client = this;

		if (params.discountTotal)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryDiscount).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Discount is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryDiscount).then(function (text)
				{
					expect(text).toMatch(params.discountTotal);
				})
			;
		}

		if (params.shippingTotal)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryShipping).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Shipping total is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryShipping).then(function (text)
				{
					expect(text).toMatch(params.shippingTotal);
				})
			;
		}

		if (params.handlingTotal)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryHandling).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Handling total is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryHandling).then(function (text)
				{
					expect(text).toMatch(params.handlingTotal);
				})
			;
		}

		if (params.giftCertTotal)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryGiftCert).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Gift Certificate is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryGiftCert).then(function (text)
				{
					expect(text).toMatch(params.giftCertTotal);
				})
			;
		}

		if (params.promoCode)
		{
			client
				.isExisting(Selectors.OrderHistory.summaryPromoCode).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Promo code is not present in summary');
				})
				.getText(Selectors.OrderHistory.summaryPromoCode).then(function (text)
				{
					expect(text).toMatch(params.promoCode);
				})
			;
		}

		this
			.isExisting(Selectors.OrderHistory.summarySubtotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Subtotal is not present in summary');
			})
			.getText(Selectors.OrderHistory.summarySubtotal).then(function (text)
			{
				expect(text).toMatch(params.subTotal);
			})

			.isExisting(Selectors.OrderHistory.summaryTax).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Tax total is not present in summary');
			})
			.getText(Selectors.OrderHistory.summaryTax).then(function (text)
			{
				expect(text).toMatch(params.taxTotal);
			})

			.isExisting(Selectors.OrderHistory.summaryTotal).then(function (isExisting)
			{
				expect(isExisting).toBeTrueOrFailWith('Total is not present in summary');
			})
			.getText(Selectors.OrderHistory.summaryTotal).then(function (text)
			{
				expect(text).toMatch(params.total);
			})

			.call(cb)
		;
	}

,	checkReorderItem: function (items, cb)
	{
		var client = this;
		items.forEach(function (item)
		{
			client
				.getAttribute(Selectors.OrderHistory.itemId, 'data-item-id').then(function (text){
					expect(text).toMatch(item.internalId);
				})

				.isExisting(Selectors.OrderHistory.itemReorderThisItem.replace('%s', item.internalId)).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Reorder this item button is not present in item.');
				})

				.click(Selectors.OrderHistory.itemReorderThisItem.replace('%s', item.internalId))

				.waitForAjax()

				.isExisting(Selectors.OrderHistory.itemReorderSuccessMessage.replace('%s', item.internalId)).then(function (isExisting)
				{
					expect(isExisting).toBeTrueOrFailWith('Item successfull message was not shown.');
				})
			;
		})
		client
			.call(cb)
		;
	}

,	checkOrderTitle: function (cb)
	{
		this
			.isExisting(Selectors.OrderHistory.titleOrder).then(function (text)
			{
				expect(text).toBeTrue();
			})
			.call(cb)
		;
	}

});