defineHelper('Cart.SB', 
{

	removeItem: function(params, cb)
	{
		this
			.waitForExist(Selectors.Cart.removeFromCart.replace('%s', params), 5000)
			.click(Selectors.Cart.removeFromCart.replace('%s', params))
			.waitForAjax()
			.call(cb)
		;			
	}

});	