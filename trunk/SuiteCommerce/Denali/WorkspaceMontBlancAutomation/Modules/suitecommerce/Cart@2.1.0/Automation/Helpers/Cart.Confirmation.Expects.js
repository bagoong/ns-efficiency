defineHelper('Cart.Confirmation.Expects',
{

	checkItemData: function (data, item, cb)
	{
		data.name && expect(data.name).toBeSameText(item.name);
		data.sku && expect(data.sku).toEqual(item.sku);
		//data.imageUrl && expect(unescape(item.imageUrl)).toContain(data.imageUrl);
		data.itemPrice && expect(data.itemPrice).toBeSameMoney(item.itemPrice);
		//data.itemTotal && expect(data.itemTotal).toEqual(item.itemTotal);
		console.log("data.options: "+JSON.stringify(data.options, null, 4))
		console.log("data.item: "+JSON.stringify(data.item, null, 4))
		if (data.options)
		{
			expect(data.options.length).toEqual(Object.keys(item.options).length);

			data.options.forEach(function(value, index)
			{
				var option = item.options[value.label];

				expect(option.label).toEqual(value.label + ':');
				expect(option.value).toEqual(value.value);
			});
		}

		this
			.call(cb)
		;
	}

});
