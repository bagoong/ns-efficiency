defineHelper('Cart.OrderSummary',
{

	validateZipField: function(cb)
	{
		var client = this;
		var isExisting = false;

		this
			.Cart.OrderSummary.expandTaxAndShipping()
			.waitForExist('.cart-summary-zip-code', 3000)
			.click('.cart-summary-button-estimate')
			.getText('.//*[@class=\'cart-summary-zip-code\']/../p', function(err, text)
			{
				if(text === 'Zip Code is required')
				{
					isExisting = true
				}
			})
			.call(function()
			{
				cb(null, isExisting)
			})
		;
	}

,	estimateTaxAndShipping: function(params, cb)
	{
		this
			.Cart.OrderSummary.expandTaxAndShipping()
			.waitForExist('.cart-summary-zip-code', 10000)
			.selectByVisibleText('.country[name=country]', params.country)
			.setValue('.cart-summary-zip-code', params.zip)
			.click('.cart-summary-button-estimate')
			.waitForAjax()
			.call(cb)
		;
	}

,	getEstimation: function(cb)
	{
		var client = this;
		var dataType = {
				'isVisible': false
			,	'zip': ''
			,	'shippingPrice': ''
			,	'taxPrice': ''
			,	'handlingPrice': ''
			}
		;
		this
			.waitForAjax()
			.isExisting('.cart-summary-shipping-cost-applied', function(err, isExisting)
			{
				if(isExisting)
				{
					dataType.isVisible = true;
					client
						.waitForExist('.cart-summary-label-shipto-success', 10000)
						.getText('.cart-summary-label-shipto-success', function(err, text){
							dataType.zip = text;
						})
						.getText('.cart-summary-amount-shipping', function(err, text){
							dataType.shippingPrice = text;
						})
						.getText('.cart-summary-amount-tax', function(err, text){
							dataType.taxPrice = text;
						})
						.isExisting('.cart-summary-amount-handling', function(err, exists)
						{
							if(exists)
							{
								client
									.getText('.cart-summary-amount-handling', function(err, text){
										dataType.handlingPrice = text;
									})
								;
							}
							else
							{
								dataType.handlingPrice = '$0.00'
							}
						})
						.call(function()
						{
							cb(null, dataType)
						})
					;
				}
				else
				{
					client
						.call(function()
						{
							cb(null, dataType)
						})
					;
				}
			})
		;
	}

,	removeEstimation: function(cb)
	{
		this
			.waitForAjax()
			.waitForExist('.cart-summary-remove-action', 10000)
			.click('.cart-summary-remove-action')
			.waitForAjax()
			.call(cb)
		;
	}

,	expandTaxAndShipping: function(cb)
	{
		installHelpers(this,'generic')
		var client = this;
		this
			.waitForAjax()
			.waitForExist('.cart-summary-container .cart-summary-expander-toggle-icon', 10000)
			.getAttribute('.cart-summary-container .cart-summary-expander-head-toggle','aria-expanded', function(err, data)
			{
				if(data != 'true')
				{
					client
						.click('.cart-summary-container .cart-summary-expander-toggle-icon')
						.waitForAjax()
					;
				}
				client
					.call(cb)
			})
		;
	}

,	fillPromoCode: function(params, cb)
	{
		this
			.click('[data-target="#promo-code-container"] .cart-summary-expander-toggle-icon')
			.waitForAjax()
			.waitFor('#promocode', 3000)
			.setValue('#promocode', params.code)

			.click('button.cart-promocode-form-summary-button-apply-promocode')
			.waitForAjax()
		;

		this.call(cb);
	}

,	checkThatPromoCodeIsApplied: function(params, cb)
    {
    	var promo_regex = new RegExp(params.code, 'i');

		this
			.getText('.cart-summary-promocode-code', function(err, text)
	        {
	            expect(text).toMatch(promo_regex);
	        })
	        .call(cb)
		;
    }

,	checkThatDiscountPercentageIsCorrect: function(params, cb)
	{
		var percentage_regex = new RegExp(params.percentage);

		this
			.getText('.cart-summary-grid-right', function(err, text)
	        {
	        	var percent_regex = /(\d+\.\d+)\s*\%/;
	        	expect(text).toMatch(percent_regex);

	        	var parts = text.match(/(\d+\.\d+)\s*\%/)

	        	if (parts)
	        	{
	        		var found_percent = parseFloat(parts[1]);
	        		var expected_percent = parseFloat(params.percentage);

	        		expect(found_percent).toEqual(expected_percent);
	        	}


	        })
	        .call(cb)
		;
    }

,	checkThatAppliedDiscountIsCorrect: function(params, cb)
	{
		var calculated_discount = null;

		this
			.getText('.cart-summary-amount-subtotal', function(err, subtotal)
			{
				var discount_percentage = parseFloat(params.percentage);

				var subtotal = subtotal.match(/([\d,]+)/);
				subtotal = subtotal[1].replace(',','');

				if(subtotal)
				{
					subtotal = parseFloat(subtotal);
					calculated_discount = subtotal / discount_percentage;
				}

			})

			.getText('.cart-summary-amount-discount-total', function(err, total_discount)
			{
				var total_discount = total_discount.match(/([\d,]+)/);
				total_discount = total_discount[1].replace(',','');

				if(total_discount)
				{
					total_discount = parseFloat(total_discount);

					expect(calculated_discount).toEqual(total_discount);
				}

			})

			.call(cb)
		;

	}

,	clickRemovePromoCode: function(cb)
    {
    	this
    		.waitForExist('[data-action=remove-promocode]', 3000)
    		.click('[data-action=remove-promocode]')
    		.waitForAjax()
    		.call(cb)
    	;
    }

,	checkPromoCodeIsRemoved: function(cb)
    {
    	this
    		.isExisting('.cart-summary-promocode-code', function(err, text)
	        {
	            expect(text).toBe(false, "PromoCode element exists");
	        })
    		.call(cb)
    	;
    }

,	checkPromoCodeIsNotApplied: function(cb)
    {
    	this
    		.isExisting('.cart-summary-promocode-code', function(err, text)
	        {
	            expect(text).toBe(false, "PromoCode was applied");
	        })
    		.call(cb)
    	;
    }

,	checkInvalidPromoCodeTextIsDisplayed: function(cb)
    {
    	this
    		.getText('.global-views-message-error', function(err, text)
	        {
	            expect(text).toMatch('Coupon code is invalid or unrecognized');
	        })
    		.call(cb)
    	;
    }

,	checkExpiredPromoCodeTextIsDisplayed: function(cb)
    {
    	this
    		.getText('.global-views-message-error', function(err, text)
	        {
	            expect(text).toMatch('This coupon code has expired or is invalid');
	        })
    		.call(cb)
    	;
    }

});