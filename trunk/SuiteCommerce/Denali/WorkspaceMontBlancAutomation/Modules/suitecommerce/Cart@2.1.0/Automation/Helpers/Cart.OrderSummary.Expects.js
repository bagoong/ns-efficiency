defineHelper('Cart.OrderSummary.Expects',
{

	checkThatOrderSummaryExists: function(cb)
	{
		this
			.waitForExist('.cart-summary', 3000)
			.isVisible('.cart-summary', function(err, is_visible)
			{
				expect(is_visible).toBe(true, 'Expected Cart Summary to be present')
			})
			.call(cb)
		;
	}

, checkThatEstimateDropdownExists: function(cb)
	{
		this
			.waitForExist('.cart-summary-expander-container', 3000)
			.isExisting('.cart-summary-expander-container', function(err, is_existing)
			{
				expect(is_existing).toBe(true, 'Expected Estimate Dropdown to be present')
			})
			.waitForExist('.cart-summary-zip-code', 3000)
			.isExisting('.cart-summary-zip-code', function(err, is_existing)
			{
				expect(is_existing).toBe(true, 'Expected Input Zip Code to be present')
			})
			.waitForExist('.cart-summary-button-estimate', 3000)
			.isExisting('.cart-summary-button-estimate', function(err, is_existing)
			{
				expect(is_existing).toBe(true, 'Expected Estimate Button to be present')
			})

			.call(cb)
		;
	}

,	checkThatEstimatedTotalIsNotVisible: function(cb)
	{
		this
			.Cart.OrderSummary.Expects.checkThatOrderSummaryExists()
			.isVisible('.cart-summary-total', function(err, is_visible)
			{
				expect(is_visible).not.toBe(true, 'Expected Cart Summary Estimated Total to not exist')
			})
			.call(cb)
		;
	}


,	checkItemData: function (data, item, cb)
	{
		data.name && expect(data.name).toBeSameText(item.name);
		data.sku && expect(data.sku).toEqual(item.sku);
		data.imageUrl && expect(unescape(item.imageUrl)).toContain(data.imageUrl);
		data.itemPrice && expect(data.itemPrice).toBeSameMoney(item.itemPrice);
		data.itemTotal && expect(data.itemTotal).toBeSameMoney(item.itemTotal);

		if (data.options)
		{
			expect(data.options.length).toEqual(Object.keys(item.options).length);

			data.options.forEach(function(value, index)
			{
				var option = item.options[value.label];

				expect(option.label).toEqual(value.label + ':');
				expect(option.value).toEqual(value.value);
			});
		}

		this
			.call(cb)
		;
	}

,	isItemCorrectlyAdded: function(item, cb)
	{
		this
			.Cart.getItemDataTypeBySku(item.Sku, function(err, data){
	            var name_matcher = new RegExp('^' + data.Name + '$', 'i')
	            expect(item.Name).toMatch(name_matcher);
	            expect(item.Sku).toEqual(data.Sku);
	            expect(item.ItemPrice).toEqual(data.ItemPrice);
	            expect(item.ItemTotal).toEqual(data.ItemTotal);
	            expect(item.Quantity).toEqual(data.Quantity);
        	})
        	.call(cb)
        ;
	}


});
