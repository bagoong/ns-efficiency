/* jshint node: true */
/*
@module gulp.automation-selectors

Creates the file automation-selectors.json in the Distribution folder. This file contains the contract of HTML class names
for automating the Reference Applications by Quality assurance.

Also it contains a verification routine that checks that all declared CSS selectors of automation.json files exists somewhere in the application.
This verification is very heuristic and not 100% infallible.

##Usage

	gulp automation-selectors

	gulp automation-selectors-verify

*/
'use strict';

var gulp = require('gulp')
,	map = require('map-stream')
,	concat = require('gulp-concat')
,	_ = require('underscore')
,	path = require('path')
,	fs = require('fs')
,	package_manager = require('../package-manager')
,	glob = require('glob').sync
,	jsonlint = require('gulp-json-lint');



gulp.task('automation-selectors-validate-json', function()
{
	return gulp.src(package_manager.getGlobsFor('automation-selectors'))
		.pipe(jsonlint())
		.pipe(jsonlint.report('verbose'))
		.pipe(jsonlint.report(function(lint, file)
		{
			process.exit(1);
		}))
	;
});



gulp.task('automation-selectors', ['automation-selectors-validate-json'], function()
{
	var selectors = {};

	return gulp.src(package_manager.getGlobsFor('automation-selectors'))
		.pipe(package_manager.handleOverrides())
		.pipe(map(function(file, cb)
		{
			var selectors_content = JSON.parse(file.contents.toString());			
			selectors[package_manager.getModuleForPath(file.path).moduleName] = selectors_content;
			cb(null, file);
		}))
		.pipe(concat('selectors.json'))
		.pipe(map(function(file, cb)
		{
			file.contents = new Buffer(JSON.stringify(selectors, '\t', 4));
			cb(null, file);
		}))
		.pipe(gulp.dest('automation/'));
});


gulp.task('automation-selectors-verify', function()
{
	var auto_selectors = package_manager.getGlobsFor('automation-selectors');

	var templates = package_manager.getGlobsFor('templates');
	var all_templates_content = '';
	_(templates).each(function(template_glob)
	{
		_(glob(template_glob)).each(function(file)
		{
			all_templates_content += fs.readFileSync(file);
		});
	});

	var javascripts = package_manager.getGlobsFor('javascript');
	var all_javascript_content = '';
	_(javascripts).each(function(javascript_glob)
	{
		_(glob(javascript_glob)).each(function(file)
		{
			all_javascript_content += fs.readFileSync(file);
		});
	});

	var errors = {};
	_(auto_selectors).each(function(selectors_file)
	{
		var module_folder = path.normalize(path.join(path.dirname(selectors_file), '..'));
		// var templates_folder = path.join(module_folder, 'Templates');

		var selectors;
		try
		{
			selectors = JSON.parse(fs.readFileSync(selectors_file).toString()).selectors;
		}catch(ex){}

		_(selectors).each(function(selector)
		{
			selector = selector.replace(/\[/g, ' ['); //contemplate case .something[attr='foo']
			var fragments = selector.split(/\s+/);
			_(fragments).each(function(fragment)
			{
				testSelector(fragment, all_templates_content, all_javascript_content, errors, module_folder);
			});
		});
	});
	console.log('SELECTORS NOT FOUND: #' + _(errors).keys().length/*,  _(errors).keys()*/);
	_(errors).each(function(e, name)
	{
		console.log(name + '\t\t\t\t' + e.module + '. Error cause: ' + e.cause);
	});
});


function testSelector(selector, template_content, javascript_content, errors, module_folder)
{
	var regex;

	if(selector.indexOf('.')===0 || selector.indexOf('#')===0)
	{
		var selector_attr = selector.indexOf('.')===0 ? 'class' : 'id';
		selector = selector.indexOf(':') !== -1 ? selector.substring(0, selector.indexOf(':')) : selector; //.span3:nth-child(2)>:nth-child(3)>:
		selector = selector.indexOf('>') !== -1 ? selector.substring(0, selector.indexOf('>')) : selector; //.pagination-links>li>a

		// if wildcard %s is found we check for the prefix only e.g. .foo-%s-bar - then we only check for .foo
		selector = selector.indexOf('%s') !== -1 ? selector.substring(0, selector.indexOf('%s')) : selector;

		//it is a class - we look in all the templates the expressions class="" and also in the javascript because Backbone.View declare html classes using attributes: {class:}
		var c = selector.substring(1, selector.length);
		var cr = c.replace(/\-/g, '\\-');
		// console.log(cr)
		regex = new RegExp(selector_attr + '="[^"]*' + cr + '[^"]*');
		var match_in_templates = template_content.match(regex);

		regex = new RegExp('\'?' + selector_attr + '\'?\\s*:\\s*\'' + c);
		var match_in_js = javascript_content.match(regex);
		if(!match_in_templates && !match_in_js)
		{
			errors[selector] = {selector: selector, module: module_folder};
		}
	}
	else if (selector.indexOf('[') === 0)
	{
		regex = /\[?([a-z\d_\-]+)=["']([a-z\d_\-%]+)["']\]?/i;
		var result = regex.exec(selector);
		var isSimple = false;

		if (!result || result.length < 3)
		{
			regex = /\[?([a-z\d_\-]+)\]?/i;
			result = regex.exec(selector);
			if (!result || result.length < 2)
			{
				errors[selector] = {selector: selector, module: module_folder, cause: 'cannot parse selector'};
				return;
			}
			isSimple = true;
		}

		var substring = (isSimple || result[2].indexOf('%s') !== -1) ? (result[1]) : (result[1] + '="' + result[2] + '"'); //contemplate simple and values with %s - like [checked] or [a="%s"]

		// console.log(selector, substring)
		if (template_content.indexOf(substring) === -1 && javascript_content.indexOf(result[1]) === -1)
		{
			errors[selector] = {selector: selector, module: module_folder, cause: 'substring ' + substring + ' dont match'};
		}
	}

}

