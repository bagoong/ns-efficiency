/* jshint node: true */
/*
@module gulp.automation

Runs the selected or all jasmine tests present in the Automation/Tests directory from each SCA Module.

##Usage

	gulp automation

The simple form of the command runs all the tests found in the Modules directory, if you want to load
a specific file or module, then use the --module or --file arguments respectively:

	gulp automation --module CheckoutApplication
	gulp automation --file CheckoutAsGuest.js

Or you can combine them:
	gulp automation-tests --module CheckoutApplication --file CheckoutAsGuest.js

Aditionally, if you have your webdriver server running in another machine you can pass the host and port like this:

	gulp automation --file CheckoutAsGuest.js --host 172.21.194.64 --port 9088

##Declaring automation tests in ns.package.json

The test files are those referenced by the property gulp.automation-tests in module's ns.package.json file. Example:

	{
		'gulp': {
			...
		,	'automation-tests': [
				'Automation/Tests/*.js'
			]
			...
		}
	}
*/

'use strict';

var gulp = require('gulp')
,	args = require('yargs').argv
,	glob = require("glob").sync
,	del = require('del')
,	map  = require('map-stream')
,	packageManager = require('../package-manager')
,	fs = require('fs')
,	path = require('path')
,	_ = require('underscore')
,	minimatch = require("minimatch")
,	inquirer = require("inquirer")
,	async = require('async')
;

var CURRENT_TEST_SUITE = null;


var initAutomationPaths = function()
{
	var automationPaths = [
		'automation'
	,	'automation/execution'
	,	'automation/suites'
	,	'automation/results'
	,	'automation/websites'
	,	'automation/screenshots'
	];

	automationPaths.forEach(function(dirPath)
	{
		try
		{
			fs.mkdirSync(dirPath);
		}
		catch (err) {}
	});
}


var getAutomationCorePath = function(relPath)
{
	var automationCoreModule = _.findWhere(
		packageManager.contents, { moduleName: 'Automation.Core' }
	);

	if (!automationCoreModule)
	{
		console.log("Unable to find 'Automation.Core' Module.");
		process.exit(1);
	}

	var automationCorePath = path.join(automationCoreModule.absoluteBaseDir, 'Automation');

	if (relPath)
	{
		automationCorePath = path.join(automationCorePath, relPath);
	}

	return automationCorePath;
}


var isProcessRunning = function(pid)
{
	try
	{
		return process.kill(pid,0);
	}
	catch (e)
	{
		return e.code === 'EPERM';
	}
}

var execJasmine = function(specPath, cb)
{
	var nodePath = process.execPath;

	var jasmineRunnerPath = getAutomationCorePath('Bin/jasmine.js');

	var jasmineScript = glob(jasmineRunnerPath);

	if (_.isEmpty(jasmineScript))
{
		console.log("\n\n====================================================");
		console.log("\nERROR! Unable to find jasmine.js with the following search pattern:");
		console.log("\n" + jasmineRunnerPath)
		console.log("\n====================================================");
		console.log("\n");
		return cb();
	}

	var commandArgs = [
		jasmineScript
	,	specPath
	];

	var automationArgIndex = process.argv.indexOf('automation');

	if (automationArgIndex !== -1)
	{
		var extraArgs = process.argv.slice(automationArgIndex);

		extraArgs = extraArgs.concat([
			"--execution-pid", process.pid
		]);

		commandArgs = commandArgs.concat(extraArgs);
	}

	var nodeModulesPath = path.join(process.cwd(), 'node_modules');

	var Subprocess = require(
		getAutomationCorePath('Lib/Subprocess')
	);

	return Subprocess(
		nodePath
	,	commandArgs
	,	{
			env: {
				NODE_PATH: nodeModulesPath
			}
	}
	,	cb
	);
};


var getConfiguration = function()
		{
	var configuration = require(
		getAutomationCorePath('Preconditions/Configuration.Module').replace(
			"Automation.Core", "Automation.Precondition"
		)
	);

	configuration.initConfigurationPaths();
	return configuration;
}


var getTestSuite = function(suiteName)
{
	var configuration = getConfiguration();
	var configurationPath = configuration.configurationFilePath;

	var TestSuiteSearch = require(getAutomationCorePath('Lib/TestSuiteSearch'));

	var testSuiteSearch = new TestSuiteSearch([
		configurationPath
	,	'automation/suites/**/*.json'
	]);

	var suiteEntry = testSuiteSearch.find(suiteName + '');

	if (suiteEntry)
	{
		var srcDirs = packageManager.getGlobsFor('automation-tests');

		suiteEntry.testCases = testSuiteSearch.getTestCasesFromSuite(
			suiteName, srcDirs
		);

		return suiteEntry;
	}
	else
	{
		console.log("");

		if (typeof suiteName === 'string')
		{
			console.log("  Suite of name '" + suiteName + "' doesn't exists in the 'suites'");
			console.log("  entry of the configuration file at:")
			console.log("");
			console.log("  " + configurationPath);
			console.log("");
		}
		else
		{
			console.log("  Please specify a suite name in the --suite option.\n")
		}

		if (!_.isEmpty(testSuiteSearch.suites))
		{
			console.log("  Available test suites:\n")

			Object.keys(testSuiteSearch.suites).forEach(function(keyName)
			{
				console.log("  " + keyName);
			});

			console.log("");
		}
		else
		{
			console.log("  No test suites found.\n")
		}

		process.exit(1);
	}
}


var getTests = function()
{
	var tests = [];

	var fileFilters = [];

	if (args.suite || args.suites)
	{
		var suiteName = args.suite || args.suites;
		var testSuite = getTestSuite(suiteName);

		CURRENT_TEST_SUITE = testSuite;
		tests = testSuite.testCases;
	}
	else
	{
		var pattern = null;

		if (args.module && args.file)
		{
			pattern = args.module + '/**/' + args.file;
		}
		else if (args.module)
		{
			pattern = args.module + '/*';
		}
		else if (args.file)
		{
			pattern = '*/' + args.file;
		}
		else if (args.name)
{
			pattern = args.name
		}

		if (pattern)
		{
			fileFilters.push({
				pattern: pattern, against: 'location'
	});
		}

		var TestCaseSearch = require(getAutomationCorePath('Lib/TestCaseSearch'));

		var srcDirs = packageManager.getGlobsFor('automation-tests');

		var testCaseSearch = new TestCaseSearch(
			srcDirs
		,	fileFilters
		);

		tests = testCaseSearch.getTestCases();

		CURRENT_TEST_SUITE = {
			sourceGlobs: _.pluck(fileFilters, 'pattern')
		,	testCases: tests
		}
	}
	
	return tests;
}



var getGlobsForJasmineHelpers = function()
{
	return packageManager.getGlobsFor('automation-jasmine-helpers');
}


var getGlobsForAutomationHelpers = function()
{
	return packageManager.getGlobsFor('automation-helpers');
}


var dumpJasmineSpec = function(params, dstPath)
{
	var specData = {
		'spec_dir': ''
	,	'spec_files': []
	,	'helpers': []
	};

	if (params.testFiles)
	{
		specData.spec_files = _.uniq(params.testFiles, true);
	}

	var helperParams = ['jasmineHelpers', 'automationHelpers', 'helpers'];

	helperParams.forEach(function(paramName)
	{
		var paramData = params[paramName];

		if (Array.isArray(paramData))
		{
			specData.helpers = specData.helpers.concat(paramData);
		}
	});
	
	specData = _.extend(specData,
		_.omit(
			params
		,	_.union(['testFiles'], helperParams, Object.keys(specData))
		)
	);

	fs.writeFileSync(dstPath, JSON.stringify(specData, null, 4));
}


var cleanFiles = function ()
{
	var files = _.toArray(arguments);
	
	if (args['keep-files'])
	{
		return false;
	}

	del.sync(files);
}


var cleanOldSpecs = function ()
{
	glob('automation/execution/spec.*.json').forEach(function(filePath)
	{
		var pidMatch = filePath.match(/spec\.(\d+)\.json$/);

		if (pidMatch && !isProcessRunning(pidMatch[1]))
		{
			del.sync(filePath);

			del.sync(
				filePath.replace('/spec.' + pidMatch[1] + '.json', '/status.' + pidMatch[1] + '.json')
			);
		}
	})
}


var dumpPreconditionsStatus = function(entries)
{
	var EOL = require('os').EOL;
	var reportPath = 'ported_to_preconditions.txt';

	var reportContents = '';

	var portedTestsLocations = [];

	var counters = {
		ported: 0
	,	pending: 0
}

	_.each(entries, function(entry)
	{
		var testFileContents = fs.readFileSync(entry.filePath) + '';

		var portedStatus =  /Preconditions\.start/i.test(testFileContents) ? 'ported' : 'pending';
		counters[portedStatus]++;

		if (portedStatus === 'ported')
{
			portedTestsLocations.push(entry.location);
		}

		var line = [
			entry.module, entry.location, portedStatus
		]

		reportContents += line.join("\t") + EOL;
		//process.exit(1);
	});

	console.log("")
	console.log("Ported files : " + counters.ported);
	console.log("Pending files: " + counters.pending);
	console.log("Total files  : " + (counters.ported + counters.pending));
	console.log("");

	fs.writeFileSync(reportPath, reportContents);

	fs.writeFileSync(
		'automation/suites/automation-portable.json'
	,	JSON.stringify({
			'tests': portedTestsLocations
		}, null, 4)
	);

	console.log("Finished report to " + reportPath)
}


var showList = function(elements, elementName)
{
	if (!elements.length)
	{
		console.log("\n  No " + elementName + " found\n");
	}
	else
	{
		console.log("\n  Found " + elementName + ":");

		_.each(elements, function(element)
		{
			console.log("\n  " + element);
		});
		
		console.log('');
	}
}


gulp.task('automation-webdriver-setup', function(cb)
{
	var commandPrefix = (process.platform === 'linux')? 'sudo ' : '';

	var exec = require('child_process').exec;
	var execSync = require('child_process').execSync;
	var execSyncDefaults = {
		stdio: [
			// redirect all pipes to stdio
			0, 1, 2  
		]
	}

	var child = exec('webdriver-manager', function(error, stdout, stderr)
	{
		var isInstalled = error && /--ie|--chrome|start/i.test(error);

		if (!isInstalled)
		{
			console.log("\nWebdriver Manager is not installed.")
			console.log("\nInstalling webdriver-manager...")

			execSync(commandPrefix + 'npm install -g webdriver-manager', execSyncDefaults);
			execSync(commandPrefix + 'webdriver-manager update', execSyncDefaults);

			if (process.platform === 'win32')
			{
				execSync(commandPrefix + 'webdriver-manager update --ie', execSyncDefaults);
			}
		}

		cb();
	});
			})


gulp.task('automation', ['automation-selectors'], function(automationTaskDone)
{
	initAutomationPaths();
	cleanOldSpecs();

	// WE APPEND THE PROCESS.PID TO SPEC NAME TO PREVENT
	// MULTIPLE GENERATED SPECS AT THE SAME TIME TO OVERWRITE THEMSELVES
	var specPath = 'automation/execution/spec.' + process.pid + '.json';

	if (args['dump-spec'] && typeof args['dump-spec'] === 'string')
	{
		specPath = path.resolve(args['dump-spec']);
	}

	var statusPath = 'automation/execution/status.' + process.pid + '.json';

	var tests = getTests();

	var testFiles = _.pluck(tests, 'filePath');


	if (args["preconditions-status"])
	{
		dumpPreconditionsStatus(tests);
		process.exit(1)
	}
	else if (args['list-files'])
	{
		showList(testFiles, 'test files');
		process.exit(1);
	}
	else if (args['list-tests'])
	{
		showList(_.pluck(tests, 'location'), 'tests');
		process.exit(1);
	}

	dumpJasmineSpec(
		{
			testFiles: testFiles
		,	jasmineHelpers: getGlobsForJasmineHelpers()
		,	automationHelpers: getGlobsForAutomationHelpers()
		,	suite: CURRENT_TEST_SUITE
		}
	,	specPath
	);

	if (args['dump-spec'])
	{
		console.log(specPath);
		process.exit(0);
	}

	var executionsLimit = args.retries || args.retry || args.loop || args.repeat || 1;
	var executionsCount = 0;

	async.whilst(
		function() {
			return executionsCount < executionsLimit;
		}
	
	,	function(loopCb)
		{
			var jasmineProcess = execJasmine(specPath, function()
			{
				executionsCount++;
				loopCb();
			});
		}

	,	function(err)
		{
			cleanFiles(
				specPath, statusPath
			);

			automationTaskDone();
		}
	);

});

