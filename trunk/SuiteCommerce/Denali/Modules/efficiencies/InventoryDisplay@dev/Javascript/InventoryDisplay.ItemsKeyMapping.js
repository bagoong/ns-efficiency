define('InventoryDisplay.ItemsKeyMapping', [
    'InventoryDisplay.Locations',
    'SC.Configuration',
    'ArrayBigIntersect',
    'underscore'
], function BOPISItemsKeyMapping(
    Locations,
    Configuration,
    arrayBigIntersect,
    _
) {
    'use strict';

    var safeParsingJSON = function safeParsingJSON(jsonString, defaultReturn) {
        try {
            return JSON.parse(jsonString);
        } catch (e) {
            return defaultReturn;
        }
    };

    Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};

    _.extend(Configuration.itemKeyMapping, {
        _allshippinglocations: function _allshippinglocations(item) {
            return safeParsingJSON(item.get('custitem_bopis_ships_from'), []);
        },
        _shippinglocations: function _shippinglocations(item) {
            return arrayBigIntersect(Locations.getInstance().get('all'), item.get('_allshippinglocations'));
        },
        _pickuplocations: function _pickuplocations(item) {
            return arrayBigIntersect(Locations.getInstance().get('stores'), item.get('_allpickuplocations'));
        },
        _allpickuplocations: function _allpickuplocations(item) {
            return safeParsingJSON(item.get('custitem_bopis_pick_from'), []);
        },

        _ispickable: function _ispickable(item) {
            return (item.get('_isfulfillable') && item.get('_pickuplocations').length > 0) || false;
        },
        _isshippable: function _isshipable(item) {
            return (item.get('_isfulfillable') && item.get('_shippinglocations').length > 0) || false;
        },
        _isonlyshippable: function _isonlyshippable(item) {
            return !item.get('_ispickable') && item.get('_isshippable');
        },
        _isonlypickable: function _isonlypickable(item) {
            return item.get('_ispicakble') && !item.get('_isshippable');
        }
    });
});