define('InventoryDisplay.Locations', [
    'Backbone',
    'Singleton',
    'underscore'
], function BOPISLocations(
    Backbone,
    Singleton,
    _
) {
    'use strict';

    return Backbone.Model.extend({

    }, _.extend({
        bootstrap: function bootstrap(locations) {
            var instance = this.getInstance();
            instance.set('warehouses', locations.warehouses);
            instance.set('stores', locations.stores);
            instance.set('all', _.union(locations.stores, locations.warehouses));
            return instance;
        }
    }, Singleton));
});