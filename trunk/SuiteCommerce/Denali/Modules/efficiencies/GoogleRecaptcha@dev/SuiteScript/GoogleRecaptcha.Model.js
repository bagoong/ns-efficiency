define('GoogleRecaptcha.Model', [
    'SC.Model',
    'GoogleRecaptcha.Configuration',
    'Application',
    'underscore'
], function ProductPreviewGoogleRecaptchaModel(
    SCModel,
    GoogleRecaptchaConfiguration,
    Application,
    _
) {
    'use strict';

    var extendGoogleRecaptcha;
    var recaptchaError;
    var resp;
    var result;

    extendGoogleRecaptcha =  _.extend(SCModel, {
        name: 'googlerecaptchamodel',
        validate: function validate(response) {
            recaptchaError = {
                status: 400,
                code: 'ERR_RECAPTCHA_INVALID',
                message: 'ReCaptcha is invalid'
            };

            resp = nlapiRequestURL(GoogleRecaptchaConfiguration.googleRecaptchaWidget.verifyUrl, {
                secret: GoogleRecaptchaConfiguration.googleRecaptchaWidget.serverkey,
                response: response
            });

            result = JSON.parse(resp.getBody());

            if (result.success !== true) {
                throw recaptchaError;
            }
        }
    });

    return extendGoogleRecaptcha;
});