# Store Locator
The Store Locator allows a shopper to find the nearest stores to their current location or a preferred location and to view them on a map. The shopper can view store details such as store address and opening hours as well as view directions to the store.

## Documentation
https://confluence.corp.netsuite.com/x/BXjnAg