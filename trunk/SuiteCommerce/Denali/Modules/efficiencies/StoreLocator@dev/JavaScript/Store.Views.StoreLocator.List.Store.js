define('Store.Views.StoreLocator.List.Store', [
    'Backbone',
    'StoreLocator.Configuration',
    'storelocator_list_store.tpl',
    'Utils'
], function StoreViewsStoreLocatorListStore(
    Backbone, Configuration, Template) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(data) {
            this.model  = data.model;
            this.application  = data.application;
            this.isInStoreStock = data.isInStoreStock;
            this.isInMyStore = data.isInMyStore;
        },

        getContext: function getContext() {
            var showStoreLink = (this.isInStoreStock || this.isInMyStore) ? false :
                                Configuration.storePage && this.model.get('urlcomponent');
            return {
                model: this.model,
                isInStoreStock: this.isInStoreStock,
                showStoreLink: showStoreLink,
                storeMarker: Configuration.storeMarker,
                configuration: Configuration,
                geolocation: Configuration.storeMarker.icon ||
                            'http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless.png'
            };
        }
    });
});