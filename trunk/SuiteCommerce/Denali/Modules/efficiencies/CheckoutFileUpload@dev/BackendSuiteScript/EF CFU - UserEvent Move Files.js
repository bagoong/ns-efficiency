define('EF CFU - UserEvent Move Files', [
    'Application',
    'CheckoutFileUpload.Configuration',
    'underscore'
], function EFCFUUserEventMoveFiles(
    Application,
    Configuration,
    _
) {
    'use strict';

    var main = function main() {
        var record;
        var executionContext = nlapiGetContext().getExecutionContext();
        if (executionContext.toString() !== 'webstore') {
            return true;
        }

        record = nlapiGetNewRecord();

        _.each(Configuration.fields, function eachField(field) {
            var fileId = record.getFieldValue(field.bodyFields.file);
            var newFileName;
            var file;
            var oldName;
            var fileType;

            if (fileId) {
                try {
                    newFileName = 'SO ' + record.getId() + ' ' + field.name;

                    file = nlapiLoadFile(fileId);

                    oldName = file.getName();
                    fileType = oldName.substring(oldName.lastIndexOf('.'), oldName.length);

                    file.setFolder(field.folderId);
                    file.setName(newFileName + fileType);

                    nlapiSubmitFile(file);
                    nlapiAttachRecord('file', fileId, 'salesorder', record.getId());
                } catch (e) {
                    nlapiLogExecution('ERROR', 'Error Moving File', e);
                }
            }
        });
        return true;
    };

    return {
        main: main
    };
});

