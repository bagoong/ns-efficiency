# Checkout File Upload
The module will allow the web site customers to upload a file during the Checkout process, and get the file associated to the Sales Order.

## Documentation
https://confluence.corp.netsuite.com/x/8sr8Ag
