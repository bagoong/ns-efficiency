define('Facets.FacetedNavigationItemCategory.View', [
    'facets_faceted_navigation_item_category.tpl',
    'Backbone',
    'Backbone.CompositeView',
    'underscore'
], function FacetsFacetedNavigationItemCategoryViewConstructor(
    facetsFacetedNavigationItemTpl,
    Backbone,
    BackboneCompositeView,
    _
) {
    'use strict';

    // @class Facets.FacetedNavigationItem.View @extends Backbone.View
    var FacetsFacetedNavigationItemCategoryView = Backbone.View.extend({
        template: facetsFacetedNavigationItemTpl,
        initialize: function initialize() {
            BackboneCompositeView.add(this);
            this.facetId = this.model.get('url') || this.model.get('id');
            this.facet_config = this.options.translator.getFacetConfig(this.facetId);

            if (!this.model.get('level')) {
                this.model.set('level', 1);
            }
        },
        childViews: {
            'Facets.HierarchicalFacetedNavigationItem.Item': function FacetsHierarchicalFacetedNavigationItemItem(options) {
                var originalValue = _.isArray(this.model.get('values')) ? this.model.get('values') : [this.model.get('values')];
                var childValues = _.findWhere(originalValue, {id: options.facetValue}).values;
                var constructorOptions = {
                    model: new Backbone.Model({
                        url: this.model.get('url'),
                        id: this.model.get('id'),
                        values: childValues,
                        level: this.model.get('level') + 1
                    }),
                    translator: this.options.translator
                };


                return new FacetsFacetedNavigationItemCategoryView(constructorOptions);
            }
        },
        // @method getContext @return {Facets.FacetedNavigationItem.View.Context}
        getContext: function getContext() {
            var facetId = this.facetId;
            var translator = this.options.translator;
            var facetConfig = this.facet_config;
            var values = [];
            var maxItems;
            var self = this;
            var displayValues;
            var showFacet;
            var context;
            // fixes the selected items
            var selectedValues = this.options.translator.getFacetValue(facetId) || [];
            var originalValues = _.isArray(this.model.get('values')) ? this.model.get('values') : [this.model.get('values')];

            selectedValues = _.isArray(selectedValues) ? selectedValues : [selectedValues];

            // Prepares the values for display


            _.each(originalValues, function each(value) {
                if (value.url !== '') {
                    value.isActive = _.contains(selectedValues, value.url);
                    value.link = translator.cloneForFacetId(facetId, value.url).getUrl();
                    value.displayName = value.label || value.url || _('(none)').translate();

                    if (value.values) {
                        value.hasChildren = true;
                        _.each(value.values, function eachValue(childValues) {
                            if (_.contains(selectedValues, childValues.url)) {
                                value.isParentOfActive = true;
                                value.displayChildren = true;
                            }
                        });
                    }
                    value.isBranchActive = (value.isActive || value.isParentOfActive);
                    value.showChildren = value.isBranchActive && value.hasChildren;

                    value.level = self.model.get('level');
                    values.push(value);
                }
            });

            maxItems = facetConfig.max || values.length;
            displayValues = _.first(values, maxItems);

            showFacet = !!values.length;

            facetConfig.uncollapsible = false;

            // @class Facets.FacetedNavigationItem.View.Context
            context = {
                // @property {String} htmlId
                htmlId: _.uniqueId('facetCategory_'),
                // @property {String} facetId
                facetId: facetId,
                // @property {Boolean} showFacet
                showFacet: showFacet,
                // @property {Boolean} showHeading
                showHeading: this.model.get('level') === 1 &&
                    (_.isBoolean(facetConfig.showHeading) ?
                        facetConfig.showHeading :
                        true
                    ),
                // @property {Boolean} isUncollapsible
                isUncollapsible: !!facetConfig.uncollapsible,
                // @property {Boolean} isCollapsed
                isCollapsed: !this.facet_config.uncollapsible && this.facet_config.collapsed,
                // @property {String} facetDisplayName
                facetDisplayName: facetConfig.name || facetId,
                // @property {Array<Object>} values
                values: values,
                // @property {Array<Object>} displayValues
                displayValues: displayValues
            };

            // @class Facets.FacetedNavigationItem.View
            return context;
        }
    });
    return FacetsFacetedNavigationItemCategoryView;
});