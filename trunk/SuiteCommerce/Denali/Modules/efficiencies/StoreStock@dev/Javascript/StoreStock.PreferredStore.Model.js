/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.PreferredStore.Model', [
    'Backbone.CachedModel',
    'Singleton',
    'jQuery',
    'underscore',
    'Utils'
], function StoreStockPreferredStoreModel(
    BackboneCachedModel,
    Singleton,
    jQuery,
    _,
    Utils
) {
    'use strict';

    return BackboneCachedModel.extend({
        url: _.getAbsoluteUrl('services/StoreStock.PreferredStore.Service.ss')
    }, Singleton);
});