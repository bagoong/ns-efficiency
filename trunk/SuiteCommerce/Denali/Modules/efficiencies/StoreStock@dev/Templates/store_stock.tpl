<section class="store-stock-container">
    <div role="tabpanel">
        <ul class="tabpanel-head" role="tablist">
            <li role="presentation" class="{{#if activeTabShipping}}active{{/if}}"  data-action="store-stock-tabheader-click">
                <a class="tabpanel-head-tab" data-target="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">
                    {{ translate 'Shipping' }}
                </a>
            </li>
            <li role="presentation" class="{{#if activeTabPickup}}active{{/if}}" data-action="store-stock-tabheader-click">
                <a class="tabpanel-head-tab" data-target="#pickup" aria-controls="pickup" role="tab" data-toggle="tab">
                    {{ translate 'Pickup' }}
                </a>
            </li>
        </ul>

        <div class="tabpanel-body">
            <div role="tabpanel" class="tabpanel-tab {{#if activeTabShipping}}active{{/if}}" id="shipping">
                <div class="store-stock-total-view">
                    {{#if isDisplayAsBadge}}
                        <span class="{{ badgeClass }}">{{ translate badgeText }}</span>
                    {{else}}
                        {{ translate "$(0) items in stock" quantityAvailable }}
                    {{/if}}
                </div>
            </div>
            <div role="tabpanel" class="tabpanel-tab {{#if activeTabPickup}}active{{/if}}" id="pickup">
                <div data-view="PickupTab.Content.View"></div>
            </div>
        </div>
</section>