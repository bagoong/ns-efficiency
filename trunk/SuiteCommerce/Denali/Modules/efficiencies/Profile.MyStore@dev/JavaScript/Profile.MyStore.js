define('Profile.MyStore', [
    'Profile.MyStore.Model',
    'Profile.MyStore.Header.View',
    'Profile.Model',
    'Backbone',
    'Tracker',
    'PluginContainer',
    'Header.View',
    'Header.Menu.View',
    'jQuery',
    'underscore',
    'Utils'
], function ProfileMyStore(
    MyStoreModel,
    ProfileMyStoreHeaderView,
    Profile,
    Backbone,
    Tracker,
    PluginContainer,
    HeaderView,
    HeaderMenuView,
    jQuery,
    _,
    Utils
) {
    'use strict';

    var myStoreAsTracker = {
        addCrossDomainParameters: function addCrossDomainParameters(url) {
            var newUrl = url;
            var profile = Profile.getInstance();
            var store =  MyStoreModel.getInstance();
            if (store && !store.isNew() && profile.get('internalid') === '0') {
                newUrl = Utils.addParamsToUrl(
                    url,
                    {myStore: store.get('internalid')}
                );
            }
            return  newUrl;
        }
    };

    return {
        mountToApp: function mountToApp(application) {

            jQuery.when(Profile.getPromise()).then(function profileResolved() {
                MyStoreModel.getInstance().set(
                    (SC &&
                    SC.ENVIRONMENT &&
                    SC.ENVIRONMENT.sessionPublished &&
                    SC.ENVIRONMENT.sessionPublished.MyStore) || null
                );
            });

            // INSTALL NAVIGATION PLUGIN
            Tracker.getInstance().trackers.push(myStoreAsTracker);

            HeaderMenuView.prototype.preRenderPlugins = HeaderView.prototype.preRenderPlugins || new PluginContainer();
            HeaderMenuView.prototype.preRenderPlugins.install({
                name: 'Header.Profile.MyStore',
                execute: function execute($el) {
                    $el
                        .find('[data-view="Header.Profile"]')
                        .after('<div class="profile-mystore-header-sidebar" data-view="Header.Profile.MyStore"></div>');
                }
            });

            HeaderMenuView.addExtraChildrenViews({
                'Header.Profile.MyStore': function wrapperFunction() {
                    return function childViewHeaderProfileMyStore() {
                        return new ProfileMyStoreHeaderView({
                            application: application,
                            model: MyStoreModel.getInstance()
                        });
                    };
                }
            });


            HeaderView.prototype.preRenderPlugins = HeaderView.prototype.preRenderPlugins || new PluginContainer();
            HeaderView.prototype.preRenderPlugins.install({
                name: 'Header.Profile.MyStore',
                execute: function execute($el) {
                    $el
                        .find('[data-view="Header.Profile"]')
                        .after('<div class="profile-mystore-header" data-view="Header.Profile.MyStore"></div>');
                }
            });

            HeaderView.addExtraChildrenViews({
                'Header.Profile.MyStore': function wrapperFunction() {
                    return function childViewHeaderProfileMyStore() {
                        return new ProfileMyStoreHeaderView({
                            application: application,
                            model: MyStoreModel.getInstance()
                        });
                    };
                }
            });

        }
    };
});