## Header Region

Use structure and classes matching the main nav to get the correct styling

### qs-header-menu

<ul class="header-menu-level1"><li><a class="header-menu-level1-anchor" href="">My Link</a></li><li><a class="header-menu-level1-anchor" href="">My Link</a></li>
</ul>

### qs-sidebar-menu (mobile equiv of header-menu)

Similar (copy structure of ul in sidebar-menu)


## Footer
qs-footer-content-nav

<ul class="footer-content-nav-list">  <li><a href="#"> Link a </a></li>  <li><a href="#"> Link b </a></li>  <li><a href="#"> Link c </a></li>  </ul>



## NOTES

SC.Configuration.js - remove navigationDummyCategories config?


Replaced company Name (in footer): SCEnvironment displayName



Seems you can have dummy content inside CMS divs, and it will be replaced by SMT content, but not if it uses the translate function (in which case the dummy content will remain and the SMT content will be added next to it)

Speak to  Sebastian Gurin about this - he seems to know stuff. i.e. is this how it's meant to work. Should I be putting dummy content there or not?


## PS DIscussion with Guillermo Villa and Leslie Real

PS will actually create the initial CMS content, so for example can add placeholder content.

PS have a quite a specific thing they want to have for the header - a hard coded expanding version with CMS - editable areas available at various levels (so clients can add images, videos etc). Built by Ignacio Correa (icorrea@netsuite.com). This can be a separate module.