define('Store.Bootstrapped.Collection', [
    'Store.Collection',
    'Singleton'
], function StoreBootstrapedCollection(
    Collection,
    Singleton
) {
    'use strict';
    var BootstrappedCollection = Collection.extend({}, Singleton);
    BootstrappedCollection.getInstance().set(
        SC && SC.ENVIRONMENT && SC.ENVIRONMENT.published && SC.ENVIRONMENT.published.Stores
    );

    return BootstrappedCollection;
});