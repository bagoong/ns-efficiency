{{#if isDeliverable}}
    <h5>{{translate 'Delivery'}}</h5>
    {{#if isShippable}}
        <i class="item-delivery-shipping"></i>
        <input type="radio"
               name="delivery-options-{{lineId}}"
               data-line="{{lineId}}"
               data-action="change-delivery-method"
               class="order-wizard-omet"
               {{#if isShippingSelected}}checked{{/if}}
               value="shipping"
               id="delivery-options-{{internalid}}"
        />
        <span>{{translate 'Ship'}}</span>
    {{/if}}

    {{#if isPickable}}
        <i class="item-delivery-pickup"></i>
        <input type="radio"
               name="delivery-options-{{lineId}}"
               data-action="change-delivery-method"
               data-line="{{lineId}}"
               class="order-wizard-omet"
               {{#if isPickupSelected}}checked{{/if}}
                value="pickup"
        id="delivery-options-{{internalid}}"
        />
        <span>{{translate 'Pick'}}</span>
    {{/if}}
{{/if}}

