define('BOPIS.OrderHistory.Model', [
    'OrderHistory.Model',
    'OrderStatusSummary.Model',
    'SearchHelper',
    'Application',
    'extendWithEvents',
    'underscore'
], function BOPISOrderHistoryModel(
    OrderHistory,
    OrderStatusSummary,
    SearchHelper,
    Application,
    extendWithEvents,
    _
) {
    'use strict';

    var FULFILLMENTCHOICE_SHIPPING = 1;
    var FULFILLMENTCHOICE_PICKUP_IN_STORE = 2;

    extendWithEvents(OrderHistory, {
        orderRecord: 'salesorder',

        orderItemLinesFilters: {fieldName: 'entity', operator: 'is', value1: nlapiGetUser()},

        getOrderStatusSummary: function getOrderStatusSummary(salesOrderIDs) {
            var results = OrderStatusSummary.get(salesOrderIDs, this.orderItemLinesFilters);

            return results;
        },

        appendStatusDetail: function setStatusDetail(orderList) {
            var orderStatus;
            var statusDetail;
            var salesOrderIDs =
                _.pluck(
                    _.filter(orderList.records, function filterOrderListRecord(line) {
                        return (!_.contains(OrderStatusSummary.statusesWithNoSummary, line.status.internalid));
                    }),
                'internalid');


            if (salesOrderIDs && salesOrderIDs.length) {
                orderStatus = this.getOrderStatusSummary(salesOrderIDs);
                if (orderStatus) {
                    _.each(orderList.records, function eachOrder(orderRecord) {
                        statusDetail = orderStatus[orderRecord.internalid] ?
                            orderStatus[orderRecord.internalid] : '';

                        if (statusDetail !== '') {
                            orderRecord.status.name = orderRecord.status.name + ' (' + statusDetail + ')';
                        }
                    });
                }
            }
        },

        getPickupBy: function getPickupBy() {
            var orderRecord = this.getTransactionRecord(this.recordType, this.recordId);
            this.result.pickup_by = orderRecord.getFieldValue('custbody_ef_bopis_contact');
            this.result.pickup_by_email = orderRecord.getFieldValue('custbody_ef_bopis_contact_email');
        },

        transformLinesGroups: function transformLinesGroups() {
            var self = this;
            _.each(this.result.lines, function eachResultLine(line) {
                if (self.result.recordtype === 'salesorder') {
                    if (line.location &&
                        parseInt(line.itemfulfillmentchoice, 10) === FULFILLMENTCHOICE_PICKUP_IN_STORE &&
                        line.isfulfillable === true) {
                        line.linegroup = 'bopis';
                    }
                }
            });
        },

        transformLineData: function transformLineData(line, ffline) {
            var itemPending = 0;
            var quantitypicked = parseInt(ffline.getValue('quantitypicked') || 0, 10);

            line.itemfulfillmentchoice = ffline.getValue('itemfulfillmentchoice');
            line.location = {
                internalid: ffline.getValue('internalid', 'location'),
                lat: ffline.getValue('latitude', 'location'),
                lon: ffline.getValue('longitude', 'location'),
                address1: ffline.getValue('address1', 'location'),
                address2: ffline.getValue('address2', 'location'),
                city: ffline.getValue('city', 'location'),
                country: ffline.getValue('country', 'location'),
                name: ffline.getValue('name', 'location'),
                openingHours: ffline.getValue('openingHours', 'location'),
                state: ffline.getValue('state', 'location'),
                zip: ffline.getValue('zip', 'location')
            };

            if (parseInt(line.itemfulfillmentchoice, 10) === FULFILLMENTCHOICE_PICKUP_IN_STORE) {
                line.quantitypicked_orig = quantitypicked;
                line.quantitypicked = quantitypicked - line.quantityfulfilled;
                line.quantitypacked = 0;

                itemPending = line.quantity - line.quantityfulfilled;
                itemPending = itemPending - line.quantitypicked;
                line.quantitybackordered = itemPending;
            }
        },

        transformResultData: function transformResultData() {
            var itemsForShippingQty = 0;
            var itemsForShippingQtyShippped = 0;

            var itemsForStorePickupQty = 0;
            var itemsForStorePickupQtyPicked = 0;
            var itemsForStorePickupQtyPickedUp = 0;

            _.each(this.result.lines, function eachLinesInResult(line) {
                switch (parseInt(line.itemfulfillmentchoice, 10)) {
                case FULFILLMENTCHOICE_SHIPPING:
                    itemsForShippingQty += line.quantity;
                    itemsForShippingQtyShippped += line.quantityfulfilled;
                    break;
                case FULFILLMENTCHOICE_PICKUP_IN_STORE:
                    itemsForStorePickupQty += line.quantity;
                    itemsForStorePickupQtyPicked += line.quantitypicked_orig;
                    itemsForStorePickupQtyPickedUp += line.quantityfulfilled;
                    break;
                default:
                    break;
                }
            });

            this.result.storepickupfulfillments = _.filter(this.result.fulfillments, function getPickedUp(fulfillments) {
                return fulfillments.status.internalid === 'pickedup';
            });

            this.result.fulfillments = _.difference(this.result.fulfillments, this.result.storepickupfulfillments);
            this.result.status.name +=
            ' (' +
                OrderStatusSummary.getOrderStatusSummaryText({
                    itemsForStorePickupQty: itemsForStorePickupQty,
                    itemsForStorePickupQtyPicked: itemsForStorePickupQtyPicked,
                    itemsForStorePickupQtyPickedUp: itemsForStorePickupQtyPickedUp,
                    itemsForShippingQty: itemsForShippingQty,
                    itemsForShippingQtyShippped: itemsForShippingQtyShippped
                }, true) +
            ')';
        },

        getFulfillments: function getFulfillments() {
            var self = this;
            var filters = [
                new nlobjSearchFilter('internalid', null, 'is', this.result.internalid),
                new nlobjSearchFilter('mainline', null, 'is', 'F'),
                new nlobjSearchFilter('shipping', null, 'is', 'F'),
                new nlobjSearchFilter('taxline', null, 'is', 'F')
            ];
            var columns = [
                new nlobjSearchColumn('line'),
                new nlobjSearchColumn('fulfillingtransaction'),
                new nlobjSearchColumn('quantitypicked'),
                new nlobjSearchColumn('quantitypacked'),
                new nlobjSearchColumn('quantityshiprecv'),
                new nlobjSearchColumn('actualshipdate'),

                new nlobjSearchColumn('quantity', 'fulfillingtransaction'),
                new nlobjSearchColumn('item', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipmethod', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipto', 'fulfillingtransaction'),
                new nlobjSearchColumn('trackingnumbers', 'fulfillingtransaction'),
                new nlobjSearchColumn('trandate', 'fulfillingtransaction'),
                new nlobjSearchColumn('status', 'fulfillingtransaction'),

                // Ship Address
                new nlobjSearchColumn('shipaddress', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipaddress1', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipaddress2', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipaddressee', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipattention', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipcity', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipcountry', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipstate', 'fulfillingtransaction'),
                new nlobjSearchColumn('shipzip', 'fulfillingtransaction'),

                // ADDED FOR BOPIS
                new nlobjSearchColumn('itemfulfillmentchoice'),
                new nlobjSearchColumn('location', 'fulfillingtransaction'),
                new nlobjSearchColumn('internalid', 'location'),
                new nlobjSearchColumn('longitude', 'location'),
                new nlobjSearchColumn('latitude', 'location'),
                new nlobjSearchColumn('address1', 'location'),
                new nlobjSearchColumn('address2', 'location'),
                new nlobjSearchColumn('country', 'location'),
                new nlobjSearchColumn('phone', 'location'),
                new nlobjSearchColumn('name', 'location'),
                new nlobjSearchColumn('city', 'location'),
                new nlobjSearchColumn('state', 'location'),
                new nlobjSearchColumn('zip', 'location'),
                new nlobjSearchColumn('custrecord_bopis_location_opening_hs', 'location')

            ];
            this.result.fulfillments = {};

            Application.getAllSearchResults('salesorder', filters, columns).forEach(function forEachResult(ffline) {
                var fulfillmentId = ffline.getValue('fulfillingtransaction');
                var lineInternalid = self.result.internalid + '_' + ffline.getValue('line');
                var line = _.findWhere(self.result.lines, {internalid: lineInternalid});

                var shipaddress;

                if (fulfillmentId) {
                    shipaddress = self.addAddress({
                        internalid: ffline.getValue('shipaddress', 'fulfillingtransaction'),
                        country: ffline.getValue('shipcountry', 'fulfillingtransaction'),
                        state: ffline.getValue('shipstate', 'fulfillingtransaction'),
                        city: ffline.getValue('shipcity', 'fulfillingtransaction'),
                        zip: ffline.getValue('shipzip', 'fulfillingtransaction'),
                        addr1: ffline.getValue('shipaddress1', 'fulfillingtransaction'),
                        addr2: ffline.getValue('shipaddress2', 'fulfillingtransaction'),
                        attention: ffline.getValue('shipattention', 'fulfillingtransaction'),
                        addressee: ffline.getValue('shipaddressee', 'fulfillingtransaction')
                    }, self.result);

                    self.result.fulfillments[fulfillmentId] = self.result.fulfillments[fulfillmentId] || {
                        internalid: fulfillmentId,
                        location: ffline.getValue('location', 'fulfillingtransaction'),
                        shipaddress: shipaddress,
                        shipmethod: self.addShippingMethod({
                            internalid: ffline.getValue('shipmethod', 'fulfillingtransaction'),
                            name: ffline.getText('shipmethod', 'fulfillingtransaction')
                        }),
                        date: ffline.getValue('actualshipdate'),
                        trackingnumbers: ffline.getValue('trackingnumbers', 'fulfillingtransaction') ?
                            ffline.getValue('trackingnumbers', 'fulfillingtransaction').split('<BR>') : null,
                        lines: [],
                        status: {
                            internalid: ffline.getValue('status', 'fulfillingtransaction'),
                            name: ffline.getText('status', 'fulfillingtransaction')
                        }
                    };

                    self.result.fulfillments[fulfillmentId].lines.push({
                        internalid: lineInternalid,
                        quantity: parseInt(ffline.getValue('quantity', 'fulfillingtransaction'), 10)
                    });
                }

                if (line) {
                    line.quantityfulfilled = parseInt(ffline.getValue('quantityshiprecv') || 0, 10);

                    line.quantitypacked =
                        parseInt(ffline.getValue('quantitypacked') || 0, 10) - line.quantityfulfilled;

                    line.quantitypicked =
                        parseInt(ffline.getValue('quantitypicked') || 0, 10) - line.quantitypacked - line.quantityfulfilled;

                    line.quantitybackordered =
                        line.quantity - line.quantityfulfilled - line.quantitypacked - line.quantitypicked;

                    // ADDED FOR BOPIS
                    self.transformLineData(line, ffline);
                }
            });

            this.result.fulfillments = _.values(this.result.fulfillments);

            // ADDED FOR BOPIS
            this.transformResultData();
        }
    });
});