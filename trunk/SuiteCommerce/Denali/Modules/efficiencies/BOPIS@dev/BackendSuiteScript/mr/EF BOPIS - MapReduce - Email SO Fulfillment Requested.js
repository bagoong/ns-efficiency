/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 *
 */
define([
    '../libs/Bopis.SalesOrder.ItemLines',
    '../libs/Bopis.SalesOrder.Email'
], function EFBOPISEmailSOFulfillmentRequested(
    BopisSOItemLines,
    BopisSOEmail
) {
    'use strict';

    return {
        getInputData: function getInputData() {
            BopisSOItemLines.addPickupRequestColumns();
            BopisSOItemLines.addPickupRequestFilter();
            return BopisSOItemLines.getOrderIDs();
        },
        map: function map(context) {
            var result = JSON.parse(context.value);
            var quantityDiffs = parseInt(result.values['MAX(formulanumeric)'], 10);
            var salesOrderID = result.values['GROUP(internalid)'].value;

            if ( quantityDiffs === 0 ) {
                BopisSOEmail.sendEmail({
                    salesOrderID: salesOrderID,
                    emailType: BopisSOEmail.EMAIL_TYPE_SO_FULLFILLMENT_REQUEST,
                    templatePath: 'SuiteScripts/Bopis/advanced_templates/' +
                    'EF BOPIS - Sales Order - Email SO Request Fulfillment.html'
                });
            }
        }
    }
});