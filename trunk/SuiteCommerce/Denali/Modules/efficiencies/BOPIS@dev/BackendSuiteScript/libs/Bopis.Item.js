/**
 *@NApiVersion 2.x
 */
define([
    '../third_party/underscore-custom',
    '../libs/EFUtil',
    '../libs/Bopis.InventoryDetail',
    'N/search',
    'N/config'
], function BopisItem(
    _,
    EFUtil,
    InventoryDetail,
    searchAPI,
    configAPI
) {
    'use strict';


    return {
        /**
         * @param options
         * @param {Integer} options.subsidiary Subsidiary
         * @param {Integer} options.website Website
         * @param {Integer[]} options.internalids Item InternalIds
         * @param {Integer} [options.locationType] LocationType (1 - Store, 2 - Warehouse)
         * @param {Integer[]} [options.locations] Location Ids to return
         * @returns {String[][]} Filters
         */
        _getItemQuantityAvailableFilters: function _getItemQuantityAvailableFilters(options) {
            var filters = [
                ['internalid', 'anyof', options.internalids],
                'and',
                ['isonline', 'is', true],
                'and',
                ['isinactive', 'is', false],
                'and',
                ['inventorylocation.subsidiary', 'is', options.subsidiary],
                'and',
                ['inventorylocation.custrecord_bopis_location_webstores', 'is', options.website],
                'and',
                ['inventorylocation.makeinventoryavailable', 'is', true],
                'and',
                ['inventorylocation.makeinventoryavailablestore', 'is', true],
                'and',
                ['inventorylocation.isinactive', 'is', false]
            ];

            if (options.locationType) {
                filters.push('and');
                filters.push(['inventorylocation.locationtype', 'is', options.locationType]);

                /* Check that this particular item is really able to be picked from a store */
                if (options.locationType === 1) {
                    filters.push('and');
                    filters.push(['locationallowstorepickup', 'is', true]);
                }

                /* Check that this particular item is really able to be shipped to the user */
                if (options.locationType === 2) {
                    filters.push('and');
                    filters.push(['locationautolocassignallowed', 'is', true]);
                }
            }

            if (options.locations) {
                filters.push('and');
                filters.push(['inventorylocation', 'anyof', options.locations]);
            }

            return filters;
        },

        /**
         * For an item, obtain total stock information related to a specific website and sub
         * @param options
         * @param {Integer} options.subsidiary Subsidiary
         * @param {Integer} options.website Website
         * @param {Integer[]} options.internalids Item InternalIds
         * @param {Integer[]} options.locations locations
         * @param {Integer} [options.locationType] LocationType (1 - Store, 2 - Warehouse)
         * @returns {{}} Map of locations by id
         */
        getItemQuantityAvailableSummary: function getItemQuantityAvailableSummary(options) {
            /** @type {Search} */
            var inventorySearch;

            var results = {};
            var filters = this._getItemQuantityAvailableFilters(options);
            var columns = {
                internalid: {
                    name: 'internalid',
                    summary: searchAPI.Summary.GROUP
                },
                outOfStockBehavior: {
                    name: 'outofstockbehavior',
                    summary: searchAPI.Summary.GROUP,
                    type: 'name'
                },
                quantityAvailable: {
                    name: 'locationquantityavailable',
                    summary: searchAPI.Summary.SUM,
                    type: 'float',
                    default: 0
                },
                parentOutOfStockBehavior: {
                    name: 'outofstockbehavior',
                    summary: searchAPI.Summary.GROUP,
                    join: 'parent',
                    type: 'name'
                },
                parentIsMatrix: {
                    name: 'matrix',
                    summary: searchAPI.Summary.GROUP,
                    join: 'parent'
                }
            };

            inventorySearch = searchAPI.create({
                type: searchAPI.Type.ITEM,
                columns: EFUtil.createColumns(columns),
                filters: filters
            });

            inventorySearch.run({}).each(function eachRunResult(line) {
                var inventoryDetail;
                var itemKey = line.getValue(columns.internalid).toString();
                results[itemKey] = EFUtil.mapResultLine(line, columns);

                inventoryDetail = InventoryDetail.getDetail({
                    quantityAvailable: results[itemKey].quantityAvailable,
                    outOfStockBehavior: results[itemKey].parentIsMatrix ?
                                        results[itemKey].parentOutOfStockBehavior :
                                        results[itemKey].outOfStockBehavior
                });

                _.extend(results[itemKey], inventoryDetail);
                return true;
            });

            return results;
        },

        /**
         * For an item, obtain stock information related to a specific website and sub, refined by location
         * @param options
         * @param {Integer} options.subsidiary Subsidiary
         * @param {Integer} options.website Website
         * @param {Integer} options.internalid Item InternalId
         * @param {Integer} [options.locationType] LocationType (1 - Store, 2 - Warehouse)
         * @param {Integer} [options.location] Location Id to return
         * @param {Integer[]} [options.locations] Location Ids to return
         * @returns {{}} Map of locations by id
         */
        getItemQuantityAvaliableDetails: function getItemQuantityAvaliableDetails(options) {
            /** @type {Search} */
            var inventorySearch;

            var results = {};
            var filters = this._getItemQuantityAvailableFilters(options);
            var columns = {
                internalid: {name: 'internalid', type: 'integer'},
                preferredStockLevel: {name: 'preferredstocklevel', type: 'float'},
                location: {name: 'inventorylocation', type: 'integer'},
                quantityAvailable: {name: 'locationquantityavailable', type: 'float', default: 0},
                locationPreferredStockLevel: {name: 'locationpreferredstocklevel', type: 'float'},
                outOfStockBehavior: {name: 'outofstockbehavior'},
                parentOutOfStockBehavior: {
                    name: 'outofstockbehavior',
                    join: 'parent'
                },
                parentIsMatrix: {
                    name: 'matrix',
                    join: 'parent'
                }
            };

            var configFeatures = configAPI.load({ type: configAPI.Type.FEATURES });
            var isAdvanceInventoryEnabled = configFeatures.getValue({ fieldId: 'advinventorymgmt' });
            var mappedInternalIDs;
            var internalids;
            var inventoryDetail;

            if ( isAdvanceInventoryEnabled ) {
                columns.safetyStockLevel = {
                    name: 'safetystocklevel',
                    type: 'float'
                };

                columns.locationSafetyStockLevel = {
                    name: 'locationsafetystocklevel',
                    type: 'float'
                };
            }

            inventorySearch = searchAPI.create({
                type: searchAPI.Type.ITEM,
                columns: EFUtil.createColumns(columns),
                filters: filters
            });


            inventorySearch.run().each(function eachRunResult(line) {
                var safetyStockLevel;
                var itemKey = line.getValue('internalid').toString();
                var locationKey = line.getValue('inventorylocation').toString();

                results[itemKey] = results[itemKey] || {};
                results[itemKey][locationKey] = EFUtil.mapResultLine(line, columns);

                if ( isAdvanceInventoryEnabled ) {
                    safetyStockLevel = (results[itemKey][locationKey].safetyStockLevel) ?
                                        results[itemKey][locationKey].safetyStockLevel :
                                        results[itemKey][locationKey].locationSafetyStockLevel;
                }

                inventoryDetail = InventoryDetail.getDetail({
                    quantityAvailable: results[itemKey][locationKey].quantityAvailable,
                    preferredStockLevel: results[itemKey][locationKey].locationPreferredStockLevel,
                    safetyStockLevel: safetyStockLevel,
                    outOfStockBehavior: results[itemKey][locationKey].parentIsMatrix ?
                                        results[itemKey][locationKey].parentOutOfStockBehavior :
                                        results[itemKey][locationKey].outOfStockBehavior
                });

                _.extend(results[itemKey][locationKey], inventoryDetail);

                return true;
            });

            if ( _.size(results) > 0 ) {
                mappedInternalIDs = [];
                _.each(results, function eachResult(resultDetail, itemKey) {
                    var outofstockbehavior = '';
                    var mappedLocation = _.map(resultDetail, function mapLocation(res, locationID) {
                        outofstockbehavior = results[itemKey][locationID].parentIsMatrix ?
                                             results[itemKey][locationID].parentOutOfStockBehavior :
                                             results[itemKey][locationID].outOfStockBehavior;
                        return parseInt(locationID, 10);
                    });

                    var locations = _.difference(options.locations, mappedLocation);
                    _.each(locations, function eachLocation(locationID) {
                        inventoryDetail = InventoryDetail.getDetail({
                            quantityAvailable: 0,
                            outOfStockBehavior: outofstockbehavior
                        });

                        results[itemKey][locationID] = _.extend(inventoryDetail, {location: locationID});
                    });

                    mappedInternalIDs.push(parseInt(itemKey, 10));
                });

                internalids = _.difference(options.internalids, mappedInternalIDs);
                _.each(internalids, function eachItemKey(itemKey) {
                    _.each(options.locations, function eachLocation(locationID) {
                        inventoryDetail = InventoryDetail.getDetail({
                            quantityAvailable: 0
                        });
                        results[itemKey] = results[itemKey] || {};
                        results[itemKey][locationID] = _.extend(inventoryDetail, {location: locationID});
                    });
                });
            } else {
                _.each(options.internalids, function eachItemKey(itemKey) {
                    _.each(options.locations, function eachLocation(locationID) {
                        inventoryDetail = InventoryDetail.getDetail({
                            quantityAvailable: 0
                        });
                        results[itemKey] = results[itemKey] || {};
                        results[itemKey][locationID] = _.extend(inventoryDetail, {location: locationID});
                    });
                });
            }
            return results;
        },

        /**
         * For an item, obtain stock information related to a specific website and sub, refined by location
         * @param options
         * @param {Integer} options.subsidiary Subsidiary
         * @param {Integer} options.website Website
         * @param {Object[]} options.cartLines
         * @returns {{}} Hashmap of items with stock info.
         *              Hashmap key is itemid_locationType[_location] in case of stores. This to support multi pickup
         *              in the future without to much hassle
         */
        getCartStockByDeliveryType: function getCartStockByDeliveryType(options) {
            var self = this;
            var shippingLines = _.where(options.cartLines, {locationType: 2});
            var pickupLines = _.where(options.cartLines, {locationType: 1});
            var pickupLinesByStore;
            var shippingLinesStock;
            var pickupLinesByStoreStock;
            var resultsHash = {};

            var stores = _.pluck(pickupLines, 'location');

            pickupLinesByStore = _.reduce(stores, function reduceStores(memo, store) {
                memo[store.toString()] = _.where(pickupLines, {location: store});
                return memo;
            }, {});

            shippingLinesStock = shippingLines && shippingLines.length && this.getItemQuantityAvailableSummary({
                subsidiary: options.subsidiary,
                website: options.website,
                internalids: _.pluck(shippingLines, 'internalid'),
                locationType: 2
            });

            pickupLinesByStoreStock = pickupLinesByStore && _.keys(pickupLinesByStore).length &&
                _.reduce(pickupLinesByStore, function reduceStoreLines(memo, lines, store) {
                    memo[store] = self.getItemQuantityAvaliableDetails({
                        subsidiary: options.subsidiary,
                        website: options.website,
                        location: [parseInt(store, 10)],
                        internalids: _.pluck(lines, 'internalid'),
                        locationType: 1
                    });
                    return memo;
                }, {});

            _.each(shippingLinesStock, function eachShippingLine(line, itemIdKey) {
                resultsHash[itemIdKey + '_' + 2] = line;
            });

            _.each(pickupLinesByStoreStock, function eachStoreStock(resultsByStore, storeKey) {
                _.each(resultsByStore, function eachPickUpLine(line, itemIdKey) {
                    resultsHash[itemIdKey + '_' + 1 + '_' + storeKey ] = line && line[storeKey];
                });
            });


            return resultsHash;
        },

        /**
         * Summarize information about PickUp Stores && Shipping Warehouses for SCA's ITEM API use
         * @param {Record} itemRecord
         */
        setSummarizedLocations: function setSummarizedLocations(itemRecord) {
            var count = itemRecord.getLineCount({sublistId: 'locations'});
            var shippingLocations = [];
            var pickupLocations = [];

            var locationId;
            var storePickUp;
            var shippedFrom;
            var i;

            for (i = 0; i < count; i++) {
                // Get Data
                locationId = parseInt(itemRecord.getSublistValue({
                    sublistId: 'locations',
                    fieldId: 'location',
                    line: i
                }), 10);

                storePickUp = itemRecord.getSublistValue({
                    sublistId: 'locations',
                    fieldId: 'locationallowstorepickup',
                    line: i
                });

                shippedFrom = itemRecord.getSublistValue({
                    sublistId: 'locations',
                    fieldId: 'locationautolocassignallowed',
                    line: i
                });

                // Processing
                if (storePickUp) {
                    pickupLocations.push(locationId);
                }

                if (shippedFrom) {
                    shippingLocations.push(locationId);
                }
            }

            // Set
            // TODO: enforce sorting here for better performance on webstore
            itemRecord.setValue('custitem_bopis_ships_from', JSON.stringify(shippingLocations));
            itemRecord.setValue('custitem_bopis_pick_from', JSON.stringify(pickupLocations));
        }

    };
});