define('HomepagePopup', [
    'HomepagePopup.View',
    'underscore',
    'SC.Configuration',
    'Profile.Model',
    'jQuery',
    'jquery.cookie'
], function HomepagePopup(
    View,
    _,
    Configuration,
    ProfileModel,
    jQuery
) {
    'use strict';

    var SC = window.SC;

    return {
        onceAfterAppendViewPromise: jQuery.Deferred(),

        mountToApp: function mountToApp(application) {
            var self = this;
            var layout = application.getLayout();
            var view = new View({
                application: application
            });
            var cookie = jQuery.cookie('alreadySelectedCountry');

            // Only in browser
            if (SC.ENVIRONMENT.jsEnvironment === 'browser') {
                // only if it's the first time we show this
                // if (!cookie) { // commented so I can test
                    // render on first after append view. A Promise because we don't need
                    layout.once('afterAppendView', function onceAfterAppendView() {
                        self.onceAfterAppendViewPromise.resolve();
                    });

                    // Once we have both the rendering promise resolved, and a profile
                    jQuery.when(self.onceAfterAppendViewPromise, ProfileModel.getPromise()).then(function then() {
                        var internalid = parseInt(ProfileModel.getInstance().get('internalid'), 10);

                        if (!internalid) {
                            layout.showInModal(view);
                            jQuery.cookie('alreadySelectedCountry', true);
                        }
                    });
                // }
            }
        }
    };
});