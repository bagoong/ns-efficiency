/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

/**
 * @NApiVersion 2.0
 * @NScriptName QuickOrder
 * @NScriptId _quickorder2
 * @NModuleScope Public
 * @NScriptType Suitelet
 */
define(['N/search'], function QuickOrder(searchAPI) {
    'use strict';
    return {
        onRequest: function onRequest(context) {
            var filters = [];
            var columns = [
                'internalid',
                'itemid',
                'type',
                'subtype',
                'parent'
            ];
            var userInput = context.request.parameters.userInput;
            var siteId = context.request.parameters.siteId;
            var results = [];

            // add filter - product is available for online use (webstore check)
            filters.push(searchAPI.createFilter({
                name: 'isonline',
                operator: searchAPI.Operator.IS,
                values: 'T'
            }));

            // add filter - product is active
            filters.push(searchAPI.createFilter({
                name: 'isinactive',
                operator: searchAPI.Operator.IS,
                values: 'F'
            }));

            // add filter - is not parent product
            filters.push(searchAPI.createFilter({
                name: 'matrix',
                operator: searchAPI.Operator.IS,
                values: 'F'
            }));

            // add filter - itemid startswith input value
            filters.push(searchAPI.createFilter({
                name: 'website',
                operator: searchAPI.Operator.ANYOF,
                values: siteId
            }));

            // add filter - itemid startswith input value
            filters.push(searchAPI.createFilter({
                name: 'itemid',
                operator: searchAPI.Operator.STARTSWITH,
                values: userInput
            }));

            // searchAPI - item search / apply filters / request certain columns (fields)
            searchAPI.create({
                type: 'item',
                columns: columns,
                filters: filters
            }).run().each(function each(r) {
                // for each return only these columns (fields)
                results.push({
                    id: r.getValue('internalid'),
                    itemid: r.getValue('itemid'),
                    type: r.getValue('type'),
                    subtype: r.getValue('subtype'),
                    parent: r.getValue('parent')
                });
                return true;
            });
            // convert value to json string
            context.response.write(JSON.stringify(results));
        }
    };
});