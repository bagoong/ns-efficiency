define('BOPIS.OrderWizard.Module.StoreSelector', [
    'Backbone.CompositeView',
    'Wizard.Module',
    'Backbone',
    'BOPIS.StoreDetails.Simplified.View',
    'Profile.MyStore.Model',
    'Store.Model',

    'underscore',
    'jQuery',
    'bopis_order_wizard_store_selector.tpl'

], function BOPISOrderWizardModuleStoreSelector(
    BackboneCompositeView,
    WizardModule,
    Backbone,
    StoreDetailView,
    MyStoreModel,
    Store,
    _,
    jQuery,
    templateTpl
) {
    'use strict';

    return WizardModule.extend({
        template: templateTpl,
        events: {
            'change [data-action="set-store"]': 'changeStore'
        },

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);
            WizardModule.prototype.initialize.apply(this, arguments);

            this.listenTo(this.model, 'change:deliverymethod', jQuery.proxy(this.render, this));
            this.listenTo(this.model, 'sync', jQuery.proxy(this.render, this));

            if (!options.readOnly) {
                this.listenTo(MyStoreModel.getInstance(), 'sync', this.changeStore);
            }
        },
        childViews: {
            'StoreDetail': function childViewStoreDetail() {
                return new StoreDetailView({
                    application: this.wizard.application,
                    model: MyStoreModel.getInstance(),
                    showButton: !this.options.readOnly,
                    mapSizeSmall: false
                });
            }
        },
        submit: function submit() {
            if (!this.model.get('deliverymethod')) {
                this.model.set('deliverymethod', 'partial');
            }
        },

        changeStore: function changeStore() {
            this.wizard.model.set('store', MyStoreModel.getInstance()).save();
        },

        isActive: function isActive() {
            return this.wizard.shouldOfferBOPIS();
        },

        getContext: function getContext() {
            return {
                isReadOnly: this.options.readOnly,
                isActive: this.model.getLinesForPicking().length > 0
            };
        }
    });
});