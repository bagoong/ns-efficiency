// This is just a backup of the standard flow, reformated with Efficiencies standards for further reference
define('SC.Checkout.Configuration.Steps.BOPIS', [
    'underscore',
    'Utils',
    'OrderWizard.Module.MultiShipTo.EnableLink',
    'OrderWizard.Module.CartSummary',
    'OrderWizard.Module.Address.Shipping',
    'OrderWizard.Module.PaymentMethod.GiftCertificates',
    'OrderWizard.Module.PaymentMethod.Selector',
    'OrderWizard.Module.Address.Billing',
    'OrderWizard.Module.RegisterEmail',
    'OrderWizard.Module.ShowPayments',
    'OrderWizard.Module.SubmitButton',
    'OrderWizard.Module.TermsAndConditions',
    'OrderWizard.Module.Confirmation',
    'OrderWizard.Module.RegisterGuest',
    'OrderWizard.Module.PromocodeForm',

    'BOPIS.OrderWizard.Module.PickupBy',
    'BOPIS.OrderWizard.Module.StoreSelector',
    'BOPIS.OrderWizard.Module.ShippingPackage',
    'BOPIS.OrderWizard.Module.PickupPackage',
    'BOPIS.OrderWizard.Module.NonDeliverableItems',
    'BOPIS.OrderWizard.Module.CartItemsForPickup',
    'BOPIS.OrderWizard.Module.CartItemsForShipping',
    'BOPIS.OrderWizard.Module.StockValidator',

    'OrderWizard.Module.MultiShipTo.Select.Addresses.Shipping',
    'OrderWizard.Module.MultiShipTo.Package.Creation',
    'OrderWizard.Module.MultiShipTo.Package.List',
    'OrderWizard.Module.NonShippableItems',
    'OrderWizard.Module.MultiShipTo.Shipmethod',
    'OrderWizard.Module.Shipmethod',
    'OrderWizard.Module.ShowShipments',
    'OrderWizard.Module.CartItems',
    'Header.View'
], function SCCheckoutConfigurationStepsStandardReformatted(
    _,
    Utils,
    OrderWizardModuleMultiShipToEnableLink,
    OrderWizardModuleCartSummary,
    OrderWizardModuleAddressShipping,
    OrderWizardModulePaymentMethodGiftCertificates,
    OrderWizardModulePaymentMethodSelector,
    OrderWizardModuleAddressBilling,
    OrderWizardModuleRegisterEmail,
    OrderWizardModuleShowPayments,
    OrderWizardModuleSubmitButton,
    OrderWizardModuleTermsAndConditions,
    OrderWizardModuleConfirmation,
    OrderWizardModuleRegisterGuest,
    OrderWizardModulePromocodeForm,

    BOPISOrderWizardModulePickupBy,
    BOPISOrderWizardModuleStoreSelector,
    BOPISOrderWizardModuleShippingPackage,
    BOPISOrderWizardModulePickupPackage,
    BOPISOrderWizardModuleNonDeliverableItems,
    BOPISOrderWizardModuleCartItemsForPickup,
    BOPISOrderWizardModuleCartItemsForShipping,
    BOPISOrderWizardModuleStockValidator,

    OrderWizardModuleMultiShipToSelectAddressesShipping,
    OrderWizardModuleMultiShipToPackageCreation,
    OrderWizardModuleMultiShipToPackageList,
    OrderWizardModuleNonShippableItems,
    OrderWizardModuleMultiShipToShipmethod,
    OrderWizardModuleShipmethod,
    OrderWizardModuleShowShipments,
    OrderWizardModuleCartItems,
    HeaderView
) {
    'use strict';

    var mstDeliveryOptions = {
        is_read_only: false,
        show_edit_address_url: false,
        hide_accordion: true,
        collapse_items: true
    };

    var showShipmentOptions = {
        edit_url: '/shipping/address',
        show_edit_address_url: true,
        hide_title: true,
        edit_shipment_url: 'shipping/addressPackages',
        edit_shipment_address_url: 'shipping/selectAddress',
        is_read_only: false,
        show_combo: true,
        show_edit_button: true,
        hide_item_link: true
    };

    var cartSummaryOptions = {
        exclude_on_skip_step: true,
        allow_remove_promocode: true,
        container: '#wizard-step-content-right'
    };

    var cartItemOptionsRight = {
        container: '#wizard-step-content-right',
        hideHeaders: true,
        showMobile: true,
        exclude_on_skip_step: true,
        showOpenedAccordion: _.isTabletDevice() || _.isDesktopDevice() || false
    };

    return [
        {
            name: _('Pick up in store').translate(),
            steps: [
                {
                    url: 'bopis/pickup',
                    isActive: function isActiveBOPIS() {
                        return this.wizard.shouldOfferBOPIS();
                    },
                    modules: [
                        BOPISOrderWizardModuleStoreSelector,
                        BOPISOrderWizardModulePickupBy,
                        BOPISOrderWizardModulePickupPackage,
                        BOPISOrderWizardModuleShippingPackage,
                        BOPISOrderWizardModuleNonDeliverableItems,
                        [OrderWizardModuleCartSummary, cartSummaryOptions],
                        BOPISOrderWizardModuleStockValidator
                    ]
                }
            ]
        }, {
            name: _('Shipping Address').translate(),
            steps: [
                {
                    name: _('Choose Shipping Address').translate(),
                    url: 'shipping/address',
                    isActive: function isActiveShippingAddress() {
                        return this.wizard.hasItemsToShip();
                    },
                    modules: [
                        OrderWizardModuleAddressShipping,
                        [OrderWizardModuleCartSummary, cartSummaryOptions],
                        [OrderWizardModulePromocodeForm, {container: '#wizard-step-content-right'}],

                        // =========== OrderWizardModuleCartItems replacement start
                        [BOPISOrderWizardModuleCartItemsForPickup, cartItemOptionsRight],
                        [BOPISOrderWizardModuleCartItemsForShipping, cartItemOptionsRight]
                        // OrderWizardModuleCartItems replacement end ===========
                    ]
                }
            ]
        }, {
            name: _('Shipping Method').translate(),
            steps: [
                {
                    name: _('Choose delivery method').translate(),
                    url: 'shipping/method',
                    isActive: function isActiveShippingMethod() {
                        return this.wizard.hasItemsToShip();
                    },
                    modules: [
                        [OrderWizardModuleAddressShipping, {title: _('Ship To:').translate()}],
                        [OrderWizardModuleShipmethod, mstDeliveryOptions],
                        [OrderWizardModuleCartSummary, cartSummaryOptions],
                        [OrderWizardModulePromocodeForm, {container: '#wizard-step-content-right'}],

                        // =========== OrderWizardModuleCartItems replacement start
                        [BOPISOrderWizardModuleCartItemsForPickup, cartItemOptionsRight],
                        [BOPISOrderWizardModuleCartItemsForShipping, cartItemOptionsRight]
                        // OrderWizardModuleCartItems replacement end ===========
                    ]
                }
            ]
        }, {
            name: _('Payment').translate(),
            steps: [
                {
                    name: _('Choose Payment Method').translate(),
                    url: 'billing',
                    bottomMessage: _('You will have an opportunity to review your order on the next step.').translate(),
                    modules: [
                        OrderWizardModulePaymentMethodGiftCertificates,
                        [OrderWizardModulePaymentMethodSelector, {
                            external_checkout_thank_you_url: 'confirmation',
                            external_checkout_error_url: 'billing'
                        }],
                        [OrderWizardModuleAddressBilling, {
                            enable_same_as: function enableSameAsPaymentMethod() {
                                return !this.wizard.isMultiShipTo() &&
                                    this.wizard.model.shippingAddressIsRequired() &&
                                    this.wizard.model.get('deliverymethod') !== 'pickup';
                            },
                            title: _('Enter Billing Address').translate(),
                            select_shipping_address_url: 'shipping/selectAddress'
                        }],
                        OrderWizardModuleRegisterEmail,
                        [OrderWizardModuleCartSummary, cartSummaryOptions],
                        [OrderWizardModulePromocodeForm, {container: '#wizard-step-content-right'}],

                        // =========== OrderWizardModuleCartItems replacement start
                        [BOPISOrderWizardModuleCartItemsForPickup, cartItemOptionsRight],
                        [BOPISOrderWizardModuleCartItemsForShipping, cartItemOptionsRight]
                        // OrderWizardModuleCartItems replacement end ===========
                    ]
                }
            ]
        }, {
            name: _('Review').translate(),
            steps: [{
                name: _('Review Your Order').translate(),
                url: 'review',
                continueButtonLabel: function continueButtonLabelReview() {
                    return this.wizard && this.wizard.isExternalCheckout() ?
                        _('Continue to External Payment').translate() : _('Place Order').translate();
                },
                bottomMessage: function continueButtonLabelReview() {
                    return this.wizard && this.wizard.isExternalCheckout() ?
                        _('You will be redirected to a secure site to confirm your payment.').translate() : '';
                },
                modules: [
                    // Mobile Top
                    [OrderWizardModuleTermsAndConditions, { className: 'order-wizard-termsandconditions-module-top'}],
                    // Mobile Top
                    [OrderWizardModuleSubmitButton, {className: 'order-wizard-submitbutton-module-top'}],
                    [BOPISOrderWizardModuleStoreSelector, { readOnly: true} ],
                    [BOPISOrderWizardModulePickupBy, { readOnly: true}],

                    [BOPISOrderWizardModuleCartItemsForPickup, {
                        isActive: function isActiveReviewCartItems() {
                            return !this.wizard.isMultiShipTo();
                        }
                    }],

                    [OrderWizardModuleShowShipments, showShipmentOptions],
                    [OrderWizardModuleMultiShipToShipmethod, showShipmentOptions],

                    [BOPISOrderWizardModuleCartItemsForShipping, {
                        isActive: function isActiveReviewCartItems() {
                            return !this.wizard.isMultiShipTo();
                        }
                    }],

                    [OrderWizardModuleNonShippableItems, showShipmentOptions],
                    [OrderWizardModuleShowPayments, {edit_url_billing: '/billing', edit_url_address: '/billing'}],

                    // Desktop Bottom
                    [OrderWizardModuleTermsAndConditions, {
                        className: 'order-wizard-termsandconditions-module-default'
                    }],
                    [OrderWizardModuleCartSummary, cartSummaryOptions],
                    // Desktop Right
                    [OrderWizardModuleTermsAndConditions, {
                        container: '#wizard-step-content-right',
                        className: 'order-wizard-termsandconditions-module-top-summary'
                    }],
                    [OrderWizardModuleSubmitButton, {
                        container: '#wizard-step-content-right', showWrapper: true,
                        wrapperClass: 'order-wizard-submitbutton-container'
                    }],
                    [OrderWizardModulePromocodeForm, {container: '#wizard-step-content-right'}],
                    // Mobile Right Bottom
                    [OrderWizardModuleTermsAndConditions, {
                        className: 'order-wizard-termsandconditions-module-bottom',
                        container: '#wizard-step-content-right'
                    }]
                ],
                save: function save() {
                    var self = this;
                    var submitOperation;
                    _.first(this.moduleInstances).trigger('change_label_continue', _('Processing...').translate());

                    submitOperation = this.wizard.model.submit();

                    submitOperation.always(function alwaysAfterSubmit() {
                        _.first(self.moduleInstances).trigger('change_label_continue');
                    });

                    return submitOperation;
                }
            }, {
                url: 'confirmation',
                hideContinueButton: true,
                hideBackButton: true,
                hideBreadcrumb: true,
                headerView: HeaderView,
                modules: [
                    [OrderWizardModuleConfirmation, {
                        additional_confirmation_message: _('You will receive an email with ' +
                            'this confirmation in a few minutes.'
                        ).translate()
                    }],
                    [OrderWizardModuleRegisterGuest],
                    [OrderWizardModuleCartSummary, _.extend(_.clone(cartSummaryOptions), {
                        hideSummaryItems: true,
                        show_promocode_form: false,
                        allow_remove_promocode: false,
                        isConfirmation: true
                    })]
                ]
            }]
        }
    ];
});
