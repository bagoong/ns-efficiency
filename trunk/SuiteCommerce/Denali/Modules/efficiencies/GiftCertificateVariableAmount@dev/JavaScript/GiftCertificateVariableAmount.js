define('GiftCertificateVariableAmount', [
    'ItemDetails.View',
    'underscore'
], function GiftCertificateVariableAmount(
     ItemDetailsView,
     _
) {
    'use strict';
    return {
        mountToApp: function mountToApp() {
            ItemDetailsView.prototype.initialize =
                _.wrap(ItemDetailsView.prototype.initialize, function initialize(fn, options) {
                    fn.apply(this, _.toArray(arguments).slice(1));

                    this.on('afterViewRender', function afterViewRender() {
                        if (this.model.get('itemtype') === 'GiftCert') {
                            if (this.model.get('custitem_is_variable_amount')) {

                            }
                        }
                    });
            });
        }
    };
});