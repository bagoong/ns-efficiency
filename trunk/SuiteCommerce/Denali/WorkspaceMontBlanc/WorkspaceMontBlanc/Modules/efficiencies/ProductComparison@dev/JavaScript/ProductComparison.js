// module ProductComparison
define('ProductComparison', [
    'Backbone',
    'ProductComparison.Router',
    'underscore',
    'Utils',
    'Facets.Browse.View.Extended',
    'Facets.ItemCell.View.Extended'
], function ProductComparison(
    Backbone,
    Router
) {
    'use strict';

    var ObjModule = ( function ObjModule() {
        var mountToApp = function mountToApp(application) {
            var layout = application.getLayout();

            layout.goToProductComparison = function goToProductComparison() {
                Backbone.history.navigate('productcomparison', { trigger: true });
            };

            application.ProductComparisonModule = ObjModule;
            return new Router(application);
        };

        return {
            Router: Router,
            mountToApp: mountToApp
        };
    })();

    return ObjModule;
});
