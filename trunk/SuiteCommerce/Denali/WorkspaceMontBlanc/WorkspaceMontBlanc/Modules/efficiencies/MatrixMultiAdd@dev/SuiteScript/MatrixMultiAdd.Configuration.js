define('MatrixMultiAdd.Configuration', [
    'Configuration',
    'underscore'

], function MatrixMultiAddConfiguration(
    Configuration,
    _
) {
    'use strict';
    var colors;
    var MatrixMultiAdd;
    colors = {
        'black': '#212121',
        'gray': '#9c9c9c',
        'grey': '#9c9c9c',
        'white': '#fff',
        'brown': '#804d3b',
        'beige': '#eedcbe',
        'blue': '#0f5da3',
        'light-blue': '#8fdeec',
        'purple': '#9b4a97 ',
        'lilac': '#ceadd0',
        'red': '#f63440',
        'pink': '#ffa5c1',
        'orange': '#ff5200',
        'peach': '#ffcc8c',
        'yellow': '#ffde00',
        'light-yellow': '#ffee7a',
        'green': '#00af43',
        'lime': '#c3d600',
        'teal': '#00ab95',
        'aqua': '#28e1c5',
        'burgandy': '#9c0633',
        'navy': '#002d5d'
    };

    MatrixMultiAdd = {
        siteWide: false,
        colsFieldIds: ['custcol1'],
        rowsFieldIds: ['custcol2'],
        colors: colors
    };

    _.extend(MatrixMultiAdd, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'MatrixMultiAdd_config',
        model: 'MatrixMultiAdd.Configuration',
        call: 'get'
    });

    return MatrixMultiAdd;
});