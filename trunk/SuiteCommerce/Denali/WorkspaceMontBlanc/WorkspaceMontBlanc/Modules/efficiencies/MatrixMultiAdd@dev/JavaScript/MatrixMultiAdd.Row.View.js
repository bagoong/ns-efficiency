define('MatrixMultiAdd.Row.View', [
    'Backbone',
    'QuantityPricing.View',
    'MatrixMultiAdd.Item.HashMap',

    'matrix_multi_add_row.tpl',
    'underscore',
    'jQuery'
], function MatrixMultiAddRowView(
    Backbone,
    QuantityPricingView,
    MatrixMultiAddItemHashMap,
    Template,
    _,
    jQuery

) {
    'use strict';

    var isDisplay;
    var self;

    return Backbone.View.extend({
        template: Template,

        events: {
            'click [data-action="mma-show-prices"]': 'showPrices',
            'click [data-field="input"]': 'stopPropagation'
        },

        initialize: function initialize(options) {
            var colsFieldID;
            var rowsFieldID;
            var selectedRows;
            var selectedCols;
            var selectedColsResult;

            self = this;
            this.itemsForCart = options.itemsForCart;
            this.parentModel = this.options.parentModel; // parent model from the view that will be also use for quantity pricing.

            // quantity updates
            _.each(this.itemsForCart.getAll(), function eachItemsForCart(item) {
                colsFieldID = (item.get('_getColsOption')) ? item.get('_getColsOption').cartOptionId : '';
                rowsFieldID = (item.get('_getRowsOption')) ? item.get('_getRowsOption').cartOptionId : '';

                selectedRows = (rowsFieldID) ? _.findWhere(self.model.get('rows'),
                    {internalid: item.getOption(rowsFieldID).internalid}) : '';
                selectedCols = (colsFieldID) ? _.findWhere(self.model,
                    {internalid: item.getOption(colsFieldID).internalid}) : '';

                if (selectedCols) {
                    if (selectedCols.rows && rowsFieldID) { // @if has rows and columns
                        selectedColsResult = (rowsFieldID) ? _.findWhere(selectedCols.rows,
                            {internalid: item.getOption(rowsFieldID).internalid}) : 0;
                        selectedColsResult.quantity = item.get('quantity');
                    } else { // @if has cols only
                        self.model.set('quantity', item.get('quantity'));
                    }
                } else {
                    if (selectedRows && colsFieldID === '') { // @if has rows only
                        selectedRows.quantity = item.get('quantity');
                    }
                }
            });
        },

        showPrices: function showPrices(e) {
            var $target = this.$el.find(e.currentTarget);
            var $rowID = $target.data('rowid');
            // this.$el.find('tr[data-priceid]') or this.$('tr[data-priceid]')
            // produces the incorrect behaviour if used below
            var $priceID = jQuery('tr[data-priceid]');
            var collapseClass = 'matrix-multi-add-collapse';
            $priceID.each(function eachPriceID(i, tr) {
                // show the prices under the clicked tr and
                // if tr isn't the one clicked and doesn't have collapse class then add it
                $priceID = jQuery(tr).data('priceid');
                if ($priceID === $rowID) {
                    jQuery(tr).toggleClass(collapseClass);
                } else if (($priceID !== $rowID) && (!jQuery(tr).hasClass(collapseClass))) {
                    jQuery(tr).addClass(collapseClass);
                }
            });
        },

        stopPropagation: function stopPropagation(e) {
            // don't open the pricing row when user clicks on the quantity input field
            e.stopPropagation();
        },

        getContext: function getContext() {
            if (this.model.get('internalid')) {
                isDisplay = this.model.get('internalid');
            } else {
                isDisplay = this.model.get('rows');
            }

            return {
                isDisplay: isDisplay,
                uniqueID: this.model.get('internalid'),
                thumbnailImage: this.model.get('thumbnail'),

                colsLabel: this.model.get('label'),
                cols: this.model.get('cols'),
                colsCollection: this.model.attributes,

                rowsCollection: this.model.get('rows'),

                pricing: this.itemsForCart.quantityPricingCollection(this.parentModel, this.model)
            };
        }
    });
});