var Handler = (function(){
    var websiteResultsCache = {};
    var urlsCache = {};


    function suiteletCallback(response){
        var body = JSON.parse(response.body);


        var website = nlapiGetFieldValue('custrecord_ef_gfg_fi_website');
        websiteResultsCache['website'+website] = body;

        setValues(body);

    }

    function setValues(data)
    {

        //Get fields
        var fakeLocale = getDropdown(nlapiGetField('custpage_fake_locale').uifield);
        var fakeCurrency = getDropdown(nlapiGetField('custpage_fake_currency').uifield);


        //Cleanup locales
        fakeLocale.deleteAllOptions();
        fakeCurrency.deleteAllOptions();

        //Set fields
        fakeLocale.addOption('', '');
        _.each(data.languages, function(l){
            fakeLocale.addOption(l.locale, l.locale);
        });

        fakeCurrency.addOption('', '');
        _.each(data.currencies, function(l){
            fakeCurrency.addOption(l.internalid + '-' + l.name, l.name);
        });


        var website =  nlapiGetFieldValue('custrecord_ef_gfg_fi_website');
        var realCurrency = nlapiGetFieldValue('custrecord_ef_gfg_fi_currency');
        var realLocale = nlapiGetFieldValue('custrecord_ef_gfg_fi_language') + '_' + nlapiGetFieldValue('custrecord_ef_gfg_fi_country');

        if( data.defaultCurrency )
        {
            if( realCurrency )
            {
                //sync values (edit case)
                fakeCurrency.setValue(realCurrency,false); //missing: validate still available to be selected! Change real otherwise
            }
            else
            {
                //set default (create case)
                fakeCurrency.setValue(data.defaultCurrency.name);
            }
        }

        if( data.defaultLanguage )
        {
            if( realLocale !== '_' )
            {
                //sync values (edit case)
                fakeLocale.setValue(realLocale,false); //missing: validate still available to be selected! Change real otherwise
            }
            else
            {
                //set default (create case)
                var defaultLocale = data.defaultLanguage.locale,
                    splitLocale = defaultLocale.split('_'),
                    defaultLanguage = splitLocale[0] || '',
                    defaultCountry = splitLocale [1] || '';

                fakeLocale.setValue(data.defaultLanguage.locale);
            }
        }





    }

    function websiteChanged()
    {
        var website = nlapiGetFieldValue('custrecord_ef_gfg_fi_website');
        if(!website)
        {
            setValues({});
            return;
        }

        if(!urlsCache.suiteletWebsite)
        {
            urlsCache.suiteletWebsite = nlapiResolveURL('SUITELET', 'customscript_ef_gfg_sl_website', 'customdeploy_ef_gfg_website_locales_curr');
        }

        if (!websiteResultsCache['website'+website])
        {
            nlapiRequestURL(urlsCache.suiteletWebsite + '&website=' + website, null, null, suiteletCallback, 'GET');
        }
        else {
            setValues(websiteResultsCache['website'+website]);
        }
    }

    function currencyChanged()
    {
        nlapiSetFieldValue('custrecord_ef_gfg_fi_currency',nlapiGetFieldValue('custpage_fake_currency'),false,false);
    }

    function localeChanged()
    {
        var locale = nlapiGetFieldValue('custpage_fake_locale') || '',
            sp = locale.split('_'),
            country = sp[1],
            language = sp[0];

        nlapiSetFieldValue('custrecord_ef_gfg_fi_country', country , false, false);
        nlapiSetFieldValue('custrecord_ef_gfg_fi_language', language , false, false);

    }
    function pageInit(type)
    {
        websiteChanged();
    }

    function fieldChanged(type, name)
    {
        if(name === 'custrecord_ef_gfg_fi_website')
        {
            websiteChanged();
        }

        if(name === 'custpage_fake_currency')
        {
            currencyChanged();
        }

        if(name === 'custpage_fake_locale')
        {
            localeChanged();
        }

    }

    function saveRecord()
    {

        if( nlapiGetFieldValue('custrecord_ef_gfg_fi_country') === '' )
        {
            alert('Please select a value for country');
            return false;
        }

        if( nlapiGetFieldValue('custrecord_ef_gfg_fi_language') === '' )
        {
            alert('Please select a value for locale');
            return false;
        }

        if( nlapiGetFieldValue('custrecord_ef_gfg_fi_currency')  === '' )
        {

            alert('Please select a value for locale');
            return false;
        }


        return true;
    }


    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        saveRecord: saveRecord
    };

}());