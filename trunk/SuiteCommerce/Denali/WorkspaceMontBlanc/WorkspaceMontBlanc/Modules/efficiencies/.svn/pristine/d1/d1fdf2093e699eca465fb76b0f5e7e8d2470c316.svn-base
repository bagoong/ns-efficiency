/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('GoogleTrustedStore', [
    'GoogleTrustedStore.LoadScript',
    'jQuery',
    'underscore',
    'GoogleTrustedStore.OrderWizard.Steps'
], function GoogleTrustedStore(GTSScript, jQuery, _) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            var index;
            var Configuration = SC.ENVIRONMENT &&
                                SC.ENVIRONMENT.published &&
                                SC.ENVIRONMENT.published.GoogleTrustedStore_config;

            Configuration.locale = SC.ENVIRONMENT.currentLanguage.locale;
            index = _.size(Configuration.gts);

            Object.defineProperty(window, 'GoogleTrustedStore', {configurable: true});

            application.getLayout().on('afterAppendView', function() {
                var model = application.getLayout().currentView.model;

                window.gts = [];

                _.each(Configuration.gts, function configEach(v, k) {
                    if (v.length !== 0 && !_.isEmpty(v)) {
                        window.gts.push([k, v]);
                    }
                });

                if (model && model.get('itemtype')) {
                    if (Configuration.gts.google_base_subaccount_id) {
                        window.gts.splice(index - 1, 0, [
                            'google_base_offer_id',
                            (model.get('matrixchilditems_detail') ?
                                model.get('matrixchilditems_detail')[0].internalid :
                                model.get('internalid'))
                        ]);
                    }
                }

                GTSScript.loadScript();
            });
        }
    };
});