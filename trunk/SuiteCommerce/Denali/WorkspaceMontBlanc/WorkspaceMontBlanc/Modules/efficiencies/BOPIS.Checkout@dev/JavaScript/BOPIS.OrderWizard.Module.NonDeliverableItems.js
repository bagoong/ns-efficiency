define('BOPIS.OrderWizard.Module.NonDeliverableItems', [
    'OrderWizard.Module.NonShippableItems'
], function BOPISOrderWizardModuleNonDeliverableItems(
    WizardModule
) {
    'use strict';

    return WizardModule.extend({
        isActive: function isActive() {
            return this.wizard.shouldOfferBOPIS() && this.wizard.model.getNonShippableLines().length > 0;
        }
    });
});