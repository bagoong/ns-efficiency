define('MyGiftCertificates.Collection', [
    'Backbone',
    'MyGiftCertificates.Model',
    'underscore'
], function GiftCertsCollection(
    Backbone,
    Model,
    _
) {
    'use strict';

    return Backbone.Collection.extend({
        model: Model,
        url: _.getAbsoluteUrl('services/MyGiftCertificates.Service.ss')
    });
});