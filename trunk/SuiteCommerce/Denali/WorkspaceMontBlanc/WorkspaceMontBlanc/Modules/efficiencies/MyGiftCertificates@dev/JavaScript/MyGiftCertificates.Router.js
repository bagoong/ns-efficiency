define('MyGiftCertificates.Router', [
    'Backbone',
    'MyGiftCertificates.List.View',
    'MyGiftCertificates.Model',
    'MyGiftCertificates.Collection'
], function GiftCertsRouter(
    Backbone,
    ListView,
    Model,
    Collection
) {
    'use strict';

    return Backbone.Router.extend({
        initialize: function initialize(application) {
            this.application = application;
        },

        routes: {
            'giftcertificates': 'giftCertList'
            //,
            //'giftcertificates/:id': 'giftCertificate'
        },

        /*
         * List user's available gift certificates and balance
         */
        giftCertList: function giftCertList() {
            var collection = new Collection();
            var view = new ListView({collection: collection, application: this.application});
            collection.fetch().done(function fetchColl() {
                view.showContent();
            });
        }
    });
});