/*
    © 2015 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

/*exported service*/
// MyGiftCertificates.Service.ss
// ----------------
// Service to manage MyGiftCertificateses requests

function service(request) {
    'use strict';

    var Application = require('Application');

    try {
        //Only can get, modify, update or delete an MyGiftCertificates if you are logged in
        if (session.isLoggedIn()) {
            var method = request.getMethod()
            ,   id = request.getParameter('internalid')
            //  MyGiftCertificates model is defined on ssp library Models.js
            ,   MyGiftCertificates = require('MyGiftCertificates.Model')
            ,   data = JSON.parse(request.getBody() || '{}');

            switch (method) {
                case 'GET':
                    //If the id exist, sends the response of MyGiftCertificates.get(id), else sends the response of (MyGiftCertificates.list() || [])
                    Application.sendContent(id ? MyGiftCertificates.get(id) : (MyGiftCertificates.list() || []));
                break;


                default:
                    // methodNotAllowedError is defined in ssp library commons.js
                    Application.sendError(methodNotAllowedError);
            }
        }
        else {
            // unauthorizedError is defined in ssp library commons.js
            Application.sendError(unauthorizedError);
        }
    }
    catch (e) {
        Application.sendError(e);
    }
}