<div class="quick-order-line" data-lineid="{{referenceline}}">

    <div class="quick-order-item">
        {{#if addedToCart}}
        <div class="quick-order-item-addedtocart-message">
            {{translate 'Added to Cart'}} <br>
            <!-- <span class="quick-order-item-addedtocart-addmore"><a href="#">{{translate '(Add more?)'}}</a></span> -->
        </div>
        {{else}}
        <div>

            <input class="quick-order-item-input" data-type="itemid" data-index="{{index}}" type="text" placeholder="SKU/UPC"  data-field="itemid" name="itemid" value="{{query}}" data-lastquery="{{query}}">

            <button class="quick-order-item-button" type="button" data-action="queryAgain" data-index="{{index}}" ><span class="sr-only">{{translate 'Search'}}</span> <i class="quick-order-search" data-index="{{index}}"></i></button>

        </div>
        {{/if}}
    </div>

    <div {{#if addedToCart}}class="quick-order-item-addtocart-success"{{/if}} >
        <div class="quick-order-qty">
           <input type="number" class="quick-order-qty-input {{#if minQtyAlert}}quick-order-item-minqty-warning{{/if}}" min="{{minquantityvalue}}" name="qty" data-index="{{index}}" data-field="itemqty" value="{{quantity}}" required>
        </div>
    </div>

        <div class="quick-order-remove">
            <a class="quick-order-remove-icon" data-action="removeLine" data-index="{{index}}"><span class="sr-only"> {{translate 'Remove'}}</span></a>
        </div>

    <div {{#if addedToCart}}class="quick-order-item-addtocart-success"{{/if}} >

        {{#if resultsFound}}

        <div class="quick-order-status">
            <i class="quick-order-status-results"></i>
        </div>

        <div class="quick-order-lookup-results">
            <label class="quick-order-lookup-results-group-label sr-only" for="results-{{index}}">{{translate 'Items Found'}}</label>
            <div class="quick-order-dropdown-form-controls">
                <select name="results-{{index}}" id="results-{{index}}" class="quick-order-dropdown-select" data-action="select" data-field="results" data-query="{{query}}" data-index="{{index}}">
                        <option value="">{{translate '-- Select --'}}</option>
                    {{#each results}}
                        <option value="{{internalid}}" {{#if selected}}selected{{/if}}>{{name}}, {{sku}}</option>
                    {{/each}}
                </select>
            </div>
        </div>

        {{/if}}

        {{#if noResults}}

        <div class="quick-order-status">
            <i class="quick-order-status-no-results"></i>
        </div>

        <div class="quick-order-lookup-noresults">
            {{translate 'No Items Found Matching $(0)' query}}
        </div>

        {{/if}}



        {{#if itemSelected}}

        <div class="quick-order-status">
            {{#if isinstock}}
                <i class="quick-order-status-found-instock">In Stock</i>
            {{else}}
                <i class="quick-order-status-found-outofstock">Out of Stock</i>
            {{/if}}

            {{#if minquantity}}
                <i class="quick-order-status-min-quantity">{{translate 'Min Qty:'}} {{minquantityvalue}}</i>
            {{/if}}
        </div>

        {{#if isNavigable}}
        <div class="quick-order-item-image">
            <a {{linkAttributes}} class="quick-order-image-link" target="_blank">
                <img src="{{resizeImage thumbnailURL 'thumbnail'}}" alt="{{thumbnailAltImageText}}" />
            </a>
        </div>
        <div class="quick-order-item-name">
            <a {{linkAttributes}} class="quick-order-name-link" target="_blank">
                {{name}}
                <div class="quick-order-item-sku">{{translate 'SKU/UPC:'}} {{sku}}</div>
            </a>
        </div>

        {{else}}
        <div class="quick-order-item-image-viewonly">
            <img src="{{resizeImage thumbnailURL 'thumbnail'}}" alt="{{thumbnailAltImageText}}" target="_blank" />
        </div>
        <div class="quick-order-item-name-viewonly">
            {{name}}
            <div class="quick-order-item-sku">{{translate 'SKU/UPC:'}} {{sku}}</div>
        </div>
        {{/if}}

        <div class="quick-order-item-price" data-view="Item.Price"></div>

        {{/if}}

        {{#if newline}}
        <div class="quick-order-item-image-placeholder">
            <div class="quick-order-item-image-shape"></div>
        </div>

        <div class="quick-order-item-name-placeholder">
            <div class="quick-order-item-name-shape"></div>
            <div class="quick-order-item-name-shape"></div>
        </div>

        <div class="quick-order-item-price-placeholder">
            <div class="quick-order-item-price-shape"></div>
        </div>
        {{/if}}

    </div>

</div>

<hr>