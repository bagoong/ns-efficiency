/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function service(request, response) {
    'use strict';
    var result = {
        status: 'sucess'
    };
    var customer;
    var customerId = request.getParameter('id');
    var storeId = request.getParameter('storeid');
    var value = null;

    try {
        switch(request.getMethod()) {

        case 'POST':
            nlapiSubmitField('customer',customerId, 'custentity_ef_bopis_preferred_store', storeId);
            result.value = storeId;
            break;
        case 'GET':
            result.value =  nlapiLookupField('customer', customerId, 'custentity_ef_bopis_preferred_store');

        }
    } catch (e) {
        result = {
            status: e.message
        };
    }

    response.setContentType('JSON');
    response.write(JSON.stringify(result));
}