{{#if pricingSchedule}}
<div class="quantity-pricing">
    <h4 class="quantity-pricing-title">{{translate 'Quantity Pricing'}}</h4>
    <div class="quantity-pricing-cms-intro" data-cms-area="quantity_pricing_intro" data-cms-area-filters="page_type"></div>
    {{#each pricingSchedule}}
        {{#if @last}}
            <p class="quantity-pricing-price-each">{{translate 'Buy $(0) or above and pay only $(1) each' minimum price}}</p>
        {{else}}
            {{#if singleFigure}}
                <p class="quantity-pricing-price-each">{{translate 'Buy $(0) and pay only $(1) each' singleFigure price}}</p>
            {{else}}
                <p class="quantity-pricing-price-each">{{translate 'Buy $(0) - $(1) and pay only $(2) each' minimum maximum price}}</p>
            {{/if}}
        {{/if}}

    {{/each}}
</div>
{{/if}}