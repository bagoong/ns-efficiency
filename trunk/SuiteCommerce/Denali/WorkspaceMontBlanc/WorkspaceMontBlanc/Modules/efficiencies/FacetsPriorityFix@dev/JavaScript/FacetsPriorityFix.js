define('FacetsPriorityFix', [
    'Facets.Helper',
    'Facets.FacetedNavigationItem.View',
    'Facets.FacetedNavigation.View',

    'Backbone',
    'Backbone.CollectionView'
], function FacetsPriorityFix(
    FacetsHelper,
    FacetsFacetedNavigationItemView,
    FacetsFacetedNavigationView,

    Backbone,
    BackboneCollectionView
) {
    'use strict';

    var viewPrototype = FacetsFacetedNavigationView.prototype;

    viewPrototype.childViews['Facets.FacetedNavigationItems'] = function FacetsFacetedNavigationItems() {
        var translator = FacetsHelper.parseUrl(this.options.translatorUrl, this.options.translatorConfig);
        var orderedFacets = this.options.facets && this.options.facets.sort(function sort(a, b) {
            // Default Priority is 0
            return (translator.getFacetConfig(b.url || b.id).priority || 0) -
                (translator.getFacetConfig(a.url || a.id).priority || 0);
        });

        return new BackboneCollectionView({
            childView: FacetsFacetedNavigationItemView,
            viewsPerRow: 1,
            collection: new Backbone.Collection(orderedFacets),
            childViewOptions: {
                translator: translator
            }
        });
    };
});