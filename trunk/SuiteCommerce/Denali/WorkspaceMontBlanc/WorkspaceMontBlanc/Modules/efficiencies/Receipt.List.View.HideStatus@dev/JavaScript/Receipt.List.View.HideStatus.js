define('Receipt.List.View.HideStatus', [
    'Receipt.List.View',
    'RecordViews.View',
    'Backbone.CollectionView',
    'Backbone',
    'underscore'
], function ReceiptHideStatus(
    ReceiptListView,
    RecordViewsView,
    BackboneCollectionView,
    Backbone,
    _
) {
    'use strict';
    ReceiptListView.prototype.childViews['Receipt.List.Item'] = function ReceiptListItem() {
        var recordsCollection = new Backbone.Collection(this.collection.map(function mapRecords(receipt) {
            return new Backbone.Model({
                touchpoint: 'customercenter',
                title: _('Receipt #$(0)').translate(receipt.get('order_number')),
                detailsURL: '/receiptshistory/view/' + receipt.id,

                id: receipt.id,
                internalid: receipt.id,

                columns: [{
                    label: _('Date:').translate(),
                    type: 'date',
                    name: 'creation-date',
                    value: receipt.get('date')
                }, {
                    label: _('Amount:').translate(),
                    type: 'currency',
                    name: 'amount',
                    value: receipt.get('summary').total_formatted
                }]

            });
        }));

        return new BackboneCollectionView({
            childView: RecordViewsView,
            collection: recordsCollection,
            viewsPerRow: 1
        });
    };
});