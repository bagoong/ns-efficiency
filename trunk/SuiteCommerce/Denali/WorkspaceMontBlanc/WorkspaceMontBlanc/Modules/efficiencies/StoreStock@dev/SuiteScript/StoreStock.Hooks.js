define('StoreStock.Hooks', [
    'Application',
    'Configuration',
    'underscore'
], function StoreStockHooks(
    Application,
    Configuration,
    _
) {
    'use strict';

    Application.on('after:Profile.get', function afterProfileGet(Model, response) {
        var customerCustomFields = customer.getCustomFields();
        var preferredStoreField;

        if (session.isLoggedIn2()) {
            preferredStoreField = _.findWhere(customerCustomFields, {name: 'custentity_isp_preferred_store'});
            response.preferredStoreID = preferredStoreField.value;
        }
    });
});