/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.Configuration', [
    'underscore',
    'Utils'
], function StoreStockConfiguration(
    _
) {
    'use strict';

    var IN_STOCK = 1;
    var OUT_OF_STOCK = 2;
    var LOW_STOCK = 3;

    var stockStatus = [];

    stockStatus[IN_STOCK] = {
        badgeLabel: _('In Stock').translate(),
        htmlClass: 'store-stock-bg-in-stock'
    };

    stockStatus[OUT_OF_STOCK] = {
        badgeLabel: _('Out of Stock').translate(),
        htmlClass: 'store-stock-bg-out-stock'
    };

    stockStatus[LOW_STOCK] = {
        badgeLabel: _('Low Stock').translate(),
        htmlClass: 'store-stock-bg-limited-stock'
    };

    return {
        isDisplayAsBadge: false,
        stockStatus: stockStatus,
        itemTypesWithInvt: ['InvtPart']
    };
});