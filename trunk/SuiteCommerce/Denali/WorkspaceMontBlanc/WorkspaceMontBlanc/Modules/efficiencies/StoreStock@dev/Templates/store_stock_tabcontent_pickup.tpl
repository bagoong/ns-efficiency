 {{#if isSelectionComplete}}
    {{#if preferredStore}}
        <div>
            <div data-view="StoreStock.PreferredStoreStockStatus.View"></div>
            <button class="store-stock-button-change-store" data-action="show-storelocator" data-showin="modal">
                {{translate 'Change Store'}}
            </button>
            <button class="store-stock-button-change-store-mobile" data-action="show-storelocator" data-showin="pushpane" data-type="sc-pusher" data-target="pushable-map" >
                {{translate 'Change Store'}}
            </button>
        </div>
    {{else}}
        <div>
            <div>
                <label class="store-stock-label-find-store">
                    {{translate 'Find your local store'}}
                </label>
                <input type="text" name="store-stock-store" class="store-stock-input-store"
                       value="{{storeStockStoreSearchText}}" />
            </div>
            <button class="store-stock-button-check-stock" data-action="show-storelocator" data-showin="modal">
                {{translate 'Check Stock'}}
            </button>
            <button class="store-stock-button-check-stock-mobile" data-type="sc-pusher" data-target="pushable-map" data-action="show-storelocator" data-showin="pushpane">
                {{translate 'Check Stock'}}
            </button>
        </div>
    {{/if}}
{{else}}
    <p class="store-stock-help">
        <i class="store-stock-help-icon"></i>
        <span class="store-stock-help-text">
            {{ translate 'Please select options to check stock on your local store'}}
        </span>
    </p>
{{/if}}