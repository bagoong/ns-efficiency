define('CheckoutFileUpload.Model', [
    'LiveOrder.Model',
    'CheckoutFileUpload.Configuration',
    'Application',
    'underscore'
], function CheckoutFileUploadModel(
    LiveOrder,
    CheckoutFileUploadConfiguration,
    Application,
    _
) {
    'use strict';
    var x;

    Application.on('before:LiveOrder.setTransactionBodyField', function beforeLiveOrderSet(Model, data) {
        var fileFields;
        if (!Model.isSecure) return;

        fileFields = Model.getFileCheckoutFileUploadFieldsListNames();
        if (!_.isEmpty(data.options)) {
            data.options = _.omit(data.options, fileFields);
        }
    });

    Application.on('after:LiveOrder.get', function afterLiveOrderGet(Model, data) {
        var fileFields;

        if (!Model.isSecure) return;
        fileFields = Model.getFileCheckoutFileUploadFieldsListNames();
        if (!_.isEmpty(data.options)) {
            Model.fileOptions = _.pick(data.options, fileFields);
            Model.fileOptionsProcessed = true;
            data.options = _.omit(data.options, fileFields);
        } else {
            Model.fileOptionsProcessed = true;
        }
        data.files = Model.getFileCheckoutFileUploadValues();
    });

    Application.on('before:LiveOrder.submit', function beforeLiveOrderSubmit(Model) {
        var requiredIds;
        var withDataIds;
        var missing;

        requiredIds = _.chain(CheckoutFileUploadConfiguration.fields).where(
            {required: true}
        ).pluck('internalid').value();

        withDataIds = _.chain(Model.getFileCheckoutFileUploadValues()).pluck('internalid').value();
        missing = _.difference(requiredIds, withDataIds);

        if ( missing > 0 ) {
            throw nlapiCreateError('ERR_FILE_MISSING', 'File Missing');
        }
    });

    x = _.extend(LiveOrder, {
        addFiles: function addFiles(files) {
            var self;
            if (!this.isSecure) return;
            self = this;
            if (this.isSecure && !_.isEmpty(files)) {
                _.each(files, function filesCFU(file) {
                    self.addFile(file);
                });
            }
        },

        addFile: function addFile(file) {
            var field;
            var objToSave;

            if (!this.isSecure) return;
            objToSave = {};

            field = _.findWhere(CheckoutFileUploadConfiguration.fields, {internalid: file.internalid});

            if (!field) {
                throw methodNotAllowedError;
            }

            objToSave[field.bodyFields.file] = file.file;
            objToSave[field.bodyFields.filename] = file.name;
            objToSave[field.bodyFields.link] = file.link;
            order.setCustomFieldValues(objToSave);
        },

        getFileCheckoutFileUploadFieldsListNames: function getFileCheckoutFileUploadFieldsListNames() {
            var fields = CheckoutFileUploadConfiguration.fields;

            return _.flatten(_.map(fields, function mapFields(field) {
                return _.values(field.bodyFields);
            }));
        },

        removeFile: function removeFile(internalid) {
            var field;
            var objToSave;

            if (!this.isSecure) return;

            field = _.findWhere(CheckoutFileUploadConfiguration.fields, {internalid: internalid});
            objToSave = {};

            if (!field) {
                throw methodNotAllowedError;
            }

            objToSave[field.bodyFields.file] = '';
            objToSave[field.bodyFields.filename] = '';
            objToSave[field.bodyFields.link] = '';

            order.setCustomFieldValues(objToSave);

            this.fileOptionsProcessed = false;
            this.fileOptions = null;
        },

        getFileCheckoutFileUploadValues: function getFileCheckoutFileUploadValues() {
            var fileOptions;

            if (!this.fileOptionsProcessed) {
                fileOptions = _.omit(this.getTransactionBodyField(), this.getFileCheckoutFileUploadFieldsListNames());
            } else {
                fileOptions = this.fileOptions;
            }
            fileOptions = fileOptions || {};

            return _.reject(_.map(
            CheckoutFileUploadConfiguration.fields,

            function mapCheckoutFileUploadConfiguration(value) {
                return {
                    internalid: value.internalid,
                    file: fileOptions[value.bodyFields.file],
                    filename: fileOptions[value.bodyFields.filename],
                    link: fileOptions[value.bodyFields.link]
                };
            }), function lines(line) {
                return !line.file;
            });
        }
    });
    return x;
});