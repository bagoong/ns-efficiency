function service (request) {
    'use strict';
    var method = request.getMethod();
    var	internalid = request.getParameter('internalid');
    var	summarize = request.getParameter('summarize');
    var locationType = request.getParameter('locationType');
    var stores = request.getParameter('stores');
    var InventoryDisplay = require('InventoryDisplay.Model');
    var Application = require('Application');
    try {
        switch (method) {
        case 'GET':
            if (summarize && parseInt(locationType, 10) === 2 && internalid) {
                Application.sendContent(
                    InventoryDisplay.listSummaryShipping(internalid),
                    {'cache': response.CACHE_DURATION_SHORT}
                );
            } else if (!summarize && parseInt(locationType, 10) === 1 && internalid && stores) {
                Application.sendContent(
                    InventoryDisplay.listDetailStorePickup(internalid, stores),
                    {'cache': response.CACHE_DURATION_SHORT}
                );
            } else {
                Application.sendError(methodNotAllowedError);
            }
            break;

        default:
            Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}