# Inventory Display
Provide the shopper with inventory level information for both web store stock and physical store stock.

## Documentation
https://confluence.corp.netsuite.com/x/Y86cAg
