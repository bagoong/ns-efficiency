<section class="inventory-display-container">
    {{#if isSelectionComplete}}
        {{#if hasOneChildView}}
            <div data-view="InventoryDisplay.Status"></div>
        {{else}}
            <div data-view="InventoryDisplay.Shipping"></div>
            <div data-view="InventoryDisplay.Pickup"></div>
        {{/if}}
    {{else}}
        <span>
            <p class="inventory-display-help">
                <i class="inventory-display-help-icon"></i>
                <span class="inventory-display-help-text">
                    {{ translate 'Please select options to check availability'}}
                </span>
            </p>
        </span>
    {{/if}}
</section>