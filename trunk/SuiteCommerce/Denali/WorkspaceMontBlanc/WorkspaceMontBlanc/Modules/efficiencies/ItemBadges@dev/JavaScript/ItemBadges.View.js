/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('ItemBadges.View', [
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'ItemBadges.List.View',
    'itembadges_view.tpl',
    'underscore'
], function ItemBadgesView(Backbone, CompositeView, CollectionView, ItemBadgesList, Template, _) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            CompositeView.add(this);

            this.model = options.model;
            this.collection = options.collection;
            this.listenToOnce(this.collection, 'sync', this.render);

            this.collection.fetch();
        },

        render: function render() {
            return this._render();
        },

        getContext: function getContext() {
            this.badgeCollection = this.collection.filterBadges(this.model.get('custitem_ef_badges') || false);
            return {
                hasBadges: !!this.badgeCollection.length
            };
        },

        childViews: {
            'Itembadges.List.View': function ItembadgesLisView() {
                return new CollectionView({
                    collection: this.badgeCollection,
                    childView: ItemBadgesList
                });
            }
        }
    });
});