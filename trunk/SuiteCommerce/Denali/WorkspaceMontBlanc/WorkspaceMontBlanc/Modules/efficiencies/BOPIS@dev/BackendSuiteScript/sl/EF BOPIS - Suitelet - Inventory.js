/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet

 */
define([
    '../libs/Bopis.Item',
    '../libs/Bopis.Location',
    '../third_party/underscore-custom',
    'N/error'
], function EFBOPISSuiteletLocations(
    BopisItem,
    BopisLocation,
    _,
    errorAPI
) {
    'use strict';

    var handlers = {
        /**
         * @param {Object} context
         * @param {http.ServerRequest} context.request
         * @param {http.ServerResponse} context.response
         */
        getCartStockByDeliveryType: function getCartStockByDeliveryType(context) {
            var parsedParams = {};
            var cartData;
            var cartDataJSON;

            if (context.request.method !== 'POST') {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_INVALID_METHOD',
                    message: 'Invalid Call to method (SHOULD BE POST)'
                });
            }

            parsedParams = {
                subsidiary: parseInt(context.request.parameters.subsidiary, 10),
                website: parseInt(context.request.parameters.website, 10)
            };

            try {
                cartDataJSON = JSON.parse(context.request.body);
            } catch (e) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_INVALID_METHOD',
                    message: 'Cannot parse request body'
                });
            }
            cartData = _.map(cartDataJSON, function eachCartLine(cartLine, index) {

                var newCartLine = {
                    internalid: parseInt(cartLine.internalid, 10),
                    locationType: parseInt(cartLine.locationType, 10),
                    location: parseInt(cartLine.location, 10)
                };

                if (_.isNaN(newCartLine.internalid)) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Need Item Id For line ' + index
                    });
                }

                if (_.isNaN(newCartLine.locationType) ||
                    !BopisLocation.LOCATION_TYPES[newCartLine.locationType - 1]
                ) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Need delivery type For line ' + index
                    });
                }

                if (newCartLine.locationType === 1 /* Store */ &&
                    _.isNaN(newCartLine.location)
                ) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'If it\'s a delivery then a store is needed'
                    });
                }

                if (newCartLine.locationType === 2 /* Delivery/warehouse */ &&
                    !_.isNaN(newCartLine.location)
                ) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'If it\'s a shipment then you cannot specify location'
                    });
                }


                return newCartLine;
            });
            parsedParams.cartLines = cartData;

            context.response.write({
                output: JSON.stringify(BopisItem.getCartStockByDeliveryType(parsedParams))
            });
        },

        /**
         * @param {Context} context
         */
        getItemQuantityAvaliableDetails: function getItemQuantityAvaliableDetails(context) {
            var parsedParams = {
                subsidiary: parseInt(context.request.parameters.subsidiary, 10),
                website: parseInt(context.request.parameters.website, 10)
            };
            var locations = [];
            var internalids = [];

            if (context.request.method !== 'GET') {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_INVALID_METHOD',
                    message: 'Invalid Call to method'
                });
            }

            if (!parsedParams.subsidiary || !util.isNumber(parsedParams.subsidiary)) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Subsidiary missing'
                });
            }

            if (!parsedParams.website || !util.isNumber(parsedParams.website)) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Website missing'
                });
            }

            util.each(
                (context.request.parameters.internalid &&
                context.request.parameters.internalid.split(',')) || [],
                function eachInternalId(i) {
                    var idToInt = parseInt(i, 10);
                    if (!util.isNumber(idToInt)) {
                        throw  errorAPI.create({
                            name: 'SSS_MISSING_REQD_ARGUMENT',
                            message: 'Bad value for Item Internal ID'
                        });
                    } else {
                        internalids.push(idToInt);
                    }
                }
            );

            if (internalids.length < 1) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Bad value for Item Internal ID'
                });
            }
            parsedParams.internalids = internalids;


            if (context.request.parameters.locationType) {
                parsedParams.locationType = parseInt(context.request.parameters.locationType, 10);
                if (
                    !util.isNumber(parsedParams.locationType) ||
                    !BopisLocation.LOCATION_TYPES[parsedParams.locationType - 1]
                ) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Bad value for Location Type'
                    });
                }
            }


            if (context.request.parameters.location) {
                util.each(context.request.parameters.location.split(','), function eachLocation(l) {
                    var lToInt = parseInt(l, 10);
                    if (!util.isNumber(lToInt)) {
                        throw  errorAPI.create({
                            name: 'SSS_MISSING_REQD_ARGUMENT',
                            message: 'Bad value for Location'
                        });
                    } else {
                        locations.push(lToInt);
                    }
                });

                if (locations.length < 1) {
                    throw  errorAPI.create({
                        name: 'SSS_MISSING_REQD_ARGUMENT',
                        message: 'Bad value for Locations'
                    });
                }

                parsedParams.locations = locations;
            }

            if (
                (!parsedParams.locations || parsedParams.locations.length < 1) &&
                !parsedParams.locationType
            ) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Either A Location Type or a location must be specified'
                });
            }


            // context.response.setHeader('Content-Type', 'application/json; charset=UTF-8');
            if ( context.request.parameters.summarize ) {
                context.response.write({
                    output: JSON.stringify(BopisItem.getItemQuantityAvailableSummary(parsedParams))
                });
            } else {
                context.response.write({
                    output: JSON.stringify(BopisItem.getItemQuantityAvaliableDetails(parsedParams))
                });
            }
        }
    };

    return {
        onRequest: function onRequest(context) {
            if (!context.request.parameters ||
                !context.request.parameters.handler ||
                !handlers[context.request.parameters.handler]) {
                throw  errorAPI.create({
                    name: 'SSS_MISSING_REQD_ARGUMENT',
                    message: 'Handler not specified'
                });
            }
            return handlers[context.request.parameters.handler](context);
        }
    };
});