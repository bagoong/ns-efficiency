define('BOPIS.OrderHistory.Hooks', [
    'Application',
    'underscore'
], function BOPISOrderHistoryHooks(
    Application,
    _
) {
    'use strict';

    Application.on(
        'after:OrderHistory.list',
        function bopisAfterTransactionList(Model, orderList) {
            Model.appendStatusDetail(orderList);
        }
    );

    Application.on(
        'after:OrderHistory.getLinesGroups',
        function bopisAfterGetLinesGroups(Model) {
            Model.transformLinesGroups();
        }
    );

    Application.on(
        'after:OrderHistory.get',
        function bopisAfterGet(Model) {
            Model.getPickupBy();
        }
    );
});
