define('BOPIS.FAQs.Collection', [
    'Backbone',
    'underscore',
    'Utils'
], function BopisFAQsCollection(
    Backbone,
    _,
    Utils
) {
    'use strict';

    return Backbone.Collection.extend({
        url: _.getAbsoluteUrl('services/BOPIS.Service.ss')
    });
});