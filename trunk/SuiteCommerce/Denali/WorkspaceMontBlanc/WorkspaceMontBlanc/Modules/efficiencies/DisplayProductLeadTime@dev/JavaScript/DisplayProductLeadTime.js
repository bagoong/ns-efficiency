define('DisplayProductLeadTime', [
    'Backbone.CompositeView',
    'ItemDetails.View',
    'DisplayProductLeadTime.Model',
    'DisplayProductLeadTime.View',
    'underscore'
], function DisplayProductLeadTime(
    CompositeView,
    ItemDetailsView,
    DisplayProductLeadTimeModel,
    DisplayProductLeadTimeView,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            ItemDetailsView.prototype.initialize =
                _.wrap(ItemDetailsView.prototype.initialize, function prototypeInitialize(fn) {
                    fn.apply(this, _.toArray(arguments).slice(1));

                    this.on('afterViewRender', function afterViewRender() {
                        if (!this.model.getStockInfo().isInStock) {
                            this.$el
                                .find('.item-details-info [data-view="Item.Stock"]')
                                .after('<div data-view="display.product.lead.time"></div>');
                        }
                    });
                });

            _.extend(ItemDetailsView.prototype.childViews, {
                'display.product.lead.time': function ChildViewDisplayProductLeadTime() {
                    return new DisplayProductLeadTimeView({
                        model: new DisplayProductLeadTimeModel(),
                        application: application,
                        id: this.model.get('_id')
                    });
                }
            });

        }
    };
});