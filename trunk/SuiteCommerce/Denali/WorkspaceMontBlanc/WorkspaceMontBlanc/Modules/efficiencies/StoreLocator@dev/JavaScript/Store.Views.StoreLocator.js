/**
 * Store.Views.StoreLocator.js
 * Store Locator view constructor, loads other region views.
 * View Dependencies : Store.Views.StoreLocator.Map, Store.Views.StoreLocator.List, Store.Views.StoreLocator.Search
 */
define('Store.Views.StoreLocator', [
    'Backbone',
    'Backbone.CompositeView',
    'StoreLocator.Configuration',
    'Store.Views.StoreLocator.Map',
    'Store.Views.StoreLocator.List',
    'Store.Views.StoreLocator.Search',
    'storelocator.tpl',
    'underscore'
], function StoreViewsStoreLocator(
    Backbone,
    CompositeView,
    Configuration,
    Map,
    List,
    Search,
    Template,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        attributes: {
            'id': 'store-locator',
            'class': 'view store-locator'
        },

        title: _('Store Locator').translate(),

        initialize: function initialize(data) {
            CompositeView.add(this);

            this.options        = data;
            this.application    = data.application;
            this.collection     = data.collection;
            this.configuration  = Configuration;
            this.eventBus       = _.extend({}, Backbone.Events);

            this.isInStoreStock = data.isInStoreStock && data.isInStoreStock || false;
            this.isInMyStore = data.isInMyStore && data.isInMyStore || false;

            this.itemId = data.itemId && data.itemId || 0;
            this.storeSearchText = data.storeSearchText;
        },

        getChildrenData: function getChildrenData() {
            return {
                application: this.application,
                configuration: this.configuration,
                collection: this.collection,
                eventBus: this.eventBus,
                hideTitle: (this.isInStoreStock || this.isInMyStore),
                itemId: this.itemId,
                isInStoreStock: this.isInStoreStock,
                isInMyStore: this.isInMyStore,
                storeSearchText: this.storeSearchText
            };
        },

        getBreadcrumbPages: function getBreadcrumbPages() {
            this.application.history = [{text: this.title, href: '/stores'}];
            return this.application.history;
        },

        childViews: {
            'StoreLocator.Form': function StoreLocatorForm() {
                return new Search(this.getChildrenData());
            },
            'StoreLocator.Map': function StoreLocatorMap() {
                return new Map(this.getChildrenData());
            },
            'StoreLocator.List': function StoreLocatorList() {
                return new List(this.getChildrenData());
            }
        },

        getContext: function getContext() {
            return {
                isInStoreStock: this.isInStoreStock && this.isInStoreStock
            };
        }
    });
});