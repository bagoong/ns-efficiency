define('GoogleMapsLoader', [
    'Session',
    'jQuery',
    'underscore',
    'Utils'
], function GoogleMapsLoader(
    Session,
    jQuery,
    _,
    Utils
) {
    'use strict';

    var loader = {
        loadedPromise: jQuery.Deferred(),
        initialized: false,
        loadScript: function loadScript(apiKey) {
            var url = '//maps.googleapis.com/maps/api/js?sensor=false&libraries=places&callback=_gMapsCallback';
            var language = (Session.get('language.locale') || '').split('_')[0];
            var region = ((Session.get('language.locale') || '_').split('_')[1]).toLowerCase();

            var params = {
                language: language,
                region: region
            };

            if (apiKey) {
                _.extend(params, {key: apiKey});
            }
            url = Utils.addParamsToUrl(url, params);


            if (!this.initialized) {
                if (SC.ENVIRONMENT.jsEnvironment === 'browser') {
                    jQuery.ajax({
                        url: url,
                        dataType: 'script',
                        // always call this script without appending timestamp params to the url
                        cache: true,
                        // Don't throw internal errors/404 because of third party integration
                        preventDefault: true
                    });
                } else {
                    this.loadedPromise.rejectWith('Google Maps is a Browser Script only');
                }

                this.initialized = true;
            }

            return this.loadedPromise;
        }
    };
    window._gMapsCallback = function _gMapsCallback() {
        loader.loadedPromise.resolve();
    };
    return loader;
});