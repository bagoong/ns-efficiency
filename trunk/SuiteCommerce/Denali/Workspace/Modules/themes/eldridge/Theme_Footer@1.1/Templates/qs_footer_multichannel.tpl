<div data-view="Global.BackToTop"></div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

<div class="footer-content">
	<div class="footer-content-nav">

		<ul class="footer-content-nav-list help">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".help .footer-collapsed-list" data-type="collapse">Help</a>
			</li>
			<li class="footer-collapsed-list"><a href="">{{translate 'FAQ'}}</a></li>
			<li>
                <a href="">{{translate 'Shipping & Handling'}}</a>
            </li>
			<li>
                <a href="">{{translate 'My Account'}}</a>
            </li>

            <li class="footer-collapsed-list">
                <a href="/guestorderstatus" data-touchpoint="home" data-hashtag="#/guestorderstatus">{{translate 'Order Status'}}</a>
            </li>
		</ul>

		<ul class="footer-content-nav-list privacy">
			<li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".privacy .footer-collapsed-list" data-type="collapse">Privacy & Terms</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="">{{translate 'Privacy'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="">{{translate 'Terms and Conditions'}}</a>
			</li>
            <li>
                <a href="">{{translate 'Return policy'}}</a>
            </li>

		</ul>

		<ul class="footer-content-nav-list about-us">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'About us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="">{{translate 'About us'}}</a>
			</li>

			<li class="footer-collapsed-list">
				<a href="/stores" data-touchpoint="home" data-hashtag="#/stores">{{translate 'Stores'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="">{{translate 'Press & More'}}</a>
			</li>
            <li class="footer-collapsed-list">
                <a href="">{{translate 'Site Map'}}</a>
            </li>
		</ul>

		<ul class="footer-content-nav-social">
			<li class="first">
				<a href="" class="first">{{translate 'Follow us'}}</a></li>
			<li>
				<ul class="footer-social">
					<li class="footer-social-item">
						<a class="" href="https://www.twitter.com/" target="_blank">
							<i class="footer-social-twitter-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-google-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-facebook-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-pinterest-icon"></i>
						</a>
					</li>
				</ul>
			</li>
		</ul>

		<ul class="footer-content-newsletter">
			<li class="first">{{translate 'Sign up for email updates'}}</li>
			<li data-view="Newsletter.SignUp"></li>
		</ul>

	</div>
	<div class="footer-content-bottom">
		<p class="footer-copyright">{{translate '&copy; 2008-2016 Company Name'}}</p>
	</div>

</div>
