define('BOPIS.MyAccount', [
    'BOPIS',
    'Profile.MyStore',
    'BOPIS.OrderHistory.Model',
    'BOPIS.OrderHistory.Details.View',
    'BOPIS.OrderHistory.ShippingGroup.View'

], function BOPISMyAccount(
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {

        }
    };
});