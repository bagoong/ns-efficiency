define('BOPIS.OrderHistory.ShippingGroup.View', [
    'OrderHistory.ShippingGroup.View',
    'PluginContainer',
    'underscore'
], function BOPISOrderHistoryShippingGroupView(
    View,
    PluginContainer,
    _
) {
    'use strict';

    View.prototype.preRenderPlugins =
        View.prototype.preRenderPlugins || new PluginContainer();

    View.prototype.preRenderPlugins.install({
        name: 'BOPIS.InventoryDisplay',
        execute: function execute($el /* , view */) {
            $el
               .find('.order-history-shipping-group-shipping-title')
               .html(
                    '<i class="order-history-shipping-icon"></i> ' + _.translate('Ship to Address')
                );
        }
    });
});