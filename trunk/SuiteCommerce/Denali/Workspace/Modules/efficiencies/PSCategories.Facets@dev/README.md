# Categories
A merchandiser can categorise items in the Netsuite backend using Lists > Website > Categories. This module provides the front end component, which exposes these categories in the SCA website so that they appear as a navigation hierarchy. Configurable category landing pages are also generated.

## Documentation
https://confluence.corp.netsuite.com/x/4Mr8Ag
