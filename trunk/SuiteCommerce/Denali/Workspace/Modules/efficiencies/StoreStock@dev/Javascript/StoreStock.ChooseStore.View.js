/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.ChooseStore.View', [
    'Backbone',
    'Profile.Model',
    'StoreStock.PreferredStore.Model',
    'store_stock_choose_store.tpl',
    'jQuery'
], function StoreStockChooseStoreView(
    Backbone,
    ProfileModel,
    PreferredStoreModel,
    storeStockChooseStoretpl,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockChooseStoretpl,

        events: {
            'click [data-action="save-store"]': 'savePreferredStore'
        },

        initialize: function initializeChooseStoreView() {
            this.profile_model = ProfileModel.getInstance();
            this.storeId = this.options.storeId;
        },

        savePreferredStore: function savePreferredStore() {
            var self = this;
            if (this.profile_model.get('isLoggedIn') === 'T') {
                PreferredStoreModel.getInstance().save({storeid: this.storeId}).done(function done() {
                    self.afterSavePreferredStore();
                });
            } else {
                this.afterSavePreferredStore();
            }
        },

        afterSavePreferredStore: function afterSavePreferredStore() {
            if ( this.options.application.getLayout().$containerModal ) {
                this.options.application.getLayout().$containerModal.modal('hide');
            }

            this.profile_model.set('preferredStoreID', this.storeId);
            jQuery.cookie('preferredStoreID', this.storeId);
        }
    });
});