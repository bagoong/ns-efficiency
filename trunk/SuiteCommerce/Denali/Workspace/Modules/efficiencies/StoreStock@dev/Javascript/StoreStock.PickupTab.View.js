/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.PickupTab.View', [
    'Backbone',

    'Store.Collection',
    'StoreStock.Collection',
    'Store.Bootstrapped.Collection',

    'StoreStock.PreferredStore.Model',
    'StoreStock.PreferredStoreStatus.Model',

    'StoreStock.Status.View',
    'StoreStock.ChooseStore.View',
    'Store.Views.StoreLocator',
    'Profile.Model',

    'store_stock_tabcontent_pickup.tpl',

    'jQuery',
    'underscore'
], function StoreStockPickupTabView(
    Backbone,

    StoreCollection,
    StoreStockCollection,
    StoreBootstrappedCollection,

    PreferredStoreModel,
    PreferredStoreStatusModel,

    StoreStockStatusView,
    StoreStockChooseStoreView,
    StoreLocatorView,
    ProfileModel,

    storeStockPickupTabTpl,

    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: storeStockPickupTabTpl,

        events: {
            'click [data-action="show-storelocator"]': 'showStoreLocator',
            'keyup [name="store-stock-store"]': 'keyupStoreStockSearch'
        },

        initialize: function initialize() {
            var syncPreferredStore = false;
            this.itemModel = this.model;

            this.collectionStoreStock = new StoreStockCollection();
            this.profile_model = ProfileModel.getInstance();

            if ( jQuery.cookie('preferredStoreID') &&
               (this.profile_model.get('preferredStoreID') !== jQuery.cookie('preferredStoreID') )
            ) {
                this.profile_model.set('preferredStoreID', jQuery.cookie('preferredStoreID'));
                if (this.profile_model.get('isLoggedIn') === 'T') {
                    syncPreferredStore = true;
                }
            }

            this.preferredStore = StoreBootstrappedCollection
                                    .getInstance()
                                    .findWhere({internalid: this.profile_model.get('preferredStoreID')});

            this.preferredStoreModel = this.preferredStore ?
                                        new PreferredStoreModel(this.preferredStore.attributes) :
                                        new PreferredStoreModel();

            if(syncPreferredStore){
                this.preferredStoreModel.save({storeid: this.profile_model.get('preferredStoreID')});
            }

            this.listenTo(this.profile_model, 'change:preferredStoreID', this.prefferedStoreChange);
        },

        render: function renderView() {
            var self = this;

            this._render();

            if(this.preferredStore){
                this.preferredStoreStatusModel = new PreferredStoreStatusModel();
                this.preferredStoreStatusModel.fetch({
                    data: {
                        id: this.itemModel.id,
                        stores: this.preferredStore.get('location')
                    }
                }).success(function success() {
                    self.preferredStoreStockStatusView = new StoreStockStatusView({
                        model: self.preferredStoreStatusModel,
                        application: self.options.application,
                        preferredStore: self.preferredStore.get('name')
                    });

                    self.preferredStoreStockStatusView
                        .setElement('*[data-view="StoreStock.PreferredStoreStockStatus.View"]').render();
                });
            }
        },

        getContext: function getContext() {
            return {
                isSelectionComplete: this.itemModel.isSelectionComplete(),
                storeStockStoreSearchText: jQuery.cookie('storeStockStoreSearchText'),
                preferredStore: this.preferredStore,
                isPickupTabActive: this.isPickupTabActive
            };
        },

        keyupStoreStockSearch: function keyupStoreStockSearch(e) {
            if (e.which === 13) {
                this.showStoreLocator();
            }
        },

        showStoreLocator: function showStoreLocator(e) {
            var showIn = $(e.target).data('showin');
            var storeStockStoreSearchText = this.$('[name=store-stock-store]').val();
            var storeLocatorView = new StoreLocatorView({
                application: this.options.application,
                collection: new StoreCollection(null),
                isInStoreStock: true,
                itemId: this.itemModel.id,
                storeSearchText: storeStockStoreSearchText
            });

            this.listenTo(storeLocatorView.collection, 'collectionRendered', _.bind(this.fetchStoreStockDetail, this));
            this.listenTo(storeLocatorView.eventBus, 'infoWindowRendered',
                _.bind( function appendChooseStoreButton(storeId) {
                    var chooseStoreView = new StoreStockChooseStoreView({
                        storeId: storeId,
                        application: this.options.application
                    });
                    chooseStoreView.setElement('#bot-cont').render();

                }, this));

            jQuery.cookie('storeStockStoreSearchText', storeStockStoreSearchText);

            storeLocatorView.title = _('Choose Store').translate();
            storeLocatorView.modalClass = 'store-stock-modal-storelocator';

            if ( showIn === 'modal') {
                this.options.application.getLayout().showInModal(storeLocatorView);
            } else if ( showIn === 'pushpane') {
                storeLocatorView.setElement('*[data-view="SC.PUSHER.MAP"]').render();
            }
        },

        fetchStoreStockDetail: function fetchStoreStockDetail(storeCollection) {
            var self = this;
            var storeStockStatusView;
            var parentElement;

            var storeLocationIDs = storeCollection.map(function mapStoreLocationIds(store) {
                return store.get('location');
            });

            this.collectionStoreStock.fetch({
                data: {
                    id: this.itemModel.id,
                    stores: _.compact(storeLocationIDs).join(',')
                }
            }).success(function success() {
                self.collectionStoreStock.each(function appendToStoreLocator(modelStoreStock) {
                    storeStockStatusView = new StoreStockStatusView({
                        model: modelStoreStock,
                        application: self.options.application
                    });

                    parentElement = '*[data-view="StoreStock.Status.View'+modelStoreStock.get('locationId')+'"]';
                    storeStockStatusView.setElement(parentElement).render();
                });
            });
        },

        prefferedStoreChange: function prefferedStoreChange() {
            this.preferredStore = StoreBootstrappedCollection.getInstance().findWhere({internalid: this.profile_model.get('preferredStoreID')});
            this.render();
        }

    });
});