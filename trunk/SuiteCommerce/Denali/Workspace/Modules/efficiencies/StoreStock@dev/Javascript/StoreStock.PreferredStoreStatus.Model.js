/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock.PreferredStoreStatus.Model', [
    'Backbone.CachedModel',
    'jQuery',
    'underscore',
    'Utils',
], function StoreStockPreferredStoreModel(
    BackboneCachedModel,
    jQuery,
    _,
    Utils
) {
    'use strict';

    return BackboneCachedModel.extend({
        url: _.getAbsoluteUrl('services/StoreStock.Status.Service.ss'),

        parse: function parseResponse(response) {
            var status;

            if ( response.length > 0 ) {
                status = response[0];
            }

            return status;
        }
    });
});