/*
 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module StoreStock
define('StoreStock', [
    'ItemDetails.View',
    'StoreStock.View',
    'StoreStock.Configuration',
    'underscore'
], function StoreStock(
    ItemDetailsView,
    StoreStockView,
    StoreStockConfiguration,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            ItemDetailsView.prototype.initialize =
                _.wrap(ItemDetailsView.prototype.initialize, function wrapInitialize(fn) {
                    fn.apply(this, _.toArray(arguments).slice(1));

                    this.application.Configuration.storeStock = StoreStockConfiguration;

                    _.extend(ItemDetailsView.prototype.childViews, {
                        'Item.StoreStock': function childViewItemStoreStock() {
                            return new StoreStockView({
                                model: this.model,
                                application: this.application
                            });
                        }
                    });

                    this.on('afterViewRender', function afterViewRender() {
                        var inventoryIndex;

                        if ( this.model ) {
                            inventoryIndex = _.indexOf(StoreStockConfiguration.itemTypesWithInvt,
                                            this.model.get('_itemType'));

                            if ( inventoryIndex >= 0 ) {
                                this.$el
                                    .find('.item-details-main [class="item-details-options"]')
                                    .after('<div data-view="Item.StoreStock"></div>');


                                var pushercontainer = '<div class="example-pusher" data-action="pushable" data-id="pushable-map"> \
                                    <div data-view="SC.PUSHER.MAP"></div>\
                                </div>';

                                this.$el
                                    .find('.item-details')
                                    .after(pushercontainer);
                            }
                        }
                    });
                });
        }
    };
});