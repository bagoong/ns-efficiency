{{#if hasModel}}
    {{#if isDisplayAsBadge }}
        <span class="{{badgeClass}}">{{ translate badgeText }}</span>
    {{else}}
        <span>
        {{#if isValuePreferredStock }}
            {{ translate "More than $(0) items In Stock" value }}
        {{else}}
            {{ translate "$(0) items In Stock" value }}
        {{/if}}
        </span>
    {{/if}}
{{/if}}
{{#if preferredStore}}
    {{translate 'at'}} {{ preferredStore }}
{{/if}}