// @module Profile.MyStore
define('Profile.MyStore.MyAccount.View', [
    'Backbone',
    'Backbone.CompositeView',
    'PluginContainer',

    'Profile.MyStore.Model',

    'Store.View.StoreDetail',
    'Profile.MyStore.StoreLocator.View',

    'profile_mystore_myaccount.tpl',

    'underscore'
], function ProfileMyStoreMyAccountView(
    Backbone,
    BackboneCompositeView,
    PluginContainer,

    MyStore,

    StoreDetailView,
    ProfileMyStoreStoreLocatorView,

    myStoreHeaderTpl,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: myStoreHeaderTpl,

        store: null,

        childViews: {
            'Profile.MyStore.Detail': function ProfileMyStoreChildView() {
                return new StoreDetailView({
                    application: this.application,
                    model: this.model,
                    isInMyStore: true
                });
            }
        },

        initialize: function initialize() {
            var self = this;
            BackboneCompositeView.add(this);

            this.model = MyStore.getInstance();
            this.application = this.options.application;

            if ( ! StoreDetailView.prototype.childViews['Profile.MyStore.StoreLocator'] ) {
                _.extend(StoreDetailView.prototype.childViews, {
                    'Profile.MyStore.StoreLocator': function ProfileMyStoreChildView() {
                        return new ProfileMyStoreStoreLocatorView({
                            application: self.application,
                            model: self.model
                        });
                    }
                });

                StoreDetailView.prototype.preRenderPlugins =
                StoreDetailView.prototype.preRenderPlugins || new PluginContainer();

                StoreDetailView.prototype.preRenderPlugins.install({
                    name: 'Profile.MyStore.Detail',
                    execute: function execute($el) {
                        $el
                            .find('.panel-title-store')
                            .after('<div data-view="Profile.MyStore.StoreLocator"></div>');
                    }
                });
            }

            this.listenTo(this.model, 'sync', jQuery.proxy(this.render));
        },

        getBreadcrumbPages: function getBreadcrumbPages() {
            return [{text: 'My Store', href: '/mystore'}];
        },

        getSelectedMenu: function getSelectedMenu() {
            return 'mystore';
        }

    });
});