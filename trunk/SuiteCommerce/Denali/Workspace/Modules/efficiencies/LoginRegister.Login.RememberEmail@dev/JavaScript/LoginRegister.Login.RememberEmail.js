define('LoginRegister.Login.RememberEmail', [
    'SC.Configuration',
    'LoginRegister.Login.View',
    'underscore',
    'jQuery',
    'jquery.cookie'
], function LoginRegisterLoginRememberEmail(
    Configuration,
    View,
    _,
    jQuery
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            if (Configuration.rememberEmailOnLoginPage) {
                View.prototype.initialize = _.wrap(View.prototype.initialize, function wrapLoginRegister(fn) {
                    var self = this;
                    fn.apply(this, _.toArray(arguments).slice(1));

                    this.application.getLayout().once('afterAppendView', function afterAppendView() {
                        var email = jQuery.cookie('StoreEmail');
                        if (email && email !== '') {
                            self.$('#login-email').val(email);
                        }
                    });
                });
            }
        }
    }
});