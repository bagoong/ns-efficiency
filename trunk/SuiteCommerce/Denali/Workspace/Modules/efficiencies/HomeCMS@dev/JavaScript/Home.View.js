define('HomeCMS.View',	[
    'SC.Configuration',
    'home_cms_ef.tpl',
    'Backbone',
    'jQuery',
    'underscore',
    'Utils'
], function HomeView(
    Configuration,
    homeTpl,
    Backbone,
    jQuery,
    _
) {
    'use strict';


    return Backbone.View.extend({

        template: homeTpl,
        attributes: {
            'id': 'home-page',
            'class': 'home-page'
        },
        initialize: function initialize() {
            var initBXSliderFn;
            var windowResizeHandlerFn;

            this.title = this.options.application.getConfig('siteSettings.displayname') ||
                _('Welcome to the store').translate();
            this.page_header = this.options.application.getConfig('siteSettings.displayname') ||
                _('Welcome to the store').translate();

            this.windowWidth = jQuery(window).width();

            initBXSliderFn = function initBXSliderFnName() {
                var $slider = this.$('[data-slider][data-slider-init!=true]');
                _.initBxSlider($slider, {
                    // http://bxslider.com/options
                    nextText: '<a class="home-gallery-next-icon"></a>',
                    prevText: '<a class="home-gallery-prev-icon"></a>',
                    slideMargin: 0,
                    auto: true,
                    pause: 10000,
                    useCSS: true,
                    maxSlides: 1,
                    preloadImages: 'visible',
                    controls: true,
                    mode: 'fade'
                });
                $slider.attr('data-slider-init', 'true');
            };

            windowResizeHandlerFn = _.throttle(function winResizeHandlerThrottled() {
                if (_.getDeviceType(this.windowWidth) === _.getDeviceType(jQuery(window).width())) {
                    return;
                }
                this.showContent();
                _.resetViewportWidth();
                this.windowWidth = jQuery(window).width();
            }, 1000);

            this._windowResizeHandler = _.bind(windowResizeHandlerFn, this);
            this._initBXSlider = _.bind(initBXSliderFn, this);

            jQuery(window).on('resize', this._windowResizeHandler);


            this.listenTo(
                typeof CMS !== 'undefined' ? CMS : Backbone.Events,
                'cms:rendered', this._initBXSlider
            );
        },
        remove: function remove() {
            this.stopListening();
            jQuery(window).off('resize', this._windowResizeHandler);
        }
    });
});
