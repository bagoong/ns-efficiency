define('BOPIS.OrderWizard.Module.CartItemsForShipping', [
    'BOPIS.OrderWizard.Module.CartItemsBase'
], function BOPISOrderWizardModuleCartItemsForShipping(
    CartItemsBase
) {
    'use strict';

    return CartItemsBase.extend({
        cartItemsTitle: 'Shipping',
        accordionID: 'bopis-accordion-cart-item-shipping',
        getLineCollection: function getCollection() {
            return this.wizard.model.getLinesForShipping();
        }
    });
});