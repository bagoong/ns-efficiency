define('HomepagePopup.View', [
    'SC.Configuration',

    'homepage_popup.tpl',

    'jQuery',
    'Backbone',
    'underscore'
], function HomepagePopupView(
    Configuration,

    homepagePopupTpl,

    jQuery,
    Backbone,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: homepagePopupTpl,
        attributes: {
            'id': 'homepage',
            'class': 'view homepage'
        },
        events: {
            'change select[id="in-modal-setlanguage"]': 'selectLanguage'
        },

        selectLanguage: function selectLanguage(e) {
            var $element = jQuery(e.target);
            var locale = $element.find(':selected').data('locale');
            var singleCurrency = $element.find(':selected').data('singlecurrency');
            if (locale && singleCurrency) {
                // selected language only has one currency
                console.log('Redirect to: ' + e.target.value + '&lang=' + locale + '&cur=' + singleCurrency);
            } else if (locale && !singleCurrency) {
                // selected language has multiple currencies so show second dropdown
                console.log('Load the currencies dropdown');
                this.showCurrency(e.target.value, locale, $element.find(':selected').text());
            }
        },
        showCurrency: function showCurrency(host, locale, title) {
            // find the currencies that match the language
            var hosts = SC.ENVIRONMENT.availableHosts;
            var selectedLocale;
            var selectedCurrencies = {};
            jQuery.each( hosts, function( i, val ) {
                jQuery.each( val, function( key, obj ) {
                    // find the languages object
                    if (_.where(obj, {'host': host, 'locale': locale, 'title': title}).length) {
                        selectedLocale = locale;
                        selectedCurrencies = val.currencies;
                    }
                });
            });
            this.selectCurrency(selectedLocale, selectedCurrencies);
        },
        selectCurrency: function selectCurrency(locale, currencies) {
            // function to build the currencies dropdown
            var select = this.$('#in-modal-setcurrency');
            jQuery.each( currencies, function( i, val ) {
                // loop through the currencies object to populate the 'Currencies' dropdown
                select.append(jQuery('<option>', {
                    value: val.code,
                    text: val.title
                }));
            });
            // then use the locale and selected currency code to build up the redirect url
            // this is as far as I got
        },
        getContext: function getContext() {
            var environment = SC.ENVIRONMENT;
            var isHomeTouchpoint = Configuration.currentTouchpoint === 'home';
            var useParameter = !isHomeTouchpoint;
            var currentHost = isHomeTouchpoint ? environment.currentHostString : environment.currentLanguage.locale;
            var availableHosts = _.map(environment.availableHosts, function(host) {
                return {
                    // @property {Boolean} hasLanguages
                    hasLanguages: host.languages && host.languages.length,
                    // @property {Boolean} hasCurrencies
                    hasCurrencies: host.currencies && host.currencies.length,
                    // @property {String} title
                    title: host.title,
                    languages: _.map(host.languages, function(language) {
                        return {
                            // @property {Boolean} hasSingleCurrency
                            hasSingleCurrency: host.currencies && host.currencies.length === 1,
                            // @property {String} singleCurrency
                            singleCurrency: host.currencies[0].code,
                            // @property {Boolean} hasMultipleCurrencies
                            hasMultipleCurrencies: host.currencies && host.currencies.length > 1,
                            // @property {String} host
                            host: language.host,
                            // @property {String} locale
                            locale: language.locale,
                            // @property {String} title
                            displayName: language.title,
                            // @property {Boolean} isSelected
                            isSelected: !!((useParameter && language.locale === currentHost) ||  language.host === currentHost)
                        };
                    })
                };
            });
            return {
                showHosts: !!(availableHosts && availableHosts.length > 1),
                availableHosts: availableHosts,
                currentHost: currentHost,
                useParameter: useParameter
            };
        }
    });
});