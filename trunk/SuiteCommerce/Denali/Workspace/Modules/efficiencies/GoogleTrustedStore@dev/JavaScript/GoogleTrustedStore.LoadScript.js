/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('GoogleTrustedStore.LoadScript', [
    'jQuery'
], function GoogleTrustedStoreLoadScript(jQuery) {
    'use strict';

    var ids = '#gts-comm, #gts-c, script[src^="//www.gstatic.com/trustedstores"]';

    var GoogleTrustedStoreScript = {
        loadScript: function loadScript() {
            var script;
            var firstScriptTag;
            var dirtyDestroy;

            if (SC.ENVIRONMENT.jsEnvironment === 'browser') {
                Object.defineProperty(window, 'GoogleTrustedStore', {value: undefined});

                jQuery('#gtsScript').remove();
                dirtyDestroy = jQuery(ids);
                [].slice.call(dirtyDestroy, 0).forEach(function(el) {
                    el.parentNode.removeChild(el);
                });

                script = document.createElement('script');
                script.type = 'text/javascript';
                script.async = true;
                script.id = 'gtsScript';
                script.src = 'https://www.googlecommerce.com/trustedstores/api/js';

                firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
            }
        }
    };

    return GoogleTrustedStoreScript;
});