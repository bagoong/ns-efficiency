/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('GoogleTrustedStore.OrderWizard.Steps', [
    'SC.Checkout.Configuration',
    'GoogleTrustedStore.OrderWizard.View',
    'underscore'
], function CheckoutFileUpload(SCCheckOutConfiguration, OrderWizardModuleGoogleTrustedStore, _) {
    'use strict';

    var stepSize = _.size(SCCheckOutConfiguration.checkoutSteps) - 1;

    SCCheckOutConfiguration.checkoutSteps[
       stepSize
    ].steps[
       1
    ].modules.push([
        OrderWizardModuleGoogleTrustedStore
    ]);
});