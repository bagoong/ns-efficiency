/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
define('GoogleTrustedStore.Configuration', [
    'Configuration',
    'underscore'
], function GoogleTrustedStoreConfiguration(Configuration, _) {
    'use strict';

    var googleTrustedStoreConfiguration = {
        // The estimated date on which you will ship the order; this is different from the estimated delivery date.
        estShipDate: 7,
        // The estimated date on which you expect delivery of the order to the customer.
        estDeliveryDate: 7,
        gts: {
            /*
            *   id : This field must be populated with the client's Google Trusted Stores.
            *   Required : True
            */
            id: '643469',
            /*
            *   badge_position :
            *   The default value for this variable is BOTTOM_RIGHT.
            *   Change this value to BOTTOM_LEFT if you would like the badge to float in the bottom left corner instead.
            *   You may also set this value to USER_DEFINED to specify a non-floating location for the badge on a given
            *   web page. If you choose USER_DEFINED, then this requires defining a new container element as explained
            *   below (see badge_container)
            *   Required : True
            */
            badge_position: 'BOTTOM_RIGHT'
            /*
            *   badge_container: An HTML element ID which you would like the Trusted Stores Bage to be injected into.
            *   You can then use CSS to position the badge. It can be placed anywhere within the <body> tag.
            *   Required : if badge_position === USER_DEFINED
            */
            // badge_container: 'ID of the container'
            /*
            *   google_base_subaccount_id : Account ID from Google Merchant Center. This value should match the account
            *   ID you use to submit your product data feed to Google.
            *   Required: Optional
            */
            // google_base_subaccount_id: '123456'
        }
    };

    _.extend(googleTrustedStoreConfiguration, {
        get: function get() {
            return this;
        }
    });

    Configuration.publish.push({
        key: 'GoogleTrustedStore_config',
        model: 'GoogleTrustedStore.Configuration',
        call: 'get'
    });

    return googleTrustedStoreConfiguration;
});


