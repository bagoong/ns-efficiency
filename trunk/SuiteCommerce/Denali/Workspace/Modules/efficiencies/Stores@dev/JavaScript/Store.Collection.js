define('Store.Collection', [
    'Store.Model',
    'Backbone.CachedCollection',
    'underscore',
    'Utils'
], function StoreCollection(
    Model,
    CachedCollection,
    _
) {
    'use strict';

    return CachedCollection.extend({
        initialize: function initialize(options) {
            this.options = options;
        },
        url: function() {
            var str;
            var self = this;
            var base = _.getAbsoluteUrl('services/Store.Service.ss');

            if (!_.isNull(this.options) && _.isObject(this.options)) {
                str = Object.keys(this.options).map(function(key) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent(self.options[key]);
                }).join('&');

                return base + '?' + str;
            }

            return base;
        },
        model: Model,
        sortByNearest: function sortByNearest(point) {
            var k;
            this.sortByDistanceCache = this.sortByDistanceCache || {};

            k = point.lat() + '-' + point.lng();

            if (!this.sortByDistanceCache[k]) {
                delete this.sortByDistanceCache;
                this.sortByDistanceCache = {}; // Limit cache to last point in a quick way

                this.sortByDistanceCache[k] = this.sortBy(function sorting(a) {
                    return a.distanceTo(point);
                });
            }

            return this.sortByDistanceCache[k];
        }
    });
});
