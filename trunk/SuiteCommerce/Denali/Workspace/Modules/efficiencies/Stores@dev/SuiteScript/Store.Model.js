define('Store.Model', [
    'SC.Model',
    'SearchHelper',
    'underscore'
], function StoreModel(
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    // @class Store.Model
    // @extends SCModel
    return SCModel.extend({
        name: 'Store',
        record: 'customrecord_ef_st_store',
        fieldsets: {
            basic: [
                'internalid',
                'name',
                'location',
                'urlcomponent',
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'lat',
                'lon',
                'phone',
                'zipcode',
                'thumbnail',
                'marker',
                'shortDescription',
                'distance',
                'openingHours'
            ],
            details: [
                'internalid',
                'name',
                'location',
                'urlcomponent',
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'lat',
                'lon',
                'phone',
                'zipcode',
                'mainImage',
                'thumbnail',
                'marker',
                'shortDescription',
                'longDescription',
                'openingHours'
            ]
        },
        filters: [
            {fieldName: 'isinactive', operator: 'is', value1: 'F'}
        ],
        columns: {
            internalid: {fieldName: 'internalid'},
            name: {fieldName: 'name'},
            location: {fieldName: 'custrecord_ef_st_s_location'},
            urlcomponent: {fieldName: 'custrecord_ef_st_s_urlcomponent'},
            address1: {fieldName: 'custrecord_ef_st_s_address1'},
            address2: {fieldName: 'custrecord_ef_st_s_address2'},
            city: {fieldName: 'custrecord_ef_st_s_city'},
            state: {fieldName: 'custrecord_ef_st_s_state', type: 'text'},
            country: {fieldName: 'custrecord_ef_st_s_country', type: 'text'},
            lat: {fieldName: 'custrecord_ef_st_s_latitude'},
            lon: {fieldName: 'custrecord_ef_st_s_longitude'},
            phone: {fieldName: 'custrecord_ef_st_s_phone'},
            zipcode: {fieldName: 'custrecord_ef_st_s_zipcode'},
            mainImage: {fieldName: 'custrecord_ef_st_s_main_image', type: 'object'},
            thumbnail: {fieldName: 'custrecord_ef_st_s_thumbnail_image', type: 'object'},
            marker: {fieldName: 'custrecord_ef_st_s_marker_image', type: 'object'},
            shortDescription: {fieldName: 'custrecord_ef_st_s_short_description'},
            longDescription: {fieldName: 'custrecord_ef_st_s_short_description'},
            openingHours: {fieldName: 'custrecord_ef_st_s_openinghours'}
        },
        filtersForPurchases: function filtersForPurchases() {
            var filtersArr = [];
            filtersArr.push({fieldName: 'custrecord_ef_st_s_location', operator: 'noneof', value1: '@NONE@'});
            filtersArr.push({
                fieldName: 'makeinventoryavailablestore',
                joinKey: 'custrecord_ef_st_s_location',
                operator: 'is',
                value1: 'T'
            });

            return filtersArr;
        },
        detail: function detail(filter, isForPurchases) {
            var filters = _.clone(this.filters);

            if(isForPurchases){
                filters = filters.concat(this.filtersForPurchases());
            }
        },
        listStoresForPurchases: function listStoresForPurchases() {
            return this.list(true);
        },
        list: function list(isForPurchases) {
            var Search;
            var filters = _.clone(this.filters);
            if(isForPurchases){
                filters = filters.concat(this.filtersForPurchases());
            }

            Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.basic).search();
            return Search.getResults();
        },
        nearestStore: function nearestStore(latitude, longitude, limit, isForPurchases, radius) {
            var lat = latitude * Math.PI / 180;
            var lon = longitude * Math.PI / 180;
            var filters = _.clone(this.filters);
            var PI = Math.PI;
            var R = 6371;
            var Search;

            var formula = R +
            ' * (2 * ATAN2(SQRT((SIN((' + lat + '- ({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) / 2) *' +
            'SIN((' + lat + '- ({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) / 2) + ' +
            'COS(({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) * COS(' + lat + ') *' +
            'SIN((' + lon + '- ({custrecord_ef_st_s_longitude} * ' + PI + ' / 180)) /2) *' +
            'SIN((' + lon + '- ({custrecord_ef_st_s_longitude} * ' + PI + ' / 180)) /2))),' +
            'SQRT(1 - (SIN((' + lat + '- ({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) / 2) *' +
            'SIN((' + lat + '- ({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) / 2) +' +
            'COS(({custrecord_ef_st_s_latitude} * ' + PI + ' / 180)) * COS(' + lat + ') * ' +
            'SIN((' + lon + '- ({custrecord_ef_st_s_longitude} * ' + PI + ' / 180)) /2) * ' +
            'SIN((' + lon + '- ({custrecord_ef_st_s_longitude} * ' + PI + ' / 180)) /2)))))';

            filters.push({
                fieldName: 'formulanumeric',
                operator: 'lessthan',
                value1: radius,
                formula: formula
            });

            this.columns.distance = {
                fieldName: 'formulanumeric',
                formula: formula
            };


            if(isForPurchases){
                filters = filters.concat(this.filtersForPurchases());
            }

            Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.basic);
            Search.setSort('distance');
            Search.setSortOrder('asc');

            return Search.searchRange(0, limit).getResults();
        },
        getByUrlcomponent: function getByUrlcomponent(urlcomponent, isForPurchases) {
            var Search;
            var store;
            var filters = _.clone(this.filters);

            if (urlcomponent) {
                if(isForPurchases){
                    filters = filters.concat(this.filtersForPurchases());
                }

                Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.details);

                Search.addFilter({
                    fieldName: this.columns.urlcomponent.fieldName,
                    operator: 'is',
                    value1: urlcomponent
                });
                Search.search();
                store = Search.getResult();

                if (!store) {
                    throw notFoundError;
                }
            } else {
                throw notFoundError;
            }

            return store;
        }
    });
});

