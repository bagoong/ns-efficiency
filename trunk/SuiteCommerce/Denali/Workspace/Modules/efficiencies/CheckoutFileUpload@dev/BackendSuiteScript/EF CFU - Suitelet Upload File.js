define('EF CFU - Suitelet Upload File', [
    'Application',
    'underscore',
    'CheckoutFileUpload.Configuration'
], function EFCFUSuiteletUploadFile(
    Application,
    _,
    Configuration
) {
    'use strict';

    var getNewFileName = function getNewFileName(file) {
        var name = file.getName();
        var type = name.substring(name.lastIndexOf('.'), name.length);
        return nlapiGetUser() + '_' +
            new Date().getMilliseconds().toString() + parseInt(Math.random() * 10000000, 10).toString() + type;
    };

    var main = function main(request) {
        var fileObject = request.getFile('file');
        var internalid;
        var fileId;
        var fileUrl;
        var field;
        var oldName;
        var returnData;

        try {
            fileObject = request.getFile('file');

            if (!fileObject) {
                throw nlapiCreateError('ERR_FILE_NOT_PRESENT', 'No file uploaded');
            }

            internalid = request.getParameter('internalid');

            // Only for logged in users
            if (!parseInt(nlapiGetUser(), 10)) {
                throw unauthorizedError;
            }

            // no field definition
            field = _.findWhere(Configuration.fields, {
                internalid: internalid
            });

            if (!field) {
                throw nlapiCreateError('ERR_FILE_ID_UNRECOGNIZED', 'Field uploaded is unrecognized');
            }

            if (field.sizeLimit) {
                if (fileObject.getSize() > field.sizeLimit) {
                    throw nlapiCreateError('ERR_FILE_SIZE', 'File size limit exceeded');
                }
            }

            oldName = fileObject.getName();

            fileObject.setName(getNewFileName(fileObject));
            fileObject.setFolder(Configuration.tempUploadFolderId);

            if (_.isArray(field.allowTypes) && field.allowTypes.length > 0) {
                if (!_.contains(field.allowTypes, fileObject.getType())) {
                    throw nlapiCreateError('ERR_FILE_TYPE', 'File type not allowed');
                }
            }

            try {
                fileId = nlapiSubmitFile(fileObject);

                if (fileId) {
                    fileUrl = nlapiLookupField('file', fileId.toString(), 'url');
                }
            } catch (e) {
                if (e instanceof nlobjError) {
                    console.error(e.getCode(), e.getDetails());
                }

                throw nlapiCreateError('ERR_FILE_UPLOADED', 'Error ocurred while uploading');
            }

            returnData = {
                internalid: field.internalid,
                file: fileId.toString(),
                link: fileUrl,
                name: oldName
            };

            Application.sendContent(_.extend(returnData));
        } catch (e) {
            Application.sendError(e);
        }
    };

    return {
        main: main
    };
});