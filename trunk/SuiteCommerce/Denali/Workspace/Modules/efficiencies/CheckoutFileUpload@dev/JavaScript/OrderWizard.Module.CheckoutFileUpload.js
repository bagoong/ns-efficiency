define('OrderWizard.Module.CheckoutFileUpload', [
    'Wizard.Module',
    'orderwizard_module_checkoutfileupload.tpl',
    'GlobalViews.Message.View',

    'Backbone',
    'underscore',
    'jQuery',
    'Utils',
    'jquery.form'

], function OrderWizardModuleCheckoutFileUpload(
        WizardModule,
        CheckOutFileUploadTpl,

        GlobalViewsMessageView,

        Backbone,
        _,
        jQuery

) {
    'use strict';

    return WizardModule.extend({

        template: CheckOutFileUploadTpl,
        fileRequiredMessage: _.translate('File is mandatory'),
        events: {
            'submit [data-action="upload-form"]': 'uploadAction',
            'click [data-action="delete-file"]': 'deleteFile',
            'click [data-action="preView-file"]': 'preViewFile'
        },

        errors: [
            'ERR_FILE_ID_UNRECOGNIZED',
            'ERR_FILE_SIZE',
            'ERR_FILE_MISSING',
            'ERR_FILE_TYPE',
            'ERR_FILE_UPLOAD_PROCESS',
            'ERR_FILE_SEC'
        ],

        initialize: function initialize(options) {
            this.wizard = options.wizard;
            this.step = options.step;
            this.model = options.wizard.model;
            this.options = options;

            this.options.field = _.findWhere(
                SC.ENVIRONMENT.published.CheckoutFileUpload_config.fields,
                {internalid: options.fieldid }
            );
        },

        getFile: function getFile() {
            return this.model.get('files').findWhere({internalid: this.options.field.internalid});
        },

        deleteFile: function deleteFile(e) {
            var self;
            var internalid;
            var file;

            internalid = jQuery(e.target).data('internalid');
            file = this.model.get('files').findWhere({internalid: internalid});

            self = this;

            this.model.deleteFile(file, {
                success: function success() {
                    self.render();
                }
            });
        },

        submit: function submit() {
            var self = this;
            var promise;

            promise = jQuery.Deferred();

            if (this.getFile() || !jQuery('#file' + this.options.field.internalid).val()) {
                return this.isValid();
            }

            if (self.validateFileTypes()) {
                this.upload('[data-action="upload-form"]', {
                    success: function success() {
                        self.render();
                        promise.resolve();
                    }
                });
            }
            return promise;
        },

        validateFileTypes: function validateFileTypes() {
            var promise;
            var allowTypesExtensions;
            var strAllowTypesExtensions;
            var i;
            var val;
            var regex;
            var msg;

            strAllowTypesExtensions = '';
            allowTypesExtensions = this.options.field.allowTypesExtensions;

            for (i = 0; i <= (allowTypesExtensions.length - 1); i++) {
                strAllowTypesExtensions = strAllowTypesExtensions + '|' + allowTypesExtensions[i];
            }

            promise = jQuery.Deferred();
            strAllowTypesExtensions = strAllowTypesExtensions.slice(1);


            val = jQuery('#file' + this.options.field.internalid ).val().toLowerCase();
            regex = new RegExp('(.*?)\.(' + strAllowTypesExtensions + ')$');

            if (!(regex.test(val))) {
                msg = _.translate('File type not allowed');
                this.uploadStatus('error', msg);
                return promise.reject();
            }

            return promise.resolve();
        },

        isValid: function isValid() {
            var promise;
            var fileModel;
            promise = jQuery.Deferred();
            fileModel = this.getFile();

            if (!!fileModel || !this.options.field.required) {
                promise.resolve();
            } else {
                this.uploadStatus('error', this.options.field.requiredMessage);
            }

            return promise;
        },

        uploadAction: function uploadAction(e) {
            var self;
            var err;


            e.preventDefault();
            self = this;

            if (this.getFile() || jQuery('#file' + this.options.field.internalid).val()) {
                if (self.validateFileTypes()) {
                    this.upload(e.target, {
                        success: function success() {
                            self.render();
                        }
                    });
                }
            } else {
                err = _.translate('No file uploaded');
                this.uploadStatus('error', err);
            }
        },

        getContext: function getContext() {
            return {
                file: this.model.get('files').findWhere({internalid: this.options.field.internalid}),
                lblSubmit: _.translate('Upload'),
                lblFile: _.translate('Upload ' + this.options.field.name),
                lblRequire: (this.options.field.required) ? _.translate('Required') : '',
                lbldelete: _.translate('Remove'),
                internalid: this.options.field.internalid
            };
        },

        preViewFile: function preViewFile() {
            var baseUrl;
            var fileUrl;
            fileUrl = this.model.get('files').findWhere({internalid: this.options.field.internalid}).attributes.link;
            baseUrl = window.location.origin;
            this.$('#preView-file').attr('href', baseUrl + fileUrl);
        },

        uploadStatus: function uploadStatus(status, msg) {
            var msgContainerParent;
            var globalViewMessage;

            globalViewMessage = new GlobalViewsMessageView({
                message: _.translate(msg),
                type: status,
                closable: true
            });
            msgContainerParent = jQuery('.msg' + this.options.field.internalid);
            msgContainerParent.html(globalViewMessage.render().$el.html());
            jQuery('input, button').prop('disabled', false);
        },

        upload: function upload(form, options) {
            var self;

            self = this;

            jQuery(form).ajaxSubmit({
                type: 'json',
                method: 'POST',
                beforeSend: function beforeSend() {

                },
                url: _.resolveSuiteletURL(
                    SC.ENVIRONMENT.published.CheckoutFileUpload_config.suitelet.script,
                    SC.ENVIRONMENT.published.CheckoutFileUpload_config.suitelet.deploy
                ),
                success: function success(data) {
                    jQuery('.global-views-message-button').click();
                    self.uploadStatus('success', _.translate('Upload Success'));
                    self.model.addFile(data, options);
                },
                error: function error() {
                    self.wizard.getCurrentStep().enableNavButtons();
                    jQuery('input, button').prop('disabled', false);
                }
            });
        }
    });
});