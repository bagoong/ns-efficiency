define('Store.View.StoreDetail', [
    'Backbone',
    'Backbone.CompositeView',
    'Store.View.StoreLocator.Directions',
    'Store.View.StoreLocator.DirectionSearch',
    'store_details.tpl',
    'jQuery',
    'underscore'
], function StoreViewStoreDetail(
    Backbone,
    BackboneCompositeView,
    DirectionsView,
    DirectionSearchView,
    Template,
    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        events: {
            'click [data-action="print-direction"]': 'printDirection'
        },

        attributes: {
            'id': 'store-detail',
            'class': 'view storeDetail'
        },

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);

            this.options = options;
            this.model = options.model;
            this.application = options.application;
            this.configuration = this.application.getConfig('storeLocator');
            this.eventBus = _.extend({}, Backbone.Events);

            this.isInMyStore = options.isInMyStore;
        },

        title: _('Store Detail').translate(),

        getBreadcrumbPages: function getBreadcrumbPages() {
            var history = _.clone(this.application.history);
            history.push({text: this.model.get('name'), href: ''});
            return history;
        },

        getContext: function getContext() {
            return {
                model: this.model,
                image: this.model.get('mainImage') && this.model.get('mainImage').name,
                configuration: this.configuration,
                isInMyStore: this.isInMyStore
            };
        },

        getChildrenData: function getChildrenData() {
            return {
                configuration: this.configuration,
                model: this.model,
                options: this.options,
                eventBus: this.eventBus
            };
        },

        childViews: {
            'StoreLocator.Map': function StoreLocatorMap() {
                return new DirectionsView(this.getChildrenData());
            },
            'StoreLocator.DirectionForm': function StoreLocatorDirectionForm() {
                return new DirectionSearchView(this.getChildrenData());
            }
        },

        printDirection: function printDirection(e) {
            var el = jQuery('.storelocator-print-direction');

            e.preventDefault();
            if (el.is(':empty')) {
                return;
            }

            window.print();
        }
    });
});