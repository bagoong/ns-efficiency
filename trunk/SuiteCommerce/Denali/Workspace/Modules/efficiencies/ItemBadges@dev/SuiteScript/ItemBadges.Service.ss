/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
function service (request) {
    'use strict';

    var Application = require('Application');
    var method = request.getMethod();
    var ItemBadges = require('ItemBadges.Model');

    try {
        switch (method) {
        case 'GET':
            Application.sendContent(ItemBadges.list(), {
                'cache': response.CACHE_DURATION_MEDIUM
            });
            break;
        default:
            return Application.sendError(methodNotAllowedError);
        }
    } catch (e) {
        Application.sendError(e);
    }
}