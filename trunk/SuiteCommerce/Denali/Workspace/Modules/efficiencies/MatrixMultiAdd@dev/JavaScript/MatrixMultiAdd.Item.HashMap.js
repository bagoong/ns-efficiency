define('MatrixMultiAdd.Item.HashMap', [
    'ItemDetails.Model',
    'MatrixMultiAdd.Item.Model',
    'Backbone',
    'underscore',
    'Utils'
], function MatrixMultiAddOrderLineHashMap(
    ItemDetailsModel,
    MatrixMultiAddItemModel,
    Backbone,
    _
) {
    'use strict';

    var COLUMN = 'col';
    var ROW = 'row';

    return Backbone.Model.extend({
        initialize: function initialize() {
            this.data = {};
        },
        put: function put(key, value) {
            if (!this.data[key[COLUMN]]) {
                this.data[key[COLUMN]] = {};
            }
            this.data[key[COLUMN]][key[ROW]] = value;
            this.trigger('put', value);
        },
        remove: function remove(key) {
            if (!this.data[key[COLUMN]]) {
                this.data[key[COLUMN]] = {};
            }
            delete this.data[key[COLUMN]][key[ROW]];
            this.trigger('remove', key);
        },
        get: function get(key) {
            return this.data[key[COLUMN]] &&  this.data[key[COLUMN]][key[ROW]];
        },
        getAll: function getAll() {
            var all = [];
            _.each(this.data, function eachColumn(rows) {
                _.each(rows, function eachRow(value) {
                    all.push(value);
                });
            });
            return all;
        },
        getTotal: function getTotal() {
            var sum = 0;
            _.each(this.getAll(), function eachGetAll(item) {
                sum += item.getEstimatedAmount();
            });
            return sum;
        },
        
        quantityPricingCollection: function quantityPricingCollection(parentModel, itemOptionsModel) {
            var rowsValue;
            var rowsFieldId;
            var colsFieldId;
            var colsValue;
            var qtyPricingCollectionData = [];

            rowsFieldId = (parentModel.get('_getRowsOption')) ?
                parentModel.get('_getRowsOption').cartOptionId : ''; // rows configuration
            colsFieldId = (parentModel.get('_getColsOption')) ?
                parentModel.get('_getColsOption').cartOptionId : ''; // columns configuration

            colsValue = (itemOptionsModel.get('internalid')) ? itemOptionsModel.get('internalid') : 0;

            _.each(itemOptionsModel, function rowsCollection(data) {
                if (data.internalid) { // quantity pricing for both rows and columns
                    if (data.rows.length !== 0) {
                        qtyPricingCollectionData = [];
                        _.each(data.rows, function rowData(rowsData) {
                            if (rowsData.internalid) {
                                rowsValue = (rowsData.internalid) ? rowsData.internalid : 0;
                                parentModel.setOption(colsFieldId, colsValue, true);
                                parentModel.setOption(rowsFieldId, rowsValue, true);
                                qtyPricingCollectionData.push(parentModel.getSelectedMatrixChilds()[0]);
                            }
                        });
                    } else { // Quantity Pricing for columns only
                        qtyPricingCollectionData = [];
                        parentModel.setOption(colsFieldId, colsValue, true);
                        qtyPricingCollectionData.push(parentModel.getSelectedMatrixChilds()[0]);
                    }
                } else { // Quantity pricing for rows only
                    if (data.rows) {
                        qtyPricingCollectionData = [];
                        _.each(data.rows, function rowData(rowsData) {
                            if (rowsData.internalid) {
                                rowsValue = (rowsData.internalid) ? rowsData.internalid : 0;
                                parentModel.setOption(rowsFieldId, rowsValue, true);
                                qtyPricingCollectionData.push(parentModel.getSelectedMatrixChilds()[0]);
                            }
                        });
                    }
                }
            });
            return qtyPricingCollectionData;
        }
    });
});