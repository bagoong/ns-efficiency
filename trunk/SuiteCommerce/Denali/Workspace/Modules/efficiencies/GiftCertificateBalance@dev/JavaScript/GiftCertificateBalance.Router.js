define('GiftCertificateBalance.Router', [
    'Backbone',
    'GiftCertificateBalance.View'
], function Router(
    Backbone,
    GiftCertificateBalanceView
) {
    return Backbone.Router.extend({
        initialize: function initialize(application) {
            this.application = application;
        },
        routes: {
            'giftbalance': 'giftBalanceList'
        },
         giftBalanceList: function giftBalance() {
            var view;
             console.log('router');
            view = new GiftCertificateBalanceView({
                application: this.application
            });
            view.showContent();
        }
    });
});