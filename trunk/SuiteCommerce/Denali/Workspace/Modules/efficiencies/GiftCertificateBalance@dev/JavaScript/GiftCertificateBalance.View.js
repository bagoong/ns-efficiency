define('GiftCertificateBalance.View', [
    'Backbone',
    'giftcertificate_balance_view.tpl'
], function GiftCertificateBalanceView(
     Backbone,
     giftCertificateBalanceViewTPL
) {
    'use strict';

    return Backbone.View.extend({
        template: giftCertificateBalanceViewTPL,
        initialize: function initialize() {
            console.log('Hello');
        }
    });
});