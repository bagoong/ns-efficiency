define('BOPIS.EntryPoint', [
    'BOPIS.Configuration',
    'BOPIS.Hooks',
    'BOPIS.Location.Model',
    'BOPIS.OrderHistory.Model',
    'BOPIS.Store.Model',

    'BOPIS.LiveOrder.Hooks',
    'BOPIS.Address.Hooks',
    'BOPIS.OrderHistory.Hooks',

    'BOPIS.FAQs.Model'
], function BOPISEntryPoint(
    Configuration,
    Hooks,
    LocationModel,
    OrderHistoryModel
) {
    'use strict';
    return {
        Configuration: Configuration,
        Hooks: Hooks,
        LocationModel: LocationModel,
        OrderHistoryModel: OrderHistoryModel
    };
});