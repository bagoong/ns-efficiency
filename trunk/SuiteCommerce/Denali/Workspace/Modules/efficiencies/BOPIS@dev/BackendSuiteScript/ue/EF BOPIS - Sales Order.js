/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

define([
    '../libs/Bopis.SalesOrder',
    '../libs/Bopis.SalesOrder.Email'
], function EFBOPISSalesOrder(
    bopisSalesOrder,
    bopisSOEmail
) {
    'use strict';

    var Handlers  = {
        beforeSubmit: function beforeSubmit(context) {
            // Initialization
            var salesOrderRecord = context.newRecord;
            bopisSalesOrder.transformCustomToNative(salesOrderRecord);
            bopisSOEmail.setEmailFields(context);
        }
    };

    return Handlers;
});