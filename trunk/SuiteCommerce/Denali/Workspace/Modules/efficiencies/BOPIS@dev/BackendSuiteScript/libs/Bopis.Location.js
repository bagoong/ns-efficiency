/**
 *@NApiVersion 2.x
 */
define([
    'N/search'
], function BopisLocation(
    searchAPI
) {
    'use strict';

    // Keep this array with locationTypes with ids that backend has.
    var LOCATION_TYPES = ['stores', 'warehouses'];

    /**
    * get Location IDS by Website, locationType and website
    *
    * @param {Object} options
    * @param {Integer} options.locationType (1:STORE/2:WAREHOUSE)
    * @param {Integer} options.subsidiary
    * @param {Integer} options.website
    * @param {Boolean} options.splitByType
    * @param {*} options.id

    */

    return {
        LOCATION_TYPES: LOCATION_TYPES,

        getLocationsByType: function getLocationsByType(options) {
            var locationSearch;
            var locationsByType = {
                warehouses: [],
                stores: []
            };
            var locations = [];
            var filters = [
                [ 'subsidiary', 'is', options.subsidiary],
                'and',
                ['custrecord_bopis_location_webstores', 'is', options.website],
                'and',
                ['makeinventoryavailable', 'is', true],
                'and',
                ['makeinventoryavailablestore', 'is', true],
                'and',
                ['isinactive', 'is', false]
            ];

            if (options.locationType) {
                filters.push('and');
                filters.push(['locationtype', 'is', options.locationType]);
            }

            locationSearch = searchAPI.create({
                type: searchAPI.Type.LOCATION,
                columns: ['internalid', 'locationtype'],
                filters: filters
            });

            locationSearch.run().each(function eachRunResult(result) {
                var internalid = parseInt(result.getValue('internalid'), 10);
                var locationType = parseInt(result.getValue('locationtype'), 10);

                if (LOCATION_TYPES[locationType - 1]) {
                    locationsByType[LOCATION_TYPES[locationType - 1]].push(internalid);
                }
                locations.push(internalid);
                return true;
            });

            if (options.splitByType) {
                return locationsByType;
            }

            return locations;
        }
    };
});