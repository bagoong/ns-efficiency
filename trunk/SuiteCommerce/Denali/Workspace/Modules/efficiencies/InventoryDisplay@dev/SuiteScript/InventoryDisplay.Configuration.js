define('InventoryDisplay.Configuration', [
    'Configuration',
    'underscore'
], function InventoryDisplayConfiguration(
    GlobalConfiguration,
    _
) {
    'use strict';

    var Configuration =  {
        isDisplayAsBadge: false,

        ITEM_TYPES_WITH_INVENTORY: ['InvtPart'],
        STATUS_IN_STOCK: '1',
        STATUS_OUT_OF_STOCK: '2',
        STATUS_LOW_STOCK: '3',
        STATUS_AVAILABLE: '4',
        STATUS_NOT_AVAILABLE: '5',
        STATUS_NOT_PURCHASABLE: '6',

        LOCATION_TYPE_WAREHOUSE: 2,
        LOCATION_TYPE_STORE: 1
    };

    var stockStatus = {};

    stockStatus[Configuration.STATUS_IN_STOCK] = {
        badgeLabel: 'In stock',
        badgeClass: 'inventory-display-bg-in-stock',
        textLabel: '$(0) items In Stock',
        textLabelForPrefferedStock: 'More than $(0) items in stock',
        textClass: 'inventory-display-font-in-stock'
    };

    stockStatus[Configuration.STATUS_LOW_STOCK] = {
        badgeLabel: 'Low stock',
        badgeClass: 'inventory-display-bg-limited-stock',
        textLabel: '$(0) items in stock',
        textClass: 'inventory-display-font-limited-stock'
    };

    stockStatus[Configuration.STATUS_OUT_OF_STOCK] = {
        badgeLabel: 'Out of stock',
        badgeClass: 'inventory-display-bg-out-stock',
        textLabel: 'Out of stock',
        textClass: 'inventory-display-font-out-stock'
    };

    stockStatus[Configuration.STATUS_AVAILABLE] = {
        badgeLabel: 'Available',
        badgeClass: 'inventory-display-bg-available',
        textLabel: 'Available',
        textClass: 'inventory-display-font-available'
    };

    stockStatus[Configuration.STATUS_NOT_AVAILABLE] = {
        badgeLabel: 'Not available',
        badgeClass: 'inventory-display-bg-not-available',
        textLabel: 'Not available',
        textClass: 'inventory-display-font-not-available'
    };

    stockStatus[Configuration.STATUS_NOT_PURCHASABLE] = {
        badgeLabel: 'This item is currently not purchasable',
        badgeClass: 'inventory-display-bg-not-purchasable',
        textLabel: 'This item is currently not purchasable',
        textClass: 'inventory-display-font-not-purchasable'
    };

    Configuration.STOCK_STATUS =  stockStatus;

    _.extend(Configuration, {
        get: function get() {
            return Configuration;
        }
    });

    _.extend(GlobalConfiguration, {
        inventoryDisplay: Configuration
    });

    GlobalConfiguration.publish.push({
        key: 'InventoryDisplay_config',
        model: 'InventoryDisplay.Configuration',
        call: 'get'
    });

    return Configuration;
});
