define('Blog.EntryPoint', [
        'Blog.Configuration',
        'Blog.Post.Model',
        'Blog.Tags.Model'
], function CheckoutFileUploadEntryPoint(
    Configuration,
    BlogPostModel,
    BlogTagsModel
) {
    'use strict';
    return {
        Configuration: Configuration,
        BlogPostModel: BlogPostModel,
        BlogTagsModel: BlogTagsModel
    };
});