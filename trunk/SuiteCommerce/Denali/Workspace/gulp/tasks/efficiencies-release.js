/*
@module gulp.copy
#gulp copy

This gulp task will copy the files declared in the 'copy' property of ns.package.json. For example, if a module need to copy some static files to be available then in the filecabinet, it can declare something like this:

	{
		"gulp": {
			"copy": [
				"someFolder/** /*"
			]
		}
	}

and the content of 'someFolder' will be copied to the output respecting its internal folder structure
*/

/* jshint node: true */
'use strict';

var gulp = require('gulp')
,	package_manager = require('../package-manager')
	,	del = require('del')
	,jsonlint = require('jsonlint')
	,fs = require('fs')
	,	args = require('yargs').argv
	,	gif = require('gulp-if')
,	zip = require('gulp-zip')
,	_ = require('underscore');

var releaseFolder = 'ReleaseDistribution';

gulp.task('efficiencies-release', function()
{
	var referenceModules = ['suitecommerce','third_parties'];
	var distroFiles;
	var distros = [];
	var defaultReleaseName;
	var releaseName;
	var releaseAssets = [];
	if(args.distros){
		distroFiles = args.distros.split(',');

		_.each(distroFiles, function(distroFile){
			try
			{
				var distro = jsonlint.parse(fs.readFileSync(distroFile, {encoding: 'utf8'}));
				distros.push(distro);
				releaseAssets.push(distroFile);
			}
			catch(err)
			{
				err.message = 'Error parsing distro file ' + distroFile + ': ' + err.message;
				throw err;
			}
		});
		defaultReleaseName = args.distros.replace(/.json/gi, '').replace(',','_');
	} else {
		distros = [package_manager.distro];
		releaseAssets.push(args.distro);
		defaultReleaseName = args.distro.replace(/.json/gi, '').replace(',','_');
	}
	releaseName = args.name || defaultReleaseName;



	del.sync(releaseFolder);



	_.each(distros, function(distro){
		_.each(distro.modules, function(v,k){

			var namespace = k.split('/')[0];
			if(!_.contains(referenceModules, namespace)){

				var modulePath = './Modules/' + k + '@' + v +'/**';
				releaseAssets.push(modulePath);

				if(v==='dev'){
					console.warn('BUILDING A DEV RELEASE');
				}
			}
		});
	});

	_.each(distros, function(distro){
		var config = distro.tasksConfig['efficiencies-release'] || {};
		if(config.extras && config.extras.length) {
			releaseAssets = releaseAssets.concat(config.extras);
		}
	});

	console.log(releaseAssets);
	releaseAssets = _.uniq(releaseAssets);

	var shouldZip = !args.nozip;

	if(!shouldZip){
		releaseFolder += '/' + releaseName;
	}


	return gulp.src(releaseAssets,{base:"."})
		.pipe(gif(shouldZip, zip(releaseName+'.zip')))
		.pipe(gulp.dest(releaseFolder));

});
