{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="row">
	<div class="item-details-box col-xs-5 col-lg-7">
		<a 	{{#unless isGiftCertificate}}
				href="{{url}}"
			{{/unless}}
			class="item-cell-thumbnail img-thumbnail pull-left hidden-sm hidden-xs" >
	      <img src="{{thumbnailResized}}"
	           alt="{{thumbnailAltImageText}}"
	           itemprop="image" class="media-object">
	    </a>

	    <div class="item-details-container">
          <a class="item-title"
            {{#unless isGiftCertificate}}
              href="{{url}}"
            {{/unless}}
            >
            {{itemDetailsName}}
          </a>
          {{#unless isGiftCertificate}}
            <br class="hidden-xs">
            <small class="hidden-xs">
            	ITEM #<span class="upccode">{{itemDetailsUpcCode}}</span>
            </small>
          {{/unless}}
		</div>
    </div>

	<div class="text-right col-sm-1 col-xs-2">
	  {{#if hasQuantity}}
		<h5 class="quantity">{{absoluteQuantity}}</h5>
	  {{/if}}
	</div>

	<div class="text-right col-lg-1 col-sm-2 hidden-xs">
	  {{#if hasItemDetailsPriceFormatted}}
	      <h4 class="rate">{{itemDetailsPriceFormattedCurrency}}</h4>
	  {{/if}}
	</div>

	<div class="text-right col-xs-5 col-sm-4 col-lg-3">
	 	<a data-action="add-item" data-item="{{itemId}}" class="btn btn-primary">{{translate 'Add to cart'}}</a>
	 	<span data-type="added-alert-{{itemId}}" class="text-info fade">{{translate 'Added!'}}</span>
	</div>
</div>