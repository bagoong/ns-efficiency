{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="product-list-control-item">
	<label class="product-list-control-item-label" {{#if isMoving}}data-action="product-list-item"{{/if}}>
		
		{{#unless isMoving}}
		<input class="product-list-control-item-checkbox" type="checkbox" data-action="product-list-item" {{#if isChecked}}checked{{/if}}>
		{{/unless}}

		{{#if isTypePredefined}}
			{{translate itemName}}
		{{else}}
			{{itemName}}
		{{/if}}
	</label>
</div>
