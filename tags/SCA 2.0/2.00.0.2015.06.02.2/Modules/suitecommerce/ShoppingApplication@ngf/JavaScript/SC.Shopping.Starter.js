/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ShoppingApplication
// @class SCA.Shopping.Starter the main file to be loaded with requirejs tools. It contains all required dependencies
// to run Shopping and also it will start() the Shopping Applicaiton.
define(
	'SC.Shopping.Starter'
,	[
		'SC.Shopping'
	,	'jQuery'
	,	'underscore'
	,	'Backbone'
	,	'SC.Shopping.Starter.Dependencies' // Auto generated at build time using configuration from distro.json
	]
,	function(
		Shopping

	,	jQuery
	,	_
	,	Backbone
	,	entryPointModules
	)
{
	'use strict';

	Shopping.getConfig().siteSettings = SC.ENVIRONMENT.siteSettings || {};

	// remove the script added for load script function
	// only if we are in the page generator
	if (SC.ENVIRONMENT.jsEnvironment === 'server')
	{
		jQuery('.seo-remove').remove();
	}

	// When the document is ready we call the application.start, and once that's done we bootstrap and start backbone
	Shopping.start(entryPointModules, function ()
	{
		// Checks for errors in the context
		if (SC.ENVIRONMENT.contextError)
		{
			// Hide the header and footer.
			Shopping.getLayout().$('#site-header').hide();
			Shopping.getLayout().$('#site-footer').hide();

			// Shows the error.
			Shopping.getLayout().internalError(SC.ENVIRONMENT.contextError.errorMessage,'Error ' + SC.ENVIRONMENT.contextError.errorStatusCode + ': ' + SC.ENVIRONMENT.contextError.errorCode);
		}
		else
		{
			var fragment = _.parseUrlOptions(location.search).fragment;

			if (fragment && !location.hash)
			{
				location.hash = decodeURIComponent(fragment);
			}

			// Only do push state client side.
			Backbone.history.start({
				pushState: SC.ENVIRONMENT.seoSupport && SC.ENVIRONMENT.jsEnvironment === 'browser'
			});
		}

		Shopping.getLayout().appendToDom();
	});
});