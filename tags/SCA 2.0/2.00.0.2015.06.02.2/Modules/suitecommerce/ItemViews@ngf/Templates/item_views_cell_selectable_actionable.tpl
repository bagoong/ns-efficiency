{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<article data-id="{{lineId}}" class="item-list-navigable {{lineId}}" data-item-id="{{itemId}}">
	{{#if showCustomAlert}}
		<div class="alert-placeholder" data-type="alert-placeholder">
			<div class="alert alert-{{customAlertType}}">
				{{alertText}}
			</div>
		</div>
	{{/if}}

	<div class="item-list-navigable-item">
		<div class="input-checkbox">
			<input type="checkbox" name="" value="{{lineId}}" {{#if isLineChecked}}checked{{/if}}>
		</div>

		<div class="item-list-navigable-image">
			<div class="item-list-navigable-thumbnail">
				{{#if isNavigable}}
					<a {{{linkAttributes}}}>
						<img src="{{resizeImage imageUrl 'thumbnail'}}" alt="{{altImageText}}">
					</a>
				{{else}}
					<img src="{{resizeImage imageUrl 'tinythumb'}}" alt="{{altImageText}}">
				{{/if}}
			</div>
		</div>
		<div class="item-list-navigable-details">
				<div  class="item-list-navigable-name">
				{{#if isNavigable}}
					<a {{{linkAttributes}}} class="item-list-navigable-name-link">
						{{itemName}}
					</a>
				{{else}}
					{{itemName}}
				{{/if}}
				</div>
				<div class="item-list-navigable-price">
					<div data-view="Item.Price"></div>
				</div>
				<div class="item-list-navigable-sku">
					{{translate '<span class="text-light">SKU:</span> #$(0)' itemSKU}}
				</div>
				<div class="item-list-navigable-options">
					<div data-view="Item.SelectedOptions"></div>
				</div>
			{{#if showSummaryView}}
			<div class="item-list-navigable-summary" data-view="Item.Summary.View">
			</div>
			{{/if}}
		</div>
	</div>
	{{#if showActionsView}}
	<div data-view="Item.Actions.View" class="item-list-navigable-actions">
	</div>
	{{/if}}
</article>