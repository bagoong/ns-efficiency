{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="item-view-stock">
	{{#if showOutOfStockMessage}}
		<p class="item-view-stock-msg-out">
			<span class="item-view-stock-icon-out">
				<i></i>	
			</span>
			<span class="item-view-stock-msg-out-text">{{stockInfo.outOfStockMessage}}</span>
		</p>
	{{/if}}
	{{#if showInStockMessage}}
		<p class="item-view-stock-msg-in">
			<span class="item-view-stock-icon-in">
				<i></i>
			</span>
			{{stockInfo.inStockMessage}}
		</p>
	{{/if}}
	{{#if showStockDescription}}
		<p class="item-view-stock-msg-description {{stockInfo.stockDescriptionClass}}">
			<i class="item-view-stock-icon-description"></i>
			{{stockInfo.stockDescription}}
		</p>
	{{/if}}
</div>