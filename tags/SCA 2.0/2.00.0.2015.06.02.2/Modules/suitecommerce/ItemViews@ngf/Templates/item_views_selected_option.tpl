{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showOption}}
	<div class="items-views-selected-option" name="{{label}}">
		<span class="items-views-selected-option-label">{{label}}: </span>
    <span class="items-views-selected-option-value">{{value}}</span>
    <!-- <div class="items-views-selected-option-picker"><span>{{value}}</span></div> -->
	</div>
{{/if}}