/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/*
@module jQueryExtras
#jQuery.scStickyButton
A jQuery plugin that implements a sticky button behavior. Usage: 

	this.$el.find('[data-action="sticky"]').scStickyButton()
*/

define(
	'jQuery.scStickyButton'
,	[
		'jQuery'
	,	'Utils'
	]
,	function(
		jQuery
	,	Utils
	)
{
	'use strict';

	jQuery.fn.scStickyButton = function (options)
	{
		if (Utils.getViewportWidth() < 768)
		{
			var stickies = this;

			// TODO: solve image loading height difference
		 //    setTimeout(	function() 
			// {
				stickies.each(function() 
				{
					var self = this
					,	$this = jQuery(this);

					$this.data('position', $this.offset().top);

					var button_clone = $('<div class="sticky-button-container-clone">').append($this.clone(true).removeAttr('data-type'));
					// TODO: clone events
					$this.wrap('<div class="sticky-button-container">').parent().append(button_clone);

					
			    });

		    	checkSticks(stickies);
			//}, 1000);

		    $(document).on('scroll touchstart touchmove', function (e){
				checkSticks(stickies);
			});
		}

	    return this;
	};

	function checkSticks($stickers)
	{
		var top_delta = jQuery(document).scrollTop();

		$stickers.each(function ()
		{
			var $element = jQuery(this)
			,	top_position = $element.data('position')
			,	clone = $element.parent().find('.sticky-button-container-clone').first();
			
			if (top_delta > (top_position - 15))
			{
				clone.addClass('sticked').addClass('sticking');
				$element.addClass('sticked').addClass('sticking');
				$element.parent().addClass('sticked').addClass('sticking');
			}
			else if(top_delta <= $element.data('position') - 15 && $element.hasClass('sticked'))
			{
				clone.removeClass('sticked').removeClass('sticking');
				$element.removeClass('sticked').removeClass('sticking');
				$element.parent().removeClass('sticked').removeClass('sticking');
			}	
		});
	}
});