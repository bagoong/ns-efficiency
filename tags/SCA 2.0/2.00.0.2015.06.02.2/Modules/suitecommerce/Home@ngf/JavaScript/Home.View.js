/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module Home
define(
	'Home.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'home.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	home_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module Home.View @extends Backbone.View
	return Backbone.View.extend({

		template: home_tpl

	,	title: _('Welcome to the store').translate()

	,	page_header: _('Welcome to the store').translate()

	,	attributes: {
			'id': 'home-page'
		,	'class': 'home-page'
		}

	,	events:
		{
			'click [data-action=slide-carousel]': 'carouselSlide'
		}

	,	initialize: function ()
		{
			var self = this;
			this.windowWidth = jQuery(window).width();
			this.on('afterViewRender', function()
			{
				self.initSlider();
			});

			jQuery(window).on('resize', _.throttle(function ()
			{
				if (_.getDeviceType(self.windowWidth) === _.getDeviceType(jQuery(window).width()))
				{
					return;
				}
				self.render();

				_.resetViewportWidth();

				self.windowWidth = jQuery(window).width();

			}, 1000));

		}

	,	initSlider: function()
		{
			this.$el.find('[data-slider]').bxSlider({
				nextText: '<a class="home-gallery-next-icon"></a>'
	  		,	prevText: '<a class="home-gallery-prev-icon"></a>'
			});
		}

	,	getContext: function()
		{
			var config = Configuration.homePage || {};
			return {
				// @property {String} imageResizeId
				imageHomeSize: Utils.getViewportWidth() < 768 ? 'homeslider' : 'main'
				,	imageHomeSizeBottom: Utils.getViewportWidth() < 768 ? 'homecell' : 'main'
				,	carouselImages: config.carouselImages || []
    		,	bottomBannerImages: config.bottomBannerImages || []
			};
		}

	});
});
