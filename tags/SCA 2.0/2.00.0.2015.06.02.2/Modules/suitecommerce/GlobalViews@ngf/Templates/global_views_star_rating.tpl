{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="star-rating" data-validation="control-group">
	{{#if showLabelRating}}
		<div class="star-rating-content">
			<span class='star-rating-label-visible'>
				{{translate 'Rating'}}
			</span>
		</div>
	{{/if}}

	{{#if showLabel}}
		<div class="star-rating-content">
			<span class="star-rating-label" for="{{name}}">
				{{label}}
			</span>
		</div>
	{{/if}}

	<div class="star-rating-area" data-toggle='rater' data-validation="control" data-name="{{name}}" data-max="{{maxValue}}" data-value="{{value}}">

		{{#if isWritable}}
			<div class="star-rating-area-writable{{../className}}">
				{{#each buttons}}
					<button type="button" data-action="rate" name="{{../name}}" value="{{@indexPlusOne}}"></button>
				{{/each}}
			</div>
		{{/if}}

		<div class="star-rating-area-empty">
			<div class="star-rating-area-empty-content">
				{{#each buttons}}
					<i class="star-rating-empty{{../className}}"></i>
				{{/each}}
			</div>
		</div>

		<meta itemprop="bestRating" content="{{maxValue}}">

		<div class="star-rating-area-fill" data-toggle='ratting-component-fill' style="width: {{filledBy}}%">
			<div class="star-rating-area-filled">
				{{#each buttons}}
					<i class="star-rating-filled{{../className}}"></i>
				{{/each}}
			</div>
		</div>
	</div>

	{{#if showValue}}
		<span class="star-rating-value" itemprop="ratingValue">
			{{value}}
		</span>
	{{else}}
		<meta itemprop="ratingValue" content="{{value}}">
	{{/if}}

	{{#if showRatingCount}}
		<span class="star-review-total">
			
			{{#if ratingCountGreaterThan0}}
				<span class="star-review-total-number" itemprop="reviewCount">{{ratingCount}}</span> 
				<span class="star-review-total-review">{{ translate ' Reviews'}}</span>
			{{else}}
				<span class="star-review-total-empty-number" itemprop="reviewCount">({{ratingCount}})</span>
				<span class="star-review-total-no-review">{{ translate ' No Reviews yet'}}</span>
			{{/if}}
		</span>
	{{/if}}
</div>