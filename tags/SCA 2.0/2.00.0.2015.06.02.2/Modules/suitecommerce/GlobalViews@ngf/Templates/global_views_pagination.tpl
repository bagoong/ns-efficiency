{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if currentPageLowerThanTotalPagesAndCurrentPageGreaterThan0AndPagesCountGreaterThan1}}
<nav class="global-views-pagination">
	{{#if showPageIndicator}}
	<p class="pagination-count">{{translate '$(0) of $(1)' currentPage totalPages}}</p>
	{{/if}}

	<ul class="pagination-links {{#if showPaginationLinksCompactClass}} pagination-links-compact {{/if}}">

		{{#if isCurrentPageDifferentThan1}}
		<li class="pagination-prev">
			<a href="{{previousPageURL}}">
				<i class="global-views-pagination-prev-icon"></i>
			</a>
		</li>
		{{else}}
		<li class="pagination-prev-disabled">
			<a href="{{currentPageURL}}">
				<i class="global-views-pagination-prev-icon"></i>
			</a>
		</li>
		{{/if}}

		{{#if showPageList}}
		{{#if isRangeStartGreaterThan1}}
		<li class="pagination-disabled">
			<a href="{{currentPageURL}}">...</a>
		</li>
		{{/if}}

		{{#each pages}}
		{{#if isCurrentPageActivePage}}
		<li class="pagination-links-number">
			<a class="pagination-active" href="{{fixedURL}}">{{pageIndex}}</a>
		</li>
		{{else}}
		<li class="pagination-links-number">
			<a href="{{URL}}">{{pageIndex}}</a>
		</li>
		{{/if}}
		{{/each}}

		{{#if isRangeEndLowerThanTotalPages}}
		<li class="pagination-disabled">
			<a href="{{currentPageURL}}">...</a>
		</li>
		{{/if}}
		{{/if}}

		{{#if isCurrentPageLast}}
		<li class="pagination-next-disabled">
			<a href="{{currentPageURL}}">
				<i class="global-views-pagination-next-icon"></i>
			</a>
		</li>
		{{else}}
		<li class="pagination-next">
			<a href="{{nextPageURL}}">
				<i class="global-views-pagination-next-icon"></i>
			</a>
		</li>
		{{/if}}
	</ul>
</nav>
{{/if}}