{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if isCountryAndStatePresent}}
	<label class="global-views-state-group-label is-required" for="{{manage}}state">
		{{translate 'State'}} <span class="input-required">*</span>
	</label>
	<div  class="global-views-state-group-form-controls" data-validation="control">
		<select class="{{inputClass}} global-views-state-group-select" id="{{manage}}state" name="state" data-type="selectstate" data-action="selectstate" >
			<option value="">
				{{translate '-- Select --'}}
			</option>
			{{#each states}}
				<option value="{{code}}" {{#if isSelected}} selected {{/if}} >
					{{name}}
				</option>
			{{/each}}
		</select>
	</div>
{{else}}
	<label class="global-views-state-group-label" for="{{manage}}state">
		{{translate 'State/Province/Region'}}
		<p class="global-views-state-optional-label">{{translate '(optional)'}}</p>
	</label>
	<div  class="global-views-state-group-form-controls" data-validation="control">
		<input
			type="text"
			id="{{manage}}state"
			name="state"
			class="{{inputClass}} global-views-state-group-input"
			value="{{selectedState}}"
			data-action="inputstate"
		>
	</div>
{{/if}}