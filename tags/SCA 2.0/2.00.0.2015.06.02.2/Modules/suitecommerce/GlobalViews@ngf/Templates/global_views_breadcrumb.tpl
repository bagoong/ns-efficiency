{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="banner-breadcrumb-top" class="content-banner banner-breadcrumb-top" data-cms-area="breadcrumb_top" data-cms-area-filters="global"></div>
<ul class="breadcrumb" itemprop="breadcrumb">
	{{#each pages}}
		{{#if @last}}
			<li class="breadcrumb-item-active">
				{{text}}
			</li>
		{{else}}
			<li class="breadcrumb-item">
				<a href="{{href}}" 
					{{#if hasDataTouchpoint}} data-touchpoint="{{data-touchpoint}}" {{/if}}
					{{#if hasDataHashtag}} data-hashtag="{{data-hashtag}}" {{/if}}
				> {{text}} </a>
			</li>
			<li class="breadcrumb-divider">/</li>
		{{/if}}
	{{/each}}
</ul>
<div id="banner-breadcrumb-bottom" class="content-banner banner-breadcrumb-bottom" data-cms-area="breadcrumb_bttom" data-cms-area-filters="global"></div>