/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/* global CMS: false */
//@module CMSadapter
define('CMSadapter'
,	[	'jQuery'
	,	'underscore'

	,	'CMSadapter.Utils'
	,	'CMSadapter.EventHandlers'

	,	'CMSadapter.Render'
	,	'CMSadapter.Render.Text'
	,	'CMSadapter.Render.Merchand'
	,	'CMSadapter.Render.Image'
	,	'CMSadapter.Render.HTML'
	]
,	function (
		jQuery
	,	_

	,	CMSadapterUtils
	,	CMSadapterEventHandlers

	,	CMSadapterRender
	,	CMSadapterRenderText
	,	CMSadapterRenderMerchand
	,	CMSadapterRenderImage
	,	CMSadapterRenderHTML
	)
{
	'use strict';

	var startup_cms_load_done = false;

	//@class CMSadapter @extend ApplicationModule
	return {

		mountToApp: function (application)
		{
			//Register renders
			CMSadapterRender.addRender('TEXT', CMSadapterRenderText);
			CMSadapterRender.addRender('MERCHZONE', CMSadapterRenderMerchand);
			CMSadapterRender.addRender('IMAGE', CMSadapterRenderImage);
			CMSadapterRender.addRender('HTML', CMSadapterRenderHTML);

			//Start Adapter Code
			var self = this
			,	readyHandler = function ()
				{
					CMSadapterUtils.removeEventListener('cms:ready', readyHandler);
					self.registerEventHandlers(self.application);
				};

			self.application = application;
			self.page_cache = {};

			application.getLayout().once('afterAppendView', function ()
			{
				// CMS start
				if (typeof CMS !== 'undefined' && CMS.api && CMS.api.is_ready())
				{
					readyHandler();
				}
				else
				{
					CMSadapterUtils.addEventListener('cms:ready', readyHandler);
				}
			});
		}

		//@method registerEventHandlers Register event handlers for each event fired by the CMS engine
		//@return {Void}
	,	registerEventHandlers: function ()
		{
			var self = this
			,	loadHandler = function ()
				{
					CMSadapterUtils.removeEventListener('cms:load', loadHandler);
					startup_cms_load_done = true;
					self.getPage()
						.then(function (page)
						{
							CMSadapterEventHandlers.renderPageContent(page, self.application)
								.always(function ()
								{
									CMS.trigger('adapter:rendered');
								});
						});
				};

			// ==================
			// App listeners
			// ==================
			// beforeStart
			// afterModulesLoaded
			// afterStart

			// afterAppendView fires when HTML is added to the page. Usually when a user has navigated to another page. But can also fire on things like modal for the "quick look" on products.
			// This is the only event we have currently to know that SCA might have loaded a page.
			// We wait until startup_cms_load_done is true before really listening to afterAppendView requests. This prevents extra requests during page startup due to race-conditions/timing of SCA and CMS.
			self.application.getLayout().on('afterAppendView', function ()
			{
				// Adapter needs to get new content in case it was a page load that happened.
				// This is outside of the CMS on purpose. When a normal user is not authenticated, the website still needs to get content.
				if (startup_cms_load_done)
				{
					self.getPage()
						.then(function (page)
						{
							CMSadapterEventHandlers.renderPageContent(page, self.application, _.isEqual(page, self.last_render_page))
								.always(function ()
								{
									CMS.trigger('adapter:rendered');
								});
						});

					// let the cms know
					CMS.trigger('adapter:page:changed');
				}
			});

			// ==================
			// CMS listeners - CMS tells us to do something, could fire anytime
			// ==================

			// Ask for default values the cms needs upon startup
			CMS.on('cms:get:setup', function ()
			{
				CMS.trigger('adapter:got:setup', CMSadapterEventHandlers.getSetup());
			});

			CMS.on('cms:new:content', function (content)
			{
				CMSadapterEventHandlers.newContent(content, self.application)
				 	.done(function ()
					 {
					 	// let CMS know we're done
						CMS.trigger('adapter:rendered');
					 });
			});

			// Re-render an existing piece of content. Changing only the content values, NOT creating/re-creating the cms-content wrapper divs.
			CMS.on('cms:rerender:content', function (content)
			{
				var $render_place_holder = jQuery(CMSadapterUtils.generateContentHtmlID(content, true));

				CMSadapterEventHandlers.renderContentByType($render_place_holder, content, true, self.application)
					.always(function ()
					{
						CMS.trigger('adapter:rendered');
					});
			});

			CMS.on('cms:get:context', function ()
			{
				var context = CMSadapterEventHandlers.getPageContext(self.application);
				CMS.trigger('adapter:got:context', context);
			});

			// This is for times when the CMS is telling the adapter to re-render the page, even though the adapter did not initiate the request for page data
			CMS.on('cms:render:page', function (page)
			{
				var optimize_rendering = _.isEqual(page, self.last_render_page);
				self.last_render_page = page;

				CMSadapterEventHandlers.renderPageContent(page, self.application, optimize_rendering)
					.always(function ()
					{
						CMS.trigger('adapter:rendered');
					});
			});

			// ==================
			// END CMS listeners
			// ==================

			// Notify CMS that we are done loading the adapter code
			if (CMS.api.has_loaded())
			{
				loadHandler();
			}
			else
			{
				CMSadapterUtils.addEventListener('cms:load', loadHandler);
			}

			CMS.trigger('adapter:ready');
		}

		//@method getPage Asynchronously get the current CMS.Page
		//@return {jQuery.Deferred}
	,	getPage: function ()
		{
			var self = this
			,	ctx = CMSadapterEventHandlers.getPageContext(self.application)
			,	cache_key = JSON.stringify(ctx)
			,	result = jQuery.Deferred();

			if (self.page_cache[cache_key])
			{
				result.resolve(self.page_cache[cache_key]);
			}
			else
			{
				// get current page's content on load
				CMS.api.getPage({
					page: ctx
				,	success: function (page)
					{
						self.page_cache[cache_key] = page;
						self.last_render_page = page;

						result.resolve(page);
					}
				,	error: result.reject
				});
			}

			return result;
		}
	};
});


//@class CMS.Content
//@class CMS.Page