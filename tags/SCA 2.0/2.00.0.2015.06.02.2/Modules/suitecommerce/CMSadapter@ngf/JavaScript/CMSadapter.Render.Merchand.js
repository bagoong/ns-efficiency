/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module CMSadapter
/* global CMS: false */
define('CMSadapter.Render.Merchand'
,	[	'jQuery'
	,	'underscore'

	,	'CMSadapter.Merchandising.Zone'
	]
,	function (
		jQuery
	,	_

	,	CMSadapterMerchandisingZone
	)
{
	'use strict';

	//@class CMSadapter.Render.Merchand Class responsible to render merchandising zones into a CMS place holder
	return {

		//@method renderContent
		//@param {jQuery} $place_holder Place holder to render the image
		//@param {CMS.Content} content
		//@param {CMSAdapter.Render.Options} options
		//@return {jQuery.Deferred}
		renderContent: function (place_holder, content, options)
		{
			var render_promises = jQuery.Deferred()
			,	merchand_zone;

			if (content.fields.clob_merch_rule_url)
			{
				// Call items API.
				// NOTE: It's OK to use fields.clob_merch_rule_url here instead of making an additional request for merchzone data because this value should be the latest available when merchzone content is being edited.
				if (options.isEditPreview)
				{
					//The creation of the CMS merchandising will take care of the entire rendering
					merchand_zone = new CMSadapterMerchandisingZone(place_holder, _.extend({application: options.application}, content));
					merchand_zone.itemsAppendedToDom.always(render_promises.resolve);
				}
				else
				{
					// NOTE: We need the most currently available items api URL (so we get merchzone data), can't rely on content.fields.clob_merch_rule_url, it could be old (and we only use that for preview when editing anyway).
					CMS.api.getMerchzone({
						merch_rule_id: content.fields.number_merch_rule_id
					,	success: function (zone)
						{
							if (zone && zone.merch_zone && !zone.merch_zone.is_inactive && zone.merch_zone.is_approved && zone.merch_zone.query_string )
							{
								//The creation of the CMS merchandising will take care of the entire rendering
								//Pass the current application, the content that contains which items to exclude, and the zone that contains its name and description

								merchand_zone = new CMSadapterMerchandisingZone(place_holder, _.extend({application: options.application}, content, zone.merch_zone));
								merchand_zone.itemsAppendedToDom.always(render_promises.resolve);

							// If the rule was set to inactive after it was placed on the page we need to clear out the content rule URL.
							}
							else if (zone.merch_zone.is_inactive || !zone.merch_zone.is_approved)
							{
								content.fields.clob_merch_rule_url = undefined;
								render_promises.resolve('');
							}
						}
					});
				}
			}
			else
			{
				render_promises.resolve('');
			}

			return render_promises;
		}
	};
});