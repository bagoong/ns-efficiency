/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module CMSadapter
define('CMSadapter.Render'
,	[	'underscore'
	,	'jQuery'
	]
,	function (
		_
	,	jQuery
	)
{
	'use strict';

	//@class Merchandising.Context
	//@constructor
	var CMSadapterRender = function CMSadapterRender ()
	{
	};

	_.extend(CMSadapterRender, {

		//@property {Object} handlers List of registered handlers
		handlers: {}

		//@method addRender
		//@param {String} content_type
		//@param {Object} renderer
		//@return {Void}
	,	addRender: function (content_type, renderer)
		{
			CMSadapterRender.handlers[content_type] = renderer;
		}

		//@method render
		//@param {String} content_type
		//@param {jQuery} $place_holder
		//@param {CMS.Content} content
		//@param {CMSAdapter.Render.Options} options
		//@return {jQuery.Deferred}
	,	render: function (content_type, $place_holder, content, options)
		{
			if (CMSadapterRender.handlers[content_type])
			{
				return CMSadapterRender.handlers[content_type].renderContent($place_holder, content, options);
			}

			return jQuery.Deferred().resolve('');
		}
	});


	return CMSadapterRender;
});