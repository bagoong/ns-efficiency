/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module CMSadapter
define('CMSadapter.Render.Image'
,	[	'jQuery'

	,	'CMSadapter.ImageDefault.View'
	]
,	function (
		jQuery

	,	CMSadapterImageDefaultView
	)
{
	'use strict';

	//@class CMSadapter.Render.Image Class responsible to render images into a CMS place holder
	return {
		//@method renderContent
		//@param {jQuery} $place_holder Place holder to render the image
		//@param {CMS.Content} content
		//@return {jQuery.Deferred}
		renderContent: function ($place_holder, content)
		{
			if (content.fields.string_src)
			{
				var image_view = new CMSadapterImageDefaultView(content);
				image_view.render();

				$place_holder.html(image_view.$el);

				return jQuery.Deferred().resolve(image_view.$el.html());
			}

			return jQuery.Deferred().resolve('');
		}
	};
});