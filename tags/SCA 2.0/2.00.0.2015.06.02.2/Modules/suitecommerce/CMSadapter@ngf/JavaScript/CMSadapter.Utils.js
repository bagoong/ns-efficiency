/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module CMSadapter
define('CMSadapter.Utils'
,	['Backbone']
,	function (Backbone)
{
	'use strict';

	// @class CMSadapter.Utils
	// NOTE: Not polyfilling the addEventListener & removeEventListener methods in the browser
	// so libs like jQuery will continue to work without issue in legacy IE.
	var	ie_prop_listeners = {}

		//@method addEventListener
		//@param {String} type
		//@param {Function} callback
		//@return {Void}
	,	addEventListener = function (type, callback)
		{
			var prop_fn = function (e)
				{
					if (e.propertyName === type)
					{
						callback();
					}
				};
			// Modern browsers have addEventListener.
			if (document.addEventListener)
			{
				document.addEventListener(type, callback);
			// IE5-8 handled here.
			// The cms will update a property with the same name as the event on the documentElement.
			}
			else if (document.attachEvent)
			{
				document.documentElement.attachEvent('onpropertychange', prop_fn);
				ie_prop_listeners[type] = prop_fn; // Keep track of our new function for removal later.
			}
		}

		//@method removeEventListener
		//@param {String} type
		//@param {Function} callback
		//@return {Void}
	,	removeEventListener = function (type, listener)
		{
			if (document.removeEventListener)
			{
				document.removeEventListener(type, listener);
			}
			else if (document.detachEvent)
			{
				if (ie_prop_listeners[type])
				{
					document.documentElement.detachEvent('onpropertychange', ie_prop_listeners[type]);
				}
				ie_prop_listeners[type] = null;
			}
		}

		//@method getPagePath Return the canonical path to the current page
		//@return {String}
	,	getPagePath = function ()
		{
			var canonical = Backbone.history.fragment
			,	index_of_query = canonical.indexOf('?');

			return '/' + (!~index_of_query ? canonical : canonical.substring(0, index_of_query));
		}

		//@method generateContentHtmlClass
		//@param {CMS.Content} content
		//@param {Boolean} include_dot Flag to add or not a dot in front of the class name
		//@return {String}
	,	generateContentHtmlClass = function (content, include_dot)
		{
			return (include_dot ? '.': '') + 'cms-content cms-content-'+ content.type.toLowerCase();
		}

		//@method generateContentHtmlID
		//@param {CMS.Content} content
		//@param {Boolean} include_hash
		//@return {String}
	,	generateContentHtmlID = function (content, include_hash)
		{
			return (include_hash ? '#': '') + 'cms-content-'+ content.id +'-'+ content.current_context;
		};

	return {
		addEventListener: addEventListener
	,	removeEventListener: removeEventListener
	,	getPagePath: getPagePath
	,	generateContentHtmlClass: generateContentHtmlClass
	,	generateContentHtmlID: generateContentHtmlID
	};
});