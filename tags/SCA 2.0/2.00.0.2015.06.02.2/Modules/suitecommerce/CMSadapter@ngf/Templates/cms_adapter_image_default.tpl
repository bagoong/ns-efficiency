{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showStringLink}}
	<a href="{{{stringLink}}}">
{{/if}}
<img src="{{imageSource}}" {{#if showImageAltText}}alt="{{altString}}" {{/if}} class="cms-adapter-image-default-image" data-loader="false" />

{{#if showStringLink}}
	</a>
{{/if}}