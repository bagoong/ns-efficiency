{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="nav-search">
    <div class="nav-search-content">
        <form class="nav-search-content-form" method="GET" action="/search" data-action="search">
            <div class="nav-search-content-input">
                <input autocorrect="off" data-type="site-search" class="nav-search-input typeahead" placeholder="{{translate 'Search for products'}}" type="search" name="keywords" autocomplete="off" maxlength="{{maxLength}}"/>
                <i class="nav-search-input-icon"></i>
                <a class="nav-search-input-reset"><i class="nav-search-input-reset-icon"></i></a>
            </div>
            <button class="nav-search-button-submit" type="submit">{{translate 'Go'}}</button>
            <a href="#" class="nav-search-button-close" data-action="hide-sitesearch">{{translate 'Close'}}</a>
        </form>
    </div>
</div>