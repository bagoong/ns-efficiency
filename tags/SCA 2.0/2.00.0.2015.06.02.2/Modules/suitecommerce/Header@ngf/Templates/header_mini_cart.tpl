{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<a class="header-menu-cart-link {{#if showLines}}header-menu-cart-link-enabled{{/if}}" data-type="mini-cart" title="{{translate 'Cart'}}" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart" href="#">
	<i class="header-menu-cart-icon"></i>
	<span class="header-menu-cart-legend">
		{{#if isLoading}}
		<span class="cart_ellipsis"></span>
		{{else}}
			{{#if showPluraLabel}}
				{{translate '$(0) items' itemsInCart}}
			{{else}}
				{{translate '1 item'}}
			{{/if}}
		{{/if}}
	</span>
</a>
<div class="mini-cart">
	 {{#if showLines}} 
	 	<div data-view="Header.MiniCartItemCell" class="mini-cart-container"></div>
		<div class="mini-cart-subtotal">
			<div class="mini-cart-subtotal-items">
				{{#if showPluraLabel}}
					{{translate '$(0) items' itemsInCart}}
				{{else}}
					{{translate '1 item'}}
				{{/if}}
			</div>
			<div class="mini-cart-subtotal-amount">
				{{translate 'SUBTOTAL: $(0)' subtotalFormatted}}
			</div>
		</div>
		<div class="mini-cart-buttons">
			<div class="mini-cart-buttons-left">
				<a href="#" class="button-view-cart" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart"  data-action="view-cart">
					{{translate 'View Cart'}}
				</a>
			</div>
			<div class="mini-cart-buttons-right">
				<a href="#" class="button-checkout" data-touchpoint="checkout" data-hashtag="#"  data-action="checkout">
					{{translate 'Checkout'}}
				</a>
			</div>
		</div>
		{{else}} 
		<div class="mini-cart-empty">
			<a href="#" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart">
				{{#if isLoading}}
					{{translate 'Your cart is loading'}}
				{{else}}
					{{translate 'Your cart is empty'}}
				{{/if}}
			</a>
		</div>
	{{/if}} 
</div>

