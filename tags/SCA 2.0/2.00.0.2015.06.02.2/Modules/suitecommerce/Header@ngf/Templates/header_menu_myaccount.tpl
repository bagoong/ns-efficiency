{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<a class="header-menu-myaccount-anchor" href="#" data-action="push-menu">
	{{translate 'My Account'}}
	<i class="header-menu-myaccount-menu-push-icon"></i>
</a>

<ul class="header-menu-myaccount">
	<li>
		<a href="#" class="header-menu-myaccount-back" data-action="pop-menu">
			<i class="header-menu-myaccount-pop-icon "></i>
			{{translate 'Back'}}
		</a>
	</li>
	<li class="header-menu-myaccount-overview">
		<a class="header-menu-myaccount-overview-anchor" href="#" data-touchpoint="customercenter" data-hashtag="#overview">
			{{translate 'Account Overview'}}
		</a>

		<a class="header-menu-myaccount-signout-link" href="#" data-touchpoint="logout">
			<i class="header-menu-myaccount-signout-icon"></i>
			{{translate 'Sign Out'}}
		</a>
	</li>

	<li class="header-menu-myaccount-item-level2" data-permissions="transactions.tranFind.1,transactions.tranSalesOrd.1">
		<a class="header-menu-myaccount-anchor-level2" href="#" data-action="push-menu">
			{{translate 'Orders'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#ordershistory">
					{{translate 'Order History'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#returns" data-permissions="transactions.tranFind.1,transactions.tranRtnAuth.1">
					{{translate 'Returns'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#receiptshistory">
					{{translate 'Receipts'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#reorderItems">
					{{translate 'Reorder Items'}}
				</a>
			</li>
			<li data-permissions="transactions.tranFind.1,transactions.tranEstimate.1">
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#quotes">
					{{translate 'Quotes'}}
				</a>
			</li>
		</ul>
	</li>

	<!-- Product Lists - For single list mode data-hashtag will be added dynamically -->
	{{#if isProductListsEnabled}}
		<li class="header-menu-myaccount-item-level2">
			<a class="header-menu-myaccount-anchor-level2" href="#" data-touchpoint="customercenter" data-hashtag="#wishlist">
				{{translate 'Wishlist'}}
			</a>
			
			<ul class="header-menu-myaccount-level3">
				{{#if productListsReady}}
					<li>
						<a href="#" class="header-menu-myaccount-anchor-level3" data-touchpoint="customercenter" data-hashtag="#wishlist">
							{{translate 'All my lists'}}
						</a>
					</li>
					{{#each productLists}}			
					<li>
						<a href="#" class="header-menu-myaccount-anchor-level3" data-touchpoint="customercenter" data-hashtag="{{url}}">
							{{name}}
						</a>
					</li>
					{{/each}}	
				{{else}}
					<li>
						<a href="#" class="header-menu-myaccount-anchor-level3">
							{{translate 'Loading...'}}
						</a>
					</li>
				{{/if}}
			</ul>
		</li>
	{{/if}}


	<!-- Billing -->
	<li class="header-menu-myaccount-item-level2">
		<a class="header-menu-myaccount-anchor-level2" href="#" data-action="push-menu">
			{{translate 'Billing'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#balance">{{translate 'Account Balance'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#invoices" data-permissions="transactions.tranCustInvc.1,transactions.tranSalesOrd.1">{{translate 'Invoices'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#transactionhistory" data-permissions="transactions.tranCustInvc.1, transactions.tranCustCred.1, transactions.tranCustPymt.1, transactions.tranCustDep.1, transactions.tranDepAppl.1" data-permissions-operator="OR">{{translate 'Transaction History'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#printstatement" data-permissions="transactions.tranStatement.2">{{translate 'Print a Statement'}}</a>
			</li>
		</ul>
	</li>

	<!-- Settings -->
	<li class="header-menu-myaccount-item-level2">
		<a class="header-menu-myaccount-anchor-level2" tabindex="-1" href="#" data-action="push-menu">
			{{translate 'Settings'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#profileinformation">
					{{translate 'Profile Information'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#emailpreferences">
					{{translate 'Email Preferences'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#addressbook">
					{{translate 'Address Book'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#creditcards">
					{{translate 'Credit Cards'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="#" data-touchpoint="customercenter" data-hashtag="#updateyourpassword">
					{{translate 'Update Your Password'}}
				</a>
			</li>
		</ul>
	</li>

	{{#if isCaseModuleEnabled}}
	<li class="header-menu-myaccount-item-level2" data-permissions="lists.listCase.1">
		<a  class="header-menu-myaccount-anchor-level2" tabindex="-1" href="#" data-action="push-menu">
			{{translate 'Cases'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#cases" >{{translate 'All My Cases'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="#" data-touchpoint="customercenter" data-hashtag="#newcase">{{translate 'Submit New Case'}}</a>
			</li>
		</ul>
	</li>
	{{/if}}

</ul>