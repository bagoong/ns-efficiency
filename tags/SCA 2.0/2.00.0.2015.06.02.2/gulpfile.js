/* jshint node: true */

'use strict';

process.gulp_init_cwd = process.env.INIT_CWD || process.cwd();
var gulp = require('gulp')
,	package_manager = require('./gulp/package-manager')
,	path = require('path')
,	fs = require('fs');

process.gulp_dest_distro = path.join(process.gulp_init_cwd, package_manager.distro.folders.distribution);
process.gulp_dest_deploy = path.join(process.gulp_init_cwd, package_manager.distro.folders.deploy);
process.gulp_dest = process.gulp_dest_distro;


fs.readdirSync('./gulp/tasks').forEach(function(task_name)
{
	if (/\.js/.test(task_name))
	{
		require('./gulp/tasks/' + task_name.replace('.js', ''));
	}
});



gulp.task(
	'frontend'
,	[
		'javascript'
	,	'javascript-move'
	,	'less'
	,	'sass'
	,	'languages'
	,	'images'
	,	'fonts'
	,	'font-awesome'
	,	'clean-templates'
	,	'clean-sass-tmp'
	]
);

gulp.task(
	'backend'
,	[
		'services'
	,	'ssp-libraries'
	,	'ssp-files'
	]
);

gulp.task(
	'default'
,	[
		'backend'
	,	'frontend'	

	]
);