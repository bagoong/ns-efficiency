<?php

class CombinerCss extends Combiner {

    public function getContentType() {
        return 'text/css';
    }
    public function getConfigFilename() {
        return "combiner.config";
    }
    public function combine() {
        $this->result = '';
        $added_files = array();
        foreach($this->config['Input-files'] as $file_name) {
            if(!empty($file_name)) {
                $matched_files = $this->precedence->globFilePathNetSuite($this->internalPath, $file_name, "*.css", GLOB_NOSORT);
                foreach($matched_files as $relative => $matched_file) {
                    if(substr($matched_file, 0, 2) === './') {
                        $matched_file = substr_replace($matched_file, '', 0, 2);
                    }
                    if(!in_array($matched_file, $added_files)) {
                        $added_files[] = $matched_file;
                        $this->addFile($relative, $matched_file);
                    }
                }
            }
        }
    }
    public function addFile($relative, $absolute) {
        if($this->debug) {
            $this->result .= "@import url(\"".$this->getBaseUrl().$relative."\");"."\n";
        }
        else {
            $this->result .= ""
                ."/************************************************************************************************".PHP_EOL
                ."FILE: ".$relative.PHP_EOL
                ."*************************************************************************************************/".PHP_EOL.PHP_EOL
                .file_get_contents($absolute).PHP_EOL.PHP_EOL.PHP_EOL
                ."/*******************************************************************************".PHP_EOL
                ."END FILE: ".$relative.PHP_EOL
                ."********************************************************************************/".PHP_EOL.PHP_EOL;
        }
    }
}