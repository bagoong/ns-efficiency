<?php

abstract class Combiner {

    /**
     * @var Precedence
     */
    protected $precedence;
    protected $internalPath;
    protected $app;
    protected $config;
    protected $result;
    protected $debug;

    public function initialize(Precedence $precedence, $internalPath, $app) {
        $this->precedence = $precedence;
        $this->internalPath = $internalPath;
        $this->app = $app;
        $this->parseConfig();
    }

    abstract public function getConfigFilename();
    abstract public function getContentType();
    abstract public function addFile($relative, $absolute);
    abstract public function combine();

    public function getBaseUrl() {
        return '//'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/';
    }
    public function parseConfig() {
        $filename = $this->getConfigFilename();
        $config_file = file($this->precedence->getFilePath($this->internalPath.$filename));
        $this->config = array();
        foreach ($config_file as $line) {
            if(trim($line)) {
                $tokens = explode(':', $line);
                $this->config[trim($tokens[0])] = trim($tokens[1]);
            }
        }
        $this->config['Input-files'] = explode(' ', $this->config['Input-files']);
        $this->debug = true;//array_key_exists('Method', $this->config) && ($this->config['Method'] != 'Minify');
    }
    public function sendHeaders() {
        header("Content-Type: ".$this->getContentType()."; charset=UTF-8");
        header("Content-Disposition:inline;filename=\"".$this->config['Combined-file']."\"");
        header("Content-length: ".strlen($this->result));
    }
    public function output() {
        echo $this->result;
    }

}