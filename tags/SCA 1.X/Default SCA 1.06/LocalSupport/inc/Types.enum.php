<?php

abstract class Types {
    const JAVASCRIPT = 'js';
    const STYLES = 'css';
    const TEMPLATES = 'templates';

    public static function infer($name) {
        switch($name) {
            case 'Application':
            case 'Libraries':
            case 'BootUtilities':
                return self::JAVASCRIPT;
            case 'Styles':
            case 'Skins':
                return self::STYLES;
            case 'Templates':
                return self::TEMPLATES;
            default:
                return NULL;
        }
    }
}