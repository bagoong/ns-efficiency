require(['OrderWizard.Router', 'OrderWizard.Module.ThreeDSecure'], function(Router, ModuleThreeDSecure) {

    'use strict';

    _.extend(Router.prototype, {

        /**
         * CUSTOMIZATIONS FOR 3D SECURE
         */
        start3dSecure: function(promise) {
            var self = this,
                wrapper_deferred = new jQuery.Deferred();

            promise.done(function() {

                var confirmation = self.model.get('confirmation'),
                    success = false;

                if (confirmation.statuscode) {
                    if(confirmation.statuscode === "success") {
                        wrapper_deferred.resolveWith(this);
                        success = true;
                    }
                    else if(confirmation.statuscode === "error") {
                        // Order is not success since payment authorization is required
                        if (confirmation.reasoncode && confirmation.reasoncode == "ERR_WS_REQ_PAYMENT_AUTHORIZATION") {
                            if (confirmation.paymentauthorization) {
                                var view = new ModuleThreeDSecure({
                                    layout: self.application.getLayout(),
                                    application: self.application,
                                    wizard: self,
                                    deferred: wrapper_deferred
                                });
                                view.showInModal();
                                success = true;
                            }
                        }
                    }
                }
                if(!success) {
                    wrapper_deferred.rejectWith(this);
                }
            }).fail(function() {
                wrapper_deferred.rejectWith(this);
            });

            return wrapper_deferred.promise();
        }

    })

});