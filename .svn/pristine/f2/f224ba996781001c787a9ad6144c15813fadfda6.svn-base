define('ProductEditReviewGoogleRecaptcha', [
    'ProductReviews.FormPreview.View',
    'ProductEditReviewGoogleRecaptcha.View',
    'Backbone.CompositeView',
    'underscore',
    'jQuery',
    'ProductEditReviewGoogleRecaptcha.validate'
], function ProductEditReviewGoogleRecaptcha(
    ProductFormPreviews,
    ProductEditReviewGoogleRecaptchaView,
    BackboneCompositeView,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            ProductFormPreviews.prototype.initialize =
            _.wrap(ProductFormPreviews.prototype.initialize, function wrapProductFormInitialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                if (! ProductFormPreviews.prototype.childViews) {
                    ProductFormPreviews.prototype.childViews = {};
                    BackboneCompositeView.add(this);
                }

                _.extend(ProductFormPreviews.prototype.childViews, {
                    'renderCaptcha.GoogleRecaptcha': function childViewGoogleCaptcha() {
                        return new ProductEditReviewGoogleRecaptchaView({
                            model: this.model,
                            application: this.application
                        });
                    }
                });

                this.on('afterViewRender', function afterViewRender() {
                    this.$el
                        .find('.product-reviews-form-preview-main')
                        .after('<div class="product-reviews-preview-review-content" ' +
                               'data-view="renderCaptcha.GoogleRecaptcha"></div>');
                });
            });
        }
    };
});