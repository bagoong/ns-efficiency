// exercise S02-02: This is an automatically executing function that modifies
//                 the application configuration after it has already been
//                 loaded into the SC global from content in Configuration.js
(function (MyAccount) {

	// _.extend is an Underscore JS utility that allows us to easily extend an
	// object with new object keys using object literal notation. There is no
	// requirement to do it this way, but Underscore JS is used all throughout
	// the reference applications, including _.extend. Check for its use within
	// Configuration.js itself
    _.extend(MyAccount.Configuration.creditCardIcons, {
    	'JCB': 'img/jcb.png' 
    });
    
// If we pass in the application object to the automatically executing function,
// then we can refer to it with less code if we have lots of updates on the
// application object.
})(SC.Application('MyAccount'));

/*
 * exercise S02-03: this is an automatically executing function to update
 * one of the functions defined in SC.Utils
 */
(function ()
{
	// this was the current version of the function in Utils.js, after adding JCB for exercise 06-02
	function paymenthodIdCreditCart(cc_number)
	{
		// regex for credit card issuer validation
		var cards_reg_ex = {
			'VISA': /^4[0-9]{12}(?:[0-9]{3})?$/
		,	'Master Card': /^5[1-5][0-9]{14}$/
		,	'American Express': /^3[47][0-9]{13}$/
		,	'Discover': /^6(?:011|5[0-9]{2})[0-9]{12}$/
		,   'JCB': /^(?:2131|1800|35\d{3})\d{11}$/  // exercise S02-03: JCB entry, originally from exercise 07-02
		}
		
		// get the credit card name 
		,	paymenthod_name;

		// validate that the number and issuer
		_.each(cards_reg_ex, function(reg_ex, name)
		{
			if (reg_ex.test(cc_number))
			{
				paymenthod_name = name;
			}
		});
		
		var paymentmethod = paymenthod_name && _.findWhere(SC.ENVIRONMENT.siteSettings.paymentmethods, {name: paymenthod_name.toString()});
		
		return paymentmethod && paymentmethod.internalid;
	}

	// We override the function in the SC global, specifically SC.Utils
	SC.Utils.paymenthodIdCreditCart = paymenthodIdCreditCart;
	
	// You may recall that at the very bottom of Utils.js is an execution of the Underscore JS function
	// _.mixin. This function makes the utility functions in Utils.js available as Underscore extensions.
	// We're re-executing the same function here to re-register the updated paymenthodIdCreditCart function
	// to Underscore. See http://underscorejs.org/#mixin for more information.
	_.mixin({
		paymenthodIdCreditCart: paymenthodIdCreditCart
	});
    
}());
