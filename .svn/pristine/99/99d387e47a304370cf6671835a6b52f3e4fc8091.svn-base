/**
 * Store Stock
 *
 * Version    Date            Author           Remarks
 * 1.00       25 Aug 2015     cparayno
 *
 */

var IN_STOCK = 1;
var OUT_OF_STOCK = 2;
var LIMITED_STOCK = 3;

Array.prototype.removeInArray = function(val) {
    var i = this.indexOf(val);
    return i>-1 ? this.splice(i, 1) : [];
}

Array.prototype.unique = function() {
    if (this == null) return [];
    var result = [];
    for (var i = 0; i < this.length; i++) {
        var value = this[i];
        if (result.indexOf(value) < 0) {
            result.push(value);
        }
    }
    return result;
};

function getStockValue(quantityAvailable, preferredStockLevel, safetyStockLevel) {
    'use strict';
    var stock = 0;
    var badge = 0;
    var valueType = 0;
    var hasChecked = false;

    if (preferredStockLevel !== '') {                       // return preferred stock level to display in PDP
        preferredStockLevel = parseFloat(preferredStockLevel);
        if (quantityAvailable >= preferredStockLevel) {     // e.g. More than [preferered stock level] items in stock
            hasChecked = true;
            valueType = 1;
            stock = preferredStockLevel;
            badge = IN_STOCK;
        }
    }

    // if limited stock, display the actual quantity available in PDP
    if (safetyStockLevel !== '') {
        safetyStockLevel = parseFloat(safetyStockLevel);

        // e.g. [quantity available] items in stock
        if (quantityAvailable < safetyStockLevel) {
            hasChecked = true;
            stock = 0;
            badge = OUT_OF_STOCK;
        }
    }

    if (preferredStockLevel !== '' && safetyStockLevel !== '') {
        safetyStockLevel = parseFloat(safetyStockLevel);
        preferredStockLevel = parseFloat(preferredStockLevel);
        if (quantityAvailable < preferredStockLevel && quantityAvailable >= safetyStockLevel) {
            hasChecked = true;
            stock = quantityAvailable;
            badge = LIMITED_STOCK;
        }
    }

    if (!hasChecked && stock === 0) {
        valueType = 0;
        if (quantityAvailable > 0) {
            stock = quantityAvailable;
            badge = IN_STOCK;
        } else {
            stock = 0;
            badge = OUT_OF_STOCK;
        }
    }

    return [stock, badge, valueType];
}

function getStoreStock(id, storeId) {
    'use strict';
    var nsContext = nlapiGetContext();
    var isAdvanceInventoryEnabled = nsContext.getFeature('ADVINVENTORYMGMT');
    var items;
    var result = [];
    var item = '';
    var quantityAvailable = 0;
    var preferredStockLevel = '';
    var safetyStockLevel = '';
    var itemDetail = {};
    var i;
    var stockValue;
    var storeIds = [];


    if (storeId) {
        storeIds = storeId.split(',');
        storeIds = storeIds.unique();
    }

    var filters = [
        new nlobjSearchFilter('isonline', null, 'is', 'T'),
        // new nlobjSearchFilter('locationquantityavailable', null, 'greaterthan', 0),
        new nlobjSearchFilter('makeinventoryavailablestore', 'inventorylocation', 'is', 'T')
    ];
    var columns = [
        new nlobjSearchColumn('internalid'),
        new nlobjSearchColumn('displayname'),
        new nlobjSearchColumn('internalid', 'inventorylocation'),
        new nlobjSearchColumn('locationquantityavailable'),
        new nlobjSearchColumn('locationpreferredstocklevel'),

    ];

    if( isAdvanceInventoryEnabled ){
        columns.push( new nlobjSearchColumn('safetystocklevel') );
        columns.push( new nlobjSearchColumn('locationsafetystocklevel') );
    }


    if (id) {
        filters.push(new nlobjSearchFilter('internalid', null, 'is', id));
    }

    if (storeIds.length > 0) {
        filters.push(new nlobjSearchFilter('internalid', 'inventorylocation', 'is', storeIds));
    }

    try {
        items = nlapiSearchRecord('item', null, filters, columns);

        if (items) {
            for (i = 0; i < items.length; i++) {
                item = items[i];
                quantityAvailable = parseFloat(item.getValue('locationquantityavailable'));
                preferredStockLevel = item.getValue('locationpreferredstocklevel');

                if( isAdvanceInventoryEnabled ){
                    safetyStockLevel = item.getValue('safetystocklevel');
                    if(safetyStockLevel === ''){
                        safetyStockLevel = item.getValue('locationsafetystocklevel');
                    }
                }

                itemDetail = {
                    itemId: parseInt(item.getValue('internalid'), 10),
                    locationId: item.getValue('internalid', 'inventorylocation')
                };

                stockValue = getStockValue(quantityAvailable, preferredStockLevel, safetyStockLevel);

                itemDetail.quantityAvailable = quantityAvailable;
                itemDetail.preferredStockLevel = preferredStockLevel;
                itemDetail.safetyStockLevel = safetyStockLevel;

                itemDetail.stock = stockValue[0];
                itemDetail.badge = stockValue[1];

                if (stockValue[2] === 1) {
                    itemDetail.isValuePreferred = true;
                }

                result.push(itemDetail);
                storeIds.removeInArray(itemDetail.locationId);
            }

            if(storeIds.length > 0){
                for (i = 0; i < storeIds.length; i++) {
                    result.push({
                        itemId: id,
                        locationId: storeIds[i],
                        stock: 0,
                        badge: OUT_OF_STOCK
                    });
                }
            }
        } else {
            // if no stock available on the selected store, return Out of Stock badge
            if (storeIds.length > 0) {
                for (i = 0; i < storeIds.length; i++) {
                    result.push({
                        itemId: id,
                        locationId: storeIds[i],
                        stock: 0,
                        badge: OUT_OF_STOCK
                    });
                }
            }


        }
    } catch (e) {
        result = {status: e.message};
    }

    return result;
}


/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function service(request, response) {
    'use strict';
    var result = {};
    var id;
    var storeId;

    try {
        if (request.getMethod() === 'GET') {
            id = request.getParameter('id');
            storeId = request.getParameter('stores');

            result = getStoreStock(id, storeId);
        }
    } catch (e) {
        result = {status: e.message};
    }

    response.setContentType('JSON');
    response.write(JSON.stringify(result));
}