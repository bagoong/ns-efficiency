/* PSG ECOMMERCE formatCurrency (found in commons.js) in a single file to be used in non web contexts */

function formatCurrency (value, symbol, settings)
{
    'use strict';
    var value_float = parseFloat(value);

    if (isNaN(value_float))
    {
        value_float = parseFloat(0); //return value;
    }

    var negative = value_float < 0;
    value_float = Math.abs(value_float);
    value_float = parseInt((value_float + 0.005) * 100, 10) / 100;

    var value_string = value_float.toString()

        ,	groupseparator = ','
        ,	decimalseparator = '.'
        ,	negativeprefix = '('
        ,	negativesuffix = ')'
        ,	settings = settings || {};

    if (settings.hasOwnProperty('groupseparator'))
    {
        groupseparator = settings.groupseparator;
    }

    if (settings.hasOwnProperty('decimalseparator'))
    {
        decimalseparator = settings.decimalseparator;
    }

    if (settings.hasOwnProperty('negativeprefix'))
    {
        negativeprefix = settings.negativeprefix;
    }

    if (settings.hasOwnProperty('negativesuffix'))
    {
        negativesuffix = settings.negativesuffix;
    }

    value_string = value_string.replace('.',decimalseparator);
    var decimal_position = value_string.indexOf(decimalseparator);

    // if the string doesn't contains a .
    if (!~decimal_position)
    {
        value_string += decimalseparator + '00';
        decimal_position = value_string.indexOf(decimalseparator);
    }
    // if it only contains one number after the .
    else if (value_string.indexOf(decimalseparator) === (value_string.length - 2))
    {
        value_string += '0';
    }

    var thousand_string = '';
    for (var i = value_string.length - 1; i >= 0; i--)
    {
        //If the distance to the left of the decimal separator is a multiple of 3 you need to add the group separator
        thousand_string = (i > 0 && i < decimal_position && (((decimal_position-i) % 3) === 0) ? groupseparator : '') + value_string[i] + thousand_string;
    }

    if (!symbol)
    {
        symbol = '$';
    }

    value_string  = symbol + thousand_string;

    return negative ? (negativeprefix + value_string + negativesuffix) : value_string;
}