{
  "name": "gulp-rimraf",
  "description": "rimraf plugin for gulp",
  "version": "0.1.1",
  "homepage": "https://github.com/robrich/gulp-rimraf",
  "repository": {
    "type": "git",
    "url": "git://github.com/robrich/gulp-rimraf.git"
  },
  "author": {
    "name": "Rob Richardson",
    "url": "http://robrich.org/"
  },
  "main": "./index.js",
  "keywords": [
    "gulpplugin",
    "rimraf",
    "clean",
    "remove",
    "delete",
    "gulp-clean"
  ],
  "dependencies": {
    "gulp-util": "^3.0.1",
    "rimraf": "^2.2.8",
    "through2": "~0.6.2"
  },
  "devDependencies": {
    "jshint": "^2.5.6",
    "mocha": "^1.21.4",
    "should": "^4.0.4",
    "vinyl": "~0.4.3"
  },
  "scripts": {
    "test": "mocha && jshint ./index.js ./test/."
  },
  "engines": {
    "node": ">= 0.10.0"
  },
  "licenses": [
    {
      "type": "MIT",
      "url": "http://github.com/robrich/gulp-rimraf/raw/master/LICENSE"
    }
  ],
  "readme": "gulp-rimraf ![status](https://secure.travis-ci.org/robrich/gulp-rimraf.png?branch=master)\r\n===========\r\n\r\nDeprecated in favor of [https://github.com/gulpjs/gulp/blob/master/docs/recipes/delete-files-folder.md](https://github.com/gulpjs/gulp/blob/master/docs/recipes/delete-files-folder.md)\r\n\r\n[rimraf](https://github.com/isaacs/rimraf) plugin for [gulp](https://github.com/gulpjs/gulp)\r\n\r\nUsage\r\n-----\r\n\r\n1. Delete a folder: use [rimraf](https://github.com/isaacs/rimraf) directly *(you don't need gulp-rimraf!)*:\r\n\r\n```javascript\r\nvar rimraf = require('rimraf'); // rimraf directly\r\ngulp.task('task', function (cb) {\r\n\trimraf('./folder', cb);\r\n});\r\n```\r\n\r\n2. Delete some files in a folder: use gulp-rimraf\r\n\r\n```javascript\r\nvar ignore = require('gulp-ignore');\r\nvar rimraf = require('gulp-rimraf');\r\n\r\ngulp.task('task', function() {\r\n  return gulp.src('./**/*.js', { read: false }) // much faster\r\n    .pipe(ignore('node_modules/**'))\r\n    .pipe(rimraf());\r\n});\r\n```\r\nSetting option `read` to false prevents gulp to read the contents of the files and makes this task much faster.\r\n\r\nFiles and folders outside the current working directory can be removed with `force` option.\r\n\r\n```javascript\r\nvar rimraf = require('gulp-rimraf');\r\n\r\ngulp.task('task', function() {\r\n  return gulp.src('../temp/*.js', { read: false })\r\n    .pipe(rimraf({ force: true }));\r\n});\r\n```\r\n\r\nLICENSE\r\n-------\r\n\r\n(MIT License)\r\n\r\nCopyright (c) 2014 [Richardson & Sons, LLC](http://richardsonandsons.com/)\r\n\r\nPermission is hereby granted, free of charge, to any person obtaining\r\na copy of this software and associated documentation files (the\r\n\"Software\"), to deal in the Software without restriction, including\r\nwithout limitation the rights to use, copy, modify, merge, publish,\r\ndistribute, sublicense, and/or sell copies of the Software, and to\r\npermit persons to whom the Software is furnished to do so, subject to\r\nthe following conditions:\r\n\r\nThe above copyright notice and this permission notice shall be\r\nincluded in all copies or substantial portions of the Software.\r\n\r\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\r\nEXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\r\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\r\nNONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE\r\nLIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION\r\nOF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION\r\nWITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\r\n",
  "readmeFilename": "README.md",
  "bugs": {
    "url": "https://github.com/robrich/gulp-rimraf/issues"
  },
  "_id": "gulp-rimraf@0.1.1",
  "_shasum": "5aaba8f870559ca4885c044385f9926c0f863f10",
  "_from": "gulp-rimraf@^0.1.0",
  "_resolved": "https://registry.npmjs.org/gulp-rimraf/-/gulp-rimraf-0.1.1.tgz"
}
