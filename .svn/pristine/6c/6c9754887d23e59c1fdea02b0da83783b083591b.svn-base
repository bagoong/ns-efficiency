/**
 * SuiteCommerce: Developing Web Stores
 * 
 * Front-end JavaScript Exercise Template for Widget SSP development
 *
 * This file contains front end JavaScript used to render a simple web page
 * containing information about widgets and logged in customers.
 * Content exists to execute AJAX requests to process CRUD operations against
 * the Widgets custom record type. There are additional AJAX requests to log
 * customers on and off the web store.
 * 
 * This front-end JavaScript uses jQuery. jQuery is a core front-end library
 * used in the SuiteCommerce reference implementations.
 * 
 * Much of the JavaScript in this file will be used as is, but there is
 * configuration in future exercises to do the following:
 *     - configure URL on AJAX requests
 *     - configure objects for rendering widget and customer profile information
 *       (objects populated on the server, then transferred to client via SSP)
 *     - areas requiring further configuration marked with <* *>
 */

// Set up events for all the buttons, such that when clicked,
// AJAX calls are made to SuiteScript Services (SS) to  get, add,
// update, and delete widgets, as well as process customer login/logout
var setEvents = function(){

	// Log in the customer to the web store
	$('#button-post-login').on('click', function(event){
		event.preventDefault();

		// grab email and password off of form
		var email = $('#email').val();				
		var password = $('#password').val();				
				
		// email and password get sent as JSON string to SS
		var formData = {
			email: email,
			password: password
		};
		
		// AJAX request
		$.ajax({
			type: 'POST',
			// URL begins from root of SSP
			url: '<* url to login service *>',
			contentType: 'application/json; charset=UTF-8',
			data: JSON.stringify(formData),
			processData: false,
			success: function(data, status, jqXHR){
				$('#result-login').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('POST-login request failed');
			}
		});
	});		

	// Log out the customer from the web store
	$('#button-post-logout').on('click', function(event){
		event.preventDefault();

		// AJAX request
		$.ajax({
			type: 'POST',
			// URL begins from root of SSP
			url: '<* url to login service *>',
			contentType: 'application/json; charset=UTF-8',
			success: function(data, status, jqXHR){
				$('#result-login').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('POST-logout request failed');
			}
		});
	});			
	
	// Get single widget or list of widgets
	$('#button-get-widget').on('click', function(event){
		event.preventDefault();

		// <* add to query string of url in AJAX request below *>
		var internalid = $('#internalid').val();				

		// AJAX request
		$.ajax({
			type: 'GET',
			// URL begins from root of SSP
			url: '<* url to widget service *>',
			success: function(data, status, jqXHR){
				$('#result-widget').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('GET request failed');
			}
		});
	});	

	// Add a widget
	$('#button-post-widget').on('click', function(event){
		event.preventDefault();

		// grab color, size, and shape off of form
		var color = $('#color').val();
		var size = $('#size').val();
		var shape = $('#shape').val();

		// color, size, and shape get sent as JSON string to SS
		var formData = {
			color: color,
			size: size,
			shape: shape
		};

		// AJAX request
		$.ajax({
			type: 'POST',
			// URL begins from root of SSP
			url: '<* url to widget service *>',
			contentType: 'application/json; charset=UTF-8',
			data: JSON.stringify(formData),
			processData: false,
			success: function(data, status, jqXHR){
				$('#result-widget').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('POST request failed');
			}
		});
	});	

	// Update a widget
	$('#button-put-widget').on('click', function(event){
		event.preventDefault();

		// grab color, size, and shape off of form
		var color = $('#color').val();
		var size = $('#size').val();
		var shape = $('#shape').val(); 

		// color, size, and shape get sent as JSON string to SS
		var formData = {
			color: color,
			size: size,
			shape: shape
		};

		// <* add to query string of url in AJAX request below *>
		var internalid = $('#internalid').val();		
		
		// AJAX request
		$.ajax({
			type: 'PUT',
			// URL begins from root of SSP
			url: '<* url to widget service *>',
			contentType: 'application/json; charset=UTF-8',
			data: JSON.stringify(formData),
			processData: false,
			success: function(data, status, jqXHR){
				$('#result-widget').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('PUT request failed');
			}
		});
	});	

	// Delete a widget
	$('#button-delete-widget').on('click', function(event){
		event.preventDefault();

		// <* add to query string of url in AJAX request below *>
		var internalid = $('#internalid').val();				

		// AJAX request
		$.ajax({
			type: 'DELETE',
			// URL begins from root of SSP
			url: '<* url to widget service *>',
			success: function(data, status, jqXHR){
				$('#result-widget').text(JSON.stringify(data));
			},
			error: function(jqXHR, status, error){
				console.log('DELETE request failed');
			}
		});
	});	
};

// Get customer profile information upon page load
// <* Update the references to ObjectReference *>
var setCustomerProfile = function(){

	var profileMessage;
	
	if (customerProfile.loggedIn){
		profileMessage = 
			'<div>Name: ' + ObjectReferenceToName + '</div>' + 
			'<div>Balance: ' + ObjectReferenceToBalance + '</div>' + 
			'<div>Phone: ' + (ObjectReferenceToPhone || '') + '</div>' + 
			'<div>Alt Phone: ' + (ObjectReferenceToAltPhone || '') + '</div>' +
			'<div>Customer Code: ' + (ObjectReferenceToCustomerCode || '') + '</div>';
	} else {
		profileMessage = '<div>Please log in</div>';
	}
	
	$('#profile-html').html(profileMessage);			
};


// Get list of widgets upon page load
//<* Update the references to ObjectReference *>
var setWidgetList = function(){

	// The "widgets" variable is the client-side variable you defined inside
	// index.ssp. It should have been set to the serialization (i.e. JSON.stringify) 
	// of your widget search results. Change the variable name if you named it
	// something different in index.ssp
	if (widgets){
		var table = '<table class="widgetStyle"><tr>' +
	    '<td>Widget Id</td>' +
	    '<td>Widget Color</td>' +
	    '<td>Widget Size</td>' +
	    '<td>Widget Shape</td>' +
	    '</tr>';
		
		// $.each is a jQuery function that loops through the array of widgets.
		// The array is represented by the "widgets" variable. $.each
		// automatically passes each array entry to the anonymous function,
		// passing it in as the variable "widget". Depending on what you named
		// each object key for a widget in the widgets array, an object reference
		// to widget color might be entered as "widget.color". 
		$.each(widgets, function( index, widget){
			table += '<tr>';
			table += '<td>' + ObjectReferenceInternalId + '</td>';
			table += '<td>' + ObjectReferenceColor + '</td>';
			table += '<td>' + ObjectReferenceSize + '</td>';
			table += '<td>' + ObjectReferenceShape + '</td>';
			table += '</tr>';
		});
		
		table += '</table>';
		
		$('#widgets-html').html(table);					
	} else {
		$('#widgets-html').html('<b>Cannot list widgets until customer is logged in</b>');
	}
};

// Set up options for the widget shape dropdown
//<* Update the references to ObjectReference *>
var setWidgetShapes = function() {
	
	var option, options = '';
	
	$.each(widgetShapes, function(index, shape){
		option = '<option value="' + ObjectReferenceShapeId + '">' +
		         ObjectReferenceShapeName +
		         '</option>';
		
		options = options + option;
	});	
	
	$('#shape').html(options);	
};

// Wait until document is loaded and ready before proceeding with all the startup script
// <* Uncomment function calls when instructed throughout exercises *>
$(document).ready(function(){
	setEvents();
	//setCustomerProfile();
	//setWidgetList();
	//setWidgetShapes();
});
