// exercise 04-04: moved these variables from getCustomerProfile so they
//                 can be accessed globally from multiple functions, such
//                 as from both getListOfWidgets and getCustomerProfile
//                 functions, as well as from index.ssp
var webContainer = nlapiGetWebContainer();
var shoppingSession = webContainer.getShoppingSession();
var isLoggedIn = shoppingSession.isLoggedIn();


// exercise 04-02: code to get list of widgets moved from index.ssp
var getListOfWidgets = function(){

	// exercise 04-04: get internal id of logged in customer; we assume
	//                 customer is logged in as we should only be calling
	//                 getListOfWidgets if the customer is logged in
	var customer = shoppingSession.getCustomer();
	var customerFields = customer.getFieldValues(['internalid']);
	var internalId = customerFields.internalid;
	nlapiLogExecution('DEBUG', 'customer internal id', internalId);
	
	// exercise 04-04: add dynamic search filter to restrict widgets
	//                 to logged in customer
	var searchFilter = new nlobjSearchFilter
		('custrecord_gen_widget_customer',null,'anyof',internalId);
	var searchFilters = [];
	searchFilters.push(searchFilter);
	
	var searchResults = nlapiSearchRecord('customrecord_gen_widgets',
                                          'customsearch_gen_widgets_all',
                                          searchFilters);

	var widgets = []; 
	
	for (var i=0; i < searchResults.length; i++){
	var searchResult = searchResults[i];	                                     
	
	var internalid = searchResult.getId();
	var color = searchResult.getValue('custrecord_gen_widget_color');
	var size = searchResult.getValue('custrecord_gen_widget_size');
	var shape = searchResult.getText('custrecord_gen_widget_shape');
	
	widgets.push({
		internalid: internalid,
		color: color,
		size: size,
		shape: shape
		});
	}
	
	nlapiLogExecution('DEBUG', 'widget results', JSON.stringify(widgets));	
	
	return widgets; // exercise 04-02: return results to index.ssp
}

// exercise 04-02: code to get customer profile moved from index.ssp
var getCustomerProfile = function() {
	
	nlapiLogExecution('DEBUG', 'isLoggedIn', isLoggedIn);

	if (isLoggedIn){
		var customer = shoppingSession.getCustomer();
	
		
		var customerFields = customer.getFieldValues
			(['firstname','lastname','balance','phoneinfo']);
			
		nlapiLogExecution('DEBUG', 'customer fields',
		                  JSON.stringify(customerFields));
	
		var customFields = customer.getCustomFieldValues();
		nlapiLogExecution('DEBUG', 'custom field values', 
				          JSON.stringify(customFields));
		
		// exercise 04-03: get custom field customer code
		var customerCode;
		
		for (var i=0; i < customFields.length; i++){
			if (customFields[i].name == 'custentity_gen_customer_code'){
				customerCode = customFields[i].value;
			}
		}
		
		var customerProfile = {};
		
		customerProfile = {
			loggedIn: isLoggedIn,
			customername: customerFields.firstname + ' ' +
			              customerFields.lastname,
			balance: customerFields.balance,
			phone: customerFields.phoneinfo.phone,
			altphone: customerFields.phoneinfo.altphone,
			customercode: customerCode  // exercise 04-03: add customer code
		};
				
	} else {
		var customerProfile = {
			loggedIn: isLoggedIn
		};
	}
	
	nlapiLogExecution('DEBUG', 'customer profile',
	                  JSON.stringify(customerProfile));	
	
	return customerProfile; // exercise 04-02: return results to index.ssp
}

// exercise 05-03: move code that processes GET of widget to ssp library
var getWidget = function(internalid){
	
	var internalid = request.getParameter('internalid');
	
	var recWidget = 
		nlapiLoadRecord('customrecord_gen_widgets', internalid);
	
	var color = recWidget.getFieldValue('custrecord_gen_widget_color');
	var size = recWidget.getFieldValue('custrecord_gen_widget_size');
	var shape = recWidget.getFieldText('custrecord_gen_widget_shape');
	
	var widget = {
		color: color,
		size: size,
		shape: shape,
		internalid: internalid
	};
	
	widgetSerialized = JSON.stringify(widget);
	nlapiLogExecution('DEBUG', 'widget', widgetSerialized);
	
	return widgetSerialized; // exercise 05-03: returning serialized JSON to caller
}

// exercise 05-03: move code that processes POST of widget to ssp library
var addWidget = function(widget){
	
	var recWidget = nlapiCreateRecord('customrecord_gen_widgets');
	
	recWidget.setFieldValue('custrecord_gen_widget_color', widget.color);
	recWidget.setFieldValue('custrecord_gen_widget_size', widget.size);

	// exercise 05-02: optional section to set the current shopper into the
	//                 new widget record; applicable if you had previously 
	//                 completed 04-04
	if (isLoggedIn){
		var customer = shoppingSession.getCustomer();
		var customerFields = customer.getFieldValues(['internalid']);
		var customerId = customerFields.internalid;			
		
		recWidget.setFieldValue('custrecord_gen_widget_customer', customerId);
	}
	
	var widgetInternalId = nlapiSubmitRecord(recWidget);
	
	var widget = {
		color: widget.color,
		size: widget.size,
		internalid: widgetInternalId
	};

	widgetSerialized = JSON.stringify(widget);
	nlapiLogExecution('DEBUG', 'widget', widgetSerialized);
	
	return widgetSerialized; // exercise 05-03: returning serialized JSON to caller
}

// exercise 05-04: add support for DELETE
var deleteWidget = function(internalid){
	
	var internalid = request.getParameter('internalid');
	
	nlapiDeleteRecord('customrecord_gen_widgets', internalid);
		
	var result = {
		message: 'deletion okay for record with internal id: ' + internalid
	};
	
	resultSerialized = JSON.stringify(result);
	nlapiLogExecution('DEBUG', 'widget deletion message', resultSerialized);
	
	return resultSerialized;
}

// exercise 05-04: add support for PUT
var updateWidget = function(internalid, widget){
	
	var recWidget = nlapiLoadRecord('customrecord_gen_widgets', internalid);
	
	recWidget.setFieldValue('custrecord_gen_widget_color', widget.color);
	recWidget.setFieldValue('custrecord_gen_widget_size', widget.size);
	
	var widgetInternalId = nlapiSubmitRecord(recWidget);
	
	var widget = {
		color: widget.color,
		size: widget.size,
		internalid: widgetInternalId
	};

	widgetSerialized = JSON.stringify(widget);
	nlapiLogExecution('DEBUG', 'widget from update', widgetSerialized);
	
	return widgetSerialized;
}