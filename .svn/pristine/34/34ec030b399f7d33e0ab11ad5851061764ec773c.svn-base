

Application.extendModel('LiveOrder', {

    /*
    Receives the original line to be added, by reference
    Modifies that line, and returns the sketch of another line to be added as the gift wrap line
     */
    tweakLinesGetGiftWrap: function(currentLine){

        if(currentLine && currentLine.options && currentLine.options.custcol_ef_gw_giftwrap) {
            var idGenerator = function (qty) {
                    return (Math.random().toString(36) + '00000000000000000').slice(2, qty + 2)
                },
                generatedId = idGenerator(8),
                generatedIdNL = new String('G:' + generatedId).toString(), //HACKS for weird platform bugs
                generatedIdOld = new String('P:' + generatedId).toString(), //HACKS for weird platform bugs
                giftItemId = currentLine.options.custcol_ef_gw_giftwrap,
                newLine = {
                    item: {
                        internalid: parseInt(giftItemId, 10)
                    },
                    quantity: currentLine.quantity,
                    options: {
                        custcol_ef_gw_id: generatedIdNL
                    }
                };

            currentLine.options.custcol_ef_gw_id = generatedIdOld;
            return newLine;
        }
        else
            return null;

    },
    addGiftWraps: function(lines)
    {

        if (_.isArray(lines) && lines.length === 1) { //Only 1 line add to cart support right now

            var currentLine = lines[0],
                giftWrapLine = this.tweakLinesGetGiftWrap(currentLine);

            if(giftWrapLine){
                lines[1] = giftWrapLine;
            }
        }

    },
    addGiftWrap: function(currentLine)
    {
        var giftWrapLine = this.tweakLinesGetGiftWrap(currentLine);
        if(giftWrapLine)
        {
            Application.once('after:LiveOrder.addLine', function(Model,responseData){
                if(responseData)
                {
                    Model.addLine(giftWrapLine);
                }
            });
        }
    },
    removeGiftWrap: function(currentLine)
    {
        var order_field_keys = [
            	'orderitemid'
            ,	'quantity'
            ,	'internalid'
            ,	'options'
        ];

        //Removing current line, we have to find the giftwrap
        var line = order.getItem(currentLine, order_field_keys),
            optionGwId = _.findWhere(line.options, {id:"CUSTCOL_EF_GW_ID"}),
            optionGw = _.findWhere(line.options, {id: 'CUSTCOL_EF_GW_GIFTWRAP'})

        //If it has a giftwrap
        if(optionGwId && optionGw && optionGwId.value && optionGw.value){

            //we have to search for the other line :(
            var key = optionGwId.value.replace('P:','G:'),
                lines = order.getItems(order_field_keys);


            // Why EVERY instead of each? Every will break on the first FALSE returned;
            // We need to iterate only until we get the gift wrap item
            _.every(lines, function(l) {

                var gwId = _.findWhere(l.options,{id:"CUSTCOL_EF_GW_ID"});
                //Found it? so after the removeLine, let's hang to it
                if(gwId && gwId.value === key)
                {
                    Application.once('after:LiveOrder.removeLine', function(Model, responseData){
                        Model.removeLine(l.orderitemid);
                    });
                    return false;
                }
                return true;
            });
        }
    },
    reformatLines: function(lines)
    {
        var giftWrapsHashMap = {};
        for(var i=0; i<lines.length; i++){
            var line = lines[i];
            if(line.options){

                var option = _.findWhere(line.options,{id:"CUSTCOL_EF_GW_ID"}),
                    type = option && option.value && option.value.substr(0,2);

                if(type)
                {
                    var key = option.value.replace(type,'');
                    if(type == 'G:')
                    {
                        giftWrapsHashMap[key] = giftWrapsHashMap[key] || {};
                        giftWrapsHashMap[key].giftwrap = i;
                    }
                    if(type == 'P:')
                    {
                        giftWrapsHashMap[key] = giftWrapsHashMap[key] || {};
                        giftWrapsHashMap[key].parent = i;
                    }
                }
            }
        }
        var offsets = 0;
        _.each(giftWrapsHashMap, function(value,key) {
            if(value && !_.isUndefined(value.giftwrap) && !_.isUndefined(value.parent))
            {
                lines[value.parent - offsets].giftWrap = lines[value.giftwrap - offsets];
                lines.splice(value.giftwrap - offsets,1);

                offsets++;

            } else {
                console.error('Error with giftwrap sync', 'Key:'+ JSON.stringify(key)+ ',Value:'+ JSON.stringify(value));
            }
        });
        
    }
});


/* EVENTS: */
Application.on('before:LiveOrder.addLine', function(Model, currentLine){
    Model.addGiftWrap(currentLine);
});
Application.on('before:LiveOrder.addLines', function(Model, lines){
    Model.addGiftWraps(lines);
});
Application.on('before:LiveOrder.removeLine', function(Model, currentLine){
    Model.removeGiftWrap(currentLine);
});
Application.on('after:LiveOrder.getLines', function(Model,lines){
    Model.reformatLines(lines);
});
/* END EVENTS */