/**
 * SuiteCommerce: Developing Web Stores
 * 
 * Front-end JavaScript Template for method to process the creation of a
 * Backbone view for new widgets.
 * 
 * This function should be embedded as a method inside Widget.Router.js.
 * 
 * Two areas require further configuration. See sections enclosed in
 * <* *> for instructions.
 */
function newWidget ()
{
	var collection = 
		this.application.getUser().get('<* name of collection holding widget data *>')

	,	view = new Views.Details({
			application: this.application
		,	collection: collection
			
			// pass in an empty model to the view since end user will
		    // enter the data
		,	model: new Model()
		});
	
	if (collection) { 
		collection.on('add', function (){
			if (view.inModal && view.$containerModal)
			{
				view.$containerModal.modal('hide');
				view.destroy();
			}
			else
			{
				Backbone.history.navigate
				('<* key in routes hash that takes user back to list of widgets, appended with # symbol *>', 
				{trigger: true});
			}
		}, view);
	}

	// set an event that adds the model to the collection when the
	// model has changed. Note: a model is instantiated when the
	// view is created, and then changes after data is entered for
	// a widget and Save Widget is clicked
	view.model.on('change', function ( model ) {
		collection.add( model );
	}, this);

	view.showContent();
}		
