/*global google*/
define('Store.Views.StoreLocator.Search', ['GoogleMapsLoader'], function (GoogleMapsLoader)
{
    'use strict';

    return Backbone.View.extend({
        template: 'storelocator_search',
        events: {
            'click [name=geolocate]': 'geolocate',
            'reset form': 'resetSearch',
            'submit form': 'submitSearch',
            'change #storelist-distancefilter': 'submitSearch'
        },

        render: function ()
        {
            this._render();
            GoogleMapsLoader.loadScript(this.options.configuration.googleMapsApiKey).done(_.bind(this.setupSearch, this));
        },
        setupSearch: function ()
        {
            var self = this,
                autocomplete = new google.maps.places.Autocomplete(this.$('#storelist-place')[0]),
                place,
                bounds;

            this.geocoder = new google.maps.Geocoder;

            google.maps.event.addListener(autocomplete, 'place_changed', function ()
            {

                self.options.eventBus.trigger('storeSelected', {
                    component: 'search'
                });

                place = autocomplete.getPlace();
                if (!place.geometry)
                {
                    return;
                }

                self.options.eventBus.trigger('originChanged', {
                    location: place.geometry.location,
                    component: 'search',
                    distanceRatio: self.getSelectedDistance()
                });

            });

            this.createDistanceFilter();

            if (this.options.configuration.geolocationEnabled && this.options.configuration.geolocateOnInit)
            {
                this.geolocate();
            }

            //Bias to locations near stores
            bounds = new google.maps.LatLngBounds();
            this.collection.each(function (s)
            {
                bounds.extend(new google.maps.LatLng(s.get('lat'), s.get('lon')));
            });
            autocomplete.set('bounds', bounds);

        },

        getSelectedDistance: function ()
        {
            var $el = this.$('#storelist-distancefilter option:selected'),
                distance = parseInt($el.val(), 10),
                unit = $el.data('unit');

            return this.options.configuration.unitsHandlers[unit].conversionToKilometers(distance);
        },

        createDistanceFilter: function ()
        {

            var elements = _.map(this.options.configuration.distanceFilters, function (l)
            {
                return jQuery('<option />', { 'data-unit': l.unit, value: l.value, text: l.text, selected: l.isDefault ? 'selected' : null});
            });

            this.$('#storelist-distancefilter').append(elements);

        },

        resetSearch: function (e)
        {
            e.preventDefault();
            e.stopPropagation();

            this.$('#storelist-place').val('');
            this.options.eventBus.trigger('storeSelected', {
                component: 'search'
            });

            this.options.eventBus.trigger('originChanged', {
                location: null,
                component: 'search',
                distanceRatio: this.getSelectedDistance()
            });
        },

        submitSearch: function (e)
        {
            e.preventDefault();
            e.stopPropagation();

            var self = this,
                address = this.$('#storelist-place').val(),
                request;

            if (!address || address.length < 1)
            {
                return;
            }

            request = {
                address: address
            };

            this.geocoder && this.geocoder.geocode(request, function (result, status)
            {
                if (status !== google.maps.GeocoderStatus.OK)
                {
                    return;
                }
                self.options.eventBus.trigger('originChanged', {
                    location: result[0].geometry.location,
                    component: 'search',
                    distanceRatio: self.getSelectedDistance()
                });
            });
        },
        geolocate: function ()
        {
            var self = this,
                location;

            if (window.navigator && navigator.geolocation)
            {

                navigator.geolocation.getCurrentPosition(function (pos)
                {

                    location = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

                    self.options.eventBus.trigger('originChanged', {
                        location: location,
                        component: 'search',
                        distanceRatio: self.getSelectedDistance()
                    });

                    self.geocoder.geocode({'latLng': location}, function (results, status)
                    {
                        if (status === google.maps.GeocoderStatus.OK)
                        {
                            if (results[0])
                            {
                                var $searchBox = self.$('#storelist-place');
                                if (!$searchBox.val())
                                {
                                    $searchBox.val(results[0].formatted_address);
                                }
                            }
                        }
                    });

                }, undefined, /** @type GeolocationPositionOptions */({
                    maximumAge: 60 * 1000,
                    timeout: 10 * 1000
                }));
            }
        }
    });
});