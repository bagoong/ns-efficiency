Application.extendModel('LiveOrder', {
    addFiles: function (files) {

        if(!this.isSecure) return;

        var self = this;
        if (this.isSecure && !_.isEmpty(files))
        {
            _.each(files, function(file){
                self.addFile(file);
            });
        }
    },
    validateToken: function(file){

        var actionToken = new ActionToken(_InternalTBFConfig,40),
            r = actionToken.read(file && file.t, 40);

        if(!r.userId || r.userId!=nlapiGetUser())
        {
            throw nlapiCreateError('ERR_FILE_SEC', 'Invalid file (u)');
        }
        if(file.file != r.data.file){
            throw nlapiCreateError('ERR_FILE_SEC', 'Invalid file (f)');
        }
        if(file.filename!= r.data.filename){
            throw nlapiCreateError('ERR_FILE_SEC', 'Invalid file (fn)');
        }
        if(file.link!= r.data.link)
        {
            throw nlapiCreateError('ERR_FILE_SEC', 'Invalid file (ln)');
        }

    },
    addFile: function(file){

        if(!this.isSecure) return;

        var currentFiles = this.getFileTransactionBodyFieldsValues(),
            currentFile = _.findWhere(currentFiles, {internalid: file.internalid}),
            objToSave = {},
            field;

        this.validateToken(file);

        field = _.findWhere(SC.Configuration.Efficiencies.TransactionBodyFile.fields,{internalid: file.internalid});

        if(!field)
        {
            throw methodNotAllowedError;
        }

        objToSave[field.bodyFields.file] = file.file;
        objToSave[field.bodyFields.filename] = file.name;
        objToSave[field.bodyFields.link] = file.link;

        order.setCustomFieldValues(objToSave);


    },

    removeFile: function(internalid){

        if(!this.isSecure) return;

        var currentFiles = this.getFileTransactionBodyFieldsValues(),
            currentFile = _.findWhere(currentFiles, {internalid: internalid}),
            field = _.findWhere(SC.Configuration.Efficiencies.TransactionBodyFile.fields,{internalid: internalid}),
            objToSave = {};

        if(!field){
            throw methodNotAllowedError;
        }

        objToSave[field.bodyFields.file] = '';
        objToSave[field.bodyFields.filename] = '';
        objToSave[field.bodyFields.link] = '';

        order.setCustomFieldValues(objToSave);

        this.fileOptionsProcessed = false;
        this.fileOptions = null;
    },
    getFileTransactionBodyFieldsListNames: function () {
        var fields = SC.Configuration.Efficiencies.TransactionBodyFile.fields;
        return _.flatten(_.map(fields, function (field) {
            return _.values(field.bodyFields);
        }));
    },
    getFileTransactionBodyFieldsValues: function (){

        var fileOptions;
        //First, get the values
        if(!this.fileOptionsProcessed){
            fileOptions = _.omit(this.getTransactionBodyField(),this.getFileTransactionBodyFieldsListNames());
        }
        else {
            fileOptions = this.fileOptions;
        }
        fileOptions = fileOptions || {};

        return _.reject(_.map(SC.Configuration.Efficiencies.TransactionBodyFile.fields, function(value) {
            return {
                internalid: value.internalid,
                file: fileOptions[value.bodyFields.file],
                filename: fileOptions[value.bodyFields.filename],
                link: fileOptions[value.bodyFields.link]
            };
        }), function(line){
            return !line.file;
        });

    }
});


Application.on('before:LiveOrder.setTransactionBodyField', function(Model,data){
    //This options should only be modified through addFile/removeFile
    if(!Model.isSecure) return;

    var fileFields = Model.getFileTransactionBodyFieldsListNames();
    if (!_.isEmpty(data.options))
    {
        data.options = _.omit(data.options, fileFields);
    }
});


Application.on('after:LiveOrder.get', function(Model,data){
    //append file info to the response

    if(!Model.isSecure) return;

    var fileFields = Model.getFileTransactionBodyFieldsListNames();
    if (!_.isEmpty(data.options))
    {
        Model.fileOptions = _.pick(data.options,fileFields);
        Model.fileOptionsProcessed = true;
        data.options = _.omit(data.options, fileFields);

    } else {
        Model.fileOptionsProcessed = true;
    }

    data.files = Model.getFileTransactionBodyFieldsValues();
});

Application.on('before:LiveOrder.submit', function(Model, data){
    var requiredIds = _.chain(SC.Configuration.Efficiencies.TransactionBodyFile.fields).where({required:true}).pluck('internalid').value(),
        withDataIds = _.chain(Model.getFileTransactionBodyFieldsValues()).pluck('internalid').value(),
        missing = _.difference(requiredIds,withDataIds);

    if( missing > 0 ){
        throw nlapiCreateError('ERR_FILE_MISSING', 'File Missing');
    }


});