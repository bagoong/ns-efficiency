/*
 * exercise 08-01: Build this file to coordinate processing of the widget model.
 */ 

var webContainer = nlapiGetWebContainer();
var shoppingSession = webContainer.getShoppingSession();

// exercise 08-01: Using the existing Address model as a template for Widget model
Application.defineModel('Widget', {

// exercise 08-01: Copy over the code from sharedprocessing.js
// Get a single widget, widgets.ss needs to have internalid of a widget in its request
	getWidget: function (internalid)
	{
		'use strict';

		var internalid = request.getParameter('internalid');
		
		var recWidget = 
			nlapiLoadRecord('customrecord_gen_widgets', internalid);
		
		var color = recWidget.getFieldValue('custrecord_gen_widget_color');
		var size = recWidget.getFieldValue('custrecord_gen_widget_size');
		var shape = recWidget.getFieldText('custrecord_gen_widget_shape');
		
		var widget = {
			color: color,
			size: size,
			shape: shape,
			internalid: internalid
		};
		
		widgetSerialized = JSON.stringify(widget);
		nlapiLogExecution('DEBUG', 'widget', widgetSerialized);
		
		return widgetSerialized; // exercise 05-03: returning serialized JSON to caller	
	}

// exercise 08-01: Copy over the code from sharedprocessing.js
// Get all widgets, filtered here to the logged in customer
,	getListOfWidgets: function ()
	{
		'use strict';

		// exercise 04-04: get internal id of logged in customer; we assume
		//                 customer is logged in as we should only be calling
		//                 getListOfWidgets if the customer is logged in
		var customer = shoppingSession.getCustomer();
		var customerFields = customer.getFieldValues(['internalid']);
		var internalId = customerFields.internalid;
		nlapiLogExecution('DEBUG', 'customer internal id', internalId);
		
		// exercise 04-04: add dynamic search filter to restrict widgets
		//                 to logged in customer
		var searchFilter = new nlobjSearchFilter
			('custrecord_gen_widget_customer',null,'anyof',internalId);
		var searchFilters = [];
		searchFilters.push(searchFilter);
		
		var searchResults = nlapiSearchRecord('customrecord_gen_widgets',
	                                          'customsearch_gen_widgets_all',
	                                          searchFilters);

		var widgets = []; 
		
		for (var i=0; i < searchResults.length; i++){
		var searchResult = searchResults[i];	                                     
		
		var internalid = searchResult.getId();
		var color = searchResult.getValue('custrecord_gen_widget_color');
		var size = searchResult.getValue('custrecord_gen_widget_size');
		var shape = searchResult.getText('custrecord_gen_widget_shape');
		
		widgets.push({
			internalid: internalid,
			color: color,
			size: size,
			shape: shape
			});
		}
		
		nlapiLogExecution('DEBUG', 'widget results', JSON.stringify(widgets));	
		
		return widgets; // exercise 04-02: return results to index.ssp	
	}

// exercise 09-04: Copy over the code from sharedprocessing.js
// Add a widget
,	addWidget: function(widget)
	{
		'use strict';
		
		var recWidget = nlapiCreateRecord('customrecord_gen_widgets');
		
		recWidget.setFieldValue('custrecord_gen_widget_color', widget.color);
		recWidget.setFieldValue('custrecord_gen_widget_size', widget.size);
	
		// exercise 05-05
		recWidget.setFieldValue('custrecord_gen_widget_shape', widget.shape);
	
		// exercise 05-02: optional section to set the current shopper into the
		//                 new widget record; applicable if you had previously 
		//                 completed 04-04
		if (isLoggedIn){
			var customer = shoppingSession.getCustomer();
			var customerFields = customer.getFieldValues(['internalid']);
			var customerId = customerFields.internalid;			
			
			recWidget.setFieldValue('custrecord_gen_widget_customer', customerId);
		}
		
		var widgetInternalId = nlapiSubmitRecord(recWidget);
		
		var widget = {
			color: widget.color,
			size: widget.size,
			shape: widget.shape,  // exercise 05-05		
			internalid: widgetInternalId
		};
	
		widgetSerialized = JSON.stringify(widget);
		nlapiLogExecution('DEBUG', 'widget', widgetSerialized);
		
		return widgetSerialized; // exercise 05-03: returning serialized JSON to caller
	}

// exercise 09-05: Copy over the code from sharedprocessing.js
// Update a widget
,	updateWidget: function(internalid, widget)
	{
		'use strict';
	
		var recWidget = nlapiLoadRecord('customrecord_gen_widgets', internalid);
		
		recWidget.setFieldValue('custrecord_gen_widget_color', widget.color);
		recWidget.setFieldValue('custrecord_gen_widget_size', widget.size);
		
		// exercise 05-05
		recWidget.setFieldValue('custrecord_gen_widget_shape', widget.shape);
		
		var widgetInternalId = nlapiSubmitRecord(recWidget);
		
		var widget = {
			color: widget.color,
			size: widget.size,
			shape: widget.shape,  // exercise 05-05
			internalid: widgetInternalId
		};
	
		widgetSerialized = JSON.stringify(widget);
		nlapiLogExecution('DEBUG', 'widget from update', widgetSerialized);
		
		return widgetSerialized;
	}

// exercise 09-06: Copy over the code from sharedprocessing.js
// Delete a widget
,	deleteWidget: function(internalid)
	{	
		var internalid = request.getParameter('internalid');
		
		nlapiDeleteRecord('customrecord_gen_widgets', internalid);
			
		var result = {
			message: 'deletion okay for record with internal id: ' + internalid
		};
		
		resultSerialized = JSON.stringify(result);
		nlapiLogExecution('DEBUG', 'widget deletion message', resultSerialized);
		
		return resultSerialized;
	}

});