defineHelper('Quote.Expects', 
{
	overview: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.overviewTitle.replace('%s', params.quoteId), 5000)
			.getText(Selectors.Quote.overviewTitle.replace('%s', params.quoteId), function (err, text)
			{
				expect(text).toMatch(params.quoteName);
			})
			.getText(Selectors.Quote.overviewDate.replace('%s', params.quoteId), function (err, text)
			{
				expect(text).toBeEqualDates(params.quoteRequestDate);
			})
			.getText(Selectors.Quote.overviewExpirationDate.replace('%s', params.quoteId), function (err, text)
			{
				expect(text).toBeEqualDates(params.quoteExpirationDate);
			})
			.getText(Selectors.Quote.overviewAmount.replace('%s', params.quoteId), function (err, text)
			{
				expect(text).toBe(params.quoteAmount);
			})
			.getText(Selectors.Quote.overviewStatus.replace('%s', params.quoteId), function (err, text)
			{
				expect(text).toMatch(params.quoteStatus);
			})
			.call(cb)
		;
	}

,	detail: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.detailName, 5000)
			.getText(Selectors.Quote.detailName, function (err, text)
			{
				expect(text).toBe(params.quoteName);
			})
			.getText(Selectors.Quote.detailRequestDate, function (err, text)
			{
				expect(text).toBeEqualDates(params.quoteRequestDate);
			})
			.getText(Selectors.Quote.detailExpirationDate, function (err, text)
			{
				expect(text).toBeEqualDates(params.quoteExpirationDate);
			})
			.getText(Selectors.Quote.detailAmount, function (err, text)
			{
				expect(text).toBe(params.quoteAmount);
			})
			.getText(Selectors.Quote.detailStatus, function (err, text)
			{
				expect(text).toMatch(params.quoteStatus);
			})
			.call(cb)
		;
	}	

,	item: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.detailItemId, 5000)
			.getAttribute(Selectors.Quote.detailItemId, 'data-id', function (err, text)
			{
				expect(text).toMatch(params.id);
			})
			.getText(Selectors.Quote.detailItemName, function (err, text)
			{
				expect(text).toMatch(params.name);
			})
			.getText(Selectors.Quote.detailItemSku, function (err, text)
			{
				expect(text).toMatch(params.sku);
			})
			.getText(Selectors.Quote.detailItemQuantity, function (err, text)
			{
				expect(text).toMatch(params.quantity);
			})
			.getText(Selectors.Quote.detailItemPrice, function (err, text)
			{
				expect(text).toMatch(params.price);
			})
			.getText(Selectors.Quote.detailItemAmount, function (err, text)
			{
				expect(text).toMatch(params.amount);
			})
			.call(cb)
		;
	}

,	summary: function(params, cb)
	{
		this
			.waitForExist(Selectors.Quote.detailSummarySubtotal, 5000)
			.getText(Selectors.Quote.detailSummarySubtotal, function (err, text)
			{
				expect(text).toBe(params.subtotal);
			})
			.getText(Selectors.Quote.detailSummaryDiscount, function (err, text)
			{
				expect(text).toBe(params.discount);
			})
			.getText(Selectors.Quote.detailSummaryTax, function (err, text)
			{
				expect(text).toBe(params.taxTotal);
			})
			.getText(Selectors.Quote.detailSummaryShipping, function (err, text)
			{
				expect(text).toBe(params.shippingCost);
			})
			.getText(Selectors.Quote.detailSummaryTotal, function (err, text)
			{
				expect(text).toBe(params.total);
			})
			.call(cb)
		;
	}

,	message: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.detailMessage, 5000)
			.getText(Selectors.Quote.detailMessage, function (err, text)
			{
				expect(text).toBe(params.message);
			})
			.call(cb)
		;	
	}

,	thankYouPageMessage: function (cb)
	{
		this
			.waitForExist(Selectors.Quote.thankYouMessage, 5000)
			.getText(Selectors.Quote.thankYouMessage, function (err, text)
			{
				expect(text).toBeSameText('THANK YOU!');
			})
			.call(cb)
		;
	}

,	quoteDetailPage: function (cb)
	{
		this
			.waitForExist(Selectors.Quote.qdpTitle, 5000)
			.getText(Selectors.Quote.qdpTitle, function (err, text)
			{
				expect(text).toBeSameText('QUOTE');
			})
			.call(cb)
		;
	}


,	reviewAndPlaceOrderButton: function (params, cb)
	{
		var client = this;
		this
			.waitForExist(Selectors.Quote.buttonReviewAndPlaceOrder, 5000)
			.isExisting(Selectors.Quote.buttonReviewAndPlaceOrder).then(function (isExisting)
			{
				if (isExisting)
				{
					client.Quote.Expects.reviewAndPlaceOrderButtonStatus(params);
				}
				expect(isExisting).toBe(params.visibility);
			})
			.call(cb)
		;
	}

,	placeOrderButton: function (params, cb)
	{
		var client = this;
		this
			.waitForExist(Selectors.Quote.buttonPlaceOrder, 5000)
			.isExisting(Selectors.Quote.buttonPlaceOrder).then(function (isExisting)
			{
				if (isExisting)
				{

					client.Quote.Expects.placeOrderButtonStatus(params);
				}
				expect(isExisting).toBe(params.visibility);
			})
			.call(cb)
		;
	}

,	buttonBacktoListofQuotes: function (params, cb)
	{
		var client = this;
		this
			.waitForExist(Selectors.Quote.buttonBacktoListofQuotes, 5000)
			.isExisting(Selectors.Quote.buttonBacktoListofQuotes).then(function (isExisting)
			{
				if (isExisting)
				{
					client.Quote.Expects.buttonBacktoListofQuotesStatus(params);
				}
				expect(isExisting).toBe(params.visibility);
			})
			.call(cb)
		;
	}

,	reviewAndPlaceOrderButtonStatus: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.buttonReviewAndPlaceOrderDisabled, 1000)
			.isExisting(Selectors.Quote.buttonReviewAndPlaceOrderDisabled).then(function (isExisting)
			{
				expect(isExisting).not.toBe(params.enabled);

			})
			.call(cb)
		;
	}

,	buttonBacktoListofQuotesStatus: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.buttonBacktoListofQuotes, 1000)
			.isExisting(Selectors.Quote.buttonBacktoListofQuotes).then(function (isExisting)
			{
				expect(isExisting).toBe(params.enabled);
			})
			.call(cb)
		;	
	}	

,	placeOrderButtonStatus: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.buttonPlaceOrderDisabled, 1000)
			.isExisting(Selectors.Quote.buttonPlaceOrderDisabled).then(function (isExisting)
			{
				expect(isExisting).not.toBe(params.enabled);
			})
			.call(cb)
		;
	}

,	missingInformationMessages: function (errors, cb)
	{
		var found_errors = [];

		this
			.waitForExist(Selectors.Quote.messageQuoteDetailErrorMessage, 5000)
			.getText(Selectors.Quote.messageQuoteDetailErrorMessage)
			.then(function (captured)
			{
				if (typeof captured === 'string')
				{
					captured = [captured];
				}
				captured.forEach(function (message)
				{
					found_errors.push(message);
				})
			})
			.getText(Selectors.Quote.messageErrorGiftCertificate)
			.then(function (captured)
			{
				found_errors.push(captured);
			})
			.then(function (all_text)
			{
				expect(found_errors).toBeSameArray(errors);
			})
			.call(cb)
		;
	}

,	salesRep: function (params, cb)
	{
		this
			.waitForExist(Selectors.Quote.messageDisclaimerMessage, 5000)
			.getText(Selectors.Quote.messageDisclaimerMessage).then(function (text)
			{
				var verification = 'For immediate assistance contact ' + params.name + ' at ' + params.phone + '. For additional information, send an email to ' + params.email + '.';
				expect(text).toBe(verification);
			})
			.call(cb)
		;	
	}	

,	checkQuotesPageDisplayed: function (cb)
	{
		this
			.isExisting(Selectors.Quote.quotesPageLocator).then(function (isExisting)
			{
            	expect(isExisting).toBeTrueOrFailWith('Quotes page is not present.');
        	})
        	.call(cb)
        ;
	}

,	quotePermissionMsg: function (params, cb)
	{
			this
			.waitForExist(Selectors.Quote.permissionDeniedMsg, 5000)
			.getText(Selectors.Quote.permissionDeniedMsg).then(function (text)
			{
				var verification = 'Sorry, you don\'t have sufficient permissions to request a quote online.\nFor immediate assistance call us at ' + params.phone + ' or email us to ' + params.email;
				expect(text).toBe(verification);
			})
			.call(cb)
		;
	}
});	

