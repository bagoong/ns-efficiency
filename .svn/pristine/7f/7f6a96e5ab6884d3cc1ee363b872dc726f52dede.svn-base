{
  "name": "connect",
  "description": "High performance middleware framework",
  "version": "3.2.0",
  "author": {
    "name": "TJ Holowaychuk",
    "email": "tj@vision-media.ca",
    "url": "http://tjholowaychuk.com"
  },
  "contributors": [
    {
      "name": "Douglas Christopher Wilson",
      "email": "doug@somethingdoug.com"
    },
    {
      "name": "Jonathan Ong",
      "email": "me@jongleberry.com"
    },
    {
      "name": "Tim Caswell",
      "email": "tim@creationix.com"
    }
  ],
  "keywords": [
    "framework",
    "web",
    "middleware",
    "connect",
    "rack"
  ],
  "repository": {
    "type": "git",
    "url": "git://github.com/senchalabs/connect"
  },
  "dependencies": {
    "debug": "~2.0.0",
    "finalhandler": "0.2.0",
    "parseurl": "~1.3.0",
    "utils-merge": "1.0.0"
  },
  "devDependencies": {
    "istanbul": "0.3.2",
    "mocha": "~1.21.4",
    "should": "~4.0.0",
    "supertest": "~0.13.0"
  },
  "license": "MIT",
  "engines": {
    "node": ">= 0.10.0"
  },
  "scripts": {
    "test": "mocha --require test/support/env --reporter spec --bail --check-leaks test/",
    "test-cov": "istanbul cover node_modules/mocha/bin/_mocha -- --require test/support/env --reporter dot --check-leaks test/",
    "test-travis": "istanbul cover node_modules/mocha/bin/_mocha --report lcovonly -- --require test/support/env --reporter spec --check-leaks test/"
  },
  "readme": "# Connect\n\n[![NPM Version][npm-image]][npm-url]\n[![NPM Downloads][downloads-image]][downloads-url]\n[![Build Status][travis-image]][travis-url]\n[![Test Coverage][coveralls-image]][coveralls-url]\n[![Gittip][gittip-image]][gittip-url]\n\n  Connect is an extensible HTTP server framework for [node](http://nodejs.org) using \"plugins\" known as _middleware_.\n\n```js\nvar connect = require('connect')\nvar http = require('http')\n\nvar app = connect()\n\n// gzip/deflate outgoing responses\nvar compression = require('compression')\napp.use(compression())\n\n// store session state in browser cookie\nvar cookieSession = require('cookie-session')\napp.use(cookieSession({\n    keys: ['secret1', 'secret2']\n}))\n\n// parse urlencoded request bodies into req.body\nvar bodyParser = require('body-parser')\napp.use(bodyParser.urlencoded())\n\n// respond to all requests\napp.use(function(req, res){\n  res.end('Hello from Connect!\\n');\n})\n\n//create node.js http server and listen on port\nhttp.createServer(app).listen(3000)\n```\n\n## Connect 3.0\n\nConnect 3.0 is in progress in the `master` branch. The main changes in Connect are:\n\n- Middleware will be moved to their own repositories in the [expressjs](http://github.com/expressjs) organization\n- All node patches will be removed - all middleware _should_ work without Connect and with similar frameworks like [restify](https://github.com/mcavage/node-restify)\n- Node `0.8` is no longer supported\n- The website documentation has been removed - view the markdown readmes instead\n\nIf you would like to help maintain these middleware, please contact a [member of the expressjs team](https://github.com/orgs/expressjs/people).\n\n## Middleware\n\nThese middleware and libraries are officially supported by the Connect/Express team:\n\n  - [body-parser](https://github.com/expressjs/body-parser) - previous `bodyParser`, `json`, and `urlencoded`. You may also be interested in:\n    - [body](https://github.com/raynos/body)\n    - [co-body](https://github.com/visionmedia/co-body)\n    - [raw-body](https://github.com/stream-utils/raw-body)\n  - [compression](https://github.com/expressjs/compression) - previously `compress`\n  - [connect-timeout](https://github.com/expressjs/timeout) - previously `timeout`\n  - [cookie-parser](https://github.com/expressjs/cookie-parser) - previously `cookieParser`\n  - [cookie-session](https://github.com/expressjs/cookie-session) - previously `cookieSession`\n  - [csurf](https://github.com/expressjs/csurf) - previousy `csrf`\n  - [errorhandler](https://github.com/expressjs/errorhandler) - previously `error-handler`\n  - [express-session](https://github.com/expressjs/session) - previously `session`\n  - [method-override](https://github.com/expressjs/method-override) - previously `method-override`\n  - [morgan](https://github.com/expressjs/morgan) - previously `logger`\n  - [response-time](https://github.com/expressjs/response-time) - previously `response-time`\n  - [serve-favicon](https://github.com/expressjs/serve-favicon) - previously `favicon`\n  - [serve-index](https://github.com/expressjs/serve-index) - previousy `directory`\n  - [serve-static](https://github.com/expressjs/serve-static) - previously `static`\n  - [vhost](https://github.com/expressjs/vhost) - previously `vhost`\n\nMost of these are exact ports of their Connect 2.x equivalents. The primary exception is `cookie-session`.\n\nSome middleware previously included with Connect are no longer supported by the Connect/Express team, are replaced by an alternative module, or should be superseded by a better module. Use one of these alternatives instead:\n\n  - `cookieParser`\n    - [cookies](https://github.com/jed/cookies) and [keygrip](https://github.com/jed/keygrip)\n  - `limit`\n    - [raw-body](https://github.com/stream-utils/raw-body)\n  - `multipart`\n    - [connect-multiparty](https://github.com/superjoe30/connect-multiparty)\n    - [connect-busboy](https://github.com/mscdex/connect-busboy)\n  - `query`\n    - [qs](https://github.com/visionmedia/node-querystring)\n  - `staticCache`\n    - [st](https://github.com/isaacs/st)\n    - [connect-static](https://github.com/andrewrk/connect-static)\n\nCheckout [http-framework](https://github.com/Raynos/http-framework/wiki/Modules) for many other compatible middleware! \n\n## Running Tests\n\n```bash\nnpm install\nnpm test\n```\n\n## Contributors\n\n https://github.com/senchalabs/connect/graphs/contributors\n\n## Node Compatibility\n\n  - Connect `< 1.x` - node `0.2`\n  - Connect `1.x` - node `0.4`\n  - Connect `< 2.8` - node `0.6`\n  - Connect `>= 2.8 < 3` - node `0.8`\n  - Connect `>= 3` - node `0.10`\n\n## License\n\n[MIT](LICENSE)\n\n[npm-image]: https://img.shields.io/npm/v/connect.svg?style=flat\n[npm-url]: https://npmjs.org/package/connect\n[travis-image]: https://img.shields.io/travis/senchalabs/connect.svg?style=flat\n[travis-url]: https://travis-ci.org/senchalabs/connect\n[coveralls-image]: https://img.shields.io/coveralls/senchalabs/connect.svg?style=flat\n[coveralls-url]: https://coveralls.io/r/senchalabs/connect\n[downloads-image]: https://img.shields.io/npm/dm/connect.svg?style=flat\n[downloads-url]: https://npmjs.org/package/connect\n[gittip-image]: https://img.shields.io/gittip/dougwilson.svg?style=flat\n[gittip-url]: https://www.gittip.com/dougwilson/\n",
  "readmeFilename": "Readme.md",
  "bugs": {
    "url": "https://github.com/senchalabs/connect/issues"
  },
  "homepage": "https://github.com/senchalabs/connect",
  "_id": "connect@3.2.0",
  "_shasum": "184f51545eb4d9aa2064504b67fc06f76ca21c08",
  "_from": "connect@^3.0.1",
  "_resolved": "https://registry.npmjs.org/connect/-/connect-3.2.0.tgz"
}
